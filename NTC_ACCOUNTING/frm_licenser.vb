﻿Imports MySql.Data.MySqlClient
Imports System.Net
Public Class frm_licenser
    Dim query As String
    Dim cmd As MySqlCommand
    Dim reader As MySqlDataReader
    Dim soa_details_id, mopID, result, transactID As Integer
    Dim yearDiffOne, yearDiffTwo, yearDiffThree, soa_override_number As Integer
    Dim serial, last_counter, starting_digit_soa As String
    Dim soa_override_activate_value = "0"
    Dim soa_override_isActivated = False
    Dim is_first_transact As Boolean = False
    Dim row_customize_no, column_customize_no, starting_series_soa, tabFocus As Integer
    Dim MOP() As String = {"", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "",
        "", "", "", "", "", "", "", "", "", "", "", "", "", ""}
    Dim customValues() As String = {"", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "",
        "", "", "", "", "", "", "", "", "", "", "", "", "", ""}
    Public isSetPayor, isSetType, isSetProcess, isSetService, isSetParticularName, isSetParticularMOD,
        isPaymentAdded As Boolean
    Public user_id, payor_id, authority_level As String

    Sub tab_color_reset()
        btn_tab_license.BackColor = Color.FromArgb(69, 90, 100)
        btn_tab_permits.BackColor = Color.FromArgb(69, 90, 100)
        btn_tab_roc.BackColor = Color.FromArgb(69, 90, 100)
        btn_tab_others.BackColor = Color.FromArgb(69, 90, 100)
        btn_tab_license.ForeColor = Color.Silver
        btn_tab_permits.ForeColor = Color.Silver
        btn_tab_roc.ForeColor = Color.Silver
        btn_tab_others.ForeColor = Color.Silver
    End Sub

    Private Sub btn_tab_license_Click(sender As Object, e As EventArgs) Handles btn_tab_license.Click
        tab_color_reset()

        btn_tab_license.BackColor = Color.FromArgb(113, 125, 137)
        btn_tab_license.ForeColor = Color.White
        pnl_license.BringToFront()
    End Sub

    Private Sub btn_tab_permits_Click(sender As Object, e As EventArgs) Handles btn_tab_permits.Click
        tab_color_reset()

        btn_tab_permits.BackColor = Color.FromArgb(113, 125, 137)
        btn_tab_permits.ForeColor = Color.White
        pnl_permits.BringToFront()
    End Sub

    Private Sub btn_tab_roc_Click(sender As Object, e As EventArgs) Handles btn_tab_roc.Click
        tab_color_reset()

        btn_tab_roc.BackColor = Color.FromArgb(113, 125, 137)
        btn_tab_roc.ForeColor = Color.White
        pnl_roc.BringToFront()
    End Sub

    Private Sub btn_tab_others_Click(sender As Object, e As EventArgs) Handles btn_tab_others.Click
        tab_color_reset()

        btn_tab_others.BackColor = Color.FromArgb(113, 125, 137)
        btn_tab_others.ForeColor = Color.White
        pnl_others.BringToFront()
    End Sub

    Private Sub btn_license_add_Click(sender As Object, e As EventArgs) Handles btn_license_add.Click
        Dim isItemExisted As Boolean = False
        Dim row As String()

        If cmb_license_items.Text <> "" Then
            initialize_table_license()

            If dgv_license.Rows.Count <> 0 Then
                For i As Integer = 0 To dgv_license.Rows.Count - 1 Step +1
                    If dgv_license.Rows(i).Cells(15).Value.ToString() = cmb_payment_number_license.Text Then
                        isItemExisted = True
                    End If
                Next
            End If

            If cb_particular_two.Checked = False And cb_particular_three.Checked = False Then
                dgv_license.Columns(6).ReadOnly = True
                dgv_license.Columns(7).ReadOnly = True
                dgv_license.Columns(8).ReadOnly = True
                dgv_license.Columns(9).ReadOnly = True
                dgv_license.Columns(10).ReadOnly = True
                dgv_license.Columns(11).ReadOnly = True
                dgv_license.Columns(12).ReadOnly = True
                dgv_license.Columns(13).ReadOnly = True
                yearDiffOne = DateDiff(DateInterval.Year, dtp_p1_from.Value, dtp_p1_to.Value)

                If isItemExisted = False Then
                    row = New String() {cmb_license_items.Text.Substring(0, 11),
                        cmb_license_items.Text.Substring(14), yearDiffOne, "100", "0", "0.00", "0", "0", "0",
                        "0.00", "0", "0", "0", "0.00", "0.00", cmb_payment_number_license.Text}
                    dgv_license.Rows.Add(row)
                End If
            ElseIf cb_particular_two.Checked And cb_particular_three.Checked = False Then
                dgv_license.Columns(6).ReadOnly = False
                dgv_license.Columns(7).ReadOnly = False
                dgv_license.Columns(8).ReadOnly = False
                dgv_license.Columns(9).ReadOnly = False
                dgv_license.Columns(10).ReadOnly = True
                dgv_license.Columns(11).ReadOnly = True
                dgv_license.Columns(12).ReadOnly = True
                dgv_license.Columns(13).ReadOnly = True
                yearDiffOne = DateDiff(DateInterval.Year, dtp_p1_from.Value, dtp_p1_to.Value)
                yearDiffTwo = DateDiff(DateInterval.Year, dtp_p2_from.Value, dtp_p2_to.Value)

                If isItemExisted = False Then
                    row = New String() {cmb_license_items.Text.Substring(0, 11),
                        cmb_license_items.Text.Substring(14), yearDiffOne, "100", "0", "0.00", yearDiffTwo,
                        "100", "0", "0.00", "0", "0", "0", "0.00", "0.00", cmb_payment_number_license.Text}
                    dgv_license.Rows.Add(row)
                End If
            ElseIf cb_particular_two.Checked And cb_particular_three.Checked Then
                dgv_license.Columns(6).ReadOnly = False
                dgv_license.Columns(7).ReadOnly = False
                dgv_license.Columns(8).ReadOnly = False
                dgv_license.Columns(9).ReadOnly = False
                dgv_license.Columns(10).ReadOnly = False
                dgv_license.Columns(11).ReadOnly = False
                dgv_license.Columns(12).ReadOnly = False
                dgv_license.Columns(13).ReadOnly = False
                yearDiffOne = DateDiff(DateInterval.Year, dtp_p1_from.Value, dtp_p1_to.Value)
                yearDiffTwo = DateDiff(DateInterval.Year, dtp_p2_from.Value, dtp_p2_to.Value)
                yearDiffThree = DateDiff(DateInterval.Year, dtp_p3_from.Value, dtp_p3_to.Value)

                If isItemExisted = False Then
                    row = New String() {cmb_license_items.Text.Substring(0, 11),
                        cmb_license_items.Text.Substring(14), yearDiffOne, "100", "0", "0.00", yearDiffTwo,
                        "100", "0", "0.00", yearDiffThree, "100", "0", "0.00", "0.00",
                        cmb_payment_number_license.Text}
                    dgv_license.Rows.Add(row)
                End If
            End If

            For Each column As DataGridViewColumn In dgv_license.Columns
                column.SortMode = DataGridViewColumnSortMode.NotSortable
            Next

            isPaymentAdded = True
            btn_tab_license.Text = "LICENSE(" + dgv_license.Rows.Count.ToString + ")"
        End If
    End Sub

    Sub initialize_table_license()
        dgv_license.ColumnCount = 16
        dgv_license.Columns(0).Name = "Code"
        dgv_license.Columns(0).ReadOnly = True
        dgv_license.Columns(0).Width = 75
        dgv_license.Columns(1).Name = "Description"
        dgv_license.Columns(1).ReadOnly = True
        dgv_license.Columns(1).Width = 230
        dgv_license.Columns(2).Name = "No. Yrs."
        dgv_license.Columns(2).DefaultCellStyle.BackColor = Color.FromArgb(107, 117, 122)
        dgv_license.Columns(2).ReadOnly = False
        dgv_license.Columns(2).Width = 60
        dgv_license.Columns(3).Name = "%"
        dgv_license.Columns(3).DefaultCellStyle.BackColor = Color.FromArgb(107, 117, 122)
        dgv_license.Columns(3).Width = 60
        dgv_license.Columns(4).Name = "No. Units"
        dgv_license.Columns(4).DefaultCellStyle.BackColor = Color.FromArgb(107, 117, 122)
        dgv_license.Columns(4).Width = 60
        dgv_license.Columns(5).Name = "Fees"
        dgv_license.Columns(5).DefaultCellStyle.BackColor = Color.FromArgb(107, 117, 122)
        dgv_license.Columns(5).Width = 80
        dgv_license.Columns(6).Name = "No. Yrs"
        dgv_license.Columns(6).ReadOnly = True
        dgv_license.Columns(6).Width = 60
        dgv_license.Columns(7).Name = "%"
        dgv_license.Columns(7).Width = 60
        dgv_license.Columns(8).Name = "No. Units"
        dgv_license.Columns(8).Width = 60
        dgv_license.Columns(9).Name = "Fees"
        dgv_license.Columns(9).Width = 80
        dgv_license.Columns(10).Name = "No. Yrs"
        dgv_license.Columns(10).DefaultCellStyle.BackColor = Color.FromArgb(107, 117, 122)
        dgv_license.Columns(10).ReadOnly = True
        dgv_license.Columns(10).Width = 60
        dgv_license.Columns(11).Name = "%"
        dgv_license.Columns(11).DefaultCellStyle.BackColor = Color.FromArgb(107, 117, 122)
        dgv_license.Columns(11).Width = 60
        dgv_license.Columns(12).Name = "No. Units"
        dgv_license.Columns(12).DefaultCellStyle.BackColor = Color.FromArgb(107, 117, 122)
        dgv_license.Columns(12).Width = 60
        dgv_license.Columns(13).Name = "Fees"
        dgv_license.Columns(13).DefaultCellStyle.BackColor = Color.FromArgb(107, 117, 122)
        dgv_license.Columns(13).Width = 80
        dgv_license.Columns(14).Name = "Sub-total"
        dgv_license.Columns(14).Width = 100
        dgv_license.Columns(14).ReadOnly = True
        dgv_license.Columns(15).Name = "Payment ID"
        dgv_license.Columns(15).Visible = False
    End Sub

    Private Sub btn_others_add_Click(sender As Object, e As EventArgs) Handles btn_others_add.Click
        Dim isItemExisted As Boolean = False
        Dim row As String()

        If cmb_others_items.Text <> "" Then
            initialize_table_others()

            If dgv_others.Rows.Count <> 0 Then
                For i As Integer = 0 To dgv_others.Rows.Count - 1 Step +1
                    If dgv_others.Rows(i).Cells(15).Value.ToString() = cmb_payment_number_others.Text Then
                        isItemExisted = True
                    End If
                Next
            End If

            If cb_particular_two.Checked = False And cb_particular_three.Checked = False Then
                dgv_others.Columns(6).ReadOnly = True
                dgv_others.Columns(7).ReadOnly = True
                dgv_others.Columns(8).ReadOnly = True
                dgv_others.Columns(9).ReadOnly = True
                dgv_others.Columns(10).ReadOnly = True
                dgv_others.Columns(11).ReadOnly = True
                dgv_others.Columns(12).ReadOnly = True
                dgv_others.Columns(13).ReadOnly = True
                yearDiffOne = DateDiff(DateInterval.Year, dtp_p1_from.Value, dtp_p1_to.Value)

                If isItemExisted = False Then
                    row = New String() {cmb_others_items.Text.Substring(0, 11),
                        cmb_others_items.Text.Substring(14), yearDiffOne, "100", "0", "0.00", "0", "0",
                        "0", "0.00", "0", "0", "0", "0.00", "0.00", cmb_payment_number_others.Text}
                    dgv_others.Rows.Add(row)
                End If
            ElseIf cb_particular_two.Checked And cb_particular_three.Checked = False Then
                dgv_others.Columns(6).ReadOnly = False
                dgv_others.Columns(7).ReadOnly = False
                dgv_others.Columns(8).ReadOnly = False
                dgv_others.Columns(9).ReadOnly = False
                dgv_others.Columns(10).ReadOnly = True
                dgv_others.Columns(11).ReadOnly = True
                dgv_others.Columns(12).ReadOnly = True
                dgv_others.Columns(13).ReadOnly = True
                yearDiffOne = DateDiff(DateInterval.Year, dtp_p1_from.Value, dtp_p1_to.Value)
                yearDiffTwo = DateDiff(DateInterval.Year, dtp_p2_from.Value, dtp_p2_to.Value)

                If isItemExisted = False Then
                    row = New String() {cmb_others_items.Text.Substring(0, 11),
                        cmb_others_items.Text.Substring(14), yearDiffOne, "100", "0", "0.00", yearDiffTwo,
                        "100", "0", "0.00", "0", "0", "0", "0.00", "0.00", cmb_payment_number_others.Text}
                    dgv_others.Rows.Add(row)
                End If
            Else
                dgv_others.Columns(6).ReadOnly = False
                dgv_others.Columns(7).ReadOnly = False
                dgv_others.Columns(8).ReadOnly = False
                dgv_others.Columns(9).ReadOnly = False
                dgv_others.Columns(10).ReadOnly = False
                dgv_others.Columns(11).ReadOnly = False
                dgv_others.Columns(12).ReadOnly = False
                dgv_others.Columns(13).ReadOnly = False
                yearDiffOne = DateDiff(DateInterval.Year, dtp_p1_from.Value, dtp_p1_to.Value)
                yearDiffTwo = DateDiff(DateInterval.Year, dtp_p2_from.Value, dtp_p2_to.Value)
                yearDiffThree = DateDiff(DateInterval.Year, dtp_p3_from.Value, dtp_p3_to.Value)

                If isItemExisted = False Then
                    row = New String() {cmb_others_items.Text.Substring(0, 11),
                        cmb_others_items.Text.Substring(14), yearDiffOne, "100", "0", "0.00", yearDiffTwo,
                        "100", "0", "0.00", yearDiffThree, "100", "0", "0.00", "0.00",
                        cmb_payment_number_others.Text}
                    dgv_others.Rows.Add(row)
                End If
            End If

            For Each column As DataGridViewColumn In dgv_others.Columns
                column.SortMode = DataGridViewColumnSortMode.NotSortable
            Next

            isPaymentAdded = True
            btn_tab_others.Text = "OTHERS(" + dgv_others.Rows.Count.ToString + ")"
        End If
    End Sub

    Sub initialize_table_others()
        dgv_others.ColumnCount = 16
        dgv_others.Columns(0).Name = "Code"
        dgv_others.Columns(0).ReadOnly = True
        dgv_others.Columns(0).Width = 75
        dgv_others.Columns(1).Name = "Description"
        dgv_others.Columns(1).ReadOnly = True
        dgv_others.Columns(1).Width = 230
        dgv_others.Columns(2).Name = "No. Yrs."
        dgv_others.Columns(2).DefaultCellStyle.BackColor = Color.FromArgb(107, 117, 122)
        dgv_others.Columns(2).ReadOnly = False
        dgv_others.Columns(2).Width = 60
        dgv_others.Columns(3).Name = "%"
        dgv_others.Columns(3).DefaultCellStyle.BackColor = Color.FromArgb(107, 117, 122)
        dgv_others.Columns(3).Width = 60
        dgv_others.Columns(4).Name = "No. Units"
        dgv_others.Columns(4).DefaultCellStyle.BackColor = Color.FromArgb(107, 117, 122)
        dgv_others.Columns(4).Width = 60
        dgv_others.Columns(5).Name = "Fees"
        dgv_others.Columns(5).DefaultCellStyle.BackColor = Color.FromArgb(107, 117, 122)
        dgv_others.Columns(5).Width = 80
        dgv_others.Columns(6).Name = "No. Yrs"
        dgv_others.Columns(6).ReadOnly = True
        dgv_others.Columns(6).Width = 60
        dgv_others.Columns(7).Name = "%"
        dgv_others.Columns(7).Width = 60
        dgv_others.Columns(8).Name = "No. Units"
        dgv_others.Columns(8).Width = 60
        dgv_others.Columns(9).Name = "Fees"
        dgv_others.Columns(9).Width = 80
        dgv_others.Columns(10).Name = "No. Yrs"
        dgv_others.Columns(10).DefaultCellStyle.BackColor = Color.FromArgb(107, 117, 122)
        dgv_others.Columns(10).ReadOnly = True
        dgv_others.Columns(10).Width = 60
        dgv_others.Columns(11).Name = "%"
        dgv_others.Columns(11).DefaultCellStyle.BackColor = Color.FromArgb(107, 117, 122)
        dgv_others.Columns(11).Width = 60
        dgv_others.Columns(12).Name = "No. Units"
        dgv_others.Columns(12).DefaultCellStyle.BackColor = Color.FromArgb(107, 117, 122)
        dgv_others.Columns(12).Width = 60
        dgv_others.Columns(13).Name = "Fees"
        dgv_others.Columns(13).DefaultCellStyle.BackColor = Color.FromArgb(107, 117, 122)
        dgv_others.Columns(13).Width = 80
        dgv_others.Columns(14).Name = "Sub-total"
        dgv_others.Columns(14).Width = 100
        dgv_others.Columns(14).ReadOnly = True
        dgv_others.Columns(15).Name = "Payment ID"
        dgv_others.Columns(15).Visible = False
    End Sub

    Private Sub btn_roc_add_Click(sender As Object, e As EventArgs) Handles btn_roc_add.Click
        Dim isItemExisted As Boolean = False
        Dim row As String()

        If cmb_roc_items.Text <> "" Then
            initalize_table_ROC()

            If dgv_roc.Rows.Count <> 0 Then
                For i As Integer = 0 To dgv_roc.Rows.Count - 1 Step +1
                    If dgv_roc.Rows(i).Cells(15).Value.ToString() = cmb_payment_number_roc.Text Then
                        isItemExisted = True
                    End If
                Next
            End If

            If cb_particular_two.Checked = False And cb_particular_three.Checked = False Then
                dgv_roc.Columns(6).ReadOnly = True
                dgv_roc.Columns(7).ReadOnly = True
                dgv_roc.Columns(8).ReadOnly = True
                dgv_roc.Columns(9).ReadOnly = True
                dgv_roc.Columns(10).ReadOnly = True
                dgv_roc.Columns(11).ReadOnly = True
                dgv_roc.Columns(12).ReadOnly = True
                dgv_roc.Columns(13).ReadOnly = True
                yearDiffOne = DateDiff(DateInterval.Year, dtp_p1_from.Value, dtp_p1_to.Value)

                If isItemExisted = False Then
                    row = New String() {cmb_roc_items.Text.Substring(0, 11),
                        cmb_roc_items.Text.Substring(14), yearDiffOne, "100", "0", "0.00", "0", "0",
                        "0", "0.00", "0", "0", "0", "0.00", "0.00", cmb_payment_number_roc.Text}
                    dgv_roc.Rows.Add(row)
                End If
            ElseIf cb_particular_two.Checked And cb_particular_three.Checked = False Then
                dgv_roc.Columns(6).ReadOnly = False
                dgv_roc.Columns(7).ReadOnly = False
                dgv_roc.Columns(8).ReadOnly = False
                dgv_roc.Columns(9).ReadOnly = False
                dgv_roc.Columns(10).ReadOnly = True
                dgv_roc.Columns(11).ReadOnly = True
                dgv_roc.Columns(12).ReadOnly = True
                dgv_roc.Columns(13).ReadOnly = True
                yearDiffOne = DateDiff(DateInterval.Year, dtp_p1_from.Value, dtp_p1_to.Value)
                yearDiffTwo = DateDiff(DateInterval.Year, dtp_p2_from.Value, dtp_p2_to.Value)

                If isItemExisted = False Then
                    row = New String() {cmb_roc_items.Text.Substring(0, 11),
                        cmb_roc_items.Text.Substring(14), yearDiffOne, "100", "0", "0.00", yearDiffTwo,
                        "100", "0", "0.00", "0", "0", "0", "0.00", "0.00", cmb_payment_number_roc.Text}
                    dgv_roc.Rows.Add(row)
                End If
            Else
                dgv_roc.Columns(6).ReadOnly = False
                dgv_roc.Columns(7).ReadOnly = False
                dgv_roc.Columns(8).ReadOnly = False
                dgv_roc.Columns(9).ReadOnly = False
                dgv_roc.Columns(10).ReadOnly = False
                dgv_roc.Columns(11).ReadOnly = False
                dgv_roc.Columns(12).ReadOnly = False
                dgv_roc.Columns(13).ReadOnly = False

                yearDiffOne = DateDiff(DateInterval.Year, dtp_p1_from.Value, dtp_p1_to.Value)
                yearDiffTwo = DateDiff(DateInterval.Year, dtp_p2_from.Value, dtp_p2_to.Value)
                yearDiffThree = DateDiff(DateInterval.Year, dtp_p3_from.Value, dtp_p3_to.Value)
                If isItemExisted = False Then
                    row = New String() {cmb_roc_items.Text.Substring(0, 11),
                        cmb_roc_items.Text.Substring(14), yearDiffOne, "100", "0", "0.00", yearDiffTwo,
                        "100", "0", "0.00", yearDiffThree, "100", "0", "0.00", "0.00",
                        cmb_payment_number_roc.Text}
                    dgv_roc.Rows.Add(row)
                End If
            End If

            For Each column As DataGridViewColumn In dgv_roc.Columns
                column.SortMode = DataGridViewColumnSortMode.NotSortable
            Next

            isPaymentAdded = True
            btn_tab_roc.Text = "AROC/ROC(" + dgv_roc.Rows.Count.ToString + ")"
        End If
    End Sub

    Sub initalize_table_ROC()
        dgv_roc.ColumnCount = 16
        dgv_roc.Columns(0).Name = "Code"
        dgv_roc.Columns(0).ReadOnly = True
        dgv_roc.Columns(0).Width = 75
        dgv_roc.Columns(1).Name = "Description"
        dgv_roc.Columns(1).ReadOnly = True
        dgv_roc.Columns(1).Width = 230
        dgv_roc.Columns(2).Name = "No. Yrs."
        dgv_roc.Columns(2).DefaultCellStyle.BackColor = Color.FromArgb(107, 117, 122)
        dgv_roc.Columns(2).ReadOnly = False
        dgv_roc.Columns(2).Width = 60
        dgv_roc.Columns(3).Name = "%"
        dgv_roc.Columns(3).DefaultCellStyle.BackColor = Color.FromArgb(107, 117, 122)
        dgv_roc.Columns(3).Width = 60
        dgv_roc.Columns(4).Name = "No. Units"
        dgv_roc.Columns(4).DefaultCellStyle.BackColor = Color.FromArgb(107, 117, 122)
        dgv_roc.Columns(4).Width = 60
        dgv_roc.Columns(5).Name = "Fees"
        dgv_roc.Columns(5).DefaultCellStyle.BackColor = Color.FromArgb(107, 117, 122)
        dgv_roc.Columns(5).Width = 80
        dgv_roc.Columns(6).Name = "No. Yrs"
        dgv_roc.Columns(6).ReadOnly = True
        dgv_roc.Columns(6).Width = 60
        dgv_roc.Columns(7).Name = "%"
        dgv_roc.Columns(7).Width = 60
        dgv_roc.Columns(8).Name = "No. Units"
        dgv_roc.Columns(8).Width = 60
        dgv_roc.Columns(9).Name = "Fees"
        dgv_roc.Columns(9).Width = 80
        dgv_roc.Columns(10).Name = "No. Yrs"
        dgv_roc.Columns(10).DefaultCellStyle.BackColor = Color.FromArgb(107, 117, 122)
        dgv_roc.Columns(10).ReadOnly = True
        dgv_roc.Columns(10).Width = 60
        dgv_roc.Columns(11).Name = "%"
        dgv_roc.Columns(11).DefaultCellStyle.BackColor = Color.FromArgb(107, 117, 122)
        dgv_roc.Columns(11).Width = 60
        dgv_roc.Columns(12).Name = "No. Units"
        dgv_roc.Columns(12).DefaultCellStyle.BackColor = Color.FromArgb(107, 117, 122)
        dgv_roc.Columns(12).Width = 60
        dgv_roc.Columns(13).Name = "Fees"
        dgv_roc.Columns(13).DefaultCellStyle.BackColor = Color.FromArgb(107, 117, 122)
        dgv_roc.Columns(13).Width = 80
        dgv_roc.Columns(14).Name = "Sub-total"
        dgv_roc.Columns(14).Width = 100
        dgv_roc.Columns(14).ReadOnly = True
        dgv_roc.Columns(15).Name = "Payment ID"
        dgv_roc.Columns(15).Visible = False
    End Sub

    Private Sub btn_permits_add_Click(sender As Object, e As EventArgs) Handles btn_permits_add.Click
        Dim isItemExisted As Boolean = False
        Dim row As String()

        If cmb_permits_items.Text <> "" Then
            initialize_table_permits()

            If dgv_permits.Rows.Count <> 0 Then
                For i As Integer = 0 To dgv_permits.Rows.Count - 1 Step +1
                    If dgv_permits.Rows(i).Cells(15).Value.ToString() =
                        cmb_payment_number_permits.Text Then
                        isItemExisted = True
                    End If
                Next
            End If

            If cb_particular_two.Checked = False And cb_particular_three.Checked = False Then
                dgv_permits.Columns(6).ReadOnly = True
                dgv_permits.Columns(7).ReadOnly = True
                dgv_permits.Columns(8).ReadOnly = True
                dgv_permits.Columns(9).ReadOnly = True
                dgv_permits.Columns(10).ReadOnly = True
                dgv_permits.Columns(11).ReadOnly = True
                dgv_permits.Columns(12).ReadOnly = True
                dgv_permits.Columns(13).ReadOnly = True

                yearDiffOne = DateDiff(DateInterval.Year, dtp_p1_from.Value, dtp_p1_to.Value)
                If isItemExisted = False Then
                    row = New String() {cmb_permits_items.Text.Substring(0, 11),
                        cmb_permits_items.Text.Substring(14), yearDiffOne, "100", "0", "0.00", "0",
                        "0", "0", "0.00", "0", "0", "0", "0.00", "0.00", cmb_payment_number_permits.Text}
                    dgv_permits.Rows.Add(row)
                End If
            ElseIf cb_particular_two.Checked And cb_particular_three.Checked = False Then
                dgv_permits.Columns(6).ReadOnly = False
                dgv_permits.Columns(7).ReadOnly = False
                dgv_permits.Columns(8).ReadOnly = False
                dgv_permits.Columns(9).ReadOnly = False
                dgv_permits.Columns(10).ReadOnly = True
                dgv_permits.Columns(11).ReadOnly = True
                dgv_permits.Columns(12).ReadOnly = True
                dgv_permits.Columns(13).ReadOnly = True
                yearDiffOne = DateDiff(DateInterval.Year, dtp_p1_from.Value, dtp_p1_to.Value)
                yearDiffTwo = DateDiff(DateInterval.Year, dtp_p2_from.Value, dtp_p2_to.Value)

                If isItemExisted = False Then
                    row = New String() {cmb_permits_items.Text.Substring(0, 11),
                        cmb_permits_items.Text.Substring(14), yearDiffOne, "100", "0", "0.00", yearDiffTwo,
                        "100", "0", "0.00", "0", "0", "0", "0.00", "0.00", cmb_payment_number_permits.Text}
                    dgv_permits.Rows.Add(row)
                End If
            Else
                dgv_permits.Columns(6).ReadOnly = False
                dgv_permits.Columns(7).ReadOnly = False
                dgv_permits.Columns(8).ReadOnly = False
                dgv_permits.Columns(9).ReadOnly = False
                dgv_permits.Columns(10).ReadOnly = False
                dgv_permits.Columns(11).ReadOnly = False
                dgv_permits.Columns(12).ReadOnly = False
                dgv_permits.Columns(13).ReadOnly = False

                yearDiffOne = DateDiff(DateInterval.Year, dtp_p1_from.Value, dtp_p1_to.Value)
                yearDiffTwo = DateDiff(DateInterval.Year, dtp_p2_from.Value, dtp_p2_to.Value)
                yearDiffThree = DateDiff(DateInterval.Year, dtp_p3_from.Value, dtp_p3_to.Value)
                If isItemExisted = False Then
                    row = New String() {cmb_permits_items.Text.Substring(0, 11),
                        cmb_permits_items.Text.Substring(14), yearDiffOne, "100", "0", "0.00", yearDiffTwo,
                        "100", "0", "0.00", yearDiffThree, "100", "0", "0.00", "0.00",
                        cmb_payment_number_permits.Text}
                    dgv_permits.Rows.Add(row)
                End If
            End If

            For Each column As DataGridViewColumn In dgv_permits.Columns
                column.SortMode = DataGridViewColumnSortMode.NotSortable
            Next

            isPaymentAdded = True
            btn_tab_permits.Text = "PERMITS(" + dgv_permits.Rows.Count.ToString + ")"
        End If
    End Sub

    Sub initialize_table_permits()
        dgv_permits.ColumnCount = 16
        dgv_permits.Columns(0).Name = "Code"
        dgv_permits.Columns(0).ReadOnly = True
        dgv_permits.Columns(0).Width = 75
        dgv_permits.Columns(1).Name = "Description"
        dgv_permits.Columns(1).ReadOnly = True
        dgv_permits.Columns(1).Width = 230
        dgv_permits.Columns(2).Name = "No. Yrs."
        dgv_permits.Columns(2).DefaultCellStyle.BackColor = Color.FromArgb(107, 117, 122)
        dgv_permits.Columns(2).ReadOnly = False
        dgv_permits.Columns(2).Width = 60
        dgv_permits.Columns(3).Name = "%"
        dgv_permits.Columns(3).DefaultCellStyle.BackColor = Color.FromArgb(107, 117, 122)
        dgv_permits.Columns(3).Width = 60
        dgv_permits.Columns(4).Name = "No. Units"
        dgv_permits.Columns(4).DefaultCellStyle.BackColor = Color.FromArgb(107, 117, 122)
        dgv_permits.Columns(4).Width = 60
        dgv_permits.Columns(5).Name = "Fees"
        dgv_permits.Columns(5).DefaultCellStyle.BackColor = Color.FromArgb(107, 117, 122)
        dgv_permits.Columns(5).Width = 80
        dgv_permits.Columns(6).Name = "No. Yrs"
        dgv_permits.Columns(6).ReadOnly = True
        dgv_permits.Columns(6).Width = 60
        dgv_permits.Columns(7).Name = "%"
        dgv_permits.Columns(7).Width = 60
        dgv_permits.Columns(8).Name = "No. Units"
        dgv_permits.Columns(8).Width = 60
        dgv_permits.Columns(9).Name = "Fees"
        dgv_permits.Columns(9).Width = 80
        dgv_permits.Columns(10).Name = "No. Yrs"
        dgv_permits.Columns(10).DefaultCellStyle.BackColor = Color.FromArgb(107, 117, 122)
        dgv_permits.Columns(10).ReadOnly = True
        dgv_permits.Columns(10).Width = 60
        dgv_permits.Columns(11).Name = "%"
        dgv_permits.Columns(11).DefaultCellStyle.BackColor = Color.FromArgb(107, 117, 122)
        dgv_permits.Columns(11).Width = 60
        dgv_permits.Columns(12).Name = "No. Units"
        dgv_permits.Columns(12).DefaultCellStyle.BackColor = Color.FromArgb(107, 117, 122)
        dgv_permits.Columns(12).Width = 60
        dgv_permits.Columns(13).Name = "Fees"
        dgv_permits.Columns(13).DefaultCellStyle.BackColor = Color.FromArgb(107, 117, 122)
        dgv_permits.Columns(13).Width = 80
        dgv_permits.Columns(14).Name = "Sub-total"
        dgv_permits.Columns(14).Width = 100
        dgv_permits.Columns(14).ReadOnly = True
        dgv_permits.Columns(15).Name = "Payment ID"
        dgv_permits.Columns(15).Visible = False
    End Sub

    Sub compute_total()
        Dim computeCounter As Integer
        Dim total As Double = 0

        For computeCounter = 0 To dgv_license.Rows.Count - 1 Step +1
            Try
                total += dgv_license.Rows(computeCounter).Cells("Sub-total").Value
            Catch ex As Exception
                total += 0
            End Try
        Next

        For computeCounter = 0 To dgv_permits.Rows.Count - 1 Step +1
            Try
                total += dgv_permits.Rows(computeCounter).Cells("Sub-total").Value
            Catch ex As Exception
                total += 0
            End Try
        Next

        For computeCounter = 0 To dgv_roc.Rows.Count - 1 Step +1
            Try
                total += dgv_roc.Rows(computeCounter).Cells("Sub-total").Value
            Catch ex As Exception
                total += 0
            End Try
        Next

        For computeCounter = 0 To dgv_others.Rows.Count - 1 Step +1
            Try
                total += dgv_others.Rows(computeCounter).Cells("Sub-total").Value
            Catch ex As Exception
                total += 0
            End Try
        Next

        tb_total.Text = FormatNumber(CDbl(total), 2)
    End Sub

    Private Sub dgv_license_CellValueChanged(sender As Object, e As DataGridViewCellEventArgs) Handles _
        dgv_license.CellValueChanged
        compute_license_subtotal()
    End Sub

    Sub compute_license_subtotal()
        Dim computeLicenseSubCounter As Integer

        For computeLicenseSubCounter = 0 To dgv_license.Rows.Count - 1 Step +1
            Dim value As String = dgv_license.Rows(computeLicenseSubCounter).Cells(3).Value
            Dim value2 As String = dgv_license.Rows(computeLicenseSubCounter).Cells(7).Value
            Dim value3 As String = dgv_license.Rows(computeLicenseSubCounter).Cells(11).Value
            Dim num, num2, num3 As Double
            Dim percentage, percentage2, percentage3 As Double
            Dim column_counter As Integer = 2
            Dim column_value

            If Double.TryParse(value, num) And Double.TryParse(value2, num2) And Double.TryParse(value3, num3) Then
                percentage = num / 100
                percentage2 = num2 / 100
                percentage3 = num3 / 100
            End If

            Try
                While column_counter <= 14
                    If dgv_license.Rows(computeLicenseSubCounter).Cells(column_counter).Value = Nothing Then
                        Select Case column_counter
                            Case 5, 9, 13
                                dgv_license.Rows(computeLicenseSubCounter).Cells(column_counter).Value = "0.00"
                            Case Else
                                dgv_license.Rows(computeLicenseSubCounter).Cells(column_counter).Value = "0"
                        End Select
                    Else
                        Select Case column_counter
                            Case 5, 9, 13
                                column_value = dgv_license.Rows(computeLicenseSubCounter).Cells(column_counter).Value

                                dgv_license.Rows(computeLicenseSubCounter).Cells(column_counter).Value =
                            FormatNumber(CDbl(column_value), 2)
                        End Select
                    End If

                    column_counter += 1
                End While

                dgv_license.Rows(computeLicenseSubCounter).Cells("Sub-total").Value =
                    FormatNumber(CDbl(Double.Parse(dgv_license.Rows(computeLicenseSubCounter).Cells(2).Value) *
                    Double.Parse(percentage) *
                    Double.Parse(dgv_license.Rows(computeLicenseSubCounter).Cells(4).Value) *
                    Double.Parse(dgv_license.Rows(computeLicenseSubCounter).Cells(5).Value)) +
                    (Double.Parse(dgv_license.Rows(computeLicenseSubCounter).Cells(6).Value) *
                    Double.Parse(percentage2) *
                    Double.Parse(dgv_license.Rows(computeLicenseSubCounter).Cells(8).Value) *
                    Double.Parse(dgv_license.Rows(computeLicenseSubCounter).Cells(9).Value)) +
                    (Double.Parse(dgv_license.Rows(computeLicenseSubCounter).Cells(10).Value) *
                    Double.Parse(percentage3) *
                    Double.Parse(dgv_license.Rows(computeLicenseSubCounter).Cells(12).Value) *
                    Double.Parse(dgv_license.Rows(computeLicenseSubCounter).Cells(13).Value)), 2)
            Catch ex As Exception
                dgv_license.Rows(computeLicenseSubCounter).Cells("Sub-total").Value = 0
            End Try
        Next
        compute_total()
    End Sub

    Private Sub dgv_permits_CellValueChanged(sender As Object, e As DataGridViewCellEventArgs) Handles _
        dgv_permits.CellValueChanged
        compute_permits_subtotal()
    End Sub

    Sub compute_permits_subtotal()
        Dim computePermitsSubCounter As Integer

        For computePermitsSubCounter = 0 To dgv_permits.Rows.Count - 1 Step +1
            Dim value As String = dgv_permits.Rows(computePermitsSubCounter).Cells(3).Value
            Dim value2 As String = dgv_permits.Rows(computePermitsSubCounter).Cells(7).Value
            Dim value3 As String = dgv_permits.Rows(computePermitsSubCounter).Cells(11).Value
            Dim num, num2, num3 As Double
            Dim percentage, percentage2, percentage3 As Double
            Dim column_counter As Integer = 2
            Dim column_value

            If Double.TryParse(value, num) And Double.TryParse(value2, num2) And
                Double.TryParse(value3, num3) Then
                percentage = num / 100
                percentage2 = num2 / 100
                percentage3 = num3 / 100
            End If

            Try
                While column_counter <= 14
                    If dgv_permits.Rows(computePermitsSubCounter).Cells(column_counter).Value = Nothing Then
                        Select Case column_counter
                            Case 5, 9, 13
                                dgv_permits.Rows(computePermitsSubCounter).Cells(column_counter).Value = "0.00"
                            Case Else
                                dgv_permits.Rows(computePermitsSubCounter).Cells(column_counter).Value = "0"
                        End Select
                    Else
                        column_value = dgv_permits.Rows(computePermitsSubCounter).Cells(column_counter).Value

                        Select Case column_counter
                            Case 5, 9, 13
                                dgv_permits.Rows(computePermitsSubCounter).Cells(column_counter).Value =
                            FormatNumber(CDbl(column_value), 2)
                        End Select
                    End If

                    column_counter += 1
                End While

                dgv_permits.Rows(computePermitsSubCounter).Cells("Sub-total").Value =
                    FormatNumber(CDbl(Double.Parse(dgv_permits.Rows(computePermitsSubCounter).Cells(2).Value) *
                    Double.Parse(percentage) * Double.Parse(dgv_permits.Rows(computePermitsSubCounter).Cells(4).Value) *
                    Double.Parse(dgv_permits.Rows(computePermitsSubCounter).Cells(5).Value)) +
                    (Double.Parse(dgv_permits.Rows(computePermitsSubCounter).Cells(6).Value) * Double.Parse(percentage2) *
                    Double.Parse(dgv_permits.Rows(computePermitsSubCounter).Cells(8).Value) *
                    Double.Parse(dgv_permits.Rows(computePermitsSubCounter).Cells(9).Value)) +
                    (Double.Parse(dgv_permits.Rows(computePermitsSubCounter).Cells(10).Value) * Double.Parse(percentage3) *
                    Double.Parse(dgv_permits.Rows(computePermitsSubCounter).Cells(12).Value) *
                    Double.Parse(dgv_permits.Rows(computePermitsSubCounter).Cells(13).Value)), 2)
            Catch ex As Exception
                dgv_permits.Rows(computePermitsSubCounter).Cells("Sub-total").Value = 0
            End Try
        Next
        compute_total()
    End Sub

    Private Sub dgv_others_CellValueChanged(sender As Object, e As DataGridViewCellEventArgs) Handles _
        dgv_others.CellValueChanged
        compute_others_subtotal()
    End Sub

    Sub compute_others_subtotal()
        Dim computeOthersSubCounter As Integer

        For computeOthersSubCounter = 0 To dgv_others.Rows.Count - 1 Step +1
            Dim value As String = dgv_others.Rows(computeOthersSubCounter).Cells(3).Value
            Dim value2 As String = dgv_others.Rows(computeOthersSubCounter).Cells(7).Value
            Dim value3 As String = dgv_others.Rows(computeOthersSubCounter).Cells(11).Value
            Dim num, num2, num3 As Double
            Dim percentage, percentage2, percentage3 As Double
            Dim column_counter As Integer = 2
            Dim column_value

            If Double.TryParse(value, num) And Double.TryParse(value2, num2) And Double.TryParse(value3, num3) Then
                percentage = num / 100
                percentage2 = num2 / 100
                percentage3 = num3 / 100
            End If

            Try
                While column_counter <= 14
                    If dgv_others.Rows(computeOthersSubCounter).Cells(column_counter).Value = Nothing Then
                        Select Case column_counter
                            Case 5, 9, 13
                                dgv_others.Rows(computeOthersSubCounter).Cells(column_counter).Value = "0.00"
                            Case Else
                                dgv_others.Rows(computeOthersSubCounter).Cells(column_counter).Value = "0"
                        End Select
                    Else
                        column_value = dgv_others.Rows(computeOthersSubCounter).Cells(column_counter).Value

                        Select Case column_counter
                            Case 5, 9, 13
                                dgv_others.Rows(computeOthersSubCounter).Cells(column_counter).Value =
                            FormatNumber(CDbl(column_value), 2)
                        End Select
                    End If

                    column_counter += 1
                End While

                dgv_others.Rows(computeOthersSubCounter).Cells("Sub-total").Value =
                    FormatNumber(CDbl(Double.Parse(dgv_others.Rows(computeOthersSubCounter).Cells(2).Value) *
                    Double.Parse(percentage) * Double.Parse(dgv_others.Rows(computeOthersSubCounter).Cells(4).Value) *
                    Double.Parse(dgv_others.Rows(computeOthersSubCounter).Cells(5).Value)) +
                    (Double.Parse(dgv_others.Rows(computeOthersSubCounter).Cells(6).Value) * Double.Parse(percentage2) *
                    Double.Parse(dgv_others.Rows(computeOthersSubCounter).Cells(8).Value) *
                    Double.Parse(dgv_others.Rows(computeOthersSubCounter).Cells(9).Value)) +
                    (Double.Parse(dgv_others.Rows(computeOthersSubCounter).Cells(10).Value) * Double.Parse(percentage3) *
                    Double.Parse(dgv_others.Rows(computeOthersSubCounter).Cells(12).Value) *
                    Double.Parse(dgv_others.Rows(computeOthersSubCounter).Cells(13).Value)), 2)
            Catch ex As Exception
                dgv_others.Rows(computeOthersSubCounter).Cells("Sub-total").Value = 0
            End Try
        Next
        compute_total()
    End Sub

    Private Sub dgv_roc_CellValueChanged(sender As Object, e As DataGridViewCellEventArgs) Handles _
        dgv_roc.CellValueChanged
        compute_roc_subtotal()
    End Sub

    Sub compute_roc_subtotal()
        Dim computeROCSubCounter As Integer

        For computeROCSubCounter = 0 To dgv_roc.Rows.Count - 1 Step +1
            Dim value As String = dgv_roc.Rows(computeROCSubCounter).Cells(3).Value
            Dim value2 As String = dgv_roc.Rows(computeROCSubCounter).Cells(7).Value
            Dim value3 As String = dgv_roc.Rows(computeROCSubCounter).Cells(11).Value
            Dim num, num2, num3 As Double
            Dim percentage, percentage2, percentage3 As Double
            Dim column_counter As Integer = 2
            Dim column_value

            If Double.TryParse(value, num) And Double.TryParse(value2, num2) And
                Double.TryParse(value3, num3) Then
                percentage = num / 100
                percentage2 = num2 / 100
                percentage3 = num3 / 100
            End If

            Try
                While column_counter <= 14
                    If dgv_roc.Rows(computeROCSubCounter).Cells(column_counter).Value = Nothing Then
                        Select Case column_counter
                            Case 5, 9, 13
                                dgv_roc.Rows(computeROCSubCounter).Cells(column_counter).Value = "0.00"
                            Case Else
                                dgv_roc.Rows(computeROCSubCounter).Cells(column_counter).Value = "0"
                        End Select
                    Else
                        column_value = dgv_roc.Rows(computeROCSubCounter).Cells(column_counter).Value

                        Select Case column_counter
                            Case 5, 9, 13
                                dgv_roc.Rows(computeROCSubCounter).Cells(column_counter).Value =
                            FormatNumber(CDbl(column_value), 2)
                        End Select
                    End If

                    column_counter += 1
                End While

                dgv_roc.Rows(computeROCSubCounter).Cells("Sub-total").Value =
                    FormatNumber(CDbl(Double.Parse(dgv_roc.Rows(computeROCSubCounter).Cells(2).Value) *
                    Double.Parse(percentage) *
                    Double.Parse(dgv_roc.Rows(computeROCSubCounter).Cells(4).Value) *
                    Double.Parse(dgv_roc.Rows(computeROCSubCounter).Cells(5).Value)) +
                    (Double.Parse(dgv_roc.Rows(computeROCSubCounter).Cells(6).Value) *
                    Double.Parse(percentage2) *
                    Double.Parse(dgv_roc.Rows(computeROCSubCounter).Cells(8).Value) *
                    Double.Parse(dgv_roc.Rows(computeROCSubCounter).Cells(9).Value)) +
                    (Double.Parse(dgv_roc.Rows(computeROCSubCounter).Cells(10).Value) *
                    Double.Parse(percentage3) *
                    Double.Parse(dgv_roc.Rows(computeROCSubCounter).Cells(12).Value) *
                    Double.Parse(dgv_roc.Rows(computeROCSubCounter).Cells(13).Value)), 2)
            Catch ex As Exception
                dgv_roc.Rows(computeROCSubCounter).Cells("Sub-total").Value = 0
            End Try
        Next
        compute_total()
    End Sub

    Sub get_year_difference_part_one()
        yearDiffOne = DateDiff(DateInterval.Year, dtp_p1_from.Value, dtp_p1_to.Value)

        For i As Integer = 0 To dgv_license.RowCount - 1
            dgv_license.Rows(i).Cells(2).Value = yearDiffOne
        Next

        For i As Integer = 0 To dgv_permits.RowCount - 1
            dgv_permits.Rows(i).Cells(2).Value = yearDiffOne
        Next

        For i As Integer = 0 To dgv_roc.RowCount - 1
            dgv_roc.Rows(i).Cells(2).Value = yearDiffOne
        Next

        For i As Integer = 0 To dgv_others.RowCount - 1
            dgv_others.Rows(i).Cells(2).Value = yearDiffOne
        Next
    End Sub

    Sub getYearDifferencePartTwo()
        yearDiffTwo = DateDiff(DateInterval.Year, dtp_p2_from.Value, dtp_p2_to.Value)

        For i As Integer = 0 To dgv_license.RowCount - 1
            dgv_license.Rows(i).Cells(6).Value = yearDiffTwo
        Next

        For i As Integer = 0 To dgv_permits.RowCount - 1
            dgv_permits.Rows(i).Cells(6).Value = yearDiffTwo
        Next

        For i As Integer = 0 To dgv_roc.RowCount - 1
            dgv_roc.Rows(i).Cells(6).Value = yearDiffTwo
        Next

        For i As Integer = 0 To dgv_others.RowCount - 1
            dgv_others.Rows(i).Cells(6).Value = yearDiffTwo
        Next
    End Sub

    Sub getYearDifferencePartThree()
        yearDiffTwo = DateDiff(DateInterval.Year, dtp_p3_from.Value, dtp_p3_to.Value)

        For i As Integer = 0 To dgv_license.RowCount - 1
            dgv_license.Rows(i).Cells(10).Value = yearDiffTwo
        Next

        For i As Integer = 0 To dgv_permits.RowCount - 1
            dgv_permits.Rows(i).Cells(10).Value = yearDiffTwo
        Next

        For i As Integer = 0 To dgv_roc.RowCount - 1
            dgv_roc.Rows(i).Cells(10).Value = yearDiffTwo
        Next

        For i As Integer = 0 To dgv_others.RowCount - 1
            dgv_others.Rows(i).Cells(10).Value = yearDiffTwo
        Next
    End Sub

    Private Sub dtp_p1_from_ValueChanged(sender As Object, e As EventArgs) Handles dtp_p1_from.ValueChanged
        get_year_difference_part_one()
        update_expiration_date()
    End Sub

    Private Sub dtp_p1_to_ValueChanged(sender As Object, e As EventArgs) Handles dtp_p1_to.ValueChanged
        get_year_difference_part_one()
    End Sub

    Private Sub dtp_p2_from_ValueChanged(sender As Object, e As EventArgs) Handles dtp_p2_from.ValueChanged
        getYearDifferencePartTwo()
        update_expiration_date()
    End Sub

    Private Sub dtp_p2_to_ValueChanged(sender As Object, e As EventArgs) Handles dtp_p2_to.ValueChanged
        getYearDifferencePartTwo()
        update_expiration_date()
    End Sub

    Private Sub dtp_p3_from_ValueChanged(sender As Object, e As EventArgs) Handles dtp_p3_from.ValueChanged
        getYearDifferencePartThree()
        update_expiration_date()
    End Sub

    Private Sub dtp_p3_to_ValueChanged(sender As Object, e As EventArgs) Handles dtp_p3_to.ValueChanged
        getYearDifferencePartThree()
        update_expiration_date()
    End Sub

    Private Sub tb_particular_one_TextChanged(sender As Object, e As EventArgs) Handles _
        tb_particular_one.TextChanged
        If tb_particular_one.Text <> "" Then
            isSetParticularName = True
            dtp_p1_from.Enabled = True
            dtp_p1_to.Enabled = True
        Else
            isSetParticularName = False
            dtp_p1_from.Enabled = False
            dtp_p1_to.Enabled = False
        End If
        update_expiration_date()
    End Sub

    Private Sub tb_particular_two_TextChanged(sender As Object, e As EventArgs) Handles _
        tb_particular_two.TextChanged
        update_expiration_date()

        If tb_particular_two.Text <> "" Then
            dtp_p2_from.Enabled = True
            dtp_p2_to.Enabled = True
        Else
            dtp_p2_from.Enabled = False
            dtp_p2_to.Enabled = False
        End If
    End Sub

    Private Sub tb_particular_three_TextChanged(sender As Object, e As EventArgs) Handles _
        tb_particular_three.TextChanged
        update_expiration_date()
        If tb_particular_three.Text <> "" Then
            dtp_p3_from.Enabled = True
            dtp_p3_to.Enabled = True
        Else
            dtp_p3_from.Enabled = False
            dtp_p3_to.Enabled = False
        End If
    End Sub

    Private Sub tsmi_cms_license_remove_entry_Click(sender As Object, e As EventArgs) Handles _
        tsmi_cms_license_remove_entry.Click
        For Each row As DataGridViewRow In dgv_license.SelectedRows
            dgv_license.Rows.Remove(row)
        Next

        If dgv_license.Rows.Count <> 0 Then
            btn_tab_license.Text = "LICENSE(" + dgv_license.Rows.Count.ToString + ")"
        Else
            btn_tab_license.Text = "LICENSE"
        End If
        compute_license_subtotal()
    End Sub

    Private Sub tsmi_cms_permits_remove_entry_Click(sender As Object, e As EventArgs) Handles _
        tsmi_cms_permits_remove_entry.Click
        For Each row As DataGridViewRow In dgv_permits.SelectedRows
            dgv_permits.Rows.Remove(row)
        Next

        If dgv_permits.Rows.Count <> 0 Then
            btn_tab_permits.Text = "PERMITS(" + dgv_permits.Rows.Count.ToString + ")"
        Else
            btn_tab_permits.Text = "PERMITS"
        End If

        compute_permits_subtotal()
    End Sub

    Private Sub tsmi_cms_roc_remove_entry_Click(sender As Object, e As EventArgs) Handles _
        tsmi_cms_roc_remove_entry.Click
        For Each row As DataGridViewRow In dgv_roc.SelectedRows
            dgv_roc.Rows.Remove(row)
        Next

        If dgv_roc.Rows.Count <> 0 Then
            btn_tab_roc.Text = "AROC/ROC(" + dgv_roc.Rows.Count.ToString + ")"
        Else
            btn_tab_roc.Text = "AROC/ROC"
        End If

        compute_roc_subtotal()
    End Sub

    Private Sub tsmi_cms_others_remove_entry_Click(sender As Object, e As EventArgs) Handles _
        tsmi_cms_others_remove_entry.Click
        For Each row As DataGridViewRow In dgv_others.SelectedRows
            dgv_others.Rows.Remove(row)
        Next

        If dgv_others.Rows.Count <> 0 Then
            btn_tab_others.Text = "OTHERS(" + dgv_others.Rows.Count.ToString + ")"
        Else
            btn_tab_others.Text = "OTHERS"
        End If

        compute_others_subtotal()
    End Sub

    Private Sub btn_save_Click(sender As Object, e As EventArgs) Handles btn_save.Click
        Dim success = False

        If dgv_license.Rows.Count <> 0 Or dgv_permits.Rows.Count Or dgv_roc.Rows.Count Or
            dgv_others.Rows.Count Then
            isSetParticularMOD = True
        Else
            isSetParticularMOD = False
        End If

        If isSetPayor And isSetType And isSetProcess And isSetService Then
            ' Input validation for 1.
            If isSetParticularName And isSetParticularMOD And Not cb_particular_two.Checked _
                And Not cb_particular_three.Checked Then
                save_soa()
                success = True
            End If

            ' Input validation for particulars 1 and 2.
            If isSetParticularName And isSetParticularMOD And (cb_particular_two.Checked And
                    tb_particular_two.Text <> "") And Not cb_particular_three.Checked Then
                save_soa()
                success = True
            End If

            ' Input validation for all particulars.
            If isSetParticularName And isSetParticularMOD And (cb_particular_two.Checked And
                    tb_particular_two.Text <> "") And (cb_particular_three.Checked And
                    tb_particular_three.Text <> "") Then
                save_soa()
                success = True
            End If

            If Not success Then
                MsgBox("You didn't set a name on the first, second or third particular or " &
                           "insert a payment entry.", MsgBoxStyle.Exclamation, "NTC Region 10")
            End If
        Else
            MsgBox("You didn't set payor, transaction, process, or service!",
                MsgBoxStyle.Exclamation, "NTC Region 10")
        End If
    End Sub

    Private Sub cmb_type_SelectionChangeCommitted(sender As Object, e As EventArgs) Handles _
        cmb_type.SelectionChangeCommitted
        If cmb_type.Text <> "" Then
            isSetType = True
        Else
            isSetType = False
        End If
    End Sub

    Private Sub cb_process_mod_CheckedChanged(sender As Object, e As EventArgs) Handles _
        cb_process_mod.CheckedChanged
        process_set()
    End Sub

    Private Sub cb_process_dup_CheckedChanged(sender As Object, e As EventArgs) Handles _
        cb_process_dup.CheckedChanged
        process_set()
    End Sub

    Private Sub cb_process_others_CheckedChanged(sender As Object, e As EventArgs) Handles _
        cb_process_others.CheckedChanged
        process_set()
    End Sub

    Sub process_set()
        ' Check if process type is set.
        If cb_process_dup.Checked Or cb_process_mod.Checked Or cb_process_others.Checked Then
            isSetProcess = True
        Else
            isSetProcess = False
        End If
    End Sub

    Private Sub cb_services_co_CheckedChanged(sender As Object, e As EventArgs) Handles _
        cb_services_co.CheckedChanged
        service_set()
    End Sub

    Private Sub cb_services_cv_CheckedChanged(sender As Object, e As EventArgs) Handles _
        cb_services_cv.CheckedChanged
        service_set()
    End Sub

    Private Sub cb_services_ma_CheckedChanged(sender As Object, e As EventArgs) Handles _
        cb_services_ma.CheckedChanged
        service_set()
    End Sub

    Private Sub cb_services_ms_CheckedChanged(sender As Object, e As EventArgs) Handles _
        cb_services_ms.CheckedChanged
        service_set()
    End Sub

    Private Sub cb_services_roc_CheckedChanged(sender As Object, e As EventArgs) Handles _
        cb_services_roc.CheckedChanged
        service_set()
    End Sub

    Private Sub cb_services_others_CheckedChanged(sender As Object, e As EventArgs) Handles _
        cb_services_others.CheckedChanged
        ' Show input for other if ticked. Hide other input otherwise.
        If cb_services_others.Checked = True Then
            lbl_others.Visible = True
            tb_others.Visible = True
        Else
            lbl_others.Visible = False
            tb_others.Visible = False
            tb_others.Text = ""
        End If

        service_set()
    End Sub

    Private Sub tb_others_TextChanged(sender As Object, e As EventArgs) Handles tb_others.TextChanged
        service_set()
    End Sub

    Sub service_set()
        isSetService = False

        ' Check if service type is ticked.
        If cb_services_co.Checked Or cb_services_cv.Checked Or cb_services_ms.Checked Or
            cb_services_ma.Checked Or cb_services_roc.Checked Or (cb_services_others.Checked _
            And tb_others.Text <> "") Then
            isSetService = True
        End If
    End Sub

    Private Sub number_validation(ByVal sender As Object, ByVal e As KeyPressEventArgs)
        ' Accepts numbers, point and backspace only.
        Select Case e.KeyChar
            Case "0", "1", "2", "3", "4", "5", "6", "7", "8", "9", ".", vbBack
                e.Handled = False
            Case Else
                e.Handled = True
        End Select
    End Sub

    Private Sub dgv_license_EditingControlShowing(sender As Object, e As DataGridViewEditingControlShowingEventArgs) _
        Handles dgv_license.EditingControlShowing
        AddHandler CType(e.Control, TextBox).KeyPress, AddressOf number_validation
    End Sub


    Private Sub dgv_others_EditingControlShowing(sender As Object, e As DataGridViewEditingControlShowingEventArgs) Handles _
        dgv_others.EditingControlShowing
        AddHandler CType(e.Control, TextBox).KeyPress, AddressOf number_validation
    End Sub

    Private Sub cmb_license_items_SelectionChangeCommitted(sender As Object, e As EventArgs) Handles _
        cmb_license_items.SelectionChangeCommitted
        cmb_payment_number_license.SelectedIndex = cmb_license_items.SelectedIndex
    End Sub

    Private Sub cmb_others_items_SelectionChangeCommitted(sender As Object, e As EventArgs) Handles _
        cmb_others_items.SelectionChangeCommitted
        cmb_payment_number_others.SelectedIndex = cmb_others_items.SelectedIndex
    End Sub

    Private Sub cmb_roc_items_SelectionChangeCommitted(sender As Object, e As EventArgs) Handles _
        cmb_roc_items.SelectionChangeCommitted
        cmb_payment_number_roc.SelectedIndex = cmb_roc_items.SelectedIndex
    End Sub

    Private Sub cmb_permits_items_SelectionChangeCommitted(sender As Object, e As EventArgs) Handles _
        cmb_permits_items.SelectionChangeCommitted
        cmb_payment_number_permits.SelectedIndex = cmb_permits_items.SelectedIndex
    End Sub

    Private Sub dgv_roc_EditingControlShowing(sender As Object, e As DataGridViewEditingControlShowingEventArgs) _
        Handles dgv_roc.EditingControlShowing
        AddHandler CType(e.Control, TextBox).KeyPress, AddressOf number_validation
    End Sub

    Private Sub dgv_permits_EditingControlShowing(sender As Object, e As DataGridViewEditingControlShowingEventArgs) _
        Handles dgv_permits.EditingControlShowing
        AddHandler CType(e.Control, TextBox).KeyPress, AddressOf number_validation
    End Sub

    Private Sub dgv_license_CellDoubleClick(sender As Object, e As DataGridViewCellEventArgs) Handles _
        dgv_license.CellDoubleClick
        If e.RowIndex >= 0 Then
            If e.ColumnIndex = 1 Then
                tabFocus = 1
                row_customize_no = e.RowIndex
                tb_others_value.Text =
                    customValues(Integer.Parse(dgv_license.Rows(row_customize_no).Cells(15).Value.ToString) - 1)
                pnl_mod_others.Visible = True
            End If
        End If
    End Sub

    Private Sub dgv_permits_CellDoubleClick(sender As Object, e As DataGridViewCellEventArgs) Handles _
        dgv_permits.CellDoubleClick
        If e.RowIndex >= 0 Then
            If e.ColumnIndex = 1 Then
                tabFocus = 2
                row_customize_no = e.RowIndex
                tb_others_value.Text =
                    customValues(Integer.Parse(dgv_permits.Rows(row_customize_no).Cells(15).Value.ToString) - 1)
                pnl_mod_others.Visible = True
            End If
        End If
    End Sub

    Private Sub dgv_roc_CellDoubleClick(sender As Object, e As DataGridViewCellEventArgs) Handles _
        dgv_roc.CellDoubleClick
        If e.RowIndex >= 0 Then
            If e.ColumnIndex = 1 Then
                tabFocus = 3
                row_customize_no = e.RowIndex
                tb_others_value.Text =
                    customValues(Integer.Parse(dgv_roc.Rows(row_customize_no).Cells(15).Value.ToString) - 1)
                pnl_mod_others.Visible = True
            End If
        End If
    End Sub

    Private Sub dgv_others_CellDoubleClick(sender As Object, e As DataGridViewCellEventArgs) Handles dgv_others.CellDoubleClick
        If e.RowIndex >= 0 Then
            If e.ColumnIndex = 1 Then
                tabFocus = 4
                row_customize_no = e.RowIndex
                tb_others_value.Text =
                    customValues(Integer.Parse(dgv_others.Rows(row_customize_no).Cells(15).Value.ToString) - 1)
                pnl_mod_others.Visible = True
            End If
        End If
    End Sub

    Private Sub btn_others_cancel_Click(sender As Object, e As EventArgs) Handles btn_cancel_others.Click
        pnl_mod_others.Visible = False
    End Sub

    Private Sub btn_others_confirm_Click(sender As Object, e As EventArgs) Handles btn_confirm_others.Click
        pnl_mod_others.Visible = False

        Select Case tabFocus
            Case 1
                customValues(Integer.Parse(dgv_license.Rows(row_customize_no).Cells(15).Value.ToString) _
                    - 1) = WebUtility.HtmlEncode(tb_others_value.Text)
                dgv_license.Rows(row_customize_no).Cells(1).Value = MOP(Integer.Parse(dgv_license.Rows(
                    row_customize_no).Cells(15).Value.ToString) - 1) + ": " + tb_others_value.Text
            Case 2
                customValues(Integer.Parse(dgv_permits.Rows(row_customize_no).Cells(15).Value.ToString) _
                    - 1) = WebUtility.HtmlEncode(tb_others_value.Text)
                dgv_permits.Rows(row_customize_no).Cells(1).Value = MOP(Integer.Parse(dgv_permits.Rows(
                    row_customize_no).Cells(15).Value.ToString) - 1) + ": " + tb_others_value.Text
            Case 3
                customValues(Integer.Parse(dgv_roc.Rows(row_customize_no).Cells(15).Value.ToString) -
                    1) = WebUtility.HtmlEncode(tb_others_value.Text)
                dgv_roc.Rows(row_customize_no).Cells(1).Value = MOP(Integer.Parse(dgv_roc.Rows(
                    row_customize_no).Cells(15).Value.ToString) - 1) + ": " + tb_others_value.Text
            Case Else
                customValues(Integer.Parse(dgv_others.Rows(row_customize_no).Cells(15).Value.ToString) _
                    - 1) = WebUtility.HtmlEncode(tb_others_value.Text)
                dgv_others.Rows(row_customize_no).Cells(1).Value = MOP(Integer.Parse(dgv_others.Rows(
                    row_customize_no).Cells(15).Value.ToString) - 1) + ": " + tb_others_value.Text
        End Select
    End Sub

    Sub save_soa()
        Try
            Dim uniqueID As String = Date.Now.ToString("yyyyMMddHHmmss")
            Dim process_type(3) As String
            Dim process_type_counter As Integer = 0
            Dim service_type(6) As String
            Dim service_type_counter As Integer = 0
            Dim selected_date = txt_date.Value.ToString("yyyy-MM-dd HH:mm:ss")
            Dim selected_date_expiry = dtp_expiry_date.Value.ToString("yyyy-MM-dd")

            dgv_license.EndEdit()
            dgv_permits.EndEdit()
            dgv_roc.EndEdit()
            dgv_others.EndEdit()

            If cb_process_dup.Checked Then
                process_type(process_type_counter) = "DUP"
                process_type_counter += 1
            End If

            If cb_process_mod.Checked Then
                process_type(process_type_counter) = "MOD"
                process_type_counter += 1
            End If

            If cb_process_others.Checked Then
                process_type(process_type_counter) = "OTHERS"
                process_type_counter += 1
            End If

            For Each process As String In process_type
                If process <> "" Then
                    class_connection.con.Open()
                    query = "insert into tbl_process_soa(process_type_id, process_name) Values('p" &
                        uniqueID & "', '" & process & "')"
                    cmd = New MySqlCommand(query, class_connection.con)
                    cmd.ExecuteNonQuery()
                    class_connection.con.Close()
                End If
            Next

            If cb_services_co.Checked Then
                service_type(service_type_counter) = "CO"
                service_type_counter += 1
            End If

            If cb_services_cv.Checked Then
                service_type(service_type_counter) = "CV"
                service_type_counter += 1
            End If

            If cb_services_ms.Checked Then
                service_type(service_type_counter) = "MS"
                service_type_counter += 1
            End If

            If cb_services_ma.Checked Then
                service_type(service_type_counter) = "MA"
                service_type_counter += 1
            End If

            If cb_services_roc.Checked Then
                service_type(service_type_counter) = "ROC"
                service_type_counter += 1
            End If

            If cb_services_others.Checked Then
                service_type(service_type_counter) = "OTHERS"
                service_type_counter += 1
            End If

            For Each service As String In service_type
                If service <> "" And service <> "OTHERS" Then
                    query = "insert into tbl_service_soa(service_type_id, service_name, " &
                    "custom_value) Values('s" + uniqueID + "', '" + service + "','None')"
                End If

                If service <> "" And service = "OTHERS" Then
                    query = "insert into tbl_service_soa(service_type_id, service_name, " &
                    "custom_value) Values('s" + uniqueID + "', '" + service + "','" +
                    WebUtility.HtmlEncode(tb_others.Text) + "')"
                End If
            Next

            class_connection.con.Open()
            cmd = New MySqlCommand(query, class_connection.con)
            cmd.ExecuteNonQuery()
            class_connection.con.Close()

            class_connection.con.Open()
            query = "insert into tbl_particular(part_id, part_pos, part_name, part_covered_from, " &
                "part_covered_to) Values('pt" + uniqueID + "', '1', '" +
                WebUtility.HtmlEncode(tb_particular_one.Text) + "','" +
                dtp_p1_from.Value.ToString("yyyy-MM-dd") + "', '" +
                dtp_p1_to.Value.ToString("yyyy-MM-dd") + "')"
            cmd = New MySqlCommand(query, class_connection.con)
            cmd.ExecuteNonQuery()
            class_connection.con.Close()

            If cb_particular_two.Checked Then
                class_connection.con.Open()
                query = "insert into tbl_particular(part_id, part_pos, part_name, part_covered_from, " &
                    "part_covered_to) Values('pt" + uniqueID + "', '2', '" +
                    WebUtility.HtmlEncode(tb_particular_two.Text) + "','" +
                    dtp_p2_from.Value.ToString("yyyy-MM-dd") + "', '" +
                    dtp_p2_to.Value.ToString("yyyy-MM-dd") + "')"
                cmd = New MySqlCommand(query, class_connection.con)
                cmd.ExecuteNonQuery()
                class_connection.con.Close()
            End If

            If cb_particular_three.Checked Then
                class_connection.con.Open()
                query = "insert into tbl_particular(part_id, part_pos, part_name, part_covered_from, " &
                    "part_covered_to) Values('pt" + uniqueID + "','3', '" +
                    WebUtility.HtmlEncode(tb_particular_three.Text) + "','" +
                    dtp_p3_from.Value.ToString("yyyy-MM-dd") + "', '" +
                    dtp_p3_to.Value.ToString("yyyy-MM-dd") + "')"
                cmd = New MySqlCommand(query, class_connection.con)
                cmd.ExecuteNonQuery()
                class_connection.con.Close()
            End If

            class_connection.con.Open()
            For i As Integer = 0 To dgv_license.Rows.Count - 1 Step +1
                cmd = New MySqlCommand("insert into tbl_soa_data(mop_id, others_custom_value, " &
                    "p1_no_of_years, p1_percent, p1_no_of_units, p1_fees, p2_no_of_years, p2_percent, " &
                    "p2_no_of_units, p2_fees, p3_no_of_years, p3_percent, p3_no_of_units, p3_fees, " &
                    "soa_data_id) Values(@mop_id, @others_custom_value, @p1_no_years, @p1_percent, " &
                    "@P1_no_units, @p1_fees, @p2_no_years, @p2_percent, @p2_no_units, @p2_fees, " &
                    "@p3_no_years, @p3_percent, @p3_no_units, @p3_fees, @soa_data_id)", class_connection.con)
                cmd.Parameters.Add("@mop_id", MySqlDbType.Int64).Value = dgv_license.Rows(i).Cells(15).Value.ToString()
                cmd.Parameters.Add("@others_custom_value", MySqlDbType.VarChar).Value =
                    customValues(Integer.Parse(dgv_license.Rows(i).Cells(15).Value.ToString()) - 1)
                cmd.Parameters.Add("@p1_no_years", MySqlDbType.Int64).Value = dgv_license.Rows(i).Cells(2).Value.ToString()
                cmd.Parameters.Add("@p1_percent", MySqlDbType.Decimal).Value = dgv_license.Rows(i).Cells(3).Value.ToString()
                cmd.Parameters.Add("@p1_no_units", MySqlDbType.Int64).Value = dgv_license.Rows(i).Cells(4).Value.ToString()
                cmd.Parameters.Add("@p1_fees", MySqlDbType.Decimal).Value = dgv_license.Rows(i).Cells(5).Value.ToString()
                cmd.Parameters.Add("@p2_no_years", MySqlDbType.Int64).Value = dgv_license.Rows(i).Cells(6).Value.ToString()
                cmd.Parameters.Add("@p2_percent", MySqlDbType.Decimal).Value = dgv_license.Rows(i).Cells(7).Value.ToString()
                cmd.Parameters.Add("@p2_no_units", MySqlDbType.Int64).Value = dgv_license.Rows(i).Cells(8).Value.ToString()
                cmd.Parameters.Add("@p2_fees", MySqlDbType.Decimal).Value = dgv_license.Rows(i).Cells(9).Value.ToString()
                cmd.Parameters.Add("@p3_no_years", MySqlDbType.Int64).Value = dgv_license.Rows(i).Cells(10).Value.ToString()
                cmd.Parameters.Add("@p3_percent", MySqlDbType.Decimal).Value = dgv_license.Rows(i).Cells(11).Value.ToString()
                cmd.Parameters.Add("@p3_no_units", MySqlDbType.Int64).Value = dgv_license.Rows(i).Cells(12).Value.ToString()
                cmd.Parameters.Add("@p3_fees", MySqlDbType.Decimal).Value = dgv_license.Rows(i).Cells(13).Value.ToString()
                cmd.Parameters.Add("@soa_data_id", MySqlDbType.VarChar).Value = "t" + uniqueID
                cmd.ExecuteNonQuery()
            Next
            class_connection.con.Close()

            class_connection.con.Open()
            For i As Integer = 0 To dgv_permits.Rows.Count - 1 Step +1
                cmd = New MySqlCommand("insert into tbl_soa_data(mop_id, others_custom_value, p1_no_of_years, p1_percent, " &
                        "p1_no_of_units, p1_fees, p2_no_of_years, p2_percent, p2_no_of_units, p2_fees, p3_no_of_years, " &
                        "p3_percent, p3_no_of_units, p3_fees, soa_data_id) Values(@mop_id, @others_custom_value, " &
                        "@p1_no_years, @p1_percent, @P1_no_units, @p1_fees, @p2_no_years, @p2_percent, @p2_no_units, @p2_fees, " &
                        "@p3_no_years, @p3_percent, @p3_no_units, @p3_fees, @soa_data_id)", class_connection.con)
                cmd.Parameters.Add("@mop_id", MySqlDbType.Int64).Value = dgv_permits.Rows(i).Cells(15).Value.ToString()
                cmd.Parameters.Add("@others_custom_value", MySqlDbType.VarChar).Value =
                    customValues(Integer.Parse(dgv_permits.Rows(i).Cells(15).Value.ToString()) - 1)
                cmd.Parameters.Add("@p1_no_years", MySqlDbType.Int64).Value = dgv_permits.Rows(i).Cells(2).Value.ToString()
                cmd.Parameters.Add("@p1_percent", MySqlDbType.Decimal).Value = dgv_permits.Rows(i).Cells(3).Value.ToString()
                cmd.Parameters.Add("@p1_no_units", MySqlDbType.Int64).Value = dgv_permits.Rows(i).Cells(4).Value.ToString()
                cmd.Parameters.Add("@p1_fees", MySqlDbType.Decimal).Value = dgv_permits.Rows(i).Cells(5).Value.ToString()
                cmd.Parameters.Add("@p2_no_years", MySqlDbType.Int64).Value = dgv_permits.Rows(i).Cells(6).Value.ToString()
                cmd.Parameters.Add("@p2_percent", MySqlDbType.Decimal).Value = dgv_permits.Rows(i).Cells(7).Value.ToString()
                cmd.Parameters.Add("@p2_no_units", MySqlDbType.Int64).Value = dgv_permits.Rows(i).Cells(8).Value.ToString()
                cmd.Parameters.Add("@p2_fees", MySqlDbType.Decimal).Value = dgv_permits.Rows(i).Cells(9).Value.ToString()
                cmd.Parameters.Add("@p3_no_years", MySqlDbType.Int64).Value = dgv_permits.Rows(i).Cells(10).Value.ToString()
                cmd.Parameters.Add("@p3_percent", MySqlDbType.Decimal).Value = dgv_permits.Rows(i).Cells(11).Value.ToString()
                cmd.Parameters.Add("@p3_no_units", MySqlDbType.Int64).Value = dgv_permits.Rows(i).Cells(12).Value.ToString()
                cmd.Parameters.Add("@p3_fees", MySqlDbType.Decimal).Value = dgv_permits.Rows(i).Cells(13).Value.ToString()
                cmd.Parameters.Add("@soa_data_id", MySqlDbType.VarChar).Value = "t" + uniqueID
                cmd.ExecuteNonQuery()
            Next
            class_connection.con.Close()

            class_connection.con.Open()
            For i As Integer = 0 To dgv_roc.Rows.Count - 1 Step +1
                cmd = New MySqlCommand("insert into tbl_soa_data(mop_id, others_custom_value, p1_no_of_years, p1_percent, " &
                    "p1_no_of_units, p1_fees, p2_no_of_years, p2_percent, p2_no_of_units, p2_fees, p3_no_of_years, " &
                    "p3_percent, p3_no_of_units, p3_fees, soa_data_id) Values(@mop_id, @others_custom_value, " &
                    "@p1_no_years, @p1_percent, @P1_no_units, @p1_fees, @p2_no_years, @p2_percent, @p2_no_units, @p2_fees, " &
                    "@p3_no_years, @p3_percent, @p3_no_units, @p3_fees, @soa_data_id)", class_connection.con)
                cmd.Parameters.Add("@mop_id", MySqlDbType.Int64).Value = dgv_roc.Rows(i).Cells(15).Value.ToString()
                cmd.Parameters.Add("@others_custom_value", MySqlDbType.VarChar).Value =
                    customValues(Integer.Parse(dgv_roc.Rows(i).Cells(15).Value.ToString()) - 1)
                cmd.Parameters.Add("@p1_no_years", MySqlDbType.Int64).Value = dgv_roc.Rows(i).Cells(2).Value.ToString()
                cmd.Parameters.Add("@p1_percent", MySqlDbType.Decimal).Value = dgv_roc.Rows(i).Cells(3).Value.ToString()
                cmd.Parameters.Add("@p1_no_units", MySqlDbType.Int64).Value = dgv_roc.Rows(i).Cells(4).Value.ToString()
                cmd.Parameters.Add("@p1_fees", MySqlDbType.Decimal).Value = dgv_roc.Rows(i).Cells(5).Value.ToString()
                cmd.Parameters.Add("@p2_no_years", MySqlDbType.Int64).Value = dgv_roc.Rows(i).Cells(6).Value.ToString()
                cmd.Parameters.Add("@p2_percent", MySqlDbType.Decimal).Value = dgv_roc.Rows(i).Cells(7).Value.ToString()
                cmd.Parameters.Add("@p2_no_units", MySqlDbType.Int64).Value = dgv_roc.Rows(i).Cells(8).Value.ToString()
                cmd.Parameters.Add("@p2_fees", MySqlDbType.Decimal).Value = dgv_roc.Rows(i).Cells(9).Value.ToString()
                cmd.Parameters.Add("@p3_no_years", MySqlDbType.Int64).Value = dgv_roc.Rows(i).Cells(10).Value.ToString()
                cmd.Parameters.Add("@p3_percent", MySqlDbType.Decimal).Value = dgv_roc.Rows(i).Cells(11).Value.ToString()
                cmd.Parameters.Add("@p3_no_units", MySqlDbType.Int64).Value = dgv_roc.Rows(i).Cells(12).Value.ToString()
                cmd.Parameters.Add("@p3_fees", MySqlDbType.Decimal).Value = dgv_roc.Rows(i).Cells(13).Value.ToString()
                cmd.Parameters.Add("@soa_data_id", MySqlDbType.VarChar).Value = "t" + uniqueID
                cmd.ExecuteNonQuery()

            Next
            class_connection.con.Close()

            class_connection.con.Open()
            For i As Integer = 0 To dgv_others.Rows.Count - 1 Step +1
                cmd = New MySqlCommand("insert into tbl_soa_data(mop_id, others_custom_value, p1_no_of_years, p1_percent, " &
                    "p1_no_of_units, p1_fees, p2_no_of_years, p2_percent, p2_no_of_units, p2_fees, p3_no_of_years, " &
                    "p3_percent, p3_no_of_units, p3_fees, soa_data_id) Values(@mop_id, @others_custom_value, @p1_no_years, " &
                    "@p1_percent, @P1_no_units, @p1_fees, @p2_no_years, @p2_percent, @p2_no_units, @p2_fees, @p3_no_years, " &
                    "@p3_percent, @p3_no_units, @p3_fees, @soa_data_id)", class_connection.con)
                cmd.Parameters.Add("@mop_id", MySqlDbType.Int64).Value = dgv_others.Rows(i).Cells(15).Value.ToString()
                cmd.Parameters.Add("@others_custom_value", MySqlDbType.VarChar).Value =
                    customValues(Integer.Parse(dgv_others.Rows(i).Cells(15).Value.ToString()) - 1)
                cmd.Parameters.Add("@p1_no_years", MySqlDbType.Int64).Value = dgv_others.Rows(i).Cells(2).Value.ToString()
                cmd.Parameters.Add("@p1_percent", MySqlDbType.Decimal).Value = dgv_others.Rows(i).Cells(3).Value.ToString()
                cmd.Parameters.Add("@p1_no_units", MySqlDbType.Int64).Value = dgv_others.Rows(i).Cells(4).Value.ToString()
                cmd.Parameters.Add("@p1_fees", MySqlDbType.Decimal).Value = dgv_others.Rows(i).Cells(5).Value.ToString()
                cmd.Parameters.Add("@p2_no_years", MySqlDbType.Int64).Value = dgv_others.Rows(i).Cells(6).Value.ToString()
                cmd.Parameters.Add("@p2_percent", MySqlDbType.Decimal).Value = dgv_others.Rows(i).Cells(7).Value.ToString()
                cmd.Parameters.Add("@p2_no_units", MySqlDbType.Int64).Value = dgv_others.Rows(i).Cells(8).Value.ToString()
                cmd.Parameters.Add("@p2_fees", MySqlDbType.Decimal).Value = dgv_others.Rows(i).Cells(9).Value.ToString()
                cmd.Parameters.Add("@p3_no_years", MySqlDbType.Int64).Value = dgv_others.Rows(i).Cells(10).Value.ToString()
                cmd.Parameters.Add("@p3_percent", MySqlDbType.Decimal).Value = dgv_others.Rows(i).Cells(11).Value.ToString()
                cmd.Parameters.Add("@p3_no_units", MySqlDbType.Int64).Value = dgv_others.Rows(i).Cells(12).Value.ToString()
                cmd.Parameters.Add("@p3_fees", MySqlDbType.Decimal).Value = dgv_others.Rows(i).Cells(13).Value.ToString()
                cmd.Parameters.Add("@soa_data_id", MySqlDbType.VarChar).Value = "t" + uniqueID
                cmd.ExecuteNonQuery()
            Next
            class_connection.con.Close()

            generate_SOA_serial()

            class_connection.con.Open()
            query = "insert into tbl_soa_details(payor_id, serial_id, date_created, date_modified, date_expires, " &
                "application_type, process_id, service_id, particular_id, attachment_id, soa_data_id) Values('" &
                payor_id & "','" & serial & "','" & selected_date & "', '" & selected_date & "','" &
                selected_date_expiry & "','" & cmb_type.Text & "', 'p" & uniqueID & "', 's" & uniqueID & "', 'pt" &
                uniqueID & "','a" & uniqueID & "','" & "t" & uniqueID & "'); SELECT LAST_INSERT_ID()"
            cmd = New MySqlCommand(query, class_connection.con)
            soa_details_id = CInt(cmd.ExecuteScalar())
            class_connection.con.Close()

            ' Update transact.
            class_connection.con.Open()
            query = "update tbl_soa_transact set soa_details_id = '" & soa_details_id.ToString &
                "' where soa_id = '" & transactID.ToString + "'"
            cmd = New MySqlCommand(query, class_connection.con)
            cmd.ExecuteNonQuery()
            class_connection.con.Close()

            frm_licenser_log.soa_id_transact = transactID
            frm_licenser_log.user_id = user_id
            frm_licenser_log.authority_level = authority_level
            frm_licenser_log.pnl_refresh.Visible = True
            frm_licenser_log.Show()

            Close()
        Catch ex As Exception
            class_connection.con.Close()
            MsgBox("Could'nt connect to server. Contact the IT Administrator for assistance.",
                   MsgBoxStyle.Exclamation, "NTC Region 10")
        End Try
    End Sub

    Sub generate_SOA_serial()
        Dim current_year = Now.ToString("yyyy")
        Dim current_month = Now.ToString("MM")
        Dim newCounter As Integer

        class_connection.con.Open()
        query = "select setting_value from tbl_settings where setting = 'starting_digit_soa'"
        cmd = New MySqlCommand(query, class_connection.con)
        reader = cmd.ExecuteReader()

        If reader.Read Then
            starting_digit_soa = reader.GetString("setting_value")
        End If
        class_connection.con.Close()

        'Retrieve override soa serial
        class_connection.con.Open()
        query = "select setting_value,activate from tbl_settings where setting = 'override_soa_series'"
        cmd = New MySqlCommand(query, class_connection.con)
        reader = cmd.ExecuteReader()

        If reader.Read Then
            soa_override_number = reader.GetInt32("setting_value")

            If reader.GetString("activate") = "1" Then
                soa_override_activate_value = "1"
            End If
        End If
        class_connection.con.Close()

        class_connection.con.Open()
        query = "select setting_value from tbl_settings where setting = 'starting_series_soa'"
        cmd = New MySqlCommand(query, class_connection.con)
        reader = cmd.ExecuteReader()

        If reader.Read Then
            starting_series_soa = reader.GetInt32("setting_value")
        End If
        class_connection.con.Close()

        class_connection.con.Open()
        query = "select soa_id, counter, date_created, soa_month, soa_year from tbl_soa_transact join " &
            "tbl_soa_details on tbl_soa_details.soa_details_id = tbl_soa_transact.soa_id order by " &
            "soa_year desc, soa_month desc, counter desc limit 1"
        cmd = New MySqlCommand(query, class_connection.con)
        reader = cmd.ExecuteReader()

        If reader.Read Then
            If current_month = reader.GetDateTime("date_created").ToString("MM") Then
                If soa_override_activate_value = "0" Then
                    serial = starting_digit_soa & "-" & current_year & "-" & current_month & "-" &
                        (reader.GetInt32("counter") + 1).ToString("D4")
                    is_first_transact = False
                    last_counter = reader.GetString("counter")
                Else
                    serial = starting_digit_soa & "-" & current_year & "-" & current_month & "-" &
                        soa_override_number.ToString("D4")
                    is_first_transact = False
                    last_counter = soa_override_number - 1
                    soa_override_isActivated = True
                End If
            Else
                If soa_override_activate_value = "0" Then
                    serial = starting_digit_soa & "-" & current_year & "-" & current_month & "-0001"
                    is_first_transact = False
                    last_counter = 0
                Else
                    serial = starting_digit_soa & "-" & current_year & "-" & current_month & "-" &
                        soa_override_number.ToString("D4")
                    is_first_transact = False
                    last_counter = soa_override_number - 1
                    soa_override_isActivated = True
                End If
            End If
        Else
            ' If this is the first transaction
            serial = starting_digit_soa + "-" & current_year & "-" & current_month & "-" +
                starting_series_soa.ToString("D4")
            is_first_transact = True
        End If
        class_connection.con.Close()

        If is_first_transact Then
            query = "insert into tbl_soa_transact(is_endorse, soa_details_id, user_id, user_id_approve, " &
                "user_approve, counter, soa_month, soa_year) Values('0', '0', '" & user_id &
                "', '0', '0', '" & starting_series_soa.ToString & "','" & current_month & "','" &
                current_year & "'); SELECT LAST_INSERT_ID()"
        Else
            newCounter = Integer.Parse(last_counter) + 1
            query = "insert into tbl_soa_transact(is_endorse, soa_details_id, user_id, user_id_approve, " &
                "user_approve,counter,soa_month,soa_year) Values('0', '0', '" & user_id & "', '0', '0', '" &
                newCounter.ToString & "','" & current_month & "','" & current_year & "'); SELECT LAST_INSERT_ID()"
        End If

        class_connection.con.Open()
        cmd = New MySqlCommand(query, class_connection.con)
        transactID = CInt(cmd.ExecuteScalar())
        class_connection.con.Close()

        If soa_override_isActivated Then
            deactivate_SOA_override()
        End If
    End Sub

    Sub deactivate_SOA_override()
        ' Update override soa.
        class_connection.con.Open()
        query = "update tbl_settings set activate = '0' where setting = 'override_soa_series'"
        cmd = New MySqlCommand(query, class_connection.con)
        cmd.ExecuteNonQuery()
        class_connection.con.Close()

        soa_override_isActivated = False
        soa_override_activate_value = "0"
    End Sub

    Private Sub cb_particular_two_CheckedChanged(sender As Object, e As EventArgs) Handles _
        cb_particular_two.CheckedChanged
        If cb_particular_two.Checked Then
            tb_particular_two.Enabled = True
            cb_particular_three.Enabled = True

            yearDiffTwo = DateDiff(DateInterval.Year, dtp_p2_from.Value, dtp_p2_to.Value)

            'enable edit in particular 2
            For Each row As DataGridViewRow In dgv_license.Rows
                dgv_license.Columns(6).ReadOnly = False
                dgv_license.Columns(7).ReadOnly = False
                dgv_license.Columns(8).ReadOnly = False
                dgv_license.Columns(9).ReadOnly = False
            Next

            For Each row As DataGridViewRow In dgv_permits.Rows
                dgv_permits.Columns(6).ReadOnly = False
                dgv_permits.Columns(7).ReadOnly = False
                dgv_permits.Columns(8).ReadOnly = False
                dgv_permits.Columns(9).ReadOnly = False
            Next

            For Each row As DataGridViewRow In dgv_roc.Rows
                dgv_roc.Columns(6).ReadOnly = False
                dgv_roc.Columns(7).ReadOnly = False
                dgv_roc.Columns(8).ReadOnly = False
                dgv_roc.Columns(9).ReadOnly = False
            Next

            For Each row As DataGridViewRow In dgv_others.Rows
                dgv_others.Columns(6).ReadOnly = False
                dgv_others.Columns(7).ReadOnly = False
                dgv_others.Columns(8).ReadOnly = False
                dgv_others.Columns(9).ReadOnly = False
            Next
            'end of edit in particular 2

            'change no. of years in particular 2
            For i As Integer = 0 To dgv_license.RowCount - 1
                dgv_license.Rows(i).Cells(6).Value = yearDiffTwo.ToString
                dgv_license.Rows(i).Cells(7).Value = "100"
                dgv_license.Rows(i).Cells(8).Value = "0"
            Next

            For i As Integer = 0 To dgv_permits.RowCount - 1
                dgv_permits.Rows(i).Cells(6).Value = yearDiffTwo.ToString
                dgv_permits.Rows(i).Cells(7).Value = "100"
                dgv_permits.Rows(i).Cells(8).Value = "0"
            Next

            For i As Integer = 0 To dgv_roc.RowCount - 1
                dgv_roc.Rows(i).Cells(6).Value = yearDiffTwo.ToString
                dgv_roc.Rows(i).Cells(7).Value = "100"
                dgv_roc.Rows(i).Cells(8).Value = "0"
            Next

            For i As Integer = 0 To dgv_others.RowCount - 1
                dgv_others.Rows(i).Cells(6).Value = yearDiffTwo.ToString
                dgv_others.Rows(i).Cells(7).Value = "100"
                dgv_others.Rows(i).Cells(8).Value = "0"
            Next
            'end of change no. of years in particular 2

        Else
            cb_particular_three.Enabled = False
            cb_particular_three.Checked = False
            tb_particular_two.Enabled = False
            dtp_p2_from.Enabled = False
            dtp_p2_to.Enabled = False
            tb_particular_two.Text = ""
            dtp_p2_from.Text = ""
            dtp_p2_to.Text = ""
            tb_particular_three.Text = ""
            dtp_p3_from.Text = ""
            dtp_p3_to.Text = ""

            For i As Integer = 0 To dgv_license.RowCount - 1
                dgv_license.Rows(i).Cells(6).Value = "0"
                dgv_license.Rows(i).Cells(7).Value = "0"
                dgv_license.Rows(i).Cells(8).Value = "0"
                dgv_license.Rows(i).Cells(9).Value = 0
                dgv_license.Rows(i).Cells(10).Value = "0"
                dgv_license.Rows(i).Cells(11).Value = "0"
                dgv_license.Rows(i).Cells(12).Value = "0"
                dgv_license.Rows(i).Cells(13).Value = 0
                dgv_license.Rows(i).Cells(6).ReadOnly = True
                dgv_license.Rows(i).Cells(7).ReadOnly = True
                dgv_license.Rows(i).Cells(8).ReadOnly = True
                dgv_license.Rows(i).Cells(9).ReadOnly = True
                dgv_license.Rows(i).Cells(10).ReadOnly = True
                dgv_license.Rows(i).Cells(11).ReadOnly = True
                dgv_license.Rows(i).Cells(12).ReadOnly = True
                dgv_license.Rows(i).Cells(13).ReadOnly = True
            Next

            For i As Integer = 0 To dgv_permits.RowCount - 1
                dgv_permits.Rows(i).Cells(6).Value = "0"
                dgv_permits.Rows(i).Cells(7).Value = "0"
                dgv_permits.Rows(i).Cells(8).Value = "0"
                dgv_permits.Rows(i).Cells(9).Value = 0
                dgv_permits.Rows(i).Cells(10).Value = "0"
                dgv_permits.Rows(i).Cells(11).Value = "0"
                dgv_permits.Rows(i).Cells(12).Value = "0"
                dgv_permits.Rows(i).Cells(13).Value = 0
                dgv_permits.Rows(i).Cells(6).ReadOnly = True
                dgv_permits.Rows(i).Cells(7).ReadOnly = True
                dgv_permits.Rows(i).Cells(8).ReadOnly = True
                dgv_permits.Rows(i).Cells(9).ReadOnly = True
                dgv_permits.Rows(i).Cells(10).ReadOnly = True
                dgv_permits.Rows(i).Cells(11).ReadOnly = True
                dgv_permits.Rows(i).Cells(12).ReadOnly = True
                dgv_permits.Rows(i).Cells(13).ReadOnly = True
            Next

            For i As Integer = 0 To dgv_roc.RowCount - 1
                dgv_roc.Rows(i).Cells(6).Value = "0"
                dgv_roc.Rows(i).Cells(7).Value = "0"
                dgv_roc.Rows(i).Cells(8).Value = "0"
                dgv_roc.Rows(i).Cells(9).Value = 0
                dgv_roc.Rows(i).Cells(10).Value = "0"
                dgv_roc.Rows(i).Cells(11).Value = "0"
                dgv_roc.Rows(i).Cells(12).Value = "0"
                dgv_roc.Rows(i).Cells(13).Value = 0
                dgv_roc.Rows(i).Cells(6).ReadOnly = True
                dgv_roc.Rows(i).Cells(7).ReadOnly = True
                dgv_roc.Rows(i).Cells(8).ReadOnly = True
                dgv_roc.Rows(i).Cells(9).ReadOnly = True
                dgv_roc.Rows(i).Cells(10).ReadOnly = True
                dgv_roc.Rows(i).Cells(11).ReadOnly = True
                dgv_roc.Rows(i).Cells(12).ReadOnly = True
                dgv_roc.Rows(i).Cells(13).ReadOnly = True
            Next

            For i As Integer = 0 To dgv_others.RowCount - 1
                dgv_others.Rows(i).Cells(6).Value = "0"
                dgv_others.Rows(i).Cells(7).Value = "0"
                dgv_others.Rows(i).Cells(8).Value = "0"
                dgv_others.Rows(i).Cells(9).Value = 0
                dgv_others.Rows(i).Cells(10).Value = "0"
                dgv_others.Rows(i).Cells(11).Value = "0"
                dgv_others.Rows(i).Cells(12).Value = "0"
                dgv_others.Rows(i).Cells(13).Value = 0
                dgv_others.Rows(i).Cells(6).ReadOnly = True
                dgv_others.Rows(i).Cells(7).ReadOnly = True
                dgv_others.Rows(i).Cells(8).ReadOnly = True
                dgv_others.Rows(i).Cells(9).ReadOnly = True
                dgv_others.Rows(i).Cells(10).ReadOnly = True
                dgv_others.Rows(i).Cells(11).ReadOnly = True
                dgv_others.Rows(i).Cells(12).ReadOnly = True
                dgv_others.Rows(i).Cells(13).ReadOnly = True
            Next
        End If
        compute_license_subtotal()
        compute_total()
        update_expiration_date()
    End Sub

    Private Sub cb_particular_three_CheckedChanged(sender As Object, e As EventArgs) Handles cb_particular_three.CheckedChanged
        If cb_particular_three.Checked Then
            tb_particular_three.Enabled = True

            yearDiffThree = DateDiff(DateInterval.Year, dtp_p3_from.Value, dtp_p3_to.Value)

            'enable edit in particular 3
            For Each row As DataGridViewRow In dgv_license.Rows
                dgv_license.Columns(10).ReadOnly = False
                dgv_license.Columns(11).ReadOnly = False
                dgv_license.Columns(12).ReadOnly = False
                dgv_license.Columns(13).ReadOnly = False
            Next

            For Each row As DataGridViewRow In dgv_permits.Rows
                dgv_permits.Columns(10).ReadOnly = False
                dgv_permits.Columns(11).ReadOnly = False
                dgv_permits.Columns(12).ReadOnly = False
                dgv_permits.Columns(13).ReadOnly = False
            Next

            For Each row As DataGridViewRow In dgv_roc.Rows
                dgv_roc.Columns(10).ReadOnly = False
                dgv_roc.Columns(11).ReadOnly = False
                dgv_roc.Columns(12).ReadOnly = False
                dgv_roc.Columns(13).ReadOnly = False
            Next

            For Each row As DataGridViewRow In dgv_others.Rows
                dgv_others.Columns(10).ReadOnly = False
                dgv_others.Columns(11).ReadOnly = False
                dgv_others.Columns(12).ReadOnly = False
                dgv_others.Columns(13).ReadOnly = False
            Next
            'end of edit in particular 3

            'change no. of years in particular 3
            For i As Integer = 0 To dgv_license.RowCount - 1
                dgv_license.Rows(i).Cells(10).Value = yearDiffThree.ToString
                dgv_license.Rows(i).Cells(11).Value = "100"
                dgv_license.Rows(i).Cells(12).Value = "0"
            Next

            For i As Integer = 0 To dgv_permits.RowCount - 1
                dgv_permits.Rows(i).Cells(10).Value = yearDiffThree.ToString
                dgv_permits.Rows(i).Cells(11).Value = "100"
                dgv_permits.Rows(i).Cells(12).Value = "0"
            Next

            For i As Integer = 0 To dgv_roc.RowCount - 1
                dgv_roc.Rows(i).Cells(10).Value = yearDiffThree.ToString
                dgv_roc.Rows(i).Cells(11).Value = "100"
                dgv_roc.Rows(i).Cells(12).Value = "0"
            Next

            For i As Integer = 0 To dgv_others.RowCount - 1
                dgv_others.Rows(i).Cells(10).Value = yearDiffThree.ToString
                dgv_others.Rows(i).Cells(11).Value = "100"
                dgv_others.Rows(i).Cells(12).Value = "0"
            Next
            'end of change no. of years in particular 3
        Else
            tb_particular_three.Enabled = False
            dtp_p3_from.Enabled = False
            dtp_p3_to.Enabled = False
            tb_particular_three.Text = ""
            dtp_p3_from.Text = ""
            dtp_p3_to.Text = ""

            For i As Integer = 0 To dgv_license.RowCount - 1
                dgv_license.Rows(i).Cells(10).Value = "0"
                dgv_license.Rows(i).Cells(11).Value = "0"
                dgv_license.Rows(i).Cells(12).Value = "0"
                dgv_license.Rows(i).Cells(13).Value = 0
                dgv_license.Rows(i).Cells(10).ReadOnly = True
                dgv_license.Rows(i).Cells(11).ReadOnly = True
                dgv_license.Rows(i).Cells(12).ReadOnly = True
                dgv_license.Rows(i).Cells(13).ReadOnly = True
            Next

            For i As Integer = 0 To dgv_permits.RowCount - 1
                dgv_permits.Rows(i).Cells(10).Value = "0"
                dgv_permits.Rows(i).Cells(11).Value = "0"
                dgv_permits.Rows(i).Cells(12).Value = "0"
                dgv_permits.Rows(i).Cells(13).Value = 0
                dgv_permits.Rows(i).Cells(10).ReadOnly = True
                dgv_permits.Rows(i).Cells(11).ReadOnly = True
                dgv_permits.Rows(i).Cells(12).ReadOnly = True
                dgv_permits.Rows(i).Cells(13).ReadOnly = True
            Next

            For i As Integer = 0 To dgv_roc.RowCount - 1
                dgv_roc.Rows(i).Cells(10).Value = "0"
                dgv_roc.Rows(i).Cells(11).Value = "0"
                dgv_roc.Rows(i).Cells(12).Value = "0"
                dgv_roc.Rows(i).Cells(13).Value = 0
                dgv_roc.Rows(i).Cells(10).ReadOnly = True
                dgv_roc.Rows(i).Cells(11).ReadOnly = True
                dgv_roc.Rows(i).Cells(12).ReadOnly = True
                dgv_roc.Rows(i).Cells(13).ReadOnly = True
            Next

            For i As Integer = 0 To dgv_others.RowCount - 1
                dgv_others.Rows(i).Cells(10).Value = "0"
                dgv_others.Rows(i).Cells(11).Value = "0"
                dgv_others.Rows(i).Cells(12).Value = "0"
                dgv_others.Rows(i).Cells(13).Value = 0
                dgv_others.Rows(i).Cells(10).ReadOnly = True
                dgv_others.Rows(i).Cells(11).ReadOnly = True
                dgv_others.Rows(i).Cells(12).ReadOnly = True
                dgv_others.Rows(i).Cells(13).ReadOnly = True
            Next
        End If

        compute_license_subtotal()
        compute_total()
        update_expiration_date()
    End Sub

    Sub update_expiration_date()
        If tb_particular_one.Text <> "" And tb_particular_two.Text = "" And tb_particular_three.Text = "" Then
            Dim currentDate As Date

            currentDate = Convert.ToDateTime(dtp_p1_from.Text)
            currentDate = currentDate.AddDays(1)
            dtp_expiry_date.Text = currentDate.ToString("yyyy-MM-dd")

        ElseIf tb_particular_one.Text <> "" And tb_particular_two.Text <> "" And tb_particular_three.Text = "" Then
            Dim currentDate_p1, currentDate_p2 As Date
            currentDate_p1 = Convert.ToDateTime(dtp_p1_from.Text)
            currentDate_p2 = Convert.ToDateTime(dtp_p2_from.Text)

            If currentDate_p1 < currentDate_p2 Then
                currentDate_p1 = currentDate_p1.AddDays(1)
                dtp_expiry_date.Text = currentDate_p1.ToString("yyyy-MM-dd")
            Else
                currentDate_p2 = currentDate_p2.AddDays(1)
                dtp_expiry_date.Text = currentDate_p2.ToString("yyyy-MM-dd")
            End If

        ElseIf tb_particular_one.Text <> "" And tb_particular_two.Text <> "" And tb_particular_three.Text <> "" Then
            Dim currentDate_p1, currentDate_p2, currentDate_p3 As Date
            currentDate_p1 = Convert.ToDateTime(dtp_p1_from.Text)
            currentDate_p2 = Convert.ToDateTime(dtp_p2_from.Text)
            currentDate_p3 = Convert.ToDateTime(dtp_p3_from.Text)

            If currentDate_p1 < currentDate_p2 Then
                If currentDate_p1 < currentDate_p3 Then
                    currentDate_p1 = currentDate_p1.AddDays(1)
                    dtp_expiry_date.Text = currentDate_p1.ToString("yyyy-MM-dd")
                Else
                    currentDate_p3 = currentDate_p3.AddDays(1)
                    dtp_expiry_date.Text = currentDate_p3.ToString("yyyy-MM-dd")
                End If

            Else
                If currentDate_p2 < currentDate_p3 Then
                    currentDate_p2 = currentDate_p2.AddDays(1)
                    dtp_expiry_date.Text = currentDate_p2.ToString("yyyy-MM-dd")

                Else
                    currentDate_p3 = currentDate_p3.AddDays(1)
                    dtp_expiry_date.Text = currentDate_p3.ToString("yyyy-MM-dd")
                End If

            End If
        End If
    End Sub

    Private Sub viewTransactionFrm_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Try
            Dim currentDate As Date

            currentDate = Convert.ToDateTime(dtp_p1_from.Text)
            currentDate = currentDate.AddDays(1)
            dtp_expiry_date.Text = currentDate.ToString("yyyy-MM-dd")

            loadPaymentCodesAndDescriptions()
            Icon = My.Resources.ntc_material_logo
        Catch ex As Exception
            MsgBox("Something went wrong. Couldn't process the data.", MsgBoxStyle.Exclamation,
                   "NTC Region 10")
            Close()
        End Try
    End Sub

    Sub loadPaymentCodesAndDescriptions()
        'FOR LICENSES
        class_connection.con.Open()
        query = "select mop_id, code, description from tbl_mode_of_payment where cat = 'LICENSE'"
        cmd = New MySqlCommand(query, class_connection.con)
        reader = cmd.ExecuteReader()

        While reader.Read
            cmb_payment_number_license.Items.Add(reader.GetString("mop_id"))
            cmb_license_items.Items.Add(reader.GetString("code") + " - " +
                 WebUtility.HtmlDecode(reader.GetString("description")))
        End While
        class_connection.con.Close()

        'FOR PERMITS
        class_connection.con.Open()
        query = "select mop_id, code, description from tbl_mode_of_payment where cat = 'PERMIT'"
        cmd = New MySqlCommand(query, class_connection.con)
        reader = cmd.ExecuteReader()

        While reader.Read
            cmb_payment_number_permits.Items.Add(reader.GetString("mop_id"))
            cmb_permits_items.Items.Add(reader.GetString("code") + " - " +
                WebUtility.HtmlDecode(reader.GetString("description")))
        End While
        class_connection.con.Close()

        'FOR ROC
        class_connection.con.Open()
        query = "select mop_id, code, description from tbl_mode_of_payment where cat = 'AROC/ROC'"
        cmd = New MySqlCommand(query, class_connection.con)
        reader = cmd.ExecuteReader()

        While reader.Read
            cmb_payment_number_roc.Items.Add(reader.GetString("mop_id"))
            cmb_roc_items.Items.Add(reader.GetString("code") + " - " +
                WebUtility.HtmlDecode(reader.GetString("description")))
        End While
        class_connection.con.Close()

        'OTHER APPLICATION
        class_connection.con.Open()
        query = "select mop_id, code, description from tbl_mode_of_payment where cat = 'OTHER'"
        cmd = New MySqlCommand(query, class_connection.con)
        reader = cmd.ExecuteReader()

        While reader.Read
            cmb_payment_number_others.Items.Add(reader.GetString("mop_id"))
            cmb_others_items.Items.Add(reader.GetString("code") + " - " +
                WebUtility.HtmlDecode(reader.GetString("description")))
        End While
        class_connection.con.Close()

        Dim paymentDescCounter As Integer = 0
        class_connection.con.Open()
        query = "select mop_id, code, description from tbl_mode_of_payment"
        cmd = New MySqlCommand(query, class_connection.con)
        reader = cmd.ExecuteReader()

        While reader.Read
            MOP(paymentDescCounter) = WebUtility.HtmlDecode(reader.GetString("description"))
            paymentDescCounter += 1
        End While
        class_connection.con.Close()
    End Sub

    Private Sub btn_addpayor_Click(sender As Object, e As EventArgs) Handles btn_addpayor.Click
        frm_payor_list.accessFrom = 1
        frm_payor_list.Show()
    End Sub
End Class