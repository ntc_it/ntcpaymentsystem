﻿Imports MySql.Data.MySqlClient
Imports System.Net
Imports Microsoft.Office.Interop
Imports System.IO

Public Class frm_accountant
    Dim cmd As MySqlCommand
    Dim reader As MySqlDataReader
    Dim query, endorse_id, user_id_cashier, endorse_officer_id, signature_endorser,
        enable_endorser_signature As String
    Dim export_success As Boolean = False
    Public transact_ID As Integer
    Public user_id, n_ID, authority_level, longDate As String

    Private Sub btn_finish_Click(sender As Object, e As EventArgs) Handles btn_finish.Click
        ' Update Order of Payment transaction and notify cashier.
        Try
            Dim purpose_one = WebUtility.HtmlEncode(tb_purpose_one.Text)
            Dim purpose_two = WebUtility.HtmlEncode(tb_purpose_two.Text)
            Dim bank_number_one = WebUtility.HtmlEncode(tb_bank_num.Text)
            Dim bank_name_one = WebUtility.HtmlEncode(tb_bank_name.Text)
            Dim bank_number_two = WebUtility.HtmlEncode(tb_bank_num_two.Text)
            Dim bank_name_two = WebUtility.HtmlEncode(tb_bank_name_two.Text)
            Dim current_datetime = Date.Now.ToString("yyyy-MM-dd HH:mm:ss")
            Dim user_notifications As New uc_notifications

            ' Retrieve Order of Payment ID.
            class_connection.con.Open()
            query = "select endorse_id from tbl_soa_endorse where soa_id = '" & transact_ID.ToString & "'"
            cmd = New MySqlCommand(query, class_connection.con)
            reader = cmd.ExecuteReader()

            If reader.Read Then
                endorse_id = reader.GetString("endorse_id")
            End If
            class_connection.con.Close()

            ' Update Order of Payment details after confirmation.
            class_connection.con.Open()
            query = "update tbl_soa_endorse set fund_cluster = '" & tb_fund_cluster.Text & "', " &
                "payment_amount = '" & tb_payment.Text & "', purpose_one = '" & purpose_one &
                "', purpose_two = '" & purpose_two & "', bank_num_one = '" & bank_number_one &
                "', bank_name_one = '" & bank_name_one & "', payment_amount_one = '" &
                tb_bank_payment.Text & "', bank_num_two = '" & bank_number_two &
                "', bank_name_two = '" & bank_name_two & "', payment_amount_two = '" &
                tb_bank_payment_two.Text & "', is_updated = '1' where endorse_id = '" &
                endorse_id & "'"
            cmd = New MySqlCommand(query, class_connection.con)
            cmd.ExecuteNonQuery()
            class_connection.con.Close()

            ' Retrieve current cashier ID.
            class_connection.con.Open()
            query = "select setting_value from tbl_settings where setting = 'cashiering_officer'"
            cmd = New MySqlCommand(query, class_connection.con)
            reader = cmd.ExecuteReader()

            If reader.Read Then
                user_id_cashier = reader.GetString("setting_value")
            End If
            class_connection.con.Close()

            ' Update this notification to action taken.
            class_connection.con.Open()
            query = "update tbl_notifications Set action_taken = '1' where n_ID = '" & n_ID & "'"
            cmd = New MySqlCommand(query, class_connection.con)
            cmd.ExecuteNonQuery()
            class_connection.con.Close()

            ' Create notification to cashier.
            class_connection.con.Open()
            query = "insert into tbl_notifications(message_title, message_content, transaction_id, " &
                "user_id_notified, user_id_notifier, user_opened, date_created, action_taken, " &
                "message_type) Values('SOA with Order of Payment', 'SOA Serial No. " &
                tb_per_bill.Text & " has been endorsed. Click the button below to view.', '" &
                transact_ID.ToString & "', '" & user_id_cashier & "','" & user_id & "', '0', '" &
                current_datetime & "', '0', '8')"
            cmd = New MySqlCommand(query, class_connection.con)
            cmd.ExecuteNonQuery()
            class_connection.con.Close()

            ' Reload notification form.
            dashboard.uc_notifications.load_notifications()
            dashboard.uc_notifications.retrieve_number_of_unresolved()

            ' Hide finish panel after the process.
            pnl_finish.Visible = False

            Close()
        Catch ex As MySqlException
            class_connection.con.Close()
            MsgBox("Couldn't connect to server, the process is cancelled. Contact the IT Administrator " &
            "for assistance.", MsgBoxStyle.Exclamation, "NTC Region 10")
            export_success = False
        Catch ex As Exception
            class_connection.con.Close()
        MsgBox("Something went wrong. Couldn't process the data.", MsgBoxStyle.Exclamation,
            "NTC Region 10")
        export_success = False
        End Try
    End Sub

    Private Sub btn_soa_Click(sender As Object, e As EventArgs) Handles btn_soa.Click
        ' Open SOA form.
        frm_licenser_log.soa_id_transact = transact_ID.ToString
        frm_licenser_log.user_id = user_id
        frm_licenser_log.authority_level = authority_level
        frm_licenser_log.acct_mod = "1"
        frm_licenser_log.pnl_refresh.Visible = False
        frm_licenser_log.pb_approve_status.Visible = True
        frm_licenser_log.ShowDialog()
    End Sub

    Private Sub frm_payment_order_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        load_order_of_payment_details()

        Icon = My.Resources.ntc_material_logo
    End Sub

    Sub load_order_of_payment_details()
        Try
            ' Retrieve SOA details.
            class_connection.con.Open()
            query = "select serial_id, date_created from tbl_soa_transact join tbl_soa_details " &
                "on tbl_soa_details.soa_details_id = tbl_soa_transact.soa_details_id join " &
                "tbl_user on tbl_user.user_id = tbl_soa_transact.user_id where " &
                "tbl_soa_transact.soa_id = '" & transact_ID.ToString & "'"
            cmd = New MySqlCommand(query, class_connection.con)
            reader = cmd.ExecuteReader()

            If reader.Read Then
                tb_per_bill.Text = reader.GetString("serial_id")
                tb_dated.Text = reader.GetDateTime("date_created").ToString("MM-dd-yyyy")
            End If
            class_connection.con.Close()

            ' Retrieve accountant ID. 
            class_connection.con.Open()
            query = "select endorse_officer_id from tbl_soa_endorse where soa_id = '" &
                transact_ID.ToString & "'"
            cmd = New MySqlCommand(query, class_connection.con)
            reader = cmd.ExecuteReader()

            If reader.Read Then
                endorse_officer_id = reader.GetString("endorse_officer_id")
            End If
            class_connection.con.Close()

            ' Retrieve accountant's name.
            class_connection.con.Open()
            query = "select full_name from tbl_user where user_id = '" + endorse_officer_id + "'"
            cmd = New MySqlCommand(query, class_connection.con)
            reader = cmd.ExecuteReader()

            If reader.Read Then
                tb_accountant.Text = WebUtility.HtmlDecode(reader.GetString("full_name"))
            End If
            class_connection.con.Close()

            ' Retrieve payor's name and address. 
            class_connection.con.Open()
            query = "select name, address from tbl_soa_transact join tbl_soa_details on " &
                "tbl_soa_details.soa_details_id = tbl_soa_transact.soa_details_id join tbl_payor " &
                "on tbl_payor.payor_id = tbl_soa_details.payor_id where tbl_soa_transact.soa_id = '" &
                transact_ID.ToString & "'"
            cmd = New MySqlCommand(query, class_connection.con)
            reader = cmd.ExecuteReader()

            If reader.Read Then
                tb_payor.Text = WebUtility.HtmlDecode(reader.GetString("name"))
                tb_address.Text = WebUtility.HtmlDecode(reader.GetString("address"))
            End If
            class_connection.con.Close()

            ' Convert total to amount in words.
            tb_word_figure.Text = class_number_to_word.ConvertNumberToENG(tb_payment.Text)
        Catch ex As MySqlException
            class_connection.con.Close()
            MsgBox("Couldn't connect to server. Contact the IT Administrator for assistance.",
                MsgBoxStyle.Exclamation, "NTC Region 10")
            Close()
        Catch ex As Exception
            class_connection.con.Close()
            MsgBox("Something went wrong. Couldn't process the data.", MsgBoxStyle.Exclamation,
                "NTC Region 10")
            Close()
        End Try
    End Sub

    Private Sub btn_print_Click(sender As Object, e As EventArgs) Handles btn_print.Click
        Try
            ' Export Data to excel.
            Dim xlapp As New Excel.Application
            Dim xlWorksheet As Object
            Dim accountant As String

            ' Disable print button during the process
            btn_print.Enabled = False

            xlapp.Workbooks.Open(Application.StartupPath + "\OP.xlsx")
            xlapp.Sheets("Sheet1").Select()
            xlWorksheet = xlapp.Sheets("Sheet1")

            ' Copy all order of payment details to four copies.
            xlWorksheet.cells(4, 12).value = tb_serial_num.Text
            xlWorksheet.cells(4, 27).value = tb_serial_num.Text
            xlWorksheet.cells(33, 12).value = tb_serial_num.Text
            xlWorksheet.cells(33, 27).value = tb_serial_num.Text

            xlWorksheet.cells(5, 12).value = longDate
            xlWorksheet.cells(5, 27).value = longDate
            xlWorksheet.cells(34, 12).value = longDate
            xlWorksheet.cells(34, 27).value = longDate

            xlWorksheet.cells(7, 3).value = tb_fund_cluster.Text
            xlWorksheet.cells(7, 18).value = tb_fund_cluster.Text
            xlWorksheet.cells(36, 3).value = tb_fund_cluster.Text
            xlWorksheet.cells(36, 18).value = tb_fund_cluster.Text

            xlWorksheet.cells(12, 6).value = tb_payor.Text
            xlWorksheet.cells(12, 21).value = tb_payor.Text
            xlWorksheet.cells(41, 6).value = tb_payor.Text
            xlWorksheet.cells(41, 21).value = tb_payor.Text

            xlWorksheet.cells(14, 1).value = tb_address.Text
            xlWorksheet.cells(14, 16).value = tb_address.Text
            xlWorksheet.cells(43, 1).value = tb_address.Text
            xlWorksheet.cells(43, 16).value = tb_address.Text

            xlWorksheet.cells(16, 3).value = tb_word_figure.Text
            xlWorksheet.cells(16, 18).value = tb_word_figure.Text
            xlWorksheet.cells(45, 3).value = tb_word_figure.Text
            xlWorksheet.cells(45, 18).value = tb_word_figure.Text

            xlWorksheet.cells(16, 13).value = tb_payment.Text
            xlWorksheet.cells(16, 28).value = tb_payment.Text
            xlWorksheet.cells(45, 13).value = tb_payment.Text
            xlWorksheet.cells(45, 28).value = tb_payment.Text

            xlWorksheet.cells(17, 3).value = tb_purpose_one.Text
            xlWorksheet.cells(17, 18).value = tb_purpose_one.Text
            xlWorksheet.cells(46, 3).value = tb_purpose_one.Text
            xlWorksheet.cells(46, 18).value = tb_purpose_one.Text

            xlWorksheet.cells(18, 1).value = tb_purpose_two.Text
            xlWorksheet.cells(18, 16).value = tb_purpose_two.Text
            xlWorksheet.cells(47, 1).value = tb_purpose_two.Text
            xlWorksheet.cells(47, 16).value = tb_purpose_two.Text

            xlWorksheet.cells(20, 4).value = tb_per_bill.Text
            xlWorksheet.cells(20, 19).value = tb_per_bill.Text
            xlWorksheet.cells(49, 4).value = tb_per_bill.Text
            xlWorksheet.cells(49, 19).value = tb_per_bill.Text

            xlWorksheet.cells(20, 11).value = tb_dated.Text
            xlWorksheet.cells(20, 26).value = tb_dated.Text
            xlWorksheet.cells(49, 11).value = tb_dated.Text
            xlWorksheet.cells(49, 26).value = tb_dated.Text

            xlWorksheet.cells(24, 1).value = tb_bank_num.Text
            xlWorksheet.cells(24, 16).value = tb_bank_num.Text
            xlWorksheet.cells(53, 1).value = tb_bank_num.Text
            xlWorksheet.cells(53, 16).value = tb_bank_num.Text

            xlWorksheet.cells(24, 6).value = tb_bank_name.Text
            xlWorksheet.cells(24, 21).value = tb_bank_name.Text
            xlWorksheet.cells(53, 6).value = tb_bank_name.Text
            xlWorksheet.cells(53, 21).value = tb_bank_name.Text

            xlWorksheet.cells(24, 11).value = tb_bank_payment.Text
            xlWorksheet.cells(24, 26).value = tb_bank_payment.Text
            xlWorksheet.cells(53, 11).value = tb_bank_payment.Text
            xlWorksheet.cells(53, 26).value = tb_bank_payment.Text

            xlWorksheet.cells(25, 1).value = tb_bank_num_two.Text
            xlWorksheet.cells(25, 16).value = tb_bank_num_two.Text
            xlWorksheet.cells(54, 1).value = tb_bank_num_two.Text
            xlWorksheet.cells(54, 16).value = tb_bank_num_two.Text

            xlWorksheet.cells(25, 6).value = tb_bank_name_two.Text
            xlWorksheet.cells(25, 21).value = tb_bank_name_two.Text
            xlWorksheet.cells(54, 6).value = tb_bank_name_two.Text
            xlWorksheet.cells(54, 21).value = tb_bank_name_two.Text

            xlWorksheet.cells(25, 11).value = tb_bank_payment_two.Text
            xlWorksheet.cells(25, 26).value = tb_bank_payment_two.Text
            xlWorksheet.cells(54, 11).value = tb_bank_payment_two.Text
            xlWorksheet.cells(54, 26).value = tb_bank_payment_two.Text

            xlWorksheet.cells(26, 11).value = tb_bank_total.Text
            xlWorksheet.cells(26, 26).value = tb_bank_total.Text
            xlWorksheet.cells(55, 11).value = tb_bank_total.Text
            xlWorksheet.cells(55, 26).value = tb_bank_total.Text

            ' Set accountant name to uppercase.
            accountant = tb_accountant.Text
            xlWorksheet.cells(29, 5).value = accountant.ToUpper()
            xlWorksheet.cells(29, 20).value = accountant.ToUpper()
            xlWorksheet.cells(58, 5).value = accountant.ToUpper()
            xlWorksheet.cells(58, 20).value = accountant.ToUpper()

            download_signature_accountant()

            ' Insert signature of accountant.
            If enable_endorser_signature = "1" Then
                If File.Exists("C:\temp\" + signature_endorser) Then
                    xlWorksheet.Shapes.AddPicture("C:\temp\" + signature_endorser,
                        Microsoft.Office.Core.MsoTriState.msoFalse,
                        Microsoft.Office.Core.MsoTriState.msoCTrue, 266, 386, 120, 70)

                    xlWorksheet.Shapes.AddPicture("C:\temp\" + signature_endorser,
                        Microsoft.Office.Core.MsoTriState.msoFalse,
                        Microsoft.Office.Core.MsoTriState.msoCTrue, 986, 386, 120, 70)

                    xlWorksheet.Shapes.AddPicture("C:\temp\" + signature_endorser,
                        Microsoft.Office.Core.MsoTriState.msoFalse,
                        Microsoft.Office.Core.MsoTriState.msoCTrue, 266, 828, 120, 70)

                    xlWorksheet.Shapes.AddPicture("C:\temp\" + signature_endorser,
                        Microsoft.Office.Core.MsoTriState.msoFalse,
                        Microsoft.Office.Core.MsoTriState.msoCTrue, 986, 828, 120, 70)
                End If
            End If

            ' Set excel program to visible, maximize and on top.
            xlapp.Visible = True
            xlapp.Top = 1
            xlapp.ActiveWindow.WindowState = Excel.XlWindowState.xlMaximized

            ' Show finish panel and enable btn print after the process.
            pnl_finish.Visible = True
            btn_print.Enabled = True
        Catch ex As MySqlException
            MsgBox("Couldn't connect to server. Contact the IT Administrator for assistance.",
                MsgBoxStyle.Exclamation, "NTC Region 10")
            export_success = False
            btn_print.Enabled = True
            pnl_finish.Visible = False
        Catch ex As Exception
            MsgBox("Something went wrong. Couldn't process the data.", MsgBoxStyle.Exclamation,
                "NTC Region 10")
            export_success = False
            btn_print.Enabled = True
            pnl_finish.Visible = False
        End Try
    End Sub

    Sub download_signature_accountant()
        Try
            Dim WebClient As WebClient = New WebClient()

            class_connection.con.Open()
            query = "select signature as 'path', signature_name as 'name', enable_signature from " &
                "tbl_user where user_id = '" & endorse_officer_id & "'"
            cmd = New MySqlCommand(query, class_connection.con)
            reader = cmd.ExecuteReader()

            If reader.Read Then
                If reader.GetString("enable_signature") = "1" Then
                    enable_endorser_signature = reader.GetString("enable_signature")
                    signature_endorser = reader.GetString("name")

                    If Not Directory.Exists("C:\Temp") Then
                        Directory.CreateDirectory("C:\Temp")
                    End If

                    WebClient.Credentials = New NetworkCredential("ntc_10_server", "ntc_10_server",
                        "ftp://" + class_connection.ftpAddress)
                    WebClient.DownloadFile(WebUtility.HtmlDecode(reader.GetString("path")),
                        "C:\temp\" & reader.GetString("name"))
                End If
            End If
            class_connection.con.Close()
        Catch ex As WebException
            class_connection.con.Close()
        Catch ex As Exception
            class_connection.con.Close()
            MsgBox("Something went wrong. Couldn't process the data.", MsgBoxStyle.Exclamation,
                "NTC Region 10")
        End Try
    End Sub
End Class