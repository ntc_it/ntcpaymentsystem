﻿Imports MySql.Data.MySqlClient
Imports System.Net

Public Class frm_licenser_edit
    Dim query As String
    Dim cmd As MySqlCommand
    Dim reader As MySqlDataReader

    Dim particularOneID, particularTwoID, particularThreeID, mopID, result, transactID,
        yearDiffOne, yearDiffTwo, yearDiffThree, row_customize_no, column_customize_no,
        starting_series_soa, tabFocus As Integer
    Dim serial, last_counter, starting_digit_soa, process_id, service_id, particularID,
        soa_data_id, soa_details_id, soa_id, expirationDate As String
    Dim is_first_transact As Boolean = False
    Dim isPartTwoUsed As Boolean = False
    Dim isPartThreeUsed As Boolean = False
    Dim compute_sub_total, total As Double
    Dim MOP() As String = {"", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "",
        "", "", "", "", "", "", "", "", "", "", "", "", "", ""}
    Dim custom_value_array() As String = {"", "", "", "", "", "", "", "", "", "", "", "", "", "",
        "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", ""}
    Public isSetPayor, isSetType, isSetProcess, isSetService, isSetParticularName,
        isSetParticularMOD, isPaymentAdded, isSOAEdit As Boolean
    Public user_id, payor_id, soa_id_transact, authority_level As String

    Private Sub viewTransactionFrm_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Try
            Dim payment_desc_counter As Integer = 0
            Dim current_datatime = Date.Now.ToString("yyyy-MM-dd HH:mm")

            ' Load MOP records.
            load_MOP()

            ' Load MOP records to MOP array.
            class_connection.con.Open()
            query = "select mop_id, code, description from tbl_mode_of_payment"
            cmd = New MySqlCommand(query, class_connection.con)
            reader = cmd.ExecuteReader()

            While reader.Read
                MOP(payment_desc_counter) = WebUtility.HtmlDecode(reader.GetString("description"))
                payment_desc_counter += 1
            End While
            class_connection.con.Close()

            ' Show date if this form isn't opened as edit mode.
            If isSOAEdit <> True Then
                txt_date.Text = current_datatime
                txt_date.Visible = True
            End If

            ' Retrieve SOA details.
            class_connection.con.Open()
            query = "select soa_id, application_type, serial_id, process_id, service_id, " &
                "particular_id, soa_data_id, date_created, tbl_payor.payor_id, tbl_payor.name " &
                "as 'name', date_expires, tbl_soa_details.soa_details_id from tbl_soa_transact " &
                "join tbl_soa_details on tbl_soa_details.soa_details_id = " &
                "tbl_soa_transact.soa_details_id join tbl_payor on tbl_payor.payor_id = " &
                "tbl_soa_details.payor_id where tbl_soa_transact.soa_id = '" & soa_id_transact & "'"
            cmd = New MySqlCommand(query, class_connection.con)
            reader = cmd.ExecuteReader()

            If reader.Read Then
                cmb_type.SelectedItem = reader.GetString("application_type")
                tb_date_created.Text = reader.GetDateTime("date_created").ToShortDateString
                tb_serial_id.Text = reader.GetString("serial_id")
                process_id = reader.GetString("process_id")
                service_id = reader.GetString("service_id")
                particularID = reader.GetString("particular_id")
                soa_data_id = reader.GetString("soa_data_id")
                txtpayor.Text = WebUtility.HtmlDecode(reader.GetString("name"))
                payor_id = reader.GetString("payor_id")
                dtp_expiry_date.Value = reader.GetDateTime("date_expires").ToShortDateString
                soa_details_id = reader.GetString("soa_details_id")
                soa_id = reader.GetString("soa_id")
                tb_date_created.Visible = True
                lbl_serial.Visible = True
                tb_serial_id.Visible = True
                isSetPayor = True
                isSetProcess = True
                isSetService = True
                isSetType = True
                isSetParticularName = True
                isSetParticularMOD = True
                isPaymentAdded = True
            End If
            class_connection.con.Close()

            ' Retrieve process type.
            class_connection.con.Open()
            query = "select process_name from tbl_soa_transact join tbl_soa_details on " &
                "tbl_soa_details.soa_details_id = tbl_soa_transact.soa_details_id " &
                "join tbl_process_soa on tbl_process_soa.process_type_id = " &
                "tbl_soa_details.process_id where tbl_soa_transact.soa_id = '" &
                soa_id_transact & "'"
            cmd = New MySqlCommand(query, class_connection.con)
            reader = cmd.ExecuteReader()

            While reader.Read

                Dim process = reader.GetString("process_name")

                Select Case process
                    Case "DUP"
                        cb_process_dup.Checked = True
                    Case "MOD"
                        cb_process_mod.Checked = True
                    Case Else
                        cb_process_others.Checked = True
                End Select

            End While
            class_connection.con.Close()

            ' Retrieve service type.
            class_connection.con.Open()
            query = "select service_name, custom_value from tbl_soa_transact join " &
                "tbl_soa_details on tbl_soa_details.soa_details_id = " &
                "tbl_soa_transact.soa_details_id join tbl_service_soa on " &
                "tbl_service_soa.service_type_id = tbl_soa_details.service_id where " &
                "tbl_soa_transact.soa_id = '" & soa_id_transact & "'"
            cmd = New MySqlCommand(query, class_connection.con)
            reader = cmd.ExecuteReader()

            While reader.Read

            Dim service = reader.GetString("service_name")

            Select Case service
                    Case "CO"
                        cb_services_co.Checked = True
                    Case "CV"
                        cb_services_cv.Checked = True
                    Case "MS"
                        cb_services_ms.Checked = True
                    Case "MA"
                        cb_services_ma.Checked = True
                    Case "ROC"
                        cb_services_roc.Checked = True
                    Case Else
                        cb_services_others.Checked = True
                        tb_others.Visible = True
                        tb_others.Text = reader.GetString("custom_value")
                End Select

            End While
            class_connection.con.Close()

            ' Retrieve particular details.
            class_connection.con.Open()
            query = "select part_name, part_pos, part_covered_from, part_covered_to from " &
                "tbl_particular where part_id='" & particularID & "'"
            cmd = New MySqlCommand(query, class_connection.con)
            reader = cmd.ExecuteReader()

            While reader.Read()

                Dim particular = reader.GetString("part_pos")

                Select Case particular
                    Case "1"
                        tb_particular_one.Text = WebUtility.HtmlDecode(reader.GetString("part_name"))
                        dtp_p1_from.Enabled = True
                        dtp_p1_to.Enabled = True
                        dtp_p1_from.Text = reader.GetMySqlDateTime("part_covered_from")
                        dtp_p1_to.Text = reader.GetMySqlDateTime("part_covered_to")
                    Case "2"
                        cb_particular_two.Checked = True
                        tb_particular_two.Text = WebUtility.HtmlDecode(reader.GetString("part_name"))
                        dtp_p2_from.Enabled = True
                        dtp_p2_to.Enabled = True
                        dtp_p2_from.Text = reader.GetMySqlDateTime("part_covered_from")
                        dtp_p2_to.Text = reader.GetMySqlDateTime("part_covered_to")

                        isPartTwoUsed = True
                    Case Else
                        cb_particular_three.Checked = True
                        tb_particular_three.Text = WebUtility.HtmlDecode(reader.GetString("part_name"))
                        dtp_p3_from.Enabled = True
                        dtp_p3_to.Enabled = True
                        dtp_p3_from.Text = reader.GetMySqlDateTime("part_covered_from")
                        dtp_p3_to.Text = reader.GetMySqlDateTime("part_covered_to")

                        isPartThreeUsed = True
                End Select

            End While
            class_connection.con.Close()

            ' Initialize all datagridviews.
            initialize_datagridview_all()

            ' Display SOA data entries.
            class_connection.con.Open()
            query = "select tbl_soa_data.mop_id, code as 'Code', description as 'Description', " &
                "cat as 'Category', others_custom_value, p1_no_of_years as 'No. of Years 1', " &
                "p1_percent as 'Percent 1', p1_no_of_units as 'No. of Units 1', p1_fees as " &
                "'Fees 1', p2_no_of_years as 'No. of Years 2', p2_percent as 'Percent 2', " &
                "p2_no_of_units as 'No. of Units 2', p2_fees as 'Fees 2', p3_no_of_years as " &
                "'No. of Years 3', p3_percent as 'Percent 3', p3_no_of_units as 'No. of Units " &
                "3', p3_fees as 'Fees 3' from tbl_soa_transact join tbl_soa_details on " &
                "tbl_soa_details.soa_details_id = tbl_soa_transact.soa_details_id join " &
                "tbl_soa_data on tbl_soa_data.soa_data_id = tbl_soa_details.soa_data_id join " &
                "tbl_mode_of_payment on tbl_mode_of_payment.mop_id = tbl_soa_data.mop_id  " &
                "where tbl_soa_transact.soa_id = '" & soa_id_transact.ToString & "' order " &
                "by tbl_mode_of_payment.mop_id"
            cmd = New MySqlCommand(query, class_connection.con)
            reader = cmd.ExecuteReader()

            While reader.Read

                Dim value As Double = reader.GetDouble("Percent 1")
                Dim value2 As Double = reader.GetDouble("Percent 2")
                Dim value3 As Double = reader.GetDouble("Percent 3")
                Dim num, num2, num3, percentage, percentage2, percentage3 As Double
                Dim row() As String
                Dim description = WebUtility.HtmlDecode(reader.GetString("Description"))
                Dim custom_values = WebUtility.HtmlDecode(reader.GetString("others_custom_value"))
                Dim category = reader.GetString("Category")

                ' Convert double to percentage format.
                If Double.TryParse(value, num) And Double.TryParse(value2, num2) And
                    Double.TryParse(value3, num3) Then
                    percentage = num / 100
                    percentage2 = num2 / 100
                    percentage3 = num3 / 100
                End If

                compute_sub_total = (reader.GetDouble("No. of Years 1") * percentage *
                    reader.GetDouble("No. of Units 1") * reader.GetDouble("Fees 1")) +
                    (reader.GetDouble("No. of Years 2") * percentage2 *
                    reader.GetDouble("No. of Units 2") * reader.GetDouble("Fees 2")) +
                    (reader.GetDouble("No. of Years 3") * percentage3 *
                    reader.GetDouble("No. of Units 3") * reader.GetDouble("Fees 3"))
                compute_sub_total = FormatNumber(CDbl(compute_sub_total), 2)

                If custom_values = "" Then
                    ' If it doesn't have custom value.
                    row = {reader.GetString("Code"), description, reader.GetString("No. of Years 1"),
                        reader.GetString("Percent 1"), reader.GetString("No. of Units 1"),
                        FormatNumber(CDbl(reader.GetString("Fees 1")), 2),
                        reader.GetString("No. of Years 2"), reader.GetString("Percent 2"),
                        reader.GetString("No. of Units 2"),
                        FormatNumber(CDbl(reader.GetString("Fees 2")), 2),
                        reader.GetString("No. of Years 3"), reader.GetString("Percent 3"),
                        reader.GetString("No. of Units 3"),
                        FormatNumber(CDbl(reader.GetString("Fees 3")), 2),
                        compute_sub_total, reader.GetString("mop_id")}
                Else
                    ' If it does have custom value.
                    row = {reader.GetString("Code"), description & ": " & custom_values,
                        reader.GetString("No. of Years 1"), reader.GetString("Percent 1"),
                        reader.GetString("No. of Units 1"),
                        FormatNumber(CDbl(reader.GetString("Fees 1")), 2),
                        reader.GetString("No. of Years 2"), reader.GetString("Percent 2"),
                        reader.GetString("No. of Units 2"),
                        FormatNumber(CDbl(reader.GetString("Fees 2")), 2),
                        reader.GetString("No. of Years 3"), reader.GetString("Percent 3"),
                        reader.GetString("No. of Units 3"),
                        FormatNumber(CDbl(reader.GetString("Fees 3")), 2),
                        compute_sub_total, reader.GetString("mop_id")}
                    custom_value_array(reader.GetInt32("mop_id") - 1) = custom_values
                End If

                ' Add row to datagridview.
                Select Case category
                    Case "LICENSE"
                        dgv_license.Rows.Add(row)
                    Case "PERMIT"
                        dgv_permits.Rows.Add(row)
                    Case "AROC/ROC"
                        dgv_roc.Rows.Add(row)
                    Case Else
                        dgv_others.Rows.Add(row)
                End Select

            End While
            class_connection.con.Close()

            ' If particular 2 is used, make inputs editable.
            If isPartTwoUsed Then
                For Each row As DataGridViewRow In dgv_license.Rows
                    dgv_license.Columns(6).ReadOnly = False
                    dgv_license.Columns(7).ReadOnly = False
                    dgv_license.Columns(8).ReadOnly = False
                    dgv_license.Columns(9).ReadOnly = False
                Next

                For Each row As DataGridViewRow In dgv_permits.Rows
                    dgv_permits.Columns(6).ReadOnly = False
                    dgv_permits.Columns(7).ReadOnly = False
                    dgv_permits.Columns(8).ReadOnly = False
                    dgv_permits.Columns(9).ReadOnly = False
                Next

                For Each row As DataGridViewRow In dgv_roc.Rows
                    dgv_roc.Columns(6).ReadOnly = False
                    dgv_roc.Columns(7).ReadOnly = False
                    dgv_roc.Columns(8).ReadOnly = False
                    dgv_roc.Columns(9).ReadOnly = False
                Next

                For Each row As DataGridViewRow In dgv_others.Rows
                    dgv_others.Columns(6).ReadOnly = False
                    dgv_others.Columns(7).ReadOnly = False
                    dgv_others.Columns(8).ReadOnly = False
                    dgv_others.Columns(9).ReadOnly = False
                Next
            End If

            ' If particular 3 is used, make inputs editable.
            If isPartThreeUsed Then
                For Each row As DataGridViewRow In dgv_license.Rows
                    dgv_license.Columns(10).ReadOnly = False
                    dgv_license.Columns(11).ReadOnly = False
                    dgv_license.Columns(12).ReadOnly = False
                    dgv_license.Columns(13).ReadOnly = False
                Next

                For Each row As DataGridViewRow In dgv_permits.Rows
                    dgv_permits.Columns(10).ReadOnly = False
                    dgv_permits.Columns(11).ReadOnly = False
                    dgv_permits.Columns(12).ReadOnly = False
                    dgv_permits.Columns(13).ReadOnly = False
                Next

                For Each row As DataGridViewRow In dgv_roc.Rows
                    dgv_roc.Columns(10).ReadOnly = False
                    dgv_roc.Columns(11).ReadOnly = False
                    dgv_roc.Columns(12).ReadOnly = False
                    dgv_roc.Columns(13).ReadOnly = False
                Next

                For Each row As DataGridViewRow In dgv_others.Rows
                    dgv_others.Columns(10).ReadOnly = False
                    dgv_others.Columns(11).ReadOnly = False
                    dgv_others.Columns(12).ReadOnly = False
                    dgv_others.Columns(13).ReadOnly = False
                Next
            End If

            ' Retrieve number of MOP created in each tab.
            btn_tab_license.Text = "LICENSE(" & dgv_license.Rows.Count.ToString & ")"
            btn_tab_others.Text = "OTHERS(" & dgv_others.Rows.Count.ToString & ")"
            btn_tab_roc.Text = "AROC/ROC(" & dgv_roc.Rows.Count.ToString & ")"
            btn_tab_permits.Text = "PERMITS(" & dgv_permits.Rows.Count.ToString & ")"

            ' Add all sub-total to total.
            compute_total()

            Icon = My.Resources.ntc_material_logo
        Catch ex As MySqlException
        class_connection.con.Close()
        MsgBox("Something went wrong. Couldn't process the data.", MsgBoxStyle.Exclamation,
            "NTC Region 10")
        MsgBox(ex.Message)
        Close()
        Catch ex As Exception
        class_connection.con.Close()
        MsgBox("Something went wrong. Couldn't process the data.", MsgBoxStyle.Exclamation,
            "NTC Region 10")
        MsgBox(ex.Message)
        Close()
        End Try
    End Sub

    Sub load_MOP()
        ' Load MOP into all tabs' MOP comboboxes. 
        Try
            ' FOR LICENSES.
            class_connection.con.Open()
            query = "select mop_id, code, description from tbl_mode_of_payment where cat = " &
                "'LICENSE'"
            cmd = New MySqlCommand(query, class_connection.con)
            reader = cmd.ExecuteReader()

            While reader.Read
                cmb_payment_number_license.Items.Add(reader.GetString("mop_id"))
                cmb_license_items.Items.Add(reader.GetString("code") & " - " &
                    WebUtility.HtmlDecode(reader.GetString("description")))
            End While
            class_connection.con.Close()

            ' FOR PERMITS.
            class_connection.con.Open()
            query = "select mop_id, code, description from tbl_mode_of_payment where cat = " &
                "'PERMIT'"
            cmd = New MySqlCommand(query, class_connection.con)
            reader = cmd.ExecuteReader()

            While reader.Read
                cmb_payment_number_permits.Items.Add(reader.GetString("mop_id"))
                cmb_permits_items.Items.Add(reader.GetString("code") & " - " &
                    WebUtility.HtmlDecode(reader.GetString("description")))
            End While
            class_connection.con.Close()

            ' FOR ROC.
            class_connection.con.Open()
            query = "select mop_id, code, description from tbl_mode_of_payment where cat = " &
                "'AROC/ROC'"
            cmd = New MySqlCommand(query, class_connection.con)
            reader = cmd.ExecuteReader()

            While reader.Read
                cmb_payment_number_roc.Items.Add(reader.GetString("mop_id"))
                cmb_roc_items.Items.Add(reader.GetString("code") & " - " &
                    WebUtility.HtmlDecode(reader.GetString("description")))
            End While
            class_connection.con.Close()

            ' OTHER APPLICATION.
            class_connection.con.Open()
            query = "select mop_id, code, description from tbl_mode_of_payment where cat = " &
                "'OTHER'"
            cmd = New MySqlCommand(query, class_connection.con)
            reader = cmd.ExecuteReader()

            While reader.Read
                cmb_payment_number_others.Items.Add(reader.GetString("mop_id"))
                cmb_others_items.Items.Add(reader.GetString("code") & " - " &
                    WebUtility.HtmlDecode(reader.GetString("description")))
            End While
            class_connection.con.Close()
        Catch ex As MySqlException
            class_connection.con.Close()
            MsgBox("Something went wrong. Couldn't process the data.", MsgBoxStyle.Exclamation,
                "NTC Region 10")
        Catch ex As Exception
            class_connection.con.Close()
            MsgBox("Something went wrong. Couldn't process the data.", MsgBoxStyle.Exclamation,
                "NTC Region 10")
        End Try
    End Sub

    Sub initialize_datagridview_all()
        ' License datagridview.
        dgv_license.ColumnCount = 16
        dgv_license.Columns(0).Name = "Code"
        dgv_license.Columns(0).ReadOnly = True
        dgv_license.Columns(0).Width = 75
        dgv_license.Columns(1).Name = "Description"
        dgv_license.Columns(1).ReadOnly = True
        dgv_license.Columns(1).Width = 230
        dgv_license.Columns(2).Name = "No. Yrs."
        dgv_license.Columns(2).DefaultCellStyle.BackColor = Color.FromArgb(107, 117, 122)
        dgv_license.Columns(2).ReadOnly = False
        dgv_license.Columns(2).Width = 60
        dgv_license.Columns(3).Name = "%"
        dgv_license.Columns(3).DefaultCellStyle.BackColor = Color.FromArgb(107, 117, 122)
        dgv_license.Columns(3).ReadOnly = False
        dgv_license.Columns(3).Width = 60
        dgv_license.Columns(4).Name = "No. Units"
        dgv_license.Columns(4).DefaultCellStyle.BackColor = Color.FromArgb(107, 117, 122)
        dgv_license.Columns(4).ReadOnly = False
        dgv_license.Columns(4).Width = 60
        dgv_license.Columns(5).Name = "Fees"
        dgv_license.Columns(5).DefaultCellStyle.BackColor = Color.FromArgb(107, 117, 122)
        dgv_license.Columns(5).ReadOnly = False
        dgv_license.Columns(5).Width = 80
        dgv_license.Columns(6).Name = "No. Yrs"
        dgv_license.Columns(6).ReadOnly = True
        dgv_license.Columns(6).Width = 60
        dgv_license.Columns(7).Name = "%"
        dgv_license.Columns(7).ReadOnly = True
        dgv_license.Columns(7).Width = 60
        dgv_license.Columns(8).Name = "No. Units"
        dgv_license.Columns(8).ReadOnly = True
        dgv_license.Columns(8).Width = 60
        dgv_license.Columns(9).Name = "Fees"
        dgv_license.Columns(9).ReadOnly = True
        dgv_license.Columns(9).Width = 80
        dgv_license.Columns(10).Name = "No. Yrs"
        dgv_license.Columns(10).DefaultCellStyle.BackColor = Color.FromArgb(107, 117, 122)
        dgv_license.Columns(10).ReadOnly = True
        dgv_license.Columns(10).ReadOnly = True
        dgv_license.Columns(10).Width = 60
        dgv_license.Columns(11).Name = "%"
        dgv_license.Columns(11).DefaultCellStyle.BackColor = Color.FromArgb(107, 117, 122)
        dgv_license.Columns(11).ReadOnly = True
        dgv_license.Columns(11).Width = 60
        dgv_license.Columns(12).Name = "No. Units"
        dgv_license.Columns(12).DefaultCellStyle.BackColor = Color.FromArgb(107, 117, 122)
        dgv_license.Columns(12).ReadOnly = True
        dgv_license.Columns(12).Width = 60
        dgv_license.Columns(13).Name = "Fees"
        dgv_license.Columns(13).DefaultCellStyle.BackColor = Color.FromArgb(107, 117, 122)
        dgv_license.Columns(13).ReadOnly = True
        dgv_license.Columns(13).Width = 80
        dgv_license.Columns(14).Name = "Sub-total"
        dgv_license.Columns(14).Width = 100
        dgv_license.Columns(14).ReadOnly = True
        dgv_license.Columns(15).Name = "Payment ID"
        dgv_license.Columns(15).Visible = False

        ' Set all datagridview column unsortable.
        For Each column As DataGridViewColumn In dgv_license.Columns
            column.SortMode = DataGridViewColumnSortMode.NotSortable
        Next

        ' Others datagridview.
        dgv_others.ColumnCount = 16
        dgv_others.Columns(0).Name = "Code"
        dgv_others.Columns(0).ReadOnly = True
        dgv_others.Columns(0).Width = 75
        dgv_others.Columns(1).Name = "Description"
        dgv_others.Columns(1).ReadOnly = True
        dgv_others.Columns(1).Width = 230
        dgv_others.Columns(2).Name = "No. Yrs."
        dgv_others.Columns(2).DefaultCellStyle.BackColor = Color.FromArgb(107, 117, 122)
        dgv_others.Columns(2).ReadOnly = False
        dgv_others.Columns(2).Width = 60
        dgv_others.Columns(3).Name = "%"
        dgv_others.Columns(3).DefaultCellStyle.BackColor = Color.FromArgb(107, 117, 122)
        dgv_others.Columns(3).Width = 60
        dgv_others.Columns(4).Name = "No. Units"
        dgv_others.Columns(4).DefaultCellStyle.BackColor = Color.FromArgb(107, 117, 122)
        dgv_others.Columns(4).Width = 60
        dgv_others.Columns(5).Name = "Fees"
        dgv_others.Columns(5).DefaultCellStyle.BackColor = Color.FromArgb(107, 117, 122)
        dgv_others.Columns(5).Width = 80
        dgv_others.Columns(6).Name = "No. Yrs"
        dgv_others.Columns(6).ReadOnly = True
        dgv_others.Columns(6).Width = 60
        dgv_others.Columns(7).Name = "%"
        dgv_others.Columns(7).ReadOnly = True
        dgv_others.Columns(7).Width = 60
        dgv_others.Columns(8).Name = "No. Units"
        dgv_others.Columns(8).ReadOnly = True
        dgv_others.Columns(8).Width = 60
        dgv_others.Columns(9).Name = "Fees"
        dgv_others.Columns(9).ReadOnly = True
        dgv_others.Columns(9).Width = 80
        dgv_others.Columns(10).Name = "No. Yrs"
        dgv_others.Columns(10).DefaultCellStyle.BackColor = Color.FromArgb(107, 117, 122)
        dgv_others.Columns(10).ReadOnly = True
        dgv_others.Columns(10).Width = 60
        dgv_others.Columns(11).Name = "%"
        dgv_others.Columns(11).DefaultCellStyle.BackColor = Color.FromArgb(107, 117, 122)
        dgv_others.Columns(11).ReadOnly = True
        dgv_others.Columns(11).Width = 60
        dgv_others.Columns(12).Name = "No. Units"
        dgv_others.Columns(12).DefaultCellStyle.BackColor = Color.FromArgb(107, 117, 122)
        dgv_others.Columns(12).ReadOnly = True
        dgv_others.Columns(12).Width = 60
        dgv_others.Columns(13).Name = "Fees"
        dgv_others.Columns(13).DefaultCellStyle.BackColor = Color.FromArgb(107, 117, 122)
        dgv_others.Columns(13).ReadOnly = True
        dgv_others.Columns(13).Width = 80
        dgv_others.Columns(14).Name = "Sub-total"
        dgv_others.Columns(14).Width = 100
        dgv_others.Columns(14).ReadOnly = True
        dgv_others.Columns(15).Name = "Payment ID"
        dgv_others.Columns(15).Visible = False

        ' Set all datagridview column unsortable.
        For Each column As DataGridViewColumn In dgv_others.Columns
            column.SortMode = DataGridViewColumnSortMode.NotSortable
        Next

        ' ROC datagridview.
        dgv_roc.ColumnCount = 16
        dgv_roc.Columns(0).Name = "Code"
        dgv_roc.Columns(0).ReadOnly = True
        dgv_roc.Columns(0).Width = 75
        dgv_roc.Columns(1).Name = "Description"
        dgv_roc.Columns(1).ReadOnly = True
        dgv_roc.Columns(1).Width = 230
        dgv_roc.Columns(2).Name = "No. Yrs."
        dgv_roc.Columns(2).DefaultCellStyle.BackColor = Color.FromArgb(107, 117, 122)
        dgv_roc.Columns(2).ReadOnly = False
        dgv_roc.Columns(2).Width = 60
        dgv_roc.Columns(3).Name = "%"
        dgv_roc.Columns(3).DefaultCellStyle.BackColor = Color.FromArgb(107, 117, 122)
        dgv_roc.Columns(3).Width = 60
        dgv_roc.Columns(4).Name = "No. Units"
        dgv_roc.Columns(4).DefaultCellStyle.BackColor = Color.FromArgb(107, 117, 122)
        dgv_roc.Columns(4).Width = 60
        dgv_roc.Columns(5).Name = "Fees"
        dgv_roc.Columns(5).DefaultCellStyle.BackColor = Color.FromArgb(107, 117, 122)
        dgv_roc.Columns(5).Width = 80
        dgv_roc.Columns(6).Name = "No. Yrs"
        dgv_roc.Columns(6).ReadOnly = True
        dgv_roc.Columns(6).Width = 60
        dgv_roc.Columns(7).Name = "%"
        dgv_roc.Columns(7).ReadOnly = True
        dgv_roc.Columns(7).Width = 60
        dgv_roc.Columns(8).Name = "No. Units"
        dgv_roc.Columns(8).ReadOnly = True
        dgv_roc.Columns(8).Width = 60
        dgv_roc.Columns(9).Name = "Fees"
        dgv_roc.Columns(9).ReadOnly = True
        dgv_roc.Columns(9).Width = 80
        dgv_roc.Columns(10).Name = "No. Yrs"
        dgv_roc.Columns(10).DefaultCellStyle.BackColor = Color.FromArgb(107, 117, 122)
        dgv_roc.Columns(10).ReadOnly = True
        dgv_roc.Columns(10).Width = 60
        dgv_roc.Columns(11).Name = "%"
        dgv_roc.Columns(11).DefaultCellStyle.BackColor = Color.FromArgb(107, 117, 122)
        dgv_roc.Columns(11).ReadOnly = True
        dgv_roc.Columns(11).Width = 60
        dgv_roc.Columns(12).Name = "No. Units"
        dgv_roc.Columns(12).DefaultCellStyle.BackColor = Color.FromArgb(107, 117, 122)
        dgv_roc.Columns(12).ReadOnly = True
        dgv_roc.Columns(12).Width = 60
        dgv_roc.Columns(13).Name = "Fees"
        dgv_roc.Columns(13).DefaultCellStyle.BackColor = Color.FromArgb(107, 117, 122)
        dgv_roc.Columns(13).ReadOnly = True
        dgv_roc.Columns(13).Width = 80
        dgv_roc.Columns(14).Name = "Sub-total"
        dgv_roc.Columns(14).Width = 100
        dgv_roc.Columns(14).ReadOnly = True
        dgv_roc.Columns(15).Name = "Payment ID"
        dgv_roc.Columns(15).Visible = False

        ' Set all datagridview column unsortable.
        For Each column As DataGridViewColumn In dgv_roc.Columns
            column.SortMode = DataGridViewColumnSortMode.NotSortable
        Next

        ' Permit datagridview.
        dgv_permits.ColumnCount = 16
        dgv_permits.Columns(0).Name = "Code"
        dgv_permits.Columns(0).ReadOnly = True
        dgv_permits.Columns(0).Width = 75
        dgv_permits.Columns(1).Name = "Description"
        dgv_permits.Columns(1).ReadOnly = True
        dgv_permits.Columns(1).Width = 230
        dgv_permits.Columns(2).Name = "No. Yrs."
        dgv_permits.Columns(2).DefaultCellStyle.BackColor = Color.FromArgb(107, 117, 122)
        dgv_permits.Columns(2).ReadOnly = False
        dgv_permits.Columns(2).Width = 60
        dgv_permits.Columns(3).Name = "%"
        dgv_permits.Columns(3).DefaultCellStyle.BackColor = Color.FromArgb(107, 117, 122)
        dgv_permits.Columns(3).Width = 60
        dgv_permits.Columns(4).Name = "No. Units"
        dgv_permits.Columns(4).DefaultCellStyle.BackColor = Color.FromArgb(107, 117, 122)
        dgv_permits.Columns(4).Width = 60
        dgv_permits.Columns(5).Name = "Fees"
        dgv_permits.Columns(5).DefaultCellStyle.BackColor = Color.FromArgb(107, 117, 122)
        dgv_permits.Columns(5).Width = 80
        dgv_permits.Columns(6).Name = "No. Yrs"
        dgv_permits.Columns(6).ReadOnly = True
        dgv_permits.Columns(6).Width = 60
        dgv_permits.Columns(7).Name = "%"
        dgv_permits.Columns(7).ReadOnly = True
        dgv_permits.Columns(7).Width = 60
        dgv_permits.Columns(8).Name = "No. Units"
        dgv_permits.Columns(8).ReadOnly = True
        dgv_permits.Columns(8).Width = 60
        dgv_permits.Columns(9).Name = "Fees"
        dgv_permits.Columns(9).ReadOnly = True
        dgv_permits.Columns(9).Width = 80
        dgv_permits.Columns(10).Name = "No. Yrs"
        dgv_permits.Columns(10).DefaultCellStyle.BackColor = Color.FromArgb(107, 117, 122)
        dgv_permits.Columns(10).ReadOnly = True
        dgv_permits.Columns(10).Width = 60
        dgv_permits.Columns(11).Name = "%"
        dgv_permits.Columns(11).DefaultCellStyle.BackColor = Color.FromArgb(107, 117, 122)
        dgv_permits.Columns(11).ReadOnly = True
        dgv_permits.Columns(11).Width = 60
        dgv_permits.Columns(12).Name = "No. Units"
        dgv_permits.Columns(12).DefaultCellStyle.BackColor = Color.FromArgb(107, 117, 122)
        dgv_permits.Columns(12).ReadOnly = True
        dgv_permits.Columns(12).Width = 60
        dgv_permits.Columns(13).Name = "Fees"
        dgv_permits.Columns(13).DefaultCellStyle.BackColor = Color.FromArgb(107, 117, 122)
        dgv_permits.Columns(13).ReadOnly = True
        dgv_permits.Columns(13).Width = 80
        dgv_permits.Columns(14).Name = "Sub-total"
        dgv_permits.Columns(14).Width = 100
        dgv_permits.Columns(14).ReadOnly = True
        dgv_permits.Columns(15).Name = "Payment ID"
        dgv_permits.Columns(15).Visible = False

        ' Set all datagridview column unsortable.
        For Each column As DataGridViewColumn In dgv_permits.Columns
            column.SortMode = DataGridViewColumnSortMode.NotSortable
        Next
    End Sub

    Sub initialize_license_table()
        ' Load license's column settings.
        dgv_license.ColumnCount = 16
        dgv_license.Columns(0).Name = "Code"
        dgv_license.Columns(0).ReadOnly = True
        dgv_license.Columns(0).Width = 75
        dgv_license.Columns(1).Name = "Description"
        dgv_license.Columns(1).ReadOnly = True
        dgv_license.Columns(1).Width = 230
        dgv_license.Columns(2).Name = "No. Yrs."
        dgv_license.Columns(2).DefaultCellStyle.BackColor = Color.FromArgb(107, 117, 122)
        dgv_license.Columns(2).ReadOnly = False
        dgv_license.Columns(2).Width = 60
        dgv_license.Columns(3).Name = "%"
        dgv_license.Columns(3).DefaultCellStyle.BackColor = Color.FromArgb(107, 117, 122)
        dgv_license.Columns(3).Width = 60
        dgv_license.Columns(4).Name = "No. Units"
        dgv_license.Columns(4).DefaultCellStyle.BackColor = Color.FromArgb(107, 117, 122)
        dgv_license.Columns(4).Width = 60
        dgv_license.Columns(5).Name = "Fees"
        dgv_license.Columns(5).DefaultCellStyle.BackColor = Color.FromArgb(107, 117, 122)
        dgv_license.Columns(5).Width = 80
        dgv_license.Columns(6).Name = "No. Yrs"
        dgv_license.Columns(6).ReadOnly = True
        dgv_license.Columns(6).Width = 60
        dgv_license.Columns(7).Name = "%"
        dgv_license.Columns(7).Width = 60
        dgv_license.Columns(8).Name = "No. Units"
        dgv_license.Columns(8).Width = 60
        dgv_license.Columns(9).Name = "Fees"
        dgv_license.Columns(9).Width = 80
        dgv_license.Columns(10).Name = "No. Yrs"
        dgv_license.Columns(10).DefaultCellStyle.BackColor = Color.FromArgb(107, 117, 122)
        dgv_license.Columns(10).ReadOnly = True
        dgv_license.Columns(10).Width = 60
        dgv_license.Columns(11).Name = "%"
        dgv_license.Columns(11).DefaultCellStyle.BackColor = Color.FromArgb(107, 117, 122)
        dgv_license.Columns(11).Width = 60
        dgv_license.Columns(12).Name = "No. Units"
        dgv_license.Columns(12).DefaultCellStyle.BackColor = Color.FromArgb(107, 117, 122)
        dgv_license.Columns(12).Width = 60
        dgv_license.Columns(13).Name = "Fees"
        dgv_license.Columns(13).DefaultCellStyle.BackColor = Color.FromArgb(107, 117, 122)
        dgv_license.Columns(13).Width = 80
        dgv_license.Columns(14).Name = "Sub-total"
        dgv_license.Columns(14).Width = 100
        dgv_license.Columns(14).ReadOnly = True
        dgv_license.Columns(15).Name = "Payment ID"
        dgv_license.Columns(15).Visible = False
    End Sub

    Private Sub btn_license_add_Click(sender As Object, e As EventArgs) Handles btn_license_add.Click
        If cmb_license_items.Text <> "" Then
            Dim isItemExisted As Boolean = False
            Dim row As String()
            Dim MOP_id = cmb_payment_number_license.Text
            Dim item_license = cmb_license_items.Text.Substring(14)

            ' Load license column settings.
            initialize_license_table()

            ' Check if selected SOA data entry to be added is already existed in the datagridview. 
            If dgv_license.Rows.Count <> 0 Then
                For i As Integer = 0 To dgv_license.Rows.Count - 1 Step +1
                    If dgv_license.Rows(i).Cells(15).Value.ToString() = MOP_id Then
                        isItemExisted = True
                    End If
                Next
            End If

            ' Check if particular 2 and/or 3 are ticked.
            If Not cb_particular_two.Checked And Not cb_particular_three.Checked Then
                dgv_license.Columns(6).ReadOnly = True
                dgv_license.Columns(7).ReadOnly = True
                dgv_license.Columns(8).ReadOnly = True
                dgv_license.Columns(9).ReadOnly = True
                dgv_license.Columns(10).ReadOnly = True
                dgv_license.Columns(11).ReadOnly = True
                dgv_license.Columns(12).ReadOnly = True
                dgv_license.Columns(13).ReadOnly = True
                yearDiffOne = DateDiff(DateInterval.Year, dtp_p1_from.Value, dtp_p1_to.Value)

                If isItemExisted = False Then
                    row = New String() {cmb_license_items.Text.Substring(0, 11),
                        item_license, yearDiffOne, "100", "0", "0.00", "0", "0", "0",
                        "0.00", "0", "0", "0", "0.00", "0.00", MOP_id}
                    dgv_license.Rows.Add(row)
                End If
            ElseIf cb_particular_two.Checked And Not cb_particular_three.Checked Then
                dgv_license.Columns(6).ReadOnly = False
                dgv_license.Columns(7).ReadOnly = False
                dgv_license.Columns(8).ReadOnly = False
                dgv_license.Columns(9).ReadOnly = False
                dgv_license.Columns(10).ReadOnly = True
                dgv_license.Columns(11).ReadOnly = True
                dgv_license.Columns(12).ReadOnly = True
                dgv_license.Columns(13).ReadOnly = True
                yearDiffOne = DateDiff(DateInterval.Year, dtp_p1_from.Value, dtp_p1_to.Value)
                yearDiffTwo = DateDiff(DateInterval.Year, dtp_p2_from.Value, dtp_p2_to.Value)

                If isItemExisted = False Then
                    row = New String() {cmb_license_items.Text.Substring(0, 11),
                        item_license, yearDiffOne, "100", "0", "0.00", yearDiffTwo,
                        "100", "0", "0.00", "0", "0", "0", "0.00", "0.00", MOP_id}
                    dgv_license.Rows.Add(row)
                End If
            Else
                dgv_license.Columns(6).ReadOnly = False
                dgv_license.Columns(7).ReadOnly = False
                dgv_license.Columns(8).ReadOnly = False
                dgv_license.Columns(9).ReadOnly = False
                dgv_license.Columns(10).ReadOnly = False
                dgv_license.Columns(11).ReadOnly = False
                dgv_license.Columns(12).ReadOnly = False
                dgv_license.Columns(13).ReadOnly = False
                yearDiffOne = DateDiff(DateInterval.Year, dtp_p1_from.Value, dtp_p1_to.Value)
                yearDiffTwo = DateDiff(DateInterval.Year, dtp_p2_from.Value, dtp_p2_to.Value)
                yearDiffThree = DateDiff(DateInterval.Year, dtp_p3_from.Value, dtp_p3_to.Value)

                If isItemExisted = False Then
                    row = New String() {cmb_license_items.Text.Substring(0, 11),
                        item_license, yearDiffOne, "100", "0", "0.00", yearDiffTwo,
                        "100", "0", "0.00", yearDiffThree, "100", "0", "0.00", "0.00",
                        MOP_id}
                    dgv_license.Rows.Add(row)
                End If
            End If

            ' Set all column unsortable.
            For Each column As DataGridViewColumn In dgv_license.Columns
                column.SortMode = DataGridViewColumnSortMode.NotSortable
            Next

            ' Set true if any MOP is added to SOA.
            isPaymentAdded = True

            ' Update number of items in license tab.
            btn_tab_license.Text = "LICENSE(" + dgv_license.Rows.Count.ToString + ")"
        End If
    End Sub

    Sub initialize_others_table()
        ' Load other's column settings.
        dgv_others.ColumnCount = 16
        dgv_others.Columns(0).Name = "Code"
        dgv_others.Columns(0).ReadOnly = True
        dgv_others.Columns(0).Width = 75
        dgv_others.Columns(1).Name = "Description"
        dgv_others.Columns(1).ReadOnly = True
        dgv_others.Columns(1).Width = 230
        dgv_others.Columns(2).Name = "No. Yrs."
        dgv_others.Columns(2).DefaultCellStyle.BackColor = Color.FromArgb(107, 117, 122)
        dgv_others.Columns(2).ReadOnly = False
        dgv_others.Columns(2).Width = 60
        dgv_others.Columns(3).Name = "%"
        dgv_others.Columns(3).DefaultCellStyle.BackColor = Color.FromArgb(107, 117, 122)
        dgv_others.Columns(3).Width = 60
        dgv_others.Columns(4).Name = "No. Units"
        dgv_others.Columns(4).DefaultCellStyle.BackColor = Color.FromArgb(107, 117, 122)
        dgv_others.Columns(4).Width = 60
        dgv_others.Columns(5).Name = "Fees"
        dgv_others.Columns(5).DefaultCellStyle.BackColor = Color.FromArgb(107, 117, 122)
        dgv_others.Columns(5).Width = 80
        dgv_others.Columns(6).Name = "No. Yrs"
        dgv_others.Columns(6).ReadOnly = True
        dgv_others.Columns(6).Width = 60
        dgv_others.Columns(7).Name = "%"
        dgv_others.Columns(7).Width = 60
        dgv_others.Columns(8).Name = "No. Units"
        dgv_others.Columns(8).Width = 60
        dgv_others.Columns(9).Name = "Fees"
        dgv_others.Columns(9).Width = 80
        dgv_others.Columns(10).Name = "No. Yrs"
        dgv_others.Columns(10).DefaultCellStyle.BackColor = Color.FromArgb(107, 117, 122)
        dgv_others.Columns(10).ReadOnly = True
        dgv_others.Columns(10).Width = 60
        dgv_others.Columns(11).Name = "%"
        dgv_others.Columns(11).DefaultCellStyle.BackColor = Color.FromArgb(107, 117, 122)
        dgv_others.Columns(11).Width = 60
        dgv_others.Columns(12).Name = "No. Units"
        dgv_others.Columns(12).DefaultCellStyle.BackColor = Color.FromArgb(107, 117, 122)
        dgv_others.Columns(12).Width = 60
        dgv_others.Columns(13).Name = "Fees"
        dgv_others.Columns(13).DefaultCellStyle.BackColor = Color.FromArgb(107, 117, 122)
        dgv_others.Columns(13).Width = 80
        dgv_others.Columns(14).Name = "Sub-total"
        dgv_others.Columns(14).Width = 100
        dgv_others.Columns(14).ReadOnly = True
        dgv_others.Columns(15).Name = "Payment ID"
        dgv_others.Columns(15).Visible = False
    End Sub

    Private Sub btn_others_add_Click(sender As Object, e As EventArgs) Handles btn_others_add.Click
        If cmb_others_items.Text <> "" Then
            Dim isItemExisted As Boolean = False
            Dim row As String()
            Dim MOP_id = cmb_payment_number_others.Text
            Dim item_other = cmb_others_items.Text.Substring(14)

            ' Load other's column settings.
            initialize_others_table()

            ' Check if selected SOA data entry to be added is already existed in the datagridview. 
            If dgv_others.Rows.Count <> 0 Then
                For i As Integer = 0 To dgv_others.Rows.Count - 1 Step +1
                    If dgv_others.Rows(i).Cells(15).Value.ToString() = MOP_id Then
                        isItemExisted = True
                    End If
                Next
            End If

            ' Check if particular 2 and/or 3 are ticked.
            If Not cb_particular_two.Checked And Not cb_particular_three.Checked Then
                dgv_others.Columns(6).ReadOnly = True
                dgv_others.Columns(7).ReadOnly = True
                dgv_others.Columns(8).ReadOnly = True
                dgv_others.Columns(9).ReadOnly = True
                dgv_others.Columns(10).ReadOnly = True
                dgv_others.Columns(11).ReadOnly = True
                dgv_others.Columns(12).ReadOnly = True
                dgv_others.Columns(13).ReadOnly = True
                yearDiffOne = DateDiff(DateInterval.Year, dtp_p1_from.Value, dtp_p1_to.Value)

                If isItemExisted = False Then
                    row = New String() {cmb_others_items.Text.Substring(0, 11),
                        item_other, yearDiffOne, "100", "0", "0.00", "0", "0", "0",
                        "0.00", "0", "0", "0", "0.00", "0.00", MOP_id}
                    dgv_others.Rows.Add(row)
                End If
            ElseIf cb_particular_two.Checked And Not cb_particular_three.Checked Then
                dgv_others.Columns(6).ReadOnly = False
                dgv_others.Columns(7).ReadOnly = False
                dgv_others.Columns(8).ReadOnly = False
                dgv_others.Columns(9).ReadOnly = False
                dgv_others.Columns(10).ReadOnly = True
                dgv_others.Columns(11).ReadOnly = True
                dgv_others.Columns(12).ReadOnly = True
                dgv_others.Columns(13).ReadOnly = True
                yearDiffOne = DateDiff(DateInterval.Year, dtp_p1_from.Value, dtp_p1_to.Value)
                yearDiffTwo = DateDiff(DateInterval.Year, dtp_p2_from.Value, dtp_p2_to.Value)

                If isItemExisted = False Then
                    row = New String() {cmb_others_items.Text.Substring(0, 11),
                        item_other, yearDiffOne, "100", "0", "0.00", yearDiffTwo,
                        "100", "0", "0.00", "0", "0", "0", "0.00", "0.00", MOP_id}
                    dgv_others.Rows.Add(row)
                End If
            Else
                dgv_others.Columns(6).ReadOnly = False
                dgv_others.Columns(7).ReadOnly = False
                dgv_others.Columns(8).ReadOnly = False
                dgv_others.Columns(9).ReadOnly = False
                dgv_others.Columns(10).ReadOnly = False
                dgv_others.Columns(11).ReadOnly = False
                dgv_others.Columns(12).ReadOnly = False
                dgv_others.Columns(13).ReadOnly = False
                yearDiffOne = DateDiff(DateInterval.Year, dtp_p1_from.Value, dtp_p1_to.Value)
                yearDiffTwo = DateDiff(DateInterval.Year, dtp_p2_from.Value, dtp_p2_to.Value)
                yearDiffThree = DateDiff(DateInterval.Year, dtp_p3_from.Value, dtp_p3_to.Value)

                If isItemExisted = False Then
                    row = New String() {cmb_others_items.Text.Substring(0, 11),
                        item_other, yearDiffOne, "100", "0", "0.00", yearDiffTwo,
                        "100", "0", "0.00", yearDiffThree, "100", "0", "0.00", "0.00",
                        MOP_id}
                    dgv_others.Rows.Add(row)
                End If
            End If

            ' Set all column unsortable.
            For Each column As DataGridViewColumn In dgv_others.Columns
                column.SortMode = DataGridViewColumnSortMode.NotSortable
            Next

            ' Set true if any MOP is added to SOA.
            isPaymentAdded = True

            ' Update number of items in other tab.
            btn_tab_others.Text = "OTHERS(" + dgv_others.Rows.Count.ToString + ")"
        End If
    End Sub

    Sub initialize_ROC_table()
        ' Load ROC table settings.
        dgv_roc.ColumnCount = 16
        dgv_roc.Columns(0).Name = "Code"
        dgv_roc.Columns(0).ReadOnly = True
        dgv_roc.Columns(0).Width = 75
        dgv_roc.Columns(1).Name = "Description"
        dgv_roc.Columns(1).ReadOnly = True
        dgv_roc.Columns(1).Width = 230
        dgv_roc.Columns(2).Name = "No. Yrs."
        dgv_roc.Columns(2).DefaultCellStyle.BackColor = Color.FromArgb(107, 117, 122)
        dgv_roc.Columns(2).ReadOnly = False
        dgv_roc.Columns(2).Width = 60
        dgv_roc.Columns(3).Name = "%"
        dgv_roc.Columns(3).DefaultCellStyle.BackColor = Color.FromArgb(107, 117, 122)
        dgv_roc.Columns(3).Width = 60
        dgv_roc.Columns(4).Name = "No. Units"
        dgv_roc.Columns(4).DefaultCellStyle.BackColor = Color.FromArgb(107, 117, 122)
        dgv_roc.Columns(4).Width = 60
        dgv_roc.Columns(5).Name = "Fees"
        dgv_roc.Columns(5).DefaultCellStyle.BackColor = Color.FromArgb(107, 117, 122)
        dgv_roc.Columns(5).Width = 80
        dgv_roc.Columns(6).Name = "No. Yrs"
        dgv_roc.Columns(6).ReadOnly = True
        dgv_roc.Columns(6).Width = 60
        dgv_roc.Columns(7).Name = "%"
        dgv_roc.Columns(7).Width = 60
        dgv_roc.Columns(8).Name = "No. Units"
        dgv_roc.Columns(8).Width = 60
        dgv_roc.Columns(9).Name = "Fees"
        dgv_roc.Columns(9).Width = 80
        dgv_roc.Columns(10).Name = "No. Yrs"
        dgv_roc.Columns(10).DefaultCellStyle.BackColor = Color.FromArgb(107, 117, 122)
        dgv_roc.Columns(10).ReadOnly = True
        dgv_roc.Columns(10).Width = 60
        dgv_roc.Columns(11).Name = "%"
        dgv_roc.Columns(11).DefaultCellStyle.BackColor = Color.FromArgb(107, 117, 122)
        dgv_roc.Columns(11).Width = 60
        dgv_roc.Columns(12).Name = "No. Units"
        dgv_roc.Columns(12).DefaultCellStyle.BackColor = Color.FromArgb(107, 117, 122)
        dgv_roc.Columns(12).Width = 60
        dgv_roc.Columns(13).Name = "Fees"
        dgv_roc.Columns(13).DefaultCellStyle.BackColor = Color.FromArgb(107, 117, 122)
        dgv_roc.Columns(13).Width = 80
        dgv_roc.Columns(14).Name = "Sub-total"
        dgv_roc.Columns(14).Width = 100
        dgv_roc.Columns(14).ReadOnly = True
        dgv_roc.Columns(15).Name = "Payment ID"
        dgv_roc.Columns(15).Visible = False
    End Sub

    Private Sub btn_roc_add_Click(sender As Object, e As EventArgs) Handles btn_roc_add.Click
        If cmb_roc_items.Text <> "" Then
            Dim isItemExisted As Boolean = False
            Dim row As String()
            Dim MOP_id = cmb_payment_number_roc.Text
            Dim item_ROC = cmb_roc_items.Text.Substring(14)

            ' Load ROC table settings.
            initialize_ROC_table()

            ' Check if selected SOA data entry to be added is already existed in the datagridview. 
            If dgv_roc.Rows.Count <> 0 Then
                For i As Integer = 0 To dgv_roc.Rows.Count - 1 Step +1
                    If dgv_roc.Rows(i).Cells(15).Value.ToString() = MOP_id Then
                        isItemExisted = True
                    End If
                Next
            End If

            ' Check if particular 2 and/or 3 are ticked.
            If Not cb_particular_two.Checked And Not cb_particular_three.Checked Then
                dgv_roc.Columns(6).ReadOnly = True
                dgv_roc.Columns(7).ReadOnly = True
                dgv_roc.Columns(8).ReadOnly = True
                dgv_roc.Columns(9).ReadOnly = True
                dgv_roc.Columns(10).ReadOnly = True
                dgv_roc.Columns(11).ReadOnly = True
                dgv_roc.Columns(12).ReadOnly = True
                dgv_roc.Columns(13).ReadOnly = True
                yearDiffOne = DateDiff(DateInterval.Year, dtp_p1_from.Value, dtp_p1_to.Value)

                If isItemExisted = False Then
                    row = New String() {cmb_roc_items.Text.Substring(0, 11),
                        item_ROC, yearDiffOne, "100", "0", "0.00", "0", "0", "0",
                        "0.00", "0", "0", "0", "0.00", "0.00", MOP_id}
                    dgv_roc.Rows.Add(row)
                End If
            ElseIf cb_particular_two.Checked And Not cb_particular_three.Checked Then
                dgv_roc.Columns(6).ReadOnly = False
                dgv_roc.Columns(7).ReadOnly = False
                dgv_roc.Columns(8).ReadOnly = False
                dgv_roc.Columns(9).ReadOnly = False
                dgv_roc.Columns(10).ReadOnly = True
                dgv_roc.Columns(11).ReadOnly = True
                dgv_roc.Columns(12).ReadOnly = True
                dgv_roc.Columns(13).ReadOnly = True
                yearDiffOne = DateDiff(DateInterval.Year, dtp_p1_from.Value, dtp_p1_to.Value)
                yearDiffTwo = DateDiff(DateInterval.Year, dtp_p2_from.Value, dtp_p2_to.Value)

                If isItemExisted = False Then
                    row = New String() {cmb_roc_items.Text.Substring(0, 11),
                        item_ROC, yearDiffOne, "100", "0", "0.00", yearDiffTwo,
                        "100", "0", "0.00", "0", "0", "0", "0.00", "0.00", MOP_id}
                    dgv_roc.Rows.Add(row)
                End If
            Else
                dgv_roc.Columns(6).ReadOnly = False
                dgv_roc.Columns(7).ReadOnly = False
                dgv_roc.Columns(8).ReadOnly = False
                dgv_roc.Columns(9).ReadOnly = False
                dgv_roc.Columns(10).ReadOnly = False
                dgv_roc.Columns(11).ReadOnly = False
                dgv_roc.Columns(12).ReadOnly = False
                dgv_roc.Columns(13).ReadOnly = False
                yearDiffOne = DateDiff(DateInterval.Year, dtp_p1_from.Value, dtp_p1_to.Value)
                yearDiffTwo = DateDiff(DateInterval.Year, dtp_p2_from.Value, dtp_p2_to.Value)
                yearDiffThree = DateDiff(DateInterval.Year, dtp_p3_from.Value, dtp_p3_to.Value)

                If isItemExisted = False Then
                    row = New String() {cmb_roc_items.Text.Substring(0, 11),
                        item_ROC, yearDiffOne, "100", "0", "0.00", yearDiffTwo,
                        "100", "0", "0.00", yearDiffThree, "100", "0", "0.00", "0.00",
                        MOP_id}
                    dgv_roc.Rows.Add(row)
                End If
            End If

            ' Set all columns unsortable.
            For Each column As DataGridViewColumn In dgv_roc.Columns
                column.SortMode = DataGridViewColumnSortMode.NotSortable
            Next

            ' Set true if any MOP is added to SOA.
            isPaymentAdded = True

            ' Update number of items in ROC tab.
            btn_tab_roc.Text = "AROC/ROC(" + dgv_roc.Rows.Count.ToString + ")"
        End If
    End Sub

    Sub initialize_permits_table()
        dgv_permits.ColumnCount = 16
        dgv_permits.Columns(0).Name = "Code"
        dgv_permits.Columns(0).ReadOnly = True
        dgv_permits.Columns(0).Width = 75
        dgv_permits.Columns(1).Name = "Description"
        dgv_permits.Columns(1).ReadOnly = True
        dgv_permits.Columns(1).Width = 230
        dgv_permits.Columns(2).Name = "No. Yrs."
        dgv_permits.Columns(2).DefaultCellStyle.BackColor = Color.FromArgb(107, 117, 122)
        dgv_permits.Columns(2).ReadOnly = False
        dgv_permits.Columns(2).Width = 60
        dgv_permits.Columns(3).Name = "%"
        dgv_permits.Columns(3).DefaultCellStyle.BackColor = Color.FromArgb(107, 117, 122)
        dgv_permits.Columns(3).Width = 60
        dgv_permits.Columns(4).Name = "No. Units"
        dgv_permits.Columns(4).DefaultCellStyle.BackColor = Color.FromArgb(107, 117, 122)
        dgv_permits.Columns(4).Width = 60
        dgv_permits.Columns(5).Name = "Fees"
        dgv_permits.Columns(5).DefaultCellStyle.BackColor = Color.FromArgb(107, 117, 122)
        dgv_permits.Columns(5).Width = 80
        dgv_permits.Columns(6).Name = "No. Yrs"
        dgv_permits.Columns(6).ReadOnly = True
        dgv_permits.Columns(6).Width = 60
        dgv_permits.Columns(7).Name = "%"
        dgv_permits.Columns(7).Width = 60
        dgv_permits.Columns(8).Name = "No. Units"
        dgv_permits.Columns(8).Width = 60
        dgv_permits.Columns(9).Name = "Fees"
        dgv_permits.Columns(9).Width = 80
        dgv_permits.Columns(10).Name = "No. Yrs"
        dgv_permits.Columns(10).DefaultCellStyle.BackColor = Color.FromArgb(107, 117, 122)
        dgv_permits.Columns(10).ReadOnly = True
        dgv_permits.Columns(10).Width = 60
        dgv_permits.Columns(11).Name = "%"
        dgv_permits.Columns(11).DefaultCellStyle.BackColor = Color.FromArgb(107, 117, 122)
        dgv_permits.Columns(11).Width = 60
        dgv_permits.Columns(12).Name = "No. Units"
        dgv_permits.Columns(12).DefaultCellStyle.BackColor = Color.FromArgb(107, 117, 122)
        dgv_permits.Columns(12).Width = 60
        dgv_permits.Columns(13).Name = "Fees"
        dgv_permits.Columns(13).DefaultCellStyle.BackColor = Color.FromArgb(107, 117, 122)
        dgv_permits.Columns(13).Width = 80
        dgv_permits.Columns(14).Name = "Sub-total"
        dgv_permits.Columns(14).Width = 100
        dgv_permits.Columns(14).ReadOnly = True
        dgv_permits.Columns(15).Name = "Payment ID"
        dgv_permits.Columns(15).Visible = False
    End Sub

    Private Sub btn_permits_add_Click(sender As Object, e As EventArgs) Handles btn_permits_add.Click
        If cmb_permits_items.Text <> "" Then
            Dim isItemExisted As Boolean = False
            Dim row As String()
            Dim MOP_id = cmb_payment_number_permits.Text
            Dim item_permit = cmb_permits_items.Text.Substring(14)

            ' Load ROC table settings.
            initialize_permits_table()

            ' Check if selected SOA data entry to be added is already existed in the datagridview. 
            If dgv_permits.Rows.Count <> 0 Then
                For i As Integer = 0 To dgv_permits.Rows.Count - 1 Step +1
                    If dgv_permits.Rows(i).Cells(15).Value.ToString() = MOP_id Then
                        isItemExisted = True
                    End If
                Next
            End If

            ' Check if particular 2 and/or 3 are ticked.
            If Not cb_particular_two.Checked And Not cb_particular_three.Checked Then
                dgv_permits.Columns(6).ReadOnly = True
                dgv_permits.Columns(7).ReadOnly = True
                dgv_permits.Columns(8).ReadOnly = True
                dgv_permits.Columns(9).ReadOnly = True
                dgv_permits.Columns(10).ReadOnly = True
                dgv_permits.Columns(11).ReadOnly = True
                dgv_permits.Columns(12).ReadOnly = True
                dgv_permits.Columns(13).ReadOnly = True
                yearDiffOne = DateDiff(DateInterval.Year, dtp_p1_from.Value, dtp_p1_to.Value)

                If isItemExisted = False Then
                    row = New String() {cmb_permits_items.Text.Substring(0, 11),
                        item_permit, yearDiffOne, "100", "0", "0.00", "0", "0",
                        "0", "0.00", "0", "0", "0", "0.00", "0.00", MOP_id}
                    dgv_permits.Rows.Add(row)
                End If
            ElseIf cb_particular_two.Checked And Not cb_particular_three.Checked Then
                dgv_permits.Columns(6).ReadOnly = False
                dgv_permits.Columns(7).ReadOnly = False
                dgv_permits.Columns(8).ReadOnly = False
                dgv_permits.Columns(9).ReadOnly = False
                dgv_permits.Columns(10).ReadOnly = True
                dgv_permits.Columns(11).ReadOnly = True
                dgv_permits.Columns(12).ReadOnly = True
                dgv_permits.Columns(13).ReadOnly = True
                yearDiffOne = DateDiff(DateInterval.Year, dtp_p1_from.Value, dtp_p1_to.Value)
                yearDiffTwo = DateDiff(DateInterval.Year, dtp_p2_from.Value, dtp_p2_to.Value)

                If isItemExisted = False Then
                    row = New String() {cmb_permits_items.Text.Substring(0, 11),
                        item_permit, yearDiffOne, "100", "0", "0.00", yearDiffTwo,
                        "100", "0", "0.00", "0", "0", "0", "0.00", "0.00", MOP_id}
                    dgv_permits.Rows.Add(row)
                End If
            Else
                dgv_permits.Columns(6).ReadOnly = False
                dgv_permits.Columns(7).ReadOnly = False
                dgv_permits.Columns(8).ReadOnly = False
                dgv_permits.Columns(9).ReadOnly = False
                dgv_permits.Columns(10).ReadOnly = False
                dgv_permits.Columns(11).ReadOnly = False
                dgv_permits.Columns(12).ReadOnly = False
                dgv_permits.Columns(13).ReadOnly = False
                yearDiffOne = DateDiff(DateInterval.Year, dtp_p1_from.Value, dtp_p1_to.Value)
                yearDiffTwo = DateDiff(DateInterval.Year, dtp_p2_from.Value, dtp_p2_to.Value)
                yearDiffThree = DateDiff(DateInterval.Year, dtp_p3_from.Value, dtp_p3_to.Value)

                If isItemExisted = False Then
                    row = New String() {cmb_permits_items.Text.Substring(0, 11),
                        item_permit, yearDiffOne, "100", "0", "0.00", yearDiffTwo,
                        "100", "0", "0.00", yearDiffThree, "100", "0", "0.00", "0.00",
                        MOP_id}
                    dgv_permits.Rows.Add(row)
                End If
            End If

            ' Set all columns unsortable.
            For Each column As DataGridViewColumn In dgv_permits.Columns
                column.SortMode = DataGridViewColumnSortMode.NotSortable
            Next
            ' Set true if any MOP is added to SOA.
            isPaymentAdded = True

            ' Update number of items in permit tab.
            btn_tab_permits.Text = "PERMITS(" + dgv_permits.Rows.Count.ToString + ")"
        End If
    End Sub

    Sub compute_total()
        Dim i As Integer
        Dim total As Double = 0

        ' Add all license sub-total to total.
        For i = 0 To dgv_license.Rows.Count - 1 Step +1
            Try
                total += dgv_license.Rows(i).Cells("Sub-total").Value
            Catch ex As Exception
                total += 0
            End Try
        Next

        ' Add all permits sub-total to total.
        For i = 0 To dgv_permits.Rows.Count - 1 Step +1
            Try
                total += dgv_permits.Rows(i).Cells("Sub-total").Value
            Catch ex As Exception
                total += 0
            End Try
        Next

        ' Add all ROC sub-total to total.
        For i = 0 To dgv_roc.Rows.Count - 1 Step +1
            Try
                total += dgv_roc.Rows(i).Cells("Sub-total").Value
            Catch ex As Exception
                total += 0
            End Try
        Next

        ' Add all Others sub-total to total.
        For i = 0 To dgv_others.Rows.Count - 1 Step +1
            Try
                total += dgv_others.Rows(i).Cells("Sub-total").Value
            Catch ex As Exception
                total += 0
            End Try
        Next

        ' Show total.
        tb_total.Text = FormatNumber(CDbl(total), 2)
    End Sub

    Private Sub dgv_license_CellValueChanged(sender As Object, e As DataGridViewCellEventArgs) _
        Handles dgv_license.CellValueChanged
        compute_row_subtotal_license()
    End Sub

    Sub compute_row_subtotal_license()
        Dim row_counter As Integer

        For row_counter = 0 To dgv_license.Rows.Count - 1 Step +1
            Dim value As String = dgv_license.Rows(row_counter).Cells(3).Value
            Dim value2 As String = dgv_license.Rows(row_counter).Cells(7).Value
            Dim value3 As String = dgv_license.Rows(row_counter).Cells(11).Value
            Dim num, num2, num3 As Double
            Dim percentage, percentage2, percentage3 As Double
            Dim column_counter As Integer = 2
            Dim column_value

            ' Convert double to percentage.
            If Double.TryParse(value, num) And Double.TryParse(value2, num2) And
                Double.TryParse(value3, num3) Then
                percentage = num / 100
                percentage2 = num2 / 100
                percentage3 = num3 / 100
            End If

            ' Set zero value for empty cells.
            While column_counter <= 14
                If dgv_license.Rows(row_counter).Cells(column_counter).Value = Nothing Then
                    Select Case column_counter
                        Case 5, 9, 13
                            dgv_license.Rows(row_counter).Cells(column_counter).Value = "0.00"
                        Case Else
                            dgv_license.Rows(row_counter).Cells(column_counter).Value = "0"
                    End Select
                Else
                    Select Case column_counter
                        Case 5, 9, 13
                            column_value = dgv_license.Rows(row_counter).Cells(column_counter).Value

                            dgv_license.Rows(row_counter).Cells(column_counter).Value =
                            FormatNumber(CDbl(column_value), 2)
                    End Select
                End If

                column_counter += 1
            End While

            ' Compute and set this row's sub-total.
            dgv_license.Rows(row_counter).Cells("Sub-total").Value =
                FormatNumber(CDbl(Double.Parse(dgv_license.Rows(row_counter).Cells(2).Value) *
                Double.Parse(percentage) * Double.Parse(dgv_license.Rows(row_counter).Cells(4).Value) *
                Double.Parse(dgv_license.Rows(row_counter).Cells(5).Value)) +
                (Double.Parse(dgv_license.Rows(row_counter).Cells(6).Value) * Double.Parse(percentage2) *
                Double.Parse(dgv_license.Rows(row_counter).Cells(8).Value) *
                Double.Parse(dgv_license.Rows(row_counter).Cells(9).Value)) +
                (Double.Parse(dgv_license.Rows(row_counter).Cells(10).Value) * Double.Parse(percentage3) *
                Double.Parse(dgv_license.Rows(row_counter).Cells(12).Value) *
                Double.Parse(dgv_license.Rows(row_counter).Cells(13).Value)), 2)
        Next

        compute_total()
    End Sub

    Private Sub dgv_permits_CellValueChanged(sender As Object, e As DataGridViewCellEventArgs) _
        Handles dgv_permits.CellValueChanged
        compute_row_subtotal_permits()
    End Sub

    Sub compute_row_subtotal_permits()
        Dim row_counter As Integer

        For row_counter = 0 To dgv_permits.Rows.Count - 1 Step +1
            Dim value As String = dgv_permits.Rows(row_counter).Cells(3).Value
            Dim value2 As String = dgv_permits.Rows(row_counter).Cells(7).Value
            Dim value3 As String = dgv_permits.Rows(row_counter).Cells(11).Value
            Dim num, num2, num3 As Double
            Dim percentage, percentage2, percentage3 As Double
            Dim column_counter As Integer = 2
            Dim column_value

            ' Convert double to percentage.
            If Double.TryParse(value, num) And Double.TryParse(value2, num2) And
                Double.TryParse(value3, num3) Then
                percentage = num / 100
                percentage2 = num2 / 100
                percentage3 = num3 / 100
            End If

            ' Set zero value for empty cells.
            While column_counter <= 14
                If dgv_permits.Rows(row_counter).Cells(column_counter).Value = Nothing Then
                    Select Case column_counter
                        Case 5, 9, 13
                            dgv_permits.Rows(row_counter).Cells(column_counter).Value = "0.00"
                        Case Else
                            dgv_permits.Rows(row_counter).Cells(column_counter).Value = "0"
                    End Select
                Else
                    Select Case column_counter
                        Case 5, 9, 13
                            column_value = dgv_permits.Rows(row_counter).Cells(column_counter).Value

                            dgv_permits.Rows(row_counter).Cells(column_counter).Value =
                            FormatNumber(CDbl(column_value), 2)
                    End Select
                End If

                column_counter += 1
            End While

            ' Compute and set this row's sub-total.
            dgv_permits.Rows(row_counter).Cells("Sub-total").Value =
                FormatNumber(CDbl(Double.Parse(dgv_permits.Rows(row_counter).Cells(2).Value) *
                Double.Parse(percentage) * Double.Parse(dgv_permits.Rows(row_counter).Cells(4).Value) _
                * Double.Parse(dgv_permits.Rows(row_counter).Cells(5).Value)) +
                (Double.Parse(dgv_permits.Rows(row_counter).Cells(6).Value) * Double.Parse(percentage2) _
                * Double.Parse(dgv_permits.Rows(row_counter).Cells(8).Value) *
                Double.Parse(dgv_permits.Rows(row_counter).Cells(9).Value)) +
                (Double.Parse(dgv_permits.Rows(row_counter).Cells(10).Value) *
                Double.Parse(percentage3) * Double.Parse(dgv_permits.Rows(row_counter).Cells(12).Value) _
                * Double.Parse(dgv_permits.Rows(row_counter).Cells(13).Value)), 2)
        Next

        compute_total()
    End Sub

    Private Sub dgv_others_CellValueChanged(sender As Object, e As DataGridViewCellEventArgs) _
        Handles dgv_others.CellValueChanged
        compute_row_subtotal_others()
    End Sub

    Sub compute_row_subtotal_others()
        Dim i As Integer

        For i = 0 To dgv_others.Rows.Count - 1 Step +1
            Dim value As String = dgv_others.Rows(i).Cells(3).Value
            Dim value2 As String = dgv_others.Rows(i).Cells(7).Value
            Dim value3 As String = dgv_others.Rows(i).Cells(11).Value
            Dim num, num2, num3 As Double
            Dim percentage, percentage2, percentage3 As Double
            Dim column_counter As Integer = 2
            Dim column_value

            ' Convert double to percentage.
            If Double.TryParse(value, num) And Double.TryParse(value2, num2) And
                Double.TryParse(value3, num3) Then
                percentage = num / 100
                percentage2 = num2 / 100
                percentage3 = num3 / 100
            End If

            ' Set zero value for empty cells.
            While column_counter <= 14
                If dgv_others.Rows(i).Cells(column_counter).Value = Nothing Then
                    Select Case column_counter
                        Case 5, 9, 13
                            dgv_others.Rows(i).Cells(column_counter).Value = "0.00"
                        Case Else
                            dgv_others.Rows(i).Cells(column_counter).Value = "0"
                    End Select
                Else
                    Select Case column_counter
                        Case 5, 9, 13
                            column_value = dgv_others.Rows(i).Cells(column_counter).Value

                            dgv_others.Rows(i).Cells(column_counter).Value =
                            FormatNumber(CDbl(column_value), 2)
                    End Select
                End If

                column_counter += 1
            End While

            ' Compute and set this row's sub-total.
            dgv_others.Rows(i).Cells("Sub-total").Value =
                FormatNumber(CDbl(Double.Parse(dgv_others.Rows(i).Cells(2).Value) *
                Double.Parse(percentage) * Double.Parse(dgv_others.Rows(i).Cells(4).Value) _
                * Double.Parse(dgv_others.Rows(i).Cells(5).Value)) +
                (Double.Parse(dgv_others.Rows(i).Cells(6).Value) * Double.Parse(percentage2) _
                * Double.Parse(dgv_others.Rows(i).Cells(8).Value) *
                Double.Parse(dgv_others.Rows(i).Cells(9).Value)) +
                (Double.Parse(dgv_others.Rows(i).Cells(10).Value) * Double.Parse(percentage3) _
                * Double.Parse(dgv_others.Rows(i).Cells(12).Value) *
                Double.Parse(dgv_others.Rows(i).Cells(13).Value)), 2)
        Next

        compute_total()
    End Sub

    Private Sub dgv_roc_CellValueChanged(sender As Object, e As DataGridViewCellEventArgs) _
        Handles dgv_roc.CellValueChanged
        compute_row_subtotal_ROC()
    End Sub

    Sub compute_row_subtotal_ROC()
        Dim i As Integer

        For i = 0 To dgv_roc.Rows.Count - 1 Step +1
            Dim value As String = dgv_roc.Rows(i).Cells(3).Value
            Dim value2 As String = dgv_roc.Rows(i).Cells(7).Value
            Dim value3 As String = dgv_roc.Rows(i).Cells(11).Value
            Dim num, num2, num3 As Double
            Dim percentage, percentage2, percentage3 As Double
            Dim column_counter As Integer = 2
            Dim column_value

            ' Convert double to percentage.
            If Double.TryParse(value, num) And Double.TryParse(value2, num2) And
                Double.TryParse(value3, num3) Then
                percentage = num / 100
                percentage2 = num2 / 100
                percentage3 = num3 / 100
            End If

            ' Set zero value for empty cells.
            While column_counter <= 14
                If dgv_roc.Rows(i).Cells(column_counter).Value = Nothing Then
                    Select Case column_counter
                        Case 5, 9, 13
                            dgv_roc.Rows(i).Cells(column_counter).Value = "0.00"
                        Case Else
                            dgv_roc.Rows(i).Cells(column_counter).Value = "0"
                    End Select
                Else
                    Select Case column_counter
                        Case 5, 9, 13
                            column_value = dgv_roc.Rows(i).Cells(column_counter).Value

                            dgv_roc.Rows(i).Cells(column_counter).Value =
                            FormatNumber(CDbl(column_value), 2)
                    End Select
                End If

                column_counter += 1
            End While

            ' Compute and set this row's sub-total.
            dgv_roc.Rows(i).Cells("Sub-total").Value =
                FormatNumber(CDbl(Double.Parse(dgv_roc.Rows(i).Cells(2).Value) *
                Double.Parse(percentage) * Double.Parse(dgv_roc.Rows(i).Cells(4).Value) *
                Double.Parse(dgv_roc.Rows(i).Cells(5).Value)) +
                (Double.Parse(dgv_roc.Rows(i).Cells(6).Value) * Double.Parse(percentage2) _
                * Double.Parse(dgv_roc.Rows(i).Cells(8).Value) *
                Double.Parse(dgv_roc.Rows(i).Cells(9).Value)) +
                (Double.Parse(dgv_roc.Rows(i).Cells(10).Value) * Double.Parse(percentage3) _
                * Double.Parse(dgv_roc.Rows(i).Cells(12).Value) *
                Double.Parse(dgv_roc.Rows(i).Cells(13).Value)), 2)
        Next

        compute_total()
    End Sub

    Sub get_year_difference_part_1()
        ' Compute for number of years between two dates.
        yearDiffOne = DateDiff(DateInterval.Year, dtp_p1_from.Value, dtp_p1_to.Value)

        For i As Integer = 0 To dgv_license.RowCount - 1
            dgv_license.Rows(i).Cells(2).Value = yearDiffOne
        Next

        For i As Integer = 0 To dgv_permits.RowCount - 1
            dgv_permits.Rows(i).Cells(2).Value = yearDiffOne
        Next

        For i As Integer = 0 To dgv_roc.RowCount - 1
            dgv_roc.Rows(i).Cells(2).Value = yearDiffOne
        Next

        For i As Integer = 0 To dgv_others.RowCount - 1
            dgv_others.Rows(i).Cells(2).Value = yearDiffOne
        Next
    End Sub

    Sub get_year_difference_part_2()
        ' Compute for number of years between two dates.
        yearDiffTwo = DateDiff(DateInterval.Year, dtp_p2_from.Value, dtp_p2_to.Value)

        For i As Integer = 0 To dgv_license.RowCount - 1
            dgv_license.Rows(i).Cells(6).Value = yearDiffTwo
        Next

        For i As Integer = 0 To dgv_permits.RowCount - 1
            dgv_permits.Rows(i).Cells(6).Value = yearDiffTwo
        Next

        For i As Integer = 0 To dgv_roc.RowCount - 1
            dgv_roc.Rows(i).Cells(6).Value = yearDiffTwo
        Next

        For i As Integer = 0 To dgv_others.RowCount - 1
            dgv_others.Rows(i).Cells(6).Value = yearDiffTwo
        Next
    End Sub

    Sub get_year_difference_part_3()
        ' Compute for number of years between two dates.
        yearDiffThree = DateDiff(DateInterval.Year, dtp_p3_from.Value, dtp_p3_to.Value)

        For i As Integer = 0 To dgv_license.RowCount - 1
            dgv_license.Rows(i).Cells(10).Value = yearDiffThree
        Next

        For i As Integer = 0 To dgv_permits.RowCount - 1
            dgv_permits.Rows(i).Cells(10).Value = yearDiffThree
        Next

        For i As Integer = 0 To dgv_roc.RowCount - 1
            dgv_roc.Rows(i).Cells(10).Value = yearDiffThree
        Next

        For i As Integer = 0 To dgv_others.RowCount - 1
            dgv_others.Rows(i).Cells(10).Value = yearDiffThree
        Next
    End Sub

    Private Sub dtp_p1_from_ValueChanged(sender As Object, e As EventArgs) Handles dtp_p1_from.ValueChanged
        get_year_difference_part_1()
        update_expiration_date()
    End Sub

    Private Sub dtp_p1_to_ValueChanged(sender As Object, e As EventArgs) Handles dtp_p1_to.ValueChanged
        get_year_difference_part_1()
    End Sub

    Private Sub dtp_p2_from_ValueChanged(sender As Object, e As EventArgs) Handles dtp_p2_from.ValueChanged
        get_year_difference_part_2()
        update_expiration_date()
    End Sub

    Private Sub dtp_p2_to_ValueChanged(sender As Object, e As EventArgs) Handles dtp_p2_to.ValueChanged
        get_year_difference_part_2()
        update_expiration_date()
    End Sub

    Private Sub dtp_p3_from_ValueChanged(sender As Object, e As EventArgs) Handles dtp_p3_from.ValueChanged
        get_year_difference_part_3()
        update_expiration_date()
    End Sub

    Private Sub dtp_p3_to_ValueChanged(sender As Object, e As EventArgs) Handles dtp_p3_to.ValueChanged
        get_year_difference_part_3()
        update_expiration_date()
    End Sub

    Private Sub tb_particular_one_TextChanged(sender As Object, e As EventArgs) Handles _
        tb_particular_one.TextChanged
        ' If particular one is filled, enable use of date range and set particular name is true.
        If tb_particular_one.Text <> "" Then
            isSetParticularName = True
            dtp_p1_from.Enabled = True
            dtp_p1_to.Enabled = True
        Else
            isSetParticularName = False
            dtp_p1_from.Enabled = False
            dtp_p1_to.Enabled = False
        End If

        update_expiration_date()
    End Sub

    Private Sub tb_particular_two_TextChanged(sender As Object, e As EventArgs) Handles _
        tb_particular_two.TextChanged
        ' If particular two is filled, enable use of date range and set particular name is true.
        If tb_particular_two.Text <> "" Then
            dtp_p2_from.Enabled = True
            dtp_p2_to.Enabled = True
        Else
            dtp_p2_from.Enabled = False
            dtp_p2_to.Enabled = False
        End If

        update_expiration_date()
    End Sub

    Private Sub tb_particular_three_TextChanged(sender As Object, e As EventArgs) Handles _
        tb_particular_three.TextChanged
        ' If particular two is filled, enable use of date range and set particular name is true.

        If tb_particular_three.Text <> "" Then
            dtp_p3_from.Enabled = True
            dtp_p3_to.Enabled = True
        Else
            dtp_p3_from.Enabled = False
            dtp_p3_to.Enabled = False
        End If

        update_expiration_date()
    End Sub

    Private Sub tsmi_cms_license_remove_entry_Click(sender As Object, e As EventArgs) Handles _
        tsmi_cms_license_remove_entry.Click
        ' Remove the selected row from datagridview. 
        For Each row As DataGridViewRow In dgv_license.SelectedRows
            dgv_license.Rows.Remove(row)
        Next

        ' Show updated number of items created.
        If dgv_license.Rows.Count <> 0 Then
            btn_tab_license.Text = "LICENSE(" + dgv_license.Rows.Count.ToString + ")"
        Else
            btn_tab_license.Text = "LICENSE"
        End If

        compute_row_subtotal_license()
    End Sub

    Private Sub tsmi_cms_permits_remove_entry_Click(sender As Object, e As EventArgs) Handles _
        tsmi_cms_permits_remove_entry.Click
        ' Remove the selected row from datagridview. 
        For Each row As DataGridViewRow In dgv_permits.SelectedRows
            dgv_permits.Rows.Remove(row)
        Next

        ' Show updated number of items created.
        If dgv_permits.Rows.Count <> 0 Then
            btn_tab_permits.Text = "PERMITS(" + dgv_permits.Rows.Count.ToString + ")"
        Else
            btn_tab_permits.Text = "PERMITS"
        End If

        compute_row_subtotal_permits()
    End Sub

    Private Sub tsmi_cms_roc_remove_entry_Click(sender As Object, e As EventArgs) Handles _
        tsmi_cms_roc_remove_entry.Click
        ' Remove the selected row from datagridview. 
        For Each row As DataGridViewRow In dgv_roc.SelectedRows
            dgv_roc.Rows.Remove(row)
        Next

        ' Show updated number of items created.
        If dgv_roc.Rows.Count <> 0 Then
            btn_tab_roc.Text = "AROC/ROC(" + dgv_roc.Rows.Count.ToString + ")"
        Else
            btn_tab_roc.Text = "AROC/ROC"
        End If

        compute_row_subtotal_ROC()
    End Sub

    Private Sub tsmi_cms_others_remove_entry_Click(sender As Object, e As EventArgs) Handles _
        tsmi_cms_others_remove_entry.Click
        ' Remove the selected row from datagridview. 
        For Each row As DataGridViewRow In dgv_others.SelectedRows
            dgv_others.Rows.Remove(row)
        Next

        ' Show updated number of items created.
        If dgv_others.Rows.Count <> 0 Then
            btn_tab_others.Text = "OTHERS(" + dgv_others.Rows.Count.ToString + ")"
        Else
            btn_tab_others.Text = "OTHERS"
        End If

        compute_row_subtotal_others()
    End Sub

    Private Sub cmb_type_SelectionChangeCommitted(sender As Object, e As EventArgs) Handles _
        cmb_type.SelectionChangeCommitted
        ' Check if transaction type is set.
        If cmb_type.Text <> "" Then
            isSetType = True
        Else
            isSetType = False
        End If
    End Sub

    Private Sub cb_process_mod_CheckedChanged(sender As Object, e As EventArgs) Handles _
        cb_process_mod.CheckedChanged
        process_set()
    End Sub

    Private Sub cb_process_dup_CheckedChanged(sender As Object, e As EventArgs) Handles _
        cb_process_dup.CheckedChanged
        process_set()
    End Sub

    Private Sub cb_process_others_CheckedChanged(sender As Object, e As EventArgs) Handles _
        cb_process_others.CheckedChanged
        process_set()
    End Sub

    Sub process_set()
        ' Check if process type is set.
        If cb_process_dup.Checked Or cb_process_mod.Checked Or cb_process_others.Checked Then
            isSetProcess = True
        Else
            isSetProcess = False
        End If
    End Sub

    Private Sub cb_services_co_CheckedChanged(sender As Object, e As EventArgs) Handles _
        cb_services_co.CheckedChanged
        service_set()
    End Sub

    Private Sub cb_services_cv_CheckedChanged(sender As Object, e As EventArgs) Handles _
        cb_services_cv.CheckedChanged
        service_set()
    End Sub

    Private Sub cb_services_ma_CheckedChanged(sender As Object, e As EventArgs) Handles _
        cb_services_ma.CheckedChanged
        service_set()
    End Sub

    Private Sub cb_services_ms_CheckedChanged(sender As Object, e As EventArgs) Handles _
        cb_services_ms.CheckedChanged
        service_set()
    End Sub

    Private Sub cb_services_roc_CheckedChanged(sender As Object, e As EventArgs) Handles _
        cb_services_roc.CheckedChanged
        service_set()
    End Sub

    Private Sub cb_services_others_CheckedChanged(sender As Object, e As EventArgs) Handles _
        cb_services_others.CheckedChanged
        ' Show input for other if ticked. Hide other input otherwise.
        If cb_services_others.Checked = True Then
            lbl_other.Visible = True
            tb_others.Visible = True
        Else
            lbl_other.Visible = False
            tb_others.Visible = False
            tb_others.Text = ""
        End If

        service_set()
    End Sub

    Sub service_set()
        isSetService = False

        ' Check if service type is ticked.
        If cb_services_co.Checked Or cb_services_cv.Checked Or cb_services_ms.Checked Or
            cb_services_ma.Checked Or cb_services_roc.Checked Or (cb_services_others.Checked _
            And tb_others.Text <> "") Then
            isSetService = True
        End If
    End Sub

    Private Sub cmb_license_items_SelectionChangeCommitted(sender As Object, e As EventArgs) _
        Handles cmb_license_items.SelectionChangeCommitted
        ' Set License MOP ID index and License MOP Description index to be the same. For
        ' the purpose of saving updated data entries.
        cmb_payment_number_license.SelectedIndex = cmb_license_items.SelectedIndex
    End Sub

    Private Sub cmb_others_items_SelectionChangeCommitted(sender As Object, e As EventArgs) _
        Handles cmb_others_items.SelectionChangeCommitted
        ' Set Others MOP ID index and Others MOP Description index to be the same. For
        ' the purpose of saving updated data entries.
        cmb_payment_number_others.SelectedIndex = cmb_others_items.SelectedIndex
    End Sub

    Private Sub cmb_roc_items_SelectionChangeCommitted(sender As Object, e As EventArgs) _
        Handles cmb_roc_items.SelectionChangeCommitted
        ' Set ROC MOP ID index and ROC MOP Description index to be the same. For
        ' the purpose of saving updated data entries.
        cmb_payment_number_roc.SelectedIndex = cmb_roc_items.SelectedIndex
    End Sub

    Private Sub cmb_permits_items_SelectionChangeCommitted(sender As Object, e As EventArgs) _
        Handles cmb_permits_items.SelectionChangeCommitted
        ' Set Permits MOP ID index and Permits MOP Description index to be the same. For
        ' the purpose of saving updated data entries.
        cmb_payment_number_permits.SelectedIndex = cmb_permits_items.SelectedIndex
    End Sub

    Private Sub btn_save_Click(sender As Object, e As EventArgs) Handles btn_update.Click
        Dim success = False

        ' Check if type of payment is inserted in the datagridview.
        If dgv_license.Rows.Count <> 0 Or dgv_permits.Rows.Count <> 0 Or dgv_roc.Rows.Count <> 0 _
            Or dgv_others.Rows.Count <> 0 Then
            isSetParticularMOD = True
        Else
            isSetParticularMOD = False
        End If

        If isSetPayor And isSetType And isSetProcess And isSetService Then
            ' Input validation for 1.
            If isSetParticularName And isSetParticularMOD And Not cb_particular_two.Checked _
                And Not cb_particular_three.Checked Then
                update_SOA()
                success = True
            End If

            ' Input validation for particulars 1 and 2.
            If isSetParticularName And isSetParticularMOD And (cb_particular_two.Checked And
                tb_particular_two.Text <> "") And Not cb_particular_three.Checked Then
                update_SOA()
                success = True
            End If

            ' Input validation for all particulars.
            If isSetParticularName And isSetParticularMOD And (cb_particular_two.Checked And
                tb_particular_two.Text <> "") And (cb_particular_three.Checked And
                tb_particular_three.Text <> "") Then
                update_SOA()
                success = True
            End If

            If Not success Then
                MsgBox("You didn't set a name on the first, second or third particular or " &
                           "insert a payment entry.", MsgBoxStyle.Exclamation, "NTC Region 10")
            End If
        Else
            MsgBox("You didn't set payor, transaction, process, or service!",
                MsgBoxStyle.Exclamation, "NTC Region 10")
        End If
    End Sub

    Sub update_SOA()
        Try
            Dim process_type(3) As String
            Dim process_type_counter As Integer = 0
            Dim service_type(6) As String
            Dim service_type_counter As Integer = 0
            Dim current_datetime = Date.Now.ToString("yyyy-MM-dd HH:mm:ss")

            ' Exit edit mode in all datagridview.
            dgv_license.EndEdit()
            dgv_permits.EndEdit()
            dgv_roc.EndEdit()
            dgv_others.EndEdit()

            ' Delete previous process types.
            class_connection.con.Open()
            query = "delete from tbl_process_soa where process_type_id = '" & process_id & "'"
            cmd = New MySqlCommand(query, class_connection.con)
            cmd.ExecuteNonQuery()
            class_connection.con.Close()

            ' Add process type to process type array.
            If cb_process_dup.Checked Then
                process_type(process_type_counter) = "DUP"
                process_type_counter += 1
            End If

            If cb_process_mod.Checked Then
                process_type(process_type_counter) = "MOD"
                process_type_counter += 1
            End If

            If cb_process_others.Checked Then
                process_type(process_type_counter) = "OTHERS"
                process_type_counter += 1
            End If

            ' Insert process type variables to database.
            For Each process As String In process_type
                If process <> "" Then
                    class_connection.con.Open()
                    query = "insert into tbl_process_soa(process_type_id, process_name) " &
                        "Values('" & process_id & "', '" & process & "')"
                    cmd = New MySqlCommand(query, class_connection.con)
                    cmd.ExecuteNonQuery()
                    class_connection.con.Close()
                End If
            Next

            ' Delete previous service type.
            class_connection.con.Open()
            query = "delete from tbl_service_soa where service_type_id = '" & service_id &
                "'"
            cmd = New MySqlCommand(query, class_connection.con)
            cmd.ExecuteNonQuery()
            class_connection.con.Close()

            ' Add process type to service type array.
            If cb_services_co.Checked Then
                service_type(service_type_counter) = "CO"
                service_type_counter += 1
            End If

            If cb_services_cv.Checked Then
                service_type(service_type_counter) = "CV"
                service_type_counter += 1
            End If

            If cb_services_ms.Checked Then
                service_type(service_type_counter) = "MS"
                service_type_counter += 1
            End If

            If cb_services_ma.Checked Then
                service_type(service_type_counter) = "MA"
                service_type_counter += 1
            End If

            If cb_services_roc.Checked Then
                service_type(service_type_counter) = "ROC"
                service_type_counter += 1
            End If

            If cb_services_others.Checked Then
                service_type(service_type_counter) = "OTHERS"
                service_type_counter += 1
            End If

            ' Insert service type variables to database.
            For Each service As String In service_type
                If service <> "" And service <> "OTHERS" Then
                    class_connection.con.Open()
                    query = "insert into tbl_service_soa(service_type_id, service_name, custom_value) " &
                        "Values('" & service_id & "', '" & service & "','None')"
                    cmd = New MySqlCommand(query, class_connection.con)
                    cmd.ExecuteNonQuery()
                    class_connection.con.Close()
                End If

                ' If other service is ticked and filled.
                If service <> "" And service = "OTHERS" Then
                    class_connection.con.Open()
                    query = "insert into tbl_service_soa(service_type_id, service_name, custom_value) " &
                        "Values('" & service_id & "', '" & service & "','" & tb_others.Text & "')"
                    cmd = New MySqlCommand(query, class_connection.con)
                    cmd.ExecuteNonQuery()
                    class_connection.con.Close()
                End If
            Next

            ' Delete previous particulars.
            class_connection.con.Open()
            query = "delete from tbl_particular where part_id = '" & particularID & "'"
            cmd = New MySqlCommand(query, class_connection.con)
            cmd.ExecuteNonQuery()
            class_connection.con.Close()

            ' Insert particular 1 to databse.
            class_connection.con.Open()
            query = "insert into tbl_particular(part_id, part_pos, part_name, part_covered_from, " &
                "part_covered_to) Values('" & particularID & "', '1', '" &
                WebUtility.HtmlEncode(tb_particular_one.Text) & "','" &
                dtp_p1_from.Value.ToString("yyyy-MM-dd") & "', '" &
                dtp_p1_to.Value.ToString("yyyy-MM-dd") & "'); SELECT LAST_INSERT_ID()"
            cmd = New MySqlCommand(query, class_connection.con)
            particularOneID = CInt(cmd.ExecuteScalar())
            class_connection.con.Close()

            ' Insert particular 2 to databse.
            If cb_particular_two.Checked Then
                class_connection.con.Open()
                query = "insert into tbl_particular(part_id, part_pos, part_name, part_covered_from, " &
                    "part_covered_to) Values('" & particularID & "', '2', '" &
                    WebUtility.HtmlEncode(tb_particular_two.Text) & "','" &
                    dtp_p2_from.Value.ToString("yyyy-MM-dd") & "', '" &
                    dtp_p2_to.Value.ToString("yyyy-MM-dd") & "'); SELECT LAST_INSERT_ID()"
                cmd = New MySqlCommand(query, class_connection.con)
                particularTwoID = CInt(cmd.ExecuteScalar())
                class_connection.con.Close()
            End If

            ' Insert particular 3 to databse.
            If cb_particular_three.Checked Then
                class_connection.con.Open()
                query = "insert into tbl_particular(part_id, part_pos, part_name, part_covered_from, " &
                    "part_covered_to) Values('" & particularID & "', '3', '" &
                    WebUtility.HtmlEncode(tb_particular_three.Text) & "','" &
                    dtp_p3_from.Value.ToString("yyyy-MM-dd") & "', '" &
                    dtp_p3_to.Value.ToString("yyyy-MM-dd") & "'); SELECT LAST_INSERT_ID()"
                cmd = New MySqlCommand(query, class_connection.con)
                particularThreeID = CInt(cmd.ExecuteScalar())
                class_connection.con.Close()
            End If

            ' Delete previous SOA data entries.
            class_connection.con.Open()
            query = "delete from tbl_soa_data where soa_data_id = '" & soa_data_id & "'"
            cmd = New MySqlCommand(query, class_connection.con)
            cmd.ExecuteNonQuery()
            class_connection.con.Close()

            ' License SOA data entries.
            class_connection.con.Open()
            For i As Integer = 0 To dgv_license.Rows.Count - 1 Step +1
                cmd = New MySqlCommand("insert into tbl_soa_data(mop_id, others_custom_value, " &
                    "p1_no_of_years, p1_percent, p1_no_of_units, p1_fees, p2_no_of_years, " &
                    "p2_percent, p2_no_of_units, p2_fees, p3_no_of_years, p3_percent, p3_no_of_units, " &
                    "p3_fees, soa_data_id) Values(@mop_id, @others_custom_value,  @p1_no_years, " &
                    "@p1_percent, @P1_no_units, @p1_fees, @p2_no_years, @p2_percent, @p2_no_units, " &
                    "@p2_fees, @p3_no_years, @p3_percent, @p3_no_units, @p3_fees, @soa_data_id)",
                    class_connection.con)
                cmd.Parameters.Add("@mop_id", MySqlDbType.Int64).Value =
                    dgv_license.Rows(i).Cells(15).Value.ToString()
                cmd.Parameters.Add("@others_custom_value", MySqlDbType.VarChar).Value =
                    custom_value_array(Integer.Parse(dgv_license.Rows(i).Cells(15).Value.ToString()) - 1)
                cmd.Parameters.Add("@p1_no_years", MySqlDbType.Int64).Value =
                    dgv_license.Rows(i).Cells(2).Value.ToString()
                cmd.Parameters.Add("@p1_percent", MySqlDbType.Decimal).Value =
                    dgv_license.Rows(i).Cells(3).Value.ToString()
                cmd.Parameters.Add("@p1_no_units", MySqlDbType.Int64).Value =
                    dgv_license.Rows(i).Cells(4).Value.ToString()
                cmd.Parameters.Add("@p1_fees", MySqlDbType.Decimal).Value =
                    dgv_license.Rows(i).Cells(5).Value.ToString()
                cmd.Parameters.Add("@p2_no_years", MySqlDbType.Int64).Value =
                    dgv_license.Rows(i).Cells(6).Value.ToString()
                cmd.Parameters.Add("@p2_percent", MySqlDbType.Decimal).Value =
                    dgv_license.Rows(i).Cells(7).Value.ToString()
                cmd.Parameters.Add("@p2_no_units", MySqlDbType.Int64).Value =
                    dgv_license.Rows(i).Cells(8).Value.ToString()
                cmd.Parameters.Add("@p2_fees", MySqlDbType.Decimal).Value =
                    dgv_license.Rows(i).Cells(9).Value.ToString()
                cmd.Parameters.Add("@p3_no_years", MySqlDbType.Int64).Value =
                    dgv_license.Rows(i).Cells(10).Value.ToString()
                cmd.Parameters.Add("@p3_percent", MySqlDbType.Decimal).Value =
                    dgv_license.Rows(i).Cells(11).Value.ToString()
                cmd.Parameters.Add("@p3_no_units", MySqlDbType.Int64).Value =
                    dgv_license.Rows(i).Cells(12).Value.ToString()
                cmd.Parameters.Add("@p3_fees", MySqlDbType.Decimal).Value =
                    dgv_license.Rows(i).Cells(13).Value.ToString()
                cmd.Parameters.Add("@soa_data_id", MySqlDbType.VarChar).Value = soa_data_id
                cmd.ExecuteNonQuery()
            Next
            class_connection.con.Close()

            ' License SOA data entries.
            class_connection.con.Open()
            For i As Integer = 0 To dgv_permits.Rows.Count - 1 Step +1
                cmd = New MySqlCommand("insert into tbl_soa_data(mop_id, others_custom_value, " &
                    "p1_no_of_years, p1_percent, p1_no_of_units, p1_fees, p2_no_of_years, " &
                    "p2_percent, p2_no_of_units, p2_fees, p3_no_of_years, p3_percent, p3_no_of_units, " &
                    "p3_fees, soa_data_id) Values(@mop_id, @others_custom_value, @p1_no_years, @p1_percent, " &
                    "@P1_no_units, @p1_fees, @p2_no_years, @p2_percent, @p2_no_units, @p2_fees, @p3_no_years, " &
                    "@p3_percent, @p3_no_units, @p3_fees, @soa_data_id)", class_connection.con)
                cmd.Parameters.Add("@mop_id", MySqlDbType.Int64).Value =
                    dgv_permits.Rows(i).Cells(15).Value.ToString()
                cmd.Parameters.Add("@others_custom_value", MySqlDbType.VarChar).Value =
                    custom_value_array(Integer.Parse(dgv_permits.Rows(i).Cells(15).Value.ToString()) - 1)
                cmd.Parameters.Add("@p1_no_years", MySqlDbType.Int64).Value =
                    dgv_permits.Rows(i).Cells(2).Value.ToString()
                cmd.Parameters.Add("@p1_percent", MySqlDbType.Decimal).Value =
                    dgv_permits.Rows(i).Cells(3).Value.ToString()
                cmd.Parameters.Add("@p1_no_units", MySqlDbType.Int64).Value =
                    dgv_permits.Rows(i).Cells(4).Value.ToString()
                cmd.Parameters.Add("@p1_fees", MySqlDbType.Decimal).Value =
                    dgv_permits.Rows(i).Cells(5).Value.ToString()
                cmd.Parameters.Add("@p2_no_years", MySqlDbType.Int64).Value =
                    dgv_permits.Rows(i).Cells(6).Value.ToString()
                cmd.Parameters.Add("@p2_percent", MySqlDbType.Decimal).Value =
                    dgv_permits.Rows(i).Cells(7).Value.ToString()
                cmd.Parameters.Add("@p2_no_units", MySqlDbType.Int64).Value =
                    dgv_permits.Rows(i).Cells(8).Value.ToString()
                cmd.Parameters.Add("@p2_fees", MySqlDbType.Decimal).Value =
                    dgv_permits.Rows(i).Cells(9).Value.ToString()
                cmd.Parameters.Add("@p3_no_years", MySqlDbType.Int64).Value =
                    dgv_permits.Rows(i).Cells(10).Value.ToString()
                cmd.Parameters.Add("@p3_percent", MySqlDbType.Decimal).Value =
                    dgv_permits.Rows(i).Cells(11).Value.ToString()
                cmd.Parameters.Add("@p3_no_units", MySqlDbType.Int64).Value =
                    dgv_permits.Rows(i).Cells(12).Value.ToString()
                cmd.Parameters.Add("@p3_fees", MySqlDbType.Decimal).Value =
                    dgv_permits.Rows(i).Cells(13).Value.ToString()
                cmd.Parameters.Add("@soa_data_id", MySqlDbType.VarChar).Value = soa_data_id
                cmd.ExecuteNonQuery()
            Next
            class_connection.con.Close()

            ' License SOA data entries.
            class_connection.con.Open()
            For i As Integer = 0 To dgv_roc.Rows.Count - 1 Step +1
                cmd = New MySqlCommand("insert into tbl_soa_data(mop_id, others_custom_value, " &
                    "p1_no_of_years, p1_percent, p1_no_of_units, p1_fees, p2_no_of_years, " &
                    "p2_percent, p2_no_of_units, p2_fees, p3_no_of_years, p3_percent, " &
                    "p3_no_of_units, p3_fees, soa_data_id) Values(@mop_id, @others_custom_value, " &
                    "@p1_no_years, @p1_percent, @P1_no_units, @p1_fees, @p2_no_years, @p2_percent, " &
                    "@p2_no_units, @p2_fees, @p3_no_years, @p3_percent, @p3_no_units, @p3_fees, " &
                    "@soa_data_id)", class_connection.con)
                cmd.Parameters.Add("@mop_id", MySqlDbType.Int64).Value =
                    dgv_roc.Rows(i).Cells(15).Value.ToString()
                cmd.Parameters.Add("@others_custom_value", MySqlDbType.VarChar).Value =
                    custom_value_array(Integer.Parse(dgv_roc.Rows(i).Cells(15).Value.ToString()) - 1)
                cmd.Parameters.Add("@p1_no_years", MySqlDbType.Int64).Value =
                    dgv_roc.Rows(i).Cells(2).Value.ToString()
                cmd.Parameters.Add("@p1_percent", MySqlDbType.Decimal).Value =
                    dgv_roc.Rows(i).Cells(3).Value.ToString()
                cmd.Parameters.Add("@p1_no_units", MySqlDbType.Int64).Value =
                    dgv_roc.Rows(i).Cells(4).Value.ToString()
                cmd.Parameters.Add("@p1_fees", MySqlDbType.Decimal).Value =
                    dgv_roc.Rows(i).Cells(5).Value.ToString()
                cmd.Parameters.Add("@p2_no_years", MySqlDbType.Int64).Value =
                    dgv_roc.Rows(i).Cells(6).Value.ToString()
                cmd.Parameters.Add("@p2_percent", MySqlDbType.Decimal).Value =
                    dgv_roc.Rows(i).Cells(7).Value.ToString()
                cmd.Parameters.Add("@p2_no_units", MySqlDbType.Int64).Value =
                    dgv_roc.Rows(i).Cells(8).Value.ToString()
                cmd.Parameters.Add("@p2_fees", MySqlDbType.Decimal).Value =
                    dgv_roc.Rows(i).Cells(9).Value.ToString()
                cmd.Parameters.Add("@p3_no_years", MySqlDbType.Int64).Value =
                    dgv_roc.Rows(i).Cells(10).Value.ToString()
                cmd.Parameters.Add("@p3_percent", MySqlDbType.Decimal).Value =
                    dgv_roc.Rows(i).Cells(11).Value.ToString()
                cmd.Parameters.Add("@p3_no_units", MySqlDbType.Int64).Value =
                    dgv_roc.Rows(i).Cells(12).Value.ToString()
                cmd.Parameters.Add("@p3_fees", MySqlDbType.Decimal).Value =
                    dgv_roc.Rows(i).Cells(13).Value.ToString()
                cmd.Parameters.Add("@soa_data_id", MySqlDbType.VarChar).Value = soa_data_id
                cmd.ExecuteNonQuery()
            Next
            class_connection.con.Close()

            ' License SOA data entries.
            class_connection.con.Open()
            For i As Integer = 0 To dgv_others.Rows.Count - 1 Step +1
                cmd = New MySqlCommand("insert into tbl_soa_data(mop_id, others_custom_value, " &
                    "p1_no_of_years, p1_percent, p1_no_of_units, p1_fees, p2_no_of_years, " &
                    "p2_percent, p2_no_of_units, p2_fees, p3_no_of_years, p3_percent, " &
                    "p3_no_of_units, p3_fees, soa_data_id) Values(@mop_id, @others_custom_value, " &
                    "@p1_no_years, @p1_percent, @P1_no_units, @p1_fees, @p2_no_years, @p2_percent, " &
                    "@p2_no_units, @p2_fees, @p3_no_years, @p3_percent, @p3_no_units, @p3_fees, " &
                    "@soa_data_id)", class_connection.con)
                cmd.Parameters.Add("@mop_id", MySqlDbType.Int64).Value =
                    dgv_others.Rows(i).Cells(15).Value.ToString()
                cmd.Parameters.Add("@others_custom_value", MySqlDbType.VarChar).Value =
                    custom_value_array(Integer.Parse(dgv_others.Rows(i).Cells(15).Value.ToString()) - 1)
                cmd.Parameters.Add("@p1_no_years", MySqlDbType.Int64).Value =
                    dgv_others.Rows(i).Cells(2).Value.ToString()
                cmd.Parameters.Add("@p1_percent", MySqlDbType.Decimal).Value =
                    dgv_others.Rows(i).Cells(3).Value.ToString()
                cmd.Parameters.Add("@p1_no_units", MySqlDbType.Int64).Value =
                    dgv_others.Rows(i).Cells(4).Value.ToString()
                cmd.Parameters.Add("@p1_fees", MySqlDbType.Decimal).Value =
                    dgv_others.Rows(i).Cells(5).Value.ToString()
                cmd.Parameters.Add("@p2_no_years", MySqlDbType.Int64).Value =
                    dgv_others.Rows(i).Cells(6).Value.ToString()
                cmd.Parameters.Add("@p2_percent", MySqlDbType.Decimal).Value =
                    dgv_others.Rows(i).Cells(7).Value.ToString()
                cmd.Parameters.Add("@p2_no_units", MySqlDbType.Int64).Value =
                    dgv_others.Rows(i).Cells(8).Value.ToString()
                cmd.Parameters.Add("@p2_fees", MySqlDbType.Decimal).Value =
                    dgv_others.Rows(i).Cells(9).Value.ToString()
                cmd.Parameters.Add("@p3_no_years", MySqlDbType.Int64).Value =
                    dgv_others.Rows(i).Cells(10).Value.ToString()
                cmd.Parameters.Add("@p3_percent", MySqlDbType.Decimal).Value =
                    dgv_others.Rows(i).Cells(11).Value.ToString()
                cmd.Parameters.Add("@p3_no_units", MySqlDbType.Int64).Value =
                    dgv_others.Rows(i).Cells(12).Value.ToString()
                cmd.Parameters.Add("@p3_fees", MySqlDbType.Decimal).Value =
                    dgv_others.Rows(i).Cells(13).Value.ToString()
                cmd.Parameters.Add("@soa_data_id", MySqlDbType.VarChar).Value = soa_data_id
                cmd.ExecuteNonQuery()
            Next
            class_connection.con.Close()

            ' Update SOA details.
            class_connection.con.Open()
            query = "update tbl_soa_details set payor_id = '" & payor_id & "', date_expires = '" &
                dtp_expiry_date.Value.ToString("yyyy-MM-dd") & "', date_modified = '" &
                current_datetime & "', application_type = '" & cmb_type.Text &
                "' where soa_details_id = '" & soa_details_id & "'"
            cmd = New MySqlCommand(query, class_connection.con)
            cmd.ExecuteNonQuery()
            class_connection.con.Close()

            MsgBox("SOA has been Updated. Opening the newly updated SOA.", MsgBoxStyle.Information,
                   "NTC Region 10")

            frm_licenser_log.soa_id_transact = soa_id
            frm_licenser_log.user_id = user_id
            frm_licenser_log.authority_level = authority_level
            frm_licenser_log.Show()

            Close()
        Catch ex As MySqlException
            class_connection.con.Close()
            MsgBox("Couldn't connect to server. Contact the IT Administrator for assistance.",
                   MsgBoxStyle.Exclamation, "NTC Region 10")
        Catch ex As Exception
            class_connection.con.Close()
            MsgBox("Something went wrong. Couldn't process the data.", MsgBoxStyle.Exclamation,
            "NTC Region 10")
        End Try
    End Sub

    Private Sub btn_add_payor_Click(sender As Object, e As EventArgs) Handles btn_add_payor.Click
        frm_payor_list.accessFrom = 2
        frm_payor_list.Show()
    End Sub

    Private Sub CheckBox8_CheckedChanged(sender As Object, e As EventArgs) Handles _
        cb_services_others.CheckedChanged
        If cb_services_others.Checked = True Then
            lbl_other.Visible = True
            tb_others.Visible = True
        Else
            lbl_other.Visible = False
            tb_others.Visible = False
            tb_others.Text = ""
        End If
        If cb_services_co.Checked Or cb_services_cv.Checked Or cb_services_ms.Checked Or
            cb_services_ma.Checked Or cb_services_roc.Checked Or (cb_services_others.Checked And
            tb_others.Text <> "") Then
            isSetService = True
        Else
            isSetService = False
        End If
    End Sub

    Private Sub tb_others_TextChanged(sender As Object, e As EventArgs) Handles _
        tb_others.TextChanged
        If cb_services_co.Checked Or cb_services_cv.Checked Or cb_services_ms.Checked Or
            cb_services_ma.Checked Or cb_services_roc.Checked Or (cb_services_others.Checked And
            tb_others.Text <> "") Then
            isSetService = True
        Else
            isSetService = False
        End If
    End Sub

    Private Sub number_validation(ByVal sender As Object, ByVal e As KeyPressEventArgs)
        Select Case e.KeyChar
            Case "0", "1", "2", "3", "4", "5", "6", "7", "8", "9", ".", vbBack
                e.Handled = False
            Case Else
                e.Handled = True
        End Select
    End Sub

    Private Sub dgv_license_EditingControlShowing(sender As Object, e As _
            DataGridViewEditingControlShowingEventArgs) Handles dgv_license.EditingControlShowing
        AddHandler CType(e.Control, TextBox).KeyPress, AddressOf number_validation
    End Sub

    Private Sub dgv_others_EditingControlShowing(sender As Object, e As _
            DataGridViewEditingControlShowingEventArgs) Handles dgv_others.EditingControlShowing
        AddHandler CType(e.Control, TextBox).KeyPress, AddressOf number_validation
    End Sub

    Private Sub dgv_roc_EditingControlShowing(sender As Object, e As _
            DataGridViewEditingControlShowingEventArgs) Handles dgv_roc.EditingControlShowing
        AddHandler CType(e.Control, TextBox).KeyPress, AddressOf number_validation
    End Sub

    Private Sub dgv_permits_EditingControlShowing(sender As Object, e As _
            DataGridViewEditingControlShowingEventArgs) Handles dgv_permits.EditingControlShowing
        AddHandler CType(e.Control, TextBox).KeyPress, AddressOf number_validation
    End Sub

    Private Sub dgv_license_CellDoubleClick(sender As Object, e As DataGridViewCellEventArgs) _
        Handles dgv_license.CellDoubleClick
        If e.RowIndex >= 0 Then
            If e.ColumnIndex = 1 Then
                tabFocus = 1
                row_customize_no = e.RowIndex
                tb_others_value.Text = custom_value_array(Integer.Parse(dgv_license.Rows(
                    row_customize_no).Cells(15).Value.ToString) - 1)
                pnl_mod_others.Visible = True
            End If
        End If
    End Sub

    Private Sub dgv_permits_CellDoubleClick(sender As Object, e As DataGridViewCellEventArgs) _
        Handles dgv_permits.CellDoubleClick
        If e.RowIndex >= 0 Then
            If e.ColumnIndex = 1 Then
                tabFocus = 2
                row_customize_no = e.RowIndex
                tb_others_value.Text = custom_value_array(Integer.Parse(dgv_permits.Rows(
                    row_customize_no).Cells(15).Value.ToString) - 1)
                pnl_mod_others.Visible = True
            End If
        End If
    End Sub

    Private Sub dgv_roc_CellDoubleClick(sender As Object, e As DataGridViewCellEventArgs) _
        Handles dgv_roc.CellDoubleClick
        If e.RowIndex >= 0 Then
            If e.ColumnIndex = 1 Then
                tabFocus = 3
                row_customize_no = e.RowIndex
                tb_others_value.Text = custom_value_array(Integer.Parse(dgv_roc.Rows(
                    row_customize_no).Cells(15).Value.ToString) - 1)
                pnl_mod_others.Visible = True
            End If
        End If
    End Sub

    Private Sub dgv_others_CellDoubleClick(sender As Object, e As DataGridViewCellEventArgs) _
        Handles dgv_others.CellDoubleClick
        If e.RowIndex >= 0 Then
            If e.ColumnIndex = 1 Then
                tabFocus = 4
                row_customize_no = e.RowIndex
                tb_others_value.Text = custom_value_array(Integer.Parse(dgv_others.Rows(
                    row_customize_no).Cells(15).Value.ToString) - 1)
                pnl_mod_others.Visible = True
            End If
        End If
    End Sub

    Private Sub btn_others_cancel_Click(sender As Object, e As EventArgs) Handles _
        btn_cancel_others.Click
        pnl_mod_others.Visible = False
    End Sub

    Private Sub btn_others_confirm_Click(sender As Object, e As EventArgs) Handles _
        btn_confirm_others.Click
        pnl_mod_others.Visible = False

        Select Case tabFocus
            Case 1
                custom_value_array(Integer.Parse(dgv_license.Rows(row_customize_no).Cells(15).Value.ToString) _
                    - 1) = WebUtility.HtmlEncode(tb_others_value.Text)
                dgv_license.Rows(row_customize_no).Cells(1).Value = MOP(Integer.Parse(dgv_license.Rows(
                    row_customize_no).Cells(15).Value.ToString) - 1) + ": " + tb_others_value.Text
            Case 2
                custom_value_array(Integer.Parse(dgv_permits.Rows(row_customize_no).Cells(15).Value.ToString) _
                    - 1) = WebUtility.HtmlEncode(tb_others_value.Text)
                dgv_permits.Rows(row_customize_no).Cells(1).Value = MOP(Integer.Parse(dgv_permits.Rows(
                    row_customize_no).Cells(15).Value.ToString) - 1) + ": " + tb_others_value.Text
            Case 3
                custom_value_array(Integer.Parse(dgv_roc.Rows(row_customize_no).Cells(15).Value.ToString) -
                    1) = WebUtility.HtmlEncode(tb_others_value.Text)
                dgv_roc.Rows(row_customize_no).Cells(1).Value = MOP(Integer.Parse(dgv_roc.Rows(
                    row_customize_no).Cells(15).Value.ToString) - 1) + ": " + tb_others_value.Text
            Case Else
                custom_value_array(Integer.Parse(dgv_others.Rows(row_customize_no).Cells(15).Value.ToString) _
                    - 1) = WebUtility.HtmlEncode(tb_others_value.Text)
                dgv_others.Rows(row_customize_no).Cells(1).Value = MOP(Integer.Parse(dgv_others.Rows(
                    row_customize_no).Cells(15).Value.ToString) - 1) + ": " + tb_others_value.Text
        End Select
    End Sub

    Private Sub cb_particular_two_CheckedChanged(sender As Object, e As EventArgs) Handles _
        cb_particular_two.CheckedChanged
        If cb_particular_two.Checked Then
            tb_particular_two.Enabled = True
            cb_particular_three.Enabled = True

            yearDiffTwo = DateDiff(DateInterval.Year, dtp_p2_from.Value, dtp_p2_to.Value)

            ' Enable edit in particular 2.
            For Each row As DataGridViewRow In dgv_license.Rows
                dgv_license.Columns(6).ReadOnly = False
                dgv_license.Columns(7).ReadOnly = False
                dgv_license.Columns(8).ReadOnly = False
                dgv_license.Columns(9).ReadOnly = False
            Next

            For Each row As DataGridViewRow In dgv_permits.Rows
                dgv_permits.Columns(6).ReadOnly = False
                dgv_permits.Columns(7).ReadOnly = False
                dgv_permits.Columns(8).ReadOnly = False
                dgv_permits.Columns(9).ReadOnly = False
            Next

            For Each row As DataGridViewRow In dgv_roc.Rows
                dgv_roc.Columns(6).ReadOnly = False
                dgv_roc.Columns(7).ReadOnly = False
                dgv_roc.Columns(8).ReadOnly = False
                dgv_roc.Columns(9).ReadOnly = False
            Next

            For Each row As DataGridViewRow In dgv_others.Rows
                dgv_others.Columns(6).ReadOnly = False
                dgv_others.Columns(7).ReadOnly = False
                dgv_others.Columns(8).ReadOnly = False
                dgv_others.Columns(9).ReadOnly = False
            Next

            ' Change no. of years in particular 2.
            For i As Integer = 0 To dgv_license.RowCount - 1
                dgv_license.Rows(i).Cells(6).Value = yearDiffTwo.ToString
                dgv_license.Rows(i).Cells(7).Value = "100"
                dgv_license.Rows(i).Cells(8).Value = "0"
            Next

            For i As Integer = 0 To dgv_permits.RowCount - 1
                dgv_permits.Rows(i).Cells(6).Value = yearDiffTwo.ToString
                dgv_permits.Rows(i).Cells(7).Value = "100"
                dgv_permits.Rows(i).Cells(8).Value = "0"
            Next

            For i As Integer = 0 To dgv_roc.RowCount - 1
                dgv_roc.Rows(i).Cells(6).Value = yearDiffTwo.ToString
                dgv_roc.Rows(i).Cells(7).Value = "100"
                dgv_roc.Rows(i).Cells(8).Value = "0"
            Next

            For i As Integer = 0 To dgv_others.RowCount - 1
                dgv_others.Rows(i).Cells(6).Value = yearDiffTwo.ToString
                dgv_others.Rows(i).Cells(7).Value = "100"
                dgv_others.Rows(i).Cells(8).Value = "0"
            Next
        Else
            cb_particular_three.Enabled = False
            cb_particular_three.Checked = False
            tb_particular_two.Enabled = False
            dtp_p2_from.Enabled = False
            dtp_p2_to.Enabled = False
            tb_particular_two.Text = ""
            dtp_p2_from.Text = ""
            dtp_p2_to.Text = ""
            tb_particular_three.Text = ""
            dtp_p3_from.Text = ""
            dtp_p3_to.Text = ""

            For i As Integer = 0 To dgv_license.RowCount - 1
                dgv_license.Rows(i).Cells(6).Value = "0"
                dgv_license.Rows(i).Cells(7).Value = "0"
                dgv_license.Rows(i).Cells(8).Value = "0"
                dgv_license.Rows(i).Cells(9).Value = 0
                dgv_license.Rows(i).Cells(10).Value = "0"
                dgv_license.Rows(i).Cells(11).Value = "0"
                dgv_license.Rows(i).Cells(12).Value = "0"
                dgv_license.Rows(i).Cells(13).Value = 0
                dgv_license.Rows(i).Cells(6).ReadOnly = True
                dgv_license.Rows(i).Cells(7).ReadOnly = True
                dgv_license.Rows(i).Cells(8).ReadOnly = True
                dgv_license.Rows(i).Cells(9).ReadOnly = True
                dgv_license.Rows(i).Cells(10).ReadOnly = True
                dgv_license.Rows(i).Cells(11).ReadOnly = True
                dgv_license.Rows(i).Cells(12).ReadOnly = True
                dgv_license.Rows(i).Cells(13).ReadOnly = True
            Next

            For i As Integer = 0 To dgv_permits.RowCount - 1
                dgv_permits.Rows(i).Cells(6).Value = "0"
                dgv_permits.Rows(i).Cells(7).Value = "0"
                dgv_permits.Rows(i).Cells(8).Value = "0"
                dgv_permits.Rows(i).Cells(9).Value = 0
                dgv_permits.Rows(i).Cells(10).Value = "0"
                dgv_permits.Rows(i).Cells(11).Value = "0"
                dgv_permits.Rows(i).Cells(12).Value = "0"
                dgv_permits.Rows(i).Cells(13).Value = 0
                dgv_permits.Rows(i).Cells(6).ReadOnly = True
                dgv_permits.Rows(i).Cells(7).ReadOnly = True
                dgv_permits.Rows(i).Cells(8).ReadOnly = True
                dgv_permits.Rows(i).Cells(9).ReadOnly = True
                dgv_permits.Rows(i).Cells(10).ReadOnly = True
                dgv_permits.Rows(i).Cells(11).ReadOnly = True
                dgv_permits.Rows(i).Cells(12).ReadOnly = True
                dgv_permits.Rows(i).Cells(13).ReadOnly = True
            Next

            For i As Integer = 0 To dgv_roc.RowCount - 1
                dgv_roc.Rows(i).Cells(6).Value = "0"
                dgv_roc.Rows(i).Cells(7).Value = "0"
                dgv_roc.Rows(i).Cells(8).Value = "0"
                dgv_roc.Rows(i).Cells(9).Value = 0
                dgv_roc.Rows(i).Cells(10).Value = "0"
                dgv_roc.Rows(i).Cells(11).Value = "0"
                dgv_roc.Rows(i).Cells(12).Value = "0"
                dgv_roc.Rows(i).Cells(13).Value = 0
                dgv_roc.Rows(i).Cells(6).ReadOnly = True
                dgv_roc.Rows(i).Cells(7).ReadOnly = True
                dgv_roc.Rows(i).Cells(8).ReadOnly = True
                dgv_roc.Rows(i).Cells(9).ReadOnly = True
                dgv_roc.Rows(i).Cells(10).ReadOnly = True
                dgv_roc.Rows(i).Cells(11).ReadOnly = True
                dgv_roc.Rows(i).Cells(12).ReadOnly = True
                dgv_roc.Rows(i).Cells(13).ReadOnly = True
            Next

            For i As Integer = 0 To dgv_others.RowCount - 1
                dgv_others.Rows(i).Cells(6).Value = "0"
                dgv_others.Rows(i).Cells(7).Value = "0"
                dgv_others.Rows(i).Cells(8).Value = "0"
                dgv_others.Rows(i).Cells(9).Value = 0
                dgv_others.Rows(i).Cells(10).Value = "0"
                dgv_others.Rows(i).Cells(11).Value = "0"
                dgv_others.Rows(i).Cells(12).Value = "0"
                dgv_others.Rows(i).Cells(13).Value = 0
                dgv_others.Rows(i).Cells(6).ReadOnly = True
                dgv_others.Rows(i).Cells(7).ReadOnly = True
                dgv_others.Rows(i).Cells(8).ReadOnly = True
                dgv_others.Rows(i).Cells(9).ReadOnly = True
                dgv_others.Rows(i).Cells(10).ReadOnly = True
                dgv_others.Rows(i).Cells(11).ReadOnly = True
                dgv_others.Rows(i).Cells(12).ReadOnly = True
                dgv_others.Rows(i).Cells(13).ReadOnly = True
            Next
        End If

        compute_row_subtotal_license()
        compute_total()
        update_expiration_date()
    End Sub

    Private Sub cb_particular_three_CheckedChanged(sender As Object, e As EventArgs) Handles cb_particular_three.CheckedChanged
        If cb_particular_three.Checked Then
            tb_particular_three.Enabled = True

            yearDiffThree = DateDiff(DateInterval.Year, dtp_p3_from.Value, dtp_p3_to.Value)

            ' Enable edit in particular 3.
            For Each row As DataGridViewRow In dgv_license.Rows
                dgv_license.Columns(10).ReadOnly = False
                dgv_license.Columns(11).ReadOnly = False
                dgv_license.Columns(12).ReadOnly = False
                dgv_license.Columns(13).ReadOnly = False
            Next

            For Each row As DataGridViewRow In dgv_permits.Rows
                dgv_permits.Columns(10).ReadOnly = False
                dgv_permits.Columns(11).ReadOnly = False
                dgv_permits.Columns(12).ReadOnly = False
                dgv_permits.Columns(13).ReadOnly = False
            Next

            For Each row As DataGridViewRow In dgv_roc.Rows
                dgv_roc.Columns(10).ReadOnly = False
                dgv_roc.Columns(11).ReadOnly = False
                dgv_roc.Columns(12).ReadOnly = False
                dgv_roc.Columns(13).ReadOnly = False
            Next

            For Each row As DataGridViewRow In dgv_others.Rows
                dgv_others.Columns(10).ReadOnly = False
                dgv_others.Columns(11).ReadOnly = False
                dgv_others.Columns(12).ReadOnly = False
                dgv_others.Columns(13).ReadOnly = False
            Next

            ' Change no. of years in particular 3.
            For i As Integer = 0 To dgv_license.RowCount - 1
                dgv_license.Rows(i).Cells(10).Value = yearDiffThree.ToString
                dgv_license.Rows(i).Cells(11).Value = "100"
                dgv_license.Rows(i).Cells(12).Value = "0"
            Next

            For i As Integer = 0 To dgv_permits.RowCount - 1
                dgv_permits.Rows(i).Cells(10).Value = yearDiffThree.ToString
                dgv_permits.Rows(i).Cells(11).Value = "100"
                dgv_permits.Rows(i).Cells(12).Value = "0"
            Next

            For i As Integer = 0 To dgv_roc.RowCount - 1
                dgv_roc.Rows(i).Cells(10).Value = yearDiffThree.ToString
                dgv_roc.Rows(i).Cells(11).Value = "100"
                dgv_roc.Rows(i).Cells(12).Value = "0"
            Next

            For i As Integer = 0 To dgv_others.RowCount - 1
                dgv_others.Rows(i).Cells(10).Value = yearDiffThree.ToString
                dgv_others.Rows(i).Cells(11).Value = "100"
                dgv_others.Rows(i).Cells(12).Value = "0"
            Next
        Else
            tb_particular_three.Enabled = False
            dtp_p3_from.Enabled = False
            dtp_p3_to.Enabled = False
            tb_particular_three.Text = ""
            dtp_p3_from.Text = ""
            dtp_p3_to.Text = ""

            For i As Integer = 0 To dgv_license.RowCount - 1
                dgv_license.Rows(i).Cells(10).Value = "0"
                dgv_license.Rows(i).Cells(11).Value = "0"
                dgv_license.Rows(i).Cells(12).Value = "0"
                dgv_license.Rows(i).Cells(13).Value = 0
                dgv_license.Rows(i).Cells(10).ReadOnly = True
                dgv_license.Rows(i).Cells(11).ReadOnly = True
                dgv_license.Rows(i).Cells(12).ReadOnly = True
                dgv_license.Rows(i).Cells(13).ReadOnly = True
            Next

            For i As Integer = 0 To dgv_permits.RowCount - 1
                dgv_permits.Rows(i).Cells(10).Value = "0"
                dgv_permits.Rows(i).Cells(11).Value = "0"
                dgv_permits.Rows(i).Cells(12).Value = "0"
                dgv_permits.Rows(i).Cells(13).Value = 0
                dgv_permits.Rows(i).Cells(10).ReadOnly = True
                dgv_permits.Rows(i).Cells(11).ReadOnly = True
                dgv_permits.Rows(i).Cells(12).ReadOnly = True
                dgv_permits.Rows(i).Cells(13).ReadOnly = True
            Next

            For i As Integer = 0 To dgv_roc.RowCount - 1
                dgv_roc.Rows(i).Cells(10).Value = "0"
                dgv_roc.Rows(i).Cells(11).Value = "0"
                dgv_roc.Rows(i).Cells(12).Value = "0"
                dgv_roc.Rows(i).Cells(13).Value = 0
                dgv_roc.Rows(i).Cells(10).ReadOnly = True
                dgv_roc.Rows(i).Cells(11).ReadOnly = True
                dgv_roc.Rows(i).Cells(12).ReadOnly = True
                dgv_roc.Rows(i).Cells(13).ReadOnly = True
            Next

            For i As Integer = 0 To dgv_others.RowCount - 1
                dgv_others.Rows(i).Cells(10).Value = "0"
                dgv_others.Rows(i).Cells(11).Value = "0"
                dgv_others.Rows(i).Cells(12).Value = "0"
                dgv_others.Rows(i).Cells(13).Value = 0
                dgv_others.Rows(i).Cells(10).ReadOnly = True
                dgv_others.Rows(i).Cells(11).ReadOnly = True
                dgv_others.Rows(i).Cells(12).ReadOnly = True
                dgv_others.Rows(i).Cells(13).ReadOnly = True
            Next
        End If

        compute_row_subtotal_license()
        compute_total()
        update_expiration_date()
    End Sub

    Sub update_expiration_date()
        ' Update expiration date of this SOA based on usage of 3 particulars' date.
        Dim currentDate_p1, currentDate_p2, currentDate_p3 As DateTime

        ' If particular 1 is used and others are not.
        If tb_particular_one.Text <> "" And tb_particular_two.Text = "" And tb_particular_three.Text = "" Then
            currentDate_p1 = Convert.ToDateTime(dtp_p1_from.Text)
            currentDate_p1 = currentDate_p1.AddDays(1)
            dtp_expiry_date.Text = currentDate_p1.ToString("yyyy-MM-dd")

            ' If particular 1 and 2 are used and 3 is not.
        ElseIf tb_particular_one.Text <> "" And tb_particular_two.Text <> "" And tb_particular_three.Text _
            = "" Then
            currentDate_p1 = Convert.ToDateTime(dtp_p1_from.Text)
            currentDate_p2 = Convert.ToDateTime(dtp_p2_from.Text)

            If currentDate_p1 < currentDate_p2 Then
                currentDate_p1 = currentDate_p1.AddDays(1)
                dtp_expiry_date.Text = currentDate_p1.ToString("yyyy-MM-dd")
            Else
                currentDate_p2 = currentDate_p2.AddDays(1)
                dtp_expiry_date.Text = currentDate_p2.ToString("yyyy-MM-dd")
            End If

            ' If all particular is used.
        ElseIf tb_particular_one.Text <> "" And tb_particular_two.Text <> "" And tb_particular_three.Text <>
            "" Then
            currentDate_p1 = Convert.ToDateTime(dtp_p1_from.Text)
            currentDate_p2 = Convert.ToDateTime(dtp_p2_from.Text)
            currentDate_p3 = Convert.ToDateTime(dtp_p3_from.Text)

            If currentDate_p1 < currentDate_p2 Then
                If currentDate_p1 < currentDate_p3 Then
                    currentDate_p1 = currentDate_p1.AddDays(1)
                    dtp_expiry_date.Text = currentDate_p1.ToString("yyyy-MM-dd")
                Else
                    currentDate_p3 = currentDate_p3.AddDays(1)
                    dtp_expiry_date.Text = currentDate_p3.ToString("yyyy-MM-dd")
                End If
            Else
                If currentDate_p2 < currentDate_p3 Then
                    currentDate_p2 = currentDate_p2.AddDays(1)
                    dtp_expiry_date.Text = currentDate_p2.ToString("yyyy-MM-dd")
                Else
                    currentDate_p3 = currentDate_p3.AddDays(1)
                    dtp_expiry_date.Text = currentDate_p3.ToString("yyyy-MM-dd")

                End If
            End If
        End If
    End Sub

    Sub tab_color_reset()
        btn_tab_license.BackColor = Color.FromArgb(69, 90, 100)
        btn_tab_permits.BackColor = Color.FromArgb(69, 90, 100)
        btn_tab_roc.BackColor = Color.FromArgb(69, 90, 100)
        btn_tab_others.BackColor = Color.FromArgb(69, 90, 100)
        btn_tab_license.ForeColor = Color.Silver
        btn_tab_permits.ForeColor = Color.Silver
        btn_tab_roc.ForeColor = Color.Silver
        btn_tab_others.ForeColor = Color.Silver
    End Sub

    Private Sub btn_tab_license_Click(sender As Object, e As EventArgs) Handles btn_tab_license.Click
        tab_color_reset()

        btn_tab_license.BackColor = Color.FromArgb(113, 125, 137)
        btn_tab_license.ForeColor = Color.White
        pnl_license.BringToFront()
    End Sub

    Private Sub btn_tab_permits_Click(sender As Object, e As EventArgs) Handles btn_tab_permits.Click
        tab_color_reset()

        btn_tab_permits.BackColor = Color.FromArgb(113, 125, 137)
        btn_tab_permits.ForeColor = Color.White
        pnl_permits.BringToFront()
    End Sub

    Private Sub btn_tab_roc_Click(sender As Object, e As EventArgs) Handles btn_tab_roc.Click
        tab_color_reset()

        btn_tab_roc.BackColor = Color.FromArgb(113, 125, 137)
        btn_tab_roc.ForeColor = Color.White
        pnl_roc.BringToFront()
    End Sub

    Private Sub btn_tab_others_Click(sender As Object, e As EventArgs) Handles btn_tab_others.Click
        tab_color_reset()

        btn_tab_others.BackColor = Color.FromArgb(113, 125, 137)
        btn_tab_others.ForeColor = Color.White
        pnl_others.BringToFront()
    End Sub
End Class