﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class uc_system_settings
    Inherits System.Windows.Forms.UserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.cmb_accounting_officer = New System.Windows.Forms.ComboBox()
        Me.cmb_cashiering_officer = New System.Windows.Forms.ComboBox()
        Me.btn_save = New System.Windows.Forms.Button()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.tb_soa_serial = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.tb_op_serial = New System.Windows.Forms.TextBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.tb_soa_series = New System.Windows.Forms.TextBox()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.tb_op_series = New System.Windows.Forms.TextBox()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.tb_soa_override = New System.Windows.Forms.TextBox()
        Me.cb_soa_override = New System.Windows.Forms.CheckBox()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.cb_op_override = New System.Windows.Forms.CheckBox()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.tb_op_override = New System.Windows.Forms.TextBox()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Segoe UI", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.DarkGray
        Me.Label1.Location = New System.Drawing.Point(199, 65)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(67, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Accountant"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Segoe UI", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.DarkGray
        Me.Label2.Location = New System.Drawing.Point(220, 109)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(46, 13)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "Cashier"
        '
        'cmb_accounting_officer
        '
        Me.cmb_accounting_officer.BackColor = System.Drawing.Color.FromArgb(CType(CType(28, Byte), Integer), CType(CType(40, Byte), Integer), CType(CType(52, Byte), Integer))
        Me.cmb_accounting_officer.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmb_accounting_officer.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.cmb_accounting_officer.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmb_accounting_officer.ForeColor = System.Drawing.Color.White
        Me.cmb_accounting_officer.FormattingEnabled = True
        Me.cmb_accounting_officer.Location = New System.Drawing.Point(286, 62)
        Me.cmb_accounting_officer.Name = "cmb_accounting_officer"
        Me.cmb_accounting_officer.Size = New System.Drawing.Size(241, 21)
        Me.cmb_accounting_officer.TabIndex = 2
        '
        'cmb_cashiering_officer
        '
        Me.cmb_cashiering_officer.BackColor = System.Drawing.Color.FromArgb(CType(CType(28, Byte), Integer), CType(CType(40, Byte), Integer), CType(CType(52, Byte), Integer))
        Me.cmb_cashiering_officer.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmb_cashiering_officer.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.cmb_cashiering_officer.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmb_cashiering_officer.ForeColor = System.Drawing.Color.White
        Me.cmb_cashiering_officer.FormattingEnabled = True
        Me.cmb_cashiering_officer.Location = New System.Drawing.Point(286, 105)
        Me.cmb_cashiering_officer.Name = "cmb_cashiering_officer"
        Me.cmb_cashiering_officer.Size = New System.Drawing.Size(241, 21)
        Me.cmb_cashiering_officer.TabIndex = 3
        '
        'btn_save
        '
        Me.btn_save.BackColor = System.Drawing.Color.FromArgb(CType(CType(218, Byte), Integer), CType(CType(144, Byte), Integer), CType(CType(27, Byte), Integer))
        Me.btn_save.FlatAppearance.BorderSize = 0
        Me.btn_save.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_save.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_save.ForeColor = System.Drawing.Color.White
        Me.btn_save.Location = New System.Drawing.Point(383, 573)
        Me.btn_save.Name = "btn_save"
        Me.btn_save.Size = New System.Drawing.Size(110, 35)
        Me.btn_save.TabIndex = 132
        Me.btn_save.Text = "Save"
        Me.btn_save.UseVisualStyleBackColor = False
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Segoe UI", 14.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ForeColor = System.Drawing.Color.White
        Me.Label4.Location = New System.Drawing.Point(17, 24)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(249, 25)
        Me.Label4.TabIndex = 135
        Me.Label4.Text = "Employee Position Settings"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Segoe UI", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.ForeColor = System.Drawing.Color.DarkGray
        Me.Label6.Location = New System.Drawing.Point(132, 244)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(134, 13)
        Me.Label6.TabIndex = 140
        Me.Label6.Text = "SOA Serial Starting Digit"
        '
        'tb_soa_serial
        '
        Me.tb_soa_serial.BackColor = System.Drawing.Color.FromArgb(CType(CType(28, Byte), Integer), CType(CType(40, Byte), Integer), CType(CType(52, Byte), Integer))
        Me.tb_soa_serial.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.tb_soa_serial.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tb_soa_serial.ForeColor = System.Drawing.Color.White
        Me.tb_soa_serial.Location = New System.Drawing.Point(299, 239)
        Me.tb_soa_serial.MaxLength = 4
        Me.tb_soa_serial.Name = "tb_soa_serial"
        Me.tb_soa_serial.Size = New System.Drawing.Size(46, 22)
        Me.tb_soa_serial.TabIndex = 139
        Me.tb_soa_serial.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Segoe UI", 14.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.ForeColor = System.Drawing.Color.White
        Me.Label5.Location = New System.Drawing.Point(107, 202)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(159, 25)
        Me.Label5.TabIndex = 141
        Me.Label5.Text = "Licenser Settings"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Segoe UI", 14.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.ForeColor = System.Drawing.Color.White
        Me.Label7.Location = New System.Drawing.Point(79, 415)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(187, 25)
        Me.Label7.TabIndex = 146
        Me.Label7.Text = "Accountant Settings"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Segoe UI", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.ForeColor = System.Drawing.Color.DarkGray
        Me.Label8.Location = New System.Drawing.Point(139, 456)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(127, 13)
        Me.Label8.TabIndex = 145
        Me.Label8.Text = "OP Serial Starting Digit"
        '
        'tb_op_serial
        '
        Me.tb_op_serial.BackColor = System.Drawing.Color.FromArgb(CType(CType(28, Byte), Integer), CType(CType(40, Byte), Integer), CType(CType(52, Byte), Integer))
        Me.tb_op_serial.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.tb_op_serial.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tb_op_serial.ForeColor = System.Drawing.Color.White
        Me.tb_op_serial.Location = New System.Drawing.Point(299, 451)
        Me.tb_op_serial.MaxLength = 4
        Me.tb_op_serial.Name = "tb_op_serial"
        Me.tb_op_serial.Size = New System.Drawing.Size(46, 22)
        Me.tb_op_serial.TabIndex = 144
        Me.tb_op_serial.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Segoe UI", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.ForeColor = System.Drawing.Color.Silver
        Me.Label9.Location = New System.Drawing.Point(353, 244)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(302, 13)
        Me.Label9.TabIndex = 148
        Me.Label9.Text = "*starting digit of SOA serial. Default is 61-XXXX-XX-XXXX"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Font = New System.Drawing.Font("Segoe UI", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.ForeColor = System.Drawing.Color.Silver
        Me.Label10.Location = New System.Drawing.Point(353, 456)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(295, 13)
        Me.Label10.TabIndex = 149
        Me.Label10.Text = "*starting digit of OP serial. Default is 61-XXXX-XX-XXXX"
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Font = New System.Drawing.Font("Segoe UI", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.ForeColor = System.Drawing.Color.Silver
        Me.Label11.Location = New System.Drawing.Point(353, 282)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(336, 13)
        Me.Label11.TabIndex = 152
        Me.Label11.Text = "*custom starting series number applies on first transaction only"
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Font = New System.Drawing.Font("Segoe UI", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label12.ForeColor = System.Drawing.Color.DarkGray
        Me.Label12.Location = New System.Drawing.Point(127, 282)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(139, 13)
        Me.Label12.TabIndex = 151
        Me.Label12.Text = "SOA Serial Starting Series"
        '
        'tb_soa_series
        '
        Me.tb_soa_series.BackColor = System.Drawing.Color.FromArgb(CType(CType(28, Byte), Integer), CType(CType(40, Byte), Integer), CType(CType(52, Byte), Integer))
        Me.tb_soa_series.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.tb_soa_series.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tb_soa_series.ForeColor = System.Drawing.Color.White
        Me.tb_soa_series.Location = New System.Drawing.Point(299, 277)
        Me.tb_soa_series.MaxLength = 4
        Me.tb_soa_series.Name = "tb_soa_series"
        Me.tb_soa_series.Size = New System.Drawing.Size(46, 22)
        Me.tb_soa_series.TabIndex = 150
        Me.tb_soa_series.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Font = New System.Drawing.Font("Segoe UI", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label13.ForeColor = System.Drawing.Color.Silver
        Me.Label13.Location = New System.Drawing.Point(353, 497)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(336, 13)
        Me.Label13.TabIndex = 155
        Me.Label13.Text = "*custom starting series number applies on first transaction only"
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Font = New System.Drawing.Font("Segoe UI", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label14.ForeColor = System.Drawing.Color.DarkGray
        Me.Label14.Location = New System.Drawing.Point(134, 497)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(132, 13)
        Me.Label14.TabIndex = 154
        Me.Label14.Text = "OP Serial Starting Series"
        '
        'tb_op_series
        '
        Me.tb_op_series.BackColor = System.Drawing.Color.FromArgb(CType(CType(28, Byte), Integer), CType(CType(40, Byte), Integer), CType(CType(52, Byte), Integer))
        Me.tb_op_series.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.tb_op_series.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tb_op_series.ForeColor = System.Drawing.Color.White
        Me.tb_op_series.Location = New System.Drawing.Point(299, 492)
        Me.tb_op_series.MaxLength = 4
        Me.tb_op_series.Name = "tb_op_series"
        Me.tb_op_series.Size = New System.Drawing.Size(46, 22)
        Me.tb_op_series.TabIndex = 153
        Me.tb_op_series.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Font = New System.Drawing.Font("Segoe UI", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label15.ForeColor = System.Drawing.Color.Silver
        Me.Label15.Location = New System.Drawing.Point(534, 66)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(300, 13)
        Me.Label15.TabIndex = 156
        Me.Label15.Text = "*receives incoming endorsed SOA to selected accountant"
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Font = New System.Drawing.Font("Segoe UI", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label16.ForeColor = System.Drawing.Color.Silver
        Me.Label16.Location = New System.Drawing.Point(534, 110)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(220, 13)
        Me.Label16.TabIndex = 157
        Me.Label16.Text = "*receives incoming OP to selected cashier"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Segoe UI", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ForeColor = System.Drawing.Color.Silver
        Me.Label3.Location = New System.Drawing.Point(353, 321)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(360, 13)
        Me.Label3.TabIndex = 160
        Me.Label3.Text = "*override SOA next serial number. Tick checkbox to enable override. "
        '
        'tb_soa_override
        '
        Me.tb_soa_override.BackColor = System.Drawing.Color.FromArgb(CType(CType(28, Byte), Integer), CType(CType(40, Byte), Integer), CType(CType(52, Byte), Integer))
        Me.tb_soa_override.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.tb_soa_override.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tb_soa_override.ForeColor = System.Drawing.Color.White
        Me.tb_soa_override.Location = New System.Drawing.Point(299, 316)
        Me.tb_soa_override.MaxLength = 4
        Me.tb_soa_override.Name = "tb_soa_override"
        Me.tb_soa_override.Size = New System.Drawing.Size(46, 22)
        Me.tb_soa_override.TabIndex = 158
        Me.tb_soa_override.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'cb_soa_override
        '
        Me.cb_soa_override.AutoSize = True
        Me.cb_soa_override.Font = New System.Drawing.Font("Segoe UI", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cb_soa_override.ForeColor = System.Drawing.Color.DarkGray
        Me.cb_soa_override.Location = New System.Drawing.Point(278, 320)
        Me.cb_soa_override.Name = "cb_soa_override"
        Me.cb_soa_override.Size = New System.Drawing.Size(15, 14)
        Me.cb_soa_override.TabIndex = 161
        Me.cb_soa_override.UseVisualStyleBackColor = True
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Font = New System.Drawing.Font("Segoe UI", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label17.ForeColor = System.Drawing.Color.DarkGray
        Me.Label17.Location = New System.Drawing.Point(80, 321)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(186, 13)
        Me.Label17.TabIndex = 159
        Me.Label17.Text = "SOA Serial Starting Series Override"
        '
        'cb_op_override
        '
        Me.cb_op_override.AutoSize = True
        Me.cb_op_override.Font = New System.Drawing.Font("Segoe UI", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cb_op_override.ForeColor = System.Drawing.Color.DarkGray
        Me.cb_op_override.Location = New System.Drawing.Point(278, 538)
        Me.cb_op_override.Name = "cb_op_override"
        Me.cb_op_override.Size = New System.Drawing.Size(15, 14)
        Me.cb_op_override.TabIndex = 165
        Me.cb_op_override.UseVisualStyleBackColor = True
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.Font = New System.Drawing.Font("Segoe UI", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label19.ForeColor = System.Drawing.Color.DarkGray
        Me.Label19.Location = New System.Drawing.Point(87, 538)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(179, 13)
        Me.Label19.TabIndex = 163
        Me.Label19.Text = "OP Serial Starting Series Override"
        '
        'tb_op_override
        '
        Me.tb_op_override.BackColor = System.Drawing.Color.FromArgb(CType(CType(28, Byte), Integer), CType(CType(40, Byte), Integer), CType(CType(52, Byte), Integer))
        Me.tb_op_override.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.tb_op_override.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tb_op_override.ForeColor = System.Drawing.Color.White
        Me.tb_op_override.Location = New System.Drawing.Point(299, 533)
        Me.tb_op_override.MaxLength = 4
        Me.tb_op_override.Name = "tb_op_override"
        Me.tb_op_override.Size = New System.Drawing.Size(46, 22)
        Me.tb_op_override.TabIndex = 162
        Me.tb_op_override.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Font = New System.Drawing.Font("Segoe UI", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label18.ForeColor = System.Drawing.Color.Silver
        Me.Label18.Location = New System.Drawing.Point(353, 538)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(350, 13)
        Me.Label18.TabIndex = 166
        Me.Label18.Text = "*override OP next serial number. Tick checkbox to enable override."
        '
        'uc_system_settings
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoScroll = True
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(44, Byte), Integer), CType(CType(62, Byte), Integer), CType(CType(80, Byte), Integer))
        Me.Controls.Add(Me.Label18)
        Me.Controls.Add(Me.cb_op_override)
        Me.Controls.Add(Me.Label19)
        Me.Controls.Add(Me.tb_op_override)
        Me.Controls.Add(Me.cb_soa_override)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label17)
        Me.Controls.Add(Me.tb_soa_override)
        Me.Controls.Add(Me.Label16)
        Me.Controls.Add(Me.Label15)
        Me.Controls.Add(Me.Label13)
        Me.Controls.Add(Me.Label14)
        Me.Controls.Add(Me.tb_op_series)
        Me.Controls.Add(Me.Label11)
        Me.Controls.Add(Me.Label12)
        Me.Controls.Add(Me.tb_soa_series)
        Me.Controls.Add(Me.Label10)
        Me.Controls.Add(Me.Label9)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.tb_op_serial)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.tb_soa_serial)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.btn_save)
        Me.Controls.Add(Me.cmb_cashiering_officer)
        Me.Controls.Add(Me.cmb_accounting_officer)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.ForeColor = System.Drawing.Color.Silver
        Me.MaximumSize = New System.Drawing.Size(1364, 726)
        Me.MinimumSize = New System.Drawing.Size(1364, 726)
        Me.Name = "uc_system_settings"
        Me.Size = New System.Drawing.Size(1364, 726)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents Label1 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents cmb_accounting_officer As ComboBox
    Friend WithEvents cmb_cashiering_officer As ComboBox
    Friend WithEvents btn_save As Button
    Friend WithEvents Label4 As Label
    Friend WithEvents Label6 As Label
    Friend WithEvents tb_soa_serial As TextBox
    Friend WithEvents Label5 As Label
    Friend WithEvents Label7 As Label
    Friend WithEvents Label8 As Label
    Friend WithEvents tb_op_serial As TextBox
    Friend WithEvents Label9 As Label
    Friend WithEvents Label10 As Label
    Friend WithEvents Label11 As Label
    Friend WithEvents Label12 As Label
    Friend WithEvents tb_soa_series As TextBox
    Friend WithEvents Label13 As Label
    Friend WithEvents Label14 As Label
    Friend WithEvents tb_op_series As TextBox
    Friend WithEvents Label15 As Label
    Friend WithEvents Label16 As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents tb_soa_override As TextBox
    Friend WithEvents cb_soa_override As CheckBox
    Friend WithEvents Label17 As Label
    Friend WithEvents cb_op_override As CheckBox
    Friend WithEvents Label19 As Label
    Friend WithEvents tb_op_override As TextBox
    Friend WithEvents Label18 As Label
End Class
