﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class uc_logs
    Inherits System.Windows.Forms.UserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle9 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle10 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.dgv_soa_logs = New System.Windows.Forms.DataGridView()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.tb_search = New System.Windows.Forms.TextBox()
        Me.cmb_year = New System.Windows.Forms.ComboBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.tb_message = New System.Windows.Forms.TextBox()
        Me.btn_refresh = New Bunifu.Framework.UI.BunifuImageButton()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.cmb_month = New System.Windows.Forms.ComboBox()
        Me.btn_search = New System.Windows.Forms.Button()
        Me.btn_retrieve = New System.Windows.Forms.Button()
        CType(Me.dgv_soa_logs, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.btn_refresh, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'dgv_soa_logs
        '
        Me.dgv_soa_logs.AllowUserToAddRows = False
        Me.dgv_soa_logs.AllowUserToDeleteRows = False
        Me.dgv_soa_logs.AllowUserToResizeColumns = False
        Me.dgv_soa_logs.AllowUserToResizeRows = False
        Me.dgv_soa_logs.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.dgv_soa_logs.BackgroundColor = System.Drawing.Color.FromArgb(CType(CType(28, Byte), Integer), CType(CType(40, Byte), Integer), CType(CType(52, Byte), Integer))
        Me.dgv_soa_logs.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.dgv_soa_logs.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.[Single]
        DataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle9.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle9.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle9.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle9.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle9.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle9.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgv_soa_logs.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle9
        Me.dgv_soa_logs.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        DataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle10.BackColor = System.Drawing.Color.FromArgb(CType(CType(28, Byte), Integer), CType(CType(40, Byte), Integer), CType(CType(52, Byte), Integer))
        DataGridViewCellStyle10.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle10.ForeColor = System.Drawing.Color.White
        DataGridViewCellStyle10.SelectionBackColor = System.Drawing.Color.FromArgb(CType(CType(181, Byte), Integer), CType(CType(186, Byte), Integer), CType(CType(188, Byte), Integer))
        DataGridViewCellStyle10.SelectionForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle10.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgv_soa_logs.DefaultCellStyle = DataGridViewCellStyle10
        Me.dgv_soa_logs.GridColor = System.Drawing.Color.FromArgb(CType(CType(28, Byte), Integer), CType(CType(40, Byte), Integer), CType(CType(52, Byte), Integer))
        Me.dgv_soa_logs.Location = New System.Drawing.Point(3, 66)
        Me.dgv_soa_logs.MultiSelect = False
        Me.dgv_soa_logs.Name = "dgv_soa_logs"
        Me.dgv_soa_logs.ReadOnly = True
        Me.dgv_soa_logs.RowHeadersVisible = False
        Me.dgv_soa_logs.RowTemplate.DefaultCellStyle.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dgv_soa_logs.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgv_soa_logs.Size = New System.Drawing.Size(1315, 586)
        Me.dgv_soa_logs.TabIndex = 10
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Segoe UI", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.DarkGray
        Me.Label1.Location = New System.Drawing.Point(659, 34)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(33, 13)
        Me.Label1.TabIndex = 22
        Me.Label1.Text = "Find "
        '
        'tb_search
        '
        Me.tb_search.BackColor = System.Drawing.Color.FromArgb(CType(CType(28, Byte), Integer), CType(CType(40, Byte), Integer), CType(CType(52, Byte), Integer))
        Me.tb_search.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.tb_search.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tb_search.ForeColor = System.Drawing.Color.White
        Me.tb_search.Location = New System.Drawing.Point(695, 29)
        Me.tb_search.MaxLength = 60
        Me.tb_search.Name = "tb_search"
        Me.tb_search.Size = New System.Drawing.Size(331, 22)
        Me.tb_search.TabIndex = 20
        Me.tb_search.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'cmb_year
        '
        Me.cmb_year.BackColor = System.Drawing.Color.FromArgb(CType(CType(28, Byte), Integer), CType(CType(40, Byte), Integer), CType(CType(52, Byte), Integer))
        Me.cmb_year.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmb_year.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.cmb_year.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmb_year.ForeColor = System.Drawing.Color.White
        Me.cmb_year.FormattingEnabled = True
        Me.cmb_year.Items.AddRange(New Object() {"Unread", "Action Taken", "Unresolved", "All"})
        Me.cmb_year.Location = New System.Drawing.Point(74, 31)
        Me.cmb_year.Name = "cmb_year"
        Me.cmb_year.Size = New System.Drawing.Size(96, 21)
        Me.cmb_year.TabIndex = 23
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Segoe UI", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.DarkGray
        Me.Label2.Location = New System.Drawing.Point(40, 35)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(31, 13)
        Me.Label2.TabIndex = 24
        Me.Label2.Text = "Year"
        '
        'tb_message
        '
        Me.tb_message.BackColor = System.Drawing.Color.FromArgb(CType(CType(44, Byte), Integer), CType(CType(62, Byte), Integer), CType(CType(80, Byte), Integer))
        Me.tb_message.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.tb_message.Font = New System.Drawing.Font("Segoe UI", 14.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tb_message.ForeColor = System.Drawing.Color.DarkGray
        Me.tb_message.Location = New System.Drawing.Point(395, 338)
        Me.tb_message.MaxLength = 25
        Me.tb_message.Name = "tb_message"
        Me.tb_message.Size = New System.Drawing.Size(246, 26)
        Me.tb_message.TabIndex = 25
        Me.tb_message.Text = "Could't connect to server."
        Me.tb_message.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.tb_message.Visible = False
        '
        'btn_refresh
        '
        Me.btn_refresh.BackColor = System.Drawing.Color.Transparent
        Me.btn_refresh.Image = Global.NTC_ACCOUNTING.My.Resources.Resources.refresh
        Me.btn_refresh.ImageActive = Nothing
        Me.btn_refresh.Location = New System.Drawing.Point(644, 334)
        Me.btn_refresh.Name = "btn_refresh"
        Me.btn_refresh.Size = New System.Drawing.Size(34, 34)
        Me.btn_refresh.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.btn_refresh.TabIndex = 26
        Me.btn_refresh.TabStop = False
        Me.btn_refresh.Visible = False
        Me.btn_refresh.Zoom = 10
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Segoe UI", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ForeColor = System.Drawing.Color.DarkGray
        Me.Label3.Location = New System.Drawing.Point(185, 35)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(41, 13)
        Me.Label3.TabIndex = 28
        Me.Label3.Text = "Month"
        '
        'cmb_month
        '
        Me.cmb_month.BackColor = System.Drawing.Color.FromArgb(CType(CType(28, Byte), Integer), CType(CType(40, Byte), Integer), CType(CType(52, Byte), Integer))
        Me.cmb_month.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmb_month.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.cmb_month.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmb_month.ForeColor = System.Drawing.Color.White
        Me.cmb_month.FormattingEnabled = True
        Me.cmb_month.Items.AddRange(New Object() {"January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"})
        Me.cmb_month.Location = New System.Drawing.Point(229, 31)
        Me.cmb_month.Name = "cmb_month"
        Me.cmb_month.Size = New System.Drawing.Size(99, 21)
        Me.cmb_month.TabIndex = 27
        '
        'btn_search
        '
        Me.btn_search.BackColor = System.Drawing.Color.FromArgb(CType(CType(218, Byte), Integer), CType(CType(144, Byte), Integer), CType(CType(27, Byte), Integer))
        Me.btn_search.FlatAppearance.BorderSize = 0
        Me.btn_search.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_search.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_search.ForeColor = System.Drawing.Color.White
        Me.btn_search.Location = New System.Drawing.Point(1027, 29)
        Me.btn_search.Name = "btn_search"
        Me.btn_search.Size = New System.Drawing.Size(110, 22)
        Me.btn_search.TabIndex = 30
        Me.btn_search.Text = "Search"
        Me.btn_search.UseVisualStyleBackColor = False
        '
        'btn_retrieve
        '
        Me.btn_retrieve.BackColor = System.Drawing.Color.FromArgb(CType(CType(218, Byte), Integer), CType(CType(144, Byte), Integer), CType(CType(27, Byte), Integer))
        Me.btn_retrieve.FlatAppearance.BorderSize = 0
        Me.btn_retrieve.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_retrieve.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_retrieve.ForeColor = System.Drawing.Color.White
        Me.btn_retrieve.Location = New System.Drawing.Point(344, 30)
        Me.btn_retrieve.Name = "btn_retrieve"
        Me.btn_retrieve.Size = New System.Drawing.Size(110, 22)
        Me.btn_retrieve.TabIndex = 31
        Me.btn_retrieve.Text = "Retrieve"
        Me.btn_retrieve.UseVisualStyleBackColor = False
        '
        'uc_logs
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(44, Byte), Integer), CType(CType(62, Byte), Integer), CType(CType(80, Byte), Integer))
        Me.Controls.Add(Me.btn_retrieve)
        Me.Controls.Add(Me.btn_search)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.cmb_month)
        Me.Controls.Add(Me.btn_refresh)
        Me.Controls.Add(Me.tb_message)
        Me.Controls.Add(Me.tb_search)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.cmb_year)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.dgv_soa_logs)
        Me.Name = "uc_logs"
        Me.Size = New System.Drawing.Size(1334, 666)
        CType(Me.dgv_soa_logs, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.btn_refresh, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents dgv_soa_logs As DataGridView
    Friend WithEvents Label1 As Label
    Friend WithEvents tb_search As TextBox
    Friend WithEvents cmb_year As ComboBox
    Friend WithEvents Label2 As Label
    Friend WithEvents btn_refresh As Bunifu.Framework.UI.BunifuImageButton
    Friend WithEvents tb_message As TextBox
    Friend WithEvents Label3 As Label
    Friend WithEvents cmb_month As ComboBox
    Friend WithEvents btn_search As Button
    Friend WithEvents btn_retrieve As Button
End Class
