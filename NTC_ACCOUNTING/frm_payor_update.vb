﻿Imports MySql.Data.MySqlClient
Imports System.Net

Public Class frm_payor_update
    Dim query As String
    Dim cmd As MySqlCommand
    Dim reader As MySqlDataReader
    Dim authority_level As String
    Public payor_id As String

    Private Sub btnAddPayor_Click(sender As Object, e As EventArgs) Handles btnAddPayor.Click
        Try
            If txtPayor.Text <> "" And txtAddress.Text <> "" Then
                class_connection.con.Open()
                query = "update tbl_payor set name = '" + WebUtility.HtmlEncode(txtPayor.Text) &
                    "', address = '" & WebUtility.HtmlEncode(txtAddress.Text) & "' where payor_id = '" &
                    payor_id + "'"
                cmd = New MySqlCommand(query, class_connection.con)
                cmd.ExecuteNonQuery()
                class_connection.con.Close()

                MsgBox("Payor information is updated!", MsgBoxStyle.Information, "NTC Region 10")
                frm_payor_list.tb_search.Text = ""
                frm_payor_list.load_payors()
                Close()
            Else
                MsgBox("Fill Up Required Information!", MsgBoxStyle.Exclamation, "NTC Region 10")
            End If
        Catch ex As Exception
            class_connection.con.Close()
            MsgBox("Couldn't connect to server. Contact the IT Administrator for assistance.",
                   MsgBoxStyle.Exclamation, "NTC Region 10")
        End Try
    End Sub
End Class