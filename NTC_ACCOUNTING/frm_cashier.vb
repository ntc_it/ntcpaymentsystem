﻿Imports MySql.Data.MySqlClient
Imports System.Net
Imports Microsoft.Office.Interop
Imports System.IO

Public Class frm_cashier
    Dim cmd As MySqlCommand
    Dim reader As MySqlDataReader
    Dim compute_sub_total, total As Double
    Dim query, cashier_name, mop_cash, mop_check, endorse_id, user_id_cashier, signature_cashier,
        preparator_id, serial_id As String
    Dim row As String()
    Dim isORSerialExist, enable_cashier_signature As Integer
    Dim export_success As Boolean = False
    Public user_id, n_ID, authority_level As String
    Public transact_ID, result As Integer

    Private Sub btn_finish_Click(sender As Object, e As EventArgs) Handles btn_finish.Click
        If txtOR.Text <> "" Then
            If isSerialNotExist() Then
                save_receipt()
            Else
                MsgBox("Receipt number entered already exist!", MsgBoxStyle.Exclamation, "NTC Region 10")
            End If
        Else
            MsgBox("Enter receipt number to finish!", MsgBoxStyle.Exclamation, "NTC Region 10")
        End If
    End Sub

    Function isSerialNotExist()
        ' Check if serial is not exist.
        Try
            Dim isNotExist = True

            class_connection.con.Open()
            query = "select count(or_num) as 'isSerialExist' from tbl_soa_cashier where or_num = '" &
                WebUtility.HtmlEncode(txtOR.Text) & "'"
            cmd = New MySqlCommand(query, class_connection.con)
            reader = cmd.ExecuteReader()

            If reader.Read Then
                If reader.GetInt32("isSerialExist") > 0 Then
                    isNotExist = False
                End If
            End If
            class_connection.con.Close()

            Return isNotExist
        Catch ex As MySqlException
            class_connection.con.Close()
            MsgBox("Couldn't connect to server. Contact the IT Administrator for assistance.",
                   MsgBoxStyle.Exclamation, "NTC Region 10")
            Return Nothing
        Catch ex As Exception
            class_connection.con.Close()
            MsgBox("Something went wrong. Couldn't process the data.", MsgBoxStyle.Exclamation,
                   "NTC Region 10")
            Return Nothing
        End Try
    End Function

    Private Sub btn_soa_Click(sender As Object, e As EventArgs) Handles btn_soa.Click
        ' Display SOA form.
        frm_licenser_log.soa_id_transact = transact_ID.ToString
        frm_licenser_log.user_id = user_id
        frm_licenser_log.authority_level = authority_level
        frm_licenser_log.cash_mod = "1"
        frm_licenser_log.pnl_refresh.Visible = False
        frm_licenser_log.ShowDialog()
        frm_licenser_log.pb_approve_status.Visible = True
    End Sub

    Private Sub btn_op_Click(sender As Object, e As EventArgs) Handles btn_op.Click
        ' Display Order of Payment form.
        frm_accountant_log.authority_level = authority_level
        frm_accountant_log.endorse_id = endorse_id
        frm_accountant_log.btn_print.Visible = True
        frm_accountant_log.btn_soa.Visible = False
        frm_accountant_log.btn_receipt.Visible = False
        frm_accountant_log.cash_mod = "1"
        frm_accountant_log.ShowDialog()
    End Sub

    Private Sub cbCheck_CheckedChanged(sender As Object, e As EventArgs) Handles cbCheck.CheckedChanged
        ' Check if this checkbox is changed.
        If cbCheck.Checked Then
            pnlCheck.Visible = True
            pnlCheck2.Visible = True
            cbCheck.BackColor = Color.FromArgb(114, 126, 138)
        Else
            pnlCheck.Visible = False
            pnlCheck2.Visible = False
            cbCheck.Checked = False
            cbCheck.BackColor = Color.FromArgb(44, 62, 80)
            tb_check_order.Text = ""
        End If
    End Sub

    Private Sub btn_print_Click(sender As Object, e As EventArgs) Handles btn_print.Click
        If cbCash.Checked Then
            export_to_excel()
        ElseIf cbCheck.Checked And tb_check_order.Text <> "" Then
            export_to_excel()
        Else
            MsgBox("Select a mode of payment!", MsgBoxStyle.Exclamation, "NTC Region 10")
        End If
    End Sub

    Sub export_to_excel()
        Try
            Dim xlapp As New Excel.Application
            Dim xlWorksheet As Object
            Dim MAXIMUM As Integer = 13
            Dim loop_counter As Integer = MAXIMUM

            xlapp.Workbooks.Open(Application.StartupPath & "\Receipt.xlsx")
            xlapp.Sheets("Sheet1").Select()
            xlWorksheet = xlapp.Sheets("Sheet1")

            ' Disable button during the process.
            btn_print.Enabled = False

            ' If payment is cash then mark cash type.
            If cbCash.Checked Then
                xlWorksheet.cells(33, 4).value = "X"
            End If

            ' If payment is check then mark check type and fill-up check details.
            If cbCheck.Checked Then
                xlWorksheet.cells(33, 6).value = "X"
                xlWorksheet.cells(35, 8).value = dtp_check_date.Value.ToString("yyyy-MM-dd")
                xlWorksheet.cells(35, 1).value = tb_check_order.Text
            End If

            ' Copy all entries to excel.
            For i As Integer = 0 To dgv_cashier.Rows.Count - 1 Step +1
                xlWorksheet.cells(loop_counter, 1).value = dgv_cashier.Rows(i).Cells(0).Value.ToString()
                xlWorksheet.cells(loop_counter, 3).value = dgv_cashier.Rows(i).Cells(1).Value.ToString()
                xlWorksheet.cells(loop_counter, 10).value = dgv_cashier.Rows(i).Cells(2).Value.ToString()
                loop_counter += 1
            Next

            ' Copy receipt details to excel.
            xlWorksheet.cells(6, 10).value = txtdate.Text
            xlWorksheet.cells(8, 1).value = tb_payor.Text
            xlWorksheet.cells(30, 1).value = txtAmountInWords.Text
            xlWorksheet.cells(34, 9).value = cashier_name
            xlWorksheet.cells(28, 10).value = tb_total.Text

            download_signature_cashier()

            ' If cashier checkbox is checked and cashier signature is enabled.
            If cb_cashier_signature.Checked And enable_cashier_signature = "1" Then
                If File.Exists("C:\temp\" + signature_cashier) Then
                    xlWorksheet.Shapes.AddPicture("C:\temp\" + signature_cashier,
                    Microsoft.Office.Core.MsoTriState.msoFalse,
                    Microsoft.Office.Core.MsoTriState.msoCTrue, 355, 320, 90, 72)
                End If
            End If

            ' Set excel program to be visible, maximized and on top.
            xlapp.Visible = True
            xlapp.Top = 1
            xlapp.ActiveWindow.WindowState = Excel.XlWindowState.xlMaximized

            ' Enable print button and show finish panel.
            pnl_finish.Visible = True
            btn_print.Enabled = True
        Catch ex As Exception
            pnl_finish.Visible = False
            btn_print.Enabled = True
            MsgBox("An error occured in processing the receipt, data may not be properly retrieved. " &
                   "Please close the excel file and try printing again.", MsgBoxStyle.Exclamation,
                   "NTC Region 10")
        End Try
    End Sub

    Sub download_signature_cashier()
        'Retrieve cashier's signature.
        Try
            Dim WebClient As WebClient = New WebClient()

            class_connection.con.Open()
            query = "select signature as 'path', signature_name as 'name', enable_signature from " &
                "tbl_user where user_id = '" & user_id & "'"
            cmd = New MySqlCommand(query, class_connection.con)
            reader = cmd.ExecuteReader()

            If reader.Read Then
                If reader.GetString("enable_signature") = "1" Then
                    enable_cashier_signature = reader.GetInt32("enable_signature")
                    signature_cashier = reader.GetString("name")

                    If Not Directory.Exists("C:\Temp") Then
                        Directory.CreateDirectory("C:\Temp")
                    End If

                    WebClient.Credentials = New NetworkCredential("ntc_10_server", "ntc_10_server",
                        "ftp://" & class_connection.ftpAddress)
                    WebClient.DownloadFile(WebUtility.HtmlDecode(reader.GetString("path")),
                        "C:\temp\" & reader.GetString("name"))
                End If
            End If
            class_connection.con.Close()
        Catch ex As WebException
            class_connection.con.Close()
        Catch ex As Exception
            class_connection.con.Close()
            MsgBox("Something went wrong. Couldn't process the data.", MsgBoxStyle.Exclamation,
                "NTC Region 10")
        End Try
    End Sub

    Sub save_receipt()
        Try
            Dim receipt_number = WebUtility.HtmlEncode(txtOR.Text)
            Dim date_setted_full = txtdate.Value.ToString("yyyy-MM-dd HH:mm:ss")
            Dim date_setted_month = txtdate.Value.ToString("MM")
            Dim date_setted_year = txtdate.Value.ToString("yyyy")
            Dim check_order = WebUtility.HtmlEncode(tb_check_order.Text)
            Dim date_setted = txtdate.Value.ToString("yyyy-MM-dd")
            Dim check_date = dtp_check_date.Value.ToString("yyyy-MM-dd")
            Dim message = "", title = ""
            Dim user_notifications As New uc_notifications

            ' Update this process' notification to action taken.
            class_connection.con.Open()
            query = "update tbl_notifications Set action_taken = '1' where n_ID = '" & n_ID & "'"
            cmd = New MySqlCommand(query, class_connection.con)
            cmd.ExecuteNonQuery()
            class_connection.con.Close()

            ' Create receipt entry.
            class_connection.con.Open()
            If cbCash.Checked And cbCheck.Checked Then
                query = "insert into tbl_soa_cashier(soa_id, or_num, date_transact, or_month, or_year, " &
                    "mode_of_payment, check_number, check_date, user_id_cashier, op_id) Values('" &
                    transact_ID.ToString & "', '" & receipt_number & "', '" & date_setted_full & "', '" &
                    date_setted_month & "', '" & date_setted_year & "','2', '" & check_order & "', '" &
                    date_setted & "', '" & user_id & "','" & endorse_id & "')"
            ElseIf cbCash.Checked And Not cbCheck.Checked Then
                query = "insert into tbl_soa_cashier(soa_id, or_num, date_transact, or_month, or_year, " &
                    "mode_of_payment, check_number, check_date, user_id_cashier, op_id) Values('" &
                    transact_ID.ToString & "', '" & receipt_number & "', '" & date_setted_full & "', '" &
                    date_setted_month & "', '" & date_setted_year & "', '0', '', '" & date_setted &
                    "', '" & user_id & "','" & endorse_id & "')"
            ElseIf Not cbCash.Checked And cbCheck.Checked Then
                query = "insert into tbl_soa_cashier(soa_id, or_num, date_transact, or_month, or_year, " &
                    "mode_of_payment, check_number, check_date, user_id_cashier, op_id) Values('" &
                    transact_ID.ToString & "', '" & receipt_number & "', '" & date_setted_full &
                    "',  '" & date_setted_month & "', '" & date_setted_year & "','1', '" & check_order &
                    "', '" & check_date & "', '" & user_id & "', '" & endorse_id & "')"
            End If
            cmd = New MySqlCommand(query, class_connection.con)
            cmd.ExecuteNonQuery()
            class_connection.con.Close()

            ' Message for SOA Preparator when received.
            message = "Payment has been made for SOA Serial No. " & serial_id & ". Click the button below to view."
            title = "Payment Successful"

            ' Create paid notification to SOA preparator. 
            class_connection.con.Open()
            query = "insert into tbl_notifications(message_title, message_content, transaction_id, " &
                "user_id_notified, user_id_notifier, user_opened, date_created, action_taken, message_type) " &
                "Values('" & title & "', '" & message & "', '" & transact_ID.ToString & "', '" &
                preparator_id & "', '" & user_id & "','0','" & date_setted_full & "','1','9')"
            cmd = New MySqlCommand(query, class_connection.con)
            cmd.ExecuteNonQuery()
            class_connection.con.Close()

            ' Reload notifications.
            dashboard.uc_notifications.load_notifications()
            dashboard.uc_notifications.retrieve_number_of_unresolved()

            ' Hide finish panel after saving the receipt.
            pnl_finish.Visible = False

            Close()
        Catch ex As MySqlException
            class_connection.con.Close()
            MsgBox("Couldn't connect to server. Contact the IT Administrator for assistance.",
                MsgBoxStyle.Exclamation, "NTC Region 10")
        Catch ex As Exception
            class_connection.con.Close()
            MsgBox("Something went wrong. Couldn't process the data.", MsgBoxStyle.Exclamation, "NTC Region 10")
        End Try
    End Sub

    Private Sub frm_cashier_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Try
            ' Retrieve endorse ID.
            class_connection.con.Open()
            query = "select endorse_id from tbl_soa_endorse where soa_id = '" & transact_ID.ToString & "'"
            cmd = New MySqlCommand(query, class_connection.con)
            reader = cmd.ExecuteReader()

            If reader.Read Then
                endorse_id = reader.GetString("endorse_id")
            End If
            class_connection.con.Close()

            ' Retrieve name of user.
            class_connection.con.Open()
            query = "select full_name from tbl_user where user_id = '" & user_id & "'"
            cmd = New MySqlCommand(query, class_connection.con)
            reader = cmd.ExecuteReader()

            If reader.Read Then
                cashier_name = WebUtility.HtmlDecode(reader.GetString("full_name"))
            End If
            class_connection.con.Close()

            ' Retrieve payor's name, SOA serial ID and SOA preparator ID.
            class_connection.con.Open()
            query = "select name, serial_id, tbl_soa_transact.user_id as 'preparator' from tbl_soa_transact " &
                "join tbl_soa_details on tbl_soa_details.soa_details_id = tbl_soa_transact.soa_details_id join" &
                " tbl_payor on tbl_payor.payor_id = tbl_soa_details.payor_id where tbl_soa_transact.soa_id = '" &
                transact_ID.ToString & "'"
            cmd = New MySqlCommand(query, class_connection.con)
            reader = cmd.ExecuteReader()

            If reader.Read Then
                tb_payor.Text = WebUtility.HtmlDecode(reader.GetString("name"))
                preparator_id = reader.GetString("preparator")
                serial_id = reader.GetString("serial_id")
            End If
            class_connection.con.Close()

            ' Initialize datagridview columns.
            initialize_table()

            ' Retrieve SOA data and compute.
            class_connection.con.Open()
            query = "select code as 'Code', description as 'Description', cat 'Category',others_custom_value , " &
                "p1_no_of_years as 'No. of Years 1', p1_percent as 'Percent 1', p1_no_of_units as 'No. of Units 1'," &
                "p1_fees as 'Fees 1', p2_no_of_years as 'No. of Years 2', p2_percent as 'Percent 2', p2_no_of_units " &
                "as 'No. of Units 2', p2_fees as 'Fees 2', p3_no_of_years as 'No. of Years 3', p3_percent as 'Percent " &
                "3', p3_no_of_units as 'No. of Units 3', p3_fees as 'Fees 3' from tbl_soa_transact join tbl_soa_details" &
                " on tbl_soa_details.soa_details_id = tbl_soa_transact.soa_details_id join tbl_soa_data on " &
                "tbl_soa_data.soa_data_id = tbl_soa_details.soa_data_id join tbl_mode_of_payment on " &
                "tbl_mode_of_payment.mop_id = tbl_soa_data.mop_id  where tbl_soa_transact.soa_id = '" &
                transact_ID.ToString & "' order by tbl_mode_of_payment.mop_id"
            cmd = New MySqlCommand(query, class_connection.con)
            reader = cmd.ExecuteReader()

            While reader.Read
                Dim value As Double = reader.GetDouble("Percent 1")
                Dim value2 As Double = reader.GetDouble("Percent 2")
                Dim value3 As Double = reader.GetDouble("Percent 3")
                Dim num, num2, num3, percentage, percentage2, percentage3 As Double
                Dim description = WebUtility.HtmlDecode(reader.GetString("Description"))
                Dim custom_value = WebUtility.HtmlDecode(reader.GetString("others_custom_value"))
                Dim entry_code = reader.GetString("Code")

                ' Convert percentage to double.
                If Double.TryParse(value, num) And Double.TryParse(value2, num2) And Double.TryParse(value3, num3) Then
                    percentage = num / 100
                    percentage2 = num2 / 100
                    percentage3 = num3 / 100
                End If

                ' Compute sub-total of each SOA data entries.
                compute_sub_total = (reader.GetDouble("No. of Years 1") * percentage * reader.GetDouble("No. of Units 1") _
                    * reader.GetDouble("Fees 1")) + (reader.GetDouble("No. of Years 2") * percentage2 *
                    reader.GetDouble("No. of Units 2") * reader.GetDouble("Fees 2")) + (reader.GetDouble("No. of Years 3") *
                    percentage3 * reader.GetDouble("No. of Units 3") * reader.GetDouble("Fees 3"))
                compute_sub_total = FormatNumber(CDbl(compute_sub_total), 2)

                ' Create SOA data entry.
                row = New String() {entry_code, description, compute_sub_total}

                ' Modify entry if it has custom value.
                If reader.GetString("others_custom_value") <> "" Then
                    row = New String() {entry_code, (description & ": " & custom_value), compute_sub_total}
                End If

                ' Add entry into datagrid.
                dgv_cashier.Rows.Add(row)
            End While
            class_connection.con.Close()

            ' Set each column to not sortable.
            For Each column As DataGridViewColumn In dgv_cashier.Columns
                column.SortMode = DataGridViewColumnSortMode.NotSortable
            Next

            ' Compute all sub-total to total.
            For i = 0 To dgv_cashier.Rows.Count - 1 Step +1
                Try
                    total += dgv_cashier.Rows(i).Cells("Sub-total").Value
                Catch ex As Exception
                    total += 0
                End Try
            Next

            ' Set total to textbox.
            total = FormatNumber(CDbl(total), 2)
            tb_total.Text = total

            ' Convert total to amount in words 
            txtAmountInWords.Text = " " & class_number_to_word.ConvertNumberToENG(tb_total.Text)

            isSignatureCashierEnabled()

            Icon = My.Resources.ntc_material_logo
        Catch ex As MySqlException
            class_connection.con.Close()
            MsgBox("Couldn't connect to server. Contact the IT Administrator for assistance.", MsgBoxStyle.Exclamation,
                "NTC Region 10")
            Close()
        Catch ex As Exception
            class_connection.con.Close()
            MsgBox("Something went wrong. Couldn't process the data.", MsgBoxStyle.Exclamation, "NTC Region 10")
            Close()
        End Try
    End Sub

    Sub isSignatureCashierEnabled()
        ' Checkbox is enabled if cashier's signature is enabled and disabled otherwise. 
        class_connection.con.Open()
        query = "select enable_signature from tbl_user where user_id = '" & user_id & "'"
        cmd = New MySqlCommand(query, class_connection.con)
        reader = cmd.ExecuteReader()

        If reader.Read Then
            If reader.GetString("enable_signature") = "1" Then
                cb_cashier_signature.Enabled = True
            Else
                cb_cashier_signature.Enabled = False
            End If
        End If
        class_connection.con.Close()
    End Sub

    Sub initialize_table()
        dgv_cashier.ColumnCount = 3
        dgv_cashier.Columns(0).Name = "Code"
        dgv_cashier.Columns(0).ReadOnly = True
        dgv_cashier.Columns(0).Width = 70
        dgv_cashier.Columns(1).Name = "Description"
        dgv_cashier.Columns(1).ReadOnly = True
        dgv_cashier.Columns(1).Width = 300
        dgv_cashier.Columns(2).Name = "Sub-Total"
        dgv_cashier.Columns(2).ReadOnly = True
        dgv_cashier.Columns(2).Width = 100
        dgv_cashier.Rows.Clear()
    End Sub
End Class