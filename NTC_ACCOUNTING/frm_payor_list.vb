﻿Imports MySql.Data.MySqlClient
Imports System.Net

Public Class frm_payor_list
    Dim query As String
    Dim cmd As MySqlCommand
    Dim reader As MySqlDataReader
    Public accessFrom As Integer

    Private Sub payorInfofrm_Load(sender As Object, e As EventArgs) Handles Me.Load
        load_payors()

        btnAddPayor.Enabled = False
    End Sub

    Sub initialize_table()
        dgv_payor.ColumnCount = 3
        dgv_payor.Columns(0).Name = "#"
        dgv_payor.Columns(0).ReadOnly = True
        dgv_payor.Columns(0).Visible = False
        dgv_payor.Columns(1).Name = "Name"
        dgv_payor.Columns(1).ReadOnly = True
        dgv_payor.Columns(2).Name = "Address"
        dgv_payor.Columns(2).ReadOnly = True
    End Sub

    Public Sub load_payors()
        Try
            initialize_table()

            dgv_payor.Rows.Clear()

            ' Load payors to datagridview.
            class_connection.con.Open()
            query = "select payor_id as '#', name as 'Name', address as 'Address' from tbl_payor " &
                "order by payor_id DESC"
            cmd = New MySqlCommand(query, class_connection.con)
            reader = cmd.ExecuteReader()

            While reader.Read
                Dim payor_id = reader.GetString("#")
                Dim payor_name = WebUtility.HtmlDecode(reader.GetString("Name"))
                Dim payor_address = WebUtility.HtmlDecode(reader.GetString("Address"))
                Dim add_row_content As String() = New String() {payor_id, payor_name, payor_address}

                dgv_payor.Rows.Add(add_row_content)
            End While
            class_connection.con.Close()

            ' Other settings.
            tb_search.Enabled = True
            dgv_payor.Visible = True
            tb_message.Visible = False
            btn_refresh.Visible = False
        Catch ex As MySqlException
            class_connection.con.Close()
            tb_search.Enabled = False
            dgv_payor.Visible = False
            tb_message.Visible = True
            btn_refresh.Visible = True
        Catch ex As Exception
            class_connection.con.Close()
            tb_search.Enabled = False
            dgv_payor.Visible = False
            tb_message.Visible = True
            btn_refresh.Visible = True
        End Try
    End Sub

    Sub addPayor()
        If txtPayor.Text <> "" And txtAddress.Text <> "" Then
            Try
                Dim payor_name = WebUtility.HtmlEncode(txtPayor.Text)
                Dim payor_address = WebUtility.HtmlEncode(txtAddress.Text)

                ' Insert payor to database.
                class_connection.con.Open()
                query = "insert into tbl_payor(name, address) Values('" & payor_name & "', '" &
                    payor_address & "')"
                cmd = New MySqlCommand(query, class_connection.con)
                cmd.ExecuteNonQuery()
                class_connection.con.Close()

                load_payors()

                ' Clear textboxes' value.
                txtPayor.Text = ""
                txtAddress.Text = ""
                pb_approve_status.Visible = False

                MsgBox("New payor has been added!", MsgBoxStyle.Information, "NTC Region 10")
            Catch ex As MySqlException
                class_connection.con.Close()
                MsgBox("Couldn't connect to server. Contact the IT Administrator for assistance.",
                    MsgBoxStyle.Exclamation, "NTC Region 10")
                load_payors()
            Catch ex As Exception
                class_connection.con.Close()
                MsgBox("Something went wrong. Couldn't process the data.",
                    MsgBoxStyle.Exclamation, "NTC Region 10")
            End Try
        Else
            MsgBox("Fill-up required details.", MsgBoxStyle.Exclamation, "NTC Region 10")
        End If
    End Sub

    Private Sub BunifuImageButton1_Click(sender As Object, e As EventArgs) Handles btn_refresh.Click
        tb_search.Text = ""

        load_payors()
    End Sub

    Private Sub btnAddPayor_Click(sender As Object, e As EventArgs) Handles btnAddPayor.Click
        addPayor()
    End Sub

    Private Sub dgv_payor_CellDoubleClick(sender As Object, e As DataGridViewCellEventArgs) Handles _
        dgv_payor.CellDoubleClick
        ' Get payor's id and name and return to original form.
        If e.RowIndex >= 0 Then
            Dim row As DataGridViewRow

            If accessFrom = 1 Then
                ' Redirects to create SOA form.
                row = dgv_payor.Rows(e.RowIndex)
                frm_licenser.txtpayor.Text = row.Cells("Name").Value.ToString
                frm_licenser.payor_id = row.Cells("#").Value.ToString
                frm_licenser.isSetPayor = True

                Close()
            ElseIf accessFrom = 2 Then
                ' Redirects to edit SOA form.
                row = dgv_payor.Rows(e.RowIndex)
                frm_licenser_edit.txtpayor.Text = row.Cells("Name").Value.ToString
                frm_licenser_edit.payor_id = row.Cells("#").Value.ToString
                frm_licenser_edit.isSetPayor = True

                Close()
            ElseIf accessFrom = 3 Then
                ' Redirects to manual creation of SOA form.
                row = dgv_payor.Rows(e.RowIndex)
                frm_licenser_manual.txtpayor.Text = row.Cells("Name").Value.ToString
                frm_licenser_manual.payor_id = row.Cells("#").Value.ToString
                frm_licenser_manual.isSetPayor = True

                Close()
            End If
        End If
    End Sub
    Private Sub tsmi_cms_update_payor_Click(sender As Object, e As EventArgs) Handles _
        tsmi_cms_update_payor.Click
        ' Opens edit mode for selected payor.
        Try
            For Each row As DataGridViewRow In dgv_payor.SelectedRows
                If row.Cells("#").Value.ToString <> "" Then
                    frm_payor_update.payor_id = row.Cells("#").Value.ToString
                    frm_payor_update.txtPayor.Text = row.Cells("Name").Value.ToString()
                    frm_payor_update.txtAddress.Text = row.Cells("Address").Value.ToString()
                    frm_payor_update.ShowDialog()
                End If
            Next
        Catch ex As Exception
            MsgBox("Something went wrong. Couldn't process the data.", MsgBoxStyle.Exclamation,
                "NTC Region 10")
        End Try
    End Sub

    Sub search_payor(query_payor_name)
        Try
            ' Clear datagridview rows.
            dgv_payor.Rows.Clear()

            ' Load payor into datagridview.
            class_connection.con.Open()
            query = "select payor_id as '#', name as 'Name', address as 'Address' from tbl_payor " &
                "where Name Like '" & query_payor_name & "%'"
            cmd = New MySqlCommand(query, class_connection.con)
            reader = cmd.ExecuteReader()

            While reader.Read
                Dim payor_id = reader.GetString("#")
                Dim payor_name = WebUtility.HtmlDecode(reader.GetString("Name"))
                Dim payor_address = WebUtility.HtmlDecode(reader.GetString("Address"))
                Dim add_row_content As String() = New String() {payor_id, payor_name, payor_address}

                dgv_payor.Rows.Add(add_row_content)
            End While
            class_connection.con.Close()
        Catch ex As MySqlException
            class_connection.con.Close()
            tb_search.Enabled = False
            dgv_payor.Visible = False
            tb_message.Visible = True
            btn_refresh.Visible = True
        Catch ex As Exception
            class_connection.con.Close()
            MsgBox("Something went wrong. Couldn't process the data.", MsgBoxStyle.Exclamation,
                "NTC Region 10")
        End Try
    End Sub

    Private Sub btn_search_Click(sender As Object, e As EventArgs) Handles btn_search.Click
        If tb_search.Text = "" Then
            load_payors()
        Else
            search_payor(tb_search.Text)
        End If
    End Sub

    Private Sub btn_check_Click(sender As Object, e As EventArgs) Handles btn_check.Click
        If txtPayor.Text <> "" Then
            Try
                search_payor(txtPayor.Text)

                If dgv_payor.Rows.Count = 0 Then
                    btnAddPayor.Enabled = True
                    pb_approve_status.Visible = True
                ElseIf dgv_payor.Rows.Count > 0 Then
                    btnAddPayor.Enabled = False
                    pb_approve_status.Visible = False
                End If
            Catch ex As Exception
                tb_search.Enabled = False
                dgv_payor.Visible = False
                tb_message.Visible = True
                btn_refresh.Visible = True
            End Try
        End If
    End Sub

    Private Sub txtPayor_TextChanged(sender As Object, e As EventArgs) Handles txtPayor.TextChanged
        btnAddPayor.Enabled = False
        pb_approve_status.Visible = False
    End Sub

    Private Sub btn_refresh_two_Click(sender As Object, e As EventArgs) Handles btn_refresh_two.Click
        load_payors()

        txtPayor.Text = ""
        txtAddress.Text = ""
        tb_search.Text = ""
    End Sub

    Private Sub tb_search_KeyDown(sender As Object, e As KeyEventArgs) Handles tb_search.KeyDown
        If e.KeyCode = Keys.Enter Then
            If tb_search.Text = "" Then
                load_payors()
            Else
                search_payor(tb_search.Text)
            End If
        End If
    End Sub
End Class