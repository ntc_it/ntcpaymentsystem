﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_accountant_manual
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btn_finish = New System.Windows.Forms.Button()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.tb_bank_payment_two = New System.Windows.Forms.TextBox()
        Me.tb_bank_name_two = New System.Windows.Forms.TextBox()
        Me.tb_bank_num_two = New System.Windows.Forms.TextBox()
        Me.Label22 = New System.Windows.Forms.Label()
        Me.Label21 = New System.Windows.Forms.Label()
        Me.tb_bank_total = New System.Windows.Forms.TextBox()
        Me.tb_bank_payment = New System.Windows.Forms.TextBox()
        Me.tb_bank_name = New System.Windows.Forms.TextBox()
        Me.tb_bank_num = New System.Windows.Forms.TextBox()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.btn_cancel = New System.Windows.Forms.Button()
        Me.btn_back = New System.Windows.Forms.Button()
        Me.tb_fund_cluster = New System.Windows.Forms.TextBox()
        Me.tb_payment = New System.Windows.Forms.TextBox()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.tb_dated = New System.Windows.Forms.TextBox()
        Me.tb_per_bill = New System.Windows.Forms.TextBox()
        Me.tb_purpose_two = New System.Windows.Forms.TextBox()
        Me.tb_purpose_one = New System.Windows.Forms.TextBox()
        Me.tb_word_figure = New System.Windows.Forms.TextBox()
        Me.tb_address = New System.Windows.Forms.TextBox()
        Me.tb_payor = New System.Windows.Forms.TextBox()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.RadTitleBar1 = New Telerik.WinControls.UI.RadTitleBar()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.Label24 = New System.Windows.Forms.Label()
        Me.tb_serial_series = New System.Windows.Forms.TextBox()
        Me.tb_serial_month = New System.Windows.Forms.TextBox()
        Me.tb_serial_year = New System.Windows.Forms.TextBox()
        Me.tb_serial_starting_digit = New System.Windows.Forms.TextBox()
        Me.cmb_accountant_id = New System.Windows.Forms.ComboBox()
        Me.cmb_accountant = New System.Windows.Forms.ComboBox()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.dtp_date_created = New System.Windows.Forms.DateTimePicker()
        Me.dgv_or = New System.Windows.Forms.DataGridView()
        Me.rbAuto = New System.Windows.Forms.RadioButton()
        Me.rbCustom = New System.Windows.Forms.RadioButton()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.Panel1.SuspendLayout()
        CType(Me.RadTitleBar1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgv_or, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel2.SuspendLayout()
        Me.SuspendLayout()
        '
        'btn_finish
        '
        Me.btn_finish.BackColor = System.Drawing.Color.FromArgb(CType(CType(218, Byte), Integer), CType(CType(144, Byte), Integer), CType(CType(27, Byte), Integer))
        Me.btn_finish.FlatAppearance.BorderSize = 0
        Me.btn_finish.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_finish.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_finish.ForeColor = System.Drawing.Color.White
        Me.btn_finish.Location = New System.Drawing.Point(145, 13)
        Me.btn_finish.Name = "btn_finish"
        Me.btn_finish.Size = New System.Drawing.Size(123, 35)
        Me.btn_finish.TabIndex = 13
        Me.btn_finish.Text = "Save and Proceed to OR"
        Me.btn_finish.UseVisualStyleBackColor = False
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Font = New System.Drawing.Font("Segoe UI", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label13.ForeColor = System.Drawing.Color.White
        Me.Label13.Location = New System.Drawing.Point(124, 453)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(124, 20)
        Me.Label13.TabIndex = 187
        Me.Label13.Text = "Bank Account/s:"
        '
        'tb_bank_payment_two
        '
        Me.tb_bank_payment_two.BackColor = System.Drawing.Color.FromArgb(CType(CType(114, Byte), Integer), CType(CType(126, Byte), Integer), CType(CType(138, Byte), Integer))
        Me.tb_bank_payment_two.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.tb_bank_payment_two.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tb_bank_payment_two.ForeColor = System.Drawing.Color.White
        Me.tb_bank_payment_two.Location = New System.Drawing.Point(561, 524)
        Me.tb_bank_payment_two.Name = "tb_bank_payment_two"
        Me.tb_bank_payment_two.ReadOnly = True
        Me.tb_bank_payment_two.Size = New System.Drawing.Size(179, 18)
        Me.tb_bank_payment_two.TabIndex = 186
        Me.tb_bank_payment_two.TabStop = False
        Me.tb_bank_payment_two.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'tb_bank_name_two
        '
        Me.tb_bank_name_two.BackColor = System.Drawing.Color.FromArgb(CType(CType(28, Byte), Integer), CType(CType(40, Byte), Integer), CType(CType(52, Byte), Integer))
        Me.tb_bank_name_two.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.tb_bank_name_two.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tb_bank_name_two.ForeColor = System.Drawing.Color.White
        Me.tb_bank_name_two.Location = New System.Drawing.Point(285, 524)
        Me.tb_bank_name_two.MaxLength = 120
        Me.tb_bank_name_two.Name = "tb_bank_name_two"
        Me.tb_bank_name_two.Size = New System.Drawing.Size(268, 18)
        Me.tb_bank_name_two.TabIndex = 10
        Me.tb_bank_name_two.Text = "Land Bank of the Philippines"
        Me.tb_bank_name_two.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'tb_bank_num_two
        '
        Me.tb_bank_num_two.BackColor = System.Drawing.Color.FromArgb(CType(CType(28, Byte), Integer), CType(CType(40, Byte), Integer), CType(CType(52, Byte), Integer))
        Me.tb_bank_num_two.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.tb_bank_num_two.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tb_bank_num_two.ForeColor = System.Drawing.Color.White
        Me.tb_bank_num_two.Location = New System.Drawing.Point(97, 524)
        Me.tb_bank_num_two.MaxLength = 120
        Me.tb_bank_num_two.Name = "tb_bank_num_two"
        Me.tb_bank_num_two.Size = New System.Drawing.Size(179, 18)
        Me.tb_bank_num_two.TabIndex = 9
        Me.tb_bank_num_two.Text = "0152-1001-75"
        Me.tb_bank_num_two.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label22
        '
        Me.Label22.AutoSize = True
        Me.Label22.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label22.ForeColor = System.Drawing.Color.DarkGray
        Me.Label22.Location = New System.Drawing.Point(121, 583)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(43, 15)
        Me.Label22.TabIndex = 181
        Me.Label22.Text = "TOTAL"
        '
        'Label21
        '
        Me.Label21.AutoSize = True
        Me.Label21.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label21.ForeColor = System.Drawing.Color.White
        Me.Label21.Location = New System.Drawing.Point(526, 578)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(30, 15)
        Me.Label21.TabIndex = 180
        Me.Label21.Text = "PHP"
        '
        'tb_bank_total
        '
        Me.tb_bank_total.BackColor = System.Drawing.Color.FromArgb(CType(CType(114, Byte), Integer), CType(CType(126, Byte), Integer), CType(CType(138, Byte), Integer))
        Me.tb_bank_total.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.tb_bank_total.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tb_bank_total.ForeColor = System.Drawing.Color.White
        Me.tb_bank_total.Location = New System.Drawing.Point(561, 576)
        Me.tb_bank_total.Name = "tb_bank_total"
        Me.tb_bank_total.ReadOnly = True
        Me.tb_bank_total.Size = New System.Drawing.Size(179, 18)
        Me.tb_bank_total.TabIndex = 179
        Me.tb_bank_total.TabStop = False
        Me.tb_bank_total.Text = "0"
        Me.tb_bank_total.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'tb_bank_payment
        '
        Me.tb_bank_payment.BackColor = System.Drawing.Color.FromArgb(CType(CType(114, Byte), Integer), CType(CType(126, Byte), Integer), CType(CType(138, Byte), Integer))
        Me.tb_bank_payment.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.tb_bank_payment.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tb_bank_payment.ForeColor = System.Drawing.Color.White
        Me.tb_bank_payment.Location = New System.Drawing.Point(561, 500)
        Me.tb_bank_payment.Name = "tb_bank_payment"
        Me.tb_bank_payment.ReadOnly = True
        Me.tb_bank_payment.Size = New System.Drawing.Size(179, 18)
        Me.tb_bank_payment.TabIndex = 178
        Me.tb_bank_payment.TabStop = False
        Me.tb_bank_payment.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'tb_bank_name
        '
        Me.tb_bank_name.BackColor = System.Drawing.Color.FromArgb(CType(CType(28, Byte), Integer), CType(CType(40, Byte), Integer), CType(CType(52, Byte), Integer))
        Me.tb_bank_name.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.tb_bank_name.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tb_bank_name.ForeColor = System.Drawing.Color.White
        Me.tb_bank_name.Location = New System.Drawing.Point(285, 500)
        Me.tb_bank_name.MaxLength = 120
        Me.tb_bank_name.Name = "tb_bank_name"
        Me.tb_bank_name.Size = New System.Drawing.Size(268, 18)
        Me.tb_bank_name.TabIndex = 8
        Me.tb_bank_name.Text = "Land Bank of the Philippines"
        Me.tb_bank_name.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'tb_bank_num
        '
        Me.tb_bank_num.BackColor = System.Drawing.Color.FromArgb(CType(CType(28, Byte), Integer), CType(CType(40, Byte), Integer), CType(CType(52, Byte), Integer))
        Me.tb_bank_num.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.tb_bank_num.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tb_bank_num.ForeColor = System.Drawing.Color.White
        Me.tb_bank_num.Location = New System.Drawing.Point(97, 500)
        Me.tb_bank_num.MaxLength = 120
        Me.tb_bank_num.Name = "tb_bank_num"
        Me.tb_bank_num.Size = New System.Drawing.Size(179, 18)
        Me.tb_bank_num.TabIndex = 7
        Me.tb_bank_num.Text = "3402-2642-40"
        Me.tb_bank_num.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.FromArgb(CType(CType(28, Byte), Integer), CType(CType(40, Byte), Integer), CType(CType(52, Byte), Integer))
        Me.Panel1.Controls.Add(Me.btn_cancel)
        Me.Panel1.Controls.Add(Me.btn_back)
        Me.Panel1.Controls.Add(Me.btn_finish)
        Me.Panel1.Location = New System.Drawing.Point(19, 629)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(413, 62)
        Me.Panel1.TabIndex = 173
        '
        'btn_cancel
        '
        Me.btn_cancel.BackColor = System.Drawing.Color.FromArgb(CType(CType(218, Byte), Integer), CType(CType(144, Byte), Integer), CType(CType(27, Byte), Integer))
        Me.btn_cancel.FlatAppearance.BorderSize = 0
        Me.btn_cancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_cancel.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_cancel.ForeColor = System.Drawing.Color.White
        Me.btn_cancel.Location = New System.Drawing.Point(278, 13)
        Me.btn_cancel.Name = "btn_cancel"
        Me.btn_cancel.Size = New System.Drawing.Size(123, 35)
        Me.btn_cancel.TabIndex = 14
        Me.btn_cancel.Text = "Cancel"
        Me.btn_cancel.UseVisualStyleBackColor = False
        '
        'btn_back
        '
        Me.btn_back.BackColor = System.Drawing.Color.FromArgb(CType(CType(218, Byte), Integer), CType(CType(144, Byte), Integer), CType(CType(27, Byte), Integer))
        Me.btn_back.FlatAppearance.BorderSize = 0
        Me.btn_back.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_back.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_back.ForeColor = System.Drawing.Color.White
        Me.btn_back.Location = New System.Drawing.Point(13, 13)
        Me.btn_back.Name = "btn_back"
        Me.btn_back.Size = New System.Drawing.Size(123, 35)
        Me.btn_back.TabIndex = 12
        Me.btn_back.Text = "Back to SOA"
        Me.btn_back.UseVisualStyleBackColor = False
        '
        'tb_fund_cluster
        '
        Me.tb_fund_cluster.BackColor = System.Drawing.Color.FromArgb(CType(CType(28, Byte), Integer), CType(CType(40, Byte), Integer), CType(CType(52, Byte), Integer))
        Me.tb_fund_cluster.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.tb_fund_cluster.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tb_fund_cluster.ForeColor = System.Drawing.Color.White
        Me.tb_fund_cluster.Location = New System.Drawing.Point(26, 50)
        Me.tb_fund_cluster.MaxLength = 30
        Me.tb_fund_cluster.Name = "tb_fund_cluster"
        Me.tb_fund_cluster.Size = New System.Drawing.Size(231, 22)
        Me.tb_fund_cluster.TabIndex = 6
        Me.tb_fund_cluster.Text = "Code 101"
        Me.tb_fund_cluster.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'tb_payment
        '
        Me.tb_payment.BackColor = System.Drawing.Color.FromArgb(CType(CType(114, Byte), Integer), CType(CType(126, Byte), Integer), CType(CType(138, Byte), Integer))
        Me.tb_payment.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.tb_payment.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tb_payment.ForeColor = System.Drawing.Color.White
        Me.tb_payment.Location = New System.Drawing.Point(705, 283)
        Me.tb_payment.MaxLength = 120
        Me.tb_payment.Name = "tb_payment"
        Me.tb_payment.ReadOnly = True
        Me.tb_payment.Size = New System.Drawing.Size(107, 22)
        Me.tb_payment.TabIndex = 168
        Me.tb_payment.TabStop = False
        Me.tb_payment.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.Font = New System.Drawing.Font("Segoe UI", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label19.ForeColor = System.Drawing.Color.DarkGray
        Me.Label19.Location = New System.Drawing.Point(626, 482)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(49, 13)
        Me.Label19.TabIndex = 167
        Me.Label19.Text = "Amount"
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Font = New System.Drawing.Font("Segoe UI", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label18.ForeColor = System.Drawing.Color.DarkGray
        Me.Label18.Location = New System.Drawing.Point(378, 482)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(82, 13)
        Me.Label18.TabIndex = 166
        Me.Label18.Text = "Name of Bank"
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Font = New System.Drawing.Font("Segoe UI", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label15.ForeColor = System.Drawing.Color.DarkGray
        Me.Label15.Location = New System.Drawing.Point(174, 482)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(25, 13)
        Me.Label15.TabIndex = 165
        Me.Label15.Text = "No."
        '
        'tb_dated
        '
        Me.tb_dated.BackColor = System.Drawing.Color.FromArgb(CType(CType(114, Byte), Integer), CType(CType(126, Byte), Integer), CType(CType(138, Byte), Integer))
        Me.tb_dated.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.tb_dated.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tb_dated.ForeColor = System.Drawing.Color.White
        Me.tb_dated.Location = New System.Drawing.Point(486, 398)
        Me.tb_dated.MaxLength = 120
        Me.tb_dated.Name = "tb_dated"
        Me.tb_dated.ReadOnly = True
        Me.tb_dated.Size = New System.Drawing.Size(172, 22)
        Me.tb_dated.TabIndex = 164
        Me.tb_dated.TabStop = False
        Me.tb_dated.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'tb_per_bill
        '
        Me.tb_per_bill.BackColor = System.Drawing.Color.FromArgb(CType(CType(114, Byte), Integer), CType(CType(126, Byte), Integer), CType(CType(138, Byte), Integer))
        Me.tb_per_bill.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.tb_per_bill.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tb_per_bill.ForeColor = System.Drawing.Color.White
        Me.tb_per_bill.Location = New System.Drawing.Point(223, 398)
        Me.tb_per_bill.MaxLength = 120
        Me.tb_per_bill.Name = "tb_per_bill"
        Me.tb_per_bill.ReadOnly = True
        Me.tb_per_bill.Size = New System.Drawing.Size(209, 22)
        Me.tb_per_bill.TabIndex = 163
        Me.tb_per_bill.TabStop = False
        Me.tb_per_bill.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'tb_purpose_two
        '
        Me.tb_purpose_two.BackColor = System.Drawing.Color.FromArgb(CType(CType(114, Byte), Integer), CType(CType(126, Byte), Integer), CType(CType(138, Byte), Integer))
        Me.tb_purpose_two.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.tb_purpose_two.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tb_purpose_two.ForeColor = System.Drawing.Color.White
        Me.tb_purpose_two.Location = New System.Drawing.Point(26, 362)
        Me.tb_purpose_two.MaxLength = 120
        Me.tb_purpose_two.Name = "tb_purpose_two"
        Me.tb_purpose_two.ReadOnly = True
        Me.tb_purpose_two.Size = New System.Drawing.Size(786, 15)
        Me.tb_purpose_two.TabIndex = 162
        Me.tb_purpose_two.TabStop = False
        Me.tb_purpose_two.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'tb_purpose_one
        '
        Me.tb_purpose_one.BackColor = System.Drawing.Color.FromArgb(CType(CType(114, Byte), Integer), CType(CType(126, Byte), Integer), CType(CType(138, Byte), Integer))
        Me.tb_purpose_one.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.tb_purpose_one.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tb_purpose_one.ForeColor = System.Drawing.Color.White
        Me.tb_purpose_one.Location = New System.Drawing.Point(142, 326)
        Me.tb_purpose_one.MaxLength = 120
        Me.tb_purpose_one.Name = "tb_purpose_one"
        Me.tb_purpose_one.ReadOnly = True
        Me.tb_purpose_one.Size = New System.Drawing.Size(670, 15)
        Me.tb_purpose_one.TabIndex = 161
        Me.tb_purpose_one.TabStop = False
        Me.tb_purpose_one.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'tb_word_figure
        '
        Me.tb_word_figure.BackColor = System.Drawing.Color.FromArgb(CType(CType(114, Byte), Integer), CType(CType(126, Byte), Integer), CType(CType(138, Byte), Integer))
        Me.tb_word_figure.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.tb_word_figure.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tb_word_figure.ForeColor = System.Drawing.Color.White
        Me.tb_word_figure.Location = New System.Drawing.Point(128, 287)
        Me.tb_word_figure.MaxLength = 120
        Me.tb_word_figure.Name = "tb_word_figure"
        Me.tb_word_figure.ReadOnly = True
        Me.tb_word_figure.Size = New System.Drawing.Size(541, 15)
        Me.tb_word_figure.TabIndex = 160
        Me.tb_word_figure.TabStop = False
        Me.tb_word_figure.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'tb_address
        '
        Me.tb_address.BackColor = System.Drawing.Color.FromArgb(CType(CType(114, Byte), Integer), CType(CType(126, Byte), Integer), CType(CType(138, Byte), Integer))
        Me.tb_address.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.tb_address.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tb_address.ForeColor = System.Drawing.Color.White
        Me.tb_address.Location = New System.Drawing.Point(25, 240)
        Me.tb_address.MaxLength = 120
        Me.tb_address.Name = "tb_address"
        Me.tb_address.ReadOnly = True
        Me.tb_address.Size = New System.Drawing.Size(787, 22)
        Me.tb_address.TabIndex = 159
        Me.tb_address.TabStop = False
        Me.tb_address.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'tb_payor
        '
        Me.tb_payor.BackColor = System.Drawing.Color.FromArgb(CType(CType(114, Byte), Integer), CType(CType(126, Byte), Integer), CType(CType(138, Byte), Integer))
        Me.tb_payor.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.tb_payor.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tb_payor.ForeColor = System.Drawing.Color.White
        Me.tb_payor.Location = New System.Drawing.Point(287, 197)
        Me.tb_payor.MaxLength = 120
        Me.tb_payor.Name = "tb_payor"
        Me.tb_payor.ReadOnly = True
        Me.tb_payor.Size = New System.Drawing.Size(525, 22)
        Me.tb_payor.TabIndex = 158
        Me.tb_payor.TabStop = False
        Me.tb_payor.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Font = New System.Drawing.Font("Segoe UI", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label17.ForeColor = System.Drawing.Color.DarkGray
        Me.Label17.Location = New System.Drawing.Point(391, 377)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(57, 13)
        Me.Label17.TabIndex = 157
        Me.Label17.Text = "(Purpose)"
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Font = New System.Drawing.Font("Segoe UI", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label16.ForeColor = System.Drawing.Color.DarkGray
        Me.Label16.Location = New System.Drawing.Point(350, 262)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(138, 13)
        Me.Label16.TabIndex = 156
        Me.Label16.Text = "(Address/Office of Payor)"
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label12.ForeColor = System.Drawing.Color.White
        Me.Label12.Location = New System.Drawing.Point(441, 402)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(39, 15)
        Me.Label12.TabIndex = 154
        Me.Label12.Text = "dated"
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.ForeColor = System.Drawing.Color.White
        Me.Label11.Location = New System.Drawing.Point(23, 402)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(194, 15)
        Me.Label11.TabIndex = 153
        Me.Label11.Text = "per STATEMENT OF ACCOUNT No."
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.ForeColor = System.Drawing.Color.White
        Me.Label10.Location = New System.Drawing.Point(23, 326)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(113, 15)
        Me.Label10.TabIndex = 152
        Me.Label10.Text = "for the payment of"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.ForeColor = System.Drawing.Color.White
        Me.Label9.Location = New System.Drawing.Point(22, 287)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(100, 15)
        Me.Label9.TabIndex = 151
        Me.Label9.Text = "in the amount of"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.ForeColor = System.Drawing.Color.White
        Me.Label8.Location = New System.Drawing.Point(56, 201)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(225, 15)
        Me.Label8.TabIndex = 150
        Me.Label8.Text = "Please issue Official Receipt in favor of "
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.ForeColor = System.Drawing.Color.White
        Me.Label5.Location = New System.Drawing.Point(289, 143)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(278, 25)
        Me.Label5.TabIndex = 147
        Me.Label5.Text = "Preview for Order of Payment"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Segoe UI", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ForeColor = System.Drawing.Color.DarkGray
        Me.Label4.Location = New System.Drawing.Point(26, 82)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(32, 13)
        Me.Label4.TabIndex = 146
        Me.Label4.Text = "Date"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Segoe UI", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.DarkGray
        Me.Label2.Location = New System.Drawing.Point(26, 33)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(78, 13)
        Me.Label2.TabIndex = 144
        Me.Label2.Text = "General Fund"
        '
        'RadTitleBar1
        '
        Me.RadTitleBar1.Dock = System.Windows.Forms.DockStyle.Top
        Me.RadTitleBar1.Location = New System.Drawing.Point(0, 0)
        Me.RadTitleBar1.Name = "RadTitleBar1"
        Me.RadTitleBar1.Size = New System.Drawing.Size(840, 23)
        Me.RadTitleBar1.TabIndex = 142
        Me.RadTitleBar1.TabStop = False
        Me.RadTitleBar1.Text = "NTC Region 10"
        CType(Me.RadTitleBar1.GetChildAt(0), Telerik.WinControls.UI.RadTitleBarElement).Text = "NTC Region 10"
        CType(Me.RadTitleBar1.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(3), Telerik.WinControls.UI.RadImageButtonElement).Visibility = Telerik.WinControls.ElementVisibility.Collapsed
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Font = New System.Drawing.Font("Segoe UI", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label14.ForeColor = System.Drawing.Color.DarkGray
        Me.Label14.Location = New System.Drawing.Point(496, 219)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(93, 13)
        Me.Label14.TabIndex = 155
        Me.Label14.Text = "(Name of Payor)"
        '
        'Label24
        '
        Me.Label24.AutoSize = True
        Me.Label24.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label24.ForeColor = System.Drawing.Color.White
        Me.Label24.Location = New System.Drawing.Point(676, 287)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(30, 15)
        Me.Label24.TabIndex = 183
        Me.Label24.Text = "PHP"
        '
        'tb_serial_series
        '
        Me.tb_serial_series.BackColor = System.Drawing.Color.FromArgb(CType(CType(28, Byte), Integer), CType(CType(40, Byte), Integer), CType(CType(52, Byte), Integer))
        Me.tb_serial_series.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.tb_serial_series.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tb_serial_series.ForeColor = System.Drawing.Color.LightGray
        Me.tb_serial_series.Location = New System.Drawing.Point(176, 50)
        Me.tb_serial_series.MaxLength = 4
        Me.tb_serial_series.Name = "tb_serial_series"
        Me.tb_serial_series.Size = New System.Drawing.Size(47, 22)
        Me.tb_serial_series.TabIndex = 4
        Me.tb_serial_series.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.tb_serial_series.Visible = False
        '
        'tb_serial_month
        '
        Me.tb_serial_month.BackColor = System.Drawing.Color.FromArgb(CType(CType(28, Byte), Integer), CType(CType(40, Byte), Integer), CType(CType(52, Byte), Integer))
        Me.tb_serial_month.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.tb_serial_month.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tb_serial_month.ForeColor = System.Drawing.Color.LightGray
        Me.tb_serial_month.Location = New System.Drawing.Point(119, 50)
        Me.tb_serial_month.MaxLength = 2
        Me.tb_serial_month.Name = "tb_serial_month"
        Me.tb_serial_month.Size = New System.Drawing.Size(47, 22)
        Me.tb_serial_month.TabIndex = 3
        Me.tb_serial_month.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.tb_serial_month.Visible = False
        '
        'tb_serial_year
        '
        Me.tb_serial_year.BackColor = System.Drawing.Color.FromArgb(CType(CType(28, Byte), Integer), CType(CType(40, Byte), Integer), CType(CType(52, Byte), Integer))
        Me.tb_serial_year.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.tb_serial_year.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tb_serial_year.ForeColor = System.Drawing.Color.LightGray
        Me.tb_serial_year.Location = New System.Drawing.Point(62, 50)
        Me.tb_serial_year.MaxLength = 4
        Me.tb_serial_year.Name = "tb_serial_year"
        Me.tb_serial_year.Size = New System.Drawing.Size(47, 22)
        Me.tb_serial_year.TabIndex = 2
        Me.tb_serial_year.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.tb_serial_year.Visible = False
        '
        'tb_serial_starting_digit
        '
        Me.tb_serial_starting_digit.BackColor = System.Drawing.Color.FromArgb(CType(CType(114, Byte), Integer), CType(CType(126, Byte), Integer), CType(CType(138, Byte), Integer))
        Me.tb_serial_starting_digit.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.tb_serial_starting_digit.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tb_serial_starting_digit.ForeColor = System.Drawing.Color.LightGray
        Me.tb_serial_starting_digit.Location = New System.Drawing.Point(5, 50)
        Me.tb_serial_starting_digit.MaxLength = 4
        Me.tb_serial_starting_digit.Name = "tb_serial_starting_digit"
        Me.tb_serial_starting_digit.ReadOnly = True
        Me.tb_serial_starting_digit.Size = New System.Drawing.Size(47, 22)
        Me.tb_serial_starting_digit.TabIndex = 1
        Me.tb_serial_starting_digit.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.tb_serial_starting_digit.Visible = False
        '
        'cmb_accountant_id
        '
        Me.cmb_accountant_id.FormattingEnabled = True
        Me.cmb_accountant_id.Location = New System.Drawing.Point(731, 680)
        Me.cmb_accountant_id.Name = "cmb_accountant_id"
        Me.cmb_accountant_id.Size = New System.Drawing.Size(52, 21)
        Me.cmb_accountant_id.TabIndex = 195
        Me.cmb_accountant_id.TabStop = False
        Me.cmb_accountant_id.Visible = False
        '
        'cmb_accountant
        '
        Me.cmb_accountant.BackColor = System.Drawing.Color.FromArgb(CType(CType(28, Byte), Integer), CType(CType(40, Byte), Integer), CType(CType(52, Byte), Integer))
        Me.cmb_accountant.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmb_accountant.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.cmb_accountant.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmb_accountant.ForeColor = System.Drawing.Color.White
        Me.cmb_accountant.FormattingEnabled = True
        Me.cmb_accountant.Location = New System.Drawing.Point(539, 653)
        Me.cmb_accountant.Name = "cmb_accountant"
        Me.cmb_accountant.Size = New System.Drawing.Size(241, 21)
        Me.cmb_accountant.TabIndex = 11
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.BackColor = System.Drawing.Color.Transparent
        Me.Label20.Font = New System.Drawing.Font("Segoe UI", 9.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label20.ForeColor = System.Drawing.Color.DarkGray
        Me.Label20.Location = New System.Drawing.Point(539, 637)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(72, 15)
        Me.Label20.TabIndex = 193
        Me.Label20.Text = "Accountant"
        Me.Label20.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'dtp_date_created
        '
        Me.dtp_date_created.CalendarFont = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtp_date_created.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtp_date_created.Location = New System.Drawing.Point(26, 98)
        Me.dtp_date_created.Name = "dtp_date_created"
        Me.dtp_date_created.Size = New System.Drawing.Size(231, 22)
        Me.dtp_date_created.TabIndex = 5
        '
        'dgv_or
        '
        Me.dgv_or.AllowUserToAddRows = False
        Me.dgv_or.AllowUserToDeleteRows = False
        Me.dgv_or.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgv_or.Location = New System.Drawing.Point(34, 498)
        Me.dgv_or.Name = "dgv_or"
        Me.dgv_or.ReadOnly = True
        Me.dgv_or.Size = New System.Drawing.Size(46, 44)
        Me.dgv_or.TabIndex = 197
        Me.dgv_or.TabStop = False
        Me.dgv_or.Visible = False
        '
        'rbAuto
        '
        Me.rbAuto.AutoSize = True
        Me.rbAuto.Checked = True
        Me.rbAuto.Font = New System.Drawing.Font("Segoe UI", 9.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rbAuto.ForeColor = System.Drawing.Color.DarkGray
        Me.rbAuto.Location = New System.Drawing.Point(7, 5)
        Me.rbAuto.Name = "rbAuto"
        Me.rbAuto.Size = New System.Drawing.Size(149, 19)
        Me.rbAuto.TabIndex = 198
        Me.rbAuto.TabStop = True
        Me.rbAuto.Text = "Auto Generate OP No."
        Me.rbAuto.UseVisualStyleBackColor = True
        '
        'rbCustom
        '
        Me.rbCustom.AutoSize = True
        Me.rbCustom.Font = New System.Drawing.Font("Segoe UI", 9.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rbCustom.ForeColor = System.Drawing.Color.DarkGray
        Me.rbCustom.Location = New System.Drawing.Point(7, 28)
        Me.rbCustom.Name = "rbCustom"
        Me.rbCustom.Size = New System.Drawing.Size(111, 19)
        Me.rbCustom.TabIndex = 199
        Me.rbCustom.Text = "Custom OP No."
        Me.rbCustom.UseVisualStyleBackColor = True
        '
        'Panel2
        '
        Me.Panel2.Controls.Add(Me.tb_serial_starting_digit)
        Me.Panel2.Controls.Add(Me.rbAuto)
        Me.Panel2.Controls.Add(Me.tb_serial_year)
        Me.Panel2.Controls.Add(Me.rbCustom)
        Me.Panel2.Controls.Add(Me.tb_serial_month)
        Me.Panel2.Controls.Add(Me.tb_serial_series)
        Me.Panel2.Location = New System.Drawing.Point(577, 43)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(235, 77)
        Me.Panel2.TabIndex = 200
        '
        'frm_accountant_manual
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(44, Byte), Integer), CType(CType(62, Byte), Integer), CType(CType(80, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(840, 720)
        Me.Controls.Add(Me.Panel2)
        Me.Controls.Add(Me.dgv_or)
        Me.Controls.Add(Me.dtp_date_created)
        Me.Controls.Add(Me.cmb_accountant_id)
        Me.Controls.Add(Me.cmb_accountant)
        Me.Controls.Add(Me.Label20)
        Me.Controls.Add(Me.Label13)
        Me.Controls.Add(Me.tb_bank_payment_two)
        Me.Controls.Add(Me.tb_bank_name_two)
        Me.Controls.Add(Me.tb_bank_num_two)
        Me.Controls.Add(Me.Label22)
        Me.Controls.Add(Me.Label21)
        Me.Controls.Add(Me.tb_bank_total)
        Me.Controls.Add(Me.tb_bank_payment)
        Me.Controls.Add(Me.tb_bank_name)
        Me.Controls.Add(Me.tb_bank_num)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.tb_fund_cluster)
        Me.Controls.Add(Me.tb_payment)
        Me.Controls.Add(Me.Label19)
        Me.Controls.Add(Me.Label18)
        Me.Controls.Add(Me.Label15)
        Me.Controls.Add(Me.tb_dated)
        Me.Controls.Add(Me.tb_per_bill)
        Me.Controls.Add(Me.tb_purpose_two)
        Me.Controls.Add(Me.tb_purpose_one)
        Me.Controls.Add(Me.tb_word_figure)
        Me.Controls.Add(Me.tb_address)
        Me.Controls.Add(Me.tb_payor)
        Me.Controls.Add(Me.Label17)
        Me.Controls.Add(Me.Label16)
        Me.Controls.Add(Me.Label12)
        Me.Controls.Add(Me.Label11)
        Me.Controls.Add(Me.Label10)
        Me.Controls.Add(Me.Label9)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.RadTitleBar1)
        Me.Controls.Add(Me.Label14)
        Me.Controls.Add(Me.Label24)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.MaximizeBox = False
        Me.MaximumSize = New System.Drawing.Size(840, 720)
        Me.MinimizeBox = False
        Me.MinimumSize = New System.Drawing.Size(840, 720)
        Me.Name = "frm_accountant_manual"
        Me.ShowIcon = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "NTC Region 10"
        Me.Panel1.ResumeLayout(False)
        CType(Me.RadTitleBar1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgv_or, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents btn_finish As Button
    Friend WithEvents Label13 As Label
    Friend WithEvents tb_bank_payment_two As TextBox
    Friend WithEvents tb_bank_name_two As TextBox
    Friend WithEvents tb_bank_num_two As TextBox
    Friend WithEvents Label22 As Label
    Friend WithEvents Label21 As Label
    Friend WithEvents tb_bank_total As TextBox
    Friend WithEvents tb_bank_payment As TextBox
    Friend WithEvents tb_bank_name As TextBox
    Friend WithEvents tb_bank_num As TextBox
    Friend WithEvents Panel1 As Panel
    Friend WithEvents tb_fund_cluster As TextBox
    Friend WithEvents tb_payment As TextBox
    Friend WithEvents Label19 As Label
    Friend WithEvents Label18 As Label
    Friend WithEvents Label15 As Label
    Friend WithEvents tb_dated As TextBox
    Friend WithEvents tb_per_bill As TextBox
    Friend WithEvents tb_purpose_two As TextBox
    Friend WithEvents tb_purpose_one As TextBox
    Friend WithEvents tb_word_figure As TextBox
    Friend WithEvents tb_address As TextBox
    Friend WithEvents tb_payor As TextBox
    Friend WithEvents Label17 As Label
    Friend WithEvents Label16 As Label
    Friend WithEvents Label12 As Label
    Friend WithEvents Label11 As Label
    Friend WithEvents Label10 As Label
    Friend WithEvents Label9 As Label
    Friend WithEvents Label8 As Label
    Friend WithEvents Label5 As Label
    Friend WithEvents Label4 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents RadTitleBar1 As Telerik.WinControls.UI.RadTitleBar
    Friend WithEvents Label14 As Label
    Friend WithEvents Label24 As Label
    Friend WithEvents tb_serial_series As TextBox
    Friend WithEvents tb_serial_month As TextBox
    Friend WithEvents tb_serial_year As TextBox
    Friend WithEvents tb_serial_starting_digit As TextBox
    Friend WithEvents cmb_accountant_id As ComboBox
    Friend WithEvents cmb_accountant As ComboBox
    Friend WithEvents Label20 As Label
    Friend WithEvents dtp_date_created As DateTimePicker
    Friend WithEvents btn_back As Button
    Friend WithEvents dgv_or As DataGridView
    Friend WithEvents btn_cancel As Button
    Friend WithEvents rbAuto As RadioButton
    Friend WithEvents rbCustom As RadioButton
    Friend WithEvents Panel2 As Panel
End Class
