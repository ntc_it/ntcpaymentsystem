﻿Imports MySql.Data.MySqlClient
Imports System.Net
Imports Microsoft.Office.Interop
Imports System.IO
Imports System.Globalization

Public Class frm_licenser_log
    Dim query, process_type, service_type, soa_service_custom_value, user_id_notified,
        assigned_enforce_chief_id, soa_process_type(3), soa_service_type(6),
        user_id_preparator, user_id_approver, attachmentID, endorse_id, cashier_transact_id,
        is_endorse, OR_num, OR_date, soa_date_created, processID, serviceID, particularID,
        soaDataID, signature_preparator, signature_approver, signature_cashier,
        soa_preparator_id, soa_approver_id, enable_preparator_signature,
        enable_approver_signature, enable_cashier_signature, user_id_cashier,
        dateExpirationLongDate As String
    Dim cmd As MySqlCommand
    Dim reader As MySqlDataReader
    Dim xlWorksheet As Object
    Dim soa_details_id As Integer
    Dim process_type_counter As Integer = 0
    Dim service_type_counter As Integer = 0
    Dim particular_one_counter As Integer = 0
    Dim particular_two_counter As Integer = 0
    Dim particular_three_counter As Integer = 0
    Dim compute_sub_total, total As Double
    Dim expiry_date As Date
    Dim user_approved As Boolean = False
    Dim is_endorse_button = False
    Dim is_assess_button = False
    Dim export_success As Boolean = False
    Dim is_soa_paid As Boolean = False
    Dim is_panel_print_open = False
    Dim soaTemplateExists = False
    Dim paymentCustomValue As String = ""
    Public soa_id_transact As Integer
    Public user_id, authority_level, acct_mod, cash_mod As String

    Private Sub btn_refresh_Click(sender As Object, e As EventArgs) Handles btn_refresh.Click
        load_soa_details()

        pnl_print.Visible = False
    End Sub

    Private Sub btn_print_complete_Click(sender As Object, e As EventArgs) Handles btn_print_complete.Click
        If user_approved Then
            ' Endorse and receipt function is enabled.
            is_assess_button = False
            is_endorse_button = True
            is_soa_paid = True

            export_data_to_excel()

            ' Reset export type.
            is_assess_button = False
            is_endorse_button = False
            is_soa_paid = False
        Else
            MsgBox("Statement of Account is not yet approved.", MsgBoxStyle.Exclamation, "NTC Region 10")
        End If
    End Sub

    Private Sub btn_print_endorse_Click(sender As Object, e As EventArgs) Handles btn_print_endorse.Click
        If user_approved Then
            ' Endorse and receipt function is enabled.
            is_assess_button = False
            is_endorse_button = True
            is_soa_paid = False

            export_data_to_excel()

            ' Reset export type.
            is_assess_button = False
            is_endorse_button = False
            is_soa_paid = False
        Else
            MsgBox("Statement of Account is not yet approved.", MsgBoxStyle.Exclamation, "NTC Region 10")
        End If
    End Sub

    Private Sub btn_print_assessment_Click(sender As Object, e As EventArgs) Handles btn_print_assessment.Click
        If user_approved Then
            ' Endorse and receipt function is enabled.
            is_assess_button = True
            is_endorse_button = False
            is_soa_paid = False

            export_data_to_excel()

            ' Reset export type.
            is_assess_button = False
            is_endorse_button = False
            is_soa_paid = False
        Else
            MsgBox("Statement of Account is not yet approved.", MsgBoxStyle.Exclamation, "NTC Region 10")
        End If
    End Sub

    Private Sub btn_print_as_menu_Click(sender As Object, e As EventArgs) Handles btn_print_as_menu.Click
        ' Show/Hide print services.
        If is_panel_print_open = False Then
            pnl_print.Visible = True
            is_panel_print_open = True
        Else
            pnl_print.Visible = False
            is_panel_print_open = False
        End If
    End Sub

    Private Sub btn_endorse_Click(sender As Object, e As EventArgs) Handles btn_endorse.Click
        If user_approved Then
            create_pre_endorsement()

            btn_endorse.Enabled = False
            tb_message.ForeColor = Color.FromArgb(0, 201, 0)
            tb_message.Text = "SOA endorsed"
            tb_message.Visible = True
        Else
            MsgBox("Statement of Account is not yet approved.", MsgBoxStyle.Exclamation, "NTC Region 10")
        End If
    End Sub

    Sub create_pre_endorsement()
        Try
            endorse_soa()

            'Retrieve current accountant's ID
            class_connection.con.Open()
            query = "select setting_value from tbl_settings where setting = 'accounting_officer'"
            cmd = New MySqlCommand(query, class_connection.con)
            reader = cmd.ExecuteReader()

            If reader.Read Then
                user_id_notified = reader.GetString("setting_value")
            End If
            class_connection.con.Close()

            create_notification_to_accountant()

            MsgBox("Statement of Account has been endorsed. The Accountant is now notified.",
                MsgBoxStyle.Information, "NTC Region 10")
        Catch ex As MySqlException
            class_connection.con.Close()
            MsgBox("Couldn't endorse Statement of Account.", MsgBoxStyle.Exclamation, "NTC Region 10")
        Catch ex As Exception
            class_connection.con.Close()
            MsgBox("Something went wrong. Couldn't process the data.", MsgBoxStyle.Exclamation,
                "NTC Region 10")
        End Try
    End Sub

    Sub create_notification_to_accountant()
        Try
            Dim title = "SOA endorsed"
            Dim message = "SOA Serial No. " + tb_serial_num.Text + " is endorsed. Click the button " &
                "below to open."
            Dim current_datetime = Date.Now.ToString("yyyy-MM-dd HH:mm:ss")

            class_connection.con.Open()
            query = "insert into tbl_notifications(message_title, message_content, transaction_id, " &
                "user_id_notified, user_id_notifier, user_opened, date_created, action_taken, " &
                "message_type) Values('" & title & "', '" & message & "', '" & soa_id_transact.ToString &
                "', '" & user_id_notified & "', '" & user_id.ToString & "', '0', '" & current_datetime &
                "', '0', '7')"
            cmd = New MySqlCommand(query, class_connection.con)
            cmd.ExecuteNonQuery()
            class_connection.con.Close()
        Catch ex As MySqlException
            class_connection.con.Close()
            MsgBox("Couldn't send notification to Accountant.", MsgBoxStyle.Exclamation, "NTC Region 10")
        Catch ex As Exception
            class_connection.con.Close()
            MsgBox("Something went wrong. Couldn't process the data.", MsgBoxStyle.Exclamation,
                "NTC Region 10")
        End Try
    End Sub

    Sub endorse_soa()
        ' Set SOA is_endorse to true.
        Try
            class_connection.con.Open()
            query = "update tbl_soa_transact set is_endorse = '1' where soa_id = '" &
                soa_id_transact.ToString & "'"
            cmd = New MySqlCommand(query, class_connection.con)
            cmd.ExecuteNonQuery()
            class_connection.con.Close()
        Catch ex As MySqlException
            class_connection.con.Close()
            MsgBox("Couldn't update Statement of Account Endorse Status.", MsgBoxStyle.Exclamation,
                "NTC Region 10")
        Catch ex As Exception
            class_connection.con.Close()
            MsgBox("Something went wrong. Couldn't process the data.", MsgBoxStyle.Exclamation,
                "NTC Region 10")
        End Try
    End Sub

    Private Sub tb_reasses_Click(sender As Object, e As EventArgs) Handles btn_reasses.Click
        ' Open edit SOA form.
        frm_licenser_edit.soa_id_transact = soa_id_transact
        frm_licenser_edit.user_id = user_id
        frm_licenser_edit.isSOAEdit = False
        frm_licenser_edit.btn_update.Text = "Save"
        frm_licenser_edit.Show()

        Close()
    End Sub

    Private Sub btn_receipt_Click(sender As Object, e As EventArgs) Handles btn_receipt.Click
        ' Open receipt form.
        frm_cashier_log.transact_id_cashier = cashier_transact_id
        frm_cashier_log.user_id = user_id
        frm_cashier_log.authority_level = authority_level
        frm_cashier_log.btn_soa.Visible = False
        frm_cashier_log.btn_op.Visible = False
        frm_cashier_log.ShowDialog()
    End Sub

    Private Sub btn_op_Click(sender As Object, e As EventArgs) Handles btn_op.Click
        ' Open OP form.
        frm_accountant_log.authority_level = authority_level
        frm_accountant_log.endorse_id = endorse_id
        frm_accountant_log.btn_print.Visible = True
        frm_accountant_log.btn_soa.Visible = False
        frm_accountant_log.engr_mod = "1"
        frm_accountant_log.ShowDialog()
    End Sub

    Private Sub btn_attachment_Click(sender As Object, e As EventArgs) Handles btn_attachment.Click
        ' Open attachment form.
        If authority_level = "3" Or authority_level = "2" Or authority_level = "4" Then
            ' For accountant, cashier and approver.
            frm_attachment.attachment_id = attachmentID
            frm_attachment.user_id = user_id
            frm_attachment.btn_attach.Visible = False
            frm_attachment.authority_level = authority_level
            frm_attachment.attachment_menu.Items.Item(1).Enabled = False
            frm_attachment.ShowDialog()
        Else
            ' For licenser and admin.
            frm_attachment.attachment_id = attachmentID
            frm_attachment.btn_attach.Visible = True
            frm_attachment.authority_level = authority_level
            frm_attachment.user_id = user_id
            frm_attachment.attachment_menu.Items.Item(1).Enabled = True
            frm_attachment.ShowDialog()
        End If
    End Sub

    Private Sub Form3_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        load_soa_details()

        Icon = My.Resources.ntc_material_logo
    End Sub

    Private Sub btn_edit_Click(sender As Object, e As EventArgs) Handles btn_edit.Click
        frm_licenser_edit.soa_id_transact = soa_id_transact
        frm_licenser_edit.user_id = user_id
        frm_licenser_edit.authority_level = authority_level
        frm_licenser_edit.isSOAEdit = True
        frm_licenser_edit.btn_update.Text = "Update"
        frm_licenser_edit.Show()
        Close()
    End Sub

    Private Sub btn_add_approve_Click(sender As Object, e As EventArgs) Handles btn_add_approve.Click
        frm_approver_list.serial_id = tb_serial_num.Text
        frm_approver_list.soa_id_transact = soa_id_transact
        frm_approver_list.user_id = user_id
        frm_approver_list.user_id_login = user_id
        frm_approver_list.ShowDialog()
    End Sub

    Sub export_data_to_excel()
        ' Export all SOA details and data entries to excel.
        Dim xlapp As New Excel.Application

        ' Disabled print buttons during the process.
        btn_print_assessment.Enabled = False
        btn_print_endorse.Enabled = False
        btn_print_complete.Enabled = False

        ' Check if what export does the user use.
        If Not is_soa_paid Then
            ' If it is an asessment or an unpaid endorsement.
            If File.Exists(Application.StartupPath + "\Harmonized SOA.xlsx") Then
                xlapp.Workbooks.Open(Application.StartupPath & "\Harmonized SOA.xlsx")
                soaTemplateExists = True
            End If
        Else
            ' If it is a paid endorsement.
            If File.Exists(Application.StartupPath + "\Harmonized SOA Modified.xlsx") Then
                xlapp.Workbooks.Open(Application.StartupPath & "\Harmonized SOA Modified.xlsx")
                soaTemplateExists = True
            End If
        End If

        ' Check if template exist in the program files.
        If soaTemplateExists Then
            Try
                xlapp.Sheets("Sheet1").Select()
                xlWorksheet = xlapp.Sheets("Sheet1")

                ' Transaction type.
                If tb_transact_type.Text = "NEW" Then
                    xlWorksheet.cells(9, 3).value = "X"
                ElseIf tb_transact_type.Text = "RENEW" Then
                    xlWorksheet.cells(10, 3).value = "X"
                End If

                ' Process type.
                For Each process As String In soa_process_type
                    Select Case process
                        Case "MOD"
                            xlWorksheet.cells(9, 6).value = "X"
                        Case "DUP"
                            xlWorksheet.cells(10, 6).value = "X"
                        Case "OTHERS"
                            xlWorksheet.cells(11, 6).value = "X"
                    End Select
                Next

                ' Service type.
                For Each service As String In soa_service_type
                    Select Case service
                        Case "CO"
                            xlWorksheet.cells(9, 10).value = "X"
                        Case "CV"
                            xlWorksheet.cells(10, 10).value = "X"
                        Case "MS"
                            xlWorksheet.cells(11, 10).value = "X"
                        Case "MA"
                            xlWorksheet.cells(9, 14).value = "X"
                        Case "ROC"
                            xlWorksheet.cells(10, 14).value = "X"
                        Case "OTHERS"
                            xlWorksheet.cells(11, 14).value = "X"

                            If soa_service_custom_value <> "" Then
                                xlWorksheet.cells(11, 16).value = soa_service_custom_value
                            End If
                    End Select
                Next

                ' Particular 2 details.
                If tb_particular_two.Text <> "N/A" Then
                    xlWorksheet.cells(14, 11).value = tb_particular_two.Text
                    xlWorksheet.cells(15, 12).value = tb_particular_two_pc_from.Text & "-" &
                        tb_particular_two_pc_to.Text
                End If

                ' Particular 3 details.
                If tb_particular_three.Text <> "N/A" Then
                    xlWorksheet.cells(14, 15).value = tb_particular_three.Text
                    xlWorksheet.cells(15, 16).value = tb_particular_three_pc_from.Text & "-" &
                        tb_particular_three_pc_to.Text
                End If

                ' Copy SOA data entries to excel.
                For Each row As DataGridViewRow In dgv_soa_data.Rows
                    Try
                        ' Retrieve custom value for this SOA data entry if it has any.
                        class_connection.con.Open()
                        query = "select others_custom_value from tbl_soa_data where soa_data_id = '" &
                            soaDataID & "' and mop_id = '" & row.Cells(16).FormattedValue.ToString & "'"
                        cmd = New MySqlCommand(query, class_connection.con)
                        reader = cmd.ExecuteReader()

                        If reader.Read Then
                            paymentCustomValue =
                                WebUtility.HtmlDecode(reader.GetString("others_custom_value"))
                        End If
                        class_connection.con.Close()
                    Catch ex As MySqlException
                        class_connection.con.Close()
                        MsgBox("Couldn't connect to server.", MsgBoxStyle.Exclamation, "NTC Region 10")
                    Catch ex As Exception
                        class_connection.con.Close()
                        MsgBox("Couldn't retrieve data properly. Try again later.", MsgBoxStyle.Exclamation,
                            "NTC Region 10")
                    End Try

                    ' LICENSE MOPs. LICENSE MOPs. LICENSE MOPs. LICENSE MOPs. LICENSE MOPs. LICENSE MOPs. LICENSE MOPs.
                    If row.Cells(0).FormattedValue = "4-02-01-010" And row.Cells(16).FormattedValue = "1" And
                        row.Cells(2).FormattedValue = "LICENSE" Then
                        If paymentCustomValue <> "" Then
                            xlWorksheet.cells(18, 2).value = "Permit to Purchase: " + paymentCustomValue
                        Else
                            xlWorksheet.cells(18, 2).value = "Permit to Purchase"
                        End If
                        xlWorksheet.cells(18, 6).value = row.Cells(3).FormattedValue.ToString
                        xlWorksheet.cells(18, 7).value = Double.Parse(row.Cells(4).FormattedValue.ToString) / 100
                        xlWorksheet.cells(18, 8).value = row.Cells(5).FormattedValue.ToString
                        xlWorksheet.cells(18, 9).value = row.Cells(6).FormattedValue.ToString

                        If tb_particular_two.Text <> "N/A" Then
                            xlWorksheet.cells(18, 10).value = row.Cells(7).FormattedValue.ToString
                            xlWorksheet.cells(18, 11).value = Double.Parse(row.Cells(8).FormattedValue.ToString) / 100
                            xlWorksheet.cells(18, 12).value = row.Cells(9).FormattedValue.ToString
                            xlWorksheet.cells(18, 13).value = row.Cells(10).FormattedValue.ToString
                        End If

                        If tb_particular_three.Text <> "N/A" Then
                            xlWorksheet.cells(18, 14).value = row.Cells(11).FormattedValue.ToString
                            xlWorksheet.cells(18, 15).value = Double.Parse(row.Cells(12).FormattedValue.ToString) / 100
                            xlWorksheet.cells(18, 16).value = row.Cells(13).FormattedValue.ToString
                            xlWorksheet.cells(18, 17).value = row.Cells(14).FormattedValue.ToString
                        End If

                        xlWorksheet.cells(18, 18).value = row.Cells(15).FormattedValue.ToString
                    End If

                    If row.Cells(0).FormattedValue = "4-02-01-130" And row.Cells(16).FormattedValue = "2" And
                        row.Cells(2).FormattedValue = "LICENSE" Then
                        If paymentCustomValue <> "" Then
                            xlWorksheet.cells(19, 2).value = "Filling Fee: " + paymentCustomValue
                        Else
                            xlWorksheet.cells(19, 2).value = "Filling Fee"
                        End If
                        xlWorksheet.cells(19, 6).value = row.Cells(3).FormattedValue.ToString
                        xlWorksheet.cells(19, 7).value = Double.Parse(row.Cells(4).FormattedValue.ToString) / 100
                        xlWorksheet.cells(19, 8).value = row.Cells(5).FormattedValue.ToString
                        xlWorksheet.cells(19, 9).value = row.Cells(6).FormattedValue.ToString

                        If tb_particular_two.Text <> "N/A" Then
                            xlWorksheet.cells(19, 10).value = row.Cells(7).FormattedValue.ToString
                            xlWorksheet.cells(19, 11).value = Double.Parse(row.Cells(8).FormattedValue.ToString) / 100
                            xlWorksheet.cells(19, 12).value = row.Cells(9).FormattedValue.ToString
                            xlWorksheet.cells(19, 13).value = row.Cells(10).FormattedValue.ToString
                        End If

                        If tb_particular_three.Text <> "N/A" Then
                            xlWorksheet.cells(19, 14).value = row.Cells(11).FormattedValue.ToString
                            xlWorksheet.cells(19, 15).value = Double.Parse(row.Cells(12).FormattedValue.ToString) / 100
                            xlWorksheet.cells(19, 16).value = row.Cells(13).FormattedValue.ToString
                            xlWorksheet.cells(19, 17).value = row.Cells(14).FormattedValue.ToString
                        End If

                        xlWorksheet.cells(19, 18).value = row.Cells(15).FormattedValue.ToString
                    End If

                    If row.Cells(0).FormattedValue = "4-02-01-010" And row.Cells(16).FormattedValue = "3" And
                        row.Cells(2).FormattedValue = "LICENSE" Then
                        If paymentCustomValue <> "" Then
                            xlWorksheet.cells(20, 2).value = "Permit to Possess: " + paymentCustomValue
                        Else
                            xlWorksheet.cells(20, 2).value = "Permit to Possess"
                        End If
                        xlWorksheet.cells(20, 6).value = row.Cells(3).FormattedValue.ToString
                        xlWorksheet.cells(20, 7).value = Double.Parse(row.Cells(4).FormattedValue.ToString) / 100
                        xlWorksheet.cells(20, 8).value = row.Cells(5).FormattedValue.ToString
                        xlWorksheet.cells(20, 9).value = row.Cells(6).FormattedValue.ToString

                        If tb_particular_two.Text <> "N/A" Then
                            xlWorksheet.cells(20, 10).value = row.Cells(7).FormattedValue.ToString
                            xlWorksheet.cells(20, 11).value = Double.Parse(row.Cells(8).FormattedValue.ToString) / 100
                            xlWorksheet.cells(20, 12).value = row.Cells(9).FormattedValue.ToString
                            xlWorksheet.cells(20, 13).value = row.Cells(10).FormattedValue.ToString
                        End If

                        If tb_particular_three.Text <> "N/A" Then
                            xlWorksheet.cells(20, 14).value = row.Cells(11).FormattedValue.ToString
                            xlWorksheet.cells(20, 15).value = Double.Parse(row.Cells(12).FormattedValue.ToString) / 100
                            xlWorksheet.cells(20, 16).value = row.Cells(13).FormattedValue.ToString
                            xlWorksheet.cells(20, 17).value = row.Cells(14).FormattedValue.ToString
                        End If

                        xlWorksheet.cells(20, 18).value = row.Cells(15).FormattedValue.ToString
                    End If

                    If row.Cells(0).FormattedValue = "4-02-01-010" And row.Cells(16).FormattedValue = "4" And
                        row.Cells(2).FormattedValue = "LICENSE" Then
                        If paymentCustomValue <> "" Then
                            xlWorksheet.cells(21, 2).value = "Construction Permit Fee: " + paymentCustomValue
                        Else
                            xlWorksheet.cells(21, 2).value = "Construction Permit Fee"
                        End If
                        xlWorksheet.cells(21, 6).value = row.Cells(3).FormattedValue.ToString
                        xlWorksheet.cells(21, 7).value = Double.Parse(row.Cells(4).FormattedValue.ToString) / 100
                        xlWorksheet.cells(21, 8).value = row.Cells(5).FormattedValue.ToString
                        xlWorksheet.cells(21, 9).value = row.Cells(6).FormattedValue.ToString

                        If tb_particular_two.Text <> "N/A" Then
                            xlWorksheet.cells(21, 10).value = row.Cells(7).FormattedValue.ToString
                            xlWorksheet.cells(21, 11).value = Double.Parse(row.Cells(8).FormattedValue.ToString) / 100
                            xlWorksheet.cells(21, 12).value = row.Cells(9).FormattedValue.ToString
                            xlWorksheet.cells(21, 13).value = row.Cells(10).FormattedValue.ToString
                        End If

                        If tb_particular_three.Text <> "N/A" Then
                            xlWorksheet.cells(21, 14).value = row.Cells(11).FormattedValue.ToString
                            xlWorksheet.cells(21, 15).value = Double.Parse(row.Cells(12).FormattedValue.ToString) / 100
                            xlWorksheet.cells(21, 16).value = row.Cells(13).FormattedValue.ToString
                            xlWorksheet.cells(21, 17).value = row.Cells(14).FormattedValue.ToString
                        End If

                        xlWorksheet.cells(21, 18).value = row.Cells(15).FormattedValue.ToString
                    End If

                    If row.Cells(0).FormattedValue = "4-02-01-060" And row.Cells(16).FormattedValue = "5" And
                        row.Cells(2).FormattedValue = "LICENSE" Then
                        If paymentCustomValue <> "" Then
                            xlWorksheet.cells(22, 2).value = "Radio Station License: " + paymentCustomValue
                        Else
                            xlWorksheet.cells(22, 2).value = "Radio Station License"
                        End If
                        xlWorksheet.cells(22, 6).value = row.Cells(3).FormattedValue.ToString
                        xlWorksheet.cells(22, 7).value = Double.Parse(row.Cells(4).FormattedValue.ToString) / 100
                        xlWorksheet.cells(22, 8).value = row.Cells(5).FormattedValue.ToString
                        xlWorksheet.cells(22, 9).value = row.Cells(6).FormattedValue.ToString

                        If tb_particular_two.Text <> "N/A" Then
                            xlWorksheet.cells(22, 10).value = row.Cells(7).FormattedValue.ToString
                            xlWorksheet.cells(22, 11).value = Double.Parse(row.Cells(8).FormattedValue.ToString) / 100
                            xlWorksheet.cells(22, 12).value = row.Cells(9).FormattedValue.ToString
                            xlWorksheet.cells(22, 13).value = row.Cells(10).FormattedValue.ToString
                        End If

                        If tb_particular_three.Text <> "N/A" Then
                            xlWorksheet.cells(22, 14).value = row.Cells(11).FormattedValue.ToString
                            xlWorksheet.cells(22, 15).value = Double.Parse(row.Cells(12).FormattedValue.ToString) / 100
                            xlWorksheet.cells(22, 16).value = row.Cells(13).FormattedValue.ToString
                            xlWorksheet.cells(22, 17).value = row.Cells(14).FormattedValue.ToString
                        End If

                        xlWorksheet.cells(22, 18).value = row.Cells(15).FormattedValue.ToString
                    End If

                    If row.Cells(0).FormattedValue = "4-02-01-100" And row.Cells(16).FormattedValue = "6" And
                        row.Cells(2).FormattedValue = "LICENSE" Then
                        If paymentCustomValue <> "" Then
                            xlWorksheet.cells(23, 2).value = "Inspection Fee: " + paymentCustomValue
                        Else
                            xlWorksheet.cells(23, 2).value = "Inspection Fee"
                        End If
                        xlWorksheet.cells(23, 6).value = row.Cells(3).FormattedValue.ToString
                        xlWorksheet.cells(23, 7).value = Double.Parse(row.Cells(4).FormattedValue.ToString) / 100
                        xlWorksheet.cells(23, 8).value = row.Cells(5).FormattedValue.ToString
                        xlWorksheet.cells(23, 9).value = row.Cells(6).FormattedValue.ToString

                        If tb_particular_two.Text <> "N/A" Then
                            xlWorksheet.cells(23, 10).value = row.Cells(7).FormattedValue.ToString
                            xlWorksheet.cells(23, 11).value = Double.Parse(row.Cells(8).FormattedValue.ToString) / 100
                            xlWorksheet.cells(23, 12).value = row.Cells(9).FormattedValue.ToString
                            xlWorksheet.cells(23, 13).value = row.Cells(10).FormattedValue.ToString
                        End If

                        If tb_particular_three.Text <> "N/A" Then
                            xlWorksheet.cells(23, 14).value = row.Cells(11).FormattedValue.ToString
                            xlWorksheet.cells(23, 15).value = Double.Parse(row.Cells(12).FormattedValue.ToString) / 100
                            xlWorksheet.cells(23, 16).value = row.Cells(13).FormattedValue.ToString
                            xlWorksheet.cells(23, 17).value = row.Cells(14).FormattedValue.ToString
                        End If

                        xlWorksheet.cells(23, 18).value = row.Cells(15).FormattedValue.ToString
                    End If

                    If row.Cells(0).FormattedValue = "4-02-01-080" And row.Cells(16).FormattedValue = "7" And
                        row.Cells(2).FormattedValue = "LICENSE" Then
                        If paymentCustomValue <> "" Then
                            xlWorksheet.cells(24, 2).value = "Spectrum User's Fee(SUF): " + paymentCustomValue
                        Else
                            xlWorksheet.cells(24, 2).value = "Spectrum User's Fee(SUF)"
                        End If
                        xlWorksheet.cells(24, 6).value = row.Cells(3).FormattedValue.ToString
                        xlWorksheet.cells(24, 7).value = Double.Parse(row.Cells(4).FormattedValue.ToString) / 100
                        xlWorksheet.cells(24, 8).value = row.Cells(5).FormattedValue.ToString
                        xlWorksheet.cells(24, 9).value = row.Cells(6).FormattedValue.ToString

                        If tb_particular_two.Text <> "N/A" Then
                            xlWorksheet.cells(24, 10).value = row.Cells(7).FormattedValue.ToString
                            xlWorksheet.cells(24, 11).value = Double.Parse(row.Cells(8).FormattedValue.ToString) / 100
                            xlWorksheet.cells(24, 12).value = row.Cells(9).FormattedValue.ToString
                            xlWorksheet.cells(24, 13).value = row.Cells(10).FormattedValue.ToString
                        End If

                        If tb_particular_three.Text <> "N/A" Then
                            xlWorksheet.cells(24, 14).value = row.Cells(11).FormattedValue.ToString
                            xlWorksheet.cells(24, 15).value = Double.Parse(row.Cells(12).FormattedValue.ToString) / 100
                            xlWorksheet.cells(24, 16).value = row.Cells(13).FormattedValue.ToString
                            xlWorksheet.cells(24, 17).value = row.Cells(14).FormattedValue.ToString
                        End If

                        xlWorksheet.cells(24, 18).value = row.Cells(15).FormattedValue.ToString
                    End If

                    If row.Cells(0).FormattedValue = "4-02-01-140" And row.Cells(16).FormattedValue = "8" And
                        row.Cells(2).FormattedValue = "LICENSE" Then
                        If paymentCustomValue <> "" Then
                            xlWorksheet.cells(25, 2).value = "Fines/Penalties/Surcharges: " + paymentCustomValue
                        Else
                            xlWorksheet.cells(25, 2).value = "Fines/Penalties/Surcharges"
                        End If
                        xlWorksheet.cells(25, 6).value = row.Cells(3).FormattedValue.ToString
                        xlWorksheet.cells(25, 7).value = Double.Parse(row.Cells(4).FormattedValue.ToString) / 100
                        xlWorksheet.cells(25, 8).value = row.Cells(5).FormattedValue.ToString
                        xlWorksheet.cells(25, 9).value = row.Cells(6).FormattedValue.ToString

                        If tb_particular_two.Text <> "N/A" Then
                            xlWorksheet.cells(25, 10).value = row.Cells(7).FormattedValue.ToString
                            xlWorksheet.cells(25, 11).value = Double.Parse(row.Cells(8).FormattedValue.ToString) / 100
                            xlWorksheet.cells(25, 12).value = row.Cells(9).FormattedValue.ToString
                            xlWorksheet.cells(25, 13).value = row.Cells(10).FormattedValue.ToString
                        End If
                        If tb_particular_three.Text <> "N/A" Then
                            xlWorksheet.cells(25, 14).value = row.Cells(11).FormattedValue.ToString
                            xlWorksheet.cells(25, 15).value = Double.Parse(row.Cells(12).FormattedValue.ToString) / 100
                            xlWorksheet.cells(25, 16).value = row.Cells(13).FormattedValue.ToString
                            xlWorksheet.cells(25, 17).value = row.Cells(14).FormattedValue.ToString
                        End If

                        xlWorksheet.cells(25, 18).value = row.Cells(15).FormattedValue.ToString
                    End If

                    If row.Cells(0).FormattedValue = "4-02-01-140" And row.Cells(16).FormattedValue = "9" And
                        row.Cells(2).FormattedValue = "LICENSE" Then
                        If paymentCustomValue <> "" Then
                            xlWorksheet.cells(26, 2).value = "(SUR)Radio Station License: " + paymentCustomValue
                        Else
                            xlWorksheet.cells(26, 2).value = "(SUR)Radio Station License"
                        End If
                        xlWorksheet.cells(26, 6).value = row.Cells(3).FormattedValue.ToString
                        xlWorksheet.cells(26, 7).value = Double.Parse(row.Cells(4).FormattedValue.ToString) / 100
                        xlWorksheet.cells(26, 8).value = row.Cells(5).FormattedValue.ToString
                        xlWorksheet.cells(26, 9).value = row.Cells(6).FormattedValue.ToString

                        If tb_particular_two.Text <> "N/A" Then
                            xlWorksheet.cells(26, 10).value = row.Cells(7).FormattedValue.ToString
                            xlWorksheet.cells(26, 11).value = Double.Parse(row.Cells(8).FormattedValue.ToString) / 100
                            xlWorksheet.cells(26, 12).value = row.Cells(9).FormattedValue.ToString
                            xlWorksheet.cells(26, 13).value = row.Cells(10).FormattedValue.ToString
                        End If

                        If tb_particular_three.Text <> "N/A" Then
                            xlWorksheet.cells(26, 14).value = row.Cells(11).FormattedValue.ToString
                            xlWorksheet.cells(26, 15).value = Double.Parse(row.Cells(12).FormattedValue.ToString) / 100
                            xlWorksheet.cells(26, 16).value = row.Cells(13).FormattedValue.ToString
                            xlWorksheet.cells(26, 17).value = row.Cells(14).FormattedValue.ToString
                        End If

                        xlWorksheet.cells(26, 18).value = row.Cells(15).FormattedValue.ToString
                    End If

                    If row.Cells(0).FormattedValue = "4-02-01-140" And row.Cells(16).FormattedValue = "10" And
                        row.Cells(2).FormattedValue = "LICENSE" Then
                        If paymentCustomValue <> "" Then
                            xlWorksheet.cells(27, 2).value = "(SUR)Spectrum User's Fee (SUF): " + paymentCustomValue
                        Else
                            xlWorksheet.cells(27, 2).value = "(SUR)Spectrum User's Fee (SUF)"
                        End If
                        xlWorksheet.cells(27, 6).value = row.Cells(3).FormattedValue.ToString
                        xlWorksheet.cells(27, 7).value = Double.Parse(row.Cells(4).FormattedValue.ToString) / 100
                        xlWorksheet.cells(27, 8).value = row.Cells(5).FormattedValue.ToString
                        xlWorksheet.cells(27, 9).value = row.Cells(6).FormattedValue.ToString

                        If tb_particular_two.Text <> "N/A" Then
                            xlWorksheet.cells(27, 10).value = row.Cells(7).FormattedValue.ToString
                            xlWorksheet.cells(27, 11).value = Double.Parse(row.Cells(8).FormattedValue.ToString) / 100
                            xlWorksheet.cells(27, 12).value = row.Cells(9).FormattedValue.ToString
                            xlWorksheet.cells(27, 13).value = row.Cells(10).FormattedValue.ToString
                        End If

                        If tb_particular_three.Text <> "N/A" Then
                            xlWorksheet.cells(27, 14).value = row.Cells(11).FormattedValue.ToString
                            xlWorksheet.cells(27, 15).value = Double.Parse(row.Cells(12).FormattedValue.ToString) / 100
                            xlWorksheet.cells(27, 16).value = row.Cells(13).FormattedValue.ToString
                            xlWorksheet.cells(27, 17).value = row.Cells(14).FormattedValue.ToString
                        End If

                        xlWorksheet.cells(27, 18).value = row.Cells(15).FormattedValue.ToString
                    End If

                    ' PERMIT MOPs. PERMIT MOPs. PERMIT MOPs. PERMIT MOPs. PERMIT MOPs. PERMIT MOPs. PERMIT MOPs.
                    If row.Cells(0).FormattedValue = "4-02-01-010" And row.Cells(16).FormattedValue = "11" And
                        row.Cells(2).FormattedValue = "PERMIT" Then
                        If paymentCustomValue <> "" Then
                            xlWorksheet.cells(29, 2).value =
                                "Permit Fees(Dealers/Reseller/Service Center/Sell/Transfer/REC/Import): " &
                                paymentCustomValue
                        Else
                            xlWorksheet.cells(29, 2).value =
                                "Permit Fees(Dealers/Reseller/Service Center/Sell/Transfer/REC/Import)"
                        End If
                        xlWorksheet.cells(29, 6).value = row.Cells(3).FormattedValue.ToString
                        xlWorksheet.cells(29, 7).value = Double.Parse(row.Cells(4).FormattedValue.ToString) / 100
                        xlWorksheet.cells(29, 8).value = row.Cells(5).FormattedValue.ToString
                        xlWorksheet.cells(29, 9).value = row.Cells(6).FormattedValue.ToString

                        If tb_particular_two.Text <> "N/A" Then
                            xlWorksheet.cells(29, 10).value = row.Cells(7).FormattedValue.ToString
                            xlWorksheet.cells(29, 11).value = Double.Parse(row.Cells(8).FormattedValue.ToString) / 100
                            xlWorksheet.cells(29, 12).value = row.Cells(9).FormattedValue.ToString
                            xlWorksheet.cells(29, 13).value = row.Cells(10).FormattedValue.ToString
                        End If

                        If tb_particular_three.Text <> "N/A" Then
                            xlWorksheet.cells(29, 14).value = row.Cells(11).FormattedValue.ToString
                            xlWorksheet.cells(29, 15).value = Double.Parse(row.Cells(12).FormattedValue.ToString) / 100
                            xlWorksheet.cells(29, 16).value = row.Cells(13).FormattedValue.ToString
                            xlWorksheet.cells(29, 17).value = row.Cells(14).FormattedValue.ToString
                        End If

                        xlWorksheet.cells(29, 18).value = row.Cells(15).FormattedValue.ToString
                    End If

                    If row.Cells(0).FormattedValue = "4-02-01-100" And row.Cells(16).FormattedValue = "12" And
                        row.Cells(2).FormattedValue = "PERMIT" Then
                        If paymentCustomValue <> "" Then
                            xlWorksheet.cells(30, 2).value = "Inspection Fee: " + paymentCustomValue
                        Else
                            xlWorksheet.cells(30, 2).value = "Inspection Fee"
                        End If
                        xlWorksheet.cells(30, 6).value = row.Cells(3).FormattedValue.ToString
                        xlWorksheet.cells(30, 7).value = Double.Parse(row.Cells(4).FormattedValue.ToString) / 100
                        xlWorksheet.cells(30, 8).value = row.Cells(5).FormattedValue.ToString
                        xlWorksheet.cells(30, 9).value = row.Cells(6).FormattedValue.ToString

                        If tb_particular_two.Text <> "N/A" Then
                            xlWorksheet.cells(30, 10).value = row.Cells(7).FormattedValue.ToString
                            xlWorksheet.cells(30, 11).value = Double.Parse(row.Cells(8).FormattedValue.ToString) / 100
                            xlWorksheet.cells(30, 12).value = row.Cells(9).FormattedValue.ToString
                            xlWorksheet.cells(30, 13).value = row.Cells(10).FormattedValue.ToString
                        End If

                        If tb_particular_three.Text <> "N/A" Then
                            xlWorksheet.cells(30, 14).value = row.Cells(11).FormattedValue.ToString
                            xlWorksheet.cells(30, 15).value = Double.Parse(row.Cells(12).FormattedValue.ToString) / 100
                            xlWorksheet.cells(30, 16).value = row.Cells(13).FormattedValue.ToString
                            xlWorksheet.cells(30, 17).value = row.Cells(14).FormattedValue.ToString
                        End If

                        xlWorksheet.cells(30, 18).value = row.Cells(15).FormattedValue.ToString
                    End If

                    If row.Cells(0).FormattedValue = "4-02-01-130" And row.Cells(16).FormattedValue = "13" And
                        row.Cells(2).FormattedValue = "PERMIT" Then
                        If paymentCustomValue <> "" Then
                            xlWorksheet.cells(31, 2).value = "Filling Fee: " + paymentCustomValue
                        Else
                            xlWorksheet.cells(31, 2).value = "Filling Fee"
                        End If
                        xlWorksheet.cells(31, 6).value = row.Cells(3).FormattedValue.ToString
                        xlWorksheet.cells(31, 7).value = Double.Parse(row.Cells(4).FormattedValue.ToString) / 100
                        xlWorksheet.cells(31, 8).value = row.Cells(5).FormattedValue.ToString
                        xlWorksheet.cells(31, 9).value = row.Cells(6).FormattedValue.ToString

                        If tb_particular_two.Text <> "N/A" Then
                            xlWorksheet.cells(31, 10).value = row.Cells(7).FormattedValue.ToString
                            xlWorksheet.cells(31, 11).value = Double.Parse(row.Cells(8).FormattedValue.ToString) / 100
                            xlWorksheet.cells(31, 12).value = row.Cells(9).FormattedValue.ToString
                            xlWorksheet.cells(31, 13).value = row.Cells(10).FormattedValue.ToString
                        End If

                        If tb_particular_three.Text <> "N/A" Then
                            xlWorksheet.cells(31, 14).value = row.Cells(11).FormattedValue.ToString
                            xlWorksheet.cells(31, 15).value = Double.Parse(row.Cells(12).FormattedValue.ToString) / 100
                            xlWorksheet.cells(31, 16).value = row.Cells(13).FormattedValue.ToString
                            xlWorksheet.cells(31, 17).value = row.Cells(14).FormattedValue.ToString
                        End If

                        xlWorksheet.cells(31, 18).value = row.Cells(15).FormattedValue.ToString
                    End If

                    If row.Cells(0).FormattedValue = "4-02-01-140" And row.Cells(16).FormattedValue = "14" And
                        row.Cells(2).FormattedValue = "PERMIT" Then
                        If paymentCustomValue <> "" Then
                            xlWorksheet.cells(32, 2).value = "Fines/Penalties/Surcharges: " + paymentCustomValue
                        Else
                            xlWorksheet.cells(32, 2).value = "Fines/Penalties/Surcharges"
                        End If
                        xlWorksheet.cells(32, 6).value = row.Cells(3).FormattedValue.ToString
                        xlWorksheet.cells(32, 7).value = Double.Parse(row.Cells(4).FormattedValue.ToString) / 100
                        xlWorksheet.cells(32, 8).value = row.Cells(5).FormattedValue.ToString
                        xlWorksheet.cells(32, 9).value = row.Cells(6).FormattedValue.ToString

                        If tb_particular_two.Text <> "N/A" Then
                            xlWorksheet.cells(32, 10).value = row.Cells(7).FormattedValue.ToString
                            xlWorksheet.cells(32, 11).value = Double.Parse(row.Cells(8).FormattedValue.ToString) / 100
                            xlWorksheet.cells(32, 12).value = row.Cells(9).FormattedValue.ToString
                            xlWorksheet.cells(32, 13).value = row.Cells(10).FormattedValue.ToString
                        End If

                        If tb_particular_three.Text <> "N/A" Then
                            xlWorksheet.cells(32, 14).value = row.Cells(11).FormattedValue.ToString
                            xlWorksheet.cells(32, 15).value = Double.Parse(row.Cells(12).FormattedValue.ToString) / 100
                            xlWorksheet.cells(32, 16).value = row.Cells(13).FormattedValue.ToString
                            xlWorksheet.cells(32, 17).value = row.Cells(14).FormattedValue.ToString
                        End If

                        xlWorksheet.cells(32, 18).value = row.Cells(15).FormattedValue.ToString
                    End If

                    ' ROC MOPs. ROC MOPs. ROC MOPs. ROC MOPs. ROC MOPs. ROC MOPs. ROC MOPs. ROC MOPs. ROC MOPs.
                    If row.Cells(0).FormattedValue = "4-02-01-060" And row.Cells(16).FormattedValue = "15" And
                        row.Cells(2).FormattedValue = "AROC/ROC" Then
                        If paymentCustomValue <> "" Then
                            xlWorksheet.cells(34, 2).value = "Radio Station License: " + paymentCustomValue
                        Else
                            xlWorksheet.cells(34, 2).value = "Radio Station License"
                        End If
                        xlWorksheet.cells(34, 6).value = row.Cells(3).FormattedValue.ToString
                        xlWorksheet.cells(34, 7).value = Double.Parse(row.Cells(4).FormattedValue.ToString) / 100
                        xlWorksheet.cells(34, 8).value = row.Cells(5).FormattedValue.ToString
                        xlWorksheet.cells(34, 9).value = row.Cells(6).FormattedValue.ToString

                        If tb_particular_two.Text <> "N/A" Then
                            xlWorksheet.cells(34, 10).value = row.Cells(7).FormattedValue.ToString
                            xlWorksheet.cells(34, 11).value = Double.Parse(row.Cells(8).FormattedValue.ToString) / 100
                            xlWorksheet.cells(34, 12).value = row.Cells(9).FormattedValue.ToString
                            xlWorksheet.cells(34, 13).value = row.Cells(10).FormattedValue.ToString
                        End If

                        If tb_particular_three.Text <> "N/A" Then
                            xlWorksheet.cells(34, 14).value = row.Cells(11).FormattedValue.ToString
                            xlWorksheet.cells(34, 15).value = Double.Parse(row.Cells(12).FormattedValue.ToString) / 100
                            xlWorksheet.cells(34, 16).value = row.Cells(13).FormattedValue.ToString
                            xlWorksheet.cells(34, 17).value = row.Cells(14).FormattedValue.ToString
                        End If

                        xlWorksheet.cells(34, 18).value = row.Cells(15).FormattedValue.ToString
                    End If

                    If row.Cells(0).FormattedValue = "4-02-01-060" And row.Cells(16).FormattedValue = "16" And
                        row.Cells(2).FormattedValue = "AROC/ROC" Then
                        If paymentCustomValue <> "" Then
                            xlWorksheet.cells(35, 2).value = "Radio Operator's Cert.: " + paymentCustomValue
                        Else
                            xlWorksheet.cells(35, 2).value = "Radio Operator's Cert."
                        End If
                        xlWorksheet.cells(35, 6).value = row.Cells(3).FormattedValue.ToString
                        xlWorksheet.cells(35, 7).value = Double.Parse(row.Cells(4).FormattedValue.ToString) / 100
                        xlWorksheet.cells(35, 8).value = row.Cells(5).FormattedValue.ToString
                        xlWorksheet.cells(35, 9).value = row.Cells(6).FormattedValue.ToString

                        If tb_particular_two.Text <> "N/A" Then
                            xlWorksheet.cells(35, 10).value = row.Cells(7).FormattedValue.ToString
                            xlWorksheet.cells(35, 11).value = Double.Parse(row.Cells(8).FormattedValue.ToString) / 100
                            xlWorksheet.cells(35, 12).value = row.Cells(9).FormattedValue.ToString
                            xlWorksheet.cells(35, 13).value = row.Cells(10).FormattedValue.ToString
                        End If

                        If tb_particular_three.Text <> "N/A" Then
                            xlWorksheet.cells(35, 14).value = row.Cells(11).FormattedValue.ToString
                            xlWorksheet.cells(35, 15).value = Double.Parse(row.Cells(12).FormattedValue.ToString) / 100
                            xlWorksheet.cells(35, 16).value = row.Cells(13).FormattedValue.ToString
                            xlWorksheet.cells(35, 17).value = row.Cells(14).FormattedValue.ToString
                        End If

                        xlWorksheet.cells(35, 18).value = row.Cells(15).FormattedValue.ToString
                    End If

                    If row.Cells(0).FormattedValue = "4-02-01-130" And row.Cells(16).FormattedValue = "17" And
                        row.Cells(2).FormattedValue = "AROC/ROC" Then
                        If paymentCustomValue <> "" Then
                            xlWorksheet.cells(36, 2).value = "Application Fee/Filing Fee: " + paymentCustomValue
                        Else
                            xlWorksheet.cells(36, 2).value = "Application Fee/Filing Fee"
                        End If
                        xlWorksheet.cells(36, 6).value = row.Cells(3).FormattedValue.ToString
                        xlWorksheet.cells(36, 7).value = Double.Parse(row.Cells(4).FormattedValue.ToString) / 100
                        xlWorksheet.cells(36, 8).value = row.Cells(5).FormattedValue.ToString
                        xlWorksheet.cells(36, 9).value = row.Cells(6).FormattedValue.ToString

                        If tb_particular_two.Text <> "N/A" Then
                            xlWorksheet.cells(36, 10).value = row.Cells(7).FormattedValue.ToString
                            xlWorksheet.cells(36, 11).value = Double.Parse(row.Cells(8).FormattedValue.ToString) / 100
                            xlWorksheet.cells(36, 12).value = row.Cells(9).FormattedValue.ToString
                            xlWorksheet.cells(36, 13).value = row.Cells(10).FormattedValue.ToString
                        End If

                        If tb_particular_three.Text <> "N/A" Then
                            xlWorksheet.cells(36, 14).value = row.Cells(11).FormattedValue.ToString
                            xlWorksheet.cells(36, 15).value = Double.Parse(row.Cells(12).FormattedValue.ToString) / 100
                            xlWorksheet.cells(36, 16).value = row.Cells(13).FormattedValue.ToString
                            xlWorksheet.cells(36, 17).value = row.Cells(14).FormattedValue.ToString
                        End If

                        xlWorksheet.cells(36, 18).value = row.Cells(15).FormattedValue.ToString
                    End If

                    If row.Cells(0).FormattedValue = "4-02-02-040" And row.Cells(16).FormattedValue = "18" And
                        row.Cells(2).FormattedValue = "AROC/ROC" Then
                        If paymentCustomValue <> "" Then
                            xlWorksheet.cells(37, 2).value = "Seminar Fee: " + paymentCustomValue
                        Else
                            xlWorksheet.cells(37, 2).value = "Seminar Fee"
                        End If
                        xlWorksheet.cells(37, 6).value = row.Cells(3).FormattedValue.ToString
                        xlWorksheet.cells(37, 7).value = Double.Parse(row.Cells(4).FormattedValue.ToString) / 100
                        xlWorksheet.cells(37, 8).value = row.Cells(5).FormattedValue.ToString
                        xlWorksheet.cells(37, 9).value = row.Cells(6).FormattedValue.ToString

                        If tb_particular_two.Text <> "N/A" Then
                            xlWorksheet.cells(37, 10).value = row.Cells(7).FormattedValue.ToString
                            xlWorksheet.cells(37, 11).value = Double.Parse(row.Cells(8).FormattedValue.ToString) / 100
                            xlWorksheet.cells(37, 12).value = row.Cells(9).FormattedValue.ToString
                            xlWorksheet.cells(37, 13).value = row.Cells(10).FormattedValue.ToString
                        End If

                        If tb_particular_three.Text <> "N/A" Then
                            xlWorksheet.cells(37, 14).value = row.Cells(11).FormattedValue.ToString
                            xlWorksheet.cells(37, 15).value = Double.Parse(row.Cells(12).FormattedValue.ToString) / 100
                            xlWorksheet.cells(37, 16).value = row.Cells(13).FormattedValue.ToString
                            xlWorksheet.cells(37, 17).value = row.Cells(14).FormattedValue.ToString
                        End If

                        xlWorksheet.cells(37, 18).value = row.Cells(15).FormattedValue.ToString
                    End If

                    If row.Cells(0).FormattedValue = "4-02-01-140" And row.Cells(16).FormattedValue = "19" And
                        row.Cells(2).FormattedValue = "AROC/ROC" Then
                        If paymentCustomValue <> "" Then
                            xlWorksheet.cells(38, 2).value = "Fines/Penalties/Surcharges: " + paymentCustomValue
                        Else
                            xlWorksheet.cells(38, 2).value = "Fines/Penalties/Surcharges"
                        End If
                        xlWorksheet.cells(38, 6).value = row.Cells(3).FormattedValue.ToString
                        xlWorksheet.cells(38, 7).value = Double.Parse(row.Cells(4).FormattedValue.ToString) / 100
                        xlWorksheet.cells(38, 8).value = row.Cells(5).FormattedValue.ToString
                        xlWorksheet.cells(38, 9).value = row.Cells(6).FormattedValue.ToString

                        If tb_particular_two.Text <> "N/A" Then
                            xlWorksheet.cells(38, 10).value = row.Cells(7).FormattedValue.ToString
                            xlWorksheet.cells(38, 11).value = Double.Parse(row.Cells(8).FormattedValue.ToString) / 100
                            xlWorksheet.cells(38, 12).value = row.Cells(9).FormattedValue.ToString
                            xlWorksheet.cells(38, 13).value = row.Cells(10).FormattedValue.ToString
                        End If

                        If tb_particular_three.Text <> "N/A" Then
                            xlWorksheet.cells(38, 14).value = row.Cells(11).FormattedValue.ToString
                            xlWorksheet.cells(38, 15).value = Double.Parse(row.Cells(12).FormattedValue.ToString) / 100
                            xlWorksheet.cells(38, 16).value = row.Cells(13).FormattedValue.ToString
                            xlWorksheet.cells(38, 17).value = row.Cells(14).FormattedValue.ToString
                        End If

                        xlWorksheet.cells(38, 18).value = row.Cells(15).FormattedValue.ToString
                    End If

                    If row.Cells(0).FormattedValue = "4-02-01-140" And row.Cells(16).FormattedValue = "20" And
                        row.Cells(2).FormattedValue = "AROC/ROC" Then
                        If paymentCustomValue <> "" Then
                            xlWorksheet.cells(39, 2).value = "(SUR)Radio Station License: " + paymentCustomValue
                        Else
                            xlWorksheet.cells(39, 2).value = "(SUR)Radio Station License"
                        End If
                        xlWorksheet.cells(39, 6).value = row.Cells(3).FormattedValue.ToString
                        xlWorksheet.cells(39, 7).value = Double.Parse(row.Cells(4).FormattedValue.ToString) / 100
                        xlWorksheet.cells(39, 8).value = row.Cells(5).FormattedValue.ToString
                        xlWorksheet.cells(39, 9).value = row.Cells(6).FormattedValue.ToString

                        If tb_particular_two.Text <> "N/A" Then
                            xlWorksheet.cells(39, 10).value = row.Cells(7).FormattedValue.ToString
                            xlWorksheet.cells(39, 11).value = Double.Parse(row.Cells(8).FormattedValue.ToString) / 100
                            xlWorksheet.cells(39, 12).value = row.Cells(9).FormattedValue.ToString
                            xlWorksheet.cells(39, 13).value = row.Cells(10).FormattedValue.ToString
                        End If

                        If tb_particular_three.Text <> "N/A" Then
                            xlWorksheet.cells(39, 14).value = row.Cells(11).FormattedValue.ToString
                            xlWorksheet.cells(39, 15).value = Double.Parse(row.Cells(12).FormattedValue.ToString) / 100
                            xlWorksheet.cells(39, 16).value = row.Cells(13).FormattedValue.ToString
                            xlWorksheet.cells(39, 17).value = row.Cells(14).FormattedValue.ToString
                        End If

                        xlWorksheet.cells(39, 18).value = row.Cells(15).FormattedValue.ToString
                    End If

                    If row.Cells(0).FormattedValue = "4-02-01-140" And row.Cells(16).FormattedValue = "21" And
                        row.Cells(2).FormattedValue = "AROC/ROC" Then
                        If paymentCustomValue <> "" Then
                            xlWorksheet.cells(40, 2).value = "(SUR)Radio Operator's Cert.: " + paymentCustomValue
                        Else
                            xlWorksheet.cells(40, 2).value = "(SUR)Radio Operator's Cert."
                        End If
                        xlWorksheet.cells(40, 6).value = row.Cells(3).FormattedValue.ToString
                        xlWorksheet.cells(40, 7).value = Double.Parse(row.Cells(4).FormattedValue.ToString) / 100
                        xlWorksheet.cells(40, 8).value = row.Cells(5).FormattedValue.ToString
                        xlWorksheet.cells(40, 9).value = row.Cells(6).FormattedValue.ToString

                        If tb_particular_two.Text <> "N/A" Then
                            xlWorksheet.cells(40, 10).value = row.Cells(7).FormattedValue.ToString
                            xlWorksheet.cells(40, 11).value = Double.Parse(row.Cells(8).FormattedValue.ToString) / 100
                            xlWorksheet.cells(40, 12).value = row.Cells(9).FormattedValue.ToString
                            xlWorksheet.cells(40, 13).value = row.Cells(10).FormattedValue.ToString
                        End If

                        If tb_particular_three.Text <> "N/A" Then
                            xlWorksheet.cells(40, 14).value = row.Cells(11).FormattedValue.ToString
                            xlWorksheet.cells(40, 15).value = Double.Parse(row.Cells(12).FormattedValue.ToString) / 100
                            xlWorksheet.cells(40, 16).value = row.Cells(13).FormattedValue.ToString
                            xlWorksheet.cells(40, 17).value = row.Cells(14).FormattedValue.ToString
                        End If

                        xlWorksheet.cells(40, 18).value = row.Cells(15).FormattedValue.ToString
                    End If

                    ' OTHER MOPs. OTHER MOPs. OTHER MOPs. OTHER MOPs. OTHER MOPs. OTHER MOPs. OTHER MOPs. OTHER MOPs.
                    If row.Cells(0).FormattedValue = "4-02-01-020" And row.Cells(16).FormattedValue = "22" And
                        row.Cells(2).FormattedValue = "OTHER" Then
                        If paymentCustomValue <> "" Then
                            xlWorksheet.cells(42, 2).value = "Registration Fee: " + paymentCustomValue
                        Else
                            xlWorksheet.cells(42, 2).value = "Registration Fee"
                        End If
                        xlWorksheet.cells(42, 6).value = row.Cells(3).FormattedValue.ToString
                        xlWorksheet.cells(42, 7).value = Double.Parse(row.Cells(4).FormattedValue.ToString) / 100
                        xlWorksheet.cells(42, 8).value = row.Cells(5).FormattedValue.ToString
                        xlWorksheet.cells(42, 9).value = row.Cells(6).FormattedValue.ToString

                        If tb_particular_two.Text <> "N/A" Then
                            xlWorksheet.cells(42, 10).value = row.Cells(7).FormattedValue.ToString
                            xlWorksheet.cells(42, 11).value = Double.Parse(row.Cells(8).FormattedValue.ToString) / 100
                            xlWorksheet.cells(42, 12).value = row.Cells(9).FormattedValue.ToString
                            xlWorksheet.cells(42, 13).value = row.Cells(10).FormattedValue.ToString
                        End If

                        If tb_particular_three.Text <> "N/A" Then
                            xlWorksheet.cells(42, 14).value = row.Cells(11).FormattedValue.ToString
                            xlWorksheet.cells(42, 15).value = Double.Parse(row.Cells(12).FormattedValue.ToString) / 100
                            xlWorksheet.cells(42, 16).value = row.Cells(13).FormattedValue.ToString
                            xlWorksheet.cells(42, 17).value = row.Cells(14).FormattedValue.ToString
                        End If

                        xlWorksheet.cells(42, 18).value = row.Cells(15).FormattedValue.ToString
                    End If

                    If row.Cells(0).FormattedValue = "4-02-01-070" And row.Cells(16).FormattedValue = "23" And
                        row.Cells(2).FormattedValue = "OTHER" Then
                        If paymentCustomValue <> "" Then
                            xlWorksheet.cells(43, 2).value = "Supervision & Regulation Fee: " + paymentCustomValue
                        Else
                            xlWorksheet.cells(43, 2).value = "Supervision & Regulation Fee"
                        End If
                        xlWorksheet.cells(43, 6).value = row.Cells(3).FormattedValue.ToString
                        xlWorksheet.cells(43, 7).value = Double.Parse(row.Cells(4).FormattedValue.ToString) / 100
                        xlWorksheet.cells(43, 8).value = row.Cells(5).FormattedValue.ToString
                        xlWorksheet.cells(43, 9).value = row.Cells(6).FormattedValue.ToString

                        If tb_particular_two.Text <> "N/A" Then
                            xlWorksheet.cells(43, 10).value = row.Cells(7).FormattedValue.ToString
                            xlWorksheet.cells(43, 11).value = Double.Parse(row.Cells(8).FormattedValue.ToString) / 100
                            xlWorksheet.cells(43, 12).value = row.Cells(9).FormattedValue.ToString
                            xlWorksheet.cells(43, 13).value = row.Cells(10).FormattedValue.ToString
                        End If

                        If tb_particular_three.Text <> "N/A" Then
                            xlWorksheet.cells(43, 14).value = row.Cells(11).FormattedValue.ToString
                            xlWorksheet.cells(43, 15).value = Double.Parse(row.Cells(12).FormattedValue.ToString) / 100
                            xlWorksheet.cells(43, 16).value = row.Cells(13).FormattedValue.ToString
                            xlWorksheet.cells(43, 17).value = row.Cells(14).FormattedValue.ToString
                        End If

                        xlWorksheet.cells(43, 18).value = row.Cells(15).FormattedValue.ToString
                    End If

                    If row.Cells(0).FormattedValue = "4-02-01-040" And row.Cells(16).FormattedValue = "24" And
                        row.Cells(2).FormattedValue = "OTHER" Then
                        If paymentCustomValue <> "" Then
                            xlWorksheet.cells(44, 2).value = "Verification Fee & Authentication Fee: " & paymentCustomValue
                        Else
                            xlWorksheet.cells(44, 2).value = "Verification Fee & Authentication Fee"
                        End If
                        xlWorksheet.cells(44, 6).value = row.Cells(3).FormattedValue.ToString
                        xlWorksheet.cells(44, 7).value = Double.Parse(row.Cells(4).FormattedValue.ToString) / 100
                        xlWorksheet.cells(44, 8).value = row.Cells(5).FormattedValue.ToString
                        xlWorksheet.cells(44, 9).value = row.Cells(6).FormattedValue.ToString

                        If tb_particular_two.Text <> "N/A" Then
                            xlWorksheet.cells(44, 10).value = row.Cells(7).FormattedValue.ToString
                            xlWorksheet.cells(44, 11).value = Double.Parse(row.Cells(8).FormattedValue.ToString) / 100
                            xlWorksheet.cells(44, 12).value = row.Cells(9).FormattedValue.ToString
                            xlWorksheet.cells(44, 13).value = row.Cells(10).FormattedValue.ToString
                        End If

                        If tb_particular_three.Text <> "N/A" Then
                            xlWorksheet.cells(44, 14).value = row.Cells(11).FormattedValue.ToString
                            xlWorksheet.cells(44, 15).value = Double.Parse(row.Cells(12).FormattedValue.ToString) / 100
                            xlWorksheet.cells(44, 16).value = row.Cells(13).FormattedValue.ToString
                            xlWorksheet.cells(44, 17).value = row.Cells(14).FormattedValue.ToString
                        End If

                        xlWorksheet.cells(44, 18).value = row.Cells(15).FormattedValue.ToString
                    End If

                    If row.Cells(0).FormattedValue = "4-02-01-030" And row.Cells(16).FormattedValue = "25" And
                        row.Cells(2).FormattedValue = "OTHER" Then
                        If paymentCustomValue <> "" Then
                            xlWorksheet.cells(45, 2).value = "Examination Fee: " & paymentCustomValue
                        Else
                            xlWorksheet.cells(45, 2).value = "Examination Fee"
                        End If
                        xlWorksheet.cells(45, 6).value = row.Cells(3).FormattedValue.ToString
                        xlWorksheet.cells(45, 7).value = Double.Parse(row.Cells(4).FormattedValue.ToString) / 100
                        xlWorksheet.cells(45, 8).value = row.Cells(5).FormattedValue.ToString
                        xlWorksheet.cells(45, 9).value = row.Cells(6).FormattedValue.ToString

                        If tb_particular_two.Text <> "N/A" Then
                            xlWorksheet.cells(45, 10).value = row.Cells(7).FormattedValue.ToString
                            xlWorksheet.cells(45, 11).value = Double.Parse(row.Cells(8).FormattedValue.ToString) / 100
                            xlWorksheet.cells(45, 12).value = row.Cells(9).FormattedValue.ToString
                            xlWorksheet.cells(45, 13).value = row.Cells(10).FormattedValue.ToString
                        End If

                        If tb_particular_three.Text <> "N/A" Then
                            xlWorksheet.cells(45, 14).value = row.Cells(11).FormattedValue.ToString
                            xlWorksheet.cells(45, 15).value = Double.Parse(row.Cells(12).FormattedValue.ToString) / 100
                            xlWorksheet.cells(45, 16).value = row.Cells(13).FormattedValue.ToString
                            xlWorksheet.cells(45, 17).value = row.Cells(14).FormattedValue.ToString
                        End If

                        xlWorksheet.cells(45, 18).value = row.Cells(15).FormattedValue.ToString
                    End If

                    If row.Cells(0).FormattedValue = "4-02-01-040" And row.Cells(16).FormattedValue = "26" And
                        row.Cells(2).FormattedValue = "OTHER" Then
                        If paymentCustomValue <> "" Then
                            xlWorksheet.cells(46, 2).value = "Clearance/Certification Fee: " & paymentCustomValue
                        Else
                            xlWorksheet.cells(46, 2).value = "Clearance/Certification Fee"
                        End If
                        xlWorksheet.cells(46, 6).value = row.Cells(3).FormattedValue.ToString
                        xlWorksheet.cells(46, 7).value = Double.Parse(row.Cells(4).FormattedValue.ToString) / 100
                        xlWorksheet.cells(46, 8).value = row.Cells(5).FormattedValue.ToString
                        xlWorksheet.cells(46, 9).value = row.Cells(6).FormattedValue.ToString

                        If tb_particular_two.Text <> "N/A" Then
                            xlWorksheet.cells(46, 10).value = row.Cells(7).FormattedValue.ToString
                            xlWorksheet.cells(46, 11).value = Double.Parse(row.Cells(8).FormattedValue.ToString) / 100
                            xlWorksheet.cells(46, 12).value = row.Cells(9).FormattedValue.ToString
                            xlWorksheet.cells(46, 13).value = row.Cells(10).FormattedValue.ToString
                        End If

                        If tb_particular_three.Text <> "N/A" Then
                            xlWorksheet.cells(46, 14).value = row.Cells(11).FormattedValue.ToString
                            xlWorksheet.cells(46, 15).value = Double.Parse(row.Cells(12).FormattedValue.ToString) / 100
                            xlWorksheet.cells(46, 16).value = row.Cells(13).FormattedValue.ToString
                            xlWorksheet.cells(46, 17).value = row.Cells(14).FormattedValue.ToString
                        End If

                        xlWorksheet.cells(46, 18).value = row.Cells(15).FormattedValue.ToString
                    End If


                    If row.Cells(0).FormattedValue = "4-02-01-060" And row.Cells(16).FormattedValue = "27" And
                        row.Cells(2).FormattedValue = "OTHER" Then
                        If paymentCustomValue <> "" Then
                            xlWorksheet.cells(47, 2).value = "Modification Fee: " + paymentCustomValue
                        Else
                            xlWorksheet.cells(47, 2).value = "Modification Fee"
                        End If
                        xlWorksheet.cells(47, 6).value = row.Cells(3).FormattedValue.ToString
                        xlWorksheet.cells(47, 7).value = Double.Parse(row.Cells(4).FormattedValue.ToString) / 100
                        xlWorksheet.cells(47, 8).value = row.Cells(5).FormattedValue.ToString
                        xlWorksheet.cells(47, 9).value = row.Cells(6).FormattedValue.ToString

                        If tb_particular_two.Text <> "N/A" Then
                            xlWorksheet.cells(47, 10).value = row.Cells(7).FormattedValue.ToString
                            xlWorksheet.cells(47, 11).value = Double.Parse(row.Cells(8).FormattedValue.ToString) / 100
                            xlWorksheet.cells(47, 12).value = row.Cells(9).FormattedValue.ToString
                            xlWorksheet.cells(47, 13).value = row.Cells(10).FormattedValue.ToString
                        End If

                        If tb_particular_three.Text <> "N/A" Then
                            xlWorksheet.cells(47, 14).value = row.Cells(11).FormattedValue.ToString
                            xlWorksheet.cells(47, 15).value = Double.Parse(row.Cells(12).FormattedValue.ToString) / 100
                            xlWorksheet.cells(47, 16).value = row.Cells(13).FormattedValue.ToString
                            xlWorksheet.cells(47, 17).value = row.Cells(14).FormattedValue.ToString
                        End If

                        xlWorksheet.cells(47, 18).value = row.Cells(15).FormattedValue.ToString
                    End If


                    If row.Cells(0).FormattedValue = "4-02-01-990" And row.Cells(16).FormattedValue = "28" And
                        row.Cells(2).FormattedValue = "OTHER" Then
                        If paymentCustomValue <> "" Then
                            xlWorksheet.cells(48, 2).value = "Miscellaneous Income: " & paymentCustomValue
                        Else
                            xlWorksheet.cells(48, 2).value = "Miscellaneous Income"
                        End If
                        xlWorksheet.cells(48, 6).value = row.Cells(3).FormattedValue.ToString
                        xlWorksheet.cells(48, 7).value = Double.Parse(row.Cells(4).FormattedValue.ToString) / 100
                        xlWorksheet.cells(48, 8).value = row.Cells(5).FormattedValue.ToString
                        xlWorksheet.cells(48, 9).value = row.Cells(6).FormattedValue.ToString

                        If tb_particular_two.Text <> "N/A" Then
                            xlWorksheet.cells(48, 10).value = row.Cells(7).FormattedValue.ToString
                            xlWorksheet.cells(48, 11).value = Double.Parse(row.Cells(8).FormattedValue.ToString) / 100
                            xlWorksheet.cells(48, 12).value = row.Cells(9).FormattedValue.ToString
                            xlWorksheet.cells(48, 13).value = row.Cells(10).FormattedValue.ToString
                        End If

                        If tb_particular_three.Text <> "N/A" Then
                            xlWorksheet.cells(48, 14).value = row.Cells(11).FormattedValue.ToString
                            xlWorksheet.cells(48, 15).value = Double.Parse(row.Cells(12).FormattedValue.ToString) / 100
                            xlWorksheet.cells(48, 16).value = row.Cells(13).FormattedValue.ToString
                            xlWorksheet.cells(48, 17).value = row.Cells(14).FormattedValue.ToString
                        End If

                        xlWorksheet.cells(48, 18).value = row.Cells(15).FormattedValue.ToString
                    End If


                    If row.Cells(0).FormattedValue = "4-01-04-010" And row.Cells(16).FormattedValue = "29" And
                        row.Cells(2).FormattedValue = "OTHER" Then
                        If paymentCustomValue <> "" Then
                            xlWorksheet.cells(49, 2).value = "Documentary Stamp Tax (DST): " & paymentCustomValue
                        Else
                            xlWorksheet.cells(49, 2).value = "Documentary Stamp Tax (DST)"
                        End If
                        xlWorksheet.cells(49, 6).value = row.Cells(3).FormattedValue.ToString
                        xlWorksheet.cells(49, 7).value = Double.Parse(row.Cells(4).FormattedValue.ToString) / 100
                        xlWorksheet.cells(49, 8).value = row.Cells(5).FormattedValue.ToString
                        xlWorksheet.cells(49, 9).value = row.Cells(6).FormattedValue.ToString

                        If tb_particular_two.Text <> "N/A" Then
                            xlWorksheet.cells(49, 10).value = row.Cells(7).FormattedValue.ToString
                            xlWorksheet.cells(49, 11).value = Double.Parse(row.Cells(8).FormattedValue.ToString) / 100
                            xlWorksheet.cells(49, 12).value = row.Cells(9).FormattedValue.ToString
                            xlWorksheet.cells(49, 13).value = row.Cells(10).FormattedValue.ToString
                        End If

                        If tb_particular_three.Text <> "N/A" Then
                            xlWorksheet.cells(49, 14).value = row.Cells(11).FormattedValue.ToString
                            xlWorksheet.cells(49, 15).value = Double.Parse(row.Cells(12).FormattedValue.ToString) / 100
                            xlWorksheet.cells(49, 16).value = row.Cells(13).FormattedValue.ToString
                            xlWorksheet.cells(49, 17).value = row.Cells(14).FormattedValue.ToString
                        End If

                        xlWorksheet.cells(49, 18).value = row.Cells(15).FormattedValue.ToString
                    End If

                    If row.Cells(0).FormattedValue = "0-00-00-000" And row.Cells(16).FormattedValue = "30" And
                        row.Cells(2).FormattedValue = "OTHER" Then
                        If paymentCustomValue <> "" Then
                            xlWorksheet.cells(50, 2).value = "Others: " & paymentCustomValue
                        Else
                            xlWorksheet.cells(50, 2).value = "Others"
                        End If

                        xlWorksheet.cells(50, 6).value = row.Cells(3).FormattedValue.ToString
                        xlWorksheet.cells(50, 7).value = Double.Parse(row.Cells(4).FormattedValue.ToString) / 100
                        xlWorksheet.cells(50, 8).value = row.Cells(5).FormattedValue.ToString
                        xlWorksheet.cells(50, 9).value = row.Cells(6).FormattedValue.ToString

                        If tb_particular_two.Text <> "N/A" Then
                            xlWorksheet.cells(50, 10).value = row.Cells(7).FormattedValue.ToString
                            xlWorksheet.cells(50, 11).value = Double.Parse(row.Cells(8).FormattedValue.ToString) / 100
                            xlWorksheet.cells(50, 12).value = row.Cells(9).FormattedValue.ToString
                            xlWorksheet.cells(50, 13).value = row.Cells(10).FormattedValue.ToString
                        End If

                        If tb_particular_three.Text <> "N/A" Then
                            xlWorksheet.cells(50, 14).value = row.Cells(11).FormattedValue.ToString
                            xlWorksheet.cells(50, 15).value = Double.Parse(row.Cells(12).FormattedValue.ToString) / 100
                            xlWorksheet.cells(50, 16).value = row.Cells(13).FormattedValue.ToString
                            xlWorksheet.cells(50, 17).value = row.Cells(14).FormattedValue.ToString
                        End If

                        xlWorksheet.cells(50, 18).value = row.Cells(15).FormattedValue.ToString
                    End If
                    paymentCustomValue = ""
                Next

                ' Copy other SOA details.
                xlWorksheet.cells(6, 5).value = tb_payor.Text
                xlWorksheet.cells(4, 15).value = tb_serial_num.Text
                xlWorksheet.cells(6, 15).value = soa_date_created
                xlWorksheet.cells(53, 9).value = dateExpirationLongDate
                xlWorksheet.cells(62, 10).value = tb_prepared_by.Text
                xlWorksheet.cells(62, 15).value = tb_approved.Text
                xlWorksheet.cells(14, 7).value = tb_particular_one.Text
                xlWorksheet.cells(15, 8).value = tb_particular_one_pc_from.Text + "-" &
                    tb_particular_one_pc_to.Text
                xlWorksheet.cells(52, 18).value = tb_total.Text

                ' If assessment is enabled.
                If is_assess_button Then
                    xlWorksheet.cells(55, 6).value = "X"
                    is_assess_button = False
                End If

                ' If endorsement is enabled.
                If is_endorse_button Then
                    xlWorksheet.cells(55, 12).value = "X"
                    is_endorse_button = False
                End If

                ' If SOA is paid.
                If is_soa_paid Then
                    xlWorksheet.cells(54, 17).value = "OR # " + OR_num
                    xlWorksheet.cells(55, 17).value = "TOTAL: " + tb_total.Text
                    xlWorksheet.cells(56, 17).value = "DATE: " + OR_date
                End If

                retrieve_signatures()

                ' Set excel program to be visible, maximize and on top.
                xlapp.Visible = True
                xlapp.Top = 1
                xlapp.ActiveWindow.WindowState = Excel.XlWindowState.xlMaximized

                re_enable_print_service()
                is_receipt_created()

                soaTemplateExists = False
            Catch ex As MySqlException
                class_connection.con.Close()
                MsgBox("Couldn't connect to server. Contact the IT Administrator for assistance.",
                    MsgBoxStyle.Exclamation, "NTC Region 10")
                is_endorse_button = False
                is_assess_button = False
                is_soa_paid = False
                soaTemplateExists = False
            Catch ex As Exception
                class_connection.con.Close()
                MsgBox("Something went wrong. Couldn't process the data.", MsgBoxStyle.Exclamation,
                "NTC Region 10")
                is_endorse_button = False
                is_assess_button = False
                is_soa_paid = False
                soaTemplateExists = False
            End Try
        Else
            MsgBox("SOA Template missing! Couldn't export data to excel.", MsgBoxStyle.Exclamation, "NTC Region 10")
        End If
    End Sub

    Sub re_enable_print_service()
        Try
            Dim datenow As Date = Date.ParseExact(Date.Now.ToString("yyyy-MM-dd"), "yyyy-MM-dd",
                CultureInfo.InvariantCulture)
            Dim is_endorsed_already = 0

            ' Enable assessment and endorsement export button.
            btn_print_assessment.Enabled = True
            btn_print_endorse.Enabled = True

            ' Retrieve SOA endorse status.
            class_connection.con.Open()
            query = "select is_endorse from tbl_soa_transact where soa_id = '" &
                soa_id_transact.ToString & "'"
            cmd = New MySqlCommand(query, class_connection.con)
            reader = cmd.ExecuteReader()

            If reader.Read Then
                is_endorsed_already = reader.GetInt32("is_endorse")
            End If
            class_connection.con.Close()

            ' If it not yet endorsed.
            If is_endorsed_already = 0 Then
                class_connection.con.Open()
                query = "select endorse_id as 'ID' from tbl_soa_endorse where soa_id = '" &
                    soa_id_transact.ToString & "'"
                cmd = New MySqlCommand(query, class_connection.con)
                reader = cmd.ExecuteReader()

                If reader.Read Then
                    If expiry_date <= datenow Then
                        ' Disable endorse if it pass its expiration date.
                        btn_endorse.Enabled = False
                    End If
                Else
                    ' If it doesn't have OP. Enable endorse button.
                    btn_endorse.Enabled = True
                End If
                class_connection.con.Close()
            End If
        Catch ex As MySqlException
            class_connection.con.Close()
            MsgBox("Couldn't connect to server. Print services are temporarily disabled. Close " &
                "the form And open it again.", MsgBoxStyle.Exclamation, "NTC Region 10")
            btn_print_assessment.Enabled = False
            btn_edit.Enabled = False
            btn_attachment.Enabled = False
            btn_op.Enabled = False
            btn_receipt.Enabled = False
        Catch ex As Exception
            class_connection.con.Close()
            MsgBox("Couldn't connect to server. Print Services are temporarily disabled. Close " &
                "the form and open it again.", MsgBoxStyle.Exclamation, "NTC Region 10")
            btn_print_assessment.Enabled = False
            btn_edit.Enabled = False
            btn_attachment.Enabled = False
            btn_op.Enabled = False
            btn_receipt.Enabled = False
        End Try
    End Sub

    Sub retrieve_signatures()
        ' Retrieve all participants' signature.
        Dim target As String = "c:\temp"

        If Directory.Exists(target) = False Then
            ' If target file isn't exist.
            Directory.CreateDirectory(target)
        End If

        ' Retrieve preparator's signature.
        download_signature_preparator()

        ' Retrieve approver's signature.
        download_signature_approver()

        ' If SOA is paid.
        If is_soa_paid Then
            ' Retrieve cashier's signature.
            download_signature_cashier()

            ' If cashier enables usage of his signature.
            If enable_cashier_signature = "1" Then
                If File.Exists("C:\temp\" + signature_cashier) Then
                    xlWorksheet.Shapes.AddPicture("C:\temp\" + signature_cashier,
                Microsoft.Office.Core.MsoTriState.msoFalse,
                Microsoft.Office.Core.MsoTriState.msoCTrue, 835, 700, 100, 95)
                End If
            End If
        End If

        ' If preparator enables usage of his signature.
        If enable_preparator_signature = "1" Then
            If File.Exists("C:\temp\" + signature_preparator) Then
                xlWorksheet.Shapes.AddPicture("C:\temp\" + signature_preparator,
                Microsoft.Office.Core.MsoTriState.msoFalse,
                Microsoft.Office.Core.MsoTriState.msoCTrue, 505, 745, 100, 85)
            End If
        End If

        ' If approver enables usage of his signature.
        If enable_approver_signature = "1" Then
            If File.Exists("C:\temp\" + signature_approver) Then
                xlWorksheet.Shapes.AddPicture("C:\temp\" + signature_approver,
                Microsoft.Office.Core.MsoTriState.msoFalse,
                Microsoft.Office.Core.MsoTriState.msoCTrue, 745, 745, 100, 85)
            End If
        End If
    End Sub

    Sub download_signature_preparator()
        ' Retrieve preparator's signature.
        Try
            Dim WebClient As WebClient = New WebClient()

            class_connection.con.Open()
            query = "select signature as 'path', signature_name as 'name', enable_signature from " &
                "tbl_user where user_id = '" & soa_preparator_id & "'"
            cmd = New MySqlCommand(query, class_connection.con)
            reader = cmd.ExecuteReader()

            If reader.Read Then
                If reader.GetString("enable_signature") = "1" Then
                    enable_preparator_signature = reader.GetString("enable_signature")
                    signature_preparator = reader.GetString("name")

                    WebClient.Credentials = New NetworkCredential("ntc_10_server", "ntc_10_server",
                        "ftp://" + class_connection.ftpAddress)
                    WebClient.DownloadFile(WebUtility.HtmlDecode(reader.GetString("path")),
                        "C:\temp\" + reader.GetString("name"))
                End If
            End If
            class_connection.con.Close()
        Catch ex As MySqlException
            class_connection.con.Close()
            MsgBox("Couldn't connect to server. Failed to download preparator's Signature.",
                MsgBoxStyle.Exclamation, "NTC Region 10")
        Catch ex As WebException
            class_connection.con.Close()
        Catch ex As Exception
            class_connection.con.Close()
            MsgBox("Something went wrong. Couldn't process the data.", MsgBoxStyle.Exclamation,
                "NTC Region 10")
        End Try
    End Sub

    Sub download_signature_approver()
        ' Retrieve approver's signature.
        Try
            Dim WebClient As WebClient = New WebClient()

            class_connection.con.Open()
            query = "select signature as 'path', signature_name as 'name', enable_signature from " &
                "tbl_user where user_id = '" & soa_approver_id & "'"
            cmd = New MySqlCommand(query, class_connection.con)
            reader = cmd.ExecuteReader()

            If reader.Read Then
                If reader.GetString("enable_signature") = "1" Then
                    enable_approver_signature = reader.GetString("enable_signature")
                    signature_approver = reader.GetString("name")

                    WebClient.Credentials = New NetworkCredential("ntc_10_server", "ntc_10_server",
                        "ftp://" + class_connection.ftpAddress)
                    WebClient.DownloadFile(WebUtility.HtmlDecode(reader.GetString("path")),
                        "C:\temp\" + reader.GetString("name"))
                End If
            End If
            class_connection.con.Close()
        Catch ex As MySqlException
            class_connection.con.Close()
            MsgBox("Couldn't connect to server. Failed to download Approver's Signature.",
                MsgBoxStyle.Exclamation, "NTC Region 10")
        Catch ex As WebException
            class_connection.con.Close()
        Catch ex As Exception
            class_connection.con.Close()
            MsgBox("Something went wrong. Couldn't process the data.", MsgBoxStyle.Exclamation,
                "NTC Region 10")
        End Try
    End Sub

    Sub download_signature_cashier()
        ' Retrieve cashier's signature.
        Try
            Dim WebClient As WebClient = New WebClient()
            user_id_cashier = retrieve_current_cashier()

            class_connection.con.Open()
            query = "select signature as 'path', signature_name as 'name', enable_signature from tbl_user " &
                "where user_id = '" & user_id_cashier & "'"
            cmd = New MySqlCommand(query, class_connection.con)
            reader = cmd.ExecuteReader()

            If reader.Read Then
                If reader.GetString("enable_signature") = "1" Then
                    enable_cashier_signature = reader.GetString("enable_signature")
                    signature_cashier = reader.GetString("name")

                    WebClient.Credentials = New NetworkCredential("ntc_10_server", "ntc_10_server",
                        "ftp://" + class_connection.ftpAddress)
                    WebClient.DownloadFile(WebUtility.HtmlDecode(reader.GetString("path")),
                        "C:\temp\" + reader.GetString("name"))
                End If
            End If
            class_connection.con.Close()
        Catch ex As MySqlException
            class_connection.con.Close()
            MsgBox("Couldn't connect to server. Failed to download Cashier's Signature. Contact the IT " &
                "Administrator for assistance.", MsgBoxStyle.Exclamation, "NTC Region 10")
        Catch ex As WebException
            class_connection.con.Close()
        Catch ex As Exception
            class_connection.con.Close()
            MsgBox("Something went wrong. Couldn't process the data.", MsgBoxStyle.Exclamation,
                "NTC Region 10")
        End Try
    End Sub

    Function retrieve_current_cashier()
        ' Retrieve current cashier's ID.
        Try
            Dim cashier_id = ""

            class_connection.con.Open()
            query = "select setting_value from tbl_settings where setting = 'cashiering_officer'"
            cmd = New MySqlCommand(query, class_connection.con)
            reader = cmd.ExecuteReader()

            If reader.Read Then
                cashier_id = reader.GetString("setting_value")
            End If
            class_connection.con.Close()

            Return cashier_id
        Catch ex As MySqlException
            class_connection.con.Close()
            MsgBox("Couldn't retrieve cashier.", MsgBoxStyle.Exclamation, "NTC Region 10")
            Return Nothing
        Catch ex As Exception
            class_connection.con.Close()
            MsgBox("Something went wrong. Couldn't process the data.", MsgBoxStyle.Exclamation,
                "NTC Region 10")
            Return Nothing
        End Try
    End Function

    Sub load_soa_details()
        Try
            ' Load SOA details.
            class_connection.con.Open()
            query = "select tbl_soa_transact.user_id as 'user id', user_id_approve, is_endorse, " &
                "application_type, date_created, serial_id, date_expires,date_modified,full_name, " &
                "tbl_soa_transact.soa_details_id, user_id_approve, attachment_id,process_id, " &
                "service_id, particular_id, soa_data_id from tbl_soa_transact join tbl_soa_details " &
                "on tbl_soa_details.soa_details_id = tbl_soa_transact.soa_details_id join tbl_user " &
                "on tbl_user.user_id = tbl_soa_transact.user_id where tbl_soa_transact.soa_id = '" &
                soa_id_transact.ToString & "'"
            cmd = New MySqlCommand(query, class_connection.con)
            reader = cmd.ExecuteReader()

            If reader.Read Then
                soa_preparator_id = reader.GetString("user id")
                soa_approver_id = reader.GetString("user_id_approve")
                is_endorse = reader.GetString("is_endorse")
                tb_transact_type.Text = reader.GetString("application_type")
                soa_date_created = reader.GetDateTime("date_created").ToLongDateString
                tb_date_created.Text = reader.GetDateTime("date_created").ToLongDateString & " " &
                    reader.GetDateTime("date_created").ToString("hh:mm:ss tt")
                tb_serial_num.Text = reader.GetString("serial_id")
                tb_expiration.Text = reader.GetDateTime("date_expires").ToLongDateString.ToString
                dateExpirationLongDate = reader.GetDateTime("date_expires").ToLongDateString.ToString
                expiry_date = reader.GetMySqlDateTime("date_expires")
                tb_prepared_by.Text = reader.GetString("full_name")
                soa_details_id = reader.GetInt32("soa_details_id")
                user_id_preparator = reader.GetString("user id")
                user_id_approver = reader.GetString("user_id_approve")
                attachmentID = reader.GetString("attachment_id")
                processID = reader.GetString("process_id")
                serviceID = reader.GetString("service_id")
                particularID = reader.GetString("particular_id")
                soaDataID = reader.GetString("soa_data_id")
                tb_date_modified.Text = reader.GetDateTime("date_modified").ToLongDateString & " " &
                    reader.GetDateTime("date_modified").ToString("hh:mm:ss tt")

                ' Display message when SOA is already endorsed.
                If reader.GetString("is_endorse") = "1" Then
                    btn_endorse.Enabled = False
                    tb_message.ForeColor = Color.FromArgb(0, 201, 0)
                    tb_message.Text = "SOA endorsed"
                    tb_message.Visible = True
                End If
            End If
            class_connection.con.Close()

            ' Retrieve payor's name. 
            class_connection.con.Open()
            query = "select name from tbl_soa_transact join tbl_soa_details on " &
                "tbl_soa_details.soa_details_id = tbl_soa_transact.soa_details_id join tbl_payor on " &
                "tbl_payor.payor_id = tbl_soa_details.payor_id where tbl_soa_transact.soa_id = '" &
                soa_id_transact.ToString & "'"
            cmd = New MySqlCommand(query, class_connection.con)
            reader = cmd.ExecuteReader()

            If reader.Read Then
                tb_payor.Text = WebUtility.HtmlDecode(reader.GetString("name"))
            End If
            class_connection.con.Close()

            ' Set process to none.
            process_type = ""
            process_type_counter = 0

            ' Retrieve process type.
            class_connection.con.Open()
            query = "select process_name from tbl_process_soa where process_type_id = '" & processID &
                "'"
            cmd = New MySqlCommand(query, class_connection.con)
            reader = cmd.ExecuteReader()

            While reader.Read
                If process_type_counter > 0 Then
                    ' Add comma every after process.
                    process_type += ", "
                End If

                ' Concat process to process type and increment process counter by 1. 
                soa_process_type(process_type_counter) = reader.GetString("process_name")
                process_type += reader.GetString("process_name")
                process_type_counter += 1
            End While
            class_connection.con.Close()

            ' Display process type after retrieving.
            tb_process_type.Text = process_type

            ' Set service to none.
            service_type = ""
            service_type_counter = 0

            ' Retrieve service type.
            class_connection.con.Open()
            query = "select service_name, custom_value from tbl_service_soa where service_type_id " &
                "= '" & serviceID & "'"
            cmd = New MySqlCommand(query, class_connection.con)
            reader = cmd.ExecuteReader()

            While reader.Read
                If service_type_counter > 0 Then
                    ' Add comma every after service.
                    service_type += ", "
                End If

                If reader.GetString("service_name") <> "OTHERS" Then
                    ' Service type is not "OTHERS".
                    soa_service_type(service_type_counter) = reader.GetString("service_name")
                    service_type += reader.GetString("service_name")
                Else
                    ' Service type is "OTHERS" then also retrieve custom value.
                    soa_service_type(service_type_counter) = reader.GetString("service_name")

                    If reader.GetString("custom_value") = "" Then
                        ' If it has no custom value.
                        soa_service_custom_value = ""
                        service_type += reader.GetString("service_name")
                    Else
                        ' If it has custom value.
                        soa_service_custom_value = reader.GetString("custom_value")
                        service_type += reader.GetString("service_name") & ": " &
                            WebUtility.HtmlDecode(reader.GetString("custom_value"))
                    End If
                End If

                ' Increment service counter by 1.
                service_type_counter += 1
            End While
            class_connection.con.Close()

            ' Display service type.
            tb_service_type.Text = service_type

            ' Initialize datagridview with modified properties.
            initialize_table()

            ' Set all particular details to N/A.
            tb_particular_one.Text = "N/A"
            tb_particular_one_pc_from.Text = "N/A"
            tb_particular_one_pc_to.Text = "N/A"
            tb_particular_two.Text = "N/A"
            tb_particular_two_pc_from.Text = "N/A"
            tb_particular_two_pc_to.Text = "N/A"
            tb_particular_three.Text = "N/A"
            tb_particular_three_pc_from.Text = "N/A"
            tb_particular_three_pc_to.Text = "N/A"

            ' Retrieve particular details.
            class_connection.con.Open()
            query = "select part_name, part_pos, part_covered_from, part_covered_to from " &
                "tbl_particular where part_id = '" & particularID & "'"
            cmd = New MySqlCommand(query, class_connection.con)
            reader = cmd.ExecuteReader()

            While reader.Read()
                If reader.GetString("part_pos") = "1" Then
                    tb_particular_one.Text = WebUtility.HtmlDecode(reader.GetString("part_name"))
                    tb_particular_one_pc_from.Text =
                        reader.GetDateTime("part_covered_from").ToString("MM/dd/yyyy")
                    tb_particular_one_pc_to.Text =
                        reader.GetDateTime("part_covered_to").ToString("MM/dd/yyyy")
                End If

                If reader.GetString("part_pos") = "2" Then
                    tb_particular_two.Text = WebUtility.HtmlDecode(reader.GetString("part_name"))
                    tb_particular_two_pc_from.Text =
                        reader.GetDateTime("part_covered_from").ToString("MM/dd/yyyy")
                    tb_particular_two_pc_to.Text =
                        reader.GetDateTime("part_covered_to").ToString("MM/dd/yyyy")
                End If

                If reader.GetString("part_pos") = "3" Then
                    tb_particular_three.Text = WebUtility.HtmlDecode(reader.GetString("part_name"))
                    tb_particular_three_pc_from.Text =
                        reader.GetDateTime("part_covered_from").ToString("MM/dd/yyyy")
                    tb_particular_three_pc_to.Text =
                        reader.GetDateTime("part_covered_to").ToString("MM/dd/yyyy")
                End If
            End While
            class_connection.con.Close()

            ' Clear datagridview rows before populate.
            dgv_soa_data.Rows.Clear()

            ' Display SOA data entries.
            class_connection.con.Open()
            query = "select tbl_soa_data.mop_id, code as 'Code', description as 'Description', " &
                "cat as 'Category', others_custom_value, p1_no_of_years as 'No. of Years 1', " &
                "p1_percent as 'Percent 1', p1_no_of_units as 'No. of Units 1', p1_fees as " &
                "'Fees 1', p2_no_of_years as 'No. of Years 2', p2_percent as 'Percent 2', " &
                "p2_no_of_units as 'No. of Units 2', p2_fees as 'Fees 2', p3_no_of_years as " &
                "'No. of Years 3', p3_percent as 'Percent 3', p3_no_of_units as 'No. of " &
                "Units 3', p3_fees as 'Fees 3' from tbl_soa_transact join tbl_soa_details " &
                "on tbl_soa_details.soa_details_id = tbl_soa_transact.soa_details_id join " &
                "tbl_soa_data on tbl_soa_data.soa_data_id = tbl_soa_details.soa_data_id join " &
                "tbl_mode_of_payment on tbl_mode_of_payment.mop_id = tbl_soa_data.mop_id  " &
                "where tbl_soa_transact.soa_id = '" & soa_id_transact.ToString & "' order by " &
                "tbl_mode_of_payment.mop_id"
            cmd = New MySqlCommand(query, class_connection.con)
            reader = cmd.ExecuteReader()

            While reader.Read
                Dim value As Double = reader.GetDouble("Percent 1")
                Dim value2 As Double = reader.GetDouble("Percent 2")
                Dim value3 As Double = reader.GetDouble("Percent 3")
                Dim num, num2, num3 As Double
                Dim percentage, percentage2, percentage3 As Double
                Dim row() As String
                Dim description = WebUtility.HtmlDecode(reader.GetString("Description"))
                Dim custom_value = WebUtility.HtmlDecode(reader.GetString("others_custom_value"))
                Dim entry_code = reader.GetString("Code")
                Dim category = reader.GetString("Category")

                ' Convert percentage to double.
                If Double.TryParse(value, num) And Double.TryParse(value2, num2) And
                    Double.TryParse(value3, num3) Then
                    percentage = num / 100
                    percentage2 = num2 / 100
                    percentage3 = num3 / 100
                End If

                ' Compute sub-total of each SOA data entries.
                compute_sub_total = (reader.GetDouble("No. of Years 1") * percentage *
                    reader.GetDouble("No. of Units 1") * reader.GetDouble("Fees 1")) +
                    (reader.GetDouble("No. of Years 2") * percentage2 *
                    reader.GetDouble("No. of Units 2") * reader.GetDouble("Fees 2")) +
                    (reader.GetDouble("No. of Years 3") * percentage3 *
                    reader.GetDouble("No. of Units 3") * reader.GetDouble("Fees 3"))

                If reader.GetString("others_custom_value") = "" Then
                    ' If it doesn't have custom value.
                    row = {entry_code, description, category, reader.GetString("No. of Years 1"),
                        reader.GetString("Percent 1"), reader.GetString("No. of Units 1"),
                        FormatNumber(CDbl(reader.GetString("Fees 1")), 2),
                        reader.GetString("No. of Years 2"), reader.GetString("Percent 2"),
                        reader.GetString("No. of Units 2"),
                        FormatNumber(CDbl(reader.GetString("Fees 2")), 2),
                        reader.GetString("No. of Years 3"), reader.GetString("Percent 3"),
                        reader.GetString("No. of Units 3"),
                        FormatNumber(CDbl(reader.GetString("Fees 3")), 2),
                        FormatNumber(CDbl(compute_sub_total), 2),
                        reader.GetString("mop_id")}
                Else
                    ' If it does have custom value.
                    row = {entry_code, description & ": " & custom_value, category,
                        reader.GetString("No. of Years 1"), reader.GetString("Percent 1"),
                        reader.GetString("No. of Units 1"),
                        FormatNumber(CDbl(reader.GetString("Fees 1")), 2),
                        reader.GetString("No. of Years 2"), reader.GetString("Percent 2"),
                        reader.GetString("No. of Units 2"),
                        FormatNumber(CDbl(reader.GetString("Fees 2")), 2),
                        reader.GetString("No. of Years 3"), reader.GetString("Percent 3"),
                        reader.GetString("No. of Units 3"),
                        FormatNumber(CDbl(reader.GetString("Fees 3")), 2),
                        FormatNumber(CDbl(compute_sub_total), 2),
                        reader.GetString("mop_id")}
                End If

                ' Add row to datagridview.
                dgv_soa_data.Rows.Add(row)
            End While
            class_connection.con.Close()

            ' Set all columns unsortable.
            For Each column As DataGridViewColumn In dgv_soa_data.Columns
                column.SortMode = DataGridViewColumnSortMode.NotSortable
            Next

            ' Set total to zero before computations.
            total = 0

            ' Compute all SOA data entries' sub-total to make total.
            For i = 0 To dgv_soa_data.Rows.Count - 1 Step +1
                Try
                    total += dgv_soa_data.Rows(i).Cells("Sub-total").Value
                Catch ex As Exception
                    total += 0
                End Try
            Next

            ' Display total.
            tb_total.Text = FormatNumber(CDbl(total), 2)

            ' Retrieve approver name and user approve status.
            class_connection.con.Open()
            query = "select full_name, user_approve from tbl_soa_transact join tbl_user on " &
                "tbl_user.user_id = tbl_soa_transact.user_id_approve where soa_id = '" &
                soa_id_transact.ToString & "'"
            cmd = New MySqlCommand(query, class_connection.con)
            reader = cmd.ExecuteReader()

            If reader.Read Then
                ' If approver is set.
                tb_approved.Text = WebUtility.HtmlDecode(reader.GetString("full_name"))

                If reader.GetInt32("user_approve") = 1 Then
                    pb_approve_status.Image = My.Resources.Ok_36px
                    btn_add_approve.Enabled = False
                    btn_edit.Enabled = False
                    user_approved = True
                    btn_add_approve.Cursor = Cursors.No
                Else
                    btn_add_approve.Enabled = False
                    btn_add_approve.Cursor = Cursors.No
                    btn_edit.Enabled = False
                    pb_approve_status.Image = My.Resources.More_36px
                End If
            Else
                ' If approver isn't set.
                user_approved = False
                pb_approve_status.Image = My.Resources.Unfriend_Male_36px
                tb_approved.Text = "************  *. ********"
                btn_edit.Enabled = True
            End If
            class_connection.con.Close()

            authorization_check()

            is_receipt_created()

            is_SOA_expired()

            retrieve_number_of_attachments()

        Catch ex As MySqlException
            class_connection.con.Close()
            MsgBox("Couldn't connect to server, data retrieval cancelled. Contact the IT " &
                "Administrator for assistance.", MsgBoxStyle.Exclamation, "NTC Region 10")
            Close()
        Catch ex As Exception
            class_connection.con.Close()
            MsgBox("Couldn't retrieve data properly. Try again later.", MsgBoxStyle.Exclamation,
                "NTC Region 10")
            Close()
        End Try
    End Sub

    Public Sub retrieve_number_of_attachments()
        ' Retrieve number of attachments available.
        Try
            class_connection.con.Open()
            query = "select count(*) as 'number_attach' from tbl_attachment where attachment_id " &
                "= '" & attachmentID & "'"
            cmd = New MySqlCommand(query, class_connection.con)
            reader = cmd.ExecuteReader()

            If reader.Read Then
                If reader.GetInt32("number_attach") = 1 Then
                    btn_attachment.Text = reader.GetInt32("number_attach").ToString + " Attachment"
                ElseIf reader.GetInt32("number_attach") > 1 Then
                    btn_attachment.Text = reader.GetInt32("number_attach").ToString + " Attachments"
                Else
                    btn_attachment.Text = "Attachment"
                End If
            End If
            class_connection.con.Close()
        Catch ex As MySqlException
            class_connection.con.Open()
            MsgBox("Couldn't retrieve number of files attached.", MsgBoxStyle.Exclamation,
                "NTC Region 10")
        Catch ex As Exception
            class_connection.con.Open()
            MsgBox("Something went wrong. Couldn't process the data.", MsgBoxStyle.Exclamation,
                "NTC Region 10")
        End Try
    End Sub

    Sub authorization_check()
        If authority_level = "2" Or authority_level = "3" Then
            ' If it is accountant or cashier.
            btn_endorse.Visible = False
            btn_edit.Visible = False
            btn_add_approve.Visible = False
            pnl_refresh.Visible = True
        ElseIf authority_level = "4" Or authority_level = "5" Then
            ' If it is approver or admin.
            btn_endorse.Visible = False
            btn_edit.Visible = False
            btn_add_approve.Visible = False
            pnl_refresh.Visible = True

            is_OP_created()
        Else
            ' If it is licenser.
            btn_endorse.Visible = True
            btn_edit.Visible = True
            btn_add_approve.Visible = True
            pnl_refresh.Visible = True

            is_OP_created()
        End If
    End Sub

    Sub is_OP_created()
        ' If OP is created, show panel for opening OP or receipt form.
        Try
            class_connection.con.Open()
            query = "select endorse_id as 'ID' from tbl_soa_endorse where soa_id = '" &
                soa_id_transact.ToString & "' and is_updated = '1'"
            cmd = New MySqlCommand(query, class_connection.con)
            reader = cmd.ExecuteReader()

            If reader.Read Then
                endorse_id = reader.GetString("ID")
                pnl_external.Visible = True
                btn_op.Visible = True
            End If
            class_connection.con.Close()
        Catch ex As MySqlException
            class_connection.con.Close()
            MsgBox("Couldn't check whether SOA is endorsed or not.", MsgBoxStyle.Exclamation,
                "NTC Region 10")
        Catch ex As Exception
            class_connection.con.Close()
            MsgBox("Something went wrong. Couldn't process the data.", MsgBoxStyle.Exclamation,
                "NTC Region 10")
        End Try
    End Sub

    Sub is_receipt_created()
        ' If receipt is created, show button for opening form.
        Try
            class_connection.con.Open()
            query = "select cashier_transact_id as 'ID', or_num as 'OR', date_transact as 'date' " &
                "from tbl_soa_cashier where soa_id = '" & soa_id_transact.ToString & "'"
            cmd = New MySqlCommand(query, class_connection.con)
            reader = cmd.ExecuteReader()

            If reader.Read Then
                cashier_transact_id = reader.GetString("ID")
                OR_num = reader.GetString("OR")
                OR_date = reader.GetDateTime("date").ToString("yyyy-MM-dd")

                ' Modify some button properties.
                btn_print_assessment.Enabled = False
                btn_print_endorse.Enabled = False
                btn_print_complete.Enabled = True
                btn_receipt.Visible = True
            End If
            class_connection.con.Close()
        Catch ex As MySqlException
            class_connection.con.Close()
            MsgBox("Couldn't check whether SOA is paid or not.", MsgBoxStyle.Exclamation,
                "NTC Region 10")
        Catch ex As Exception
            class_connection.con.Close()
            MsgBox("Something went wrong. Couldn't process the data.", MsgBoxStyle.Exclamation,
                "NTC Region 10")
        End Try
    End Sub

    Sub is_SOA_expired()
        ' Check if SOA created reached its expiration date.
        Dim datenow As Date

        datenow = Date.ParseExact(Date.Now.ToString("yyyy-MM-dd"), "yyyy-MM-dd",
            CultureInfo.InvariantCulture)

        If expiry_date <= datenow And is_endorse <> "1" Then
            ' Show message, Disable endorse and send to approver button, and show re-assess button.
            tb_message.Visible = True
            tb_message.Text = "SOA expired"
            tb_message.ForeColor = Color.Red
            btn_print_endorse.Enabled = False
            btn_endorse.Enabled = False
            btn_add_approve.Enabled = False
            btn_edit.Enabled = False
            btn_reasses.Visible = True

            ' If authority is either approver or admin, hide re-assess button.
            If authority_level = "4" Or authority_level = "5" Then
                btn_reasses.Visible = False
            End If
        End If
    End Sub

    Sub initialize_table()
        dgv_soa_data.ColumnCount = 17
        dgv_soa_data.Columns(0).Name = "Code"
        dgv_soa_data.Columns(0).ReadOnly = True
        dgv_soa_data.Columns(0).Width = 75
        dgv_soa_data.Columns(1).Name = "Description"
        dgv_soa_data.Columns(1).ReadOnly = True
        dgv_soa_data.Columns(1).Width = 220
        dgv_soa_data.Columns(2).Name = "Category"
        dgv_soa_data.Columns(2).ReadOnly = True
        dgv_soa_data.Columns(3).Width = 80
        dgv_soa_data.Columns(3).Name = "No. of Years"
        dgv_soa_data.Columns(3).DefaultCellStyle.BackColor = Color.FromArgb(107, 117, 122)
        dgv_soa_data.Columns(3).ReadOnly = True
        dgv_soa_data.Columns(3).Width = 80
        dgv_soa_data.Columns(4).Name = "Percent"
        dgv_soa_data.Columns(4).DefaultCellStyle.BackColor = Color.FromArgb(107, 117, 122)
        dgv_soa_data.Columns(4).ReadOnly = True
        dgv_soa_data.Columns(4).Width = 60
        dgv_soa_data.Columns(5).Name = "No. of Units"
        dgv_soa_data.Columns(5).DefaultCellStyle.BackColor = Color.FromArgb(107, 117, 122)
        dgv_soa_data.Columns(5).ReadOnly = True
        dgv_soa_data.Columns(5).Width = 80
        dgv_soa_data.Columns(6).Name = "Fees"
        dgv_soa_data.Columns(6).DefaultCellStyle.BackColor = Color.FromArgb(107, 117, 122)
        dgv_soa_data.Columns(6).ReadOnly = True
        dgv_soa_data.Columns(6).Width = 80
        dgv_soa_data.Columns(7).Name = "No. of Years"
        dgv_soa_data.Columns(7).ReadOnly = True
        dgv_soa_data.Columns(7).Width = 80
        dgv_soa_data.Columns(8).Name = "Percent"
        dgv_soa_data.Columns(8).ReadOnly = True
        dgv_soa_data.Columns(8).Width = 60
        dgv_soa_data.Columns(9).Name = "No. of Units"
        dgv_soa_data.Columns(9).ReadOnly = True
        dgv_soa_data.Columns(9).Width = 80
        dgv_soa_data.Columns(10).Name = "Fees"
        dgv_soa_data.Columns(10).ReadOnly = True
        dgv_soa_data.Columns(10).Width = 80
        dgv_soa_data.Columns(11).Name = "No. of Years"
        dgv_soa_data.Columns(11).DefaultCellStyle.BackColor = Color.FromArgb(107, 117, 122)
        dgv_soa_data.Columns(11).ReadOnly = True
        dgv_soa_data.Columns(11).Width = 80
        dgv_soa_data.Columns(12).Name = "Percent"
        dgv_soa_data.Columns(12).DefaultCellStyle.BackColor = Color.FromArgb(107, 117, 122)
        dgv_soa_data.Columns(12).ReadOnly = True
        dgv_soa_data.Columns(12).Width = 60
        dgv_soa_data.Columns(13).Name = "No. of Units"
        dgv_soa_data.Columns(13).DefaultCellStyle.BackColor = Color.FromArgb(107, 117, 122)
        dgv_soa_data.Columns(13).ReadOnly = True
        dgv_soa_data.Columns(13).Width = 80
        dgv_soa_data.Columns(14).Name = "Fees"
        dgv_soa_data.Columns(14).DefaultCellStyle.BackColor = Color.FromArgb(107, 117, 122)
        dgv_soa_data.Columns(14).ReadOnly = True
        dgv_soa_data.Columns(14).Width = 80
        dgv_soa_data.Columns(15).Name = "Sub-total"
        dgv_soa_data.Columns(15).Width = 80
        dgv_soa_data.Columns(15).ReadOnly = True
        dgv_soa_data.Columns(16).Name = "Payment ID"
        dgv_soa_data.Columns(16).Visible = False
        dgv_soa_data.Columns(16).Width = 0
    End Sub
End Class