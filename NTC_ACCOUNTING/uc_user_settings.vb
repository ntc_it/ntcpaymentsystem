﻿Imports MySql.Data.MySqlClient
Imports System.Net
Imports System.IO

Public Class uc_user_settings
    Dim query, message As String
    Dim cmd As MySqlCommand
    Dim reader As MySqlDataReader
    Dim fullpath, filename As String
    Dim is_picture_exist As Boolean = False
    Dim is_signature_exist As Boolean = True
    Public user_id As String

    Public Sub setUser(ID)
        user_id = ID
    End Sub

    Private Sub btn_change_un_Click(sender As Object, e As EventArgs) Handles btn_change_un.Click
        frm_username_change.user_id = user_id
        frm_username_change.ShowDialog()
    End Sub

    Private Sub btn_change_pass_Click(sender As Object, e As EventArgs) Handles btn_change_pass.Click
        frm_password_change.user_id = user_id
        frm_password_change.ShowDialog()
    End Sub

    Private Sub uc_userSettings_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        retrieve_user_info()
    End Sub

    Sub retrieve_user_info()
        Try
            Dim is_signature_ticked = False

            'Retrieve assigned accounting officer id
            class_connection.con.Open()
            query = "select * from tbl_user where user_id = '" + user_id + "'"
            cmd = New MySqlCommand(query, class_connection.con)
            reader = cmd.ExecuteReader()

            If reader.Read Then
                Dim signature_name = reader.GetString("signature_name")
                tb_name.Text = " " & WebUtility.HtmlEncode(reader.GetString("full_name"))
                tb_username.Text = " " & WebUtility.HtmlDecode(reader.GetString("user_name"))
                tb_password.Text = " Password"
                cb_signature.Checked = False
                pb_signature.Image = My.Resources._51211
                is_signature_exist = False

                Select Case reader.GetString("authority_level")
                    Case "1"
                        tb_authority.Text = " Licenser"
                    Case "2"
                        tb_authority.Text = " Accountant"
                    Case "3"
                        tb_authority.Text = " Cashier"
                    Case "4"
                        tb_authority.Text = " Approver"
                    Case "5"
                        tb_authority.Text = " Administrator"
                    Case Else
                        tb_authority.Text = " Error"
                End Select

                If reader.GetString("signature") <> "" Then
                    If File.Exists(Application.StartupPath + "\" & signature_name) Then
                        is_signature_exist = True
                        pb_signature.Image = Image.FromFile(Application.StartupPath + "\" &
                            signature_name)
                        cb_signature.Enabled = True
                    End If
                End If

                If reader.GetString("enable_signature") = "1" Then
                    is_signature_ticked = True
                End If
            End If
            class_connection.con.Close()

            If Not is_signature_exist Then
                download_signature()

                is_signature_exist = True
            End If

            If is_signature_ticked Then
                cb_signature.Checked = True
            End If
        Catch ex As Exception
            class_connection.con.Close()
        MsgBox("Something went wrong. Couldn't process the data.", MsgBoxStyle.Exclamation,
               "NTC Region 10")
        return_home()
        End Try
    End Sub

    Private Sub btn_browse_Click(sender As Object, e As EventArgs) Handles btn_browse.Click
        Dim result As DialogResult

        ofd_browse.Title = "Please select an image"
        ofd_browse.Filter = "PNG File|*.png;"
        ofd_browse.FileName = ""
        result = ofd_browse.ShowDialog()

        If result = DialogResult.OK Then
            tb_filepath.Text = Path.GetFullPath(ofd_browse.FileName)
            fullpath = ofd_browse.FileName
            btn_upload.Enabled = True
        End If
    End Sub

    Private Sub btn_upload_Click(sender As Object, e As EventArgs) Handles btn_upload.Click
        Try
            Dim filename As String
            Dim request As FtpWebRequest
            Dim stream As FileStream
            Dim reqStream As Stream

            fullpath = ofd_browse.FileName
            filename = Date.Now.ToString("yyyyMMddhhmmss")
            request = WebRequest.Create("ftp://" & class_connection.ftpAddress & "/signatures/" &
                WebUtility.UrlEncode(filename & Path.GetExtension(fullpath)))
            request.Method = WebRequestMethods.Ftp.UploadFile
            request.Credentials = New NetworkCredential(class_connection.ftpUserName, class_connection.ftpPassword)
            request.UsePassive = True
            request.UseBinary = True
            request.KeepAlive = False
            stream = File.OpenRead(fullpath)
            Dim buffer(stream.Length) As Byte
            stream.Read(buffer, 0, buffer.Length)
            stream.Close()
            reqStream = request.GetRequestStream()
            reqStream.Write(buffer, 0, buffer.Length)
            reqStream.Close()

            'insert attachment entry
            class_connection.con.Open()
            query = "update tbl_user set signature = 'http://" & class_connection.ftpAddress &
                "/soa/signatures/" & WebUtility.UrlEncode(filename & Path.GetExtension(fullpath)) &
                "', signature_name = '" & WebUtility.HtmlEncode(filename) &
                Path.GetExtension(fullpath) & "' where user_id = '" & user_id + "'"
            cmd = New MySqlCommand(query, class_connection.con)
            cmd.ExecuteNonQuery()
            class_connection.con.Close()

            download_signature()

            MsgBox("Signature Upload Complete", MsgBoxStyle.Information, "NTC Region 10")
        Catch ex As Exception
            MsgBox("Image could not be upload. Try again later or select a different file.",
                   MsgBoxStyle.Information, "NTC Region 10")
            ofd_browse.FileName = ""
            fullpath = ""
            btn_upload.Enabled = False
            tb_filepath.Text = ""
        End Try
    End Sub

    Private Sub cb_signature_CheckedChanged(sender As Object, e As EventArgs) Handles cb_signature.CheckedChanged
        Try
            If cb_signature.Checked Then
                ' Update transact.
                class_connection.con.Open()
                query = "update tbl_user set enable_signature = '1' where user_id= '" & user_id & "'"
                cmd = New MySqlCommand(query, class_connection.con)
                cmd.ExecuteNonQuery()
                class_connection.con.Close()
            Else
                ' Update transact.
                class_connection.con.Open()
                query = "update tbl_user set enable_signature = '0' where user_id= '" & user_id & "'"
                cmd = New MySqlCommand(query, class_connection.con)
                cmd.ExecuteNonQuery()
                class_connection.con.Close()

            End If
        Catch ex As Exception
            class_connection.con.Close()
        MsgBox("Something went wrong. Couldn't process the data.", MsgBoxStyle.Exclamation,
               "NTC Region 10")
        End Try
    End Sub

    Sub download_signature()
        Try
            Dim urlName As String
            Dim downloadedData(0) As Byte
            Dim req As WebRequest
            Dim response As WebResponse
            Dim stream As Stream
            Dim response_length As Integer
            Dim memStream As MemoryStream
            Dim is_downloaded As Boolean

            class_connection.con.Open()
            query = "select signature as 'path', signature_name as 'name' from tbl_user where " &
                "user_id = '" & user_id & "'"
            cmd = New MySqlCommand(query, class_connection.con)
            reader = cmd.ExecuteReader()

            If reader.Read Then

                Try
                    req = WebRequest.Create(WebUtility.HtmlDecode(reader.GetString("path")))
                    response = req.GetResponse()
                    stream = response.GetResponseStream()
                    Dim buffer(1024) As Byte
                    response_length = response.ContentLength
                    memStream = New MemoryStream()
                    is_downloaded = True

                    While is_downloaded
                        Dim bytesRead As Integer = stream.Read(buffer, 0, buffer.Length)

                        If bytesRead = 0 Then
                            is_downloaded = False
                        Else
                            memStream.Write(buffer, 0, bytesRead)
                        End If
                    End While

                    downloadedData = memStream.ToArray()
                    stream.Close()
                    memStream.Close()

                    If downloadedData IsNot Nothing And downloadedData.Length <> 0 Then
                        urlName = WebUtility.HtmlDecode(reader.GetString("path"))

                        If urlName.EndsWith("/") Then
                            urlName = urlName.Substring(0, urlName.Length - 1)
                        End If

                        urlName = urlName.Substring(urlName.LastIndexOf("/") + 1)
                    End If

                    If downloadedData IsNot Nothing And downloadedData.Length <> 0 Then
                        Dim newFile As FileStream = New FileStream(urlName, FileMode.Create)

                        newFile.Write(downloadedData, 0, downloadedData.Length)
                        newFile.Close()
                        pb_signature.Image = Image.FromFile(Application.StartupPath & "\" &
                                                            urlName)
                        cb_signature.Enabled = True
                        ofd_browse.FileName = ""
                        fullpath = ""
                        btn_upload.Enabled = False
                        tb_filepath.Text = ""
                    End If
                Catch ex As Exception

                End Try
            End If
            class_connection.con.Close()
        Catch ex As Exception
            class_connection.con.Close()
        MsgBox("Something went wrong. Couldn't process the data.", MsgBoxStyle.Exclamation,
               "NTC Region 10")
        End Try
    End Sub

    Sub return_home()
        Dim dashboard_home As New dashboard_home
        dashboard.pnl_dashboard.Controls.Clear()
        dashboard.pnl_dashboard.Controls.Add(dashboard_home)
    End Sub
End Class
