﻿Imports MySql.Data.MySqlClient

Public Class frm_password_change
    Dim query As String
    Dim cmd As MySqlCommand
    Dim reader As MySqlDataReader
    Public user_id, user_pass As String

    Private Sub btn_update_Click(sender As Object, e As EventArgs) Handles btn_update.Click
        Dim user_settings As New uc_user_settings

        ' Retrieve user's password.
        class_connection.con.Open()
        query = "select user_pass from tbl_user where user_id = '" & user_id & "'"
        cmd = New MySqlCommand(query, class_connection.con)
        reader = cmd.ExecuteReader()

        If reader.Read Then
            user_pass = reader.GetString("user_pass")
        End If

        class_connection.con.Close()

        ' Check if inputs are valid.
        If tb_old.Text <> "" And tb_new.Text <> "" And tb_rnew.Text <> "" Then
            ' Verify if old password matches the encryption's pattern.
            Dim isSamePasssword As Boolean = BCrypt.Net.BCrypt.Verify(tb_old.Text, user_pass)

            If isSamePasssword Then
                If tb_new.Text = tb_rnew.Text Then
                    ' Hashed user's password using BCrypt encryption.
                    Dim hashedPasssword As String = BCrypt.Net.BCrypt.HashPassword(tb_new.Text, 10)

                    ' Update user's password.
                    class_connection.con.Open()
                    query = "update tbl_user Set user_pass = '" & hashedPasssword &
                        "' where user_id = '" & user_id & "'"
                    cmd = New MySqlCommand(query, class_connection.con)
                    cmd.ExecuteNonQuery()
                    class_connection.con.Close()

                    MsgBox("Password changed successfully!", MsgBoxStyle.Information, "NTC Region 10")

                    Close()

                    ' Refresh dashboard and load to user settings.
                    dashboard.pnl_dashboard.Controls.Clear()
                    user_settings.setUser(user_id)
                    dashboard.Controls.Add(user_settings)
                Else
                    MsgBox("New password doesn't match!", MsgBoxStyle.Exclamation, "NTC Region 10")
                    tb_new.Text = ""
                    tb_rnew.Text = ""
                    tb_old.Text = ""
                End If
            Else
                MsgBox("Old password doesn't match!", MsgBoxStyle.Exclamation, "NTC Region 10")
                tb_new.Text = ""
                tb_rnew.Text = ""
                tb_old.Text = ""
            End If
        Else
            MsgBox("Fill-up required inputs!", MsgBoxStyle.Exclamation, "NTC Region 10")
        End If
    End Sub
End Class