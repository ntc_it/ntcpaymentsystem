﻿Imports MySql.Data.MySqlClient
Imports System.Net
Imports System.IO

Public Class frm_attachment
    Dim query, attach_id, fullpath As String
    Dim cmd As MySqlCommand
    Dim reader As MySqlDataReader
    Public user_id, attachment_id, authority_level As String

    Private Sub btn_attach_Click(sender As Object, e As EventArgs) Handles btn_attach.Click
        upload_attachment()
    End Sub

    Sub upload_attachment()
        ' Upload attachment to server.
        Try
            Dim result As DialogResult
            Dim filename As String
            Dim request As FtpWebRequest
            Dim stream As FileStream
            Dim reqStream As Stream
            Dim attachment_path As String = "http://" + class_connection.ftpAddress + "/soa/documents/"

            attachmentDialog.Title = "Please select an attachment"
            attachmentDialog.Filter = "Documents, Excels and PDFs|*.doc;*.docx;*.xlsx;*.pdf;"
            attachmentDialog.FileName = ""
            result = attachmentDialog.ShowDialog()

            If result = DialogResult.OK Then
                fullpath = attachmentDialog.FileName
                filename = Date.Now.ToString("yyyyMMddhhmmss")
                request = WebRequest.Create("ftp://" + class_connection.ftpAddress & "/documents/" &
                    WebUtility.UrlEncode(filename + Path.GetExtension(fullpath)))
                request.Method = WebRequestMethods.Ftp.UploadFile
                request.Credentials = New NetworkCredential(class_connection.ftpUserName, class_connection.ftpPassword)
                request.UsePassive = True
                request.UseBinary = True
                request.KeepAlive = False
                stream = File.OpenRead(fullpath)

                Dim buffer(stream.Length) As Byte

                stream.Read(buffer, 0, buffer.Length)
                stream.Close()

                reqStream = request.GetRequestStream()
                reqStream.Write(buffer, 0, buffer.Length)
                reqStream.Close()

                ' Create attachment entry to server.
                class_connection.con.Open()
                query = "insert into tbl_attachment(attachment_path, attachment_name, attachment_id) " &
                    "Values('" & attachment_path & WebUtility.UrlEncode(filename & Path.GetExtension(fullpath)) &
                    "', '" & WebUtility.HtmlEncode(Path.GetFileName(fullpath)) & "','" & attachment_id & "')"
                cmd = New MySqlCommand(query, class_connection.con)
                cmd.ExecuteNonQuery()
                class_connection.con.Close()

                load_attachments()
                MsgBox("Attachment Upload Complete", MsgBoxStyle.Information, "NTC Region 10")
            End If
        Catch ex As Exception
            fullpath = ""
            class_connection.con.Close()
            MsgBox("Couldn't connect to server.", MsgBoxStyle.Exclamation, "NTC Region 10")
        End Try
    End Sub

    Private Sub frm_attachment_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        load_attachments()
    End Sub

    Private Sub dtsmi_download_Click(sender As Object, e As EventArgs) Handles dtsmi_download.Click
        If attach_id <> "" Then
            Try
                Dim local_attachment_path As String = ""
                Dim local_file_name As String = ""

                ' Retrieve filepath and filename of the selected attachment entry.
                class_connection.con.Open()
                query = "select attachment_path as 'path', attachment_name as 'name' from tbl_attachment " &
                    "where entry_id = '" & attach_id & "'"
                cmd = New MySqlCommand(query, class_connection.con)
                reader = cmd.ExecuteReader()

                If reader.Read Then
                    local_attachment_path = reader.GetString("path")
                    local_file_name = reader.GetString("name")
                End If
                class_connection.con.Close()

                If local_attachment_path <> "" Then
                    download_attachment(local_attachment_path, local_file_name)
                End If
            Catch ex As MySqlException
                class_connection.con.Close()
                MsgBox("Couldn't connect to server.", MsgBoxStyle.Exclamation, "NTC Region 10")
            Catch ex As Exception
                class_connection.con.Close()
                MsgBox("Something went wrong. Couldn't process the data.", MsgBoxStyle.Exclamation,
                    "NTC Region 10")
            End Try
        End If
    End Sub

    Sub download_attachment(file_path, file_name)
        Dim downloadedData(0) As Byte

        pnl_download.Visible = True
        lbl_download.Visible = True
        pb_download.Visible = True
        pb_download.Value = 0

        Try
            ' Request for file download.
            Dim req As WebRequest = WebRequest.Create(WebUtility.HtmlDecode(file_path))
            Dim response As WebResponse = req.GetResponse()
            Dim stream As Stream = response.GetResponseStream()
            Dim buffer(1024) As Byte
            Dim response_length As Integer = response.ContentLength

            pb_download.Maximum = response_length

            Dim memStream As MemoryStream = New MemoryStream()
            Dim is_downloaded As Boolean = True

            While is_downloaded
                Dim bytesRead As Integer = stream.Read(buffer, 0, buffer.Length)

                If bytesRead = 0 Then
                    pb_download.Value = pb_download.Maximum
                    is_downloaded = False
                Else
                    memStream.Write(buffer, 0, bytesRead)

                    If (pb_download.Value + bytesRead) <= pb_download.Maximum Then
                        pb_download.Value += bytesRead
                        pb_download.Refresh()
                    End If
                End If
            End While

            downloadedData = memStream.ToArray()

            stream.Close()
            memStream.Close()

            If downloadedData IsNot Nothing And downloadedData.Length <> 0 Then
                ' Rename the downloaded file with retrieved filename.
                Dim urlName As String = file_path

                If urlName.EndsWith("/") Then
                    urlName = urlName.Substring(0, urlName.Length - 1)
                End If

                urlName = urlName.Substring(urlName.LastIndexOf("/") + 1)
                save_attach.FileName = file_name
                save_attach.Filter = "Documents, Excels and PDFs|*.doc;*.docx;*.xlsx;*.pdf;"
            End If

            If downloadedData IsNot Nothing And downloadedData.Length <> 0 Then
                pnl_download.Visible = False
                lbl_download.Visible = False
                pb_download.Visible = False
                pb_download.Value = 0

                If save_attach.ShowDialog() = DialogResult.OK Then
                    ' If user confirmed the download.
                    Dim newFile As FileStream = New FileStream(save_attach.FileName, FileMode.Create)

                    newFile.Write(downloadedData, 0, downloadedData.Length)
                    newFile.Close()
                End If
            End If
        Catch ex As Exception
            pnl_download.Visible = False
            lbl_download.Visible = False
            pb_download.Visible = False
            pb_download.Value = 0

            MsgBox("There was a problem downloading the file.", MsgBoxStyle.Exclamation, "NTC Region 10")
        End Try
    End Sub

    Private Sub frm_attachment_FormClosing(sender As Object, e As FormClosingEventArgs) Handles MyBase.FormClosing
        frm_licenser_log.retrieve_number_of_attachments()
    End Sub

    Private Sub DeleteToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles DeleteToolStripMenuItem.Click
        If attach_id <> "" Then
            If authority_level = "3" Or authority_level = "2" Or authority_level = "4" Then
                MsgBox("You are not authorized to remove the file.", MsgBoxStyle.Exclamation, "NTC Region 10")
            Else
                Try
                    ' Delete selected attachment.
                    class_connection.con.Open()
                    query = "delete from tbl_attachment where entry_id = '" & attach_id & "'"
                    cmd = New MySqlCommand(query, class_connection.con)
                    cmd.ExecuteNonQuery()
                    class_connection.con.Close()

                    load_attachments()
                    attach_id = ""
                Catch ex As MySqlException
                    class_connection.con.Close()
                    MsgBox("Couldn't connect to server. Contact the IT Administrator for assistance.",
                           MsgBoxStyle.Exclamation, "NTC Region 10")
                Catch ex As Exception
                    class_connection.con.Close()
                    MsgBox("Something went wrong. Couldn't process the data.", MsgBoxStyle.Exclamation,
                        "NTC Region 10")
                End Try
            End If
        End If
    End Sub

    Private Sub dgv_attachment_CellClick(sender As Object, e As DataGridViewCellEventArgs) Handles dgv_attachment.CellClick
        retrieveAttachmentID(e)
    End Sub

    Private Sub dgv_attachment_CellEnter(sender As Object, e As DataGridViewCellEventArgs) Handles dgv_attachment.CellEnter
        retrieveAttachmentID(e)
    End Sub

    Sub retrieveAttachmentID(e)
        ' Get attachment ID.
        If e.RowIndex >= 0 Then
            Dim row As DataGridViewRow
            row = dgv_attachment.Rows(e.RowIndex)

            attach_id = row.Cells("ID").Value.ToString()
        End If
    End Sub

    Sub load_attachments()
        Try
            ' Populate attachment datagridview.
            class_connection.con.Open()
            Dim da As MySqlDataAdapter = New MySqlDataAdapter()
            Dim ds As DataSet = New DataSet
            Dim sql As MySqlCommand = New MySqlCommand("select entry_id as 'ID', attachment_name as 'File Name' " &
                "from tbl_attachment where attachment_id = '" & attachment_id & "'", class_connection.con)

            da.SelectCommand = sql
            da.Fill(ds, "rec")
            dgv_attachment.DataSource = ds
            dgv_attachment.DataMember = "rec"

            ' Modify datagridview column settings.
            dgv_attachment.Columns(0).Width = 80
            dgv_attachment.Columns(0).Visible = False
            class_connection.con.Close()
        Catch ex As MySqlException
            class_connection.con.Close()
            MsgBox("Couldn't connect to server.", MsgBoxStyle.Exclamation, "NTC Region 10")
            Close()
        Catch ex As Exception
            class_connection.con.Close()
            MsgBox("Something went wrong. Couldn't process the data.", MsgBoxStyle.Exclamation,
                "NTC Region 10")
            Close()
        End Try
    End Sub
End Class