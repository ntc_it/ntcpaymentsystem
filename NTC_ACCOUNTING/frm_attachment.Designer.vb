﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_attachment
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.RadTitleBar1 = New Telerik.WinControls.UI.RadTitleBar()
        Me.dgv_attachment = New System.Windows.Forms.DataGridView()
        Me.attachment_menu = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.dtsmi_download = New System.Windows.Forms.ToolStripMenuItem()
        Me.DeleteToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.btn_attach = New System.Windows.Forms.Button()
        Me.attachmentDialog = New System.Windows.Forms.OpenFileDialog()
        Me.pb_download = New System.Windows.Forms.ProgressBar()
        Me.save_attach = New System.Windows.Forms.SaveFileDialog()
        Me.lbl_download = New System.Windows.Forms.Label()
        Me.pnl_download = New System.Windows.Forms.Panel()
        Me.Label1 = New System.Windows.Forms.Label()
        CType(Me.RadTitleBar1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgv_attachment, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.attachment_menu.SuspendLayout()
        Me.pnl_download.SuspendLayout()
        Me.SuspendLayout()
        '
        'RadTitleBar1
        '
        Me.RadTitleBar1.Dock = System.Windows.Forms.DockStyle.Top
        Me.RadTitleBar1.Location = New System.Drawing.Point(0, 0)
        Me.RadTitleBar1.Name = "RadTitleBar1"
        Me.RadTitleBar1.Size = New System.Drawing.Size(430, 23)
        Me.RadTitleBar1.TabIndex = 1
        Me.RadTitleBar1.TabStop = False
        Me.RadTitleBar1.Text = "NTC Region 10"
        '
        'dgv_attachment
        '
        Me.dgv_attachment.AllowUserToAddRows = False
        Me.dgv_attachment.AllowUserToDeleteRows = False
        Me.dgv_attachment.AllowUserToResizeColumns = False
        Me.dgv_attachment.AllowUserToResizeRows = False
        Me.dgv_attachment.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.dgv_attachment.BackgroundColor = System.Drawing.Color.FromArgb(CType(CType(28, Byte), Integer), CType(CType(40, Byte), Integer), CType(CType(52, Byte), Integer))
        Me.dgv_attachment.BorderStyle = System.Windows.Forms.BorderStyle.None
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle3.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgv_attachment.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle3
        Me.dgv_attachment.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgv_attachment.ContextMenuStrip = Me.attachment_menu
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle4.BackColor = System.Drawing.Color.FromArgb(CType(CType(34, Byte), Integer), CType(CType(48, Byte), Integer), CType(CType(56, Byte), Integer))
        DataGridViewCellStyle4.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle4.ForeColor = System.Drawing.Color.White
        DataGridViewCellStyle4.SelectionBackColor = System.Drawing.Color.FromArgb(CType(CType(181, Byte), Integer), CType(CType(186, Byte), Integer), CType(CType(188, Byte), Integer))
        DataGridViewCellStyle4.SelectionForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgv_attachment.DefaultCellStyle = DataGridViewCellStyle4
        Me.dgv_attachment.GridColor = System.Drawing.Color.FromArgb(CType(CType(28, Byte), Integer), CType(CType(40, Byte), Integer), CType(CType(52, Byte), Integer))
        Me.dgv_attachment.Location = New System.Drawing.Point(31, 87)
        Me.dgv_attachment.Margin = New System.Windows.Forms.Padding(0)
        Me.dgv_attachment.MultiSelect = False
        Me.dgv_attachment.Name = "dgv_attachment"
        Me.dgv_attachment.ReadOnly = True
        Me.dgv_attachment.RowHeadersVisible = False
        Me.dgv_attachment.RowTemplate.DefaultCellStyle.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dgv_attachment.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgv_attachment.Size = New System.Drawing.Size(371, 309)
        Me.dgv_attachment.TabIndex = 68
        '
        'attachment_menu
        '
        Me.attachment_menu.BackColor = System.Drawing.Color.FromArgb(CType(CType(28, Byte), Integer), CType(CType(40, Byte), Integer), CType(CType(52, Byte), Integer))
        Me.attachment_menu.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.dtsmi_download, Me.DeleteToolStripMenuItem})
        Me.attachment_menu.Name = "attachment_menu"
        Me.attachment_menu.ShowImageMargin = False
        Me.attachment_menu.Size = New System.Drawing.Size(106, 48)
        '
        'dtsmi_download
        '
        Me.dtsmi_download.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtsmi_download.ForeColor = System.Drawing.Color.White
        Me.dtsmi_download.Name = "dtsmi_download"
        Me.dtsmi_download.Size = New System.Drawing.Size(105, 22)
        Me.dtsmi_download.Text = "Download"
        '
        'DeleteToolStripMenuItem
        '
        Me.DeleteToolStripMenuItem.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DeleteToolStripMenuItem.ForeColor = System.Drawing.Color.White
        Me.DeleteToolStripMenuItem.Name = "DeleteToolStripMenuItem"
        Me.DeleteToolStripMenuItem.Size = New System.Drawing.Size(105, 22)
        Me.DeleteToolStripMenuItem.Text = "Delete"
        '
        'btn_attach
        '
        Me.btn_attach.BackColor = System.Drawing.Color.FromArgb(CType(CType(218, Byte), Integer), CType(CType(144, Byte), Integer), CType(CType(27, Byte), Integer))
        Me.btn_attach.FlatAppearance.BorderSize = 0
        Me.btn_attach.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_attach.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_attach.ForeColor = System.Drawing.Color.White
        Me.btn_attach.Location = New System.Drawing.Point(310, 41)
        Me.btn_attach.Name = "btn_attach"
        Me.btn_attach.Size = New System.Drawing.Size(91, 27)
        Me.btn_attach.TabIndex = 105
        Me.btn_attach.Text = "Upload"
        Me.btn_attach.UseVisualStyleBackColor = False
        '
        'attachmentDialog
        '
        Me.attachmentDialog.FileName = "OpenFileDialog1"
        '
        'pb_download
        '
        Me.pb_download.Location = New System.Drawing.Point(14, 27)
        Me.pb_download.Name = "pb_download"
        Me.pb_download.Size = New System.Drawing.Size(194, 23)
        Me.pb_download.TabIndex = 106
        Me.pb_download.Visible = False
        '
        'lbl_download
        '
        Me.lbl_download.AutoSize = True
        Me.lbl_download.Font = New System.Drawing.Font("Segoe UI", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_download.ForeColor = System.Drawing.Color.Silver
        Me.lbl_download.Location = New System.Drawing.Point(68, 7)
        Me.lbl_download.Name = "lbl_download"
        Me.lbl_download.Size = New System.Drawing.Size(86, 13)
        Me.lbl_download.TabIndex = 107
        Me.lbl_download.Text = "Downloading..."
        Me.lbl_download.Visible = False
        '
        'pnl_download
        '
        Me.pnl_download.Controls.Add(Me.pb_download)
        Me.pnl_download.Controls.Add(Me.lbl_download)
        Me.pnl_download.Location = New System.Drawing.Point(106, 216)
        Me.pnl_download.Name = "pnl_download"
        Me.pnl_download.Size = New System.Drawing.Size(218, 62)
        Me.pnl_download.TabIndex = 108
        Me.pnl_download.Visible = False
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Segoe UI", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.Silver
        Me.Label1.Location = New System.Drawing.Point(31, 67)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(106, 13)
        Me.Label1.TabIndex = 109
        Me.Label1.Text = "Attachment file list"
        '
        'frm_attachment
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(44, Byte), Integer), CType(CType(62, Byte), Integer), CType(CType(80, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(430, 420)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.pnl_download)
        Me.Controls.Add(Me.btn_attach)
        Me.Controls.Add(Me.dgv_attachment)
        Me.Controls.Add(Me.RadTitleBar1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.MaximizeBox = False
        Me.MaximumSize = New System.Drawing.Size(430, 420)
        Me.MinimizeBox = False
        Me.MinimumSize = New System.Drawing.Size(430, 420)
        Me.Name = "frm_attachment"
        Me.ShowIcon = False
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "NTC Region 10"
        CType(Me.RadTitleBar1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgv_attachment, System.ComponentModel.ISupportInitialize).EndInit()
        Me.attachment_menu.ResumeLayout(False)
        Me.pnl_download.ResumeLayout(False)
        Me.pnl_download.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents RadTitleBar1 As Telerik.WinControls.UI.RadTitleBar
    Friend WithEvents dgv_attachment As DataGridView
    Friend WithEvents btn_attach As Button
    Friend WithEvents attachmentDialog As OpenFileDialog
    Friend WithEvents attachment_menu As ContextMenuStrip
    Friend WithEvents dtsmi_download As ToolStripMenuItem
    Friend WithEvents pb_download As ProgressBar
    Friend WithEvents save_attach As SaveFileDialog
    Friend WithEvents lbl_download As Label
    Friend WithEvents DeleteToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents pnl_download As Panel
    Friend WithEvents Label1 As Label
End Class
