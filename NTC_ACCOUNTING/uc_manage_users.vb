﻿Imports MySql.Data.MySqlClient
Imports System.Net

Public Class uc_manage_users
    Dim query As String
    Dim cmd As MySqlCommand
    Dim reader As MySqlDataReader
    Dim authority_level As String

    Sub load_approval_personnel()
        Try
            class_connection.con.Open()
            Dim da As MySqlDataAdapter = New MySqlDataAdapter()
            Dim ds As DataSet = New DataSet
            Dim sql As MySqlCommand = New MySqlCommand("select full_name as " &
                "'Name', user_name as 'Username', 
                (case 
                    when authority_level = '1' then 'Licensing'
                    when authority_level = '2' then 'Accounting'
                    when authority_level = '3' then 'Cashier'
                    else 'Approver'
                end) as 'Position'
            from tbl_user", class_connection.con)

            da.SelectCommand = sql
            da.Fill(ds, "rec")
            dgv_user.DataSource = ds
            dgv_user.DataMember = "rec"

            dgv_user.Columns(0).Width = 150
            dgv_user.Columns(1).Width = 90
            dgv_user.Columns(2).Width = 110
            dgv_user.Rows(0).Selected = False
            class_connection.con.Close()
        Catch ex As Exception
            class_connection.con.Close()
            MsgBox("Couldn't connect to server. Couldn't process the data.", MsgBoxStyle.Exclamation,
                   "NTC Region 10")
            returnHome()
        End Try
    End Sub

    Private Sub uc_userReg_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        load_approval_personnel()
        dgv_user.ClearSelection()
    End Sub

    Private Sub EditPasswordToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles _
        EditPasswordToolStripMenuItem.Click
        Try
            For Each row As DataGridViewRow In dgv_user.SelectedRows
                'Retrieve assigned accounting officer id
                class_connection.con.Open()
                query = "select user_id from tbl_user where full_name = '" &
                    row.Cells("Name").Value.ToString() & "'"
                cmd = New MySqlCommand(query, class_connection.con)
                reader = cmd.ExecuteReader()

                If reader.Read Then
                    frm_manage_user_pass_update.user_id_selected =
                        reader.GetString("user_id")
                End If
                class_connection.con.Close()

                frm_manage_user_pass_update.ShowDialog()
            Next
            returnHome()
        Catch ex As Exception
            class_connection.con.Close()
            MsgBox("Something went wrong couldn't process the data.", MsgBoxStyle.Exclamation,
                "NTC Region 10")
        End Try
    End Sub

    Private Sub tb_name_Enter(sender As Object, e As EventArgs)
        dashboard.RadCollapsiblePanel1.IsExpanded = False
    End Sub

    Private Sub tb_username_Enter(sender As Object, e As EventArgs)
        dashboard.RadCollapsiblePanel1.IsExpanded = False
    End Sub

    Private Sub tb_password_Enter(sender As Object, e As EventArgs)
        dashboard.RadCollapsiblePanel1.IsExpanded = False
    End Sub

    Private Sub cmb_authority_DropDown(sender As Object, e As EventArgs) Handles cmb_authority.DropDown
        dashboard.RadCollapsiblePanel1.IsExpanded = False
    End Sub

    Private Sub btn_add_user_Click(sender As Object, e As EventArgs) Handles btn_add_user.Click
        dashboard.RadCollapsiblePanel1.IsExpanded = False
        If tb_name.Text <> "" And tb_password.Text <> "" And tb_username.Text <> "" And
            cmb_authority.Text <> "" Then

            If cmb_authority.Text = "Licensing" Then
                authority_level = "1"
            ElseIf cmb_authority.Text = "Accounting" Then
                authority_level = "2"
            ElseIf cmb_authority.Text = "Cashier" Then
                authority_level = "3"
            Else
                authority_level = "4"
            End If

            Dim hashedPasssword As String = BCrypt.Net.BCrypt.HashPassword(tb_password.Text, 10)

            Try
                class_connection.con.Open()
                query = "insert into tbl_user(user_name, user_pass, full_name, authority_level, signature, " &
                    "signature_name,enable_signature) Values('" & WebUtility.HtmlEncode(tb_username.Text) &
                    "','" & hashedPasssword & "','" & WebUtility.HtmlEncode(tb_name.Text) & "','" &
                    authority_level & "', '', '', '0')"
                cmd = New MySqlCommand(query, class_connection.con)
                cmd.ExecuteNonQuery()
                class_connection.con.Close()

                MsgBox(tb_name.Text + " account has been added successfully!", MsgBoxStyle.Information,
                       "NTC Region 10")

                load_approval_personnel()

                tb_name.Text = ""
                tb_password.Text = ""
                tb_username.Text = ""
                cmb_authority.SelectedIndex = -1
            Catch ex As Exception
                class_connection.con.Close()
                MsgBox("Couldn't connect to server.", MsgBoxStyle.Exclamation, "NTC Region 10")
            End Try
        Else
            MsgBox("Fill out the required account details", MsgBoxStyle.Exclamation, "NTC Region 10")
        End If
    End Sub

    Sub returnHome()
        Dim dashboard_home As New dashboard_home

        dashboard.pnl_dashboard.Controls.Clear()
        dashboard.pnl_dashboard.Controls.Add(dashboard_home)
    End Sub
End Class
