﻿Imports MySql.Data.MySqlClient
Imports System.Net
Imports Microsoft.Office.Interop
Imports System.IO
Public Class frm_accountant_manual
    Dim query, endorse_id, user_id_cashier, endorse_officer_id, signature_endorser,
        enable_endorser_signature, starting_digit_op, serial, last_counter As String
    Dim cmd As MySqlCommand
    Dim reader As MySqlDataReader
    Dim orderOfPaymentID, op_override_number, starting_series_op As Integer
    Dim isOPSerialExist As Integer = 1, maxMOnth As Integer = 0, maxYear As Integer = 0,
        maxCounter As Integer = 0
    Dim export_success As Boolean = False
    Dim op_override_activate_value As String = "0"
    Dim is_first_transact As Boolean = False
    Dim op_override_isActivated = False
    Public transact_ID As Integer
    Public user_id, n_ID, authority_level, longDate, payorID As String

    Private Sub cmb_accountant_SelectionChangeCommitted(sender As Object, e As EventArgs) _
        Handles cmb_accountant.SelectionChangeCommitted
        cmb_accountant_id.SelectedIndex = cmb_accountant.SelectedIndex
    End Sub

    Private Sub btn_back_Click(sender As Object, e As EventArgs) Handles btn_back.Click
        frm_licenser_manual.Show()

        Close()
    End Sub

    Private Sub btn_finish_Click(sender As Object, e As EventArgs) Handles btn_finish.Click
        Try
            If tb_fund_cluster.Text <> "" And (tb_bank_name.Text <> "" And tb_bank_num.Text <> "") And
                (tb_bank_name_two.Text <> "" And tb_bank_num_two.Text <> "") And cmb_accountant.Text <>
                "" Then
                create_temporary_data()
            Else
                MsgBox("Set required inputs to all dark textfields!", MsgBoxStyle.Exclamation,
                    "NTC Region 10")
                export_success = False
            End If
        Catch ex As MySqlException
            MsgBox("Couldn't connect to server, the process is cancelled. Contact the IT Administrator " &
                "for assistance.", MsgBoxStyle.Exclamation, "NTC Region 10")
            export_success = False
        Catch ex As Exception
            MsgBox("Something went wrong. Couldn't process the data.", MsgBoxStyle.Exclamation,
                "NTC Region 10")
            export_success = False
        End Try
    End Sub

    Sub create_temporary_data()
        If rbAuto.Checked Then
            hide_OP_show_receipt()
        Else
            If tb_serial_starting_digit.Text <> "" And tb_serial_year.Text <> "" And
                tb_serial_month.Text <> "" And tb_serial_series.Text <> "" Then
                limitCustomSerial()
                serial_duplicate_checker()

                If Integer.Parse(tb_serial_year.Text) <= maxYear And
                    Integer.Parse(tb_serial_month.Text) <= maxMOnth And
                    Integer.Parse(tb_serial_series.Text) <= maxCounter Then
                    If isOPSerialExist <> 1 Then
                        hide_OP_show_receipt()
                    Else
                        MsgBox("The serial number entered already existed.",
                               MsgBoxStyle.Exclamation, "NTC Region 10")
                    End If
                Else
                    MsgBox("The serial number entered exceeds the next serial " &
                        "generation.", MsgBoxStyle.Exclamation, "NTC Region 10")
                End If
            Else
                MsgBox("You did Not set a custom serial number.",
                    MsgBoxStyle.Exclamation, "NTC Region 10")
            End If
        End If
    End Sub

    Sub hide_OP_show_receipt()
        initialize_hidden_datagridview()

        Dim MAX As Integer = dgv_or.Rows.Count
        Dim row As String()

        For i = 0 To MAX - 1
            row = New String() {dgv_or.Rows(i).Cells(0).FormattedValue,
                dgv_or.Rows(i).Cells(1).FormattedValue, dgv_or.Rows(i).Cells(2).FormattedValue}
            frm_cashier_manual.dgv_cashier.Rows.Add(row)
        Next

        'frm_cashier_manual.tb_total.Text = FormatNumber(CDbl(tb_payment.Text), 2)
        frm_cashier_manual.tb_payor.Text = tb_payor.Text
        frm_cashier_manual.user_id = user_id
        frm_cashier_manual.authority_level = authority_level
        frm_cashier_manual.Show()

        Hide()
    End Sub

    Sub initialize_hidden_datagridview()
        frm_cashier_manual.dgv_cashier.ColumnCount = 3
        frm_cashier_manual.dgv_cashier.Columns(0).Name = "Code"
        frm_cashier_manual.dgv_cashier.Columns(0).ReadOnly = True
        frm_cashier_manual.dgv_cashier.Columns(0).Width = 70
        frm_cashier_manual.dgv_cashier.Columns(1).Name = "Description"
        frm_cashier_manual.dgv_cashier.Columns(1).ReadOnly = True
        frm_cashier_manual.dgv_cashier.Columns(1).Width = 300
        frm_cashier_manual.dgv_cashier.Columns(2).Name = "Sub-Total"
        frm_cashier_manual.dgv_cashier.Columns(2).ReadOnly = True
        frm_cashier_manual.dgv_cashier.Columns(2).Width = 100
    End Sub

    Private Sub tb_serial_starting_digit_KeyPress(sender As Object, e As KeyPressEventArgs) Handles tb_serial_starting_digit.KeyPress
        number_validation_for_serial(e)
    End Sub

    Private Sub tb_serial_year_KeyPress(sender As Object, e As KeyPressEventArgs) Handles tb_serial_year.KeyPress
        number_validation_for_serial(e)
    End Sub

    Private Sub tb_serial_month_KeyPress(sender As Object, e As KeyPressEventArgs) Handles tb_serial_month.KeyPress
        number_validation_for_serial(e)
    End Sub

    Private Sub tb_serial_series_KeyPress(sender As Object, e As KeyPressEventArgs) Handles tb_serial_series.KeyPress
        number_validation_for_serial(e)
    End Sub

    Sub number_validation_for_serial(e)
        Select Case e.KeyChar
            Case "0", "1", "2", "3", "4", "5", "6", "7", "8", "9", vbBack
                e.Handled = False
            Case Else
                e.Handled = True
        End Select
    End Sub

    Private Sub rbCustom_CheckedChanged(sender As Object, e As EventArgs) Handles rbCustom.CheckedChanged
        serial_radio_button_checker()
    End Sub

    Private Sub rbAuto_CheckedChanged(sender As Object, e As EventArgs) Handles rbAuto.CheckedChanged
        serial_radio_button_checker()
    End Sub

    Sub serial_radio_button_checker()
        If rbCustom.Checked Then
            tb_serial_starting_digit.Visible = True
            tb_serial_year.Visible = True
            tb_serial_month.Visible = True
            tb_serial_series.Visible = True
        Else
            tb_serial_starting_digit.Visible = False
            tb_serial_year.Visible = False
            tb_serial_month.Visible = False
            tb_serial_series.Visible = False
        End If
    End Sub

    Private Sub btn_cancel_Click(sender As Object, e As EventArgs) Handles btn_cancel.Click
        frm_licenser_manual.closeManualSOA()

        Close()
    End Sub

    Public Sub save_OP()
        Try
            If rbAuto.Checked Then
                ' If auto is selected.
                create_OP()
            Else
                ' If custom is selected.
                class_connection.con.Open()
                query = "insert into tbl_soa_endorse(soa_id, endorse_officer_id, is_updated, " &
                    "e_counter, op_month, op_year, fund_cluster, op_serial_num, op_date_created, " &
                    "payment_amount, purpose_one, purpose_two, bank_num_one, bank_name_one, " &
                    "payment_amount_one, bank_num_two, bank_name_two, payment_amount_two) " &
                    "values('" & transact_ID.ToString & "','" & cmb_accountant_id.Text & "','1', '" &
                    Integer.Parse(tb_serial_series.Text).ToString & "', '" &
                    Integer.Parse(tb_serial_month.Text).ToString & "', '" &
                    Integer.Parse(tb_serial_year.Text).ToString & "','" &
                    WebUtility.HtmlEncode(tb_fund_cluster.Text) &
                    "','" & tb_serial_starting_digit.Text & "-" & tb_serial_year.Text & "-" &
                    Integer.Parse(tb_serial_month.Text).ToString("D2") & "-" &
                    Integer.Parse(tb_serial_series.Text).ToString("D4") & "','" &
                    dtp_date_created.Value.ToString("yyyy-MM-dd HH:mm:ss") & "','" & tb_payment.Text &
                    "','" & WebUtility.HtmlEncode(tb_purpose_one.Text) & "','" &
                    WebUtility.HtmlEncode(tb_purpose_two.Text) & "','" & tb_bank_num.Text & "','" &
                    WebUtility.HtmlEncode(tb_bank_name.Text) & "','" & tb_bank_payment.Text & "','" &
                    tb_bank_num_two.Text & "','" & WebUtility.HtmlEncode(tb_bank_name_two.Text) & "','" &
                    tb_bank_payment_two.Text & "'); SELECT LAST_INSERT_ID()"
                cmd = New MySqlCommand(query, class_connection.con)
                orderOfPaymentID = CInt(cmd.ExecuteScalar())
                class_connection.con.Close()
            End If

            frm_cashier_manual.transact_ID = transact_ID
            frm_cashier_manual.orderOfPaymentID = orderOfPaymentID

            Close()
        Catch ex As Exception
            class_connection.con.Close()
            MsgBox("Couldn't connect to server. Contact the IT Administrator for assistance.",
                MsgBoxStyle.Exclamation, "NTC Region 10")
        End Try
    End Sub

    Private Sub frm_payment_order_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        load_serial_starting_digit()
        load_soa_details()
        load_accountant()

        Icon = My.Resources.ntc_material_logo
    End Sub

    Sub load_soa_details()
        Try
            'retrieve payor 
            class_connection.con.Open()
            query = "select name, address from tbl_payor where payor_id = '" & payorID & "'"
            cmd = New MySqlCommand(query, class_connection.con)
            reader = cmd.ExecuteReader()

            If reader.Read Then
                tb_payor.Text = WebUtility.HtmlDecode(reader.GetString("name"))
                tb_address.Text = WebUtility.HtmlDecode(reader.GetString("address"))
            End If
            class_connection.con.Close()

            tb_word_figure.Text = class_number_to_word.ConvertNumberToENG(tb_payment.Text)

        Catch ex As MySqlException
            MsgBox("Couldn't connect to server. Contact the IT Administrator for assistance.",
                MsgBoxStyle.Exclamation, "NTC Region 10")
            Close()
        Catch ex As Exception
            class_connection.con.Close()
            MsgBox("Couldn't connect to server. Contact the IT Administrator for assistance.",
                MsgBoxStyle.Exclamation, "NTC Region 10")
        End Try
    End Sub

    Sub load_accountant()
        Try
            cmb_accountant.Items.Clear()
            cmb_accountant_id.Items.Clear()

            class_connection.con.Open()
            query = "Select full_name, user_id from tbl_user where authority_level = '2'"
            cmd = New MySqlCommand(query, class_connection.con)
            reader = cmd.ExecuteReader()

            While reader.Read
                cmb_accountant.Items.Add(reader.GetString("full_name"))
                cmb_accountant_id.Items.Add(reader.GetString("user_id"))
            End While
            class_connection.con.Close()
        Catch ex As MySqlException
            class_connection.con.Close()
            MsgBox("Couldn't connect to server. Contact the IT Administrator for assistance.",
                   MsgBoxStyle.Exclamation, "NTC Region 10")
        Catch ex As Exception
            class_connection.con.Close()
            MsgBox("Something went wrong. Couldn't process the data.", MsgBoxStyle.Exclamation,
                "NTC Region 10")
        End Try
    End Sub

    Public Sub closeManualOP()
        Close()
    End Sub

    Sub serial_duplicate_checker()
        Try
            class_connection.con.Open()
            query = "select count(op_serial_num) as 'isSerialExist' from tbl_soa_endorse where " &
                "op_serial_num = '" & tb_serial_starting_digit.Text & "-" & tb_serial_year.Text &
                "-" & tb_serial_month.Text & "-" & tb_serial_series.Text & "'"
            cmd = New MySqlCommand(query, class_connection.con)
            reader = cmd.ExecuteReader()

            If reader.Read Then
                isOPSerialExist = reader.GetInt32("isSerialExist")
            End If
            class_connection.con.Close()
        Catch ex As MySqlException
            class_connection.con.Close()
            MsgBox("Couldn't connect to server. Contact the IT Administrator for assistance.",
                   MsgBoxStyle.Exclamation, "NTC Region 10")
        Catch ex As Exception
            class_connection.con.Close()
            MsgBox("Something went wrong. Couldn't process the data.", MsgBoxStyle.Exclamation,
                "NTC Region 10")
        End Try
    End Sub

    Sub create_OP()
        Dim current_year = Now.ToString("yyyy")
        Dim current_month = Now.ToString("MM")
        Dim op_last_id As Integer

        ' Retrieve override op serial in system settings.
        class_connection.con.Open()
        query = "select setting_value,activate from tbl_settings where setting = 'override_op_series'"
        cmd = New MySqlCommand(query, class_connection.con)
        reader = cmd.ExecuteReader()

        If reader.Read Then
            op_override_number = reader.GetInt32("setting_value")

            If reader.GetString("activate") = "1" Then
                op_override_activate_value = "1"
            End If
        End If
        class_connection.con.Close()

        ' Retrieve op starting digit.
        class_connection.con.Open()
        query = "select setting_value from tbl_settings where setting = 'starting_series_op'"
        cmd = New MySqlCommand(query, class_connection.con)
        reader = cmd.ExecuteReader()

        If reader.Read Then
            starting_series_op = reader.GetInt32("setting_value")
        End If
        class_connection.con.Close()

        ' Retrieve last counter for endorse.
        class_connection.con.Open()
        query = "select endorse_id, e_counter, op_date_created, op_month, op_year from " &
            "tbl_soa_endorse order by op_year desc, op_month desc, e_counter desc limit 1"
        cmd = New MySqlCommand(query, class_connection.con)
        reader = cmd.ExecuteReader()

        If reader.Read Then
            If current_year = reader.GetDateTime("op_date_created").ToString("yyyy") Then
                If op_override_activate_value = "0" Then
                    serial = starting_digit_op & "-" & current_year & "-" + current_month &
                        "-" & (reader.GetInt32("e_counter") + 1).ToString("D4")
                    is_first_transact = False
                    last_counter = reader.GetString("e_counter")
                Else
                    serial = starting_digit_op & "-" & current_year & "-" & current_month &
                        "-" & op_override_number.ToString("D4")
                    is_first_transact = False
                    last_counter = op_override_number - 1
                    op_override_isActivated = True
                End If
            Else
                If op_override_activate_value = "0" Then
                    serial = starting_digit_op & "-" & current_year & "-" & current_month &
                        "-0001"
                    is_first_transact = False
                    last_counter = 0
                Else
                    serial = starting_digit_op & "-" & current_year & "-" & current_month &
                        "-" & op_override_number.ToString("D4")
                    is_first_transact = False
                    last_counter = op_override_number - 1
                    op_override_isActivated = True
                End If
            End If
        Else
            ' If this is the first transaction
            serial = starting_digit_op & "-" & current_year & "-" & current_month & "-" &
                starting_series_op.ToString("D4")
            is_first_transact = True
        End If
        class_connection.con.Close()

        ' Check if this is first transaction then insert. This is only a query.
        If is_first_transact Then
            query = "insert into tbl_soa_endorse(soa_id, endorse_officer_id, is_updated, " &
                "e_counter, op_month, op_year, fund_cluster, op_serial_num, op_date_created, " &
                "payment_amount, purpose_one, purpose_two, bank_num_one, bank_name_one, " &
                "payment_amount_one, bank_num_two, bank_name_two, payment_amount_two) values('" &
                transact_ID.ToString & "', '" & cmb_accountant_id.Text & "','1', '" &
                starting_series_op.ToString & "',  '" & current_month & "', '" & current_year &
                "','" & WebUtility.HtmlEncode(tb_fund_cluster.Text) & "','" & serial & "','" &
                dtp_date_created.Value.ToString("yyyy-MM-dd HH:mm:ss") & "','" & tb_payment.Text &
                "','" & WebUtility.HtmlEncode(tb_purpose_one.Text) & "','" &
                WebUtility.HtmlEncode(tb_purpose_two.Text) & "','" & tb_bank_num.Text & "','" &
                WebUtility.HtmlEncode(tb_bank_name.Text) & "','" & tb_bank_payment.Text & "','" &
                tb_bank_num_two.Text & "','" & WebUtility.HtmlEncode(tb_bank_name_two.Text) & "','" &
                tb_bank_payment_two.Text & "')"
        Else
            op_last_id = Integer.Parse(last_counter) + 1

            query = "insert into tbl_soa_endorse(soa_id, endorse_officer_id, is_updated, e_counter, " &
                "op_month, op_year, fund_cluster, op_serial_num, op_date_created, payment_amount, " &
                "purpose_one, purpose_two, bank_num_one, bank_name_one, payment_amount_one, " &
                "bank_num_two, bank_name_two, payment_amount_two) values('" & transact_ID.ToString &
                "','" & cmb_accountant_id.Text & "','1', '" & op_last_id.ToString & "',  '" &
                current_month & "', '" & current_year & "','" &
                WebUtility.HtmlEncode(tb_fund_cluster.Text) & "','" & serial & "','" &
                dtp_date_created.Value.ToString("yyyy-MM-dd HH:mm:ss") & "','" & tb_payment.Text &
                "','" & WebUtility.HtmlEncode(tb_purpose_one.Text) & "','" &
                WebUtility.HtmlEncode(tb_purpose_two.Text) & "','" & tb_bank_num.Text & "','" &
                WebUtility.HtmlEncode(tb_bank_name.Text) & "','" & tb_bank_payment.Text & "','" &
                tb_bank_num_two.Text & "','" & WebUtility.HtmlEncode(tb_bank_name_two.Text) & "','" &
                tb_bank_payment_two.Text & "')"
        End If

        ' Command to execute the previous query.
        class_connection.con.Open()
        cmd = New MySqlCommand(query, class_connection.con)
        cmd.ExecuteNonQuery()
        class_connection.con.Close()

        If op_override_isActivated Then
            isOPOverrideActivated()
        End If
    End Sub

    Sub isOPOverrideActivated()
        ' Update override soa.
        class_connection.con.Open()
        query = "update tbl_settings set activate = '0' where setting = 'override_op_series'"
        cmd = New MySqlCommand(query, class_connection.con)
        cmd.ExecuteNonQuery()
        class_connection.con.Close()
        op_override_isActivated = False
        op_override_activate_value = "0"
    End Sub

    Sub limitCustomSerial()
        class_connection.con.Open()
        query = "select e_counter, op_date_created, op_month, op_year from tbl_soa_endorse order by op_year desc, op_month desc, e_counter desc limit 1"
        cmd = New MySqlCommand(query, class_connection.con)
        reader = cmd.ExecuteReader()

        If reader.Read Then
            maxMOnth = reader.GetInt32("op_month")
            maxYear = reader.GetInt32("op_year")
            maxCounter = reader.GetInt32("e_counter") + 1
        End If

        class_connection.con.Close()
    End Sub

    Sub load_serial_starting_digit()
        ' Retrieve op starting digit.
        class_connection.con.Open()
        query = "Select setting_value from tbl_settings where setting = 'starting_digit_op'"
        cmd = New MySqlCommand(query, class_connection.con)
        reader = cmd.ExecuteReader()

        If reader.Read Then
            starting_digit_op = reader.GetString("setting_value")
            tb_serial_starting_digit.Text = reader.GetString("setting_value")
        End If
        class_connection.con.Close()
    End Sub
End Class