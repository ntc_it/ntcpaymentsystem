﻿Imports MySql.Data.MySqlClient
Imports System.Net

Public Class frm_login
    Dim query As String
    Dim cmd As MySqlCommand
    Dim reader As MySqlDataReader
    Dim isSamePasssword As Boolean = False
    Public authority_level As String
    Public user_id_login As Integer

    Private Sub btn_login_Click(sender As Object, e As EventArgs) Handles btn_login.Click
        processLoginCredential()
    End Sub

    Sub processLoginCredential()
        Try
            Dim username = WebUtility.HtmlEncode(tb_username.Text)

            btn_login.Enabled = False

            class_connection.con.Open()
            query = "select full_name,user_id,authority_level,user_pass from tbl_user where " &
                "user_name = '" & username & "'"
            cmd = New MySqlCommand(query, class_connection.con)
            reader = cmd.ExecuteReader()

            If reader.Read Then
                isSamePasssword = BCrypt.Net.BCrypt.Verify(tb_password.Text.ToString,
                    reader.GetString("user_pass"))

                If isSamePasssword Then
                    authority_level = reader.GetString("authority_level")
                    user_id_login = reader.GetInt32("user_id")
                    isSamePasssword = False
                Else
                    tb_password.Text = ""
                    authority_level = ""
                    user_id_login = 0
                    MsgBox("You entered a wrong password! Try Again.", MsgBoxStyle.Exclamation,
                           "NTC Region 10")
                    btn_login.Enabled = True
                End If
            Else
                btn_login.Enabled = True
                MsgBox("You entered a non-existent account!", MsgBoxStyle.Exclamation,
                       "NTC Region 10")
                tb_password.Text = ""
                tb_username.Text = ""
            End If
            class_connection.con.Close()

            open_dashboard()
        Catch ex As MySqlException
            class_connection.con.Close()
            MsgBox("Couldn't connect to server. contact the it administrator for assistance.",
                   MsgBoxStyle.Exclamation, "NTC Region 10")
            btn_login.Enabled = True
        Catch ex As TimeoutException
            class_connection.con.Close()
            MsgBox("The program couldn't process your input, try again!", MsgBoxStyle.Exclamation,
                   "NTC Region 10")
            btn_login.Enabled = True
        Catch ex As Exception
            class_connection.con.Close()
            MsgBox("Something went wrong. Could not process the data.", MsgBoxStyle.Exclamation,
                   "NTC Region 10")
            btn_login.Enabled = True
        End Try
    End Sub

    Private Sub open_dashboard()
        If authority_level = "1" Then
            ' Licenser module.
            Hide()
            dashboard.authority_level = authority_level
            dashboard.user_id_login = user_id_login
            dashboard.Show()
            dashboard.btn_register_user.Visible = False
            dashboard.btn_manual.Visible = False
        ElseIf authority_level = "2" Then
            ' Accountant module.
            Hide()
            dashboard.user_id_login = user_id_login
            dashboard.authority_level = authority_level
            dashboard.Show()
            dashboard.btn_newsoa.Location = New Point(14, 201)
            dashboard.btn_newsoa.Visible = False
            dashboard.btn_view_logs.Text = "Order of Payment Logs"
            dashboard.btn_my_notifications.Location = New Point(14, 39)
            dashboard.btn_view_logs.Location = New Point(14, 93)
            dashboard.btn_register_user.Visible = False
            dashboard.btn_manual.Visible = False
            dashboard.btn_reports.Visible = True
        ElseIf authority_level = "3" Then
            ' Cashier module.
            Hide()
            dashboard.user_id_login = user_id_login
            dashboard.authority_level = authority_level
            dashboard.Show()
            dashboard.btn_newsoa.Location = New Point(14, 201)
            dashboard.btn_newsoa.Visible = False
            dashboard.btn_my_notifications.Location = New Point(14, 39)
            dashboard.btn_view_logs.Location = New Point(14, 93)
            dashboard.btn_view_logs.Text = "Receipt Logs"
            dashboard.btn_register_user.Visible = False
            dashboard.btn_manual.Visible = False
            dashboard.btn_reports.Visible = True
        ElseIf authority_level = "4" Then
            ' Approver module.
            Hide()
            dashboard.user_id_login = user_id_login
            dashboard.authority_level = authority_level
            dashboard.Show()
            dashboard.btn_newsoa.Location = New Point(14, 201)
            dashboard.btn_newsoa.Visible = False
            dashboard.btn_my_notifications.Location = New Point(14, 39)
            dashboard.btn_view_logs.Location = New Point(14, 93)
            dashboard.btn_register_user.Visible = False
            dashboard.btn_manual.Visible = False
        ElseIf authority_level = "5" Then
            ' Administrator module.
            Hide()
            dashboard.user_id_login = user_id_login
            dashboard.authority_level = authority_level
            dashboard.Show()
            dashboard.btn_newsoa.Location = New Point(14, 201)
            dashboard.btn_newsoa.Visible = False
            dashboard.btn_my_notifications.Location = New Point(14, 39)
            dashboard.btn_view_logs.Location = New Point(14, 39)
            dashboard.btn_register_user.Visible = True
            dashboard.btn_settings.Visible = True
            dashboard.btn_manual.Visible = True
            dashboard.btn_reports.Visible = True
        End If
    End Sub

    Private Sub tb_password_KeyDown(sender As Object, e As KeyEventArgs) Handles tb_password.KeyDown
        If e.KeyCode = Keys.Enter Then
            processLoginCredential()
        End If
    End Sub

    Private Sub tb_username_KeyDown(sender As Object, e As KeyEventArgs) Handles tb_username.KeyDown
        If e.KeyCode = Keys.Enter Then
            processLoginCredential()
        End If
    End Sub

    Private Sub btn_login_KeyDown(sender As Object, e As KeyEventArgs) Handles btn_login.KeyDown
        If e.KeyCode = Keys.Enter Then
            processLoginCredential()
        End If
    End Sub

    Private Sub frmLogin_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Icon = My.Resources.ntc_material_logo
    End Sub
End Class
