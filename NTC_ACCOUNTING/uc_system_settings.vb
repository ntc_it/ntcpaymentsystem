﻿Imports MySql.Data.MySqlClient
Imports System.Net
Public Class uc_system_settings
    Dim query As String
    Dim cmd As MySqlCommand
    Dim reader As MySqlDataReader
    Dim accountant_id As String = "0", cashier_id As String = "0", enforce_chief_id As String = "0",
        save_accountant_id As String = "0", save_cashier_id As String = "0", save_enforce_chief_id _
        As String = "0", starting_digit_soa, starting_digit_op As String
    Public user_id, authority_level As String

    Private Sub btn_save_Click(sender As Object, e As EventArgs) Handles btn_save.Click
        Try
            ' Retrieve accountant id.
            class_connection.con.Open()
            query = "select user_id from tbl_user where full_name = '" & cmb_accounting_officer.Text & "'"
            cmd = New MySqlCommand(query, class_connection.con)
            reader = cmd.ExecuteReader()

            If reader.Read Then
                save_accountant_id = reader.GetString("user_id")
            End If
            class_connection.con.Close()

            ' Retrieve cashier id.
            class_connection.con.Open()
            query = "select user_id from tbl_user where full_name = '" & cmb_cashiering_officer.Text & "'"
            cmd = New MySqlCommand(query, class_connection.con)
            reader = cmd.ExecuteReader()

            If reader.Read Then
                save_cashier_id = reader.GetString("user_id")
            End If
            class_connection.con.Close()

            ' Update selected accountant.
            class_connection.con.Open()
            query = "update tbl_settings set setting_value = '" & save_accountant_id & "' where setting = " &
                "'accounting_officer'"
            cmd = New MySqlCommand(query, class_connection.con)
            cmd.ExecuteNonQuery()
            class_connection.con.Close()

            ' Update selected cashier.
            class_connection.con.Open()
            query = "update tbl_settings set setting_value = '" & save_cashier_id & "' where setting = " &
                "'cashiering_officer'"
            cmd = New MySqlCommand(query, class_connection.con)
            cmd.ExecuteNonQuery()
            class_connection.con.Close()

            ' Update starting digit soa.
            class_connection.con.Open()
            query = "update tbl_settings set setting_value = '" & tb_soa_serial.Text & "' where setting = " &
                "'starting_digit_soa'"
            cmd = New MySqlCommand(query, class_connection.con)
            cmd.ExecuteNonQuery()
            class_connection.con.Close()

            ' Update starting digit op.
            class_connection.con.Open()
            query = "update tbl_settings set setting_value = '" & tb_op_serial.Text & "' where setting = " &
                "'starting_digit_op'"
            cmd = New MySqlCommand(query, class_connection.con)
            cmd.ExecuteNonQuery()
            class_connection.con.Close()

            ' Update starting series soa.
            class_connection.con.Open()
            query = "update tbl_settings set setting_value = '" & tb_soa_series.Text & "' where setting = " &
                "'starting_series_soa'"
            cmd = New MySqlCommand(query, class_connection.con)
            cmd.ExecuteNonQuery()
            class_connection.con.Close()

            ' Update starting series op.
            class_connection.con.Open()
            query = "update tbl_settings set setting_value = '" & tb_op_series.Text & "' where setting = " &
                "'starting_series_op'"
            cmd = New MySqlCommand(query, class_connection.con)
            cmd.ExecuteNonQuery()
            class_connection.con.Close()

            ' Update override soa.
            class_connection.con.Open()
            If cb_soa_override.Checked Then
                query = "update tbl_settings set setting_value = '" & tb_soa_override.Text &
                    "', activate = '1' where setting = 'override_soa_series'"
            Else
                query = "update tbl_settings set setting_value = '" & tb_soa_override.Text &
                    "', activate = '0' where setting = 'override_soa_series'"
            End If
            cmd = New MySqlCommand(query, class_connection.con)
            cmd.ExecuteNonQuery()
            class_connection.con.Close()

            ' Update override op.
            class_connection.con.Open()
            If cb_op_override.Checked Then
                query = "update tbl_settings set setting_value = '" & tb_op_override.Text &
                    "', activate = '1' where setting = 'override_op_series'"
            Else
                query = "update tbl_settings set setting_value = '" & tb_op_override.Text &
                    "', activate = '0' where setting = 'override_op_series'"
            End If
            cmd = New MySqlCommand(query, class_connection.con)
            cmd.ExecuteNonQuery()
            class_connection.con.Close()

            MsgBox("Settings Updated!", MsgBoxStyle.Information, "NTC Region 10")
        Catch ex As Exception
            class_connection.con.Close()
            MsgBox("Something went wrong. Couldn't process the data.", MsgBoxStyle.Exclamation,
                   "NTC Region 10")
        End Try
    End Sub

    Private Sub frmSettings_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Try
            ' Retrieve accounting officers.
            class_connection.con.Open()
            query = "select full_name from tbl_user where authority_level = '2'"
            cmd = New MySqlCommand(query, class_connection.con)
            reader = cmd.ExecuteReader()

            While reader.Read
                cmb_accounting_officer.Items.Add(reader.GetString("full_name"))
            End While
            class_connection.con.Close()

            ' Retrieve cashiering officers.
            class_connection.con.Open()
            query = "select full_name from tbl_user where authority_level = '3'"
            cmd = New MySqlCommand(query, class_connection.con)
            reader = cmd.ExecuteReader()

            While reader.Read
                cmb_cashiering_officer.Items.Add(reader.GetString("full_name"))
            End While
            class_connection.con.Close()

            ' Retrieve save settings.
            class_connection.con.Open()
            query = "select * from tbl_settings"
            cmd = New MySqlCommand(query, class_connection.con)
            reader = cmd.ExecuteReader()

            While reader.Read
                If reader.GetString("setting") = "accounting_officer" Then
                    accountant_id = reader.GetString("setting_value")
                End If

                If reader.GetString("setting") = "cashiering_officer" Then
                    cashier_id = reader.GetString("setting_value")
                End If
            End While
            class_connection.con.Close()

            If accountant_id <> "0" Then
                class_connection.con.Open()
                query = "select full_name from tbl_user where user_id = '" & accountant_id & "'"
                cmd = New MySqlCommand(query, class_connection.con)
                reader = cmd.ExecuteReader()

                If reader.Read Then
                    cmb_accounting_officer.SelectedItem = reader.GetString("full_name")
                End If
                class_connection.con.Close()
            End If

            If cashier_id <> "0" Then
                class_connection.con.Open()
                query = "select full_name from tbl_user where user_id = '" & cashier_id & "'"
                cmd = New MySqlCommand(query, class_connection.con)
                reader = cmd.ExecuteReader()

                If reader.Read Then
                    cmb_cashiering_officer.SelectedItem = reader.GetString("full_name")
                End If
                class_connection.con.Close()
            End If

            ' Retrieve soa starting digit.
            class_connection.con.Open()
            query = "select setting_value from tbl_settings where setting = 'starting_digit_soa'"
            cmd = New MySqlCommand(query, class_connection.con)
            reader = cmd.ExecuteReader()

            If reader.Read Then
                tb_soa_serial.Text = reader.GetString("setting_value")
            End If
            class_connection.con.Close()

            ' Retrieve op starting digit.
            class_connection.con.Open()
            query = "select setting_value from tbl_settings where setting = 'starting_digit_op'"
            cmd = New MySqlCommand(query, class_connection.con)
            reader = cmd.ExecuteReader()

            If reader.Read Then
                tb_op_serial.Text = reader.GetString("setting_value")
            End If
            class_connection.con.Close()

            ' Retrieve soa starting series.
            class_connection.con.Open()
            query = "select setting_value from tbl_settings where setting = 'starting_series_soa'"
            cmd = New MySqlCommand(query, class_connection.con)
            reader = cmd.ExecuteReader()

            If reader.Read Then
                tb_soa_series.Text = reader.GetString("setting_value")
            End If
            class_connection.con.Close()

            ' Retrieve op starting series.
            class_connection.con.Open()
            query = "select setting_value from tbl_settings where setting = 'starting_series_op'"
            cmd = New MySqlCommand(query, class_connection.con)
            reader = cmd.ExecuteReader()

            If reader.Read Then
                tb_op_series.Text = reader.GetString("setting_value")
            End If
            class_connection.con.Close()

            ' Retrieve override soa serial.
            class_connection.con.Open()
            query = "select setting_value,activate from tbl_settings where setting = 'override_soa_series'"
            cmd = New MySqlCommand(query, class_connection.con)
            reader = cmd.ExecuteReader()

            If reader.Read Then
                tb_soa_override.Text = reader.GetString("setting_value")

                If reader.GetString("activate") = "1" Then
                    cb_soa_override.Checked = True
                End If
            End If
            class_connection.con.Close()

            ' Retrieve override op serial.
            class_connection.con.Open()
            query = "select setting_value, activate from tbl_settings where setting = 'override_op_series'"
            cmd = New MySqlCommand(query, class_connection.con)
            reader = cmd.ExecuteReader()

            If reader.Read Then
                tb_op_override.Text = reader.GetString("setting_value")

                If reader.GetString("activate") = "1" Then
                    cb_op_override.Checked = True
                End If
            End If
            class_connection.con.Close()
        Catch ex As Exception
            class_connection.con.Close()
            MsgBox("Something went wrong. Couldn't process the data.", MsgBoxStyle.Exclamation,
                   "NTC Region 10")
            returnHome()
        End Try
    End Sub

    Sub returnHome()
        Dim dashboard_home As New dashboard_home

        dashboard.pnl_dashboard.Controls.Clear()
        dashboard.pnl_dashboard.Controls.Add(dashboard_home)
    End Sub
End Class
