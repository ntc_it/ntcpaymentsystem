﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frm_payor_list
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frm_payor_list))
        Me.dgv_payor = New System.Windows.Forms.DataGridView()
        Me.cms_dgv_payor = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.tsmi_cms_update_payor = New System.Windows.Forms.ToolStripMenuItem()
        Me.tb_search = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.RadTitleBar1 = New Telerik.WinControls.UI.RadTitleBar()
        Me.tb_message = New System.Windows.Forms.TextBox()
        Me.btn_refresh = New Bunifu.Framework.UI.BunifuImageButton()
        Me.txtPayor = New System.Windows.Forms.TextBox()
        Me.txtAddress = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.btnAddPayor = New System.Windows.Forms.Button()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.TextBox1 = New System.Windows.Forms.TextBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.btn_search = New System.Windows.Forms.Button()
        Me.btn_check = New System.Windows.Forms.Button()
        Me.pb_approve_status = New System.Windows.Forms.PictureBox()
        Me.btn_refresh_two = New System.Windows.Forms.Button()
        CType(Me.dgv_payor, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.cms_dgv_payor.SuspendLayout()
        CType(Me.RadTitleBar1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.btn_refresh, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        CType(Me.pb_approve_status, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'dgv_payor
        '
        Me.dgv_payor.AllowUserToAddRows = False
        Me.dgv_payor.AllowUserToDeleteRows = False
        Me.dgv_payor.AllowUserToResizeColumns = False
        Me.dgv_payor.AllowUserToResizeRows = False
        Me.dgv_payor.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.dgv_payor.BackgroundColor = System.Drawing.Color.FromArgb(CType(CType(28, Byte), Integer), CType(CType(40, Byte), Integer), CType(CType(52, Byte), Integer))
        Me.dgv_payor.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.dgv_payor.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.[Single]
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgv_payor.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.dgv_payor.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgv_payor.ContextMenuStrip = Me.cms_dgv_payor
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(CType(CType(28, Byte), Integer), CType(CType(40, Byte), Integer), CType(CType(52, Byte), Integer))
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.Color.White
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.FromArgb(CType(CType(181, Byte), Integer), CType(CType(186, Byte), Integer), CType(CType(188, Byte), Integer))
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgv_payor.DefaultCellStyle = DataGridViewCellStyle2
        Me.dgv_payor.GridColor = System.Drawing.Color.FromArgb(CType(CType(28, Byte), Integer), CType(CType(40, Byte), Integer), CType(CType(52, Byte), Integer))
        Me.dgv_payor.Location = New System.Drawing.Point(583, 87)
        Me.dgv_payor.Name = "dgv_payor"
        Me.dgv_payor.RowHeadersVisible = False
        Me.dgv_payor.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgv_payor.Size = New System.Drawing.Size(575, 420)
        Me.dgv_payor.TabIndex = 14
        '
        'cms_dgv_payor
        '
        Me.cms_dgv_payor.BackColor = System.Drawing.Color.FromArgb(CType(CType(28, Byte), Integer), CType(CType(40, Byte), Integer), CType(CType(52, Byte), Integer))
        Me.cms_dgv_payor.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsmi_cms_update_payor})
        Me.cms_dgv_payor.Name = "dgv_cms"
        Me.cms_dgv_payor.ShowImageMargin = False
        Me.cms_dgv_payor.Size = New System.Drawing.Size(105, 26)
        '
        'tsmi_cms_update_payor
        '
        Me.tsmi_cms_update_payor.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me.tsmi_cms_update_payor.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tsmi_cms_update_payor.ForeColor = System.Drawing.Color.White
        Me.tsmi_cms_update_payor.Name = "tsmi_cms_update_payor"
        Me.tsmi_cms_update_payor.Size = New System.Drawing.Size(104, 22)
        Me.tsmi_cms_update_payor.Text = "Edit payor"
        '
        'tb_search
        '
        Me.tb_search.BackColor = System.Drawing.Color.FromArgb(CType(CType(28, Byte), Integer), CType(CType(40, Byte), Integer), CType(CType(52, Byte), Integer))
        Me.tb_search.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.tb_search.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tb_search.ForeColor = System.Drawing.Color.White
        Me.tb_search.Location = New System.Drawing.Point(641, 51)
        Me.tb_search.MaxLength = 20
        Me.tb_search.Name = "tb_search"
        Me.tb_search.Size = New System.Drawing.Size(331, 22)
        Me.tb_search.TabIndex = 17
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Segoe UI", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.DarkGray
        Me.Label1.Location = New System.Drawing.Point(585, 55)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(42, 13)
        Me.Label1.TabIndex = 19
        Me.Label1.Text = "Search"
        '
        'RadTitleBar1
        '
        Me.RadTitleBar1.Dock = System.Windows.Forms.DockStyle.Top
        Me.RadTitleBar1.Location = New System.Drawing.Point(0, 0)
        Me.RadTitleBar1.Name = "RadTitleBar1"
        Me.RadTitleBar1.Size = New System.Drawing.Size(1198, 23)
        Me.RadTitleBar1.TabIndex = 20
        Me.RadTitleBar1.TabStop = False
        Me.RadTitleBar1.Text = "NTC Region 10"
        '
        'tb_message
        '
        Me.tb_message.BackColor = System.Drawing.Color.FromArgb(CType(CType(44, Byte), Integer), CType(CType(62, Byte), Integer), CType(CType(80, Byte), Integer))
        Me.tb_message.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.tb_message.Font = New System.Drawing.Font("Segoe UI", 14.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tb_message.ForeColor = System.Drawing.Color.DarkGray
        Me.tb_message.Location = New System.Drawing.Point(712, 286)
        Me.tb_message.MaxLength = 25
        Me.tb_message.Name = "tb_message"
        Me.tb_message.Size = New System.Drawing.Size(263, 26)
        Me.tb_message.TabIndex = 21
        Me.tb_message.Text = "Couldn't connect to server."
        Me.tb_message.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.tb_message.Visible = False
        '
        'btn_refresh
        '
        Me.btn_refresh.BackColor = System.Drawing.Color.Transparent
        Me.btn_refresh.Image = Global.NTC_ACCOUNTING.My.Resources.Resources.refresh
        Me.btn_refresh.ImageActive = Nothing
        Me.btn_refresh.Location = New System.Drawing.Point(983, 279)
        Me.btn_refresh.Name = "btn_refresh"
        Me.btn_refresh.Size = New System.Drawing.Size(47, 39)
        Me.btn_refresh.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.btn_refresh.TabIndex = 23
        Me.btn_refresh.TabStop = False
        Me.btn_refresh.Visible = False
        Me.btn_refresh.Zoom = 10
        '
        'txtPayor
        '
        Me.txtPayor.BackColor = System.Drawing.Color.FromArgb(CType(CType(28, Byte), Integer), CType(CType(40, Byte), Integer), CType(CType(52, Byte), Integer))
        Me.txtPayor.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtPayor.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPayor.ForeColor = System.Drawing.Color.White
        Me.txtPayor.Location = New System.Drawing.Point(25, 347)
        Me.txtPayor.MaxLength = 80
        Me.txtPayor.Name = "txtPayor"
        Me.txtPayor.Size = New System.Drawing.Size(405, 22)
        Me.txtPayor.TabIndex = 24
        '
        'txtAddress
        '
        Me.txtAddress.BackColor = System.Drawing.Color.FromArgb(CType(CType(28, Byte), Integer), CType(CType(40, Byte), Integer), CType(CType(52, Byte), Integer))
        Me.txtAddress.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtAddress.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtAddress.ForeColor = System.Drawing.Color.White
        Me.txtAddress.Location = New System.Drawing.Point(25, 409)
        Me.txtAddress.MaxLength = 80
        Me.txtAddress.Name = "txtAddress"
        Me.txtAddress.Size = New System.Drawing.Size(478, 22)
        Me.txtAddress.TabIndex = 25
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Segoe UI", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.DarkGray
        Me.Label2.Location = New System.Drawing.Point(26, 330)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(61, 13)
        Me.Label2.TabIndex = 26
        Me.Label2.Text = "Full Name"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Segoe UI", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ForeColor = System.Drawing.Color.DarkGray
        Me.Label3.Location = New System.Drawing.Point(26, 393)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(117, 13)
        Me.Label3.TabIndex = 27
        Me.Label3.Text = "Home/Office Address"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Segoe UI", 15.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ForeColor = System.Drawing.Color.DarkGray
        Me.Label4.Location = New System.Drawing.Point(24, 43)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(194, 30)
        Me.Label4.TabIndex = 28
        Me.Label4.Text = "Payor Information"
        '
        'btnAddPayor
        '
        Me.btnAddPayor.BackColor = System.Drawing.Color.FromArgb(CType(CType(218, Byte), Integer), CType(CType(144, Byte), Integer), CType(CType(27, Byte), Integer))
        Me.btnAddPayor.FlatAppearance.BorderSize = 0
        Me.btnAddPayor.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnAddPayor.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAddPayor.ForeColor = System.Drawing.Color.White
        Me.btnAddPayor.Location = New System.Drawing.Point(225, 466)
        Me.btnAddPayor.Name = "btnAddPayor"
        Me.btnAddPayor.Size = New System.Drawing.Size(110, 35)
        Me.btnAddPayor.TabIndex = 97
        Me.btnAddPayor.Text = "Create"
        Me.btnAddPayor.UseVisualStyleBackColor = False
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.FromArgb(CType(CType(114, Byte), Integer), CType(CType(126, Byte), Integer), CType(CType(138, Byte), Integer))
        Me.Panel1.Controls.Add(Me.TextBox1)
        Me.Panel1.Controls.Add(Me.Label8)
        Me.Panel1.Location = New System.Drawing.Point(25, 87)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(524, 223)
        Me.Panel1.TabIndex = 99
        '
        'TextBox1
        '
        Me.TextBox1.BackColor = System.Drawing.Color.FromArgb(CType(CType(114, Byte), Integer), CType(CType(126, Byte), Integer), CType(CType(138, Byte), Integer))
        Me.TextBox1.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.TextBox1.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox1.ForeColor = System.Drawing.Color.White
        Me.TextBox1.Location = New System.Drawing.Point(16, 34)
        Me.TextBox1.Multiline = True
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.ReadOnly = True
        Me.TextBox1.Size = New System.Drawing.Size(489, 173)
        Me.TextBox1.TabIndex = 100
        Me.TextBox1.TabStop = False
        Me.TextBox1.Text = resources.GetString("TextBox1.Text")
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Segoe UI", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.ForeColor = System.Drawing.Color.White
        Me.Label8.Location = New System.Drawing.Point(5, 5)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(44, 20)
        Me.Label8.TabIndex = 3
        Me.Label8.Text = "Note"
        '
        'btn_search
        '
        Me.btn_search.BackColor = System.Drawing.Color.FromArgb(CType(CType(218, Byte), Integer), CType(CType(144, Byte), Integer), CType(CType(27, Byte), Integer))
        Me.btn_search.FlatAppearance.BorderSize = 0
        Me.btn_search.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_search.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_search.ForeColor = System.Drawing.Color.White
        Me.btn_search.Location = New System.Drawing.Point(972, 51)
        Me.btn_search.Name = "btn_search"
        Me.btn_search.Size = New System.Drawing.Size(74, 22)
        Me.btn_search.TabIndex = 100
        Me.btn_search.Text = "Search"
        Me.btn_search.UseVisualStyleBackColor = False
        '
        'btn_check
        '
        Me.btn_check.BackColor = System.Drawing.Color.FromArgb(CType(CType(218, Byte), Integer), CType(CType(144, Byte), Integer), CType(CType(27, Byte), Integer))
        Me.btn_check.FlatAppearance.BorderSize = 0
        Me.btn_check.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_check.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_check.ForeColor = System.Drawing.Color.White
        Me.btn_check.Location = New System.Drawing.Point(429, 347)
        Me.btn_check.Name = "btn_check"
        Me.btn_check.Size = New System.Drawing.Size(74, 22)
        Me.btn_check.TabIndex = 101
        Me.btn_check.Text = "Validate"
        Me.btn_check.UseVisualStyleBackColor = False
        '
        'pb_approve_status
        '
        Me.pb_approve_status.Image = Global.NTC_ACCOUNTING.My.Resources.Resources.Ok_36px
        Me.pb_approve_status.Location = New System.Drawing.Point(511, 340)
        Me.pb_approve_status.Name = "pb_approve_status"
        Me.pb_approve_status.Size = New System.Drawing.Size(36, 36)
        Me.pb_approve_status.TabIndex = 105
        Me.pb_approve_status.TabStop = False
        Me.pb_approve_status.Visible = False
        '
        'btn_refresh_two
        '
        Me.btn_refresh_two.BackColor = System.Drawing.Color.FromArgb(CType(CType(218, Byte), Integer), CType(CType(144, Byte), Integer), CType(CType(27, Byte), Integer))
        Me.btn_refresh_two.FlatAppearance.BorderSize = 0
        Me.btn_refresh_two.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_refresh_two.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_refresh_two.ForeColor = System.Drawing.Color.White
        Me.btn_refresh_two.Location = New System.Drawing.Point(1084, 51)
        Me.btn_refresh_two.Name = "btn_refresh_two"
        Me.btn_refresh_two.Size = New System.Drawing.Size(74, 22)
        Me.btn_refresh_two.TabIndex = 106
        Me.btn_refresh_two.Text = "Refresh"
        Me.btn_refresh_two.UseVisualStyleBackColor = False
        '
        'frm_payor_list
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(44, Byte), Integer), CType(CType(62, Byte), Integer), CType(CType(80, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(1198, 535)
        Me.Controls.Add(Me.btn_refresh_two)
        Me.Controls.Add(Me.pb_approve_status)
        Me.Controls.Add(Me.btn_check)
        Me.Controls.Add(Me.btn_search)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.btnAddPayor)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.txtAddress)
        Me.Controls.Add(Me.btn_refresh)
        Me.Controls.Add(Me.tb_search)
        Me.Controls.Add(Me.tb_message)
        Me.Controls.Add(Me.RadTitleBar1)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.dgv_payor)
        Me.Controls.Add(Me.txtPayor)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.MaximizeBox = False
        Me.MaximumSize = New System.Drawing.Size(1198, 535)
        Me.MinimizeBox = False
        Me.MinimumSize = New System.Drawing.Size(1198, 535)
        Me.Name = "frm_payor_list"
        Me.ShowIcon = False
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "NTC Region 10"
        CType(Me.dgv_payor, System.ComponentModel.ISupportInitialize).EndInit()
        Me.cms_dgv_payor.ResumeLayout(False)
        CType(Me.RadTitleBar1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.btn_refresh, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        CType(Me.pb_approve_status, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents dgv_payor As DataGridView
    Friend WithEvents tb_search As TextBox
    Friend WithEvents Label1 As Label
    Friend WithEvents RadTitleBar1 As Telerik.WinControls.UI.RadTitleBar
    Friend WithEvents tb_message As TextBox
    Friend WithEvents btn_refresh As Bunifu.Framework.UI.BunifuImageButton
    Friend WithEvents txtPayor As TextBox
    Friend WithEvents txtAddress As TextBox
    Friend WithEvents Label2 As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents Label4 As Label
    Friend WithEvents btnAddPayor As Button
    Friend WithEvents cms_dgv_payor As ContextMenuStrip
    Friend WithEvents tsmi_cms_update_payor As ToolStripMenuItem
    Friend WithEvents Panel1 As Panel
    Friend WithEvents Label8 As Label
    Friend WithEvents TextBox1 As TextBox
    Friend WithEvents btn_search As Button
    Friend WithEvents btn_check As Button
    Friend WithEvents pb_approve_status As PictureBox
    Friend WithEvents btn_refresh_two As Button
End Class
