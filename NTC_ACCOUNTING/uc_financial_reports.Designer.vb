﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class uc_financial_reports
    Inherits System.Windows.Forms.UserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle9 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle10 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle11 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle12 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.dgv_Collection = New System.Windows.Forms.DataGridView()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.TotalOR = New System.Windows.Forms.TextBox()
        Me.TotalTax = New System.Windows.Forms.TextBox()
        Me.TotalFees = New System.Windows.Forms.TextBox()
        Me.BtnLoad = New System.Windows.Forms.Button()
        Me.DTP2 = New System.Windows.Forms.DateTimePicker()
        Me.DTP1 = New System.Windows.Forms.DateTimePicker()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.btn_printCollection = New System.Windows.Forms.Button()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.BtnLoad2 = New System.Windows.Forms.Button()
        Me.DTP22 = New System.Windows.Forms.DateTimePicker()
        Me.DTP11 = New System.Windows.Forms.DateTimePicker()
        Me.btn_printRecords = New System.Windows.Forms.Button()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.tb_total_cash = New System.Windows.Forms.TextBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.dgv_reciepts = New System.Windows.Forms.DataGridView()
        Me.pnl_collection_and_deposits = New System.Windows.Forms.Panel()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.pnl_cash_receipts = New System.Windows.Forms.Panel()
        Me.p1SubpanelMenu = New System.Windows.Forms.Panel()
        Me.btn_collections = New System.Windows.Forms.Button()
        Me.btn_cash = New System.Windows.Forms.Button()
        CType(Me.dgv_Collection, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgv_reciepts, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnl_collection_and_deposits.SuspendLayout()
        Me.pnl_cash_receipts.SuspendLayout()
        Me.p1SubpanelMenu.SuspendLayout()
        Me.SuspendLayout()
        '
        'dgv_Collection
        '
        Me.dgv_Collection.AllowUserToAddRows = False
        Me.dgv_Collection.AllowUserToDeleteRows = False
        Me.dgv_Collection.AllowUserToResizeColumns = False
        Me.dgv_Collection.AllowUserToResizeRows = False
        Me.dgv_Collection.BackgroundColor = System.Drawing.Color.FromArgb(CType(CType(28, Byte), Integer), CType(CType(40, Byte), Integer), CType(CType(52, Byte), Integer))
        Me.dgv_Collection.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.dgv_Collection.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.[Single]
        DataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle9.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle9.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle9.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle9.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle9.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle9.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgv_Collection.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle9
        Me.dgv_Collection.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        DataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle10.BackColor = System.Drawing.Color.FromArgb(CType(CType(28, Byte), Integer), CType(CType(40, Byte), Integer), CType(CType(52, Byte), Integer))
        DataGridViewCellStyle10.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle10.ForeColor = System.Drawing.Color.White
        DataGridViewCellStyle10.SelectionBackColor = System.Drawing.Color.FromArgb(CType(CType(181, Byte), Integer), CType(CType(186, Byte), Integer), CType(CType(188, Byte), Integer))
        DataGridViewCellStyle10.SelectionForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle10.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgv_Collection.DefaultCellStyle = DataGridViewCellStyle10
        Me.dgv_Collection.GridColor = System.Drawing.Color.White
        Me.dgv_Collection.Location = New System.Drawing.Point(33, 54)
        Me.dgv_Collection.MultiSelect = False
        Me.dgv_Collection.Name = "dgv_Collection"
        Me.dgv_Collection.ReadOnly = True
        Me.dgv_Collection.RowHeadersVisible = False
        Me.dgv_Collection.RowTemplate.DefaultCellStyle.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dgv_Collection.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgv_Collection.Size = New System.Drawing.Size(1274, 500)
        Me.dgv_Collection.TabIndex = 30
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Segoe UI", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ForeColor = System.Drawing.Color.White
        Me.Label4.Location = New System.Drawing.Point(836, 573)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(33, 13)
        Me.Label4.TabIndex = 31
        Me.Label4.Text = "Total"
        '
        'TotalOR
        '
        Me.TotalOR.BackColor = System.Drawing.Color.FromArgb(CType(CType(28, Byte), Integer), CType(CType(40, Byte), Integer), CType(CType(52, Byte), Integer))
        Me.TotalOR.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.TotalOR.Font = New System.Drawing.Font("Segoe UI", 14.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TotalOR.ForeColor = System.Drawing.Color.White
        Me.TotalOR.Location = New System.Drawing.Point(834, 589)
        Me.TotalOR.Name = "TotalOR"
        Me.TotalOR.ReadOnly = True
        Me.TotalOR.Size = New System.Drawing.Size(145, 26)
        Me.TotalOR.TabIndex = 32
        Me.TotalOR.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'TotalTax
        '
        Me.TotalTax.BackColor = System.Drawing.Color.FromArgb(CType(CType(28, Byte), Integer), CType(CType(40, Byte), Integer), CType(CType(52, Byte), Integer))
        Me.TotalTax.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.TotalTax.Font = New System.Drawing.Font("Segoe UI", 14.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TotalTax.ForeColor = System.Drawing.Color.White
        Me.TotalTax.Location = New System.Drawing.Point(998, 589)
        Me.TotalTax.Name = "TotalTax"
        Me.TotalTax.ReadOnly = True
        Me.TotalTax.Size = New System.Drawing.Size(145, 26)
        Me.TotalTax.TabIndex = 35
        Me.TotalTax.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'TotalFees
        '
        Me.TotalFees.BackColor = System.Drawing.Color.FromArgb(CType(CType(28, Byte), Integer), CType(CType(40, Byte), Integer), CType(CType(52, Byte), Integer))
        Me.TotalFees.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.TotalFees.Font = New System.Drawing.Font("Segoe UI", 14.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TotalFees.ForeColor = System.Drawing.Color.White
        Me.TotalFees.Location = New System.Drawing.Point(1162, 589)
        Me.TotalFees.Name = "TotalFees"
        Me.TotalFees.ReadOnly = True
        Me.TotalFees.Size = New System.Drawing.Size(145, 26)
        Me.TotalFees.TabIndex = 36
        Me.TotalFees.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'BtnLoad
        '
        Me.BtnLoad.BackColor = System.Drawing.Color.FromArgb(CType(CType(218, Byte), Integer), CType(CType(144, Byte), Integer), CType(CType(27, Byte), Integer))
        Me.BtnLoad.FlatAppearance.BorderSize = 0
        Me.BtnLoad.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.BtnLoad.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BtnLoad.ForeColor = System.Drawing.Color.White
        Me.BtnLoad.Location = New System.Drawing.Point(1173, 12)
        Me.BtnLoad.Name = "BtnLoad"
        Me.BtnLoad.Size = New System.Drawing.Size(134, 35)
        Me.BtnLoad.TabIndex = 112
        Me.BtnLoad.Text = "Retrieve"
        Me.BtnLoad.UseVisualStyleBackColor = False
        '
        'DTP2
        '
        Me.DTP2.Location = New System.Drawing.Point(934, 19)
        Me.DTP2.Name = "DTP2"
        Me.DTP2.Size = New System.Drawing.Size(200, 20)
        Me.DTP2.TabIndex = 111
        '
        'DTP1
        '
        Me.DTP1.Location = New System.Drawing.Point(685, 19)
        Me.DTP1.Name = "DTP1"
        Me.DTP1.Size = New System.Drawing.Size(200, 20)
        Me.DTP1.TabIndex = 110
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Font = New System.Drawing.Font("Segoe UI", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label13.ForeColor = System.Drawing.Color.White
        Me.Label13.Location = New System.Drawing.Point(908, 23)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(19, 13)
        Me.Label13.TabIndex = 109
        Me.Label13.Text = "To"
        '
        'btn_printCollection
        '
        Me.btn_printCollection.BackColor = System.Drawing.Color.FromArgb(CType(CType(218, Byte), Integer), CType(CType(144, Byte), Integer), CType(CType(27, Byte), Integer))
        Me.btn_printCollection.FlatAppearance.BorderSize = 0
        Me.btn_printCollection.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_printCollection.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_printCollection.ForeColor = System.Drawing.Color.White
        Me.btn_printCollection.Location = New System.Drawing.Point(33, 583)
        Me.btn_printCollection.Name = "btn_printCollection"
        Me.btn_printCollection.Size = New System.Drawing.Size(134, 35)
        Me.btn_printCollection.TabIndex = 108
        Me.btn_printCollection.Text = "Print"
        Me.btn_printCollection.UseVisualStyleBackColor = False
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Segoe UI", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.White
        Me.Label2.Location = New System.Drawing.Point(643, 23)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(33, 13)
        Me.Label2.TabIndex = 12
        Me.Label2.Text = "From"
        '
        'BtnLoad2
        '
        Me.BtnLoad2.BackColor = System.Drawing.Color.FromArgb(CType(CType(218, Byte), Integer), CType(CType(144, Byte), Integer), CType(CType(27, Byte), Integer))
        Me.BtnLoad2.FlatAppearance.BorderSize = 0
        Me.BtnLoad2.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.BtnLoad2.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BtnLoad2.ForeColor = System.Drawing.Color.White
        Me.BtnLoad2.Location = New System.Drawing.Point(1173, 12)
        Me.BtnLoad2.Name = "BtnLoad2"
        Me.BtnLoad2.Size = New System.Drawing.Size(134, 35)
        Me.BtnLoad2.TabIndex = 112
        Me.BtnLoad2.Text = "Retrieve"
        Me.BtnLoad2.UseVisualStyleBackColor = False
        '
        'DTP22
        '
        Me.DTP22.Location = New System.Drawing.Point(934, 19)
        Me.DTP22.Name = "DTP22"
        Me.DTP22.Size = New System.Drawing.Size(200, 20)
        Me.DTP22.TabIndex = 111
        '
        'DTP11
        '
        Me.DTP11.Location = New System.Drawing.Point(685, 19)
        Me.DTP11.Name = "DTP11"
        Me.DTP11.Size = New System.Drawing.Size(200, 20)
        Me.DTP11.TabIndex = 110
        '
        'btn_printRecords
        '
        Me.btn_printRecords.BackColor = System.Drawing.Color.FromArgb(CType(CType(218, Byte), Integer), CType(CType(144, Byte), Integer), CType(CType(27, Byte), Integer))
        Me.btn_printRecords.FlatAppearance.BorderSize = 0
        Me.btn_printRecords.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_printRecords.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_printRecords.ForeColor = System.Drawing.Color.White
        Me.btn_printRecords.Location = New System.Drawing.Point(33, 571)
        Me.btn_printRecords.Name = "btn_printRecords"
        Me.btn_printRecords.Size = New System.Drawing.Size(134, 35)
        Me.btn_printRecords.TabIndex = 109
        Me.btn_printRecords.Text = "Print"
        Me.btn_printRecords.UseVisualStyleBackColor = False
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Font = New System.Drawing.Font("Segoe UI", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.ForeColor = System.Drawing.Color.White
        Me.Label11.Location = New System.Drawing.Point(908, 23)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(19, 13)
        Me.Label11.TabIndex = 48
        Me.Label11.Text = "To"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Font = New System.Drawing.Font("Segoe UI", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.ForeColor = System.Drawing.Color.White
        Me.Label10.Location = New System.Drawing.Point(1110, 567)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(33, 13)
        Me.Label10.TabIndex = 45
        Me.Label10.Text = "Total"
        '
        'tb_total_cash
        '
        Me.tb_total_cash.BackColor = System.Drawing.Color.FromArgb(CType(CType(28, Byte), Integer), CType(CType(40, Byte), Integer), CType(CType(52, Byte), Integer))
        Me.tb_total_cash.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.tb_total_cash.Font = New System.Drawing.Font("Segoe UI", 12.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tb_total_cash.ForeColor = System.Drawing.Color.White
        Me.tb_total_cash.Location = New System.Drawing.Point(1110, 583)
        Me.tb_total_cash.Name = "tb_total_cash"
        Me.tb_total_cash.ReadOnly = True
        Me.tb_total_cash.Size = New System.Drawing.Size(197, 22)
        Me.tb_total_cash.TabIndex = 44
        Me.tb_total_cash.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Segoe UI", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.ForeColor = System.Drawing.Color.White
        Me.Label8.Location = New System.Drawing.Point(643, 23)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(33, 13)
        Me.Label8.TabIndex = 41
        Me.Label8.Text = "From"
        '
        'dgv_reciepts
        '
        Me.dgv_reciepts.AllowUserToAddRows = False
        Me.dgv_reciepts.AllowUserToDeleteRows = False
        Me.dgv_reciepts.AllowUserToResizeColumns = False
        Me.dgv_reciepts.AllowUserToResizeRows = False
        Me.dgv_reciepts.BackgroundColor = System.Drawing.Color.FromArgb(CType(CType(28, Byte), Integer), CType(CType(40, Byte), Integer), CType(CType(52, Byte), Integer))
        Me.dgv_reciepts.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.dgv_reciepts.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.[Single]
        DataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle11.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle11.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle11.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle11.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle11.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle11.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgv_reciepts.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle11
        Me.dgv_reciepts.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        DataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle12.BackColor = System.Drawing.Color.FromArgb(CType(CType(28, Byte), Integer), CType(CType(40, Byte), Integer), CType(CType(52, Byte), Integer))
        DataGridViewCellStyle12.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle12.ForeColor = System.Drawing.Color.White
        DataGridViewCellStyle12.SelectionBackColor = System.Drawing.Color.FromArgb(CType(CType(181, Byte), Integer), CType(CType(186, Byte), Integer), CType(CType(188, Byte), Integer))
        DataGridViewCellStyle12.SelectionForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle12.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgv_reciepts.DefaultCellStyle = DataGridViewCellStyle12
        Me.dgv_reciepts.GridColor = System.Drawing.Color.Silver
        Me.dgv_reciepts.Location = New System.Drawing.Point(33, 54)
        Me.dgv_reciepts.MultiSelect = False
        Me.dgv_reciepts.Name = "dgv_reciepts"
        Me.dgv_reciepts.ReadOnly = True
        Me.dgv_reciepts.RowHeadersVisible = False
        Me.dgv_reciepts.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.dgv_reciepts.RowTemplate.DefaultCellStyle.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dgv_reciepts.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgv_reciepts.Size = New System.Drawing.Size(1274, 500)
        Me.dgv_reciepts.TabIndex = 39
        '
        'pnl_collection_and_deposits
        '
        Me.pnl_collection_and_deposits.BackColor = System.Drawing.Color.FromArgb(CType(CType(113, Byte), Integer), CType(CType(125, Byte), Integer), CType(CType(137, Byte), Integer))
        Me.pnl_collection_and_deposits.Controls.Add(Me.Label3)
        Me.pnl_collection_and_deposits.Controls.Add(Me.Label1)
        Me.pnl_collection_and_deposits.Controls.Add(Me.BtnLoad)
        Me.pnl_collection_and_deposits.Controls.Add(Me.Label4)
        Me.pnl_collection_and_deposits.Controls.Add(Me.DTP2)
        Me.pnl_collection_and_deposits.Controls.Add(Me.TotalOR)
        Me.pnl_collection_and_deposits.Controls.Add(Me.DTP1)
        Me.pnl_collection_and_deposits.Controls.Add(Me.Label2)
        Me.pnl_collection_and_deposits.Controls.Add(Me.Label13)
        Me.pnl_collection_and_deposits.Controls.Add(Me.TotalTax)
        Me.pnl_collection_and_deposits.Controls.Add(Me.btn_printCollection)
        Me.pnl_collection_and_deposits.Controls.Add(Me.TotalFees)
        Me.pnl_collection_and_deposits.Controls.Add(Me.dgv_Collection)
        Me.pnl_collection_and_deposits.Location = New System.Drawing.Point(1, 52)
        Me.pnl_collection_and_deposits.Name = "pnl_collection_and_deposits"
        Me.pnl_collection_and_deposits.Size = New System.Drawing.Size(1337, 644)
        Me.pnl_collection_and_deposits.TabIndex = 113
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Segoe UI", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ForeColor = System.Drawing.Color.White
        Me.Label3.Location = New System.Drawing.Point(1164, 573)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(30, 13)
        Me.Label3.TabIndex = 114
        Me.Label3.Text = "Fees"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Segoe UI", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.White
        Me.Label1.Location = New System.Drawing.Point(1000, 573)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(37, 13)
        Me.Label1.TabIndex = 113
        Me.Label1.Text = "Taxes"
        '
        'pnl_cash_receipts
        '
        Me.pnl_cash_receipts.BackColor = System.Drawing.Color.FromArgb(CType(CType(113, Byte), Integer), CType(CType(125, Byte), Integer), CType(CType(137, Byte), Integer))
        Me.pnl_cash_receipts.Controls.Add(Me.BtnLoad2)
        Me.pnl_cash_receipts.Controls.Add(Me.dgv_reciepts)
        Me.pnl_cash_receipts.Controls.Add(Me.DTP22)
        Me.pnl_cash_receipts.Controls.Add(Me.Label8)
        Me.pnl_cash_receipts.Controls.Add(Me.DTP11)
        Me.pnl_cash_receipts.Controls.Add(Me.tb_total_cash)
        Me.pnl_cash_receipts.Controls.Add(Me.btn_printRecords)
        Me.pnl_cash_receipts.Controls.Add(Me.Label10)
        Me.pnl_cash_receipts.Controls.Add(Me.Label11)
        Me.pnl_cash_receipts.Location = New System.Drawing.Point(1, 52)
        Me.pnl_cash_receipts.Name = "pnl_cash_receipts"
        Me.pnl_cash_receipts.Size = New System.Drawing.Size(1337, 644)
        Me.pnl_cash_receipts.TabIndex = 113
        '
        'p1SubpanelMenu
        '
        Me.p1SubpanelMenu.BackColor = System.Drawing.Color.FromArgb(CType(CType(34, Byte), Integer), CType(CType(48, Byte), Integer), CType(CType(56, Byte), Integer))
        Me.p1SubpanelMenu.Controls.Add(Me.btn_collections)
        Me.p1SubpanelMenu.Controls.Add(Me.btn_cash)
        Me.p1SubpanelMenu.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.p1SubpanelMenu.Location = New System.Drawing.Point(1, 1)
        Me.p1SubpanelMenu.Margin = New System.Windows.Forms.Padding(0)
        Me.p1SubpanelMenu.Name = "p1SubpanelMenu"
        Me.p1SubpanelMenu.Size = New System.Drawing.Size(270, 51)
        Me.p1SubpanelMenu.TabIndex = 114
        '
        'btn_collections
        '
        Me.btn_collections.BackColor = System.Drawing.Color.FromArgb(CType(CType(113, Byte), Integer), CType(CType(125, Byte), Integer), CType(CType(137, Byte), Integer))
        Me.btn_collections.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(193, Byte), Integer), CType(CType(7, Byte), Integer))
        Me.btn_collections.FlatAppearance.BorderSize = 0
        Me.btn_collections.FlatAppearance.CheckedBackColor = System.Drawing.Color.Transparent
        Me.btn_collections.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_collections.Font = New System.Drawing.Font("Segoe UI", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_collections.ForeColor = System.Drawing.Color.White
        Me.btn_collections.Location = New System.Drawing.Point(0, 0)
        Me.btn_collections.Margin = New System.Windows.Forms.Padding(0)
        Me.btn_collections.Name = "btn_collections"
        Me.btn_collections.Size = New System.Drawing.Size(130, 51)
        Me.btn_collections.TabIndex = 0
        Me.btn_collections.Text = "Collections-Deposits Report"
        Me.btn_collections.UseVisualStyleBackColor = False
        '
        'btn_cash
        '
        Me.btn_cash.BackColor = System.Drawing.Color.FromArgb(CType(CType(69, Byte), Integer), CType(CType(90, Byte), Integer), CType(CType(100, Byte), Integer))
        Me.btn_cash.FlatAppearance.BorderSize = 0
        Me.btn_cash.FlatAppearance.CheckedBackColor = System.Drawing.Color.Transparent
        Me.btn_cash.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_cash.Font = New System.Drawing.Font("Segoe UI", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_cash.ForeColor = System.Drawing.Color.Silver
        Me.btn_cash.Location = New System.Drawing.Point(130, 0)
        Me.btn_cash.Margin = New System.Windows.Forms.Padding(0)
        Me.btn_cash.Name = "btn_cash"
        Me.btn_cash.Size = New System.Drawing.Size(130, 51)
        Me.btn_cash.TabIndex = 1
        Me.btn_cash.Text = "Cash Receipts - Records"
        Me.btn_cash.UseVisualStyleBackColor = False
        '
        'uc_financial_reports
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(44, Byte), Integer), CType(CType(62, Byte), Integer), CType(CType(80, Byte), Integer))
        Me.Controls.Add(Me.p1SubpanelMenu)
        Me.Controls.Add(Me.pnl_collection_and_deposits)
        Me.Controls.Add(Me.pnl_cash_receipts)
        Me.ForeColor = System.Drawing.Color.White
        Me.Name = "uc_financial_reports"
        Me.Size = New System.Drawing.Size(1339, 695)
        CType(Me.dgv_Collection, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgv_reciepts, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnl_collection_and_deposits.ResumeLayout(False)
        Me.pnl_collection_and_deposits.PerformLayout()
        Me.pnl_cash_receipts.ResumeLayout(False)
        Me.pnl_cash_receipts.PerformLayout()
        Me.p1SubpanelMenu.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents dgv_Collection As DataGridView
    Friend WithEvents Label4 As Label
    Friend WithEvents TotalOR As TextBox
    Friend WithEvents TotalTax As TextBox
    Friend WithEvents TotalFees As TextBox
    Friend WithEvents dgv_reciepts As DataGridView
    Friend WithEvents Label8 As Label
    Friend WithEvents tb_total_cash As TextBox
    Friend WithEvents Label10 As Label
    Friend WithEvents Label11 As Label
    Friend WithEvents btn_printCollection As Button
    Friend WithEvents btn_printRecords As Button
    Friend WithEvents Label13 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents DTP2 As DateTimePicker
    Friend WithEvents DTP1 As DateTimePicker
    Friend WithEvents BtnLoad As Button
    Friend WithEvents BtnLoad2 As Button
    Friend WithEvents DTP22 As DateTimePicker
    Friend WithEvents DTP11 As DateTimePicker
    Friend WithEvents pnl_collection_and_deposits As Panel
    Friend WithEvents pnl_cash_receipts As Panel
    Friend WithEvents p1SubpanelMenu As Panel
    Friend WithEvents btn_collections As Button
    Friend WithEvents btn_cash As Button
    Friend WithEvents Label3 As Label
    Friend WithEvents Label1 As Label
End Class
