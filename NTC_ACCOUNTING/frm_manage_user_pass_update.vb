﻿Imports MySql.Data.MySqlClient
Imports System.Net
Public Class frm_manage_user_pass_update
    Dim query As String
    Dim cmd As MySqlCommand
    Dim reader As MySqlDataReader
    Public user_id_selected As String

    Private Sub btn_save_Click(sender As Object, e As EventArgs) Handles btn_save.Click
        ' Update selected user's password.
        Try
            ' Hashed user's password using Bcrypt encryption.
            Dim hashedPasssword As String = BCrypt.Net.BCrypt.HashPassword(tb_password.Text, 10)

            class_connection.con.Open()
            query = "Update tbl_user Set user_pass = '" & hashedPasssword & "' where user_id = '" &
                user_id_selected & "'"
            cmd = New MySqlCommand(query, class_connection.con)
            cmd.ExecuteNonQuery()
            class_connection.con.Close()

            MsgBox("Password changed!", MsgBoxStyle.Information, "NTC Region 10")

            Close()
        Catch ex As MySqlException
            class_connection.con.Close()
            MsgBox("Couldn't connect to server. Please contact IT Admin for assistance.", MsgBoxStyle.Exclamation, "NTC Region 10")
        Catch ex As Exception
            class_connection.con.Close()
            MsgBox(ex.Message + ": " + ex.GetType.ToString)
        End Try
    End Sub
End Class