﻿Imports MySql.Data.MySqlClient
Imports System.Net

Public Class frm_username_change
    Dim query As String
    Dim cmd As MySqlCommand
    Dim reader As MySqlDataReader
    Public user_id As String

    Private Sub btn_change_un_Click(sender As Object, e As EventArgs) Handles btn_update.Click
        Dim user_settings As New uc_user_settings

        If tb_un.Text <> "" Then
            ' Update username.
            class_connection.con.Open()
            query = "update tbl_user Set user_name = '" & WebUtility.HtmlEncode(tb_un.Text) &
                "' where user_id = '" & user_id & "'"
            cmd = New MySqlCommand(query, class_connection.con)
            cmd.ExecuteNonQuery()
            class_connection.con.Close()

            MsgBox("Username successfully changed!", MsgBoxStyle.Information, "NTC Region 10")

            ' Reload dashboard and redirect to user settings.
            dashboard.pnl_dashboard.Controls.Clear()
            user_settings.setUser(user_id)
            dashboard.Controls.Add(user_settings)

            Close()
        Else
            MsgBox("Fill-up username!", MsgBoxStyle.Exclamation, "NTC Region 10")
        End If
    End Sub
End Class