﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class uc_user_settings
    Inherits System.Windows.Forms.UserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.btn_change_pass = New System.Windows.Forms.Button()
        Me.btn_change_un = New System.Windows.Forms.Button()
        Me.tb_name = New System.Windows.Forms.TextBox()
        Me.tb_username = New System.Windows.Forms.TextBox()
        Me.tb_password = New System.Windows.Forms.TextBox()
        Me.tb_authority = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.btn_browse = New System.Windows.Forms.Button()
        Me.btn_upload = New System.Windows.Forms.Button()
        Me.cb_signature = New System.Windows.Forms.CheckBox()
        Me.tb_filepath = New System.Windows.Forms.TextBox()
        Me.ofd_browse = New System.Windows.Forms.OpenFileDialog()
        Me.pb_signature = New System.Windows.Forms.PictureBox()
        CType(Me.pb_signature, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Segoe UI", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.DarkGray
        Me.Label1.Location = New System.Drawing.Point(106, 64)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(39, 13)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "Name"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Segoe UI", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.DarkGray
        Me.Label2.Location = New System.Drawing.Point(85, 118)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(60, 13)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "Username"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Segoe UI", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ForeColor = System.Drawing.Color.DarkGray
        Me.Label3.Location = New System.Drawing.Point(88, 172)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(57, 13)
        Me.Label3.TabIndex = 3
        Me.Label3.Text = "Password"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Segoe UI", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ForeColor = System.Drawing.Color.DarkGray
        Me.Label4.Location = New System.Drawing.Point(89, 226)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(56, 13)
        Me.Label4.TabIndex = 4
        Me.Label4.Text = "Authority"
        '
        'btn_change_pass
        '
        Me.btn_change_pass.BackColor = System.Drawing.Color.FromArgb(CType(CType(218, Byte), Integer), CType(CType(144, Byte), Integer), CType(CType(27, Byte), Integer))
        Me.btn_change_pass.FlatAppearance.BorderSize = 0
        Me.btn_change_pass.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_change_pass.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_change_pass.ForeColor = System.Drawing.Color.White
        Me.btn_change_pass.Location = New System.Drawing.Point(465, 165)
        Me.btn_change_pass.Name = "btn_change_pass"
        Me.btn_change_pass.Size = New System.Drawing.Size(84, 26)
        Me.btn_change_pass.TabIndex = 14
        Me.btn_change_pass.Text = "Edit"
        Me.btn_change_pass.UseVisualStyleBackColor = False
        '
        'btn_change_un
        '
        Me.btn_change_un.BackColor = System.Drawing.Color.FromArgb(CType(CType(218, Byte), Integer), CType(CType(144, Byte), Integer), CType(CType(27, Byte), Integer))
        Me.btn_change_un.FlatAppearance.BorderSize = 0
        Me.btn_change_un.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_change_un.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_change_un.ForeColor = System.Drawing.Color.White
        Me.btn_change_un.Location = New System.Drawing.Point(465, 111)
        Me.btn_change_un.Name = "btn_change_un"
        Me.btn_change_un.Size = New System.Drawing.Size(84, 26)
        Me.btn_change_un.TabIndex = 15
        Me.btn_change_un.Text = "Edit"
        Me.btn_change_un.UseVisualStyleBackColor = False
        '
        'tb_name
        '
        Me.tb_name.BackColor = System.Drawing.Color.FromArgb(CType(CType(114, Byte), Integer), CType(CType(126, Byte), Integer), CType(CType(138, Byte), Integer))
        Me.tb_name.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.tb_name.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tb_name.ForeColor = System.Drawing.Color.White
        Me.tb_name.Location = New System.Drawing.Point(174, 57)
        Me.tb_name.Name = "tb_name"
        Me.tb_name.ReadOnly = True
        Me.tb_name.Size = New System.Drawing.Size(291, 26)
        Me.tb_name.TabIndex = 16
        '
        'tb_username
        '
        Me.tb_username.BackColor = System.Drawing.Color.FromArgb(CType(CType(114, Byte), Integer), CType(CType(126, Byte), Integer), CType(CType(138, Byte), Integer))
        Me.tb_username.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.tb_username.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tb_username.ForeColor = System.Drawing.Color.White
        Me.tb_username.Location = New System.Drawing.Point(174, 111)
        Me.tb_username.Name = "tb_username"
        Me.tb_username.ReadOnly = True
        Me.tb_username.Size = New System.Drawing.Size(291, 26)
        Me.tb_username.TabIndex = 17
        '
        'tb_password
        '
        Me.tb_password.BackColor = System.Drawing.Color.FromArgb(CType(CType(114, Byte), Integer), CType(CType(126, Byte), Integer), CType(CType(138, Byte), Integer))
        Me.tb_password.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.tb_password.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tb_password.ForeColor = System.Drawing.Color.White
        Me.tb_password.Location = New System.Drawing.Point(174, 165)
        Me.tb_password.MaxLength = 8
        Me.tb_password.Name = "tb_password"
        Me.tb_password.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.tb_password.ReadOnly = True
        Me.tb_password.Size = New System.Drawing.Size(291, 26)
        Me.tb_password.TabIndex = 18
        '
        'tb_authority
        '
        Me.tb_authority.BackColor = System.Drawing.Color.FromArgb(CType(CType(114, Byte), Integer), CType(CType(126, Byte), Integer), CType(CType(138, Byte), Integer))
        Me.tb_authority.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.tb_authority.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tb_authority.ForeColor = System.Drawing.Color.White
        Me.tb_authority.Location = New System.Drawing.Point(174, 219)
        Me.tb_authority.Name = "tb_authority"
        Me.tb_authority.ReadOnly = True
        Me.tb_authority.Size = New System.Drawing.Size(291, 26)
        Me.tb_authority.TabIndex = 19
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Segoe UI", 12.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.ForeColor = System.Drawing.Color.White
        Me.Label5.Location = New System.Drawing.Point(30, 26)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(105, 21)
        Me.Label5.TabIndex = 26
        Me.Label5.Text = "User settings"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Segoe UI", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.ForeColor = System.Drawing.Color.DarkGray
        Me.Label6.Location = New System.Drawing.Point(87, 286)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(58, 13)
        Me.Label6.TabIndex = 28
        Me.Label6.Text = "Signature"
        '
        'btn_browse
        '
        Me.btn_browse.BackColor = System.Drawing.Color.FromArgb(CType(CType(218, Byte), Integer), CType(CType(144, Byte), Integer), CType(CType(27, Byte), Integer))
        Me.btn_browse.FlatAppearance.BorderSize = 0
        Me.btn_browse.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_browse.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_browse.ForeColor = System.Drawing.Color.White
        Me.btn_browse.Location = New System.Drawing.Point(671, 281)
        Me.btn_browse.Name = "btn_browse"
        Me.btn_browse.Size = New System.Drawing.Size(73, 22)
        Me.btn_browse.TabIndex = 29
        Me.btn_browse.Text = "Browse"
        Me.btn_browse.UseVisualStyleBackColor = False
        '
        'btn_upload
        '
        Me.btn_upload.BackColor = System.Drawing.Color.FromArgb(CType(CType(218, Byte), Integer), CType(CType(144, Byte), Integer), CType(CType(27, Byte), Integer))
        Me.btn_upload.Enabled = False
        Me.btn_upload.FlatAppearance.BorderSize = 0
        Me.btn_upload.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_upload.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_upload.ForeColor = System.Drawing.Color.White
        Me.btn_upload.Location = New System.Drawing.Point(520, 314)
        Me.btn_upload.Name = "btn_upload"
        Me.btn_upload.Size = New System.Drawing.Size(110, 35)
        Me.btn_upload.TabIndex = 30
        Me.btn_upload.Text = "Upload"
        Me.btn_upload.UseVisualStyleBackColor = False
        '
        'cb_signature
        '
        Me.cb_signature.AutoSize = True
        Me.cb_signature.Enabled = False
        Me.cb_signature.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cb_signature.ForeColor = System.Drawing.Color.White
        Me.cb_signature.Location = New System.Drawing.Point(443, 386)
        Me.cb_signature.Name = "cb_signature"
        Me.cb_signature.Size = New System.Drawing.Size(237, 21)
        Me.cb_signature.TabIndex = 76
        Me.cb_signature.Text = "Use signature in processing forms"
        Me.cb_signature.UseVisualStyleBackColor = True
        '
        'tb_filepath
        '
        Me.tb_filepath.BackColor = System.Drawing.Color.FromArgb(CType(CType(114, Byte), Integer), CType(CType(126, Byte), Integer), CType(CType(138, Byte), Integer))
        Me.tb_filepath.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.tb_filepath.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tb_filepath.ForeColor = System.Drawing.Color.White
        Me.tb_filepath.Location = New System.Drawing.Point(436, 281)
        Me.tb_filepath.Name = "tb_filepath"
        Me.tb_filepath.ReadOnly = True
        Me.tb_filepath.Size = New System.Drawing.Size(235, 22)
        Me.tb_filepath.TabIndex = 77
        '
        'pb_signature
        '
        Me.pb_signature.BackColor = System.Drawing.Color.FromArgb(CType(CType(114, Byte), Integer), CType(CType(126, Byte), Integer), CType(CType(138, Byte), Integer))
        Me.pb_signature.Image = Global.NTC_ACCOUNTING.My.Resources.Resources._51211
        Me.pb_signature.Location = New System.Drawing.Point(174, 275)
        Me.pb_signature.Name = "pb_signature"
        Me.pb_signature.Size = New System.Drawing.Size(240, 200)
        Me.pb_signature.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.pb_signature.TabIndex = 27
        Me.pb_signature.TabStop = False
        '
        'uc_user_settings
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoScroll = True
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(44, Byte), Integer), CType(CType(62, Byte), Integer), CType(CType(80, Byte), Integer))
        Me.Controls.Add(Me.tb_filepath)
        Me.Controls.Add(Me.cb_signature)
        Me.Controls.Add(Me.btn_upload)
        Me.Controls.Add(Me.btn_browse)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.pb_signature)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.tb_authority)
        Me.Controls.Add(Me.tb_password)
        Me.Controls.Add(Me.tb_username)
        Me.Controls.Add(Me.tb_name)
        Me.Controls.Add(Me.btn_change_un)
        Me.Controls.Add(Me.btn_change_pass)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.MaximumSize = New System.Drawing.Size(1364, 726)
        Me.MinimumSize = New System.Drawing.Size(1364, 726)
        Me.Name = "uc_user_settings"
        Me.Size = New System.Drawing.Size(1364, 726)
        CType(Me.pb_signature, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents Label1 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents Label4 As Label
    Friend WithEvents btn_change_pass As Button
    Friend WithEvents btn_change_un As Button
    Friend WithEvents tb_name As TextBox
    Friend WithEvents tb_username As TextBox
    Friend WithEvents tb_password As TextBox
    Friend WithEvents tb_authority As TextBox
    Friend WithEvents Label5 As Label
    Friend WithEvents pb_signature As PictureBox
    Friend WithEvents Label6 As Label
    Friend WithEvents btn_browse As Button
    Friend WithEvents btn_upload As Button
    Friend WithEvents cb_signature As CheckBox
    Friend WithEvents tb_filepath As TextBox
    Friend WithEvents ofd_browse As OpenFileDialog
End Class
