﻿Imports MySql.Data.MySqlClient
Imports System.Net
Imports Microsoft.Office.Interop
Imports System.IO

Public Class frm_cashier_log
    Dim query, cashier_name, mop_cash, mop_check, endorse_id, soa_id, cashierID,
        enable_cashier_signature, signature_cashier As String
    Dim cmd As MySqlCommand
    Dim reader As MySqlDataReader
    Dim compute_sub_total, total As Double
    Dim row As String()
    Public user_id As String
    Public transact_id_cashier, result, authority_level As Integer

    Private Sub btn_print_Click(sender As Object, e As EventArgs) Handles btn_print.Click
        export_to_excel()
    End Sub

    Sub export_to_excel()
        ' Export data to excel.
        Try
            Dim xlapp As New Excel.Application
            Dim xlWorksheet As Object
            Dim MAXIMUM As Integer = 13
            Dim loop_counter As Integer = MAXIMUM

            xlapp.Workbooks.Open(Application.StartupPath + "\Receipt.xlsx")
            xlapp.Sheets("Sheet1").Select()
            xlWorksheet = xlapp.Sheets("Sheet1")

            ' Disabled print button during the process.
            btn_print.Enabled = False

            ' Mark payment type as cash.
            If cbCash.Checked Then
                xlWorksheet.cells(33, 4).value = "X"
            End If

            ' Mark payment type as check and fill-up check details.
            If cbCheck.Checked Then
                xlWorksheet.cells(33, 6).value = "X"
                xlWorksheet.cells(35, 8).value = tb_check_date.Text
                xlWorksheet.cells(35, 1).value = tb_check_order.Text
            End If

            ' Copy all SOA data entries to excel.
            For i As Integer = 0 To dgv_cashier.Rows.Count - 1 Step +1
                xlWorksheet.cells(loop_counter, 1).value = dgv_cashier.Rows(i).Cells(0).Value.ToString()
                xlWorksheet.cells(loop_counter, 3).value = dgv_cashier.Rows(i).Cells(1).Value.ToString()
                xlWorksheet.cells(loop_counter, 10).value = dgv_cashier.Rows(i).Cells(2).Value.ToString()
                loop_counter += 1
            Next

            ' Copy other transaction details to excel.
            xlWorksheet.cells(6, 10).value = tb_date.Text
            xlWorksheet.cells(8, 1).value = tb_payor.Text
            xlWorksheet.cells(30, 1).value = txtAmountInWords.Text
            xlWorksheet.cells(34, 9).value = cashier_name
            xlWorksheet.cells(28, 10).value = tb_total.Text

            download_signature_cashier()

            ' If cashier checkbox is checked and cashier signature is enabled.
            If cb_cashier_signature.Checked And enable_cashier_signature = "1" Then
                If File.Exists("C:\temp\" + signature_cashier) Then
                    xlWorksheet.Shapes.AddPicture("C:\temp\" + signature_cashier,
                    Microsoft.Office.Core.MsoTriState.msoFalse,
                    Microsoft.Office.Core.MsoTriState.msoCTrue, 355, 320, 90, 72)
                End If
            End If

            ' Set excel program to visible, maximized, and on top.
            xlapp.Visible = True
            xlapp.Top = 1
            xlapp.ActiveWindow.WindowState = Excel.XlWindowState.xlMaximized

            ' Enabled print button after the process.
            btn_print.Enabled = True
        Catch ex As Exception
            btn_print.Enabled = True
            MsgBox("An error occured in processing the receipt, data may not be properly retrieved." &
                "Please close the excel file and try printing again.", MsgBoxStyle.Exclamation,
                "NTC Region 10")
        End Try
    End Sub

    Sub download_signature_cashier()
        ' Retrieve cashier's signature.
        Try
            Dim WebClient As WebClient = New WebClient()

            class_connection.con.Open()
            query = "select signature as 'path', signature_name as 'name', enable_signature from " &
                "tbl_user where user_id = '" & cashierID & "'"
            cmd = New MySqlCommand(query, class_connection.con)
            reader = cmd.ExecuteReader()

            If reader.Read Then
                If reader.GetString("enable_signature") = "1" Then
                    enable_cashier_signature = reader.GetInt32("enable_signature")
                    signature_cashier = reader.GetString("name")

                    If Not Directory.Exists("C:\Temp") Then
                        Directory.CreateDirectory("C:\Temp")
                    End If

                    WebClient.Credentials = New NetworkCredential("ntc_10_server", "ntc_10_server",
                        "ftp://" + class_connection.ftpAddress)
                    WebClient.DownloadFile(WebUtility.HtmlDecode(reader.GetString("path")),
                        "C:\temp\" + reader.GetString("name"))
                End If
            End If
            class_connection.con.Close()
        Catch ex As WebException
            class_connection.con.Close()
        Catch ex As Exception
            class_connection.con.Close()
            MsgBox("Something went wrong. Couldn't process the data.", MsgBoxStyle.Exclamation,
                "NTC Region 10")
        End Try
    End Sub

    Private Sub btn_op_Click(sender As Object, e As EventArgs) Handles btn_op.Click
        ' Open Order of Payment form.
        frm_accountant_log.authority_level = authority_level
        frm_accountant_log.endorse_id = endorse_id
        frm_accountant_log.btn_print.Visible = True
        frm_accountant_log.btn_soa.Visible = False
        frm_accountant_log.btn_receipt.Visible = False
        frm_accountant_log.cash_mod = "1"
        frm_accountant_log.ShowDialog()
    End Sub

    Private Sub btn_soa_Click(sender As Object, e As EventArgs) Handles btn_soa.Click
        ' Open SOA form.
        frm_licenser_log.soa_id_transact = soa_id
        frm_licenser_log.user_id = user_id
        frm_licenser_log.authority_level = authority_level.ToString
        frm_licenser_log.cash_mod = "1"
        frm_licenser_log.pnl_refresh.Visible = False
        frm_licenser_log.ShowDialog()
        frm_licenser_log.pb_approve_status.Visible = True
    End Sub

    Private Sub frm_cashier_log_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        load_soa_data_entries()

        Icon = My.Resources.ntc_material_logo
    End Sub

    Sub load_soa_data_entries()
        ' Retrieve receipt data.
        Try
            class_connection.con.Open()
            query = "select * from tbl_soa_cashier where cashier_transact_id = '" &
                transact_id_cashier.ToString & "'"
            cmd = New MySqlCommand(query, class_connection.con)
            reader = cmd.ExecuteReader()

            If reader.Read Then
                tb_date.Text = reader.GetDateTime("date_transact").ToString("MM-dd-yyyy") & " " &
                    reader.GetDateTime("date_transact").ToString("hh:mm:ss tt")
                txtOR.Text = reader.GetString("or_num")
                soa_id = reader.GetString("soa_id")
                endorse_id = reader.GetString("op_id")
                cashierID = reader.GetString("user_id_cashier")

                ' Check if payment type is cash, check or both.
                Select Case reader.GetString("mode_of_payment")
                    Case "0"
                        pnlCheck.Visible = False
                        pnlCheck2.Visible = False
                        cbCheck.Checked = False
                        cbCheck.BackColor = Color.FromArgb(44, 62, 80)
                        cbCash.Checked = True
                        cbCash.Enabled = False
                        cbCheck.Enabled = False
                    Case "1"
                        pnlCheck.Visible = True
                        pnlCheck2.Visible = True
                        cbCash.Checked = False
                        cbCheck.BackColor = Color.FromArgb(114, 126, 138)
                        cbCheck.Checked = True
                        tb_check_order.Text = WebUtility.HtmlDecode(reader.GetString("check_number"))
                        tb_check_date.Text = reader.GetDateTime("check_date").ToString("yyyy-MM-dd")
                        cbCash.Enabled = False
                        cbCheck.Enabled = False
                    Case Else
                        cbCash.Checked = True
                        cbCheck.Checked = True
                        pnlCheck.Visible = True
                        pnlCheck2.Visible = True
                        cbCheck.BackColor = Color.FromArgb(114, 126, 138)
                        tb_check_order.Text = WebUtility.HtmlDecode(reader.GetString("check_number"))
                        tb_check_date.Text = reader.GetDateTime("check_date").ToString("yyyy-MM-dd")
                        cbCash.Enabled = False
                        cbCheck.Enabled = False
                End Select
            End If
            class_connection.con.Close()

            ' Retrieve user's full name.
            class_connection.con.Open()
            query = "select full_name from tbl_user where user_id = '" & cashierID & "'"
            cmd = New MySqlCommand(query, class_connection.con)
            reader = cmd.ExecuteReader()

            If reader.Read Then
                cashier_name = WebUtility.HtmlDecode(reader.GetString("full_name"))
            End If
            class_connection.con.Close()

            ' Retrieve payor's name. 
            class_connection.con.Open()
            query = "Select name From tbl_soa_cashier a, tbl_soa_transact b, tbl_soa_details c, " &
                "tbl_payor d Where a.soa_id = b.soa_id And b.soa_details_id = c.soa_details_id And " &
                "c.payor_id = d.payor_id And a.cashier_transact_id = '" & transact_id_cashier.ToString & "'"
            cmd = New MySqlCommand(query, class_connection.con)
            reader = cmd.ExecuteReader()

            If reader.Read Then
                tb_payor.Text = WebUtility.HtmlDecode(reader.GetString("name"))
            End If
            class_connection.con.Close()

            ' Load datagridview settings.
            initialize_table()

            ' Retrieve SOA data entries and compute for sub-total then display.
            class_connection.con.Open()
            query = "select code as 'Code', description as 'Description', cat as 'Category', " &
                "others_custom_value , p1_no_of_years as 'No. of Years 1', p1_percent as " &
                "'Percent 1', p1_no_of_units as 'No. of Units 1', p1_fees as 'Fees 1', " &
                "p2_no_of_years as 'No. of Years 2', p2_percent as 'Percent 2', p2_no_of_units " &
                "as 'No. of Units 2', p2_fees as 'Fees 2', p3_no_of_years as 'No. of Years 3', " &
                "p3_percent as 'Percent 3', p3_no_of_units as 'No. of Units 3', p3_fees as " &
                "'Fees 3' from tbl_soa_transact join tbl_soa_details on " &
                "tbl_soa_details.soa_details_id = tbl_soa_transact.soa_details_id join " &
                "tbl_soa_data on tbl_soa_data.soa_data_id = tbl_soa_details.soa_data_id " &
                "join tbl_mode_of_payment on tbl_mode_of_payment.mop_id = tbl_soa_data.mop_id" &
                " where tbl_soa_transact.soa_id = '" & soa_id & "' order by " &
                "tbl_mode_of_payment.mop_id"
            cmd = New MySqlCommand(query, class_connection.con)
            reader = cmd.ExecuteReader()

            While reader.Read
                Dim value As Double = reader.GetDouble("Percent 1")
                Dim value2 As Double = reader.GetDouble("Percent 2")
                Dim value3 As Double = reader.GetDouble("Percent 3")
                Dim num, num2, num3, percentage, percentage2, percentage3 As Double
                Dim description = WebUtility.HtmlDecode(reader.GetString("Description"))
                Dim custom_value = WebUtility.HtmlDecode(reader.GetString("others_custom_value"))
                Dim entry_code = reader.GetString("Code")

                ' Convert percentage to double. 
                If Double.TryParse(value, num) And Double.TryParse(value2, num2) And
                    Double.TryParse(value3, num3) Then
                    percentage = num / 100
                    percentage2 = num2 / 100
                    percentage3 = num3 / 100
                End If

                ' Compute sub-total of each SOA data entries.
                compute_sub_total = (reader.GetDouble("No. of Years 1") * percentage *
                    reader.GetDouble("No. of Units 1") * reader.GetDouble("Fees 1")) +
                    (reader.GetDouble("No. of Years 2") * percentage2 * reader.GetDouble("No. of Units 2") _
                    * reader.GetDouble("Fees 2")) + (reader.GetDouble("No. of Years 3") *
                    percentage3 * reader.GetDouble("No. of Units 3") * reader.GetDouble("Fees 3"))
                compute_sub_total = FormatNumber(CDbl(compute_sub_total), 2)

                ' Create SOA data entry.
                row = New String() {entry_code, description, compute_sub_total}

                ' Modify entry if it has custom value.
                If reader.GetString("others_custom_value") <> "" Then
                    row = New String() {entry_code, (description & ": " & custom_value), compute_sub_total}
                End If

                ' Add entry into datagrid.
                dgv_cashier.Rows.Add(row)
            End While
            class_connection.con.Close()

            ' Disable sorting in all columns.
            For Each column As DataGridViewColumn In dgv_cashier.Columns
                column.SortMode = DataGridViewColumnSortMode.NotSortable
            Next

            ' Initialize total to zero.
            total = 0

            ' Compute all sub-total into total
            For i = 0 To dgv_cashier.Rows.Count - 1 Step +1
                Try
                    total += dgv_cashier.Rows(i).Cells("Sub-total").Value
                Catch ex As Exception
                    total += 0
                End Try
            Next

            ' Format total and insert it into textbox.
            total = FormatNumber(CDbl(total), 2)
            tb_total.Text = total

            ' Convert total into amount in words.
            txtAmountInWords.Text = " " & class_number_to_word.ConvertNumberToENG(tb_total.Text)

            isSignatureCashierEnabled()
        Catch ex As MySqlException
            class_connection.con.Close()
            MsgBox("Couldn't connect to server, the process is cancelled. Contact the IT " &
                "Administrator for assistance.", MsgBoxStyle.Exclamation, "NTC Region 10")
            Close()
        Catch ex As Exception
            class_connection.con.Close()
            MsgBox("Something went wrong. Couldn't process the data.", MsgBoxStyle.Exclamation,
                "NTC Region 10")
            Close()
        End Try
    End Sub

    Sub isSignatureCashierEnabled()
        ' Checkbox is enabled if cashier's signature is enabled and disabled otherwise. 
        class_connection.con.Open()
        query = "select enable_signature from tbl_user where user_id = '" & user_id & "'"
        cmd = New MySqlCommand(query, class_connection.con)
        reader = cmd.ExecuteReader()

        If reader.Read Then
            If reader.GetString("enable_signature") = "1" Then
                cb_cashier_signature.Enabled = True
            Else
                cb_cashier_signature.Enabled = False
            End If
        End If
        class_connection.con.Close()
    End Sub

    Sub initialize_table()
        dgv_cashier.ColumnCount = 3
        dgv_cashier.Columns(0).Name = "Code"
        dgv_cashier.Columns(0).ReadOnly = True
        dgv_cashier.Columns(0).Width = 70
        dgv_cashier.Columns(1).Name = "Description"
        dgv_cashier.Columns(1).ReadOnly = True
        dgv_cashier.Columns(1).Width = 300
        dgv_cashier.Columns(2).Name = "Sub-Total"
        dgv_cashier.Columns(2).ReadOnly = True
        dgv_cashier.Columns(2).Width = 100
        dgv_cashier.Rows.Clear()
    End Sub
End Class