﻿Imports MySql.Data.MySqlClient
Imports System.Net
Public Class payorInfo
    Dim query As String
    Dim cmd As MySqlCommand
    Dim reader As MySqlDataReader
    Public isAccessFromCreateSOA As Boolean

    Private Sub payorInfofrm_Load(sender As Object, e As EventArgs) Handles Me.Load
        loadPayors()
    End Sub

    Sub initTable()
        dgv_payor.ColumnCount = 3
        dgv_payor.Columns(0).Name = "#"
        dgv_payor.Columns(0).ReadOnly = True
        dgv_payor.Columns(1).Name = "Name"
        dgv_payor.Columns(1).ReadOnly = True
        dgv_payor.Columns(2).Name = "Address"
        dgv_payor.Columns(2).ReadOnly = True
    End Sub

    Sub loadPayors()
        Try
            initTable()

            dgv_payor.Rows.Clear()

            Connect.con.Open()
            query = "select payor_id as '#', name as 'Name', address as 'Address' from tbl_payor order by payor_id DESC"
            cmd = New MySqlCommand(query, Connect.con)
            reader = cmd.ExecuteReader()

            While reader.Read
                Dim add_row_content As String() = New String() {reader.GetString("#"), WebUtility.HtmlDecode(reader.GetString("Name")), WebUtility.HtmlDecode(reader.GetString("Address"))}
                dgv_payor.Rows.Add(add_row_content)
            End While
            Connect.con.Close()

            dgv_payor.Columns(0).Visible = False
            dgv_payor.Columns(0).Width = 60

            tb_search.Enabled = True
            dgv_payor.Visible = True
            tb_message.Visible = False
            btn_refresh.Visible = False
        Catch ex As MySqlException
            tb_search.Enabled = False
            dgv_payor.Visible = False
            tb_message.Visible = True
            btn_refresh.Visible = True
        Catch ex As Exception
            MsgBox(ex.Message + " ; " & ex.GetType.ToString)
        End Try

    End Sub

    Private Sub btnAddPayor_Click(sender As Object, e As EventArgs) Handles btnAddPayor.Click
        Try
            addPayor()
        Catch ex As MySqlException
            MsgBox("Could'nt connect to server. Contact the IT Administrator for assistance.", MsgBoxStyle.Exclamation, "NTC Region 10")
            loadPayors()
        Catch ex As Exception
            MsgBox(ex.Message + " ; " & ex.GetType.ToString)
        End Try

    End Sub

    Sub addPayor()
        If txtPayor.Text <> "" And txtAddress.Text <> "" Then
            Connect.con.Open()
            query = "insert into tbl_payor(name, address) Values('" + WebUtility.HtmlEncode(txtPayor.Text) + "', '" + WebUtility.HtmlEncode(txtAddress.Text) + "')"
            cmd = New MySqlCommand(query, Connect.con)
            cmd.ExecuteNonQuery()
            Connect.con.Close()

            loadPayors()

            MsgBox(txtPayor.Text + " has been added!", MsgBoxStyle.Information, "NTC Region 10")

            txtPayor.Text = ""
            txtAddress.Text = ""
        Else
            MsgBox("Fill required payor details.", MsgBoxStyle.Exclamation, "NTC Region 10")
        End If
    End Sub

    Private Sub dgv_payor_CellClick(sender As Object, e As DataGridViewCellEventArgs) Handles dgv_payor.CellClick
        If e.RowIndex >= 0 Then
            Dim row As DataGridViewRow
            If isAccessFromCreateSOA Then
                row = dgv_payor.Rows(e.RowIndex)
                frmSOATransaction.txtpayor.Text = row.Cells("Name").Value.ToString
                frmSOATransaction.payor_id = row.Cells("#").Value.ToString
                frmSOATransaction.isSetPayor = True
                Close()
            Else
                row = dgv_payor.Rows(e.RowIndex)
                frmEditSOATransaction.txtpayor.Text = row.Cells("Name").Value.ToString
                frmEditSOATransaction.payor_id = row.Cells("#").Value.ToString
                frmEditSOATransaction.isSetPayor = True
                Close()
            End If
        End If
    End Sub

    Private Sub tb_search_TextChanged(sender As Object, e As EventArgs) Handles tb_search.TextChanged
        Try
            dgv_payor.Rows.Clear()

            Connect.con.Open()
            query = "select payor_id as '#', name as 'Name', address as 'Address' from tbl_payor where Name like '" _
                + tb_search.Text + "%'"
            cmd = New MySqlCommand(query, Connect.con)
            reader = cmd.ExecuteReader()

            While reader.Read
                Dim add_row_content As String() = New String() {reader.GetString("#"), WebUtility.HtmlDecode(reader.GetString("Name")), WebUtility.HtmlDecode(reader.GetString("Address"))}
                dgv_payor.Rows.Add(add_row_content)
            End While
            Connect.con.Close()

            dgv_payor.Columns(0).Width = 50
        Catch ex As MySqlException
            tb_search.Enabled = False
            dgv_payor.Visible = False
            tb_message.Visible = True
            btn_refresh.Visible = True
        Catch ex As Exception
            MsgBox(ex.Message + " ; " & ex.GetType.ToString)
        End Try
    End Sub

    Private Sub BunifuImageButton1_Click(sender As Object, e As EventArgs) Handles btn_refresh.Click
        tb_search.Text = ""
        loadPayors()
    End Sub
End Class