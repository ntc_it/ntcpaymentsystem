﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frm_payor_add
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.dgv_payor = New System.Windows.Forms.DataGridView()
        Me.btnAddPayor = New Bunifu.Framework.UI.BunifuFlatButton()
        Me.txtAddress = New Bunifu.Framework.UI.BunifuMaterialTextbox()
        Me.txtPayor = New Bunifu.Framework.UI.BunifuMaterialTextbox()
        Me.tb_search = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.RadTitleBar1 = New Telerik.WinControls.UI.RadTitleBar()
        Me.tb_message = New System.Windows.Forms.TextBox()
        Me.btn_refresh = New Bunifu.Framework.UI.BunifuImageButton()
        CType(Me.dgv_payor, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RadTitleBar1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.btn_refresh, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'dgv_payor
        '
        Me.dgv_payor.AllowUserToAddRows = False
        Me.dgv_payor.AllowUserToDeleteRows = False
        Me.dgv_payor.AllowUserToResizeColumns = False
        Me.dgv_payor.AllowUserToResizeRows = False
        Me.dgv_payor.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.dgv_payor.BackgroundColor = System.Drawing.Color.FromArgb(CType(CType(34, Byte), Integer), CType(CType(48, Byte), Integer), CType(CType(56, Byte), Integer))
        Me.dgv_payor.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.dgv_payor.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.[Single]
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgv_payor.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.dgv_payor.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(CType(CType(34, Byte), Integer), CType(CType(48, Byte), Integer), CType(CType(56, Byte), Integer))
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.Color.White
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.FromArgb(CType(CType(181, Byte), Integer), CType(CType(186, Byte), Integer), CType(CType(188, Byte), Integer))
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgv_payor.DefaultCellStyle = DataGridViewCellStyle2
        Me.dgv_payor.GridColor = System.Drawing.Color.FromArgb(CType(CType(34, Byte), Integer), CType(CType(48, Byte), Integer), CType(CType(56, Byte), Integer))
        Me.dgv_payor.Location = New System.Drawing.Point(465, 90)
        Me.dgv_payor.Name = "dgv_payor"
        Me.dgv_payor.RowHeadersVisible = False
        Me.dgv_payor.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgv_payor.Size = New System.Drawing.Size(494, 354)
        Me.dgv_payor.TabIndex = 14
        '
        'btnAddPayor
        '
        Me.btnAddPayor.Activecolor = System.Drawing.Color.FromArgb(CType(CType(249, Byte), Integer), CType(CType(170, Byte), Integer), CType(CType(51, Byte), Integer))
        Me.btnAddPayor.BackColor = System.Drawing.Color.FromArgb(CType(CType(218, Byte), Integer), CType(CType(144, Byte), Integer), CType(CType(27, Byte), Integer))
        Me.btnAddPayor.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnAddPayor.BorderRadius = 0
        Me.btnAddPayor.ButtonText = "Register New Payor"
        Me.btnAddPayor.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnAddPayor.DisabledColor = System.Drawing.Color.Gray
        Me.btnAddPayor.Font = New System.Drawing.Font("Segoe UI Black", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAddPayor.ForeColor = System.Drawing.Color.Black
        Me.btnAddPayor.Iconcolor = System.Drawing.Color.Transparent
        Me.btnAddPayor.Iconimage = Nothing
        Me.btnAddPayor.Iconimage_right = Nothing
        Me.btnAddPayor.Iconimage_right_Selected = Nothing
        Me.btnAddPayor.Iconimage_Selected = Nothing
        Me.btnAddPayor.IconMarginLeft = 0
        Me.btnAddPayor.IconMarginRight = 0
        Me.btnAddPayor.IconRightVisible = True
        Me.btnAddPayor.IconRightZoom = 0R
        Me.btnAddPayor.IconVisible = True
        Me.btnAddPayor.IconZoom = 90.0R
        Me.btnAddPayor.IsTab = False
        Me.btnAddPayor.Location = New System.Drawing.Point(149, 292)
        Me.btnAddPayor.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.btnAddPayor.Name = "btnAddPayor"
        Me.btnAddPayor.Normalcolor = System.Drawing.Color.FromArgb(CType(CType(218, Byte), Integer), CType(CType(144, Byte), Integer), CType(CType(27, Byte), Integer))
        Me.btnAddPayor.OnHovercolor = System.Drawing.Color.FromArgb(CType(CType(249, Byte), Integer), CType(CType(170, Byte), Integer), CType(CType(51, Byte), Integer))
        Me.btnAddPayor.OnHoverTextColor = System.Drawing.Color.Black
        Me.btnAddPayor.selected = False
        Me.btnAddPayor.Size = New System.Drawing.Size(165, 36)
        Me.btnAddPayor.TabIndex = 3
        Me.btnAddPayor.Text = "Register New Payor"
        Me.btnAddPayor.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.btnAddPayor.Textcolor = System.Drawing.Color.Black
        Me.btnAddPayor.TextFont = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        '
        'txtAddress
        '
        Me.txtAddress.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtAddress.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtAddress.ForeColor = System.Drawing.Color.White
        Me.txtAddress.HintForeColor = System.Drawing.Color.White
        Me.txtAddress.HintText = "Address"
        Me.txtAddress.isPassword = False
        Me.txtAddress.LineFocusedColor = System.Drawing.Color.White
        Me.txtAddress.LineIdleColor = System.Drawing.Color.White
        Me.txtAddress.LineMouseHoverColor = System.Drawing.Color.White
        Me.txtAddress.LineThickness = 2
        Me.txtAddress.Location = New System.Drawing.Point(42, 213)
        Me.txtAddress.Margin = New System.Windows.Forms.Padding(4)
        Me.txtAddress.Name = "txtAddress"
        Me.txtAddress.Size = New System.Drawing.Size(379, 33)
        Me.txtAddress.TabIndex = 2
        Me.txtAddress.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txtPayor
        '
        Me.txtPayor.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtPayor.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPayor.ForeColor = System.Drawing.Color.White
        Me.txtPayor.HintForeColor = System.Drawing.Color.White
        Me.txtPayor.HintText = "Payor"
        Me.txtPayor.isPassword = False
        Me.txtPayor.LineFocusedColor = System.Drawing.Color.White
        Me.txtPayor.LineIdleColor = System.Drawing.Color.White
        Me.txtPayor.LineMouseHoverColor = System.Drawing.Color.White
        Me.txtPayor.LineThickness = 2
        Me.txtPayor.Location = New System.Drawing.Point(42, 160)
        Me.txtPayor.Margin = New System.Windows.Forms.Padding(4)
        Me.txtPayor.Name = "txtPayor"
        Me.txtPayor.Size = New System.Drawing.Size(379, 33)
        Me.txtPayor.TabIndex = 1
        Me.txtPayor.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'tb_search
        '
        Me.tb_search.BackColor = System.Drawing.Color.FromArgb(CType(CType(28, Byte), Integer), CType(CType(40, Byte), Integer), CType(CType(52, Byte), Integer))
        Me.tb_search.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.tb_search.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tb_search.ForeColor = System.Drawing.Color.White
        Me.tb_search.Location = New System.Drawing.Point(628, 55)
        Me.tb_search.MaxLength = 4
        Me.tb_search.Name = "tb_search"
        Me.tb_search.Size = New System.Drawing.Size(331, 22)
        Me.tb_search.TabIndex = 17
        Me.tb_search.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.White
        Me.Label1.Location = New System.Drawing.Point(581, 61)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(41, 13)
        Me.Label1.TabIndex = 19
        Me.Label1.Text = "Search"
        '
        'RadTitleBar1
        '
        Me.RadTitleBar1.Dock = System.Windows.Forms.DockStyle.Top
        Me.RadTitleBar1.Location = New System.Drawing.Point(0, 0)
        Me.RadTitleBar1.Name = "RadTitleBar1"
        Me.RadTitleBar1.Size = New System.Drawing.Size(995, 23)
        Me.RadTitleBar1.TabIndex = 20
        Me.RadTitleBar1.TabStop = False
        Me.RadTitleBar1.Text = "NTC Region 10 - Payor List"
        '
        'tb_message
        '
        Me.tb_message.BackColor = System.Drawing.Color.FromArgb(CType(CType(44, Byte), Integer), CType(CType(62, Byte), Integer), CType(CType(80, Byte), Integer))
        Me.tb_message.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.tb_message.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tb_message.ForeColor = System.Drawing.Color.White
        Me.tb_message.Location = New System.Drawing.Point(548, 246)
        Me.tb_message.MaxLength = 25
        Me.tb_message.Name = "tb_message"
        Me.tb_message.Size = New System.Drawing.Size(263, 26)
        Me.tb_message.TabIndex = 21
        Me.tb_message.Text = "Couldn't connect to server."
        Me.tb_message.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.tb_message.Visible = False
        '
        'btn_refresh
        '
        Me.btn_refresh.BackColor = System.Drawing.Color.Transparent
        Me.btn_refresh.Image = Global.NTC_ACCOUNTING.My.Resources.Resources.refresh
        Me.btn_refresh.ImageActive = Nothing
        Me.btn_refresh.Location = New System.Drawing.Point(824, 240)
        Me.btn_refresh.Name = "btn_refresh"
        Me.btn_refresh.Size = New System.Drawing.Size(47, 39)
        Me.btn_refresh.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.btn_refresh.TabIndex = 23
        Me.btn_refresh.TabStop = False
        Me.btn_refresh.Visible = False
        Me.btn_refresh.Zoom = 10
        '
        'frm_payor_add
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(44, Byte), Integer), CType(CType(62, Byte), Integer), CType(CType(80, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(995, 477)
        Me.Controls.Add(Me.btn_refresh)
        Me.Controls.Add(Me.tb_search)
        Me.Controls.Add(Me.tb_message)
        Me.Controls.Add(Me.RadTitleBar1)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.dgv_payor)
        Me.Controls.Add(Me.btnAddPayor)
        Me.Controls.Add(Me.txtAddress)
        Me.Controls.Add(Me.txtPayor)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frm_payor_add"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "NTC Region 10 - Payor List"
        CType(Me.dgv_payor, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RadTitleBar1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.btn_refresh, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents dgv_payor As DataGridView
    Friend WithEvents btnAddPayor As Bunifu.Framework.UI.BunifuFlatButton
    Friend WithEvents txtAddress As Bunifu.Framework.UI.BunifuMaterialTextbox
    Friend WithEvents txtPayor As Bunifu.Framework.UI.BunifuMaterialTextbox
    Friend WithEvents tb_search As TextBox
    Friend WithEvents Label1 As Label
    Friend WithEvents RadTitleBar1 As Telerik.WinControls.UI.RadTitleBar
    Friend WithEvents tb_message As TextBox
    Friend WithEvents btn_refresh As Bunifu.Framework.UI.BunifuImageButton
End Class
