﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frm_review_soa
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.tb_approved = New System.Windows.Forms.TextBox()
        Me.tb_prepared_by = New System.Windows.Forms.TextBox()
        Me.btn_action_two = New System.Windows.Forms.Button()
        Me.btn_action_one = New System.Windows.Forms.Button()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.tb_total = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.tb_expiration = New System.Windows.Forms.TextBox()
        Me.tb_date_created = New System.Windows.Forms.TextBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.RadTitleBar1 = New Telerik.WinControls.UI.RadTitleBar()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.tb_particular_two_pc_to = New System.Windows.Forms.TextBox()
        Me.tb_particular_two = New System.Windows.Forms.TextBox()
        Me.tb_particular_two_pc_from = New System.Windows.Forms.TextBox()
        Me.Label24 = New System.Windows.Forms.Label()
        Me.Label25 = New System.Windows.Forms.Label()
        Me.Label26 = New System.Windows.Forms.Label()
        Me.Label27 = New System.Windows.Forms.Label()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.tb_particular_three_pc_to = New System.Windows.Forms.TextBox()
        Me.tb_particular_three_pc_from = New System.Windows.Forms.TextBox()
        Me.tb_particular_three = New System.Windows.Forms.TextBox()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.Label21 = New System.Windows.Forms.Label()
        Me.Label22 = New System.Windows.Forms.Label()
        Me.Label23 = New System.Windows.Forms.Label()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.tb_particular_one_pc_to = New System.Windows.Forms.TextBox()
        Me.tb_particular_one_pc_from = New System.Windows.Forms.TextBox()
        Me.tb_particular_one = New System.Windows.Forms.TextBox()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.dgv_soa_data = New System.Windows.Forms.DataGridView()
        Me.tb_message = New System.Windows.Forms.TextBox()
        Me.tb_payor = New System.Windows.Forms.TextBox()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.tb_serial_num = New System.Windows.Forms.TextBox()
        Me.tb_service_type = New System.Windows.Forms.TextBox()
        Me.tb_process_type = New System.Windows.Forms.TextBox()
        Me.tb_transact_type = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Panel12 = New System.Windows.Forms.Panel()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Panel4 = New System.Windows.Forms.Panel()
        Me.pnl_attachments = New System.Windows.Forms.Panel()
        Me.btn_attachment = New System.Windows.Forms.Button()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.tb_date_modified = New System.Windows.Forms.TextBox()
        CType(Me.RadTitleBar1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel3.SuspendLayout()
        Me.Panel2.SuspendLayout()
        Me.Panel1.SuspendLayout()
        CType(Me.dgv_soa_data, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel12.SuspendLayout()
        Me.Panel4.SuspendLayout()
        Me.pnl_attachments.SuspendLayout()
        Me.SuspendLayout()
        '
        'tb_approved
        '
        Me.tb_approved.BackColor = System.Drawing.Color.FromArgb(CType(CType(44, Byte), Integer), CType(CType(62, Byte), Integer), CType(CType(80, Byte), Integer))
        Me.tb_approved.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.tb_approved.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tb_approved.ForeColor = System.Drawing.Color.White
        Me.tb_approved.Location = New System.Drawing.Point(660, 651)
        Me.tb_approved.Name = "tb_approved"
        Me.tb_approved.ReadOnly = True
        Me.tb_approved.Size = New System.Drawing.Size(167, 18)
        Me.tb_approved.TabIndex = 134
        Me.tb_approved.Text = "************  *. ********"
        '
        'tb_prepared_by
        '
        Me.tb_prepared_by.BackColor = System.Drawing.Color.FromArgb(CType(CType(44, Byte), Integer), CType(CType(62, Byte), Integer), CType(CType(80, Byte), Integer))
        Me.tb_prepared_by.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.tb_prepared_by.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tb_prepared_by.ForeColor = System.Drawing.Color.White
        Me.tb_prepared_by.Location = New System.Drawing.Point(660, 621)
        Me.tb_prepared_by.Name = "tb_prepared_by"
        Me.tb_prepared_by.ReadOnly = True
        Me.tb_prepared_by.Size = New System.Drawing.Size(167, 18)
        Me.tb_prepared_by.TabIndex = 132
        Me.tb_prepared_by.Text = "************  *. ********"
        '
        'btn_action_two
        '
        Me.btn_action_two.BackColor = System.Drawing.Color.FromArgb(CType(CType(218, Byte), Integer), CType(CType(144, Byte), Integer), CType(CType(27, Byte), Integer))
        Me.btn_action_two.FlatAppearance.BorderSize = 0
        Me.btn_action_two.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_action_two.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_action_two.ForeColor = System.Drawing.Color.White
        Me.btn_action_two.Location = New System.Drawing.Point(132, 11)
        Me.btn_action_two.Name = "btn_action_two"
        Me.btn_action_two.Size = New System.Drawing.Size(110, 35)
        Me.btn_action_two.TabIndex = 129
        Me.btn_action_two.Text = "Two"
        Me.btn_action_two.UseVisualStyleBackColor = False
        '
        'btn_action_one
        '
        Me.btn_action_one.BackColor = System.Drawing.Color.FromArgb(CType(CType(218, Byte), Integer), CType(CType(144, Byte), Integer), CType(CType(27, Byte), Integer))
        Me.btn_action_one.FlatAppearance.BorderSize = 0
        Me.btn_action_one.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_action_one.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_action_one.ForeColor = System.Drawing.Color.White
        Me.btn_action_one.Location = New System.Drawing.Point(10, 11)
        Me.btn_action_one.Name = "btn_action_one"
        Me.btn_action_one.Size = New System.Drawing.Size(110, 35)
        Me.btn_action_one.TabIndex = 128
        Me.btn_action_one.Text = "One"
        Me.btn_action_one.UseVisualStyleBackColor = False
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Font = New System.Drawing.Font("Segoe UI", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label16.ForeColor = System.Drawing.Color.White
        Me.Label16.Location = New System.Drawing.Point(8, 11)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(33, 13)
        Me.Label16.TabIndex = 127
        Me.Label16.Text = "Total"
        '
        'tb_total
        '
        Me.tb_total.BackColor = System.Drawing.Color.FromArgb(CType(CType(28, Byte), Integer), CType(CType(40, Byte), Integer), CType(CType(52, Byte), Integer))
        Me.tb_total.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.tb_total.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tb_total.ForeColor = System.Drawing.Color.White
        Me.tb_total.Location = New System.Drawing.Point(84, 4)
        Me.tb_total.Name = "tb_total"
        Me.tb_total.ReadOnly = True
        Me.tb_total.Size = New System.Drawing.Size(199, 26)
        Me.tb_total.TabIndex = 126
        Me.tb_total.Text = "0"
        Me.tb_total.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Segoe UI", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.ForeColor = System.Drawing.Color.DarkGray
        Me.Label6.Location = New System.Drawing.Point(739, 75)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(128, 13)
        Me.Label6.TabIndex = 122
        Me.Label6.Text = "Date and Time Created"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Segoe UI", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.ForeColor = System.Drawing.Color.DarkGray
        Me.Label5.Location = New System.Drawing.Point(795, 134)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(72, 13)
        Me.Label5.TabIndex = 121
        Me.Label5.Text = "Date Expires"
        '
        'tb_expiration
        '
        Me.tb_expiration.BackColor = System.Drawing.Color.FromArgb(CType(CType(44, Byte), Integer), CType(CType(62, Byte), Integer), CType(CType(80, Byte), Integer))
        Me.tb_expiration.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.tb_expiration.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tb_expiration.ForeColor = System.Drawing.Color.White
        Me.tb_expiration.Location = New System.Drawing.Point(886, 131)
        Me.tb_expiration.Name = "tb_expiration"
        Me.tb_expiration.ReadOnly = True
        Me.tb_expiration.Size = New System.Drawing.Size(246, 18)
        Me.tb_expiration.TabIndex = 117
        Me.tb_expiration.Text = "yyyy-MM-dd"
        '
        'tb_date_created
        '
        Me.tb_date_created.BackColor = System.Drawing.Color.FromArgb(CType(CType(44, Byte), Integer), CType(CType(62, Byte), Integer), CType(CType(80, Byte), Integer))
        Me.tb_date_created.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.tb_date_created.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tb_date_created.ForeColor = System.Drawing.Color.White
        Me.tb_date_created.Location = New System.Drawing.Point(886, 72)
        Me.tb_date_created.Name = "tb_date_created"
        Me.tb_date_created.ReadOnly = True
        Me.tb_date_created.Size = New System.Drawing.Size(445, 18)
        Me.tb_date_created.TabIndex = 115
        Me.tb_date_created.Text = "yyyy-MM-dd"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Segoe UI", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.ForeColor = System.Drawing.Color.DarkGray
        Me.Label8.Location = New System.Drawing.Point(563, 654)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(55, 13)
        Me.Label8.TabIndex = 107
        Me.Label8.Text = "Approver"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Segoe UI", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.ForeColor = System.Drawing.Color.DarkGray
        Me.Label7.Location = New System.Drawing.Point(554, 624)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(63, 13)
        Me.Label7.TabIndex = 106
        Me.Label7.Text = "Preparator"
        '
        'RadTitleBar1
        '
        Me.RadTitleBar1.Dock = System.Windows.Forms.DockStyle.Top
        Me.RadTitleBar1.Location = New System.Drawing.Point(0, 0)
        Me.RadTitleBar1.Name = "RadTitleBar1"
        Me.RadTitleBar1.Size = New System.Drawing.Size(1350, 23)
        Me.RadTitleBar1.TabIndex = 105
        Me.RadTitleBar1.TabStop = False
        Me.RadTitleBar1.Text = "NTC Region 10 "
        '
        'Panel3
        '
        Me.Panel3.BackColor = System.Drawing.Color.FromArgb(CType(CType(36, Byte), Integer), CType(CType(48, Byte), Integer), CType(CType(59, Byte), Integer))
        Me.Panel3.Controls.Add(Me.tb_particular_two_pc_to)
        Me.Panel3.Controls.Add(Me.tb_particular_two)
        Me.Panel3.Controls.Add(Me.tb_particular_two_pc_from)
        Me.Panel3.Controls.Add(Me.Label24)
        Me.Panel3.Controls.Add(Me.Label25)
        Me.Panel3.Controls.Add(Me.Label26)
        Me.Panel3.Controls.Add(Me.Label27)
        Me.Panel3.Location = New System.Drawing.Point(681, 164)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(259, 133)
        Me.Panel3.TabIndex = 138
        '
        'tb_particular_two_pc_to
        '
        Me.tb_particular_two_pc_to.BackColor = System.Drawing.Color.FromArgb(CType(CType(36, Byte), Integer), CType(CType(48, Byte), Integer), CType(CType(59, Byte), Integer))
        Me.tb_particular_two_pc_to.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.tb_particular_two_pc_to.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tb_particular_two_pc_to.ForeColor = System.Drawing.Color.White
        Me.tb_particular_two_pc_to.Location = New System.Drawing.Point(48, 97)
        Me.tb_particular_two_pc_to.Name = "tb_particular_two_pc_to"
        Me.tb_particular_two_pc_to.Size = New System.Drawing.Size(200, 18)
        Me.tb_particular_two_pc_to.TabIndex = 31
        Me.tb_particular_two_pc_to.Text = "Date"
        Me.tb_particular_two_pc_to.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'tb_particular_two
        '
        Me.tb_particular_two.BackColor = System.Drawing.Color.FromArgb(CType(CType(36, Byte), Integer), CType(CType(48, Byte), Integer), CType(CType(59, Byte), Integer))
        Me.tb_particular_two.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.tb_particular_two.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tb_particular_two.ForeColor = System.Drawing.Color.White
        Me.tb_particular_two.Location = New System.Drawing.Point(19, 26)
        Me.tb_particular_two.Margin = New System.Windows.Forms.Padding(5)
        Me.tb_particular_two.Name = "tb_particular_two"
        Me.tb_particular_two.Size = New System.Drawing.Size(223, 18)
        Me.tb_particular_two.TabIndex = 23
        Me.tb_particular_two.Text = "Particular"
        Me.tb_particular_two.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'tb_particular_two_pc_from
        '
        Me.tb_particular_two_pc_from.BackColor = System.Drawing.Color.FromArgb(CType(CType(36, Byte), Integer), CType(CType(48, Byte), Integer), CType(CType(59, Byte), Integer))
        Me.tb_particular_two_pc_from.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.tb_particular_two_pc_from.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tb_particular_two_pc_from.ForeColor = System.Drawing.Color.White
        Me.tb_particular_two_pc_from.Location = New System.Drawing.Point(49, 75)
        Me.tb_particular_two_pc_from.Name = "tb_particular_two_pc_from"
        Me.tb_particular_two_pc_from.Size = New System.Drawing.Size(200, 18)
        Me.tb_particular_two_pc_from.TabIndex = 30
        Me.tb_particular_two_pc_from.Text = "Date"
        Me.tb_particular_two_pc_from.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label24
        '
        Me.Label24.AutoSize = True
        Me.Label24.Font = New System.Drawing.Font("Segoe UI", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label24.ForeColor = System.Drawing.Color.DarkGray
        Me.Label24.Location = New System.Drawing.Point(40, 55)
        Me.Label24.Margin = New System.Windows.Forms.Padding(5)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(85, 13)
        Me.Label24.TabIndex = 22
        Me.Label24.Text = "Period Covered"
        '
        'Label25
        '
        Me.Label25.AutoSize = True
        Me.Label25.Font = New System.Drawing.Font("Segoe UI", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label25.ForeColor = System.Drawing.Color.DarkGray
        Me.Label25.Location = New System.Drawing.Point(39, 11)
        Me.Label25.Margin = New System.Windows.Forms.Padding(5)
        Me.Label25.Name = "Label25"
        Me.Label25.Size = New System.Drawing.Size(58, 13)
        Me.Label25.TabIndex = 21
        Me.Label25.Text = "Particular"
        '
        'Label26
        '
        Me.Label26.AutoSize = True
        Me.Label26.Font = New System.Drawing.Font("Segoe UI", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label26.ForeColor = System.Drawing.Color.DarkGray
        Me.Label26.Location = New System.Drawing.Point(9, 78)
        Me.Label26.Name = "Label26"
        Me.Label26.Size = New System.Drawing.Size(33, 13)
        Me.Label26.TabIndex = 26
        Me.Label26.Text = "From"
        '
        'Label27
        '
        Me.Label27.AutoSize = True
        Me.Label27.Font = New System.Drawing.Font("Segoe UI", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label27.ForeColor = System.Drawing.Color.DarkGray
        Me.Label27.Location = New System.Drawing.Point(9, 100)
        Me.Label27.Name = "Label27"
        Me.Label27.Size = New System.Drawing.Size(19, 13)
        Me.Label27.TabIndex = 27
        Me.Label27.Text = "To"
        '
        'Panel2
        '
        Me.Panel2.BackColor = System.Drawing.Color.FromArgb(CType(CType(36, Byte), Integer), CType(CType(48, Byte), Integer), CType(CType(59, Byte), Integer))
        Me.Panel2.Controls.Add(Me.tb_particular_three_pc_to)
        Me.Panel2.Controls.Add(Me.tb_particular_three_pc_from)
        Me.Panel2.Controls.Add(Me.tb_particular_three)
        Me.Panel2.Controls.Add(Me.Label20)
        Me.Panel2.Controls.Add(Me.Label21)
        Me.Panel2.Controls.Add(Me.Label22)
        Me.Panel2.Controls.Add(Me.Label23)
        Me.Panel2.Location = New System.Drawing.Point(979, 164)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(259, 133)
        Me.Panel2.TabIndex = 139
        '
        'tb_particular_three_pc_to
        '
        Me.tb_particular_three_pc_to.BackColor = System.Drawing.Color.FromArgb(CType(CType(36, Byte), Integer), CType(CType(48, Byte), Integer), CType(CType(59, Byte), Integer))
        Me.tb_particular_three_pc_to.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.tb_particular_three_pc_to.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tb_particular_three_pc_to.ForeColor = System.Drawing.Color.White
        Me.tb_particular_three_pc_to.Location = New System.Drawing.Point(48, 97)
        Me.tb_particular_three_pc_to.Name = "tb_particular_three_pc_to"
        Me.tb_particular_three_pc_to.Size = New System.Drawing.Size(200, 18)
        Me.tb_particular_three_pc_to.TabIndex = 31
        Me.tb_particular_three_pc_to.Text = "Date"
        Me.tb_particular_three_pc_to.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'tb_particular_three_pc_from
        '
        Me.tb_particular_three_pc_from.BackColor = System.Drawing.Color.FromArgb(CType(CType(36, Byte), Integer), CType(CType(48, Byte), Integer), CType(CType(59, Byte), Integer))
        Me.tb_particular_three_pc_from.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.tb_particular_three_pc_from.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tb_particular_three_pc_from.ForeColor = System.Drawing.Color.White
        Me.tb_particular_three_pc_from.Location = New System.Drawing.Point(49, 75)
        Me.tb_particular_three_pc_from.Name = "tb_particular_three_pc_from"
        Me.tb_particular_three_pc_from.Size = New System.Drawing.Size(200, 18)
        Me.tb_particular_three_pc_from.TabIndex = 30
        Me.tb_particular_three_pc_from.Text = "Date"
        Me.tb_particular_three_pc_from.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'tb_particular_three
        '
        Me.tb_particular_three.BackColor = System.Drawing.Color.FromArgb(CType(CType(36, Byte), Integer), CType(CType(48, Byte), Integer), CType(CType(59, Byte), Integer))
        Me.tb_particular_three.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.tb_particular_three.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tb_particular_three.ForeColor = System.Drawing.Color.White
        Me.tb_particular_three.Location = New System.Drawing.Point(19, 26)
        Me.tb_particular_three.Margin = New System.Windows.Forms.Padding(5)
        Me.tb_particular_three.Name = "tb_particular_three"
        Me.tb_particular_three.Size = New System.Drawing.Size(223, 18)
        Me.tb_particular_three.TabIndex = 23
        Me.tb_particular_three.Text = "Particular"
        Me.tb_particular_three.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.Font = New System.Drawing.Font("Segoe UI", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label20.ForeColor = System.Drawing.Color.DarkGray
        Me.Label20.Location = New System.Drawing.Point(40, 55)
        Me.Label20.Margin = New System.Windows.Forms.Padding(5)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(85, 13)
        Me.Label20.TabIndex = 22
        Me.Label20.Text = "Period Covered"
        '
        'Label21
        '
        Me.Label21.AutoSize = True
        Me.Label21.Font = New System.Drawing.Font("Segoe UI", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label21.ForeColor = System.Drawing.Color.DarkGray
        Me.Label21.Location = New System.Drawing.Point(39, 11)
        Me.Label21.Margin = New System.Windows.Forms.Padding(5)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(58, 13)
        Me.Label21.TabIndex = 21
        Me.Label21.Text = "Particular"
        '
        'Label22
        '
        Me.Label22.AutoSize = True
        Me.Label22.Font = New System.Drawing.Font("Segoe UI", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label22.ForeColor = System.Drawing.Color.DarkGray
        Me.Label22.Location = New System.Drawing.Point(9, 78)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(33, 13)
        Me.Label22.TabIndex = 26
        Me.Label22.Text = "From"
        '
        'Label23
        '
        Me.Label23.AutoSize = True
        Me.Label23.Font = New System.Drawing.Font("Segoe UI", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label23.ForeColor = System.Drawing.Color.DarkGray
        Me.Label23.Location = New System.Drawing.Point(9, 100)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(19, 13)
        Me.Label23.TabIndex = 27
        Me.Label23.Text = "To"
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.FromArgb(CType(CType(36, Byte), Integer), CType(CType(48, Byte), Integer), CType(CType(59, Byte), Integer))
        Me.Panel1.Controls.Add(Me.tb_particular_one_pc_to)
        Me.Panel1.Controls.Add(Me.tb_particular_one_pc_from)
        Me.Panel1.Controls.Add(Me.tb_particular_one)
        Me.Panel1.Controls.Add(Me.Label17)
        Me.Panel1.Controls.Add(Me.Label18)
        Me.Panel1.Controls.Add(Me.Label13)
        Me.Panel1.Controls.Add(Me.Label19)
        Me.Panel1.Location = New System.Drawing.Point(382, 164)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(259, 133)
        Me.Panel1.TabIndex = 137
        '
        'tb_particular_one_pc_to
        '
        Me.tb_particular_one_pc_to.BackColor = System.Drawing.Color.FromArgb(CType(CType(36, Byte), Integer), CType(CType(48, Byte), Integer), CType(CType(59, Byte), Integer))
        Me.tb_particular_one_pc_to.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.tb_particular_one_pc_to.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tb_particular_one_pc_to.ForeColor = System.Drawing.Color.White
        Me.tb_particular_one_pc_to.Location = New System.Drawing.Point(49, 97)
        Me.tb_particular_one_pc_to.Name = "tb_particular_one_pc_to"
        Me.tb_particular_one_pc_to.Size = New System.Drawing.Size(200, 18)
        Me.tb_particular_one_pc_to.TabIndex = 29
        Me.tb_particular_one_pc_to.Text = "Date"
        Me.tb_particular_one_pc_to.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'tb_particular_one_pc_from
        '
        Me.tb_particular_one_pc_from.BackColor = System.Drawing.Color.FromArgb(CType(CType(36, Byte), Integer), CType(CType(48, Byte), Integer), CType(CType(59, Byte), Integer))
        Me.tb_particular_one_pc_from.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.tb_particular_one_pc_from.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tb_particular_one_pc_from.ForeColor = System.Drawing.Color.White
        Me.tb_particular_one_pc_from.Location = New System.Drawing.Point(49, 75)
        Me.tb_particular_one_pc_from.Name = "tb_particular_one_pc_from"
        Me.tb_particular_one_pc_from.Size = New System.Drawing.Size(200, 18)
        Me.tb_particular_one_pc_from.TabIndex = 28
        Me.tb_particular_one_pc_from.Text = "Date"
        Me.tb_particular_one_pc_from.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'tb_particular_one
        '
        Me.tb_particular_one.BackColor = System.Drawing.Color.FromArgb(CType(CType(36, Byte), Integer), CType(CType(48, Byte), Integer), CType(CType(59, Byte), Integer))
        Me.tb_particular_one.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.tb_particular_one.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tb_particular_one.ForeColor = System.Drawing.Color.White
        Me.tb_particular_one.Location = New System.Drawing.Point(19, 26)
        Me.tb_particular_one.Margin = New System.Windows.Forms.Padding(5)
        Me.tb_particular_one.Name = "tb_particular_one"
        Me.tb_particular_one.Size = New System.Drawing.Size(223, 18)
        Me.tb_particular_one.TabIndex = 23
        Me.tb_particular_one.Text = "Particular"
        Me.tb_particular_one.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Font = New System.Drawing.Font("Segoe UI", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label17.ForeColor = System.Drawing.Color.DarkGray
        Me.Label17.Location = New System.Drawing.Point(40, 55)
        Me.Label17.Margin = New System.Windows.Forms.Padding(5)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(85, 13)
        Me.Label17.TabIndex = 22
        Me.Label17.Text = "Period Covered"
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Font = New System.Drawing.Font("Segoe UI", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label18.ForeColor = System.Drawing.Color.DarkGray
        Me.Label18.Location = New System.Drawing.Point(39, 11)
        Me.Label18.Margin = New System.Windows.Forms.Padding(5)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(58, 13)
        Me.Label18.TabIndex = 21
        Me.Label18.Text = "Particular"
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Font = New System.Drawing.Font("Segoe UI", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label13.ForeColor = System.Drawing.Color.DarkGray
        Me.Label13.Location = New System.Drawing.Point(9, 78)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(33, 13)
        Me.Label13.TabIndex = 26
        Me.Label13.Text = "From"
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.Font = New System.Drawing.Font("Segoe UI", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label19.ForeColor = System.Drawing.Color.DarkGray
        Me.Label19.Location = New System.Drawing.Point(9, 100)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(19, 13)
        Me.Label19.TabIndex = 27
        Me.Label19.Text = "To"
        '
        'dgv_soa_data
        '
        Me.dgv_soa_data.AllowUserToAddRows = False
        Me.dgv_soa_data.AllowUserToDeleteRows = False
        Me.dgv_soa_data.AllowUserToResizeColumns = False
        Me.dgv_soa_data.AllowUserToResizeRows = False
        Me.dgv_soa_data.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.dgv_soa_data.BackgroundColor = System.Drawing.Color.FromArgb(CType(CType(28, Byte), Integer), CType(CType(40, Byte), Integer), CType(CType(52, Byte), Integer))
        Me.dgv_soa_data.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.dgv_soa_data.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.[Single]
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.ControlDarkDark
        DataGridViewCellStyle3.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgv_soa_data.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle3
        Me.dgv_soa_data.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle4.BackColor = System.Drawing.Color.FromArgb(CType(CType(28, Byte), Integer), CType(CType(40, Byte), Integer), CType(CType(52, Byte), Integer))
        DataGridViewCellStyle4.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle4.ForeColor = System.Drawing.Color.White
        DataGridViewCellStyle4.SelectionBackColor = System.Drawing.Color.FromArgb(CType(CType(181, Byte), Integer), CType(CType(186, Byte), Integer), CType(CType(188, Byte), Integer))
        DataGridViewCellStyle4.SelectionForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgv_soa_data.DefaultCellStyle = DataGridViewCellStyle4
        Me.dgv_soa_data.GridColor = System.Drawing.Color.FromArgb(CType(CType(28, Byte), Integer), CType(CType(40, Byte), Integer), CType(CType(52, Byte), Integer))
        Me.dgv_soa_data.Location = New System.Drawing.Point(12, 286)
        Me.dgv_soa_data.Margin = New System.Windows.Forms.Padding(0)
        Me.dgv_soa_data.MultiSelect = False
        Me.dgv_soa_data.Name = "dgv_soa_data"
        Me.dgv_soa_data.ReadOnly = True
        Me.dgv_soa_data.RowHeadersVisible = False
        Me.dgv_soa_data.RowTemplate.DefaultCellStyle.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dgv_soa_data.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgv_soa_data.Size = New System.Drawing.Size(1329, 309)
        Me.dgv_soa_data.TabIndex = 136
        '
        'tb_message
        '
        Me.tb_message.BackColor = System.Drawing.Color.FromArgb(CType(CType(44, Byte), Integer), CType(CType(62, Byte), Integer), CType(CType(80, Byte), Integer))
        Me.tb_message.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.tb_message.Font = New System.Drawing.Font("Segoe UI", 12.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tb_message.ForeColor = System.Drawing.Color.Lime
        Me.tb_message.Location = New System.Drawing.Point(178, 666)
        Me.tb_message.MaxLength = 25
        Me.tb_message.Name = "tb_message"
        Me.tb_message.Size = New System.Drawing.Size(237, 22)
        Me.tb_message.TabIndex = 140
        '
        'tb_payor
        '
        Me.tb_payor.BackColor = System.Drawing.Color.FromArgb(CType(CType(114, Byte), Integer), CType(CType(126, Byte), Integer), CType(CType(138, Byte), Integer))
        Me.tb_payor.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.tb_payor.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tb_payor.ForeColor = System.Drawing.Color.LightGray
        Me.tb_payor.Location = New System.Drawing.Point(115, 40)
        Me.tb_payor.MaxLength = 120
        Me.tb_payor.Name = "tb_payor"
        Me.tb_payor.ReadOnly = True
        Me.tb_payor.Size = New System.Drawing.Size(599, 22)
        Me.tb_payor.TabIndex = 150
        Me.tb_payor.TabStop = False
        Me.tb_payor.Text = "                                                                     "
        Me.tb_payor.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Font = New System.Drawing.Font("Segoe UI", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label14.ForeColor = System.Drawing.Color.DarkGray
        Me.Label14.Location = New System.Drawing.Point(817, 45)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(50, 13)
        Me.Label14.TabIndex = 149
        Me.Label14.Text = "SOA No."
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Segoe UI", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ForeColor = System.Drawing.Color.DarkGray
        Me.Label4.Location = New System.Drawing.Point(59, 134)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(43, 13)
        Me.Label4.TabIndex = 148
        Me.Label4.Text = "Service"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Segoe UI", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ForeColor = System.Drawing.Color.DarkGray
        Me.Label3.Location = New System.Drawing.Point(57, 105)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(45, 13)
        Me.Label3.TabIndex = 147
        Me.Label3.Text = "Process"
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Font = New System.Drawing.Font("Segoe UI", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label15.ForeColor = System.Drawing.Color.DarkGray
        Me.Label15.Location = New System.Drawing.Point(34, 75)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(68, 13)
        Me.Label15.TabIndex = 146
        Me.Label15.Text = "Transaction"
        '
        'tb_serial_num
        '
        Me.tb_serial_num.BackColor = System.Drawing.Color.FromArgb(CType(CType(44, Byte), Integer), CType(CType(62, Byte), Integer), CType(CType(80, Byte), Integer))
        Me.tb_serial_num.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.tb_serial_num.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tb_serial_num.ForeColor = System.Drawing.Color.White
        Me.tb_serial_num.Location = New System.Drawing.Point(886, 42)
        Me.tb_serial_num.Name = "tb_serial_num"
        Me.tb_serial_num.ReadOnly = True
        Me.tb_serial_num.Size = New System.Drawing.Size(246, 18)
        Me.tb_serial_num.TabIndex = 145
        Me.tb_serial_num.Text = "yyyy-MM-xxxx"
        '
        'tb_service_type
        '
        Me.tb_service_type.BackColor = System.Drawing.Color.FromArgb(CType(CType(44, Byte), Integer), CType(CType(62, Byte), Integer), CType(CType(80, Byte), Integer))
        Me.tb_service_type.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.tb_service_type.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tb_service_type.ForeColor = System.Drawing.Color.White
        Me.tb_service_type.Location = New System.Drawing.Point(115, 131)
        Me.tb_service_type.Name = "tb_service_type"
        Me.tb_service_type.ReadOnly = True
        Me.tb_service_type.Size = New System.Drawing.Size(387, 18)
        Me.tb_service_type.TabIndex = 144
        Me.tb_service_type.Text = "xx, xx"
        '
        'tb_process_type
        '
        Me.tb_process_type.BackColor = System.Drawing.Color.FromArgb(CType(CType(44, Byte), Integer), CType(CType(62, Byte), Integer), CType(CType(80, Byte), Integer))
        Me.tb_process_type.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.tb_process_type.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tb_process_type.ForeColor = System.Drawing.Color.White
        Me.tb_process_type.Location = New System.Drawing.Point(115, 102)
        Me.tb_process_type.Name = "tb_process_type"
        Me.tb_process_type.ReadOnly = True
        Me.tb_process_type.Size = New System.Drawing.Size(184, 18)
        Me.tb_process_type.TabIndex = 143
        Me.tb_process_type.Text = "xx, xx"
        '
        'tb_transact_type
        '
        Me.tb_transact_type.BackColor = System.Drawing.Color.FromArgb(CType(CType(44, Byte), Integer), CType(CType(62, Byte), Integer), CType(CType(80, Byte), Integer))
        Me.tb_transact_type.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.tb_transact_type.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tb_transact_type.ForeColor = System.Drawing.Color.White
        Me.tb_transact_type.Location = New System.Drawing.Point(115, 72)
        Me.tb_transact_type.Name = "tb_transact_type"
        Me.tb_transact_type.ReadOnly = True
        Me.tb_transact_type.Size = New System.Drawing.Size(152, 18)
        Me.tb_transact_type.TabIndex = 142
        Me.tb_transact_type.Text = "xxxxx"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Segoe UI", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.DarkGray
        Me.Label2.Location = New System.Drawing.Point(62, 45)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(37, 13)
        Me.Label2.TabIndex = 141
        Me.Label2.Text = "Payor"
        '
        'Panel12
        '
        Me.Panel12.BackColor = System.Drawing.Color.FromArgb(CType(CType(28, Byte), Integer), CType(CType(40, Byte), Integer), CType(CType(52, Byte), Integer))
        Me.Panel12.Controls.Add(Me.btn_action_two)
        Me.Panel12.Controls.Add(Me.btn_action_one)
        Me.Panel12.Location = New System.Drawing.Point(12, 601)
        Me.Panel12.Name = "Panel12"
        Me.Panel12.Size = New System.Drawing.Size(252, 59)
        Me.Panel12.TabIndex = 152
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.White
        Me.Label1.Location = New System.Drawing.Point(44, 9)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(34, 17)
        Me.Label1.TabIndex = 153
        Me.Label1.Text = "PHP"
        '
        'Panel4
        '
        Me.Panel4.BackColor = System.Drawing.Color.FromArgb(CType(CType(28, Byte), Integer), CType(CType(40, Byte), Integer), CType(CType(52, Byte), Integer))
        Me.Panel4.Controls.Add(Me.tb_total)
        Me.Panel4.Controls.Add(Me.Label1)
        Me.Panel4.Controls.Add(Me.Label16)
        Me.Panel4.Location = New System.Drawing.Point(1049, 595)
        Me.Panel4.Margin = New System.Windows.Forms.Padding(0)
        Me.Panel4.Name = "Panel4"
        Me.Panel4.Size = New System.Drawing.Size(292, 35)
        Me.Panel4.TabIndex = 154
        '
        'pnl_attachments
        '
        Me.pnl_attachments.BackColor = System.Drawing.Color.FromArgb(CType(CType(28, Byte), Integer), CType(CType(40, Byte), Integer), CType(CType(52, Byte), Integer))
        Me.pnl_attachments.Controls.Add(Me.btn_attachment)
        Me.pnl_attachments.Location = New System.Drawing.Point(282, 601)
        Me.pnl_attachments.Name = "pnl_attachments"
        Me.pnl_attachments.Size = New System.Drawing.Size(133, 59)
        Me.pnl_attachments.TabIndex = 155
        Me.pnl_attachments.Visible = False
        '
        'btn_attachment
        '
        Me.btn_attachment.BackColor = System.Drawing.Color.FromArgb(CType(CType(218, Byte), Integer), CType(CType(144, Byte), Integer), CType(CType(27, Byte), Integer))
        Me.btn_attachment.FlatAppearance.BorderSize = 0
        Me.btn_attachment.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_attachment.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_attachment.ForeColor = System.Drawing.Color.White
        Me.btn_attachment.Location = New System.Drawing.Point(12, 11)
        Me.btn_attachment.Name = "btn_attachment"
        Me.btn_attachment.Size = New System.Drawing.Size(110, 35)
        Me.btn_attachment.TabIndex = 105
        Me.btn_attachment.Text = "Attachments"
        Me.btn_attachment.UseVisualStyleBackColor = False
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Segoe UI", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.ForeColor = System.Drawing.Color.DarkGray
        Me.Label9.Location = New System.Drawing.Point(734, 105)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(133, 13)
        Me.Label9.TabIndex = 157
        Me.Label9.Text = "Date and Time Modified"
        '
        'tb_date_modified
        '
        Me.tb_date_modified.BackColor = System.Drawing.Color.FromArgb(CType(CType(44, Byte), Integer), CType(CType(62, Byte), Integer), CType(CType(80, Byte), Integer))
        Me.tb_date_modified.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.tb_date_modified.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tb_date_modified.ForeColor = System.Drawing.Color.White
        Me.tb_date_modified.Location = New System.Drawing.Point(886, 102)
        Me.tb_date_modified.Name = "tb_date_modified"
        Me.tb_date_modified.ReadOnly = True
        Me.tb_date_modified.Size = New System.Drawing.Size(445, 18)
        Me.tb_date_modified.TabIndex = 156
        Me.tb_date_modified.Text = "yyyy-MM-dd"
        '
        'frm_review_soa
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(44, Byte), Integer), CType(CType(62, Byte), Integer), CType(CType(80, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(1350, 696)
        Me.Controls.Add(Me.Label9)
        Me.Controls.Add(Me.tb_date_modified)
        Me.Controls.Add(Me.pnl_attachments)
        Me.Controls.Add(Me.dgv_soa_data)
        Me.Controls.Add(Me.Panel4)
        Me.Controls.Add(Me.Panel12)
        Me.Controls.Add(Me.tb_payor)
        Me.Controls.Add(Me.Label14)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label15)
        Me.Controls.Add(Me.tb_serial_num)
        Me.Controls.Add(Me.tb_service_type)
        Me.Controls.Add(Me.tb_process_type)
        Me.Controls.Add(Me.tb_transact_type)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.tb_message)
        Me.Controls.Add(Me.Panel3)
        Me.Controls.Add(Me.Panel2)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.tb_approved)
        Me.Controls.Add(Me.tb_prepared_by)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.tb_expiration)
        Me.Controls.Add(Me.tb_date_created)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.RadTitleBar1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.MaximizeBox = False
        Me.MaximumSize = New System.Drawing.Size(1350, 696)
        Me.MinimizeBox = False
        Me.MinimumSize = New System.Drawing.Size(1350, 696)
        Me.Name = "frm_review_soa"
        Me.ShowIcon = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "NTC Region 10 "
        CType(Me.RadTitleBar1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel3.ResumeLayout(False)
        Me.Panel3.PerformLayout()
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        CType(Me.dgv_soa_data, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel12.ResumeLayout(False)
        Me.Panel4.ResumeLayout(False)
        Me.Panel4.PerformLayout()
        Me.pnl_attachments.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents tb_approved As TextBox
    Friend WithEvents tb_prepared_by As TextBox
    Friend WithEvents btn_action_two As Button
    Friend WithEvents btn_action_one As Button
    Friend WithEvents Label16 As Label
    Friend WithEvents tb_total As TextBox
    Friend WithEvents Label6 As Label
    Friend WithEvents Label5 As Label
    Friend WithEvents tb_expiration As TextBox
    Friend WithEvents tb_date_created As TextBox
    Friend WithEvents Label8 As Label
    Friend WithEvents Label7 As Label
    Friend WithEvents RadTitleBar1 As Telerik.WinControls.UI.RadTitleBar
    Friend WithEvents Panel3 As Panel
    Friend WithEvents tb_particular_two_pc_to As TextBox
    Friend WithEvents tb_particular_two As TextBox
    Friend WithEvents tb_particular_two_pc_from As TextBox
    Friend WithEvents Label24 As Label
    Friend WithEvents Label25 As Label
    Friend WithEvents Label26 As Label
    Friend WithEvents Label27 As Label
    Friend WithEvents Panel2 As Panel
    Friend WithEvents tb_particular_three_pc_to As TextBox
    Friend WithEvents tb_particular_three_pc_from As TextBox
    Friend WithEvents tb_particular_three As TextBox
    Friend WithEvents Label20 As Label
    Friend WithEvents Label21 As Label
    Friend WithEvents Label22 As Label
    Friend WithEvents Label23 As Label
    Friend WithEvents Panel1 As Panel
    Friend WithEvents tb_particular_one_pc_to As TextBox
    Friend WithEvents tb_particular_one_pc_from As TextBox
    Friend WithEvents tb_particular_one As TextBox
    Friend WithEvents Label17 As Label
    Friend WithEvents Label18 As Label
    Friend WithEvents Label13 As Label
    Friend WithEvents Label19 As Label
    Friend WithEvents dgv_soa_data As DataGridView
    Friend WithEvents tb_message As TextBox
    Friend WithEvents tb_payor As TextBox
    Friend WithEvents Label14 As Label
    Friend WithEvents Label4 As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents Label15 As Label
    Friend WithEvents tb_serial_num As TextBox
    Friend WithEvents tb_service_type As TextBox
    Friend WithEvents tb_process_type As TextBox
    Friend WithEvents tb_transact_type As TextBox
    Friend WithEvents Label2 As Label
    Friend WithEvents Panel12 As Panel
    Friend WithEvents Label1 As Label
    Friend WithEvents Panel4 As Panel
    Friend WithEvents pnl_attachments As Panel
    Friend WithEvents btn_attachment As Button
    Friend WithEvents Label9 As Label
    Friend WithEvents tb_date_modified As TextBox
End Class
