﻿Imports MySql.Data.MySqlClient

Public Class frm_approver_list
    Dim query As String
    Dim cmd As MySqlCommand
    Dim reader As MySqlDataReader
    Public soa_id_transact, user_id_login As Integer
    Public user_id, serial_id As String

    Private Sub authorizedFrm_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        initialize_table()
        load_approval_personnel()
    End Sub

    Private Sub dgv_approve_CellDoubleClick(sender As Object, e As DataGridViewCellEventArgs) Handles _
        dgv_approve.CellDoubleClick
        ' Set approver to verify and confirm SOA.
        Try
            If e.RowIndex >= 0 Then
                Dim row As DataGridViewRow
                Dim user_id_approver As String
                Dim notify_title = "A SOA needs your approval"
                Dim notify_body = "SOA Serial No. " & serial_id & " needs your approval. Click the button below to view."

                row = dgv_approve.Rows(e.RowIndex)
                user_id_approver = row.Cells("ID").Value.ToString

                ' Update chosen personnel_approve.
                class_connection.con.Open()
                query = "update tbl_soa_transact Set user_id_approve = '" & user_id_approver & "' where soa_id = '" &
                    soa_id_transact.ToString + "'"
                cmd = New MySqlCommand(query, class_connection.con)
                cmd.ExecuteNonQuery()
                class_connection.con.Close()

                ' Send approval notification to chosen approver.
                class_connection.con.Open()
                query = "insert into tbl_notifications(message_title, message_content, transaction_id, user_id_notified, " &
                    "user_id_notifier, user_opened, date_created, action_taken, message_type) Values('" & notify_title & "', " &
                    "'" & notify_body & "', '" & soa_id_transact.ToString & "', '" & user_id_approver & "', '" &
                    user_id_login.ToString & "', '0', '" & Date.Now.ToString("yyyy-MM-dd HH:mm:ss") & "', '0', '1')"
                cmd = New MySqlCommand(query, class_connection.con)
                cmd.ExecuteNonQuery()
                class_connection.con.Close()

                ' Bring from display SOA to front.
                frm_licenser_log.pb_approve_status.Image = My.Resources.More_36px
                frm_licenser_log.tb_approved.Text = row.Cells("Name").Value.ToString
                frm_licenser_log.pb_approve_status.Visible = True
                frm_licenser_log.btn_add_approve.Enabled = False
                frm_licenser_log.btn_add_approve.Cursor = Cursors.No
                frm_licenser_log.btn_edit.Enabled = False
                frm_licenser_log.Show()

                Close()
            End If
        Catch ex As MySqlException
            MsgBox("Couldn't connect to server.", MsgBoxStyle.Exclamation, "NTC Region 10")
            class_connection.con.Close()
        Catch ex As Exception
            MsgBox(ex.Message + ": " + ex.GetType.ToString)
            class_connection.con.Close()
        End Try
    End Sub

    Sub load_approval_personnel()
        Try
            ' Populate datagridview with approver officers.
            class_connection.con.Open()
            query = "select user_id as 'ID', full_name as 'Name' from tbl_user where authority_level = '4'"
            cmd = New MySqlCommand(query, class_connection.con)
            reader = cmd.ExecuteReader()

            While reader.Read
                If reader.GetString("ID") <> user_id Then
                    Dim row As String() = New String() {reader.GetString("ID"), reader.GetString("Name")}
                    dgv_approve.Rows.Add(row)
                Else
                    user_id_login = reader.GetString("ID")
                End If
            End While
            class_connection.con.Close()
        Catch ex As MySqlException
            MsgBox("Couldn't connect to server.", MsgBoxStyle.Exclamation, "NTC Region 10")
            class_connection.con.Close()
        Catch ex As Exception
            MsgBox(ex.Message + ": " + ex.GetType.ToString)
            class_connection.con.Close()
        End Try
    End Sub

    Sub initialize_table()
        dgv_approve.ColumnCount = 2
        dgv_approve.Columns(0).Name = "ID"
        dgv_approve.Columns(0).Width = 30
        dgv_approve.Columns(0).Visible = False
        dgv_approve.Columns(0).ReadOnly = True
        dgv_approve.Columns(1).Name = "Name"
        dgv_approve.Columns(1).Width = 160
        dgv_approve.Columns(1).ReadOnly = True
        dgv_approve.Rows.Clear()
    End Sub
End Class