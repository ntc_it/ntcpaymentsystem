﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frm_cashier_log
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle9 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle10 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle11 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle12 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.RadTitleBar1 = New Telerik.WinControls.UI.RadTitleBar()
        Me.cbCheck = New System.Windows.Forms.CheckBox()
        Me.pnlCheck = New System.Windows.Forms.Panel()
        Me.tb_check_date = New System.Windows.Forms.TextBox()
        Me.tb_check_order = New System.Windows.Forms.TextBox()
        Me.RadLabel23 = New Telerik.WinControls.UI.RadLabel()
        Me.RadLabel22 = New Telerik.WinControls.UI.RadLabel()
        Me.cbCash = New System.Windows.Forms.CheckBox()
        Me.txtAmountInWords = New System.Windows.Forms.TextBox()
        Me.Panel12 = New System.Windows.Forms.Panel()
        Me.btn_print = New System.Windows.Forms.Button()
        Me.btn_soa = New System.Windows.Forms.Button()
        Me.btn_op = New System.Windows.Forms.Button()
        Me.RadLabel12 = New Telerik.WinControls.UI.RadLabel()
        Me.tb_total = New System.Windows.Forms.TextBox()
        Me.RadLabel21 = New Telerik.WinControls.UI.RadLabel()
        Me.pnlCheck2 = New System.Windows.Forms.Panel()
        Me.dgv_cashier = New System.Windows.Forms.DataGridView()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtOR = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.tb_payor = New System.Windows.Forms.TextBox()
        Me.RadLabel1 = New Telerik.WinControls.UI.RadLabel()
        Me.tb_date = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.RadLabel2 = New Telerik.WinControls.UI.RadLabel()
        Me.cb_cashier_signature = New System.Windows.Forms.CheckBox()
        CType(Me.RadTitleBar1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlCheck.SuspendLayout()
        CType(Me.RadLabel23, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RadLabel22, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel12.SuspendLayout()
        CType(Me.RadLabel12, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RadLabel21, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgv_cashier, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RadLabel1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        CType(Me.RadLabel2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'RadTitleBar1
        '
        Me.RadTitleBar1.Dock = System.Windows.Forms.DockStyle.Top
        Me.RadTitleBar1.Location = New System.Drawing.Point(0, 0)
        Me.RadTitleBar1.Name = "RadTitleBar1"
        Me.RadTitleBar1.Size = New System.Drawing.Size(1020, 23)
        Me.RadTitleBar1.TabIndex = 2
        Me.RadTitleBar1.TabStop = False
        Me.RadTitleBar1.Text = "NTC Region 10"
        '
        'cbCheck
        '
        Me.cbCheck.AutoSize = True
        Me.cbCheck.Font = New System.Drawing.Font("Segoe UI", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbCheck.ForeColor = System.Drawing.Color.White
        Me.cbCheck.Location = New System.Drawing.Point(326, 482)
        Me.cbCheck.Name = "cbCheck"
        Me.cbCheck.Size = New System.Drawing.Size(87, 17)
        Me.cbCheck.TabIndex = 123
        Me.cbCheck.Text = "Check/PMO"
        Me.cbCheck.UseVisualStyleBackColor = True
        '
        'pnlCheck
        '
        Me.pnlCheck.BackColor = System.Drawing.Color.FromArgb(CType(CType(114, Byte), Integer), CType(CType(126, Byte), Integer), CType(CType(138, Byte), Integer))
        Me.pnlCheck.Controls.Add(Me.tb_check_date)
        Me.pnlCheck.Controls.Add(Me.tb_check_order)
        Me.pnlCheck.Controls.Add(Me.RadLabel23)
        Me.pnlCheck.Location = New System.Drawing.Point(36, 509)
        Me.pnlCheck.Name = "pnlCheck"
        Me.pnlCheck.Size = New System.Drawing.Size(413, 71)
        Me.pnlCheck.TabIndex = 125
        Me.pnlCheck.Visible = False
        '
        'tb_check_date
        '
        Me.tb_check_date.BackColor = System.Drawing.Color.FromArgb(CType(CType(114, Byte), Integer), CType(CType(126, Byte), Integer), CType(CType(138, Byte), Integer))
        Me.tb_check_date.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.tb_check_date.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tb_check_date.ForeColor = System.Drawing.Color.White
        Me.tb_check_date.Location = New System.Drawing.Point(274, 34)
        Me.tb_check_date.MaxLength = 40
        Me.tb_check_date.Name = "tb_check_date"
        Me.tb_check_date.ReadOnly = True
        Me.tb_check_date.Size = New System.Drawing.Size(123, 22)
        Me.tb_check_date.TabIndex = 91
        Me.tb_check_date.Text = "yyyy-MM-dd"
        '
        'tb_check_order
        '
        Me.tb_check_order.BackColor = System.Drawing.Color.FromArgb(CType(CType(114, Byte), Integer), CType(CType(126, Byte), Integer), CType(CType(138, Byte), Integer))
        Me.tb_check_order.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.tb_check_order.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tb_check_order.ForeColor = System.Drawing.Color.White
        Me.tb_check_order.Location = New System.Drawing.Point(13, 34)
        Me.tb_check_order.MaxLength = 40
        Me.tb_check_order.Name = "tb_check_order"
        Me.tb_check_order.ReadOnly = True
        Me.tb_check_order.Size = New System.Drawing.Size(255, 22)
        Me.tb_check_order.TabIndex = 89
        Me.tb_check_order.Text = "1234-1234-1234-1234"
        '
        'RadLabel23
        '
        Me.RadLabel23.Font = New System.Drawing.Font("Segoe UI", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadLabel23.ForeColor = System.Drawing.Color.White
        Me.RadLabel23.Location = New System.Drawing.Point(16, 6)
        Me.RadLabel23.Name = "RadLabel23"
        Me.RadLabel23.Size = New System.Drawing.Size(161, 18)
        Me.RadLabel23.TabIndex = 90
        Me.RadLabel23.Text = "Check/Money Order Number:"
        '
        'RadLabel22
        '
        Me.RadLabel22.Font = New System.Drawing.Font("Segoe UI", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadLabel22.ForeColor = System.Drawing.Color.DarkGray
        Me.RadLabel22.Location = New System.Drawing.Point(36, 483)
        Me.RadLabel22.Name = "RadLabel22"
        Me.RadLabel22.Size = New System.Drawing.Size(99, 18)
        Me.RadLabel22.TabIndex = 124
        Me.RadLabel22.Text = "Mode of Payment"
        '
        'cbCash
        '
        Me.cbCash.AutoSize = True
        Me.cbCash.Font = New System.Drawing.Font("Segoe UI", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbCash.ForeColor = System.Drawing.Color.White
        Me.cbCash.Location = New System.Drawing.Point(228, 482)
        Me.cbCash.Name = "cbCash"
        Me.cbCash.Size = New System.Drawing.Size(52, 17)
        Me.cbCash.TabIndex = 122
        Me.cbCash.Text = "Cash"
        Me.cbCash.UseVisualStyleBackColor = True
        '
        'txtAmountInWords
        '
        Me.txtAmountInWords.BackColor = System.Drawing.Color.FromArgb(CType(CType(114, Byte), Integer), CType(CType(126, Byte), Integer), CType(CType(138, Byte), Integer))
        Me.txtAmountInWords.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtAmountInWords.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtAmountInWords.ForeColor = System.Drawing.Color.White
        Me.txtAmountInWords.Location = New System.Drawing.Point(35, 423)
        Me.txtAmountInWords.MaxLength = 100
        Me.txtAmountInWords.Name = "txtAmountInWords"
        Me.txtAmountInWords.Size = New System.Drawing.Size(632, 18)
        Me.txtAmountInWords.TabIndex = 120
        '
        'Panel12
        '
        Me.Panel12.BackColor = System.Drawing.Color.FromArgb(CType(CType(28, Byte), Integer), CType(CType(40, Byte), Integer), CType(CType(52, Byte), Integer))
        Me.Panel12.Controls.Add(Me.btn_print)
        Me.Panel12.Controls.Add(Me.btn_soa)
        Me.Panel12.Controls.Add(Me.btn_op)
        Me.Panel12.Location = New System.Drawing.Point(628, 509)
        Me.Panel12.Name = "Panel12"
        Me.Panel12.Size = New System.Drawing.Size(380, 64)
        Me.Panel12.TabIndex = 119
        '
        'btn_print
        '
        Me.btn_print.BackColor = System.Drawing.Color.FromArgb(CType(CType(218, Byte), Integer), CType(CType(144, Byte), Integer), CType(CType(27, Byte), Integer))
        Me.btn_print.FlatAppearance.BorderSize = 0
        Me.btn_print.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_print.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_print.ForeColor = System.Drawing.Color.White
        Me.btn_print.Location = New System.Drawing.Point(14, 14)
        Me.btn_print.Name = "btn_print"
        Me.btn_print.Size = New System.Drawing.Size(110, 35)
        Me.btn_print.TabIndex = 140
        Me.btn_print.Text = "Print"
        Me.btn_print.UseVisualStyleBackColor = False
        '
        'btn_soa
        '
        Me.btn_soa.BackColor = System.Drawing.Color.FromArgb(CType(CType(218, Byte), Integer), CType(CType(144, Byte), Integer), CType(CType(27, Byte), Integer))
        Me.btn_soa.FlatAppearance.BorderSize = 0
        Me.btn_soa.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_soa.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_soa.ForeColor = System.Drawing.Color.White
        Me.btn_soa.Location = New System.Drawing.Point(256, 14)
        Me.btn_soa.Name = "btn_soa"
        Me.btn_soa.Size = New System.Drawing.Size(110, 35)
        Me.btn_soa.TabIndex = 139
        Me.btn_soa.Text = "View SOA"
        Me.btn_soa.UseVisualStyleBackColor = False
        '
        'btn_op
        '
        Me.btn_op.BackColor = System.Drawing.Color.FromArgb(CType(CType(218, Byte), Integer), CType(CType(144, Byte), Integer), CType(CType(27, Byte), Integer))
        Me.btn_op.FlatAppearance.BorderSize = 0
        Me.btn_op.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_op.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_op.ForeColor = System.Drawing.Color.White
        Me.btn_op.Location = New System.Drawing.Point(135, 14)
        Me.btn_op.Name = "btn_op"
        Me.btn_op.Size = New System.Drawing.Size(110, 35)
        Me.btn_op.TabIndex = 138
        Me.btn_op.Text = "View OP"
        Me.btn_op.UseVisualStyleBackColor = False
        '
        'RadLabel12
        '
        Me.RadLabel12.Font = New System.Drawing.Font("Segoe UI", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadLabel12.ForeColor = System.Drawing.Color.White
        Me.RadLabel12.Location = New System.Drawing.Point(14, 67)
        Me.RadLabel12.Name = "RadLabel12"
        Me.RadLabel12.Size = New System.Drawing.Size(34, 18)
        Me.RadLabel12.TabIndex = 118
        Me.RadLabel12.Text = "Total"
        '
        'tb_total
        '
        Me.tb_total.BackColor = System.Drawing.Color.FromArgb(CType(CType(28, Byte), Integer), CType(CType(40, Byte), Integer), CType(CType(52, Byte), Integer))
        Me.tb_total.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.tb_total.Font = New System.Drawing.Font("Segoe UI", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tb_total.ForeColor = System.Drawing.Color.White
        Me.tb_total.Location = New System.Drawing.Point(100, 62)
        Me.tb_total.Name = "tb_total"
        Me.tb_total.ReadOnly = True
        Me.tb_total.Size = New System.Drawing.Size(197, 28)
        Me.tb_total.TabIndex = 117
        Me.tb_total.TabStop = False
        Me.tb_total.Text = "0"
        Me.tb_total.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RadLabel21
        '
        Me.RadLabel21.Font = New System.Drawing.Font("Segoe UI", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadLabel21.ForeColor = System.Drawing.Color.DarkGray
        Me.RadLabel21.Location = New System.Drawing.Point(35, 403)
        Me.RadLabel21.Name = "RadLabel21"
        Me.RadLabel21.Size = New System.Drawing.Size(100, 18)
        Me.RadLabel21.TabIndex = 121
        Me.RadLabel21.Text = "Amount in Words"
        '
        'pnlCheck2
        '
        Me.pnlCheck2.BackColor = System.Drawing.Color.FromArgb(CType(CType(114, Byte), Integer), CType(CType(126, Byte), Integer), CType(CType(138, Byte), Integer))
        Me.pnlCheck2.Location = New System.Drawing.Point(303, 474)
        Me.pnlCheck2.Name = "pnlCheck2"
        Me.pnlCheck2.Size = New System.Drawing.Size(146, 82)
        Me.pnlCheck2.TabIndex = 126
        Me.pnlCheck2.Visible = False
        '
        'dgv_cashier
        '
        Me.dgv_cashier.AllowUserToAddRows = False
        Me.dgv_cashier.AllowUserToDeleteRows = False
        Me.dgv_cashier.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.dgv_cashier.BackgroundColor = System.Drawing.Color.FromArgb(CType(CType(28, Byte), Integer), CType(CType(40, Byte), Integer), CType(CType(52, Byte), Integer))
        Me.dgv_cashier.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.dgv_cashier.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None
        DataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle9.BackColor = System.Drawing.Color.FromArgb(CType(CType(52, Byte), Integer), CType(CType(73, Byte), Integer), CType(CType(85, Byte), Integer))
        DataGridViewCellStyle9.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle9.ForeColor = System.Drawing.Color.White
        DataGridViewCellStyle9.SelectionBackColor = System.Drawing.Color.FromArgb(CType(CType(52, Byte), Integer), CType(CType(73, Byte), Integer), CType(CType(85, Byte), Integer))
        DataGridViewCellStyle9.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle9.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgv_cashier.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle9
        Me.dgv_cashier.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgv_cashier.ColumnHeadersVisible = False
        DataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle10.BackColor = System.Drawing.Color.FromArgb(CType(CType(28, Byte), Integer), CType(CType(40, Byte), Integer), CType(CType(52, Byte), Integer))
        DataGridViewCellStyle10.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle10.ForeColor = System.Drawing.Color.White
        DataGridViewCellStyle10.SelectionBackColor = System.Drawing.Color.FromArgb(CType(CType(28, Byte), Integer), CType(CType(40, Byte), Integer), CType(CType(52, Byte), Integer))
        DataGridViewCellStyle10.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle10.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgv_cashier.DefaultCellStyle = DataGridViewCellStyle10
        Me.dgv_cashier.GridColor = System.Drawing.Color.FromArgb(CType(CType(28, Byte), Integer), CType(CType(40, Byte), Integer), CType(CType(52, Byte), Integer))
        Me.dgv_cashier.Location = New System.Drawing.Point(12, 157)
        Me.dgv_cashier.Name = "dgv_cashier"
        Me.dgv_cashier.ReadOnly = True
        Me.dgv_cashier.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None
        DataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle11.BackColor = System.Drawing.Color.FromArgb(CType(CType(52, Byte), Integer), CType(CType(73, Byte), Integer), CType(CType(85, Byte), Integer))
        DataGridViewCellStyle11.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle11.ForeColor = System.Drawing.Color.White
        DataGridViewCellStyle11.SelectionBackColor = System.Drawing.Color.FromArgb(CType(CType(52, Byte), Integer), CType(CType(73, Byte), Integer), CType(CType(85, Byte), Integer))
        DataGridViewCellStyle11.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle11.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgv_cashier.RowHeadersDefaultCellStyle = DataGridViewCellStyle11
        Me.dgv_cashier.RowHeadersVisible = False
        DataGridViewCellStyle12.BackColor = System.Drawing.Color.FromArgb(CType(CType(52, Byte), Integer), CType(CType(73, Byte), Integer), CType(CType(85, Byte), Integer))
        DataGridViewCellStyle12.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle12.ForeColor = System.Drawing.Color.White
        Me.dgv_cashier.RowsDefaultCellStyle = DataGridViewCellStyle12
        Me.dgv_cashier.RowTemplate.DefaultCellStyle.BackColor = System.Drawing.Color.FromArgb(CType(CType(28, Byte), Integer), CType(CType(40, Byte), Integer), CType(CType(52, Byte), Integer))
        Me.dgv_cashier.RowTemplate.DefaultCellStyle.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dgv_cashier.RowTemplate.DefaultCellStyle.ForeColor = System.Drawing.Color.White
        Me.dgv_cashier.Size = New System.Drawing.Size(996, 219)
        Me.dgv_cashier.TabIndex = 116
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Segoe UI", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.DarkGray
        Me.Label1.Location = New System.Drawing.Point(232, 138)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(106, 13)
        Me.Label1.TabIndex = 114
        Me.Label1.Text = "Details of Payment"
        '
        'txtOR
        '
        Me.txtOR.BackColor = System.Drawing.Color.FromArgb(CType(CType(114, Byte), Integer), CType(CType(126, Byte), Integer), CType(CType(138, Byte), Integer))
        Me.txtOR.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtOR.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtOR.ForeColor = System.Drawing.Color.White
        Me.txtOR.Location = New System.Drawing.Point(138, 33)
        Me.txtOR.MaxLength = 20
        Me.txtOR.Name = "txtOR"
        Me.txtOR.ReadOnly = True
        Me.txtOR.Size = New System.Drawing.Size(164, 22)
        Me.txtOR.TabIndex = 108
        Me.txtOR.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Segoe UI", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ForeColor = System.Drawing.Color.DarkGray
        Me.Label4.Location = New System.Drawing.Point(79, 38)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(43, 13)
        Me.Label4.TabIndex = 112
        Me.Label4.Tag = ""
        Me.Label4.Text = "OR No."
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Segoe UI", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ForeColor = System.Drawing.Color.DarkGray
        Me.Label3.Location = New System.Drawing.Point(586, 38)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(126, 13)
        Me.Label3.TabIndex = 111
        Me.Label3.Text = "Date and Time created"
        '
        'tb_payor
        '
        Me.tb_payor.BackColor = System.Drawing.Color.FromArgb(CType(CType(114, Byte), Integer), CType(CType(126, Byte), Integer), CType(CType(138, Byte), Integer))
        Me.tb_payor.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.tb_payor.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tb_payor.ForeColor = System.Drawing.Color.White
        Me.tb_payor.Location = New System.Drawing.Point(138, 97)
        Me.tb_payor.MaxLength = 100
        Me.tb_payor.Name = "tb_payor"
        Me.tb_payor.ReadOnly = True
        Me.tb_payor.Size = New System.Drawing.Size(786, 22)
        Me.tb_payor.TabIndex = 109
        Me.tb_payor.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'RadLabel1
        '
        Me.RadLabel1.Font = New System.Drawing.Font("Segoe UI", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadLabel1.ForeColor = System.Drawing.Color.DarkGray
        Me.RadLabel1.Location = New System.Drawing.Point(85, 99)
        Me.RadLabel1.Name = "RadLabel1"
        Me.RadLabel1.Size = New System.Drawing.Size(37, 18)
        Me.RadLabel1.TabIndex = 110
        Me.RadLabel1.Text = "Payor"
        '
        'tb_date
        '
        Me.tb_date.BackColor = System.Drawing.Color.FromArgb(CType(CType(114, Byte), Integer), CType(CType(126, Byte), Integer), CType(CType(138, Byte), Integer))
        Me.tb_date.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.tb_date.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tb_date.ForeColor = System.Drawing.Color.White
        Me.tb_date.Location = New System.Drawing.Point(715, 33)
        Me.tb_date.MaxLength = 20
        Me.tb_date.Name = "tb_date"
        Me.tb_date.ReadOnly = True
        Me.tb_date.Size = New System.Drawing.Size(209, 22)
        Me.tb_date.TabIndex = 131
        Me.tb_date.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Segoe UI", 12.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.White
        Me.Label2.Location = New System.Drawing.Point(57, 66)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(42, 21)
        Me.Label2.TabIndex = 135
        Me.Label2.Tag = "OR NO."
        Me.Label2.Text = "PHP"
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.FromArgb(CType(CType(28, Byte), Integer), CType(CType(40, Byte), Integer), CType(CType(52, Byte), Integer))
        Me.Panel1.Controls.Add(Me.tb_total)
        Me.Panel1.Controls.Add(Me.Label2)
        Me.Panel1.Controls.Add(Me.RadLabel12)
        Me.Panel1.Location = New System.Drawing.Point(705, 324)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(303, 96)
        Me.Panel1.TabIndex = 136
        '
        'RadLabel2
        '
        Me.RadLabel2.Font = New System.Drawing.Font("Segoe UI", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadLabel2.ForeColor = System.Drawing.Color.DarkGray
        Me.RadLabel2.Location = New System.Drawing.Point(719, 445)
        Me.RadLabel2.Name = "RadLabel2"
        Me.RadLabel2.Size = New System.Drawing.Size(100, 18)
        Me.RadLabel2.TabIndex = 146
        Me.RadLabel2.Text = "Additional option"
        '
        'cb_cashier_signature
        '
        Me.cb_cashier_signature.AutoSize = True
        Me.cb_cashier_signature.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cb_cashier_signature.ForeColor = System.Drawing.Color.White
        Me.cb_cashier_signature.Location = New System.Drawing.Point(744, 472)
        Me.cb_cashier_signature.Name = "cb_cashier_signature"
        Me.cb_cashier_signature.Size = New System.Drawing.Size(139, 17)
        Me.cb_cashier_signature.TabIndex = 145
        Me.cb_cashier_signature.Text = "Use Cashier Signature"
        Me.cb_cashier_signature.UseVisualStyleBackColor = True
        '
        'frm_cashier_log
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(44, Byte), Integer), CType(CType(62, Byte), Integer), CType(CType(80, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(1020, 600)
        Me.Controls.Add(Me.RadLabel2)
        Me.Controls.Add(Me.cb_cashier_signature)
        Me.Controls.Add(Me.txtAmountInWords)
        Me.Controls.Add(Me.tb_date)
        Me.Controls.Add(Me.cbCheck)
        Me.Controls.Add(Me.pnlCheck)
        Me.Controls.Add(Me.RadLabel22)
        Me.Controls.Add(Me.cbCash)
        Me.Controls.Add(Me.Panel12)
        Me.Controls.Add(Me.RadLabel21)
        Me.Controls.Add(Me.pnlCheck2)
        Me.Controls.Add(Me.dgv_cashier)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.txtOR)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.tb_payor)
        Me.Controls.Add(Me.RadLabel1)
        Me.Controls.Add(Me.RadTitleBar1)
        Me.Controls.Add(Me.Panel1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.MaximizeBox = False
        Me.MaximumSize = New System.Drawing.Size(1020, 600)
        Me.MinimizeBox = False
        Me.MinimumSize = New System.Drawing.Size(1020, 600)
        Me.Name = "frm_cashier_log"
        Me.ShowIcon = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "NTC Region 10"
        CType(Me.RadTitleBar1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlCheck.ResumeLayout(False)
        Me.pnlCheck.PerformLayout()
        CType(Me.RadLabel23, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RadLabel22, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel12.ResumeLayout(False)
        CType(Me.RadLabel12, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RadLabel21, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgv_cashier, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RadLabel1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        CType(Me.RadLabel2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents RadTitleBar1 As Telerik.WinControls.UI.RadTitleBar
    Friend WithEvents cbCheck As CheckBox
    Friend WithEvents pnlCheck As Panel
    Friend WithEvents tb_check_order As TextBox
    Friend WithEvents RadLabel23 As Telerik.WinControls.UI.RadLabel
    Friend WithEvents RadLabel22 As Telerik.WinControls.UI.RadLabel
    Friend WithEvents cbCash As CheckBox
    Friend WithEvents txtAmountInWords As TextBox
    Friend WithEvents Panel12 As Panel
    Friend WithEvents RadLabel12 As Telerik.WinControls.UI.RadLabel
    Friend WithEvents tb_total As TextBox
    Friend WithEvents RadLabel21 As Telerik.WinControls.UI.RadLabel
    Friend WithEvents pnlCheck2 As Panel
    Friend WithEvents dgv_cashier As DataGridView
    Friend WithEvents Label1 As Label
    Friend WithEvents txtOR As TextBox
    Friend WithEvents Label4 As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents tb_payor As TextBox
    Friend WithEvents RadLabel1 As Telerik.WinControls.UI.RadLabel
    Friend WithEvents tb_date As TextBox
    Friend WithEvents Label2 As Label
    Friend WithEvents btn_op As Button
    Friend WithEvents btn_soa As Button
    Friend WithEvents Panel1 As Panel
    Friend WithEvents tb_check_date As TextBox
    Friend WithEvents btn_print As Button
    Friend WithEvents RadLabel2 As Telerik.WinControls.UI.RadLabel
    Friend WithEvents cb_cashier_signature As CheckBox
End Class
