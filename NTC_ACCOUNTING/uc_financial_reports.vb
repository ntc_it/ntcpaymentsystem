﻿Imports MySql.Data.MySqlClient
Imports Microsoft.Office.Interop
Imports System.Net

Public Class uc_financial_reports
    Dim query As String
    Dim cmd As MySqlCommand
    Dim reader As MySqlDataReader
    Dim MOP() = {"", "", "", "", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0, 0, 0, 0}

    Private Sub BtnLoad_Click(sender As Object, e As EventArgs) Handles BtnLoad.Click
        tab_one_report()
    End Sub

    Sub tab_one_report()
        Try
            Dim from = DTP1.Value.ToString("yyyy-MM-dd") & " 00:00:00"
            Dim upto = DTP2.Value.ToString("yyyy-MM-dd") & " 23:59:59"
            Dim row As String()
            Dim Or_num As Decimal = 0, tax As Decimal = 0, fees As Decimal = 0

            initialize_collection_datagridview()

            class_connection.con.Open()
            query = "SELECT tbl_soa_cashier.date_transact as 'Date', tbl_soa_cashier.or_num " &
                "as 'O.R. No.', tbl_payor.name as 'Payor', tbl_soa_endorse.purpose_one as " &
                "'Particular1', tbl_soa_endorse.purpose_two as 'Particular2', " &
                "tbl_soa_endorse.payment_amount as 'Total Per O.R. No.', " &
                "tbl_soa_endorse.payment_amount_two as 'Taxes', tbl_soa_endorse.payment_amount_one " &
                "as 'Fees'  from tbl_soa_endorse, tbl_soa_cashier, tbl_payor, tbl_soa_transact, " &
                "tbl_soa_details where tbl_soa_endorse.endorse_id = tbl_soa_cashier.op_id And " &
                "tbl_soa_transact.soa_id = tbl_soa_cashier.soa_id And tbl_soa_transact.soa_details_id " &
                "= tbl_soa_details.soa_details_id And tbl_soa_details.payor_id = tbl_payor.payor_id " &
                "And tbl_soa_cashier.date_transact between '" & from & "' and '" & upto &
                "' order by 'O.R. No.' ASC"
            cmd = New MySqlCommand(query, class_connection.con)
            reader = cmd.ExecuteReader()

            While reader.Read
                row = New String() {reader.GetDateTime("Date").ToString("MM/dd/yyyy hh:mm tt"),
                    reader.GetString("O.R. No."), WebUtility.HtmlDecode(reader.GetString("Payor")),
                    WebUtility.HtmlDecode(reader.GetString("Particular1")) &
                    WebUtility.HtmlDecode(reader.GetString("Particular2")), "16600301010006",
                    FormatNumber(CDbl(reader.GetDecimal("Total Per O.R. No.")), 2),
                    FormatNumber(CDbl(reader.GetDecimal("Taxes")), 2),
                    FormatNumber(CDbl(reader.GetDecimal("Fees")), 2)}

                dgv_Collection.Rows.Add(row)

                Or_num = 0
                tax = 0
                fees = 0

                For i = 0 To dgv_Collection.Rows.Count() - 1 Step +1
                    Or_num = Or_num + dgv_Collection.Rows(i).Cells(5).Value
                    tax = tax + dgv_Collection.Rows(i).Cells(6).Value
                    fees = fees + dgv_Collection.Rows(i).Cells(7).Value
                Next

                TotalOR.Text = " " & FormatNumber(CDbl(Or_num), 2) & " "
                TotalTax.Text = " " & FormatNumber(CDbl(tax), 2) & " "
                TotalFees.Text = " " & FormatNumber(CDbl(fees), 2) & " "
            End While

            class_connection.con.Close()
        Catch ex As Exception
            class_connection.con.Close()
            MsgBox("Something went wrong. Couldn't process the data.", MsgBoxStyle.Exclamation,
                   "NTC Region 10")
        End Try
    End Sub

    Sub initialize_collection_datagridview()
        dgv_Collection.ColumnCount = 8
        dgv_Collection.Columns(0).Name = "Date"
        dgv_Collection.Columns(0).Width = 130
        dgv_Collection.Columns(1).Name = "O.R. No."
        dgv_Collection.Columns(1).Width = 80
        dgv_Collection.Columns(2).Name = "Payor"
        dgv_Collection.Columns(2).Width = 380
        dgv_Collection.Columns(3).Name = "Particular"
        dgv_Collection.Columns(3).Width = 220
        dgv_Collection.Columns(4).Name = "MPO/PAP"
        dgv_Collection.Columns(4).Width = 120
        dgv_Collection.Columns(5).Name = "Total Per O.R."
        dgv_Collection.Columns(5).Width = 120
        dgv_Collection.Columns(6).Name = "Taxes"
        dgv_Collection.Columns(6).Width = 100
        dgv_Collection.Columns(7).Name = "Fees"
        dgv_Collection.Columns(7).Width = 120

        dgv_Collection.Rows.Clear()
    End Sub

    Sub tab_two_report()
        Try
            Dim previous_or_num As String = ""
            Dim current_or_num As String = ""
            Dim first_run As Boolean = True
            Dim row As String(), MOP_ID As String
            Dim collection As Double
            Dim from1 = DTP11.Value.ToString("yyyy-MM-dd") & " 00:00:00"
            Dim upto1 = DTP22.Value.ToString("yyyy-MM-dd") & " 23:59:59"
            Dim value, value2, value3, num, num2, num3, percentage, percentage2, percentage3,
                p1Total, p2total, p3total, subtotal As Decimal

            initialize_cash_datagridview()

            class_connection.con.Open()
            query = "select tbl_soa_cashier.date_transact as 'Date',tbl_soa_cashier.or_num as 'OR No.', " &
                "tbl_payor.name as 'Payor', tbl_soa_data.p1_no_of_years as 'p1_years', " &
                "tbl_soa_data.p1_percent as 'p1_percent', tbl_soa_data.p1_no_of_units as 'p1_units', " &
                "tbl_soa_data.p1_fees as 'p1_fees', tbl_soa_data.p2_no_of_years as 'p2_years', " &
                "tbl_soa_data.p2_percent as 'p2_percent', tbl_soa_data.p2_no_of_units as 'p2_units', " &
                "tbl_soa_data.p2_fees as 'p2_fees', tbl_soa_data.p3_no_of_years as 'p3_years', " &
                "tbl_soa_data.p3_percent as 'p3_percent', tbl_soa_data.p3_no_of_units as 'p3_units', " &
                "tbl_soa_data.p3_fees as 'p3_fees', tbl_soa_data.mop_id as 'MOP ID' from tbl_soa_data, " &
                "tbl_payor, tbl_soa_details, tbl_soa_cashier, tbl_soa_transact where " &
                "tbl_soa_transact.soa_id = tbl_soa_cashier.soa_id And tbl_soa_details.soa_details_id = " &
                "tbl_soa_transact.soa_details_id And tbl_soa_details.payor_id = tbl_payor.payor_id " &
                "And tbl_soa_details.soa_data_id = tbl_soa_data.soa_data_id And " &
                "tbl_soa_cashier.date_transact between '" & from1 & "' and '" & upto1 & "' order by or_num ASC"
            cmd = New MySqlCommand(query, class_connection.con)

            reader = cmd.ExecuteReader()

            While reader.Read
                If first_run Then
                    previous_or_num = reader.GetString("OR No.")
                    current_or_num = previous_or_num
                    first_run = False
                End If

                If current_or_num <> reader.GetString("OR No.") Then
                    For i As Integer = 4 To MOP.Length - 1
                        collection += MOP(i)
                    Next

                    row = New String() {MOP(0), MOP(1), MOP(2), MOP(3), FormatNumber((MOP(4)), 2),
                        FormatNumber((MOP(5)), 2), FormatNumber((MOP(6)), 2), FormatNumber((MOP(7)), 2),
                        FormatNumber((MOP(8)), 2), FormatNumber((MOP(9)), 2), FormatNumber((MOP(10)), 2),
                        FormatNumber((MOP(11)), 2), FormatNumber((MOP(12)), 2), FormatNumber((MOP(13)), 2),
                        FormatNumber((MOP(14)), 2), FormatNumber((MOP(15)), 2), FormatNumber((MOP(16)), 2),
                        FormatNumber((MOP(17)), 2), FormatNumber((MOP(18)), 2), FormatNumber((MOP(19)), 2),
                        FormatNumber((MOP(20)), 2), FormatNumber((MOP(21)), 2), FormatNumber((MOP(22)), 2),
                        FormatNumber((MOP(23)), 2), FormatNumber((MOP(24)), 2), FormatNumber((MOP(25)), 2),
                        FormatNumber((MOP(26)), 2), FormatNumber((MOP(27)), 2), FormatNumber((MOP(28)), 2),
                        FormatNumber((MOP(29)), 2), FormatNumber((collection), 2)}
                    dgv_reciepts.Rows.Add(row)

                    For i As Integer = 0 To 3
                        MOP(i) = ""
                    Next

                    For i As Integer = 4 To MOP.Length - 1
                        MOP(i) = 0
                    Next

                    collection = 0
                    previous_or_num = reader.GetString("OR No.")
                    current_or_num = previous_or_num
                End If

                value = reader.GetDecimal("p1_percent")
                value2 = reader.GetDecimal("p2_percent")
                value3 = reader.GetDecimal("p3_percent")

                If Decimal.TryParse(value, num) And Decimal.TryParse(value2, num2) And
                    Decimal.TryParse(value3, num3) Then
                    percentage = num / 100
                    percentage2 = num2 / 100
                    percentage3 = num3 / 100
                End If

                p1Total = reader.GetDecimal("p1_years") * percentage *
                    reader.GetDecimal("p1_units") * reader.GetDecimal("p1_fees")
                p2total = reader.GetDecimal("p2_years") * percentage2 *
                    reader.GetDecimal("p2_units") * reader.GetDecimal("p2_fees")
                p3total = reader.GetDecimal("p3_years") * percentage3 *
                    reader.GetDecimal("p3_units") * reader.GetDecimal("p3_fees")

                subtotal = p1Total + p2total + p3total
                subtotal = FormatNumber(CDec(subtotal), 2)

                MOP_ID = reader.GetString("MOP ID")
                MOP(0) = reader.GetDateTime("Date").ToString("MM/dd/yyyy hh:mm tt")
                MOP(1) = reader.GetString("OR No.")
                MOP(2) = WebUtility.HtmlDecode(reader.GetString("Payor"))
                MOP(3) = "16600301010006"

                Select Case MOP_ID
                    Case 1 'col 1
                        MOP(4) += FormatNumber((subtotal), 2)
                    Case 2 'col 2
                        MOP(5) += FormatNumber((subtotal), 2)
                    Case 3 'col 3
                        MOP(6) += FormatNumber((subtotal), 2)
                    Case 5 'col 4
                        MOP(7) += FormatNumber((subtotal), 2)
                    Case 9 'col 26
                        MOP(24) += FormatNumber((subtotal), 2)
                    Case 27 'col 5
                        MOP(8) += FormatNumber((subtotal), 2)
                    Case 16 'col 6
                        MOP(9) += FormatNumber((subtotal), 2)
                    Case 15 'col 7
                        MOP(10) += FormatNumber((subtotal), 2)
                    Case 10 'col 25
                        MOP(24) += FormatNumber((subtotal), 2)
                    Case 11 'col 8
                        MOP(11) += FormatNumber((subtotal), 2)
                    Case 4 'col 9
                        MOP(12) += FormatNumber((subtotal), 2)
                    Case 20 'col 10
                        MOP(26) += FormatNumber((subtotal), 2)
                    Case 22 'col 10
                        MOP(13) += FormatNumber((subtotal), 2)
                    Case 26 'col 11
                        MOP(14) += FormatNumber((subtotal), 2)
                    Case 25 'col 12
                        MOP(15) += FormatNumber((subtotal), 2)
                    Case 6 'col 13
                        MOP(16) += FormatNumber((subtotal), 2)
                    Case 12 'col 14
                        MOP(17) += FormatNumber((subtotal), 2)
                    Case 13 'col 15
                        MOP(18) += FormatNumber((subtotal), 2)
                    Case 23 'col 16
                        MOP(19) += FormatNumber((subtotal), 2)
                    Case 7 'col 17
                        MOP(20) += FormatNumber((subtotal), 2)
                    Case 17 'col 18
                        MOP(21) += FormatNumber((subtotal), 2)
                    Case 18 'col 19
                        MOP(22) += FormatNumber((subtotal), 2)
                    Case 24 'col 20
                        MOP(23) += FormatNumber((subtotal), 2)
                    Case 8 'col 21
                        MOP(24) += FormatNumber((subtotal), 2)
                    Case 21 'col 25
                        MOP(26) += FormatNumber((subtotal), 2)
                    Case 14 'col 22
                        MOP(25) += FormatNumber((subtotal), 2)
                    Case 19 'col 23
                        MOP(26) += FormatNumber((subtotal), 2)
                    Case 28 'col 24
                        MOP(27) += FormatNumber((subtotal), 2)
                    Case 30 'col 25
                        MOP(28) += FormatNumber((subtotal), 2)
                    Case 29 'col 26
                        MOP(29) += FormatNumber((subtotal), 2)
                End Select
            End While

            ' Last Process START
            For i As Integer = 4 To MOP.Length - 1
                collection += MOP(i)
            Next

            row = New String() {MOP(0), MOP(1), MOP(2), MOP(3), FormatNumber((MOP(4)), 2),
                        FormatNumber((MOP(5)), 2), FormatNumber((MOP(6)), 2), FormatNumber((MOP(7)), 2),
                        FormatNumber((MOP(8)), 2), FormatNumber((MOP(9)), 2), FormatNumber((MOP(10)), 2),
                        FormatNumber((MOP(11)), 2), FormatNumber((MOP(12)), 2), FormatNumber((MOP(13)), 2),
                        FormatNumber((MOP(14)), 2), FormatNumber((MOP(15)), 2), FormatNumber((MOP(16)), 2),
                        FormatNumber((MOP(17)), 2), FormatNumber((MOP(18)), 2), FormatNumber((MOP(19)), 2),
                        FormatNumber((MOP(20)), 2), FormatNumber((MOP(21)), 2), FormatNumber((MOP(22)), 2),
                        FormatNumber((MOP(23)), 2), FormatNumber((MOP(24)), 2), FormatNumber((MOP(25)), 2),
                        FormatNumber((MOP(26)), 2), FormatNumber((MOP(27)), 2), FormatNumber((MOP(28)), 2),
                        FormatNumber((MOP(29)), 2), FormatNumber((collection), 2)}
            dgv_reciepts.Rows.Add(row)

            For i As Integer = 0 To 3
                MOP(i) = ""
            Next

            For i As Integer = 4 To MOP.Length - 1
                MOP(i) = 0
            Next

            collection = 0
            ' Last Process END

            compute_total()
            class_connection.con.Close()
        Catch ex As Exception
            class_connection.con.Close()
            MsgBox("Something went wrong. Couldn't process the data.", MsgBoxStyle.Exclamation,
                   "NTC Region 10")
        End Try
    End Sub

    Sub compute_total()
        Dim value As Double
        Dim str
        Dim total As Decimal = 0

        For i As Integer = 0 To dgv_reciepts.Rows.Count() - 1 Step +1
            str = dgv_reciepts.Rows(i).Cells(30).Value
            If Decimal.TryParse(str, value) Then
                total += str
            End If
        Next


        tb_total_cash.Text = " " & FormatNumber(CDbl(total), 2) & " "
    End Sub

    Private Sub BtnLoad2_Click(sender As Object, e As EventArgs) Handles BtnLoad2.Click
        dgv_reciepts.Rows.Clear()
        tab_two_report()
    End Sub

    Sub initialize_cash_datagridview()
        dgv_reciepts.ColumnCount = 31
        dgv_reciepts.Columns(0).Name = "Date"
        dgv_reciepts.Columns(0).Width = 150
        dgv_reciepts.Columns(1).Name = "OR No."
        dgv_reciepts.Columns(1).Width = 80
        dgv_reciepts.Columns(2).Name = "Payor"
        dgv_reciepts.Columns(2).Width = 300
        dgv_reciepts.Columns(3).Name = "MPO/PAP"
        dgv_reciepts.Columns(3).Width = 100
        dgv_reciepts.Columns(4).Name = "Permit to Purchase 4-02-01-010"
        dgv_reciepts.Columns(4).Width = 70
        dgv_reciepts.Columns(5).Name = "Filling Fee - License 4-02-01-130"
        dgv_reciepts.Columns(5).Width = 70
        dgv_reciepts.Columns(6).Name = "Permit to possess/Storage 4-02-01-010"
        dgv_reciepts.Columns(6).Width = 70
        dgv_reciepts.Columns(7).Name = "Radio Station License - LICENSE 4-02-01-060"
        dgv_reciepts.Columns(7).Width = 70
        dgv_reciepts.Columns(8).Name = "Modification Fee 4-02-01-060"
        dgv_reciepts.Columns(8).Width = 70
        dgv_reciepts.Columns(9).Name = "Radio Operator cert. 4-02-01-060"
        dgv_reciepts.Columns(9).Width = 70
        dgv_reciepts.Columns(10).Name = "Radio Station License - AROC/ROC 4-02-01-060"
        dgv_reciepts.Columns(10).Width = 70
        dgv_reciepts.Columns(11).Name = "Permit Fee 4-02-01-010"
        dgv_reciepts.Columns(11).Width = 70
        dgv_reciepts.Columns(12).Name = "Construction Permit Fees 4-02-01-010"
        dgv_reciepts.Columns(12).Width = 70
        dgv_reciepts.Columns(13).Name = "Registration Fees 4-02-01-020"
        dgv_reciepts.Columns(13).Width = 70
        dgv_reciepts.Columns(14).Name = "Clearance & Certificate Fees 4-02-01-040"
        dgv_reciepts.Columns(14).Width = 70
        dgv_reciepts.Columns(15).Name = "Examination Fees 4-02-01-030"
        dgv_reciepts.Columns(15).Width = 70
        dgv_reciepts.Columns(16).Name = "Inspection Fees - LICENSE 4-02-01-100"
        dgv_reciepts.Columns(16).Width = 70
        dgv_reciepts.Columns(17).Name = "Inspection Fees - PERMIT 4-02-01-130"
        dgv_reciepts.Columns(17).Width = 70
        dgv_reciepts.Columns(18).Name = "Filling Fee - Permit 4-02-01-130"
        dgv_reciepts.Columns(18).Width = 70
        dgv_reciepts.Columns(19).Name = "Supervision & Regulation Fees 4-02-01-070"
        dgv_reciepts.Columns(19).Width = 70
        dgv_reciepts.Columns(20).Name = "Spectrum Usage Fees 4-02-01-080"
        dgv_reciepts.Columns(20).Width = 80
        dgv_reciepts.Columns(21).Name = "Application Fee/ Filing Fee 4-02-01-130"
        dgv_reciepts.Columns(21).Width = 70
        dgv_reciepts.Columns(22).Name = "Seminar Fee 4-02-02-040"
        dgv_reciepts.Columns(22).Width = 70
        dgv_reciepts.Columns(23).Name = "Verification & Auth. Fees 4-02-01-040"
        dgv_reciepts.Columns(23).Width = 70
        dgv_reciepts.Columns(24).Name = "Fines/ Penalties/ Surcharges - License 4-02-01-140"
        dgv_reciepts.Columns(24).Width = 70
        dgv_reciepts.Columns(25).Name = "Fines/ Penalties/ Surcharges - Permit 4-02-01-140"
        dgv_reciepts.Columns(25).Width = 70
        dgv_reciepts.Columns(26).Name = "Fines/ Penalties/ Surcharges - AROC/ROC 4-02-01-140"
        dgv_reciepts.Columns(26).Width = 70
        dgv_reciepts.Columns(27).Name = "Miscelleneous Income Fees 4-02-01-990"
        dgv_reciepts.Columns(27).Width = 70
        dgv_reciepts.Columns(28).Name = "OTHERS 0-00-00-000"
        dgv_reciepts.Columns(28).Width = 70
        dgv_reciepts.Columns(29).Name = "Documentary Stamp tax (DST) 4-01-04-010"
        dgv_reciepts.Columns(29).Width = 70
        dgv_reciepts.Columns(30).Name = "Collection"
        dgv_reciepts.Columns(30).Width = 70
    End Sub

    Private Sub btn_newsoa_Click(sender As Object, e As EventArgs) Handles btn_printCollection.Click
        tab_one_export_to_excel()
    End Sub

    Sub tab_one_export_to_excel()
        Try
            Dim officeexcel As New Excel.Application
            officeexcel = CreateObject("Excel.Application")
            Dim workbook As Object = officeexcel.Workbooks.Add(Application.StartupPath &
                "\REPORT OF COLLECTIONS & DEPOSITS TEMPLATE.xltx")

            Dim i As Integer
            Dim j As Integer

            For i = 0 To dgv_Collection.RowCount - 1 Step +1  '15
                For j = 0 To dgv_Collection.ColumnCount - 1 Step +1
                    officeexcel.Cells(i + 18, j + 1) = dgv_Collection(j, i).Value
                Next
            Next

            officeexcel.Visible = True
            officeexcel.Top = 1
            officeexcel.ActiveWindow.WindowState = Excel.XlWindowState.xlMaximized

        Catch ex As Exception

        End Try
    End Sub

    Private Sub btn_printRecords_Click(sender As Object, e As EventArgs) Handles btn_printRecords.Click
        tab_two_exoport_to_excel()
    End Sub

    Sub tab_two_exoport_to_excel()
        'print tab 2
        Try
            Dim officeexcel2 As New Excel.Application
            officeexcel2 = CreateObject("Excel.Application")
            Dim workbook As Object = officeexcel2.Workbooks.Add(Application.StartupPath &
                "\CASH RECEIPTS RECORD TEMPLATE.xltx")

            Dim i As Integer
            Dim j As Integer

            For i = 0 To dgv_reciepts.RowCount - 1 Step +1  '15
                For j = 0 To dgv_reciepts.ColumnCount - 1 Step +1
                    officeexcel2.Cells(i + 17, j + 1) = dgv_reciepts(j, i).Value
                Next
            Next

            officeexcel2.Visible = True
            officeexcel2.Top = 1
            officeexcel2.ActiveWindow.WindowState = Excel.XlWindowState.xlMaximized

        Catch ex As Exception

        End Try
    End Sub

    Private Sub btn_collections_Click(sender As Object, e As EventArgs) Handles btn_collections.Click
        tab_Color_reset()

        btn_collections.BackColor = Color.FromArgb(113, 125, 137)
        btn_collections.ForeColor = Color.White

        pnl_collection_and_deposits.BringToFront()
    End Sub

    Private Sub btn_cash_Click(sender As Object, e As EventArgs) Handles btn_cash.Click
        tab_Color_reset()

        btn_cash.BackColor = Color.FromArgb(113, 125, 137)
        btn_cash.ForeColor = Color.White

        pnl_cash_receipts.BringToFront()
    End Sub

    Sub tab_Color_reset()
        btn_cash.BackColor = Color.FromArgb(69, 90, 100)
        btn_cash.ForeColor = Color.Silver
        btn_collections.BackColor = Color.FromArgb(69, 90, 100)
        btn_collections.ForeColor = Color.Silver
    End Sub
End Class
