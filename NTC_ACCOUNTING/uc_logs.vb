﻿Imports MySql.Data.MySqlClient
Imports System.Net

Public Class uc_logs
    Dim query, month_name, soa_creator_id, selected_month As String
    Dim cmd As MySqlCommand
    Dim reader As MySqlDataReader
    Dim thisMonth, retrieved_month As Integer
    Public user_id, authority_level As String

    Sub retrieve_years_from_database()
        ' Retrieve years.
        cmb_year.Items.Clear()
        class_connection.con.Open()
        query = "select distinct soa_year as 'year' from tbl_soa_transact order by soa_year desc"
        cmd = New MySqlCommand(query, class_connection.con)
        reader = cmd.ExecuteReader()

        While reader.Read
            cmb_year.Items.Add(reader.GetString("year"))
        End While
        class_connection.con.Close()

        If cmb_year.Items.Count <> 0 Then
            cmb_year.SelectedIndex() = 0
        End If
    End Sub

    Sub retrieve_months_from_database()
        ' Retrieve months.
        cmb_month.Items.Clear()
        class_connection.con.Open()
        query = "select distinct soa_month as 'month' from tbl_soa_transact where soa_year = '" &
            cmb_year.Text & "' order by soa_month desc"
        cmd = New MySqlCommand(query, class_connection.con)
        reader = cmd.ExecuteReader()

        While reader.Read
            retrieved_month = reader.GetInt16("month")

            Select Case retrieved_month
                Case 1
                    thisMonth = 1
                    month_name = MonthName(thisMonth, False)
                Case 2
                    thisMonth = 2
                    month_name = MonthName(thisMonth, False)
                Case 3
                    thisMonth = 3
                    month_name = MonthName(thisMonth, False)
                Case 4
                    thisMonth = 4
                    month_name = MonthName(thisMonth, False)
                Case 5
                    thisMonth = 5
                    month_name = MonthName(thisMonth, False)
                Case 6
                    thisMonth = 6
                    month_name = MonthName(thisMonth, False)
                Case 7
                    thisMonth = 7
                    month_name = MonthName(thisMonth, False)
                Case 8
                    thisMonth = 8
                    month_name = MonthName(thisMonth, False)
                Case 9
                    thisMonth = 9
                    month_name = MonthName(thisMonth, False)
                Case 10
                    thisMonth = 10
                    month_name = MonthName(thisMonth, False)
                Case 11
                    thisMonth = 11
                    month_name = MonthName(thisMonth, False)
                Case Else
                    thisMonth = 12
                    month_name = MonthName(thisMonth, False)
            End Select

            cmb_month.Items.Add(month_name)
        End While
        class_connection.con.Close()

        If cmb_year.Items.Count <> 0 Then
            cmb_month.SelectedIndex() = 0
        End If
    End Sub

    Sub month_number(month)
        Select Case month
            Case "January"
                selected_month = "1"
            Case "February"
                selected_month = "2"
            Case "March"
                selected_month = "3"
            Case "April"
                selected_month = "4"
            Case "May"
                selected_month = "5"
            Case "June"
                selected_month = "6"
            Case "July"
                selected_month = "7"
            Case "August"
                selected_month = "8"
            Case "September"
                selected_month = "9"
            Case "October"
                selected_month = "10"
            Case "November"
                selected_month = "11"
            Case "December"
                selected_month = "12"
        End Select
    End Sub

    Sub load_transaction_logs()
        Try
            Dim row As String()

            If cmb_year.Items.Count <> 0 Then
                ' For admin and approver.
                If authority_level = "4" Or authority_level = "5" Then
                    initialize_datagridview(authority_level)
                    month_number(cmb_month.Text)

                    class_connection.con.Open()
                    query = "SELECT tbl_soa_transact.soa_id as 'ID', tbl_payor.name as 'Payor', " &
                        "tbl_user.full_name as 'Preparator', tbl_soa_details.serial_id as 'SOA No.'" &
                        ", tbl_soa_endorse.op_serial_num as 'OP No.', tbl_soa_cashier.or_num as " &
                        "'OR No.' , tbl_soa_details.date_created as 'Date' FROM tbl_soa_transact " &
                        "join tbl_soa_details on tbl_soa_details.soa_details_id = " &
                        "tbl_soa_transact.soa_details_id join tbl_payor on tbl_payor.payor_id = " &
                        "tbl_soa_details.payor_id join tbl_user on tbl_user.user_id = " &
                        "tbl_soa_transact.user_id join tbl_soa_endorse on tbl_soa_transact.soa_id = " &
                        "tbl_soa_endorse.soa_id join tbl_soa_cashier on tbl_soa_transact.soa_id = " &
                        "tbl_soa_cashier.soa_id where tbl_soa_transact.soa_year = '" & cmb_year.Text &
                        "' and tbl_soa_transact.soa_month = '" & selected_month & "' order by " &
                        "tbl_soa_details.serial_id DESC"
                    cmd = New MySqlCommand(query, class_connection.con)
                    reader = cmd.ExecuteReader()

                    While reader.Read
                        row = New String() {reader.GetString("ID"),
                            WebUtility.HtmlDecode(reader.GetString("Payor")),
                            WebUtility.HtmlDecode(reader.GetString("Preparator")),
                            reader.GetString("SOA No."), reader.GetString("OP No."),
                            reader.GetString("Or No."), reader.GetMySqlDateTime("Date")}
                        dgv_soa_logs.Rows.Add(row)
                    End While
                    class_connection.con.Close()

                    ' For licenser.
                ElseIf authority_level = "1" Then
                    initialize_datagridview(authority_level)
                    month_number(cmb_month.Text)

                    class_connection.con.Open()
                    query = "SELECT  tbl_soa_transact.soa_id as 'ID', tbl_payor.name as 'Payor', " &
                        "tbl_soa_details.serial_id as 'SOA No.', tbl_soa_details.date_created as " &
                        "'Date' FROM tbl_soa_transact join tbl_soa_details on " &
                        "tbl_soa_details.soa_details_id = tbl_soa_transact.soa_details_id join " &
                        "tbl_payor on tbl_payor.payor_id = tbl_soa_details.payor_id where user_id = '" &
                        user_id & "' and (soa_year = '" & cmb_year.Text & "' and soa_month = '" &
                        selected_month & "') order by tbl_soa_details.serial_id DESC"
                    cmd = New MySqlCommand(query, class_connection.con)
                    reader = cmd.ExecuteReader()

                    While reader.Read
                        row = New String() {reader.GetString("ID"),
                            WebUtility.HtmlDecode(reader.GetString("Payor")),
                            reader.GetString("SOA No."), reader.GetMySqlDateTime("Date")}
                        dgv_soa_logs.Rows.Add(row)
                    End While
                    class_connection.con.Close()

                    ' For accountant.
                ElseIf authority_level = "2" Then
                    initialize_datagridview(authority_level)
                    month_number(cmb_month.Text)

                    class_connection.con.Open()
                    query = "SELECT endorse_id as 'ID', tbl_payor.name as 'Payor', op_serial_num as " &
                        "'OP No.', op_date_created as 'Date' FROM tbl_soa_endorse join tbl_soa_transact " &
                        "on tbl_soa_transact.soa_id = tbl_soa_endorse.soa_id join tbl_soa_details on " &
                        "tbl_soa_details.soa_details_id = tbl_soa_transact.soa_details_id join tbl_payor " &
                        "on tbl_payor.payor_id = tbl_soa_details.payor_id where is_updated = '1' and " &
                        "(op_year = '" & cmb_year.Text & "' and op_month = '" & selected_month &
                        "') order by op_serial_num DESC"
                    cmd = New MySqlCommand(query, class_connection.con)
                    reader = cmd.ExecuteReader()

                    While reader.Read
                        row = New String() {reader.GetString("ID"),
                            WebUtility.HtmlDecode(reader.GetString("Payor")),
                            reader.GetString("OP No."), reader.GetMySqlDateTime("Date")}
                        dgv_soa_logs.Rows.Add(row)
                    End While
                    class_connection.con.Close()

                    ' For cashier.
                ElseIf authority_level = "3" Then
                    initialize_datagridview(authority_level)
                    month_number(cmb_month.Text)

                    class_connection.con.Open()
                    query = "SELECT tbl_soa_cashier.cashier_transact_id as 'ID', tbl_payor.name as " &
                        "'Payor',  or_num as 'OR No.', tbl_soa_cashier.date_transact as 'Date' FROM " &
                        "tbl_soa_cashier join tbl_soa_transact on tbl_soa_transact.soa_id = " &
                        "tbl_soa_cashier.soa_id join tbl_soa_details on tbl_soa_details.soa_details_id " &
                        "= tbl_soa_transact.soa_details_id join tbl_payor on tbl_payor.payor_id = " &
                        "tbl_soa_details.payor_id where (or_year = '" & cmb_year.Text & "' and or_month = " &
                        "'" & selected_month & "') order by tbl_soa_cashier.or_num DESC"
                    cmd = New MySqlCommand(query, class_connection.con)
                    reader = cmd.ExecuteReader()

                    While reader.Read
                        row = New String() {reader.GetString("ID"),
                            WebUtility.HtmlDecode(reader.GetString("Payor")), reader.GetString("OR No."),
                            reader.GetMySqlDateTime("Date")}
                        dgv_soa_logs.Rows.Add(row)
                    End While
                    class_connection.con.Close()
                End If

                dgv_soa_logs.Visible = True
                btn_refresh.Visible = False
                tb_message.Visible = False
                tb_search.Enabled = True
                cmb_year.Enabled = True
                cmb_month.Enabled = True
            Else
                tb_message.Text = "You have no logs."
                dgv_soa_logs.Visible = False
                btn_refresh.Visible = True
                tb_message.Visible = True
                tb_search.Enabled = False
                cmb_year.Enabled = False
                cmb_month.Enabled = False
            End If
        Catch ex As MySqlException
            tb_message.Text = "Couldn't connect to server."
            dgv_soa_logs.Visible = False
            btn_refresh.Visible = True
            tb_message.Visible = True
            tb_search.Enabled = False
            cmb_month.Enabled = False
            cmb_year.Enabled = False
        Catch ex As Exception
            MsgBox(ex.Message + " ; " & ex.GetType.ToString)
            tb_message.Text = "Couldn't connect to server."
            dgv_soa_logs.Visible = False
            btn_refresh.Visible = True
            tb_message.Visible = True
            tb_search.Enabled = False
            cmb_month.Enabled = False
            cmb_year.Enabled = False
        End Try
    End Sub

    Sub initialize_datagridview(authority)
        ' Initialize datagridview according to authority.
        Select Case authority
            Case "1"
                dgv_soa_logs.ColumnCount = 4
                dgv_soa_logs.Columns(0).Name = "ID"
                dgv_soa_logs.Columns(0).Visible = False
                dgv_soa_logs.Columns(0).Width = 0
                dgv_soa_logs.Columns(1).HeaderText = "Payor"
                dgv_soa_logs.Columns(1).Width = 400
                dgv_soa_logs.Columns(2).HeaderText = "SOA No."
                dgv_soa_logs.Columns(2).Width = 140
                dgv_soa_logs.Columns(3).HeaderText = "Date Created"

                dgv_soa_logs.Rows.Clear()
            Case "2"
                dgv_soa_logs.ColumnCount = 4
                dgv_soa_logs.Columns(0).Name = "ID"
                dgv_soa_logs.Columns(0).Visible = False
                dgv_soa_logs.Columns(0).Width = 0
                dgv_soa_logs.Columns(1).HeaderText = "Payor"
                dgv_soa_logs.Columns(1).Width = 400
                dgv_soa_logs.Columns(2).Name = "OP No."
                dgv_soa_logs.Columns(2).Width = 140
                dgv_soa_logs.Columns(3).HeaderText = "Date Created"

                dgv_soa_logs.Rows.Clear()
            Case "3"
                dgv_soa_logs.ColumnCount = 4

                dgv_soa_logs.Columns(0).Name = "ID"
                dgv_soa_logs.Columns(0).Visible = False
                dgv_soa_logs.Columns(0).Width = 0
                dgv_soa_logs.Columns(1).HeaderText = "Payor"
                dgv_soa_logs.Columns(1).Width = 400
                dgv_soa_logs.Columns(2).Name = "OR No."
                dgv_soa_logs.Columns(2).Width = 140
                dgv_soa_logs.Columns(3).HeaderText = "Date Created"

                dgv_soa_logs.Rows.Clear()
            Case "4", "5"
                dgv_soa_logs.ColumnCount = 7
                dgv_soa_logs.Columns(0).Name = "ID"
                dgv_soa_logs.Columns(0).Visible = False
                dgv_soa_logs.Columns(0).Width = 0
                dgv_soa_logs.Columns(1).HeaderText = "Payor"
                dgv_soa_logs.Columns(1).Width = 400
                dgv_soa_logs.Columns(2).HeaderText = "Preparator"
                dgv_soa_logs.Columns(2).Width = 150
                dgv_soa_logs.Columns(3).Name = "SOA No."
                dgv_soa_logs.Columns(3).Width = 140
                dgv_soa_logs.Columns(4).Name = "OP No."
                dgv_soa_logs.Columns(4).Width = 140
                dgv_soa_logs.Columns(5).Name = "Or No."
                dgv_soa_logs.Columns(5).Width = 140
                dgv_soa_logs.Columns(6).HeaderText = "Date Created"

                dgv_soa_logs.Rows.Clear()
        End Select
    End Sub

    Private Sub uc_logs_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        retrieve_years_from_database()
        retrieve_months_from_database()

        load_transaction_logs()
    End Sub

    Private Sub dgv_soa_logs_CellClick(sender As Object, e As DataGridViewCellEventArgs) Handles dgv_soa_logs.CellClick
        dashboard.RadCollapsiblePanel1.IsExpanded = False
    End Sub

    Private Sub tb_search_TextChanged(sender As Object, e As EventArgs) Handles tb_search.TextChanged
        dashboard.RadCollapsiblePanel1.IsExpanded = False
    End Sub

    Private Sub tb_search_Click(sender As Object, e As EventArgs) Handles tb_search.Click
        dashboard.RadCollapsiblePanel1.IsExpanded = False
    End Sub

    Private Sub cmb_year_SelectionChangeCommitted(sender As Object, e As EventArgs) Handles cmb_year.SelectionChangeCommitted
        dashboard.RadCollapsiblePanel1.IsExpanded = False

        retrieve_months_from_database()

        tb_search.Text = ""
    End Sub

    Public Sub setUserandAuthorLevel(ID, Level)
        user_id = ID
        authority_level = Level
    End Sub

    Private Sub tb_search_KeyDown(sender As Object, e As KeyEventArgs) Handles tb_search.KeyDown
        If e.KeyCode = Keys.Enter Then
            keyword_search()
        End If
    End Sub

    Private Sub btn_refresh_Click(sender As Object, e As EventArgs) Handles btn_refresh.Click
        load_transaction_logs()
    End Sub

    Private Sub dgv_soa_logs_CellDoubleClick(sender As Object, e As DataGridViewCellEventArgs) Handles dgv_soa_logs.CellDoubleClick
        dashboard.RadCollapsiblePanel1.IsExpanded = False
        If e.RowIndex >= 0 Then
            Dim row As DataGridViewRow
            row = Me.dgv_soa_logs.Rows(e.RowIndex)

            Try
                class_connection.con.Open()
                query = "select user_id from tbl_soa_transact where soa_id = '" + row.Cells("ID").Value.ToString + "'"
                cmd = New MySqlCommand(query, class_connection.con)
                reader = cmd.ExecuteReader()

                If reader.Read Then
                    soa_creator_id = reader.GetString("user_id")
                End If
                class_connection.con.Close()

                If authority_level = "1" And user_id = soa_creator_id Then
                    frm_licenser_log.soa_id_transact = row.Cells("ID").Value.ToString
                    frm_licenser_log.user_id = user_id
                    frm_licenser_log.authority_level = authority_level
                    frm_licenser_log.Show()

                    frm_licenser_log.btn_print_assessment.Visible = True
                    frm_licenser_log.btn_print_endorse.Visible = True
                    frm_licenser_log.btn_edit.Visible = True
                    frm_licenser_log.btn_add_approve.Visible = True
                    frm_licenser_log.pb_approve_status.Visible = True
                    frm_licenser_log.pnl_refresh.Visible = True
                ElseIf authority_level = "1" And user_id <> soa_creator_id Then
                    frm_licenser_log.soa_id_transact = row.Cells("ID").Value.ToString
                    frm_licenser_log.user_id = user_id
                    frm_licenser_log.authority_level = authority_level
                    frm_licenser_log.Show()

                    frm_licenser_log.btn_print_assessment.Visible = True
                    frm_licenser_log.btn_print_endorse.Visible = False
                    frm_licenser_log.btn_edit.Visible = False
                    frm_licenser_log.btn_add_approve.Visible = False
                    frm_licenser_log.pb_approve_status.Visible = True
                    frm_licenser_log.pnl_refresh.Visible = True
                End If
                If authority_level = "2" Then
                    frm_accountant_log.endorse_id = row.Cells("ID").Value.ToString
                    frm_accountant_log.authority_level = authority_level
                    frm_accountant_log.Show()
                End If
                If authority_level = "4" Or authority_level = "5" Then
                    frm_licenser_log.soa_id_transact = row.Cells("ID").Value.ToString
                    frm_licenser_log.authority_level = authority_level
                    frm_licenser_log.btn_add_approve.Visible = False
                    frm_licenser_log.pnl_refresh.Visible = False
                    frm_licenser_log.user_id = user_id
                    frm_licenser_log.btn_reasses.Visible = False
                    frm_licenser_log.pnl_refresh.Visible = True
                    frm_licenser_log.Show()

                    frm_licenser_log.pb_approve_status.Visible = True
                End If
                If authority_level = "3" Then
                    frm_cashier_log.transact_id_cashier = row.Cells("ID").Value.ToString
                    frm_cashier_log.user_id = user_id
                    frm_cashier_log.authority_level = authority_level
                    frm_cashier_log.btn_op.Visible = True
                    frm_cashier_log.btn_soa.Visible = True
                    frm_cashier_log.Show()
                End If
            Catch ex As Exception
                class_connection.con.Close()
                MsgBox("Could'nt connect to server.", MsgBoxStyle.Exclamation, "NTC Region 10")
            End Try
        End If
    End Sub

    Private Sub btn_search_Click(sender As Object, e As EventArgs) Handles btn_search.Click
        keyword_search()
    End Sub

    Sub keyword_search()
        Try
            Dim keyword = WebUtility.HtmlEncode(tb_search.Text)
            Dim row As String()

            Select Case authority_level
                ' For admin and approver.
                Case "4", "5"
                    initialize_datagridview(authority_level)
                    month_number(cmb_month.Text)

                    class_connection.con.Open()
                    query = "SELECT tbl_soa_transact.soa_id as 'ID', tbl_payor.name as 'Payor', " &
                        "tbl_user.full_name as 'Preparator', tbl_soa_details.serial_id as 'SOA No.', " &
                        "tbl_soa_endorse.op_serial_num as 'OP No.', tbl_soa_cashier.or_num as 'OR No.', " &
                        "tbl_soa_details.date_created as 'Date' FROM tbl_soa_transact join tbl_soa_details " &
                        "on tbl_soa_details.soa_details_id = tbl_soa_transact.soa_details_id join tbl_payor " &
                        "on tbl_payor.payor_id = tbl_soa_details.payor_id join tbl_user on tbl_user.user_id = " &
                        "tbl_soa_transact.user_id join tbl_soa_endorse on tbl_soa_transact.soa_id = " &
                        "tbl_soa_endorse.soa_id join tbl_soa_cashier on tbl_soa_transact.soa_id = " &
                        "tbl_soa_cashier.soa_id where tbl_soa_transact.soa_year = '" & cmb_year.Text &
                        "' and soa_month = '" & selected_month & "' and (tbl_payor.name like '" &
                        keyword & "%' or tbl_soa_details.serial_id like '" & keyword + "%' or " &
                        "tbl_user.full_name like '" & keyword + "%') order by tbl_soa_details.serial_id DESC"
                    cmd = New MySqlCommand(query, class_connection.con)
                    reader = cmd.ExecuteReader()

                    While reader.Read
                        row = New String() {reader.GetString("ID"),
                            WebUtility.HtmlDecode(reader.GetString("Payor")),
                            WebUtility.HtmlDecode(reader.GetString("Preparator")),
                            reader.GetString("SOA No."), reader.GetString("OP No."),
                            reader.GetString("OR No."), reader.GetMySqlDateTime("Date")}
                        dgv_soa_logs.Rows.Add(row)
                    End While
                    class_connection.con.Close()

                    ' For Licenser
                Case "1"
                    initialize_datagridview(authority_level)
                    month_number(cmb_month.Text)

                    class_connection.con.Open()
                    query = "SELECT  tbl_soa_transact.soa_id as 'ID', tbl_payor.name as 'Payor', " &
                        "tbl_soa_details.serial_id as 'SOA No.', tbl_soa_details.date_created as 'Date' " &
                        "FROM tbl_soa_transact join tbl_soa_details on tbl_soa_details.soa_details_id = " &
                        "tbl_soa_transact.soa_details_id join tbl_payor on tbl_payor.payor_id = " &
                        "tbl_soa_details.payor_id where soa_year = '" & cmb_year.Text & "' and soa_month = '" &
                        selected_month + "' and user_id = '" & user_id & "' and (tbl_payor.name like '" &
                        keyword & "%' or tbl_soa_details.serial_id like '" & keyword &
                        "%' or tbl_soa_details.date_created like '" & keyword & "%') order by " &
                        "tbl_soa_details.serial_id DESC"
                    cmd = New MySqlCommand(query, class_connection.con)
                    reader = cmd.ExecuteReader()

                    While reader.Read
                        row = New String() {reader.GetString("ID"),
                            WebUtility.HtmlDecode(reader.GetString("Payor")), reader.GetString("SOA No."),
                            reader.GetMySqlDateTime("Date")}
                        dgv_soa_logs.Rows.Add(row)
                    End While
                    class_connection.con.Close()

                    ' For accountant.
                Case "2"
                    initialize_datagridview(authority_level)
                    month_number(cmb_month.Text)

                    class_connection.con.Open()
                    query = "SELECT endorse_id as 'ID', tbl_payor.name as 'Payor', op_serial_num as 'OP No.', " &
                        "op_date_created as 'Date' FROM tbl_soa_endorse join tbl_soa_transact on " &
                        "tbl_soa_transact.soa_id = tbl_soa_endorse.soa_id join tbl_soa_details on " &
                        "tbl_soa_details.soa_details_id = tbl_soa_transact.soa_details_id join tbl_payor on " &
                        "tbl_payor.payor_id = tbl_soa_details.payor_id where op_year = '" & cmb_year.Text &
                        "' and op_month = '" & selected_month & "' and is_updated = '1' and (tbl_payor.name like '" &
                        keyword & "%' or op_serial_num like '" & keyword & "%' or tbl_soa_endorse.op_date_created " &
                        "Like '" & keyword & "%') order by op_serial_num DESC"
                    cmd = New MySqlCommand(query, class_connection.con)
                    reader = cmd.ExecuteReader()

                    While reader.Read
                        row = New String() {reader.GetString("ID"), WebUtility.HtmlDecode(reader.GetString("Payor")),
                            reader.GetString("OP No."), reader.GetMySqlDateTime("Date")}
                        dgv_soa_logs.Rows.Add(row)
                    End While
                    class_connection.con.Close()

                    ' For cashier.
                Case "3"
                    initialize_datagridview(authority_level)
                    month_number(cmb_month.Text)

                    class_connection.con.Open()
                    query = "SELECT tbl_soa_cashier.cashier_transact_id as 'ID', tbl_payor.name as 'Payor', " &
                        "or_num as 'OR No.', tbl_soa_cashier.date_transact as 'Date' FROM tbl_soa_cashier join " &
                        "tbl_soa_transact on tbl_soa_transact.soa_id = tbl_soa_cashier.soa_id join tbl_soa_details " &
                        "on tbl_soa_details.soa_details_id = tbl_soa_transact.soa_details_id join tbl_payor on " &
                        "tbl_payor.payor_id = tbl_soa_details.payor_id where or_year = '" & cmb_year.Text &
                        "' and or_month = '" & selected_month & "' and user_id_cashier = '" & user_id &
                        "' and (tbl_payor.name like '" & keyword & "%' or or_num like '" & keyword &
                        "%' or tbl_soa_cashier.date_transact = '" & keyword & "%') order by tbl_soa_cashier.or_num DESC"
                    cmd = New MySqlCommand(query, class_connection.con)
                    reader = cmd.ExecuteReader()

                    While reader.Read
                        row = New String() {reader.GetString("ID"), WebUtility.HtmlDecode(reader.GetString("Payor")),
                            reader.GetString("OR No."), reader.GetMySqlDateTime("Date")}
                        dgv_soa_logs.Rows.Add(row)
                    End While
                    class_connection.con.Close()
            End Select
        Catch ex As Exception
            class_connection.con.Close()
            dgv_soa_logs.Visible = False
            btn_refresh.Visible = True
            tb_message.Visible = True
            tb_search.Enabled = False
            cmb_year.Enabled = False
        End Try
    End Sub

    Private Sub btn_retrieve_Click(sender As Object, e As EventArgs) Handles btn_retrieve.Click
        load_transaction_logs()
    End Sub

    Sub returnHome()
        Dim dashboard_home As New dashboard_home

        dashboard.pnl_dashboard.Controls.Clear()
        dashboard.pnl_dashboard.Controls.Add(dashboard_home)
    End Sub
End Class
