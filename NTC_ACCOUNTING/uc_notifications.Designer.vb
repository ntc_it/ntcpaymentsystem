﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class uc_notifications
    Inherits System.Windows.Forms.UserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.dgv_notify = New System.Windows.Forms.DataGridView()
        Me.tb_msg_title = New System.Windows.Forms.TextBox()
        Me.tb_msg_content = New System.Windows.Forms.TextBox()
        Me.tb_action = New System.Windows.Forms.Button()
        Me.tb_notifier = New System.Windows.Forms.TextBox()
        Me.cmb_msg_category = New System.Windows.Forms.ComboBox()
        Me.pnl_message = New System.Windows.Forms.Panel()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.btn_markAll = New System.Windows.Forms.Button()
        Me.tb_notification_extra = New System.Windows.Forms.TextBox()
        CType(Me.dgv_notify, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnl_message.SuspendLayout()
        Me.SuspendLayout()
        '
        'dgv_notify
        '
        Me.dgv_notify.AllowUserToAddRows = False
        Me.dgv_notify.AllowUserToDeleteRows = False
        Me.dgv_notify.AllowUserToResizeColumns = False
        Me.dgv_notify.AllowUserToResizeRows = False
        Me.dgv_notify.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.dgv_notify.BackgroundColor = System.Drawing.Color.FromArgb(CType(CType(28, Byte), Integer), CType(CType(40, Byte), Integer), CType(CType(52, Byte), Integer))
        Me.dgv_notify.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.dgv_notify.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.[Single]
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.AppWorkspace
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgv_notify.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.dgv_notify.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(CType(CType(28, Byte), Integer), CType(CType(40, Byte), Integer), CType(CType(52, Byte), Integer))
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.Color.White
        DataGridViewCellStyle2.NullValue = Nothing
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.FromArgb(CType(CType(181, Byte), Integer), CType(CType(186, Byte), Integer), CType(CType(188, Byte), Integer))
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgv_notify.DefaultCellStyle = DataGridViewCellStyle2
        Me.dgv_notify.GridColor = System.Drawing.Color.FromArgb(CType(CType(28, Byte), Integer), CType(CType(40, Byte), Integer), CType(CType(52, Byte), Integer))
        Me.dgv_notify.Location = New System.Drawing.Point(28, 53)
        Me.dgv_notify.MultiSelect = False
        Me.dgv_notify.Name = "dgv_notify"
        Me.dgv_notify.ReadOnly = True
        Me.dgv_notify.RowHeadersVisible = False
        Me.dgv_notify.RowTemplate.DefaultCellStyle.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dgv_notify.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgv_notify.Size = New System.Drawing.Size(798, 580)
        Me.dgv_notify.TabIndex = 10
        '
        'tb_msg_title
        '
        Me.tb_msg_title.BackColor = System.Drawing.Color.FromArgb(CType(CType(113, Byte), Integer), CType(CType(125, Byte), Integer), CType(CType(137, Byte), Integer))
        Me.tb_msg_title.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.tb_msg_title.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tb_msg_title.ForeColor = System.Drawing.Color.White
        Me.tb_msg_title.Location = New System.Drawing.Point(92, 80)
        Me.tb_msg_title.Name = "tb_msg_title"
        Me.tb_msg_title.ReadOnly = True
        Me.tb_msg_title.Size = New System.Drawing.Size(332, 22)
        Me.tb_msg_title.TabIndex = 11
        Me.tb_msg_title.Text = "Insert Sample Long Mesage Title Here"
        '
        'tb_msg_content
        '
        Me.tb_msg_content.BackColor = System.Drawing.Color.FromArgb(CType(CType(113, Byte), Integer), CType(CType(125, Byte), Integer), CType(CType(137, Byte), Integer))
        Me.tb_msg_content.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.tb_msg_content.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tb_msg_content.ForeColor = System.Drawing.Color.White
        Me.tb_msg_content.Location = New System.Drawing.Point(88, 136)
        Me.tb_msg_content.MaxLength = 255
        Me.tb_msg_content.Multiline = True
        Me.tb_msg_content.Name = "tb_msg_content"
        Me.tb_msg_content.ReadOnly = True
        Me.tb_msg_content.Size = New System.Drawing.Size(336, 116)
        Me.tb_msg_content.TabIndex = 12
        Me.tb_msg_content.TabStop = False
        Me.tb_msg_content.Text = "Insert Long Sample Message Content here so that the user will what the message co" &
    "ntains"
        '
        'tb_action
        '
        Me.tb_action.BackColor = System.Drawing.Color.FromArgb(CType(CType(218, Byte), Integer), CType(CType(144, Byte), Integer), CType(CType(27, Byte), Integer))
        Me.tb_action.FlatAppearance.BorderSize = 0
        Me.tb_action.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.tb_action.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tb_action.ForeColor = System.Drawing.Color.White
        Me.tb_action.Location = New System.Drawing.Point(172, 318)
        Me.tb_action.Name = "tb_action"
        Me.tb_action.Size = New System.Drawing.Size(110, 35)
        Me.tb_action.TabIndex = 13
        Me.tb_action.Text = "Action"
        Me.tb_action.UseVisualStyleBackColor = False
        '
        'tb_notifier
        '
        Me.tb_notifier.BackColor = System.Drawing.Color.FromArgb(CType(CType(113, Byte), Integer), CType(CType(125, Byte), Integer), CType(CType(137, Byte), Integer))
        Me.tb_notifier.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.tb_notifier.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tb_notifier.ForeColor = System.Drawing.Color.White
        Me.tb_notifier.Location = New System.Drawing.Point(92, 260)
        Me.tb_notifier.Name = "tb_notifier"
        Me.tb_notifier.ReadOnly = True
        Me.tb_notifier.Size = New System.Drawing.Size(332, 22)
        Me.tb_notifier.TabIndex = 14
        Me.tb_notifier.Text = "Sample name Here"
        '
        'cmb_msg_category
        '
        Me.cmb_msg_category.BackColor = System.Drawing.Color.FromArgb(CType(CType(28, Byte), Integer), CType(CType(40, Byte), Integer), CType(CType(52, Byte), Integer))
        Me.cmb_msg_category.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmb_msg_category.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.cmb_msg_category.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmb_msg_category.ForeColor = System.Drawing.Color.White
        Me.cmb_msg_category.FormattingEnabled = True
        Me.cmb_msg_category.Items.AddRange(New Object() {"Unread", "Action Taken", "Unresolved"})
        Me.cmb_msg_category.Location = New System.Drawing.Point(678, 23)
        Me.cmb_msg_category.Name = "cmb_msg_category"
        Me.cmb_msg_category.Size = New System.Drawing.Size(148, 21)
        Me.cmb_msg_category.TabIndex = 15
        '
        'pnl_message
        '
        Me.pnl_message.BackColor = System.Drawing.Color.FromArgb(CType(CType(113, Byte), Integer), CType(CType(125, Byte), Integer), CType(CType(137, Byte), Integer))
        Me.pnl_message.Controls.Add(Me.Label3)
        Me.pnl_message.Controls.Add(Me.Label2)
        Me.pnl_message.Controls.Add(Me.Label1)
        Me.pnl_message.Controls.Add(Me.tb_msg_title)
        Me.pnl_message.Controls.Add(Me.tb_msg_content)
        Me.pnl_message.Controls.Add(Me.tb_notifier)
        Me.pnl_message.Controls.Add(Me.tb_action)
        Me.pnl_message.Location = New System.Drawing.Point(847, 53)
        Me.pnl_message.Name = "pnl_message"
        Me.pnl_message.Size = New System.Drawing.Size(446, 580)
        Me.pnl_message.TabIndex = 16
        Me.pnl_message.Visible = False
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Segoe UI", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ForeColor = System.Drawing.Color.Silver
        Me.Label3.Location = New System.Drawing.Point(36, 266)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(33, 13)
        Me.Label3.TabIndex = 17
        Me.Label3.Text = "From"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Segoe UI", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.Silver
        Me.Label2.Location = New System.Drawing.Point(21, 136)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(48, 13)
        Me.Label2.TabIndex = 16
        Me.Label2.Text = "Content"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Segoe UI", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.Silver
        Me.Label1.Location = New System.Drawing.Point(40, 85)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(29, 13)
        Me.Label1.TabIndex = 15
        Me.Label1.Text = "Title"
        '
        'btn_markAll
        '
        Me.btn_markAll.BackColor = System.Drawing.Color.FromArgb(CType(CType(218, Byte), Integer), CType(CType(144, Byte), Integer), CType(CType(27, Byte), Integer))
        Me.btn_markAll.FlatAppearance.BorderSize = 0
        Me.btn_markAll.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_markAll.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_markAll.ForeColor = System.Drawing.Color.White
        Me.btn_markAll.Location = New System.Drawing.Point(540, 23)
        Me.btn_markAll.Name = "btn_markAll"
        Me.btn_markAll.Size = New System.Drawing.Size(110, 21)
        Me.btn_markAll.TabIndex = 17
        Me.btn_markAll.Text = "Mark all as read"
        Me.btn_markAll.UseVisualStyleBackColor = False
        Me.btn_markAll.Visible = False
        '
        'tb_notification_extra
        '
        Me.tb_notification_extra.BackColor = System.Drawing.Color.FromArgb(CType(CType(44, Byte), Integer), CType(CType(62, Byte), Integer), CType(CType(80, Byte), Integer))
        Me.tb_notification_extra.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.tb_notification_extra.Cursor = System.Windows.Forms.Cursors.Hand
        Me.tb_notification_extra.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tb_notification_extra.ForeColor = System.Drawing.Color.Yellow
        Me.tb_notification_extra.Location = New System.Drawing.Point(28, 22)
        Me.tb_notification_extra.Name = "tb_notification_extra"
        Me.tb_notification_extra.ReadOnly = True
        Me.tb_notification_extra.ShortcutsEnabled = False
        Me.tb_notification_extra.Size = New System.Drawing.Size(488, 22)
        Me.tb_notification_extra.TabIndex = 18
        Me.tb_notification_extra.TabStop = False
        Me.tb_notification_extra.Text = "Extra Notification"
        Me.tb_notification_extra.Visible = False
        '
        'uc_notifications
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(44, Byte), Integer), CType(CType(62, Byte), Integer), CType(CType(80, Byte), Integer))
        Me.Controls.Add(Me.tb_notification_extra)
        Me.Controls.Add(Me.btn_markAll)
        Me.Controls.Add(Me.pnl_message)
        Me.Controls.Add(Me.cmb_msg_category)
        Me.Controls.Add(Me.dgv_notify)
        Me.Name = "uc_notifications"
        Me.Size = New System.Drawing.Size(1348, 759)
        CType(Me.dgv_notify, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnl_message.ResumeLayout(False)
        Me.pnl_message.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents dgv_notify As DataGridView
    Friend WithEvents tb_msg_title As TextBox
    Friend WithEvents tb_msg_content As TextBox
    Friend WithEvents tb_action As Button
    Friend WithEvents tb_notifier As TextBox
    Friend WithEvents cmb_msg_category As ComboBox
    Friend WithEvents pnl_message As Panel
    Friend WithEvents Label3 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents Label1 As Label
    Friend WithEvents btn_markAll As Button
    Friend WithEvents tb_notification_extra As TextBox
End Class
