﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_cashier_manual
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle9 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle10 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle11 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle12 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.tb_total = New System.Windows.Forms.TextBox()
        Me.RadLabel12 = New Telerik.WinControls.UI.RadLabel()
        Me.btn_print = New System.Windows.Forms.Button()
        Me.cbCheck = New System.Windows.Forms.CheckBox()
        Me.pnlCheck = New System.Windows.Forms.Panel()
        Me.dtp_check_date = New System.Windows.Forms.DateTimePicker()
        Me.tb_check_order = New System.Windows.Forms.TextBox()
        Me.RadLabel23 = New Telerik.WinControls.UI.RadLabel()
        Me.RadLabel22 = New Telerik.WinControls.UI.RadLabel()
        Me.cbCash = New System.Windows.Forms.CheckBox()
        Me.txtAmountInWords = New System.Windows.Forms.TextBox()
        Me.Panel12 = New System.Windows.Forms.Panel()
        Me.btn_cancel = New System.Windows.Forms.Button()
        Me.btn_back = New System.Windows.Forms.Button()
        Me.RadLabel21 = New Telerik.WinControls.UI.RadLabel()
        Me.pnlCheck2 = New System.Windows.Forms.Panel()
        Me.dgv_cashier = New System.Windows.Forms.DataGridView()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtdate = New System.Windows.Forms.DateTimePicker()
        Me.txtOR = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.tb_payor = New System.Windows.Forms.TextBox()
        Me.RadLabel1 = New Telerik.WinControls.UI.RadLabel()
        Me.RadTitleBar1 = New Telerik.WinControls.UI.RadTitleBar()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.cmb_cashier_id = New System.Windows.Forms.ComboBox()
        Me.cmb_cashier = New System.Windows.Forms.ComboBox()
        Me.Label20 = New System.Windows.Forms.Label()
        CType(Me.RadLabel12, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlCheck.SuspendLayout()
        CType(Me.RadLabel23, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RadLabel22, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel12.SuspendLayout()
        CType(Me.RadLabel21, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgv_cashier, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RadLabel1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RadTitleBar1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Segoe UI", 12.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.White
        Me.Label2.Location = New System.Drawing.Point(57, 66)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(42, 21)
        Me.Label2.TabIndex = 136
        Me.Label2.Tag = "OR NO."
        Me.Label2.Text = "PHP"
        '
        'tb_total
        '
        Me.tb_total.BackColor = System.Drawing.Color.FromArgb(CType(CType(28, Byte), Integer), CType(CType(40, Byte), Integer), CType(CType(52, Byte), Integer))
        Me.tb_total.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.tb_total.Font = New System.Drawing.Font("Segoe UI", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tb_total.ForeColor = System.Drawing.Color.White
        Me.tb_total.Location = New System.Drawing.Point(100, 62)
        Me.tb_total.Name = "tb_total"
        Me.tb_total.ReadOnly = True
        Me.tb_total.Size = New System.Drawing.Size(197, 28)
        Me.tb_total.TabIndex = 93
        Me.tb_total.TabStop = False
        Me.tb_total.Text = "0"
        Me.tb_total.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'RadLabel12
        '
        Me.RadLabel12.Font = New System.Drawing.Font("Segoe UI", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadLabel12.ForeColor = System.Drawing.Color.White
        Me.RadLabel12.Location = New System.Drawing.Point(14, 67)
        Me.RadLabel12.Name = "RadLabel12"
        Me.RadLabel12.Size = New System.Drawing.Size(42, 18)
        Me.RadLabel12.TabIndex = 94
        Me.RadLabel12.Text = "TOTAL"
        '
        'btn_print
        '
        Me.btn_print.BackColor = System.Drawing.Color.FromArgb(CType(CType(218, Byte), Integer), CType(CType(144, Byte), Integer), CType(CType(27, Byte), Integer))
        Me.btn_print.FlatAppearance.BorderSize = 0
        Me.btn_print.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_print.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_print.ForeColor = System.Drawing.Color.White
        Me.btn_print.Location = New System.Drawing.Point(138, 12)
        Me.btn_print.Name = "btn_print"
        Me.btn_print.Size = New System.Drawing.Size(110, 35)
        Me.btn_print.TabIndex = 7
        Me.btn_print.Text = "Finish"
        Me.btn_print.UseVisualStyleBackColor = False
        '
        'cbCheck
        '
        Me.cbCheck.AutoSize = True
        Me.cbCheck.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbCheck.ForeColor = System.Drawing.Color.White
        Me.cbCheck.Location = New System.Drawing.Point(302, 453)
        Me.cbCheck.Name = "cbCheck"
        Me.cbCheck.Size = New System.Drawing.Size(88, 17)
        Me.cbCheck.TabIndex = 4
        Me.cbCheck.Text = "Check/PMO"
        Me.cbCheck.UseVisualStyleBackColor = True
        '
        'pnlCheck
        '
        Me.pnlCheck.BackColor = System.Drawing.Color.FromArgb(CType(CType(114, Byte), Integer), CType(CType(126, Byte), Integer), CType(CType(138, Byte), Integer))
        Me.pnlCheck.Controls.Add(Me.dtp_check_date)
        Me.pnlCheck.Controls.Add(Me.tb_check_order)
        Me.pnlCheck.Controls.Add(Me.RadLabel23)
        Me.pnlCheck.Location = New System.Drawing.Point(12, 480)
        Me.pnlCheck.Name = "pnlCheck"
        Me.pnlCheck.Size = New System.Drawing.Size(413, 71)
        Me.pnlCheck.TabIndex = 158
        Me.pnlCheck.Visible = False
        '
        'dtp_check_date
        '
        Me.dtp_check_date.CalendarFont = New System.Drawing.Font("Consolas", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtp_check_date.Cursor = System.Windows.Forms.Cursors.Hand
        Me.dtp_check_date.CustomFormat = ""
        Me.dtp_check_date.DropDownAlign = System.Windows.Forms.LeftRightAlignment.Right
        Me.dtp_check_date.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtp_check_date.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtp_check_date.Location = New System.Drawing.Point(292, 33)
        Me.dtp_check_date.Name = "dtp_check_date"
        Me.dtp_check_date.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.dtp_check_date.Size = New System.Drawing.Size(113, 22)
        Me.dtp_check_date.TabIndex = 91
        Me.dtp_check_date.TabStop = False
        '
        'tb_check_order
        '
        Me.tb_check_order.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.tb_check_order.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tb_check_order.Location = New System.Drawing.Point(13, 33)
        Me.tb_check_order.MaxLength = 40
        Me.tb_check_order.Name = "tb_check_order"
        Me.tb_check_order.Size = New System.Drawing.Size(273, 22)
        Me.tb_check_order.TabIndex = 89
        Me.tb_check_order.TabStop = False
        '
        'RadLabel23
        '
        Me.RadLabel23.Font = New System.Drawing.Font("Segoe UI", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadLabel23.ForeColor = System.Drawing.Color.White
        Me.RadLabel23.Location = New System.Drawing.Point(16, 6)
        Me.RadLabel23.Name = "RadLabel23"
        Me.RadLabel23.Size = New System.Drawing.Size(161, 18)
        Me.RadLabel23.TabIndex = 90
        Me.RadLabel23.Text = "Check/Money Order Number:"
        '
        'RadLabel22
        '
        Me.RadLabel22.Font = New System.Drawing.Font("Segoe UI", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadLabel22.ForeColor = System.Drawing.Color.DarkGray
        Me.RadLabel22.Location = New System.Drawing.Point(19, 453)
        Me.RadLabel22.Name = "RadLabel22"
        Me.RadLabel22.Size = New System.Drawing.Size(99, 18)
        Me.RadLabel22.TabIndex = 157
        Me.RadLabel22.Text = "Mode of Payment"
        '
        'cbCash
        '
        Me.cbCash.AutoSize = True
        Me.cbCash.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbCash.ForeColor = System.Drawing.Color.White
        Me.cbCash.Location = New System.Drawing.Point(204, 453)
        Me.cbCash.Name = "cbCash"
        Me.cbCash.Size = New System.Drawing.Size(51, 17)
        Me.cbCash.TabIndex = 3
        Me.cbCash.Text = "Cash"
        Me.cbCash.UseVisualStyleBackColor = True
        '
        'txtAmountInWords
        '
        Me.txtAmountInWords.BackColor = System.Drawing.Color.FromArgb(CType(CType(114, Byte), Integer), CType(CType(126, Byte), Integer), CType(CType(138, Byte), Integer))
        Me.txtAmountInWords.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtAmountInWords.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtAmountInWords.ForeColor = System.Drawing.Color.White
        Me.txtAmountInWords.Location = New System.Drawing.Point(12, 404)
        Me.txtAmountInWords.MaxLength = 100
        Me.txtAmountInWords.Name = "txtAmountInWords"
        Me.txtAmountInWords.Size = New System.Drawing.Size(632, 18)
        Me.txtAmountInWords.TabIndex = 153
        Me.txtAmountInWords.TabStop = False
        '
        'Panel12
        '
        Me.Panel12.BackColor = System.Drawing.Color.FromArgb(CType(CType(28, Byte), Integer), CType(CType(40, Byte), Integer), CType(CType(52, Byte), Integer))
        Me.Panel12.Controls.Add(Me.btn_cancel)
        Me.Panel12.Controls.Add(Me.btn_back)
        Me.Panel12.Controls.Add(Me.btn_print)
        Me.Panel12.Location = New System.Drawing.Point(581, 501)
        Me.Panel12.Name = "Panel12"
        Me.Panel12.Size = New System.Drawing.Size(385, 59)
        Me.Panel12.TabIndex = 152
        '
        'btn_cancel
        '
        Me.btn_cancel.BackColor = System.Drawing.Color.FromArgb(CType(CType(218, Byte), Integer), CType(CType(144, Byte), Integer), CType(CType(27, Byte), Integer))
        Me.btn_cancel.FlatAppearance.BorderSize = 0
        Me.btn_cancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_cancel.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_cancel.ForeColor = System.Drawing.Color.White
        Me.btn_cancel.Location = New System.Drawing.Point(257, 12)
        Me.btn_cancel.Name = "btn_cancel"
        Me.btn_cancel.Size = New System.Drawing.Size(110, 35)
        Me.btn_cancel.TabIndex = 8
        Me.btn_cancel.Text = "Cancel"
        Me.btn_cancel.UseVisualStyleBackColor = False
        '
        'btn_back
        '
        Me.btn_back.BackColor = System.Drawing.Color.FromArgb(CType(CType(218, Byte), Integer), CType(CType(144, Byte), Integer), CType(CType(27, Byte), Integer))
        Me.btn_back.FlatAppearance.BorderSize = 0
        Me.btn_back.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_back.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_back.ForeColor = System.Drawing.Color.White
        Me.btn_back.Location = New System.Drawing.Point(18, 12)
        Me.btn_back.Name = "btn_back"
        Me.btn_back.Size = New System.Drawing.Size(110, 35)
        Me.btn_back.TabIndex = 6
        Me.btn_back.Text = "Back to OP"
        Me.btn_back.UseVisualStyleBackColor = False
        '
        'RadLabel21
        '
        Me.RadLabel21.Font = New System.Drawing.Font("Segoe UI", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadLabel21.ForeColor = System.Drawing.Color.DarkGray
        Me.RadLabel21.Location = New System.Drawing.Point(20, 384)
        Me.RadLabel21.Name = "RadLabel21"
        Me.RadLabel21.Size = New System.Drawing.Size(100, 18)
        Me.RadLabel21.TabIndex = 154
        Me.RadLabel21.Text = "Amount in Words"
        '
        'pnlCheck2
        '
        Me.pnlCheck2.BackColor = System.Drawing.Color.FromArgb(CType(CType(114, Byte), Integer), CType(CType(126, Byte), Integer), CType(CType(138, Byte), Integer))
        Me.pnlCheck2.Location = New System.Drawing.Point(279, 445)
        Me.pnlCheck2.Name = "pnlCheck2"
        Me.pnlCheck2.Size = New System.Drawing.Size(146, 82)
        Me.pnlCheck2.TabIndex = 159
        Me.pnlCheck2.Visible = False
        '
        'dgv_cashier
        '
        Me.dgv_cashier.AllowUserToAddRows = False
        Me.dgv_cashier.AllowUserToDeleteRows = False
        Me.dgv_cashier.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.dgv_cashier.BackgroundColor = System.Drawing.Color.FromArgb(CType(CType(28, Byte), Integer), CType(CType(40, Byte), Integer), CType(CType(52, Byte), Integer))
        Me.dgv_cashier.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.dgv_cashier.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None
        DataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle9.BackColor = System.Drawing.Color.FromArgb(CType(CType(28, Byte), Integer), CType(CType(40, Byte), Integer), CType(CType(52, Byte), Integer))
        DataGridViewCellStyle9.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle9.ForeColor = System.Drawing.Color.White
        DataGridViewCellStyle9.SelectionBackColor = System.Drawing.Color.FromArgb(CType(CType(28, Byte), Integer), CType(CType(40, Byte), Integer), CType(CType(52, Byte), Integer))
        DataGridViewCellStyle9.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle9.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgv_cashier.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle9
        Me.dgv_cashier.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgv_cashier.ColumnHeadersVisible = False
        DataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle10.BackColor = System.Drawing.Color.FromArgb(CType(CType(28, Byte), Integer), CType(CType(40, Byte), Integer), CType(CType(52, Byte), Integer))
        DataGridViewCellStyle10.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle10.ForeColor = System.Drawing.Color.White
        DataGridViewCellStyle10.SelectionBackColor = System.Drawing.Color.FromArgb(CType(CType(28, Byte), Integer), CType(CType(40, Byte), Integer), CType(CType(52, Byte), Integer))
        DataGridViewCellStyle10.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle10.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgv_cashier.DefaultCellStyle = DataGridViewCellStyle10
        Me.dgv_cashier.GridColor = System.Drawing.Color.FromArgb(CType(CType(28, Byte), Integer), CType(CType(40, Byte), Integer), CType(CType(52, Byte), Integer))
        Me.dgv_cashier.Location = New System.Drawing.Point(12, 143)
        Me.dgv_cashier.Name = "dgv_cashier"
        Me.dgv_cashier.ReadOnly = True
        Me.dgv_cashier.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None
        DataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle11.BackColor = System.Drawing.Color.FromArgb(CType(CType(52, Byte), Integer), CType(CType(73, Byte), Integer), CType(CType(85, Byte), Integer))
        DataGridViewCellStyle11.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle11.ForeColor = System.Drawing.Color.White
        DataGridViewCellStyle11.SelectionBackColor = System.Drawing.Color.FromArgb(CType(CType(52, Byte), Integer), CType(CType(73, Byte), Integer), CType(CType(85, Byte), Integer))
        DataGridViewCellStyle11.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle11.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgv_cashier.RowHeadersDefaultCellStyle = DataGridViewCellStyle11
        Me.dgv_cashier.RowHeadersVisible = False
        DataGridViewCellStyle12.BackColor = System.Drawing.Color.FromArgb(CType(CType(52, Byte), Integer), CType(CType(73, Byte), Integer), CType(CType(85, Byte), Integer))
        DataGridViewCellStyle12.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle12.ForeColor = System.Drawing.Color.White
        Me.dgv_cashier.RowsDefaultCellStyle = DataGridViewCellStyle12
        Me.dgv_cashier.RowTemplate.DefaultCellStyle.BackColor = System.Drawing.Color.FromArgb(CType(CType(28, Byte), Integer), CType(CType(40, Byte), Integer), CType(CType(52, Byte), Integer))
        Me.dgv_cashier.RowTemplate.DefaultCellStyle.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dgv_cashier.Size = New System.Drawing.Size(996, 219)
        Me.dgv_cashier.TabIndex = 151
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Segoe UI", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.DarkGray
        Me.Label1.Location = New System.Drawing.Point(232, 124)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(106, 13)
        Me.Label1.TabIndex = 150
        Me.Label1.Text = "Details of Payment"
        '
        'txtdate
        '
        Me.txtdate.CalendarFont = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtdate.Cursor = System.Windows.Forms.Cursors.Hand
        Me.txtdate.CustomFormat = ""
        Me.txtdate.DropDownAlign = System.Windows.Forms.LeftRightAlignment.Right
        Me.txtdate.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtdate.Location = New System.Drawing.Point(707, 44)
        Me.txtdate.Name = "txtdate"
        Me.txtdate.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtdate.Size = New System.Drawing.Size(246, 22)
        Me.txtdate.TabIndex = 2
        '
        'txtOR
        '
        Me.txtOR.BackColor = System.Drawing.Color.FromArgb(CType(CType(28, Byte), Integer), CType(CType(40, Byte), Integer), CType(CType(52, Byte), Integer))
        Me.txtOR.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtOR.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtOR.ForeColor = System.Drawing.Color.White
        Me.txtOR.Location = New System.Drawing.Point(138, 40)
        Me.txtOR.MaxLength = 30
        Me.txtOR.Name = "txtOR"
        Me.txtOR.Size = New System.Drawing.Size(220, 26)
        Me.txtOR.TabIndex = 1
        Me.txtOR.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Segoe UI", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ForeColor = System.Drawing.Color.DarkGray
        Me.Label4.Location = New System.Drawing.Point(69, 47)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(43, 13)
        Me.Label4.TabIndex = 148
        Me.Label4.Tag = "OR NO."
        Me.Label4.Text = "OR No."
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Segoe UI", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ForeColor = System.Drawing.Color.DarkGray
        Me.Label3.Location = New System.Drawing.Point(666, 49)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(32, 13)
        Me.Label3.TabIndex = 147
        Me.Label3.Text = "Date"
        '
        'tb_payor
        '
        Me.tb_payor.BackColor = System.Drawing.Color.FromArgb(CType(CType(114, Byte), Integer), CType(CType(126, Byte), Integer), CType(CType(138, Byte), Integer))
        Me.tb_payor.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.tb_payor.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tb_payor.ForeColor = System.Drawing.Color.White
        Me.tb_payor.Location = New System.Drawing.Point(138, 83)
        Me.tb_payor.MaxLength = 100
        Me.tb_payor.Name = "tb_payor"
        Me.tb_payor.ReadOnly = True
        Me.tb_payor.Size = New System.Drawing.Size(815, 22)
        Me.tb_payor.TabIndex = 145
        Me.tb_payor.TabStop = False
        Me.tb_payor.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'RadLabel1
        '
        Me.RadLabel1.Font = New System.Drawing.Font("Segoe UI", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadLabel1.ForeColor = System.Drawing.Color.DarkGray
        Me.RadLabel1.Location = New System.Drawing.Point(75, 85)
        Me.RadLabel1.Name = "RadLabel1"
        Me.RadLabel1.Size = New System.Drawing.Size(37, 18)
        Me.RadLabel1.TabIndex = 146
        Me.RadLabel1.Text = "Payor"
        '
        'RadTitleBar1
        '
        Me.RadTitleBar1.Dock = System.Windows.Forms.DockStyle.Top
        Me.RadTitleBar1.Location = New System.Drawing.Point(0, 0)
        Me.RadTitleBar1.Name = "RadTitleBar1"
        Me.RadTitleBar1.Size = New System.Drawing.Size(1020, 23)
        Me.RadTitleBar1.TabIndex = 143
        Me.RadTitleBar1.TabStop = False
        Me.RadTitleBar1.Text = "NTC Region 10"
        CType(Me.RadTitleBar1.GetChildAt(0), Telerik.WinControls.UI.RadTitleBarElement).Text = "NTC Region 10"
        CType(Me.RadTitleBar1.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(3), Telerik.WinControls.UI.RadImageButtonElement).Visibility = Telerik.WinControls.ElementVisibility.Collapsed
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.FromArgb(CType(CType(28, Byte), Integer), CType(CType(40, Byte), Integer), CType(CType(52, Byte), Integer))
        Me.Panel1.Controls.Add(Me.Label2)
        Me.Panel1.Controls.Add(Me.tb_total)
        Me.Panel1.Controls.Add(Me.RadLabel12)
        Me.Panel1.Location = New System.Drawing.Point(705, 310)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(303, 96)
        Me.Panel1.TabIndex = 160
        '
        'cmb_cashier_id
        '
        Me.cmb_cashier_id.FormattingEnabled = True
        Me.cmb_cashier_id.Location = New System.Drawing.Point(777, 431)
        Me.cmb_cashier_id.Name = "cmb_cashier_id"
        Me.cmb_cashier_id.Size = New System.Drawing.Size(52, 21)
        Me.cmb_cashier_id.TabIndex = 198
        Me.cmb_cashier_id.TabStop = False
        Me.cmb_cashier_id.Visible = False
        '
        'cmb_cashier
        '
        Me.cmb_cashier.BackColor = System.Drawing.Color.FromArgb(CType(CType(28, Byte), Integer), CType(CType(40, Byte), Integer), CType(CType(52, Byte), Integer))
        Me.cmb_cashier.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmb_cashier.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.cmb_cashier.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmb_cashier.ForeColor = System.Drawing.Color.White
        Me.cmb_cashier.FormattingEnabled = True
        Me.cmb_cashier.Location = New System.Drawing.Point(713, 457)
        Me.cmb_cashier.Name = "cmb_cashier"
        Me.cmb_cashier.Size = New System.Drawing.Size(262, 21)
        Me.cmb_cashier.TabIndex = 5
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.BackColor = System.Drawing.Color.Transparent
        Me.Label20.Font = New System.Drawing.Font("Segoe UI", 9.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label20.ForeColor = System.Drawing.Color.DarkGray
        Me.Label20.Location = New System.Drawing.Point(713, 439)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(49, 15)
        Me.Label20.TabIndex = 196
        Me.Label20.Text = "Cashier"
        Me.Label20.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'frm_cashier_manual
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(44, Byte), Integer), CType(CType(62, Byte), Integer), CType(CType(80, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(1020, 575)
        Me.Controls.Add(Me.cmb_cashier_id)
        Me.Controls.Add(Me.cmb_cashier)
        Me.Controls.Add(Me.Label20)
        Me.Controls.Add(Me.cbCheck)
        Me.Controls.Add(Me.pnlCheck)
        Me.Controls.Add(Me.RadLabel22)
        Me.Controls.Add(Me.cbCash)
        Me.Controls.Add(Me.txtAmountInWords)
        Me.Controls.Add(Me.Panel12)
        Me.Controls.Add(Me.RadLabel21)
        Me.Controls.Add(Me.pnlCheck2)
        Me.Controls.Add(Me.dgv_cashier)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.txtdate)
        Me.Controls.Add(Me.txtOR)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.tb_payor)
        Me.Controls.Add(Me.RadLabel1)
        Me.Controls.Add(Me.RadTitleBar1)
        Me.Controls.Add(Me.Panel1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frm_cashier_manual"
        Me.ShowIcon = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "NTC Region 10"
        CType(Me.RadLabel12, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlCheck.ResumeLayout(False)
        Me.pnlCheck.PerformLayout()
        CType(Me.RadLabel23, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RadLabel22, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel12.ResumeLayout(False)
        CType(Me.RadLabel21, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgv_cashier, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RadLabel1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RadTitleBar1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label2 As Label
    Friend WithEvents tb_total As TextBox
    Friend WithEvents RadLabel12 As Telerik.WinControls.UI.RadLabel
    Friend WithEvents btn_print As Button
    Friend WithEvents cbCheck As CheckBox
    Friend WithEvents pnlCheck As Panel
    Friend WithEvents dtp_check_date As DateTimePicker
    Friend WithEvents tb_check_order As TextBox
    Friend WithEvents RadLabel23 As Telerik.WinControls.UI.RadLabel
    Friend WithEvents RadLabel22 As Telerik.WinControls.UI.RadLabel
    Friend WithEvents cbCash As CheckBox
    Friend WithEvents txtAmountInWords As TextBox
    Friend WithEvents Panel12 As Panel
    Friend WithEvents RadLabel21 As Telerik.WinControls.UI.RadLabel
    Friend WithEvents pnlCheck2 As Panel
    Friend WithEvents dgv_cashier As DataGridView
    Friend WithEvents Label1 As Label
    Friend WithEvents txtdate As DateTimePicker
    Friend WithEvents txtOR As TextBox
    Friend WithEvents Label4 As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents tb_payor As TextBox
    Friend WithEvents RadLabel1 As Telerik.WinControls.UI.RadLabel
    Friend WithEvents RadTitleBar1 As Telerik.WinControls.UI.RadTitleBar
    Friend WithEvents Panel1 As Panel
    Friend WithEvents btn_back As Button
    Friend WithEvents cmb_cashier_id As ComboBox
    Friend WithEvents cmb_cashier As ComboBox
    Friend WithEvents Label20 As Label
    Friend WithEvents btn_cancel As Button
End Class
