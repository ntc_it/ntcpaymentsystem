﻿Imports MySql.Data.MySqlClient

Public Class uc_notifications
    Dim query As String
    Dim cmd As MySqlCommand
    Dim reader As MySqlDataReader
    Dim message_type As Integer
    Dim notify_transact_id, n_ID, action_taken, endorse_id As String
    Dim rowNumber As Integer
    Public user_id, user_id_notifier, authority_level As String

    Public Sub load_notifications()
        Try
            Dim da As MySqlDataAdapter = New MySqlDataAdapter()
            Dim ds As DataSet = New DataSet
            Dim sql As MySqlCommand

            If cmb_msg_category.SelectedIndex = 0 Then
                Select Case authority_level
                    Case "1"
                        sql = New MySqlCommand("select n_ID as 'Message ID', message_title as 'Title', " &
                            "date_created as 'Date and Time Notified' from tbl_notifications where " &
                            "user_id_notified = '" & user_id & "' and ((user_opened = '0' and action_taken " &
                            "= '0') or (user_opened = '0' and action_taken = '1')) order by date_created ASC",
                            class_connection.con)
                    Case "2"
                        sql = New MySqlCommand("select n_ID as 'Message ID', message_title as 'Title', " &
                            "date_created as 'Date and Time Notified' from tbl_notifications where " &
                            "user_id_notified = '" & user_id & "' and ((user_opened = '0' and action_taken " &
                            "= '0') or (user_opened = '0' and action_taken = '1')) order by date_created ASC",
                            class_connection.con)
                    Case "3"
                        sql = New MySqlCommand("select n_ID as 'Message ID', message_title as 'Title', " &
                            "date_created as 'Date and Time Notified' from tbl_notifications where " &
                            "user_id_notified = '" & user_id & "' and ((user_opened = '0' and action_taken " &
                            "= '0') or (user_opened = '0' and action_taken = '1')) order by date_created ASC",
                            class_connection.con)
                    Case "4", "5"
                        sql = New MySqlCommand("select n_ID as 'Message ID', message_title as 'Title', " &
                            "date_created as 'Date and Time Notified' from tbl_notifications where " &
                            "user_id_notified = '" & user_id & "' and ((user_opened = '0' and " &
                            "action_taken = '0') or (user_opened = '0' and action_taken = '1')) order by " &
                            "date_created ASC", class_connection.con)
                    Case Else
                        sql = New MySqlCommand("")
                End Select

                btn_markAll.Visible = True
            ElseIf cmb_msg_category.SelectedIndex = 1 Then
                sql = New MySqlCommand("select n_ID as 'Message ID', message_title as " &
                    "'Title', date_created as 'Date and Time Notified' from tbl_notifications where " &
                    "user_id_notified = '" & user_id & "' and user_opened = '1' and action_taken = '1' " &
                    "order by date_created DESC", class_connection.con)

                btn_markAll.Visible = False
            ElseIf cmb_msg_category.SelectedIndex = 2 Then
                sql = New MySqlCommand("select n_ID as 'Message ID', message_title as " &
                        "'Title',date_created as 'Date and Time Notified' from tbl_notifications where " &
                        "user_id_notified = '" & user_id & "' and user_opened = '1' and action_taken = " &
                        "'0' order by date_created DESC", class_connection.con)

                btn_markAll.Visible = False
            End If

            da.SelectCommand = sql
            da.Fill(ds, "rec")
            dgv_notify.DataSource = ds
            dgv_notify.DataMember = "rec"

            dgv_notify.Columns(0).Width = 0
            dgv_notify.Columns("Title").Width = 300
            dgv_notify.Columns(2).DefaultCellStyle.Format = "MM/dd/yyyy hh:mm:ss tt"
            dgv_notify.Columns(0).Visible = False
        Catch ex As Exception
            MsgBox("Couldn't connect to server. Contact the IT Administrator for assistance.",
               MsgBoxStyle.Exclamation, "NTC Region 10")
            returnHome()
        End Try
    End Sub

    Public Sub setUserandAuthorLevel(ID, author_level)
        user_id = ID
        authority_level = author_level
    End Sub

    Private Sub uc_authorize_Load(sender As Object, e As EventArgs) Handles MyBase.Load

    End Sub

    Private Sub cmb_msg_category_Click(sender As Object, e As EventArgs) Handles cmb_msg_category.Click
        dashboard.RadCollapsiblePanel1.IsExpanded = False
        dashboard.loadNumberOfNotifications()
        dashboard.btn_my_notifications.BackColor = Color.FromArgb(249, 170, 51)
    End Sub

    Private Sub cmb_msg_category_SelectionChangeCommitted(sender As Object, e As EventArgs) _
        Handles cmb_msg_category.SelectionChangeCommitted
        dashboard.RadCollapsiblePanel1.IsExpanded = False
        dashboard.loadNumberOfNotifications()
        dashboard.btn_my_notifications.BackColor = Color.FromArgb(249, 170, 51)
        load_notifications()
    End Sub

    Private Sub btn_markAll_Click(sender As Object, e As EventArgs) Handles btn_markAll.Click
        class_connection.con.Open()
        query = "update tbl_notifications set user_opened = '1' where user_id_notified = '" & user_id &
            "'"
        cmd = New MySqlCommand(query, class_connection.con)
        cmd.ExecuteNonQuery()
        class_connection.con.Close()

        load_notifications()
    End Sub

    Private Sub tb_action_Click(sender As Object, e As EventArgs) Handles tb_action.Click
        dashboard.RadCollapsiblePanel1.IsExpanded = False

        open_form()
    End Sub

    Private Sub tb_notification_extra_Click(sender As Object, e As EventArgs) Handles tb_notification_extra.Click
        cmb_msg_category.SelectedIndex = 2
        dashboard.RadCollapsiblePanel1.IsExpanded = False
        dashboard.loadNumberOfNotifications()
        dashboard.btn_my_notifications.BackColor = Color.FromArgb(249, 170, 51)
        load_notifications()
    End Sub

    Sub open_form()
        ' For approver open form. For reviewing the SOA created by licenser.
        If message_type = 1 And action_taken = "0" Then
            frm_review_soa.transact_ID = notify_transact_id
            frm_review_soa.n_ID = n_ID
            frm_review_soa.notified_ID = user_id_notifier
            frm_review_soa.user_id = user_id
            frm_review_soa.message_type = message_type
            frm_review_soa.btn_action_one.Text = "Approve"
            frm_review_soa.btn_action_two.Text = "Disapprove"
            frm_review_soa.pnl_attachments.Visible = True
            frm_review_soa.Show()
            pnl_message.Visible = False

            load_notifications()
        ElseIf message_type = 1 And action_taken = "1" Then
            frm_review_soa.transact_ID = notify_transact_id
            frm_review_soa.n_ID = n_ID
            frm_review_soa.notified_ID = user_id_notifier
            frm_review_soa.user_id = user_id
            frm_review_soa.message_type = message_type
            frm_review_soa.btn_action_one.Text = "Approve"
            frm_review_soa.btn_action_two.Text = "Disapprove"
            frm_review_soa.pnl_attachments.Visible = True
            frm_review_soa.btn_action_one.Enabled = False
            frm_review_soa.btn_action_two.Enabled = False
            frm_review_soa.Show()
            pnl_message.Visible = False

            load_notifications()
        End If

        ' FOr licenser open form. To open SOA that is approved by Approver.
        If message_type = 2 Then
            frm_licenser_log.soa_id_transact = notify_transact_id
            frm_licenser_log.user_id = user_id
            frm_licenser_log.pnl_refresh.Visible = True
            frm_licenser_log.Show()
            pnl_message.Visible = False

            load_notifications()
        End If

        ' FOr licenser open form. To open SOA that is declined by Approver.
        If message_type = 3 Then
            frm_licenser_log.soa_id_transact = notify_transact_id
            frm_licenser_log.user_id = user_id
            frm_licenser_log.pnl_refresh.Visible = True
            frm_licenser_log.Show()
            pnl_message.Visible = False

            load_notifications()
        End If

        ' Accountant open form. For viewing SOA that is endorsed by licenser and creating OP.
        If message_type = 7 And action_taken = "0" Then
            frm_review_soa.transact_ID = notify_transact_id
            frm_review_soa.n_ID = n_ID      ' Notification ID.
            frm_review_soa.notified_ID = user_id_notifier
            frm_review_soa.user_id = user_id
            frm_review_soa.message_type = message_type
            frm_review_soa.authority_level = authority_level
            frm_review_soa.btn_action_one.Text = "Accept"
            frm_review_soa.btn_action_two.Text = "Decline"
            frm_review_soa.btn_action_one.Enabled = True

            frm_review_soa.Show()
            pnl_message.Visible = False

            load_notifications()
        ElseIf message_type = 7 And action_taken = "1" Then
            Try
                Dim isSOAAccepted As Boolean = False
                class_connection.con.Open()
                query = "select endorse_id from tbl_soa_endorse where soa_id = '" & notify_transact_id & "'"
                cmd = New MySqlCommand(query, class_connection.con)
                reader = cmd.ExecuteReader()

                If reader.Read Then
                    endorse_id = reader.GetString("endorse_id")

                    isSOAAccepted = True
                Else
                    MsgBox("Cannot open the declined SOA.", MsgBoxStyle.Exclamation, "NTC Region 10")
                    isSOAAccepted = False
                End If
                class_connection.con.Close()

                If isSOAAccepted Then
                    frm_accountant_log.endorse_id = endorse_id
                    frm_accountant_log.authority_level = authority_level
                    frm_accountant_log.Show()
                    pnl_message.Visible = False

                    load_notifications()
                End If

            Catch ex As MySqlException
                MsgBox("Couldn't connect to server.", MsgBoxStyle.Exclamation, "NTC Region 10")
            Catch ex As Exception
                MsgBox(ex.Message + "; " & ex.GetType.ToString)
            End Try
        End If

        ' Cashier open form. For creating the official receipt.
        If message_type = 8 And action_taken = "0" Then
            frm_cashier.transact_ID = notify_transact_id
            frm_cashier.n_ID = n_ID      ' Notification id.
            frm_cashier.user_id = user_id
            frm_cashier.authority_level = authority_level
            frm_cashier.Show()
            pnl_message.Visible = False

            load_notifications()
        ElseIf message_type = 8 And action_taken = "1" Then
            Try
                Dim transact_id_cashier As String = ""

                'retrieve user
                class_connection.con.Open()
                query = "select cashier_transact_id from tbl_soa_cashier where soa_id = '" & notify_transact_id &
                    "'"
                cmd = New MySqlCommand(query, class_connection.con)
                reader = cmd.ExecuteReader()

                If reader.Read Then
                    transact_id_cashier = reader.GetString("cashier_transact_id")
                End If

                class_connection.con.Close()

                frm_cashier_log.transact_id_cashier = transact_id_cashier
                frm_cashier_log.user_id = user_id
                frm_cashier_log.Show()
                pnl_message.Visible = False

                load_notifications()
            Catch ex As MySqlException
                MsgBox("Couldn't connect to server.", MsgBoxStyle.Exclamation, "NTC Region 10")
            Catch ex As Exception
                MsgBox(ex.Message + " ; " & ex.GetType.ToString)
            End Try
        End If

        ' Licenser open form. For complete transaction notification.
        If message_type = 9 And action_taken = "0" Then
            Try
                class_connection.con.Open()
                query = "update tbl_notifications Set action_taken = '1' where n_ID = '" & n_ID & "'"
                cmd = New MySqlCommand(query, class_connection.con)
                cmd.ExecuteNonQuery()
                class_connection.con.Close()

                frm_licenser_log.soa_id_transact = notify_transact_id
                frm_licenser_log.user_id = user_id
                frm_licenser_log.pnl_refresh.Visible = True
                frm_licenser_log.Show()
                pnl_message.Visible = False

                retrieve_number_of_unresolved()
                load_notifications()
            Catch ex As MySqlException
                MsgBox("Couldn't connect to server.", MsgBoxStyle.Exclamation, "NTC Region 10")
            Catch ex As Exception
                MsgBox(ex.Message + " ; " & ex.GetType.ToString)
            End Try
        ElseIf message_type = 9 And action_taken = "1" Then
            frm_licenser_log.soa_id_transact = notify_transact_id
            frm_licenser_log.user_id = user_id
            frm_licenser_log.pnl_refresh.Visible = True
            frm_licenser_log.Show()
            pnl_message.Visible = False

            load_notifications()
        End If

        ' For licenser open form. For viewing the SOA that is declined by Accountant.
        If message_type = 10 And action_taken = "0" Then
            Try
                class_connection.con.Open()
                query = "update tbl_notifications Set action_taken = '1' where n_ID = '" & n_ID & "'"
                cmd = New MySqlCommand(query, class_connection.con)
                cmd.ExecuteNonQuery()
                class_connection.con.Close()

                frm_licenser_log.soa_id_transact = notify_transact_id
                frm_licenser_log.user_id = user_id
                frm_licenser_log.pnl_refresh.Visible = True
                frm_licenser_log.Show()
                pnl_message.Visible = False

                load_notifications()
            Catch ex As MySqlException
                MsgBox("Couldn't connect to server.", MsgBoxStyle.Exclamation, "NTC Region 10")
            Catch ex As Exception
                MsgBox(ex.Message + " ; " & ex.GetType.ToString)
            End Try
        ElseIf message_type = 10 And action_taken = "1" Then
            frm_licenser_log.soa_id_transact = notify_transact_id
            frm_licenser_log.user_id = user_id
            frm_licenser_log.pnl_refresh.Visible = True
            frm_licenser_log.Show()
            pnl_message.Visible = False

            load_notifications()
        End If
    End Sub

    Private Sub dgv_notify_CellClick(sender As Object, e As DataGridViewCellEventArgs) Handles _
        dgv_notify.CellClick
        dashboard.btn_my_notifications.BackColor = Color.FromArgb(249, 170, 51)
        dashboard.loadNumberOfNotifications()
        dashboard.RadCollapsiblePanel1.IsExpanded = False

        rowNumber = e.RowIndex
        open_notification_message()
    End Sub

    Public Sub retrieve_number_of_unresolved()
        Try
            class_connection.con.Open()
            query = "select count(n_ID) from tbl_notifications where user_id_notified = '" & user_id &
            "' and user_opened = '1' and action_taken = " &
                    "'0' order by date_created DESC"
            cmd = New MySqlCommand(query, class_connection.con)
            reader = cmd.ExecuteReader()

            If reader.Read Then
                tb_notification_extra.Visible = False

                If reader.GetInt16("count(n_ID)") = 1 Then
                    tb_notification_extra.Text = "Attention! " & reader.GetString("count(n_ID)") &
                    " notification needs your input. Go to unresolved."
                    tb_notification_extra.Visible = True
                ElseIf reader.GetInt16("count(n_ID)") > 1 Then
                    tb_notification_extra.Text = "Attention! " & reader.GetString("count(n_ID)") &
                    " notifications need your input.  Go to unresolved."
                    tb_notification_extra.Visible = True
                End If

            End If
            class_connection.con.Close()
        Catch ex As Exception
            class_connection.con.Close()
            'MsgBox("Couldn't connect to server. Contact the IT Administrator for assistance.",
            'MsgBoxStyle.Exclamation, "NTC Region 10")
            MsgBox(ex.Message)
            'returnHome()
        End Try
    End Sub

    Sub open_notification_message()
        If rowNumber >= 0 Then
            Dim row As DataGridViewRow
            row = dgv_notify.Rows(rowNumber)
            Try
                class_connection.con.Open()
                query = "update tbl_notifications Set user_opened = '1' where n_ID = '" &
                    row.Cells("Message ID").Value.ToString & "'"
                cmd = New MySqlCommand(query, class_connection.con)
                cmd.ExecuteNonQuery()
                class_connection.con.Close()

                class_connection.con.Open()
                query = "select n_ID as 'Message ID', message_title as 'Title', message_content, " &
                    "transaction_id, user_id_notifier, action_taken, message_type from " &
                    "tbl_notifications where n_ID = '" & row.Cells("Message ID").Value.ToString & "'"
                cmd = New MySqlCommand(query, class_connection.con)
                reader = cmd.ExecuteReader()

                If reader.Read Then
                    tb_msg_title.Text = reader.GetString("Title")
                    tb_msg_content.Text = reader.GetString("message_content")
                    user_id_notifier = reader.GetString("user_id_notifier")
                    action_taken = reader.GetString("action_taken")
                    notify_transact_id = reader.GetString("transaction_id")
                    n_ID = reader.GetString("Message ID")
                    message_type = reader.GetInt32("message_type")
                End If
                class_connection.con.Close()

                class_connection.con.Open()
                query = "select full_name from tbl_user where user_id = '" & user_id_notifier & "'"
                cmd = New MySqlCommand(query, class_connection.con)
                reader = cmd.ExecuteReader()

                If reader.Read Then
                    tb_notifier.Text = reader.GetString("full_name")
                End If
                class_connection.con.Close()
            Catch ex As MySqlException
                MsgBox("Couldn't connect to server. Contact the IT Administrator for assistance.",
                       MsgBoxStyle.Exclamation, "NTC Region 10")
                returnHome()
            Catch ex As Exception
                MsgBox(ex.Message + " ; " & ex.GetType.ToString)
            End Try

            ' Notification for approver.
            If message_type = 1 Then
                tb_action.Text = "Review"
                tb_action.Enabled = True
            End If

            ' Notification for SOA approved by approver.
            If message_type = 2 Then
                ' Update action taken to 1 if notification type is approve.
                Try
                    class_connection.con.Open()
                    query = "update tbl_notifications Set action_taken = '1' where n_ID = '" &
                        row.Cells("Message ID").Value.ToString & "'"
                    cmd = New MySqlCommand(query, class_connection.con)
                    cmd.ExecuteNonQuery()
                    class_connection.con.Close()

                    tb_action.Enabled = True
                    tb_action.Text = "View"
                    class_connection.con.Close()
                Catch ex As MySqlException
                    MsgBox("Couldn't connect to server. Contact the IT Administrator for assistance.",
                        MsgBoxStyle.Exclamation, "NTC Region 10")

                    returnHome()
                Catch ex As Exception
                    MsgBox(ex.Message + " ; " & ex.GetType.ToString)
                End Try
            End If

            ' Notification for SOA disapproved by approver.
            If message_type = 3 Then
                Try
                    'update action taken to 1 if notification type is disapprove
                    class_connection.con.Open()
                    query = "update tbl_notifications Set action_taken = '1' where n_ID = '" &
                        row.Cells("Message ID").Value.ToString & "'"
                    cmd = New MySqlCommand(query, class_connection.con)
                    cmd.ExecuteNonQuery()
                    class_connection.con.Close()

                    tb_action.Enabled = True
                    tb_action.Text = "View"
                    class_connection.con.Close()
                Catch ex As MySqlException
                    MsgBox("Couldn't connect to server. Contact the IT Administrator for assistance.",
                        MsgBoxStyle.Exclamation, "NTC Region 10")

                    returnHome()
                Catch ex As Exception
                    MsgBox(ex.Message + " ; " & ex.GetType.ToString)
                End Try
            End If

            ' Endorse Notification.
            If message_type = 7 Then
                tb_action.Text = "Open"
            End If

            ' Cashier Notification.
            If message_type = 8 Then
                tb_action.Text = "Open"
            End If

            ' SOA Paid Notification.
            If message_type = 9 Then
                tb_action.Enabled = True
                tb_action.Text = "View"
            End If

            ' SOA declined by Accountant.
            If message_type = 10 Then
                tb_action.Enabled = True
                tb_action.Text = "View"
            End If

            pnl_message.Visible = True

            If cmb_msg_category.Text = "Unread" Then
                load_notifications()
            End If
        End If
    End Sub

    Sub returnHome()
        Dim dashboard_home As New dashboard_home

        dashboard.pnl_dashboard.Controls.Clear()
        dashboard.pnl_dashboard.Controls.Add(dashboard_home)
    End Sub
End Class
