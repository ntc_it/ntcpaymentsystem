﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_accountant
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.RadTitleBar1 = New Telerik.WinControls.UI.RadTitleBar()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.tb_payor = New System.Windows.Forms.TextBox()
        Me.tb_address = New System.Windows.Forms.TextBox()
        Me.tb_word_figure = New System.Windows.Forms.TextBox()
        Me.tb_purpose_one = New System.Windows.Forms.TextBox()
        Me.tb_purpose_two = New System.Windows.Forms.TextBox()
        Me.tb_per_bill = New System.Windows.Forms.TextBox()
        Me.tb_dated = New System.Windows.Forms.TextBox()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.tb_payment = New System.Windows.Forms.TextBox()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.tb_accountant = New System.Windows.Forms.TextBox()
        Me.tb_fund_cluster = New System.Windows.Forms.TextBox()
        Me.tb_serial_num = New System.Windows.Forms.TextBox()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.btn_soa = New System.Windows.Forms.Button()
        Me.btn_print = New System.Windows.Forms.Button()
        Me.tb_date_created = New System.Windows.Forms.TextBox()
        Me.BunifuSeparator9 = New Bunifu.Framework.UI.BunifuSeparator()
        Me.tb_bank_num = New System.Windows.Forms.TextBox()
        Me.tb_bank_name = New System.Windows.Forms.TextBox()
        Me.tb_bank_payment = New System.Windows.Forms.TextBox()
        Me.tb_bank_total = New System.Windows.Forms.TextBox()
        Me.Label21 = New System.Windows.Forms.Label()
        Me.Label22 = New System.Windows.Forms.Label()
        Me.Label24 = New System.Windows.Forms.Label()
        Me.tb_bank_payment_two = New System.Windows.Forms.TextBox()
        Me.tb_bank_name_two = New System.Windows.Forms.TextBox()
        Me.tb_bank_num_two = New System.Windows.Forms.TextBox()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.pnl_finish = New System.Windows.Forms.Panel()
        Me.btn_finish = New System.Windows.Forms.Button()
        CType(Me.RadTitleBar1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        Me.pnl_finish.SuspendLayout()
        Me.SuspendLayout()
        '
        'RadTitleBar1
        '
        Me.RadTitleBar1.Dock = System.Windows.Forms.DockStyle.Top
        Me.RadTitleBar1.Location = New System.Drawing.Point(0, 0)
        Me.RadTitleBar1.Name = "RadTitleBar1"
        Me.RadTitleBar1.Size = New System.Drawing.Size(840, 23)
        Me.RadTitleBar1.TabIndex = 1
        Me.RadTitleBar1.TabStop = False
        Me.RadTitleBar1.Text = "NTC Region 10"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Segoe UI", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.DarkGray
        Me.Label2.Location = New System.Drawing.Point(26, 33)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(78, 13)
        Me.Label2.TabIndex = 17
        Me.Label2.Text = "General Fund"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Segoe UI", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ForeColor = System.Drawing.Color.DarkGray
        Me.Label3.Location = New System.Drawing.Point(594, 33)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(43, 13)
        Me.Label3.TabIndex = 18
        Me.Label3.Text = "OP No."
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Segoe UI", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ForeColor = System.Drawing.Color.DarkGray
        Me.Label4.Location = New System.Drawing.Point(594, 81)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(32, 13)
        Me.Label4.TabIndex = 19
        Me.Label4.Text = "Date"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.ForeColor = System.Drawing.Color.White
        Me.Label5.Location = New System.Drawing.Point(277, 140)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(278, 25)
        Me.Label5.TabIndex = 20
        Me.Label5.Text = "Preview for Order of Payment"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.ForeColor = System.Drawing.Color.White
        Me.Label8.Location = New System.Drawing.Point(56, 198)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(225, 15)
        Me.Label8.TabIndex = 23
        Me.Label8.Text = "Please issue Official Receipt in favor of "
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.ForeColor = System.Drawing.Color.White
        Me.Label9.Location = New System.Drawing.Point(22, 284)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(100, 15)
        Me.Label9.TabIndex = 24
        Me.Label9.Text = "in the amount of"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.ForeColor = System.Drawing.Color.White
        Me.Label10.Location = New System.Drawing.Point(23, 323)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(113, 15)
        Me.Label10.TabIndex = 25
        Me.Label10.Text = "for the payment of"
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.ForeColor = System.Drawing.Color.White
        Me.Label11.Location = New System.Drawing.Point(23, 399)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(194, 15)
        Me.Label11.TabIndex = 26
        Me.Label11.Text = "per STATEMENT OF ACCOUNT No."
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label12.ForeColor = System.Drawing.Color.White
        Me.Label12.Location = New System.Drawing.Point(441, 399)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(39, 15)
        Me.Label12.TabIndex = 27
        Me.Label12.Text = "dated"
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Font = New System.Drawing.Font("Segoe UI", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label14.ForeColor = System.Drawing.Color.DarkGray
        Me.Label14.Location = New System.Drawing.Point(496, 216)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(93, 13)
        Me.Label14.TabIndex = 29
        Me.Label14.Text = "(Name of Payor)"
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Font = New System.Drawing.Font("Segoe UI", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label16.ForeColor = System.Drawing.Color.DarkGray
        Me.Label16.Location = New System.Drawing.Point(350, 259)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(138, 13)
        Me.Label16.TabIndex = 30
        Me.Label16.Text = "(Address/Office of Payor)"
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Font = New System.Drawing.Font("Segoe UI", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label17.ForeColor = System.Drawing.Color.DarkGray
        Me.Label17.Location = New System.Drawing.Point(391, 374)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(57, 13)
        Me.Label17.TabIndex = 31
        Me.Label17.Text = "(Purpose)"
        '
        'tb_payor
        '
        Me.tb_payor.BackColor = System.Drawing.Color.FromArgb(CType(CType(114, Byte), Integer), CType(CType(126, Byte), Integer), CType(CType(138, Byte), Integer))
        Me.tb_payor.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.tb_payor.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tb_payor.ForeColor = System.Drawing.Color.White
        Me.tb_payor.Location = New System.Drawing.Point(287, 194)
        Me.tb_payor.MaxLength = 120
        Me.tb_payor.Name = "tb_payor"
        Me.tb_payor.ReadOnly = True
        Me.tb_payor.Size = New System.Drawing.Size(525, 22)
        Me.tb_payor.TabIndex = 90
        Me.tb_payor.TabStop = False
        Me.tb_payor.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'tb_address
        '
        Me.tb_address.BackColor = System.Drawing.Color.FromArgb(CType(CType(114, Byte), Integer), CType(CType(126, Byte), Integer), CType(CType(138, Byte), Integer))
        Me.tb_address.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.tb_address.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tb_address.ForeColor = System.Drawing.Color.White
        Me.tb_address.Location = New System.Drawing.Point(25, 237)
        Me.tb_address.MaxLength = 120
        Me.tb_address.Name = "tb_address"
        Me.tb_address.ReadOnly = True
        Me.tb_address.Size = New System.Drawing.Size(787, 22)
        Me.tb_address.TabIndex = 91
        Me.tb_address.TabStop = False
        Me.tb_address.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'tb_word_figure
        '
        Me.tb_word_figure.BackColor = System.Drawing.Color.FromArgb(CType(CType(114, Byte), Integer), CType(CType(126, Byte), Integer), CType(CType(138, Byte), Integer))
        Me.tb_word_figure.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.tb_word_figure.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tb_word_figure.ForeColor = System.Drawing.Color.White
        Me.tb_word_figure.Location = New System.Drawing.Point(128, 284)
        Me.tb_word_figure.MaxLength = 120
        Me.tb_word_figure.Name = "tb_word_figure"
        Me.tb_word_figure.ReadOnly = True
        Me.tb_word_figure.Size = New System.Drawing.Size(541, 15)
        Me.tb_word_figure.TabIndex = 92
        Me.tb_word_figure.TabStop = False
        Me.tb_word_figure.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'tb_purpose_one
        '
        Me.tb_purpose_one.BackColor = System.Drawing.Color.FromArgb(CType(CType(114, Byte), Integer), CType(CType(126, Byte), Integer), CType(CType(138, Byte), Integer))
        Me.tb_purpose_one.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.tb_purpose_one.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tb_purpose_one.ForeColor = System.Drawing.Color.White
        Me.tb_purpose_one.Location = New System.Drawing.Point(142, 323)
        Me.tb_purpose_one.MaxLength = 120
        Me.tb_purpose_one.Name = "tb_purpose_one"
        Me.tb_purpose_one.ReadOnly = True
        Me.tb_purpose_one.Size = New System.Drawing.Size(670, 15)
        Me.tb_purpose_one.TabIndex = 94
        Me.tb_purpose_one.TabStop = False
        Me.tb_purpose_one.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'tb_purpose_two
        '
        Me.tb_purpose_two.BackColor = System.Drawing.Color.FromArgb(CType(CType(114, Byte), Integer), CType(CType(126, Byte), Integer), CType(CType(138, Byte), Integer))
        Me.tb_purpose_two.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.tb_purpose_two.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tb_purpose_two.ForeColor = System.Drawing.Color.White
        Me.tb_purpose_two.Location = New System.Drawing.Point(26, 359)
        Me.tb_purpose_two.MaxLength = 120
        Me.tb_purpose_two.Name = "tb_purpose_two"
        Me.tb_purpose_two.ReadOnly = True
        Me.tb_purpose_two.Size = New System.Drawing.Size(786, 15)
        Me.tb_purpose_two.TabIndex = 95
        Me.tb_purpose_two.TabStop = False
        Me.tb_purpose_two.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'tb_per_bill
        '
        Me.tb_per_bill.BackColor = System.Drawing.Color.FromArgb(CType(CType(114, Byte), Integer), CType(CType(126, Byte), Integer), CType(CType(138, Byte), Integer))
        Me.tb_per_bill.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.tb_per_bill.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tb_per_bill.ForeColor = System.Drawing.Color.White
        Me.tb_per_bill.Location = New System.Drawing.Point(223, 395)
        Me.tb_per_bill.MaxLength = 120
        Me.tb_per_bill.Name = "tb_per_bill"
        Me.tb_per_bill.ReadOnly = True
        Me.tb_per_bill.Size = New System.Drawing.Size(209, 22)
        Me.tb_per_bill.TabIndex = 96
        Me.tb_per_bill.TabStop = False
        Me.tb_per_bill.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'tb_dated
        '
        Me.tb_dated.BackColor = System.Drawing.Color.FromArgb(CType(CType(114, Byte), Integer), CType(CType(126, Byte), Integer), CType(CType(138, Byte), Integer))
        Me.tb_dated.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.tb_dated.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tb_dated.ForeColor = System.Drawing.Color.White
        Me.tb_dated.Location = New System.Drawing.Point(486, 395)
        Me.tb_dated.MaxLength = 120
        Me.tb_dated.Name = "tb_dated"
        Me.tb_dated.ReadOnly = True
        Me.tb_dated.Size = New System.Drawing.Size(172, 22)
        Me.tb_dated.TabIndex = 97
        Me.tb_dated.TabStop = False
        Me.tb_dated.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Font = New System.Drawing.Font("Segoe UI", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label15.ForeColor = System.Drawing.Color.DarkGray
        Me.Label15.Location = New System.Drawing.Point(172, 479)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(25, 13)
        Me.Label15.TabIndex = 98
        Me.Label15.Text = "No."
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Font = New System.Drawing.Font("Segoe UI", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label18.ForeColor = System.Drawing.Color.DarkGray
        Me.Label18.Location = New System.Drawing.Point(367, 479)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(82, 13)
        Me.Label18.TabIndex = 99
        Me.Label18.Text = "Name of Bank"
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.Font = New System.Drawing.Font("Segoe UI", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label19.ForeColor = System.Drawing.Color.DarkGray
        Me.Label19.Location = New System.Drawing.Point(622, 479)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(49, 13)
        Me.Label19.TabIndex = 100
        Me.Label19.Text = "Amount"
        '
        'tb_payment
        '
        Me.tb_payment.BackColor = System.Drawing.Color.FromArgb(CType(CType(114, Byte), Integer), CType(CType(126, Byte), Integer), CType(CType(138, Byte), Integer))
        Me.tb_payment.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.tb_payment.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tb_payment.ForeColor = System.Drawing.Color.White
        Me.tb_payment.Location = New System.Drawing.Point(705, 280)
        Me.tb_payment.MaxLength = 120
        Me.tb_payment.Name = "tb_payment"
        Me.tb_payment.ReadOnly = True
        Me.tb_payment.Size = New System.Drawing.Size(107, 22)
        Me.tb_payment.TabIndex = 110
        Me.tb_payment.TabStop = False
        Me.tb_payment.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.Font = New System.Drawing.Font("Segoe UI", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label20.ForeColor = System.Drawing.Color.DarkGray
        Me.Label20.Location = New System.Drawing.Point(614, 667)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(76, 13)
        Me.Label20.TabIndex = 111
        Me.Label20.Text = "Accountant II"
        '
        'tb_accountant
        '
        Me.tb_accountant.BackColor = System.Drawing.Color.FromArgb(CType(CType(44, Byte), Integer), CType(CType(62, Byte), Integer), CType(CType(80, Byte), Integer))
        Me.tb_accountant.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.tb_accountant.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tb_accountant.ForeColor = System.Drawing.Color.White
        Me.tb_accountant.Location = New System.Drawing.Point(539, 642)
        Me.tb_accountant.MaxLength = 120
        Me.tb_accountant.Name = "tb_accountant"
        Me.tb_accountant.ReadOnly = True
        Me.tb_accountant.Size = New System.Drawing.Size(226, 22)
        Me.tb_accountant.TabIndex = 112
        Me.tb_accountant.TabStop = False
        Me.tb_accountant.Text = "                                                                     "
        Me.tb_accountant.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'tb_fund_cluster
        '
        Me.tb_fund_cluster.BackColor = System.Drawing.Color.FromArgb(CType(CType(28, Byte), Integer), CType(CType(40, Byte), Integer), CType(CType(52, Byte), Integer))
        Me.tb_fund_cluster.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.tb_fund_cluster.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tb_fund_cluster.ForeColor = System.Drawing.Color.White
        Me.tb_fund_cluster.Location = New System.Drawing.Point(26, 49)
        Me.tb_fund_cluster.MaxLength = 30
        Me.tb_fund_cluster.Name = "tb_fund_cluster"
        Me.tb_fund_cluster.Size = New System.Drawing.Size(218, 22)
        Me.tb_fund_cluster.TabIndex = 114
        Me.tb_fund_cluster.TabStop = False
        Me.tb_fund_cluster.Text = "Code 101"
        Me.tb_fund_cluster.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'tb_serial_num
        '
        Me.tb_serial_num.BackColor = System.Drawing.Color.FromArgb(CType(CType(114, Byte), Integer), CType(CType(126, Byte), Integer), CType(CType(138, Byte), Integer))
        Me.tb_serial_num.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.tb_serial_num.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tb_serial_num.ForeColor = System.Drawing.Color.White
        Me.tb_serial_num.Location = New System.Drawing.Point(594, 49)
        Me.tb_serial_num.MaxLength = 120
        Me.tb_serial_num.Name = "tb_serial_num"
        Me.tb_serial_num.ReadOnly = True
        Me.tb_serial_num.Size = New System.Drawing.Size(218, 22)
        Me.tb_serial_num.TabIndex = 115
        Me.tb_serial_num.TabStop = False
        Me.tb_serial_num.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.FromArgb(CType(CType(28, Byte), Integer), CType(CType(40, Byte), Integer), CType(CType(52, Byte), Integer))
        Me.Panel1.Controls.Add(Me.btn_soa)
        Me.Panel1.Controls.Add(Me.btn_print)
        Me.Panel1.Location = New System.Drawing.Point(25, 628)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(256, 58)
        Me.Panel1.TabIndex = 117
        '
        'btn_soa
        '
        Me.btn_soa.BackColor = System.Drawing.Color.FromArgb(CType(CType(218, Byte), Integer), CType(CType(144, Byte), Integer), CType(CType(27, Byte), Integer))
        Me.btn_soa.FlatAppearance.BorderSize = 0
        Me.btn_soa.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_soa.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_soa.ForeColor = System.Drawing.Color.White
        Me.btn_soa.Location = New System.Drawing.Point(132, 12)
        Me.btn_soa.Name = "btn_soa"
        Me.btn_soa.Size = New System.Drawing.Size(110, 35)
        Me.btn_soa.TabIndex = 101
        Me.btn_soa.Text = "View SOA"
        Me.btn_soa.UseVisualStyleBackColor = False
        '
        'btn_print
        '
        Me.btn_print.BackColor = System.Drawing.Color.FromArgb(CType(CType(218, Byte), Integer), CType(CType(144, Byte), Integer), CType(CType(27, Byte), Integer))
        Me.btn_print.FlatAppearance.BorderSize = 0
        Me.btn_print.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_print.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_print.ForeColor = System.Drawing.Color.White
        Me.btn_print.Location = New System.Drawing.Point(13, 12)
        Me.btn_print.Name = "btn_print"
        Me.btn_print.Size = New System.Drawing.Size(110, 35)
        Me.btn_print.TabIndex = 98
        Me.btn_print.Text = "Print"
        Me.btn_print.UseVisualStyleBackColor = False
        '
        'tb_date_created
        '
        Me.tb_date_created.BackColor = System.Drawing.Color.FromArgb(CType(CType(114, Byte), Integer), CType(CType(126, Byte), Integer), CType(CType(138, Byte), Integer))
        Me.tb_date_created.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.tb_date_created.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tb_date_created.ForeColor = System.Drawing.Color.White
        Me.tb_date_created.Location = New System.Drawing.Point(594, 97)
        Me.tb_date_created.MaxLength = 120
        Me.tb_date_created.Name = "tb_date_created"
        Me.tb_date_created.ReadOnly = True
        Me.tb_date_created.Size = New System.Drawing.Size(218, 22)
        Me.tb_date_created.TabIndex = 118
        Me.tb_date_created.TabStop = False
        Me.tb_date_created.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'BunifuSeparator9
        '
        Me.BunifuSeparator9.BackColor = System.Drawing.Color.Transparent
        Me.BunifuSeparator9.LineColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.BunifuSeparator9.LineThickness = 2
        Me.BunifuSeparator9.Location = New System.Drawing.Point(537, 660)
        Me.BunifuSeparator9.Name = "BunifuSeparator9"
        Me.BunifuSeparator9.Size = New System.Drawing.Size(230, 10)
        Me.BunifuSeparator9.TabIndex = 127
        Me.BunifuSeparator9.Transparency = 255
        Me.BunifuSeparator9.Vertical = False
        '
        'tb_bank_num
        '
        Me.tb_bank_num.BackColor = System.Drawing.Color.FromArgb(CType(CType(28, Byte), Integer), CType(CType(40, Byte), Integer), CType(CType(52, Byte), Integer))
        Me.tb_bank_num.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.tb_bank_num.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tb_bank_num.ForeColor = System.Drawing.Color.White
        Me.tb_bank_num.Location = New System.Drawing.Point(94, 497)
        Me.tb_bank_num.MaxLength = 120
        Me.tb_bank_num.Name = "tb_bank_num"
        Me.tb_bank_num.Size = New System.Drawing.Size(179, 18)
        Me.tb_bank_num.TabIndex = 128
        Me.tb_bank_num.Text = "3402-2642-40"
        Me.tb_bank_num.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'tb_bank_name
        '
        Me.tb_bank_name.BackColor = System.Drawing.Color.FromArgb(CType(CType(28, Byte), Integer), CType(CType(40, Byte), Integer), CType(CType(52, Byte), Integer))
        Me.tb_bank_name.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.tb_bank_name.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tb_bank_name.ForeColor = System.Drawing.Color.White
        Me.tb_bank_name.Location = New System.Drawing.Point(281, 497)
        Me.tb_bank_name.MaxLength = 120
        Me.tb_bank_name.Name = "tb_bank_name"
        Me.tb_bank_name.Size = New System.Drawing.Size(268, 18)
        Me.tb_bank_name.TabIndex = 129
        Me.tb_bank_name.Text = "Land Bank of the Philippines"
        Me.tb_bank_name.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'tb_bank_payment
        '
        Me.tb_bank_payment.BackColor = System.Drawing.Color.FromArgb(CType(CType(114, Byte), Integer), CType(CType(126, Byte), Integer), CType(CType(138, Byte), Integer))
        Me.tb_bank_payment.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.tb_bank_payment.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tb_bank_payment.ForeColor = System.Drawing.Color.White
        Me.tb_bank_payment.Location = New System.Drawing.Point(557, 497)
        Me.tb_bank_payment.Name = "tb_bank_payment"
        Me.tb_bank_payment.ReadOnly = True
        Me.tb_bank_payment.Size = New System.Drawing.Size(179, 18)
        Me.tb_bank_payment.TabIndex = 130
        Me.tb_bank_payment.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'tb_bank_total
        '
        Me.tb_bank_total.BackColor = System.Drawing.Color.FromArgb(CType(CType(114, Byte), Integer), CType(CType(126, Byte), Integer), CType(CType(138, Byte), Integer))
        Me.tb_bank_total.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.tb_bank_total.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tb_bank_total.ForeColor = System.Drawing.Color.White
        Me.tb_bank_total.Location = New System.Drawing.Point(557, 571)
        Me.tb_bank_total.Name = "tb_bank_total"
        Me.tb_bank_total.ReadOnly = True
        Me.tb_bank_total.Size = New System.Drawing.Size(179, 18)
        Me.tb_bank_total.TabIndex = 131
        Me.tb_bank_total.Text = "0"
        Me.tb_bank_total.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label21
        '
        Me.Label21.AutoSize = True
        Me.Label21.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label21.ForeColor = System.Drawing.Color.Silver
        Me.Label21.Location = New System.Drawing.Point(522, 573)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(29, 13)
        Me.Label21.TabIndex = 132
        Me.Label21.Text = "PHP"
        '
        'Label22
        '
        Me.Label22.AutoSize = True
        Me.Label22.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label22.ForeColor = System.Drawing.Color.Silver
        Me.Label22.Location = New System.Drawing.Point(117, 573)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(40, 13)
        Me.Label22.TabIndex = 133
        Me.Label22.Text = "TOTAL"
        '
        'Label24
        '
        Me.Label24.AutoSize = True
        Me.Label24.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label24.ForeColor = System.Drawing.Color.White
        Me.Label24.Location = New System.Drawing.Point(676, 284)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(30, 15)
        Me.Label24.TabIndex = 135
        Me.Label24.Text = "PHP"
        '
        'tb_bank_payment_two
        '
        Me.tb_bank_payment_two.BackColor = System.Drawing.Color.FromArgb(CType(CType(114, Byte), Integer), CType(CType(126, Byte), Integer), CType(CType(138, Byte), Integer))
        Me.tb_bank_payment_two.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.tb_bank_payment_two.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tb_bank_payment_two.ForeColor = System.Drawing.Color.White
        Me.tb_bank_payment_two.Location = New System.Drawing.Point(557, 521)
        Me.tb_bank_payment_two.Name = "tb_bank_payment_two"
        Me.tb_bank_payment_two.ReadOnly = True
        Me.tb_bank_payment_two.Size = New System.Drawing.Size(179, 18)
        Me.tb_bank_payment_two.TabIndex = 138
        Me.tb_bank_payment_two.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'tb_bank_name_two
        '
        Me.tb_bank_name_two.BackColor = System.Drawing.Color.FromArgb(CType(CType(28, Byte), Integer), CType(CType(40, Byte), Integer), CType(CType(52, Byte), Integer))
        Me.tb_bank_name_two.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.tb_bank_name_two.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tb_bank_name_two.ForeColor = System.Drawing.Color.White
        Me.tb_bank_name_two.Location = New System.Drawing.Point(281, 521)
        Me.tb_bank_name_two.MaxLength = 120
        Me.tb_bank_name_two.Name = "tb_bank_name_two"
        Me.tb_bank_name_two.Size = New System.Drawing.Size(268, 18)
        Me.tb_bank_name_two.TabIndex = 137
        Me.tb_bank_name_two.Text = "Land Bank of the Philippines"
        Me.tb_bank_name_two.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'tb_bank_num_two
        '
        Me.tb_bank_num_two.BackColor = System.Drawing.Color.FromArgb(CType(CType(28, Byte), Integer), CType(CType(40, Byte), Integer), CType(CType(52, Byte), Integer))
        Me.tb_bank_num_two.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.tb_bank_num_two.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tb_bank_num_two.ForeColor = System.Drawing.Color.White
        Me.tb_bank_num_two.Location = New System.Drawing.Point(94, 521)
        Me.tb_bank_num_two.MaxLength = 120
        Me.tb_bank_num_two.Name = "tb_bank_num_two"
        Me.tb_bank_num_two.Size = New System.Drawing.Size(179, 18)
        Me.tb_bank_num_two.TabIndex = 136
        Me.tb_bank_num_two.Text = "0152-1001-75"
        Me.tb_bank_num_two.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Font = New System.Drawing.Font("Segoe UI", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label13.ForeColor = System.Drawing.Color.White
        Me.Label13.Location = New System.Drawing.Point(94, 450)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(124, 20)
        Me.Label13.TabIndex = 140
        Me.Label13.Text = "Bank Account/s:"
        '
        'pnl_finish
        '
        Me.pnl_finish.BackColor = System.Drawing.Color.FromArgb(CType(CType(28, Byte), Integer), CType(CType(40, Byte), Integer), CType(CType(52, Byte), Integer))
        Me.pnl_finish.Controls.Add(Me.btn_finish)
        Me.pnl_finish.Location = New System.Drawing.Point(287, 628)
        Me.pnl_finish.Name = "pnl_finish"
        Me.pnl_finish.Size = New System.Drawing.Size(133, 58)
        Me.pnl_finish.TabIndex = 141
        Me.pnl_finish.Visible = False
        '
        'btn_finish
        '
        Me.btn_finish.BackColor = System.Drawing.Color.FromArgb(CType(CType(218, Byte), Integer), CType(CType(144, Byte), Integer), CType(CType(27, Byte), Integer))
        Me.btn_finish.FlatAppearance.BorderSize = 0
        Me.btn_finish.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_finish.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_finish.ForeColor = System.Drawing.Color.White
        Me.btn_finish.Location = New System.Drawing.Point(12, 12)
        Me.btn_finish.Name = "btn_finish"
        Me.btn_finish.Size = New System.Drawing.Size(110, 35)
        Me.btn_finish.TabIndex = 102
        Me.btn_finish.Text = "Finish"
        Me.btn_finish.UseVisualStyleBackColor = False
        '
        'frm_accountant
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(44, Byte), Integer), CType(CType(62, Byte), Integer), CType(CType(80, Byte), Integer))
        Me.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.ClientSize = New System.Drawing.Size(840, 720)
        Me.Controls.Add(Me.pnl_finish)
        Me.Controls.Add(Me.Label13)
        Me.Controls.Add(Me.tb_bank_payment_two)
        Me.Controls.Add(Me.tb_bank_name_two)
        Me.Controls.Add(Me.tb_bank_num_two)
        Me.Controls.Add(Me.Label22)
        Me.Controls.Add(Me.Label21)
        Me.Controls.Add(Me.tb_bank_total)
        Me.Controls.Add(Me.tb_bank_payment)
        Me.Controls.Add(Me.tb_bank_name)
        Me.Controls.Add(Me.tb_bank_num)
        Me.Controls.Add(Me.tb_date_created)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.tb_serial_num)
        Me.Controls.Add(Me.tb_fund_cluster)
        Me.Controls.Add(Me.tb_accountant)
        Me.Controls.Add(Me.Label20)
        Me.Controls.Add(Me.tb_payment)
        Me.Controls.Add(Me.Label19)
        Me.Controls.Add(Me.Label18)
        Me.Controls.Add(Me.Label15)
        Me.Controls.Add(Me.tb_dated)
        Me.Controls.Add(Me.tb_per_bill)
        Me.Controls.Add(Me.tb_purpose_two)
        Me.Controls.Add(Me.tb_purpose_one)
        Me.Controls.Add(Me.tb_word_figure)
        Me.Controls.Add(Me.tb_address)
        Me.Controls.Add(Me.tb_payor)
        Me.Controls.Add(Me.Label17)
        Me.Controls.Add(Me.Label16)
        Me.Controls.Add(Me.Label12)
        Me.Controls.Add(Me.Label11)
        Me.Controls.Add(Me.Label10)
        Me.Controls.Add(Me.Label9)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.RadTitleBar1)
        Me.Controls.Add(Me.Label14)
        Me.Controls.Add(Me.BunifuSeparator9)
        Me.Controls.Add(Me.Label24)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.MaximizeBox = False
        Me.MaximumSize = New System.Drawing.Size(840, 720)
        Me.MinimizeBox = False
        Me.MinimumSize = New System.Drawing.Size(840, 720)
        Me.Name = "frm_accountant"
        Me.ShowIcon = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "NTC Region 10"
        CType(Me.RadTitleBar1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        Me.pnl_finish.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents RadTitleBar1 As Telerik.WinControls.UI.RadTitleBar
    Friend WithEvents Label2 As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents Label4 As Label
    Friend WithEvents Label5 As Label
    Friend WithEvents Label8 As Label
    Friend WithEvents Label9 As Label
    Friend WithEvents Label10 As Label
    Friend WithEvents Label11 As Label
    Friend WithEvents Label12 As Label
    Friend WithEvents Label14 As Label
    Friend WithEvents Label16 As Label
    Friend WithEvents Label17 As Label
    Friend WithEvents tb_payor As TextBox
    Friend WithEvents tb_address As TextBox
    Friend WithEvents tb_word_figure As TextBox
    Friend WithEvents tb_purpose_one As TextBox
    Friend WithEvents tb_purpose_two As TextBox
    Friend WithEvents tb_per_bill As TextBox
    Friend WithEvents tb_dated As TextBox
    Friend WithEvents Label15 As Label
    Friend WithEvents Label18 As Label
    Friend WithEvents Label19 As Label
    Friend WithEvents tb_payment As TextBox
    Friend WithEvents Label20 As Label
    Friend WithEvents tb_accountant As TextBox
    Friend WithEvents tb_fund_cluster As TextBox
    Friend WithEvents tb_serial_num As TextBox
    Friend WithEvents Panel1 As Panel
    Friend WithEvents btn_print As Button
    Friend WithEvents tb_date_created As TextBox
    Friend WithEvents BunifuSeparator9 As Bunifu.Framework.UI.BunifuSeparator
    Friend WithEvents tb_bank_num As TextBox
    Friend WithEvents tb_bank_name As TextBox
    Friend WithEvents tb_bank_payment As TextBox
    Friend WithEvents tb_bank_total As TextBox
    Friend WithEvents Label21 As Label
    Friend WithEvents Label22 As Label
    Friend WithEvents Label24 As Label
    Friend WithEvents tb_bank_payment_two As TextBox
    Friend WithEvents tb_bank_name_two As TextBox
    Friend WithEvents tb_bank_num_two As TextBox
    Friend WithEvents Label13 As Label
    Friend WithEvents btn_soa As Button
    Friend WithEvents pnl_finish As Panel
    Friend WithEvents btn_finish As Button
End Class
