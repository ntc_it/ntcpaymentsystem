﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class dashboard
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(dashboard))
        Me.pnl_dashboard = New System.Windows.Forms.Panel()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.RadCollapsiblePanel1 = New Telerik.WinControls.UI.RadCollapsiblePanel()
        Me.btn_reports = New System.Windows.Forms.Button()
        Me.btn_manual = New System.Windows.Forms.Button()
        Me.btn_about = New Bunifu.Framework.UI.BunifuImageButton()
        Me.btn_user_settings = New Bunifu.Framework.UI.BunifuImageButton()
        Me.btn_settings = New System.Windows.Forms.Button()
        Me.btn_register_user = New System.Windows.Forms.Button()
        Me.btn_view_logs = New System.Windows.Forms.Button()
        Me.btn_newsoa = New System.Windows.Forms.Button()
        Me.btn_my_notifications = New System.Windows.Forms.Button()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.tb_name = New System.Windows.Forms.TextBox()
        Me.btn_logout = New Bunifu.Framework.UI.BunifuImageButton()
        Me.btn_refresh = New Bunifu.Framework.UI.BunifuImageButton()
        Me.NotifyIcon1 = New System.Windows.Forms.NotifyIcon(Me.components)
        Me.idleTimer = New System.Windows.Forms.Timer(Me.components)
        Me.pnl_dashboard.SuspendLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RadCollapsiblePanel1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.RadCollapsiblePanel1.PanelContainer.SuspendLayout()
        Me.RadCollapsiblePanel1.SuspendLayout()
        CType(Me.btn_about, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.btn_user_settings, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.btn_logout, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.btn_refresh, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'pnl_dashboard
        '
        Me.pnl_dashboard.BackColor = System.Drawing.Color.FromArgb(CType(CType(44, Byte), Integer), CType(CType(62, Byte), Integer), CType(CType(80, Byte), Integer))
        Me.pnl_dashboard.Controls.Add(Me.PictureBox1)
        Me.pnl_dashboard.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnl_dashboard.Location = New System.Drawing.Point(287, 0)
        Me.pnl_dashboard.Name = "pnl_dashboard"
        Me.pnl_dashboard.Size = New System.Drawing.Size(1061, 687)
        Me.pnl_dashboard.TabIndex = 6
        '
        'PictureBox1
        '
        Me.PictureBox1.Image = Global.NTC_ACCOUNTING.My.Resources.Resources._1200px_National_Telecommunications_Commission_svg
        Me.PictureBox1.Location = New System.Drawing.Point(463, 144)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(376, 372)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox1.TabIndex = 1
        Me.PictureBox1.TabStop = False
        '
        'RadCollapsiblePanel1
        '
        Me.RadCollapsiblePanel1.AnimationFrames = 5
        Me.RadCollapsiblePanel1.AnimationInterval = 1
        Me.RadCollapsiblePanel1.AnimationType = Telerik.WinControls.UI.CollapsiblePanelAnimationType.Slide
        Me.RadCollapsiblePanel1.BackColor = System.Drawing.Color.FromArgb(CType(CType(54, Byte), Integer), CType(CType(66, Byte), Integer), CType(CType(76, Byte), Integer))
        Me.RadCollapsiblePanel1.Dock = System.Windows.Forms.DockStyle.Left
        Me.RadCollapsiblePanel1.ExpandDirection = Telerik.WinControls.UI.RadDirection.Right
        Me.RadCollapsiblePanel1.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadCollapsiblePanel1.ForeColor = System.Drawing.Color.White
        Me.RadCollapsiblePanel1.HorizontalHeaderAlignment = Telerik.WinControls.UI.RadHorizontalAlignment.Center
        Me.RadCollapsiblePanel1.Location = New System.Drawing.Point(0, 0)
        Me.RadCollapsiblePanel1.Name = "RadCollapsiblePanel1"
        Me.RadCollapsiblePanel1.OwnerBoundsCache = New System.Drawing.Rectangle(0, 0, 287, 687)
        '
        'RadCollapsiblePanel1.PanelContainer
        '
        Me.RadCollapsiblePanel1.PanelContainer.Controls.Add(Me.tb_name)
        Me.RadCollapsiblePanel1.PanelContainer.Controls.Add(Me.btn_reports)
        Me.RadCollapsiblePanel1.PanelContainer.Controls.Add(Me.btn_manual)
        Me.RadCollapsiblePanel1.PanelContainer.Controls.Add(Me.btn_about)
        Me.RadCollapsiblePanel1.PanelContainer.Controls.Add(Me.btn_user_settings)
        Me.RadCollapsiblePanel1.PanelContainer.Controls.Add(Me.btn_settings)
        Me.RadCollapsiblePanel1.PanelContainer.Controls.Add(Me.btn_register_user)
        Me.RadCollapsiblePanel1.PanelContainer.Controls.Add(Me.btn_view_logs)
        Me.RadCollapsiblePanel1.PanelContainer.Controls.Add(Me.btn_newsoa)
        Me.RadCollapsiblePanel1.PanelContainer.Controls.Add(Me.btn_my_notifications)
        Me.RadCollapsiblePanel1.PanelContainer.Controls.Add(Me.Label4)
        Me.RadCollapsiblePanel1.PanelContainer.Controls.Add(Me.btn_logout)
        Me.RadCollapsiblePanel1.PanelContainer.Controls.Add(Me.btn_refresh)
        Me.RadCollapsiblePanel1.PanelContainer.Margin = New System.Windows.Forms.Padding(0)
        Me.RadCollapsiblePanel1.PanelContainer.Size = New System.Drawing.Size(259, 685)
        Me.RadCollapsiblePanel1.Size = New System.Drawing.Size(287, 687)
        Me.RadCollapsiblePanel1.TabIndex = 2
        Me.RadCollapsiblePanel1.Text = "DASHBOARD PANEL"
        Me.RadCollapsiblePanel1.VerticalHeaderAlignment = Telerik.WinControls.UI.RadVerticalAlignment.Center
        CType(Me.RadCollapsiblePanel1.GetChildAt(0), Telerik.WinControls.UI.RadCollapsiblePanelElement).ExpandDirection = Telerik.WinControls.UI.RadDirection.Right
        CType(Me.RadCollapsiblePanel1.GetChildAt(0), Telerik.WinControls.UI.RadCollapsiblePanelElement).IsExpanded = True
        CType(Me.RadCollapsiblePanel1.GetChildAt(0), Telerik.WinControls.UI.RadCollapsiblePanelElement).AnimationInterval = 1
        CType(Me.RadCollapsiblePanel1.GetChildAt(0), Telerik.WinControls.UI.RadCollapsiblePanelElement).AnimationFrames = 5
        CType(Me.RadCollapsiblePanel1.GetChildAt(0), Telerik.WinControls.UI.RadCollapsiblePanelElement).AnimationType = Telerik.WinControls.UI.CollapsiblePanelAnimationType.Slide
        CType(Me.RadCollapsiblePanel1.GetChildAt(0), Telerik.WinControls.UI.RadCollapsiblePanelElement).Text = "Dashboard"
        CType(Me.RadCollapsiblePanel1.GetChildAt(0).GetChildAt(1), Telerik.WinControls.UI.CollapsiblePanelHeaderElement).HorizontalHeaderAlignment = Telerik.WinControls.UI.RadHorizontalAlignment.Center
        CType(Me.RadCollapsiblePanel1.GetChildAt(0).GetChildAt(1), Telerik.WinControls.UI.CollapsiblePanelHeaderElement).VerticalHeaderAlignment = Telerik.WinControls.UI.RadVerticalAlignment.Center
        CType(Me.RadCollapsiblePanel1.GetChildAt(0).GetChildAt(1), Telerik.WinControls.UI.CollapsiblePanelHeaderElement).Orientation = System.Windows.Forms.Orientation.Vertical
        CType(Me.RadCollapsiblePanel1.GetChildAt(0).GetChildAt(1).GetChildAt(0), Telerik.WinControls.UI.CollapsiblePanelButtonElement).TextOrientation = System.Windows.Forms.Orientation.Horizontal
        CType(Me.RadCollapsiblePanel1.GetChildAt(0).GetChildAt(1).GetChildAt(0), Telerik.WinControls.UI.CollapsiblePanelButtonElement).StretchVertically = False
        CType(Me.RadCollapsiblePanel1.GetChildAt(0).GetChildAt(1).GetChildAt(1), Telerik.WinControls.UI.CollapsiblePanelTextElement).TextOrientation = System.Windows.Forms.Orientation.Horizontal
        CType(Me.RadCollapsiblePanel1.GetChildAt(0).GetChildAt(1).GetChildAt(1), Telerik.WinControls.UI.CollapsiblePanelTextElement).StretchVertically = False
        '
        'btn_reports
        '
        Me.btn_reports.BackColor = System.Drawing.Color.FromArgb(CType(CType(218, Byte), Integer), CType(CType(144, Byte), Integer), CType(CType(27, Byte), Integer))
        Me.btn_reports.FlatAppearance.BorderSize = 0
        Me.btn_reports.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_reports.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_reports.ForeColor = System.Drawing.Color.White
        Me.btn_reports.Location = New System.Drawing.Point(14, 438)
        Me.btn_reports.Name = "btn_reports"
        Me.btn_reports.Size = New System.Drawing.Size(224, 48)
        Me.btn_reports.TabIndex = 117
        Me.btn_reports.Text = "Financial Reports"
        Me.btn_reports.UseVisualStyleBackColor = False
        Me.btn_reports.Visible = False
        '
        'btn_manual
        '
        Me.btn_manual.BackColor = System.Drawing.Color.FromArgb(CType(CType(218, Byte), Integer), CType(CType(144, Byte), Integer), CType(CType(27, Byte), Integer))
        Me.btn_manual.FlatAppearance.BorderSize = 0
        Me.btn_manual.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_manual.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_manual.ForeColor = System.Drawing.Color.White
        Me.btn_manual.Location = New System.Drawing.Point(14, 246)
        Me.btn_manual.Name = "btn_manual"
        Me.btn_manual.Size = New System.Drawing.Size(224, 48)
        Me.btn_manual.TabIndex = 115
        Me.btn_manual.Text = "Manual Insert SOA, OP and OR"
        Me.btn_manual.UseVisualStyleBackColor = False
        Me.btn_manual.Visible = False
        '
        'btn_about
        '
        Me.btn_about.BackColor = System.Drawing.Color.Transparent
        Me.btn_about.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btn_about.Image = Global.NTC_ACCOUNTING.My.Resources.Resources.ntc_about
        Me.btn_about.ImageActive = Nothing
        Me.btn_about.Location = New System.Drawing.Point(202, 635)
        Me.btn_about.Name = "btn_about"
        Me.btn_about.Size = New System.Drawing.Size(47, 39)
        Me.btn_about.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.btn_about.TabIndex = 114
        Me.btn_about.TabStop = False
        Me.btn_about.Zoom = 13
        '
        'btn_user_settings
        '
        Me.btn_user_settings.BackColor = System.Drawing.Color.Transparent
        Me.btn_user_settings.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btn_user_settings.Image = Global.NTC_ACCOUNTING.My.Resources.Resources.ntc_settings
        Me.btn_user_settings.ImageActive = Nothing
        Me.btn_user_settings.Location = New System.Drawing.Point(113, 635)
        Me.btn_user_settings.Name = "btn_user_settings"
        Me.btn_user_settings.Size = New System.Drawing.Size(47, 39)
        Me.btn_user_settings.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.btn_user_settings.TabIndex = 111
        Me.btn_user_settings.TabStop = False
        Me.btn_user_settings.Zoom = 13
        '
        'btn_settings
        '
        Me.btn_settings.BackColor = System.Drawing.Color.FromArgb(CType(CType(218, Byte), Integer), CType(CType(144, Byte), Integer), CType(CType(27, Byte), Integer))
        Me.btn_settings.FlatAppearance.BorderSize = 0
        Me.btn_settings.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_settings.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_settings.ForeColor = System.Drawing.Color.White
        Me.btn_settings.Location = New System.Drawing.Point(14, 374)
        Me.btn_settings.Name = "btn_settings"
        Me.btn_settings.Size = New System.Drawing.Size(224, 48)
        Me.btn_settings.TabIndex = 110
        Me.btn_settings.Text = "System Settings"
        Me.btn_settings.UseVisualStyleBackColor = False
        Me.btn_settings.Visible = False
        '
        'btn_register_user
        '
        Me.btn_register_user.BackColor = System.Drawing.Color.FromArgb(CType(CType(218, Byte), Integer), CType(CType(144, Byte), Integer), CType(CType(27, Byte), Integer))
        Me.btn_register_user.FlatAppearance.BorderSize = 0
        Me.btn_register_user.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_register_user.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_register_user.ForeColor = System.Drawing.Color.White
        Me.btn_register_user.Location = New System.Drawing.Point(14, 310)
        Me.btn_register_user.Name = "btn_register_user"
        Me.btn_register_user.Size = New System.Drawing.Size(224, 48)
        Me.btn_register_user.TabIndex = 109
        Me.btn_register_user.Text = "Manage users"
        Me.btn_register_user.UseVisualStyleBackColor = False
        '
        'btn_view_logs
        '
        Me.btn_view_logs.BackColor = System.Drawing.Color.FromArgb(CType(CType(218, Byte), Integer), CType(CType(144, Byte), Integer), CType(CType(27, Byte), Integer))
        Me.btn_view_logs.FlatAppearance.BorderSize = 0
        Me.btn_view_logs.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_view_logs.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_view_logs.ForeColor = System.Drawing.Color.White
        Me.btn_view_logs.Location = New System.Drawing.Point(14, 182)
        Me.btn_view_logs.Name = "btn_view_logs"
        Me.btn_view_logs.Size = New System.Drawing.Size(224, 48)
        Me.btn_view_logs.TabIndex = 108
        Me.btn_view_logs.Text = "SOA Logs"
        Me.btn_view_logs.UseVisualStyleBackColor = False
        '
        'btn_newsoa
        '
        Me.btn_newsoa.BackColor = System.Drawing.Color.FromArgb(CType(CType(218, Byte), Integer), CType(CType(144, Byte), Integer), CType(CType(27, Byte), Integer))
        Me.btn_newsoa.FlatAppearance.BorderSize = 0
        Me.btn_newsoa.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_newsoa.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_newsoa.ForeColor = System.Drawing.Color.White
        Me.btn_newsoa.Location = New System.Drawing.Point(14, 54)
        Me.btn_newsoa.Name = "btn_newsoa"
        Me.btn_newsoa.Size = New System.Drawing.Size(224, 48)
        Me.btn_newsoa.TabIndex = 107
        Me.btn_newsoa.Text = "Create SOA"
        Me.btn_newsoa.UseVisualStyleBackColor = False
        '
        'btn_my_notifications
        '
        Me.btn_my_notifications.BackColor = System.Drawing.Color.FromArgb(CType(CType(218, Byte), Integer), CType(CType(144, Byte), Integer), CType(CType(27, Byte), Integer))
        Me.btn_my_notifications.FlatAppearance.BorderSize = 0
        Me.btn_my_notifications.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_my_notifications.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_my_notifications.ForeColor = System.Drawing.Color.White
        Me.btn_my_notifications.Location = New System.Drawing.Point(14, 118)
        Me.btn_my_notifications.Name = "btn_my_notifications"
        Me.btn_my_notifications.Size = New System.Drawing.Size(224, 48)
        Me.btn_my_notifications.TabIndex = 106
        Me.btn_my_notifications.Text = "Notifications"
        Me.btn_my_notifications.UseVisualStyleBackColor = False
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Segoe UI", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ForeColor = System.Drawing.Color.White
        Me.Label4.Location = New System.Drawing.Point(11, 591)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(18, 13)
        Me.Label4.TabIndex = 82
        Me.Label4.Text = "Hi"
        '
        'tb_name
        '
        Me.tb_name.BackColor = System.Drawing.Color.FromArgb(CType(CType(54, Byte), Integer), CType(CType(66, Byte), Integer), CType(CType(76, Byte), Integer))
        Me.tb_name.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.tb_name.Cursor = System.Windows.Forms.Cursors.Hand
        Me.tb_name.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tb_name.ForeColor = System.Drawing.Color.White
        Me.tb_name.Location = New System.Drawing.Point(11, 605)
        Me.tb_name.Name = "tb_name"
        Me.tb_name.ReadOnly = True
        Me.tb_name.Size = New System.Drawing.Size(238, 22)
        Me.tb_name.TabIndex = 69
        Me.tb_name.Text = "xxxxxxxxxx"
        '
        'btn_logout
        '
        Me.btn_logout.BackColor = System.Drawing.Color.Transparent
        Me.btn_logout.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btn_logout.Image = Global.NTC_ACCOUNTING.My.Resources.Resources.ntc_shutdown
        Me.btn_logout.ImageActive = Nothing
        Me.btn_logout.Location = New System.Drawing.Point(11, 635)
        Me.btn_logout.Name = "btn_logout"
        Me.btn_logout.Size = New System.Drawing.Size(47, 39)
        Me.btn_logout.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.btn_logout.TabIndex = 8
        Me.btn_logout.TabStop = False
        Me.btn_logout.Zoom = 13
        '
        'btn_refresh
        '
        Me.btn_refresh.BackColor = System.Drawing.Color.Transparent
        Me.btn_refresh.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btn_refresh.Image = Global.NTC_ACCOUNTING.My.Resources.Resources.ntc_refresh
        Me.btn_refresh.ImageActive = Nothing
        Me.btn_refresh.Location = New System.Drawing.Point(62, 635)
        Me.btn_refresh.Name = "btn_refresh"
        Me.btn_refresh.Size = New System.Drawing.Size(47, 39)
        Me.btn_refresh.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.btn_refresh.TabIndex = 7
        Me.btn_refresh.TabStop = False
        Me.btn_refresh.Zoom = 13
        '
        'NotifyIcon1
        '
        Me.NotifyIcon1.Icon = CType(resources.GetObject("NotifyIcon1.Icon"), System.Drawing.Icon)
        Me.NotifyIcon1.Text = "NTC Region 10 Payment System"
        Me.NotifyIcon1.Visible = True
        '
        'idleTimer
        '
        Me.idleTimer.Interval = 5000
        '
        'dashboard
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.ClientSize = New System.Drawing.Size(1348, 687)
        Me.Controls.Add(Me.pnl_dashboard)
        Me.Controls.Add(Me.RadCollapsiblePanel1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MaximumSize = New System.Drawing.Size(1364, 726)
        Me.MinimumSize = New System.Drawing.Size(1364, 726)
        Me.Name = "dashboard"
        Me.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "NTC Region 10"
        Me.pnl_dashboard.ResumeLayout(False)
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.RadCollapsiblePanel1.PanelContainer.ResumeLayout(False)
        Me.RadCollapsiblePanel1.PanelContainer.PerformLayout()
        CType(Me.RadCollapsiblePanel1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.RadCollapsiblePanel1.ResumeLayout(False)
        CType(Me.btn_about, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.btn_user_settings, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.btn_logout, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.btn_refresh, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnl_dashboard As Panel
    Friend WithEvents PictureBox1 As PictureBox
    Friend WithEvents RadCollapsiblePanel1 As Telerik.WinControls.UI.RadCollapsiblePanel
    Friend WithEvents btn_logout As Bunifu.Framework.UI.BunifuImageButton
    Friend WithEvents tb_name As TextBox
    Friend WithEvents Label4 As Label
    Friend WithEvents btn_my_notifications As Button
    Friend WithEvents btn_refresh As Bunifu.Framework.UI.BunifuImageButton
    Friend WithEvents btn_newsoa As Button
    Friend WithEvents btn_view_logs As Button
    Friend WithEvents btn_register_user As Button
    Friend WithEvents btn_settings As Button
    Friend WithEvents btn_user_settings As Bunifu.Framework.UI.BunifuImageButton
    Friend WithEvents NotifyIcon1 As NotifyIcon
    Friend WithEvents idleTimer As Timer
    Friend WithEvents btn_about As Bunifu.Framework.UI.BunifuImageButton
    Friend WithEvents btn_manual As Button
    Friend WithEvents btn_reports As Button
End Class
