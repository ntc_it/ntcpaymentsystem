﻿Imports MySql.Data.MySqlClient

Public Class dashboard
    Dim query As String
    Dim cmd As MySqlCommand
    Dim reader As MySqlDataReader
    Dim numberOfNotify As Integer
    Public user_id_login, authority_level As String
    Public uc_notifications As New uc_notifications

    Sub loadNumberOfNotifications()
        Try
            ' Retrieve number of notifications from server.
            class_connection.conTwo.Open()
            query = "select count(*) as 'notify' from tbl_notifications where user_id_notified = '" & user_id_login &
                "' and ((user_opened = '0' and action_taken = '0') or (user_opened = '0' and action_taken = '1'))"
            cmd = New MySqlCommand(query, class_connection.conTwo)
            reader = cmd.ExecuteReader()

            If reader.Read Then
                If reader.GetInt32("notify") <> 0 Then
                    btn_my_notifications.Text = "Notifications(" + reader.GetString("notify") + ")"
                    btn_my_notifications.BackColor = Color.FromArgb(125, 209, 0)

                    ' If number of notification is not the same with previous then notify.
                    If numberOfNotify <> reader.GetString("notify") Then
                        NotifyIcon1.Icon = My.Resources.ntc_material_logo
                        NotifyIcon1.BalloonTipIcon = ToolTipIcon.None
                        NotifyIcon1.BalloonTipTitle = "NTC Region 10 Payment System"
                        NotifyIcon1.BalloonTipText = "You have " & reader.GetString("notify") & " notification/s on your account."
                        NotifyIcon1.ShowBalloonTip(8000)
                        numberOfNotify = reader.GetString("notify")
                        My.Computer.Audio.Play(Application.StartupPath + "\notification.wav")
                    End If
                Else
                    btn_my_notifications.Text = "Notifications"
                    btn_my_notifications.BackColor = Color.FromArgb(249, 170, 51)
                    numberOfNotify = "0"
                End If
            End If
            class_connection.conTwo.Close()
        Catch ex As MySqlException
            class_connection.conTwo.Close()
        Catch ex As Exception
            class_connection.conTwo.Close()
        End Try
    End Sub

    Private Sub admin_dashboard_Load(sender As Object, e As EventArgs) Handles Me.Load
        retrieveUserName()

        ' Initialize number of notify to zero.
        numberOfNotify = 0

        loadNumberOfNotifications()

        ' Enable automation of notification retrieval.
        idleTimer.Enabled = True
        idleTimer.Interval = 15000

        ' show program in taskbar and icon.
        ShowInTaskbar = True
        NotifyIcon1.Visible = True

        Icon = My.Resources.ntc_material_logo
    End Sub

    Private Sub retrieveUserName()
        Try
            class_connection.con.Open()
            query = "select full_name from tbl_user where user_id = '" + user_id_login + "'"
            cmd = New MySqlCommand(query, class_connection.con)
            reader = cmd.ExecuteReader()

            If reader.Read Then
                tb_name.Text = reader.GetString("full_name")
            End If
            class_connection.con.Close()
        Catch ex As MySqlException
            class_connection.con.Close()
            MsgBox("Could'nt connect to server. Contact the IT Administrator for assistance.",
                   MsgBoxStyle.Exclamation, "NTC Region 10")
            Close()

            ' Reset login credentials and show login form.
            frm_login.tb_username.Text = ""
            frm_login.tb_password.Text = ""
            frm_login.tb_username.Focus()
            frm_login.Show()
        Catch ex As Exception
            MsgBox(ex.Message + " ; " & ex.GetType.ToString)
            class_connection.con.Close()
        End Try
    End Sub

    Private Sub btn_my_notifications_Click(sender As Object, e As EventArgs) Handles btn_my_notifications.Click
        pnl_dashboard.Controls.Clear()
        uc_notifications.setUserandAuthorLevel(user_id_login, authority_level)
        pnl_dashboard.Controls.Add(uc_notifications)
        uc_notifications.cmb_msg_category.SelectedIndex() = 0
        uc_notifications.load_notifications()
        uc_notifications.retrieve_number_of_unresolved()

    End Sub
    Private Sub btn_logs_Click(sender As Object, e As EventArgs) Handles btn_view_logs.Click
        Dim uc_logs As New uc_logs

        pnl_dashboard.Controls.Clear()
        uc_logs.setUserandAuthorLevel(user_id_login, authority_level)
        pnl_dashboard.Controls.Add(uc_logs)
    End Sub

    Private Sub btn_register_user_Click(sender As Object, e As EventArgs) Handles btn_register_user.Click
        Dim uc_userReg As New uc_manage_users

        pnl_dashboard.Controls.Clear()
        pnl_dashboard.Controls.Add(uc_userReg)
    End Sub

    Private Sub btn_settings_Click(sender As Object, e As EventArgs) Handles btn_settings.Click
        Dim settings As New uc_system_settings

        pnl_dashboard.Controls.Clear()
        pnl_dashboard.Controls.Add(settings)
    End Sub

    Private Sub btn_refresh_Click(sender As Object, e As EventArgs) Handles btn_refresh.Click
        Dim dashboard_home As New dashboard_home

        pnl_dashboard.Controls.Clear()
        pnl_dashboard.Controls.Add(dashboard_home)

        loadNumberOfNotifications()
    End Sub

    Private Sub btn_logout_Click(sender As Object, e As EventArgs) Handles btn_logout.Click
        ' Force all open/hidden forms to close and redirect to login form.
        My.Application.OpenForms.Cast(Of Form)() _
      .Except({frm_login}) _
      .ToList() _
      .ForEach(Sub(form) form.Close())

        ' Disable notification automation and close form.
        idleTimer.Enabled = False
        Close()

        ' Open login form.
        frm_login.tb_username.Text = ""
        frm_login.tb_password.Text = ""
        frm_login.user_id_login = Nothing
        frm_login.authority_level = ""
        frm_login.btn_login.Enabled = True
        frm_login.tb_username.Focus()
        frm_login.Show()
    End Sub

    Private Sub btn_user_settings_Click(sender As Object, e As EventArgs) Handles btn_user_settings.Click
        Dim user_settings As New uc_user_settings

        pnl_dashboard.Controls.Clear()
        user_settings.setUser(user_id_login)
        pnl_dashboard.Controls.Add(user_settings)
    End Sub
    Private Sub AdminDashboardPanel_Resize(sender As Object, e As EventArgs) Handles pnl_dashboard.Resize
        ' If minimized, hide form and show program icon on taskbar.
        If Me.WindowState = FormWindowState.Minimized Then
            NotifyIcon1.Visible = True
            NotifyIcon1.Icon = My.Resources.ntc_material_logo
            NotifyIcon1.BalloonTipIcon = ToolTipIcon.None
            NotifyIcon1.BalloonTipTitle = "NTC Region 10 Payment System"
            NotifyIcon1.BalloonTipText = "System is running in background."
            NotifyIcon1.ShowBalloonTip(5000)

            ShowInTaskbar = False
        End If
    End Sub

    Private Sub NotifyIcon1_MouseDoubleClick(sender As Object, e As MouseEventArgs) Handles NotifyIcon1.MouseDoubleClick
        ShowInTaskbar = True
        WindowState = FormWindowState.Normal
        NotifyIcon1.Visible = True
    End Sub

    Private Sub idleTimer_Tick(sender As Object, e As EventArgs) Handles idleTimer.Tick
        loadNumberOfNotifications()
    End Sub

    Private Sub dashboard_MinimumSizeChanged(sender As Object, e As EventArgs) Handles MyBase.MinimumSizeChanged
        ShowInTaskbar = False
    End Sub

    Private Sub btn_about_Click(sender As Object, e As EventArgs) Handles btn_about.Click
        frm_about.ShowDialog()
    End Sub

    Private Sub btn_manual_Click(sender As Object, e As EventArgs) Handles btn_manual.Click
        frm_licenser_manual.user_id = user_id_login
        frm_licenser_manual.authority_level = authority_level
        frm_licenser_manual.Show()
    End Sub

    Private Sub btnReport_Click(sender As Object, e As EventArgs) Handles btn_reports.Click
        Dim report As New uc_financial_reports

        pnl_dashboard.Controls.Clear()
        pnl_dashboard.Controls.Add(report)
    End Sub

    Private Sub btn_newsoa_Click_1(sender As Object, e As EventArgs) Handles btn_newsoa.Click
        frm_licenser.user_id = user_id_login
        frm_licenser.authority_level = authority_level
        frm_licenser.Show()
    End Sub

    Protected Overrides ReadOnly Property CreateParams() As CreateParams
        Get
            Dim param As CreateParams = MyBase.CreateParams
            param.ClassStyle = param.ClassStyle Or &H200
            Return param
        End Get
    End Property
End Class