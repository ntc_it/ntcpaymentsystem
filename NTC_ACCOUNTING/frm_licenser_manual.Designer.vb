﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_licenser_manual
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle7 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle8 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle9 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frm_licenser_manual))
        Dim DataGridViewCellStyle10 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle11 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle12 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.pnl_roc = New System.Windows.Forms.Panel()
        Me.cmb_payment_number_roc = New System.Windows.Forms.ComboBox()
        Me.btn_roc_add = New System.Windows.Forms.Button()
        Me.cmb_roc_items = New System.Windows.Forms.ComboBox()
        Me.dgv_roc = New System.Windows.Forms.DataGridView()
        Me.cms_dgv_roc = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.tsmi_cms_roc_remove_entry = New System.Windows.Forms.ToolStripMenuItem()
        Me.tsmi_cms_others_remove_entry = New System.Windows.Forms.ToolStripMenuItem()
        Me.cms_dgv_others = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.cmb_payment_number_others = New System.Windows.Forms.ComboBox()
        Me.btn_others_add = New System.Windows.Forms.Button()
        Me.cmb_others_items = New System.Windows.Forms.ComboBox()
        Me.dgv_others = New System.Windows.Forms.DataGridView()
        Me.pnl_others = New System.Windows.Forms.Panel()
        Me.cmb_payment_number_license = New System.Windows.Forms.ComboBox()
        Me.btn_license_add = New System.Windows.Forms.Button()
        Me.cmb_license_items = New System.Windows.Forms.ComboBox()
        Me.dgv_license = New System.Windows.Forms.DataGridView()
        Me.cms_dgv_license = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.tsmi_cms_license_remove_entry = New System.Windows.Forms.ToolStripMenuItem()
        Me.pnl_license = New System.Windows.Forms.Panel()
        Me.btn_tab_license = New System.Windows.Forms.Button()
        Me.btn_tab_others = New System.Windows.Forms.Button()
        Me.btn_tab_roc = New System.Windows.Forms.Button()
        Me.btn_tab_permits = New System.Windows.Forms.Button()
        Me.panel_particular = New System.Windows.Forms.Panel()
        Me.p1SubpanelMenu = New System.Windows.Forms.Panel()
        Me.tb_particular_one = New System.Windows.Forms.TextBox()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.dtp_expiry_date = New System.Windows.Forms.DateTimePicker()
        Me.Panel5 = New System.Windows.Forms.Panel()
        Me.btn_cancel = New System.Windows.Forms.Button()
        Me.btn_save = New System.Windows.Forms.Button()
        Me.Panel4 = New System.Windows.Forms.Panel()
        Me.tb_total = New System.Windows.Forms.TextBox()
        Me.Label24 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.btn_addpayor = New Bunifu.Framework.UI.BunifuImageButton()
        Me.pnl_mod_others = New System.Windows.Forms.Panel()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.btn_cancel_others = New System.Windows.Forms.Button()
        Me.btn_confirm_others = New System.Windows.Forms.Button()
        Me.tb_others_value = New System.Windows.Forms.TextBox()
        Me.cms_dgv_permits = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.tsmi_cms_permits_remove_entry = New System.Windows.Forms.ToolStripMenuItem()
        Me.dgv_permits = New System.Windows.Forms.DataGridView()
        Me.cmb_payment_number_permits = New System.Windows.Forms.ComboBox()
        Me.btn_permits_add = New System.Windows.Forms.Button()
        Me.cmb_permits_items = New System.Windows.Forms.ComboBox()
        Me.pnl_permits = New System.Windows.Forms.Panel()
        Me.dtp_p1_from = New System.Windows.Forms.DateTimePicker()
        Me.txtpayor = New System.Windows.Forms.TextBox()
        Me.cb_process_others = New System.Windows.Forms.CheckBox()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.cb_process_mod = New System.Windows.Forms.CheckBox()
        Me.cb_process_dup = New System.Windows.Forms.CheckBox()
        Me.cb_services_others = New System.Windows.Forms.CheckBox()
        Me.cb_services_roc = New System.Windows.Forms.CheckBox()
        Me.cb_services_ma = New System.Windows.Forms.CheckBox()
        Me.cb_services_ms = New System.Windows.Forms.CheckBox()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.cb_services_co = New System.Windows.Forms.CheckBox()
        Me.cb_services_cv = New System.Windows.Forms.CheckBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.lbl_others = New System.Windows.Forms.Label()
        Me.dtp_date_created = New System.Windows.Forms.DateTimePicker()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.tb_others = New System.Windows.Forms.TextBox()
        Me.cmb_type = New System.Windows.Forms.ComboBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.RadTitleBar1 = New Telerik.WinControls.UI.RadTitleBar()
        Me.dtp_p1_to = New System.Windows.Forms.DateTimePicker()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.tb_particular_two = New System.Windows.Forms.TextBox()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.dtp_p2_from = New System.Windows.Forms.DateTimePicker()
        Me.dtp_p2_to = New System.Windows.Forms.DateTimePicker()
        Me.Label21 = New System.Windows.Forms.Label()
        Me.Label22 = New System.Windows.Forms.Label()
        Me.cb_particular_two = New System.Windows.Forms.CheckBox()
        Me.tb_particular_three = New System.Windows.Forms.TextBox()
        Me.Label23 = New System.Windows.Forms.Label()
        Me.dtp_p3_from = New System.Windows.Forms.DateTimePicker()
        Me.dtp_p3_to = New System.Windows.Forms.DateTimePicker()
        Me.Label27 = New System.Windows.Forms.Label()
        Me.Label28 = New System.Windows.Forms.Label()
        Me.cb_particular_three = New System.Windows.Forms.CheckBox()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.panelParticular1 = New System.Windows.Forms.Panel()
        Me.tb_serial_starting_digit = New System.Windows.Forms.TextBox()
        Me.tb_serial_year = New System.Windows.Forms.TextBox()
        Me.tb_serial_month = New System.Windows.Forms.TextBox()
        Me.tb_serial_series = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.cmb_preparator = New System.Windows.Forms.ComboBox()
        Me.cmb_approver = New System.Windows.Forms.ComboBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.cmb_preparator_id = New System.Windows.Forms.ComboBox()
        Me.cmb_approver_id = New System.Windows.Forms.ComboBox()
        Me.rbAuto = New System.Windows.Forms.RadioButton()
        Me.rbCustom = New System.Windows.Forms.RadioButton()
        Me.Panel6 = New System.Windows.Forms.Panel()
        Me.pnl_roc.SuspendLayout()
        CType(Me.dgv_roc, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.cms_dgv_roc.SuspendLayout()
        Me.cms_dgv_others.SuspendLayout()
        CType(Me.dgv_others, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnl_others.SuspendLayout()
        CType(Me.dgv_license, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.cms_dgv_license.SuspendLayout()
        Me.pnl_license.SuspendLayout()
        Me.p1SubpanelMenu.SuspendLayout()
        Me.Panel5.SuspendLayout()
        Me.Panel4.SuspendLayout()
        CType(Me.btn_addpayor, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnl_mod_others.SuspendLayout()
        Me.cms_dgv_permits.SuspendLayout()
        CType(Me.dgv_permits, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnl_permits.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        CType(Me.RadTitleBar1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel3.SuspendLayout()
        Me.Panel2.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.panelParticular1.SuspendLayout()
        Me.Panel6.SuspendLayout()
        Me.SuspendLayout()
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Font = New System.Drawing.Font("Segoe UI", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label17.ForeColor = System.Drawing.Color.DarkGray
        Me.Label17.Location = New System.Drawing.Point(39, 55)
        Me.Label17.Margin = New System.Windows.Forms.Padding(5)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(85, 13)
        Me.Label17.TabIndex = 22
        Me.Label17.Text = "Period Covered"
        '
        'pnl_roc
        '
        Me.pnl_roc.BackColor = System.Drawing.Color.FromArgb(CType(CType(113, Byte), Integer), CType(CType(125, Byte), Integer), CType(CType(137, Byte), Integer))
        Me.pnl_roc.Controls.Add(Me.cmb_payment_number_roc)
        Me.pnl_roc.Controls.Add(Me.btn_roc_add)
        Me.pnl_roc.Controls.Add(Me.cmb_roc_items)
        Me.pnl_roc.Controls.Add(Me.dgv_roc)
        Me.pnl_roc.Location = New System.Drawing.Point(2, 137)
        Me.pnl_roc.Margin = New System.Windows.Forms.Padding(0)
        Me.pnl_roc.Name = "pnl_roc"
        Me.pnl_roc.Size = New System.Drawing.Size(1348, 316)
        Me.pnl_roc.TabIndex = 91
        '
        'cmb_payment_number_roc
        '
        Me.cmb_payment_number_roc.FormattingEnabled = True
        Me.cmb_payment_number_roc.Location = New System.Drawing.Point(424, 275)
        Me.cmb_payment_number_roc.Name = "cmb_payment_number_roc"
        Me.cmb_payment_number_roc.Size = New System.Drawing.Size(121, 21)
        Me.cmb_payment_number_roc.TabIndex = 14
        Me.cmb_payment_number_roc.TabStop = False
        Me.cmb_payment_number_roc.Visible = False
        '
        'btn_roc_add
        '
        Me.btn_roc_add.BackColor = System.Drawing.Color.FromArgb(CType(CType(218, Byte), Integer), CType(CType(144, Byte), Integer), CType(CType(27, Byte), Integer))
        Me.btn_roc_add.FlatAppearance.BorderSize = 0
        Me.btn_roc_add.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_roc_add.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_roc_add.ForeColor = System.Drawing.Color.White
        Me.btn_roc_add.Location = New System.Drawing.Point(322, 274)
        Me.btn_roc_add.Margin = New System.Windows.Forms.Padding(5)
        Me.btn_roc_add.Name = "btn_roc_add"
        Me.btn_roc_add.Size = New System.Drawing.Size(75, 23)
        Me.btn_roc_add.TabIndex = 12
        Me.btn_roc_add.TabStop = False
        Me.btn_roc_add.Text = "Add"
        Me.btn_roc_add.UseVisualStyleBackColor = False
        '
        'cmb_roc_items
        '
        Me.cmb_roc_items.BackColor = System.Drawing.Color.FromArgb(CType(CType(28, Byte), Integer), CType(CType(40, Byte), Integer), CType(CType(52, Byte), Integer))
        Me.cmb_roc_items.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmb_roc_items.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.cmb_roc_items.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmb_roc_items.ForeColor = System.Drawing.Color.White
        Me.cmb_roc_items.FormattingEnabled = True
        Me.cmb_roc_items.Location = New System.Drawing.Point(19, 275)
        Me.cmb_roc_items.Margin = New System.Windows.Forms.Padding(5)
        Me.cmb_roc_items.Name = "cmb_roc_items"
        Me.cmb_roc_items.Size = New System.Drawing.Size(282, 21)
        Me.cmb_roc_items.TabIndex = 11
        Me.cmb_roc_items.TabStop = False
        '
        'dgv_roc
        '
        Me.dgv_roc.AllowUserToAddRows = False
        Me.dgv_roc.AllowUserToDeleteRows = False
        Me.dgv_roc.AllowUserToResizeColumns = False
        Me.dgv_roc.AllowUserToResizeRows = False
        Me.dgv_roc.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.dgv_roc.BackgroundColor = System.Drawing.Color.FromArgb(CType(CType(28, Byte), Integer), CType(CType(40, Byte), Integer), CType(CType(52, Byte), Integer))
        Me.dgv_roc.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.dgv_roc.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.[Single]
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(CType(CType(34, Byte), Integer), CType(CType(48, Byte), Integer), CType(CType(56, Byte), Integer))
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.Color.White
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgv_roc.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.dgv_roc.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgv_roc.ContextMenuStrip = Me.cms_dgv_roc
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(CType(CType(28, Byte), Integer), CType(CType(40, Byte), Integer), CType(CType(52, Byte), Integer))
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.Color.FromArgb(CType(CType(238, Byte), Integer), CType(CType(231, Byte), Integer), CType(CType(227, Byte), Integer))
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.FromArgb(CType(CType(181, Byte), Integer), CType(CType(186, Byte), Integer), CType(CType(188, Byte), Integer))
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgv_roc.DefaultCellStyle = DataGridViewCellStyle2
        Me.dgv_roc.GridColor = System.Drawing.Color.FromArgb(CType(CType(28, Byte), Integer), CType(CType(40, Byte), Integer), CType(CType(52, Byte), Integer))
        Me.dgv_roc.Location = New System.Drawing.Point(19, 1)
        Me.dgv_roc.Margin = New System.Windows.Forms.Padding(5)
        Me.dgv_roc.MultiSelect = False
        Me.dgv_roc.Name = "dgv_roc"
        Me.dgv_roc.RowHeadersVisible = False
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle3.NullValue = Nothing
        Me.dgv_roc.RowsDefaultCellStyle = DataGridViewCellStyle3
        Me.dgv_roc.RowTemplate.DefaultCellStyle.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dgv_roc.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgv_roc.Size = New System.Drawing.Size(1315, 260)
        Me.dgv_roc.TabIndex = 10
        Me.dgv_roc.TabStop = False
        '
        'cms_dgv_roc
        '
        Me.cms_dgv_roc.BackColor = System.Drawing.Color.FromArgb(CType(CType(28, Byte), Integer), CType(CType(40, Byte), Integer), CType(CType(52, Byte), Integer))
        Me.cms_dgv_roc.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cms_dgv_roc.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsmi_cms_roc_remove_entry})
        Me.cms_dgv_roc.Name = "cms_dgv_roc"
        Me.cms_dgv_roc.ShowImageMargin = False
        Me.cms_dgv_roc.Size = New System.Drawing.Size(129, 26)
        '
        'tsmi_cms_roc_remove_entry
        '
        Me.tsmi_cms_roc_remove_entry.ForeColor = System.Drawing.Color.White
        Me.tsmi_cms_roc_remove_entry.Name = "tsmi_cms_roc_remove_entry"
        Me.tsmi_cms_roc_remove_entry.Size = New System.Drawing.Size(128, 22)
        Me.tsmi_cms_roc_remove_entry.Text = "Remove Entry"
        '
        'tsmi_cms_others_remove_entry
        '
        Me.tsmi_cms_others_remove_entry.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tsmi_cms_others_remove_entry.ForeColor = System.Drawing.Color.White
        Me.tsmi_cms_others_remove_entry.Name = "tsmi_cms_others_remove_entry"
        Me.tsmi_cms_others_remove_entry.Size = New System.Drawing.Size(128, 22)
        Me.tsmi_cms_others_remove_entry.Text = "Remove Entry"
        '
        'cms_dgv_others
        '
        Me.cms_dgv_others.BackColor = System.Drawing.Color.FromArgb(CType(CType(28, Byte), Integer), CType(CType(40, Byte), Integer), CType(CType(52, Byte), Integer))
        Me.cms_dgv_others.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsmi_cms_others_remove_entry})
        Me.cms_dgv_others.Name = "cms_dgv_others"
        Me.cms_dgv_others.ShowImageMargin = False
        Me.cms_dgv_others.Size = New System.Drawing.Size(129, 26)
        '
        'cmb_payment_number_others
        '
        Me.cmb_payment_number_others.FormattingEnabled = True
        Me.cmb_payment_number_others.Location = New System.Drawing.Point(433, 275)
        Me.cmb_payment_number_others.Name = "cmb_payment_number_others"
        Me.cmb_payment_number_others.Size = New System.Drawing.Size(121, 21)
        Me.cmb_payment_number_others.TabIndex = 14
        Me.cmb_payment_number_others.TabStop = False
        Me.cmb_payment_number_others.Visible = False
        '
        'btn_others_add
        '
        Me.btn_others_add.BackColor = System.Drawing.Color.FromArgb(CType(CType(218, Byte), Integer), CType(CType(144, Byte), Integer), CType(CType(27, Byte), Integer))
        Me.btn_others_add.FlatAppearance.BorderSize = 0
        Me.btn_others_add.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_others_add.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_others_add.ForeColor = System.Drawing.Color.White
        Me.btn_others_add.Location = New System.Drawing.Point(322, 274)
        Me.btn_others_add.Margin = New System.Windows.Forms.Padding(5)
        Me.btn_others_add.Name = "btn_others_add"
        Me.btn_others_add.Size = New System.Drawing.Size(75, 23)
        Me.btn_others_add.TabIndex = 12
        Me.btn_others_add.TabStop = False
        Me.btn_others_add.Text = "Add"
        Me.btn_others_add.UseVisualStyleBackColor = False
        '
        'cmb_others_items
        '
        Me.cmb_others_items.BackColor = System.Drawing.Color.FromArgb(CType(CType(28, Byte), Integer), CType(CType(40, Byte), Integer), CType(CType(52, Byte), Integer))
        Me.cmb_others_items.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmb_others_items.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.cmb_others_items.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmb_others_items.ForeColor = System.Drawing.Color.White
        Me.cmb_others_items.FormattingEnabled = True
        Me.cmb_others_items.Location = New System.Drawing.Point(19, 275)
        Me.cmb_others_items.Margin = New System.Windows.Forms.Padding(5)
        Me.cmb_others_items.Name = "cmb_others_items"
        Me.cmb_others_items.Size = New System.Drawing.Size(282, 21)
        Me.cmb_others_items.TabIndex = 11
        Me.cmb_others_items.TabStop = False
        '
        'dgv_others
        '
        Me.dgv_others.AllowUserToAddRows = False
        Me.dgv_others.AllowUserToDeleteRows = False
        Me.dgv_others.AllowUserToResizeColumns = False
        Me.dgv_others.AllowUserToResizeRows = False
        Me.dgv_others.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.dgv_others.BackgroundColor = System.Drawing.Color.FromArgb(CType(CType(28, Byte), Integer), CType(CType(40, Byte), Integer), CType(CType(52, Byte), Integer))
        Me.dgv_others.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.dgv_others.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.[Single]
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle4.BackColor = System.Drawing.Color.FromArgb(CType(CType(34, Byte), Integer), CType(CType(48, Byte), Integer), CType(CType(56, Byte), Integer))
        DataGridViewCellStyle4.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle4.ForeColor = System.Drawing.Color.White
        DataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle4.SelectionForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgv_others.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle4
        Me.dgv_others.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgv_others.ContextMenuStrip = Me.cms_dgv_others
        DataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle5.BackColor = System.Drawing.Color.FromArgb(CType(CType(28, Byte), Integer), CType(CType(40, Byte), Integer), CType(CType(52, Byte), Integer))
        DataGridViewCellStyle5.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle5.ForeColor = System.Drawing.Color.FromArgb(CType(CType(238, Byte), Integer), CType(CType(231, Byte), Integer), CType(CType(227, Byte), Integer))
        DataGridViewCellStyle5.SelectionBackColor = System.Drawing.Color.FromArgb(CType(CType(181, Byte), Integer), CType(CType(186, Byte), Integer), CType(CType(188, Byte), Integer))
        DataGridViewCellStyle5.SelectionForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgv_others.DefaultCellStyle = DataGridViewCellStyle5
        Me.dgv_others.GridColor = System.Drawing.Color.FromArgb(CType(CType(28, Byte), Integer), CType(CType(40, Byte), Integer), CType(CType(52, Byte), Integer))
        Me.dgv_others.Location = New System.Drawing.Point(19, 1)
        Me.dgv_others.Margin = New System.Windows.Forms.Padding(5)
        Me.dgv_others.MultiSelect = False
        Me.dgv_others.Name = "dgv_others"
        Me.dgv_others.RowHeadersVisible = False
        DataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle6.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle6.NullValue = Nothing
        Me.dgv_others.RowsDefaultCellStyle = DataGridViewCellStyle6
        Me.dgv_others.RowTemplate.DefaultCellStyle.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dgv_others.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgv_others.Size = New System.Drawing.Size(1315, 260)
        Me.dgv_others.TabIndex = 10
        Me.dgv_others.TabStop = False
        '
        'pnl_others
        '
        Me.pnl_others.BackColor = System.Drawing.Color.FromArgb(CType(CType(113, Byte), Integer), CType(CType(125, Byte), Integer), CType(CType(137, Byte), Integer))
        Me.pnl_others.Controls.Add(Me.cmb_payment_number_others)
        Me.pnl_others.Controls.Add(Me.btn_others_add)
        Me.pnl_others.Controls.Add(Me.cmb_others_items)
        Me.pnl_others.Controls.Add(Me.dgv_others)
        Me.pnl_others.Location = New System.Drawing.Point(2, 137)
        Me.pnl_others.Margin = New System.Windows.Forms.Padding(0)
        Me.pnl_others.Name = "pnl_others"
        Me.pnl_others.Size = New System.Drawing.Size(1348, 316)
        Me.pnl_others.TabIndex = 92
        '
        'cmb_payment_number_license
        '
        Me.cmb_payment_number_license.FormattingEnabled = True
        Me.cmb_payment_number_license.Location = New System.Drawing.Point(438, 275)
        Me.cmb_payment_number_license.Name = "cmb_payment_number_license"
        Me.cmb_payment_number_license.Size = New System.Drawing.Size(121, 21)
        Me.cmb_payment_number_license.TabIndex = 13
        Me.cmb_payment_number_license.TabStop = False
        Me.cmb_payment_number_license.Visible = False
        '
        'btn_license_add
        '
        Me.btn_license_add.BackColor = System.Drawing.Color.FromArgb(CType(CType(218, Byte), Integer), CType(CType(144, Byte), Integer), CType(CType(27, Byte), Integer))
        Me.btn_license_add.FlatAppearance.BorderSize = 0
        Me.btn_license_add.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_license_add.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_license_add.ForeColor = System.Drawing.Color.White
        Me.btn_license_add.Location = New System.Drawing.Point(322, 274)
        Me.btn_license_add.Margin = New System.Windows.Forms.Padding(5)
        Me.btn_license_add.Name = "btn_license_add"
        Me.btn_license_add.Size = New System.Drawing.Size(75, 23)
        Me.btn_license_add.TabIndex = 12
        Me.btn_license_add.TabStop = False
        Me.btn_license_add.Text = "Add"
        Me.btn_license_add.UseVisualStyleBackColor = False
        '
        'cmb_license_items
        '
        Me.cmb_license_items.BackColor = System.Drawing.Color.FromArgb(CType(CType(28, Byte), Integer), CType(CType(40, Byte), Integer), CType(CType(52, Byte), Integer))
        Me.cmb_license_items.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmb_license_items.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.cmb_license_items.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmb_license_items.ForeColor = System.Drawing.Color.White
        Me.cmb_license_items.FormattingEnabled = True
        Me.cmb_license_items.Location = New System.Drawing.Point(19, 275)
        Me.cmb_license_items.Margin = New System.Windows.Forms.Padding(5)
        Me.cmb_license_items.Name = "cmb_license_items"
        Me.cmb_license_items.Size = New System.Drawing.Size(282, 21)
        Me.cmb_license_items.TabIndex = 11
        Me.cmb_license_items.TabStop = False
        '
        'dgv_license
        '
        Me.dgv_license.AllowUserToAddRows = False
        Me.dgv_license.AllowUserToDeleteRows = False
        Me.dgv_license.AllowUserToResizeColumns = False
        Me.dgv_license.AllowUserToResizeRows = False
        Me.dgv_license.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.dgv_license.BackgroundColor = System.Drawing.Color.FromArgb(CType(CType(28, Byte), Integer), CType(CType(40, Byte), Integer), CType(CType(52, Byte), Integer))
        Me.dgv_license.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.dgv_license.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.[Single]
        DataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle7.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle7.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle7.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle7.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle7.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgv_license.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle7
        Me.dgv_license.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgv_license.ContextMenuStrip = Me.cms_dgv_license
        DataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle8.BackColor = System.Drawing.Color.FromArgb(CType(CType(28, Byte), Integer), CType(CType(40, Byte), Integer), CType(CType(52, Byte), Integer))
        DataGridViewCellStyle8.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle8.ForeColor = System.Drawing.Color.FromArgb(CType(CType(238, Byte), Integer), CType(CType(231, Byte), Integer), CType(CType(227, Byte), Integer))
        DataGridViewCellStyle8.SelectionBackColor = System.Drawing.Color.FromArgb(CType(CType(181, Byte), Integer), CType(CType(186, Byte), Integer), CType(CType(188, Byte), Integer))
        DataGridViewCellStyle8.SelectionForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgv_license.DefaultCellStyle = DataGridViewCellStyle8
        Me.dgv_license.GridColor = System.Drawing.Color.FromArgb(CType(CType(28, Byte), Integer), CType(CType(40, Byte), Integer), CType(CType(52, Byte), Integer))
        Me.dgv_license.Location = New System.Drawing.Point(19, 1)
        Me.dgv_license.Margin = New System.Windows.Forms.Padding(0, 5, 5, 5)
        Me.dgv_license.MultiSelect = False
        Me.dgv_license.Name = "dgv_license"
        Me.dgv_license.RowHeadersVisible = False
        DataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle9.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle9.NullValue = Nothing
        Me.dgv_license.RowsDefaultCellStyle = DataGridViewCellStyle9
        Me.dgv_license.RowTemplate.DefaultCellStyle.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dgv_license.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgv_license.Size = New System.Drawing.Size(1315, 260)
        Me.dgv_license.TabIndex = 10
        Me.dgv_license.TabStop = False
        '
        'cms_dgv_license
        '
        Me.cms_dgv_license.BackColor = System.Drawing.Color.FromArgb(CType(CType(28, Byte), Integer), CType(CType(40, Byte), Integer), CType(CType(52, Byte), Integer))
        Me.cms_dgv_license.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsmi_cms_license_remove_entry})
        Me.cms_dgv_license.Name = "dgv_cms"
        Me.cms_dgv_license.ShowImageMargin = False
        Me.cms_dgv_license.Size = New System.Drawing.Size(129, 26)
        '
        'tsmi_cms_license_remove_entry
        '
        Me.tsmi_cms_license_remove_entry.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me.tsmi_cms_license_remove_entry.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tsmi_cms_license_remove_entry.ForeColor = System.Drawing.Color.White
        Me.tsmi_cms_license_remove_entry.Name = "tsmi_cms_license_remove_entry"
        Me.tsmi_cms_license_remove_entry.Size = New System.Drawing.Size(128, 22)
        Me.tsmi_cms_license_remove_entry.Text = "Remove Entry"
        '
        'pnl_license
        '
        Me.pnl_license.BackColor = System.Drawing.Color.FromArgb(CType(CType(113, Byte), Integer), CType(CType(125, Byte), Integer), CType(CType(137, Byte), Integer))
        Me.pnl_license.Controls.Add(Me.cmb_payment_number_license)
        Me.pnl_license.Controls.Add(Me.btn_license_add)
        Me.pnl_license.Controls.Add(Me.cmb_license_items)
        Me.pnl_license.Controls.Add(Me.dgv_license)
        Me.pnl_license.Location = New System.Drawing.Point(2, 137)
        Me.pnl_license.Margin = New System.Windows.Forms.Padding(0)
        Me.pnl_license.Name = "pnl_license"
        Me.pnl_license.Size = New System.Drawing.Size(1348, 316)
        Me.pnl_license.TabIndex = 86
        '
        'btn_tab_license
        '
        Me.btn_tab_license.BackColor = System.Drawing.Color.FromArgb(CType(CType(113, Byte), Integer), CType(CType(125, Byte), Integer), CType(CType(137, Byte), Integer))
        Me.btn_tab_license.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(193, Byte), Integer), CType(CType(7, Byte), Integer))
        Me.btn_tab_license.FlatAppearance.BorderSize = 0
        Me.btn_tab_license.FlatAppearance.CheckedBackColor = System.Drawing.Color.Transparent
        Me.btn_tab_license.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_tab_license.Font = New System.Drawing.Font("Segoe UI", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_tab_license.ForeColor = System.Drawing.Color.White
        Me.btn_tab_license.Location = New System.Drawing.Point(0, 0)
        Me.btn_tab_license.Margin = New System.Windows.Forms.Padding(0)
        Me.btn_tab_license.Name = "btn_tab_license"
        Me.btn_tab_license.Size = New System.Drawing.Size(85, 51)
        Me.btn_tab_license.TabIndex = 0
        Me.btn_tab_license.TabStop = False
        Me.btn_tab_license.Text = "LICENSE"
        Me.btn_tab_license.UseVisualStyleBackColor = False
        '
        'btn_tab_others
        '
        Me.btn_tab_others.BackColor = System.Drawing.Color.FromArgb(CType(CType(69, Byte), Integer), CType(CType(90, Byte), Integer), CType(CType(100, Byte), Integer))
        Me.btn_tab_others.FlatAppearance.BorderSize = 0
        Me.btn_tab_others.FlatAppearance.CheckedBackColor = System.Drawing.Color.Transparent
        Me.btn_tab_others.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_tab_others.Font = New System.Drawing.Font("Segoe UI", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_tab_others.ForeColor = System.Drawing.Color.Silver
        Me.btn_tab_others.Location = New System.Drawing.Point(282, 0)
        Me.btn_tab_others.Margin = New System.Windows.Forms.Padding(0)
        Me.btn_tab_others.Name = "btn_tab_others"
        Me.btn_tab_others.Size = New System.Drawing.Size(85, 51)
        Me.btn_tab_others.TabIndex = 3
        Me.btn_tab_others.TabStop = False
        Me.btn_tab_others.Text = "OTHERS"
        Me.btn_tab_others.UseVisualStyleBackColor = False
        '
        'btn_tab_roc
        '
        Me.btn_tab_roc.BackColor = System.Drawing.Color.FromArgb(CType(CType(69, Byte), Integer), CType(CType(90, Byte), Integer), CType(CType(100, Byte), Integer))
        Me.btn_tab_roc.FlatAppearance.BorderSize = 0
        Me.btn_tab_roc.FlatAppearance.CheckedBackColor = System.Drawing.Color.Transparent
        Me.btn_tab_roc.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_tab_roc.Font = New System.Drawing.Font("Segoe UI", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_tab_roc.ForeColor = System.Drawing.Color.Silver
        Me.btn_tab_roc.Location = New System.Drawing.Point(170, 0)
        Me.btn_tab_roc.Margin = New System.Windows.Forms.Padding(0)
        Me.btn_tab_roc.Name = "btn_tab_roc"
        Me.btn_tab_roc.Size = New System.Drawing.Size(112, 51)
        Me.btn_tab_roc.TabIndex = 2
        Me.btn_tab_roc.TabStop = False
        Me.btn_tab_roc.Text = "AROC/ROC"
        Me.btn_tab_roc.UseVisualStyleBackColor = False
        '
        'btn_tab_permits
        '
        Me.btn_tab_permits.BackColor = System.Drawing.Color.FromArgb(CType(CType(69, Byte), Integer), CType(CType(90, Byte), Integer), CType(CType(100, Byte), Integer))
        Me.btn_tab_permits.FlatAppearance.BorderSize = 0
        Me.btn_tab_permits.FlatAppearance.CheckedBackColor = System.Drawing.Color.Transparent
        Me.btn_tab_permits.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_tab_permits.Font = New System.Drawing.Font("Segoe UI", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_tab_permits.ForeColor = System.Drawing.Color.Silver
        Me.btn_tab_permits.Location = New System.Drawing.Point(85, 0)
        Me.btn_tab_permits.Margin = New System.Windows.Forms.Padding(0)
        Me.btn_tab_permits.Name = "btn_tab_permits"
        Me.btn_tab_permits.Size = New System.Drawing.Size(85, 51)
        Me.btn_tab_permits.TabIndex = 1
        Me.btn_tab_permits.TabStop = False
        Me.btn_tab_permits.Text = "PERMITS"
        Me.btn_tab_permits.UseVisualStyleBackColor = False
        '
        'panel_particular
        '
        Me.panel_particular.BackColor = System.Drawing.Color.FromArgb(CType(CType(113, Byte), Integer), CType(CType(125, Byte), Integer), CType(CType(137, Byte), Integer))
        Me.panel_particular.Location = New System.Drawing.Point(2, 115)
        Me.panel_particular.Margin = New System.Windows.Forms.Padding(0)
        Me.panel_particular.Name = "panel_particular"
        Me.panel_particular.Size = New System.Drawing.Size(1348, 22)
        Me.panel_particular.TabIndex = 85
        '
        'p1SubpanelMenu
        '
        Me.p1SubpanelMenu.BackColor = System.Drawing.Color.FromArgb(CType(CType(34, Byte), Integer), CType(CType(48, Byte), Integer), CType(CType(56, Byte), Integer))
        Me.p1SubpanelMenu.Controls.Add(Me.btn_tab_license)
        Me.p1SubpanelMenu.Controls.Add(Me.btn_tab_others)
        Me.p1SubpanelMenu.Controls.Add(Me.btn_tab_roc)
        Me.p1SubpanelMenu.Controls.Add(Me.btn_tab_permits)
        Me.p1SubpanelMenu.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.p1SubpanelMenu.Location = New System.Drawing.Point(2, 64)
        Me.p1SubpanelMenu.Margin = New System.Windows.Forms.Padding(0)
        Me.p1SubpanelMenu.Name = "p1SubpanelMenu"
        Me.p1SubpanelMenu.Size = New System.Drawing.Size(376, 51)
        Me.p1SubpanelMenu.TabIndex = 89
        '
        'tb_particular_one
        '
        Me.tb_particular_one.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tb_particular_one.Location = New System.Drawing.Point(19, 26)
        Me.tb_particular_one.Margin = New System.Windows.Forms.Padding(5)
        Me.tb_particular_one.MaxLength = 60
        Me.tb_particular_one.Name = "tb_particular_one"
        Me.tb_particular_one.Size = New System.Drawing.Size(223, 23)
        Me.tb_particular_one.TabIndex = 17
        Me.tb_particular_one.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Font = New System.Drawing.Font("Segoe UI", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label18.ForeColor = System.Drawing.Color.DarkGray
        Me.Label18.Location = New System.Drawing.Point(39, 11)
        Me.Label18.Margin = New System.Windows.Forms.Padding(5)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(58, 13)
        Me.Label18.TabIndex = 21
        Me.Label18.Text = "Particular"
        '
        'dtp_expiry_date
        '
        Me.dtp_expiry_date.CalendarFont = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtp_expiry_date.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtp_expiry_date.Location = New System.Drawing.Point(1091, 116)
        Me.dtp_expiry_date.Name = "dtp_expiry_date"
        Me.dtp_expiry_date.Size = New System.Drawing.Size(231, 22)
        Me.dtp_expiry_date.TabIndex = 16
        '
        'Panel5
        '
        Me.Panel5.BackColor = System.Drawing.Color.FromArgb(CType(CType(28, Byte), Integer), CType(CType(40, Byte), Integer), CType(CType(52, Byte), Integer))
        Me.Panel5.Controls.Add(Me.btn_cancel)
        Me.Panel5.Controls.Add(Me.btn_save)
        Me.Panel5.Location = New System.Drawing.Point(6, 629)
        Me.Panel5.Name = "Panel5"
        Me.Panel5.Size = New System.Drawing.Size(256, 59)
        Me.Panel5.TabIndex = 180
        '
        'btn_cancel
        '
        Me.btn_cancel.BackColor = System.Drawing.Color.FromArgb(CType(CType(218, Byte), Integer), CType(CType(144, Byte), Integer), CType(CType(27, Byte), Integer))
        Me.btn_cancel.FlatAppearance.BorderSize = 0
        Me.btn_cancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_cancel.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_cancel.ForeColor = System.Drawing.Color.White
        Me.btn_cancel.Location = New System.Drawing.Point(173, 13)
        Me.btn_cancel.Name = "btn_cancel"
        Me.btn_cancel.Size = New System.Drawing.Size(70, 35)
        Me.btn_cancel.TabIndex = 71
        Me.btn_cancel.Text = "Cancel"
        Me.btn_cancel.UseVisualStyleBackColor = False
        '
        'btn_save
        '
        Me.btn_save.BackColor = System.Drawing.Color.FromArgb(CType(CType(218, Byte), Integer), CType(CType(144, Byte), Integer), CType(CType(27, Byte), Integer))
        Me.btn_save.FlatAppearance.BorderSize = 0
        Me.btn_save.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_save.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_save.ForeColor = System.Drawing.Color.White
        Me.btn_save.Location = New System.Drawing.Point(13, 13)
        Me.btn_save.Name = "btn_save"
        Me.btn_save.Size = New System.Drawing.Size(153, 35)
        Me.btn_save.TabIndex = 27
        Me.btn_save.Text = "Save and Proceed to OP"
        Me.btn_save.UseVisualStyleBackColor = False
        '
        'Panel4
        '
        Me.Panel4.BackColor = System.Drawing.Color.FromArgb(CType(CType(113, Byte), Integer), CType(CType(125, Byte), Integer), CType(CType(137, Byte), Integer))
        Me.Panel4.Controls.Add(Me.tb_total)
        Me.Panel4.Controls.Add(Me.Label24)
        Me.Panel4.Controls.Add(Me.Label5)
        Me.Panel4.Location = New System.Drawing.Point(998, 617)
        Me.Panel4.Margin = New System.Windows.Forms.Padding(0)
        Me.Panel4.Name = "Panel4"
        Me.Panel4.Size = New System.Drawing.Size(352, 48)
        Me.Panel4.TabIndex = 179
        '
        'tb_total
        '
        Me.tb_total.BackColor = System.Drawing.Color.FromArgb(CType(CType(113, Byte), Integer), CType(CType(125, Byte), Integer), CType(CType(137, Byte), Integer))
        Me.tb_total.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.tb_total.Font = New System.Drawing.Font("Segoe UI", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tb_total.ForeColor = System.Drawing.Color.White
        Me.tb_total.Location = New System.Drawing.Point(121, 8)
        Me.tb_total.Name = "tb_total"
        Me.tb_total.ReadOnly = True
        Me.tb_total.Size = New System.Drawing.Size(220, 32)
        Me.tb_total.TabIndex = 94
        Me.tb_total.Text = "0.00"
        Me.tb_total.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label24
        '
        Me.Label24.AutoSize = True
        Me.Label24.BackColor = System.Drawing.Color.Transparent
        Me.Label24.Font = New System.Drawing.Font("Segoe UI", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label24.ForeColor = System.Drawing.Color.White
        Me.Label24.Location = New System.Drawing.Point(15, 20)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(41, 13)
        Me.Label24.TabIndex = 95
        Me.Label24.Text = "TOTAL"
        Me.Label24.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.BackColor = System.Drawing.Color.Transparent
        Me.Label5.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.ForeColor = System.Drawing.Color.White
        Me.Label5.Location = New System.Drawing.Point(63, 12)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(51, 25)
        Me.Label5.TabIndex = 135
        Me.Label5.Text = "PHP"
        Me.Label5.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'btn_addpayor
        '
        Me.btn_addpayor.BackColor = System.Drawing.Color.Transparent
        Me.btn_addpayor.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btn_addpayor.Image = CType(resources.GetObject("btn_addpayor.Image"), System.Drawing.Image)
        Me.btn_addpayor.ImageActive = Nothing
        Me.btn_addpayor.Location = New System.Drawing.Point(628, 52)
        Me.btn_addpayor.Name = "btn_addpayor"
        Me.btn_addpayor.Size = New System.Drawing.Size(31, 31)
        Me.btn_addpayor.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.btn_addpayor.TabIndex = 173
        Me.btn_addpayor.TabStop = False
        Me.btn_addpayor.Zoom = 10
        '
        'pnl_mod_others
        '
        Me.pnl_mod_others.BackColor = System.Drawing.Color.FromArgb(CType(CType(113, Byte), Integer), CType(CType(125, Byte), Integer), CType(CType(137, Byte), Integer))
        Me.pnl_mod_others.Controls.Add(Me.Label2)
        Me.pnl_mod_others.Controls.Add(Me.btn_cancel_others)
        Me.pnl_mod_others.Controls.Add(Me.btn_confirm_others)
        Me.pnl_mod_others.Controls.Add(Me.tb_others_value)
        Me.pnl_mod_others.Location = New System.Drawing.Point(550, 378)
        Me.pnl_mod_others.Name = "pnl_mod_others"
        Me.pnl_mod_others.Size = New System.Drawing.Size(271, 135)
        Me.pnl_mod_others.TabIndex = 165
        Me.pnl_mod_others.Visible = False
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Segoe UI", 12.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.White
        Me.Label2.Location = New System.Drawing.Point(58, 19)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(162, 21)
        Me.Label2.TabIndex = 62
        Me.Label2.Text = "Enter additional info"
        '
        'btn_cancel_others
        '
        Me.btn_cancel_others.BackColor = System.Drawing.Color.FromArgb(CType(CType(218, Byte), Integer), CType(CType(144, Byte), Integer), CType(CType(27, Byte), Integer))
        Me.btn_cancel_others.FlatAppearance.BorderSize = 0
        Me.btn_cancel_others.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_cancel_others.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_cancel_others.ForeColor = System.Drawing.Color.White
        Me.btn_cancel_others.Location = New System.Drawing.Point(161, 91)
        Me.btn_cancel_others.Margin = New System.Windows.Forms.Padding(5)
        Me.btn_cancel_others.Name = "btn_cancel_others"
        Me.btn_cancel_others.Size = New System.Drawing.Size(75, 23)
        Me.btn_cancel_others.TabIndex = 26
        Me.btn_cancel_others.TabStop = False
        Me.btn_cancel_others.Text = "Cancel"
        Me.btn_cancel_others.UseVisualStyleBackColor = False
        '
        'btn_confirm_others
        '
        Me.btn_confirm_others.BackColor = System.Drawing.Color.FromArgb(CType(CType(218, Byte), Integer), CType(CType(144, Byte), Integer), CType(CType(27, Byte), Integer))
        Me.btn_confirm_others.FlatAppearance.BorderSize = 0
        Me.btn_confirm_others.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_confirm_others.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_confirm_others.ForeColor = System.Drawing.Color.White
        Me.btn_confirm_others.Location = New System.Drawing.Point(38, 91)
        Me.btn_confirm_others.Margin = New System.Windows.Forms.Padding(5)
        Me.btn_confirm_others.Name = "btn_confirm_others"
        Me.btn_confirm_others.Size = New System.Drawing.Size(75, 23)
        Me.btn_confirm_others.TabIndex = 25
        Me.btn_confirm_others.TabStop = False
        Me.btn_confirm_others.Text = "Confirm"
        Me.btn_confirm_others.UseVisualStyleBackColor = False
        '
        'tb_others_value
        '
        Me.tb_others_value.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tb_others_value.Location = New System.Drawing.Point(26, 53)
        Me.tb_others_value.Margin = New System.Windows.Forms.Padding(5)
        Me.tb_others_value.MaxLength = 60
        Me.tb_others_value.Name = "tb_others_value"
        Me.tb_others_value.Size = New System.Drawing.Size(223, 25)
        Me.tb_others_value.TabIndex = 24
        Me.tb_others_value.TabStop = False
        Me.tb_others_value.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'cms_dgv_permits
        '
        Me.cms_dgv_permits.BackColor = System.Drawing.Color.FromArgb(CType(CType(28, Byte), Integer), CType(CType(40, Byte), Integer), CType(CType(52, Byte), Integer))
        Me.cms_dgv_permits.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsmi_cms_permits_remove_entry})
        Me.cms_dgv_permits.Name = "cms_dgv_permits"
        Me.cms_dgv_permits.ShowImageMargin = False
        Me.cms_dgv_permits.Size = New System.Drawing.Size(129, 26)
        '
        'tsmi_cms_permits_remove_entry
        '
        Me.tsmi_cms_permits_remove_entry.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tsmi_cms_permits_remove_entry.ForeColor = System.Drawing.Color.White
        Me.tsmi_cms_permits_remove_entry.Name = "tsmi_cms_permits_remove_entry"
        Me.tsmi_cms_permits_remove_entry.Size = New System.Drawing.Size(128, 22)
        Me.tsmi_cms_permits_remove_entry.Text = "Remove Entry"
        '
        'dgv_permits
        '
        Me.dgv_permits.AllowUserToAddRows = False
        Me.dgv_permits.AllowUserToDeleteRows = False
        Me.dgv_permits.AllowUserToResizeColumns = False
        Me.dgv_permits.AllowUserToResizeRows = False
        Me.dgv_permits.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.dgv_permits.BackgroundColor = System.Drawing.Color.FromArgb(CType(CType(28, Byte), Integer), CType(CType(40, Byte), Integer), CType(CType(52, Byte), Integer))
        Me.dgv_permits.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.dgv_permits.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.[Single]
        DataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle10.BackColor = System.Drawing.Color.FromArgb(CType(CType(34, Byte), Integer), CType(CType(48, Byte), Integer), CType(CType(56, Byte), Integer))
        DataGridViewCellStyle10.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle10.ForeColor = System.Drawing.Color.White
        DataGridViewCellStyle10.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle10.SelectionForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle10.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgv_permits.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle10
        Me.dgv_permits.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgv_permits.ContextMenuStrip = Me.cms_dgv_permits
        DataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle11.BackColor = System.Drawing.Color.FromArgb(CType(CType(28, Byte), Integer), CType(CType(40, Byte), Integer), CType(CType(52, Byte), Integer))
        DataGridViewCellStyle11.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle11.ForeColor = System.Drawing.Color.FromArgb(CType(CType(238, Byte), Integer), CType(CType(231, Byte), Integer), CType(CType(227, Byte), Integer))
        DataGridViewCellStyle11.SelectionBackColor = System.Drawing.Color.FromArgb(CType(CType(181, Byte), Integer), CType(CType(186, Byte), Integer), CType(CType(188, Byte), Integer))
        DataGridViewCellStyle11.SelectionForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle11.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgv_permits.DefaultCellStyle = DataGridViewCellStyle11
        Me.dgv_permits.GridColor = System.Drawing.Color.FromArgb(CType(CType(28, Byte), Integer), CType(CType(40, Byte), Integer), CType(CType(52, Byte), Integer))
        Me.dgv_permits.Location = New System.Drawing.Point(19, 1)
        Me.dgv_permits.Margin = New System.Windows.Forms.Padding(5)
        Me.dgv_permits.MultiSelect = False
        Me.dgv_permits.Name = "dgv_permits"
        Me.dgv_permits.RowHeadersVisible = False
        DataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle12.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle12.NullValue = Nothing
        Me.dgv_permits.RowsDefaultCellStyle = DataGridViewCellStyle12
        Me.dgv_permits.RowTemplate.DefaultCellStyle.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dgv_permits.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgv_permits.Size = New System.Drawing.Size(1315, 260)
        Me.dgv_permits.TabIndex = 10
        Me.dgv_permits.TabStop = False
        '
        'cmb_payment_number_permits
        '
        Me.cmb_payment_number_permits.FormattingEnabled = True
        Me.cmb_payment_number_permits.Location = New System.Drawing.Point(424, 275)
        Me.cmb_payment_number_permits.Name = "cmb_payment_number_permits"
        Me.cmb_payment_number_permits.Size = New System.Drawing.Size(121, 21)
        Me.cmb_payment_number_permits.TabIndex = 14
        Me.cmb_payment_number_permits.TabStop = False
        Me.cmb_payment_number_permits.Visible = False
        '
        'btn_permits_add
        '
        Me.btn_permits_add.BackColor = System.Drawing.Color.FromArgb(CType(CType(218, Byte), Integer), CType(CType(144, Byte), Integer), CType(CType(27, Byte), Integer))
        Me.btn_permits_add.FlatAppearance.BorderSize = 0
        Me.btn_permits_add.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_permits_add.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_permits_add.ForeColor = System.Drawing.Color.White
        Me.btn_permits_add.Location = New System.Drawing.Point(322, 274)
        Me.btn_permits_add.Margin = New System.Windows.Forms.Padding(5)
        Me.btn_permits_add.Name = "btn_permits_add"
        Me.btn_permits_add.Size = New System.Drawing.Size(75, 23)
        Me.btn_permits_add.TabIndex = 12
        Me.btn_permits_add.TabStop = False
        Me.btn_permits_add.Text = "Add"
        Me.btn_permits_add.UseVisualStyleBackColor = False
        '
        'cmb_permits_items
        '
        Me.cmb_permits_items.BackColor = System.Drawing.Color.FromArgb(CType(CType(28, Byte), Integer), CType(CType(40, Byte), Integer), CType(CType(52, Byte), Integer))
        Me.cmb_permits_items.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmb_permits_items.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.cmb_permits_items.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmb_permits_items.ForeColor = System.Drawing.Color.White
        Me.cmb_permits_items.FormattingEnabled = True
        Me.cmb_permits_items.Location = New System.Drawing.Point(19, 275)
        Me.cmb_permits_items.Margin = New System.Windows.Forms.Padding(5)
        Me.cmb_permits_items.Name = "cmb_permits_items"
        Me.cmb_permits_items.Size = New System.Drawing.Size(282, 21)
        Me.cmb_permits_items.TabIndex = 11
        Me.cmb_permits_items.TabStop = False
        '
        'pnl_permits
        '
        Me.pnl_permits.BackColor = System.Drawing.Color.FromArgb(CType(CType(113, Byte), Integer), CType(CType(125, Byte), Integer), CType(CType(137, Byte), Integer))
        Me.pnl_permits.Controls.Add(Me.cmb_payment_number_permits)
        Me.pnl_permits.Controls.Add(Me.btn_permits_add)
        Me.pnl_permits.Controls.Add(Me.cmb_permits_items)
        Me.pnl_permits.Controls.Add(Me.dgv_permits)
        Me.pnl_permits.Location = New System.Drawing.Point(2, 137)
        Me.pnl_permits.Margin = New System.Windows.Forms.Padding(0)
        Me.pnl_permits.Name = "pnl_permits"
        Me.pnl_permits.Size = New System.Drawing.Size(1348, 316)
        Me.pnl_permits.TabIndex = 90
        '
        'dtp_p1_from
        '
        Me.dtp_p1_from.CalendarFont = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtp_p1_from.Enabled = False
        Me.dtp_p1_from.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtp_p1_from.Location = New System.Drawing.Point(42, 70)
        Me.dtp_p1_from.Name = "dtp_p1_from"
        Me.dtp_p1_from.Size = New System.Drawing.Size(200, 22)
        Me.dtp_p1_from.TabIndex = 18
        '
        'txtpayor
        '
        Me.txtpayor.BackColor = System.Drawing.Color.FromArgb(CType(CType(114, Byte), Integer), CType(CType(126, Byte), Integer), CType(CType(138, Byte), Integer))
        Me.txtpayor.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtpayor.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtpayor.ForeColor = System.Drawing.Color.LightGray
        Me.txtpayor.Location = New System.Drawing.Point(26, 56)
        Me.txtpayor.MaxLength = 120
        Me.txtpayor.Name = "txtpayor"
        Me.txtpayor.ReadOnly = True
        Me.txtpayor.Size = New System.Drawing.Size(599, 22)
        Me.txtpayor.TabIndex = 177
        Me.txtpayor.TabStop = False
        Me.txtpayor.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'cb_process_others
        '
        Me.cb_process_others.AutoSize = True
        Me.cb_process_others.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cb_process_others.ForeColor = System.Drawing.Color.White
        Me.cb_process_others.Location = New System.Drawing.Point(76, 15)
        Me.cb_process_others.Name = "cb_process_others"
        Me.cb_process_others.Size = New System.Drawing.Size(67, 17)
        Me.cb_process_others.TabIndex = 9
        Me.cb_process_others.Text = "OTHERS"
        Me.cb_process_others.UseVisualStyleBackColor = True
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.cb_process_others)
        Me.GroupBox2.Controls.Add(Me.cb_process_mod)
        Me.GroupBox2.Controls.Add(Me.cb_process_dup)
        Me.GroupBox2.Font = New System.Drawing.Font("Segoe UI", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox2.ForeColor = System.Drawing.Color.DarkGray
        Me.GroupBox2.Location = New System.Drawing.Point(188, 99)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(148, 53)
        Me.GroupBox2.TabIndex = 176
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Process Type"
        '
        'cb_process_mod
        '
        Me.cb_process_mod.AutoSize = True
        Me.cb_process_mod.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cb_process_mod.ForeColor = System.Drawing.Color.White
        Me.cb_process_mod.Location = New System.Drawing.Point(14, 15)
        Me.cb_process_mod.Name = "cb_process_mod"
        Me.cb_process_mod.Size = New System.Drawing.Size(53, 17)
        Me.cb_process_mod.TabIndex = 7
        Me.cb_process_mod.Text = "MOD"
        Me.cb_process_mod.UseVisualStyleBackColor = True
        '
        'cb_process_dup
        '
        Me.cb_process_dup.AutoSize = True
        Me.cb_process_dup.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cb_process_dup.ForeColor = System.Drawing.Color.White
        Me.cb_process_dup.Location = New System.Drawing.Point(14, 33)
        Me.cb_process_dup.Name = "cb_process_dup"
        Me.cb_process_dup.Size = New System.Drawing.Size(49, 17)
        Me.cb_process_dup.TabIndex = 8
        Me.cb_process_dup.Text = "DUP"
        Me.cb_process_dup.UseVisualStyleBackColor = True
        '
        'cb_services_others
        '
        Me.cb_services_others.AutoSize = True
        Me.cb_services_others.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cb_services_others.ForeColor = System.Drawing.Color.White
        Me.cb_services_others.Location = New System.Drawing.Point(165, 33)
        Me.cb_services_others.Name = "cb_services_others"
        Me.cb_services_others.Size = New System.Drawing.Size(67, 17)
        Me.cb_services_others.TabIndex = 15
        Me.cb_services_others.Text = "OTHERS"
        Me.cb_services_others.UseVisualStyleBackColor = True
        '
        'cb_services_roc
        '
        Me.cb_services_roc.AutoSize = True
        Me.cb_services_roc.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cb_services_roc.ForeColor = System.Drawing.Color.White
        Me.cb_services_roc.Location = New System.Drawing.Point(165, 15)
        Me.cb_services_roc.Name = "cb_services_roc"
        Me.cb_services_roc.Size = New System.Drawing.Size(48, 17)
        Me.cb_services_roc.TabIndex = 14
        Me.cb_services_roc.Text = "ROC"
        Me.cb_services_roc.UseVisualStyleBackColor = True
        '
        'cb_services_ma
        '
        Me.cb_services_ma.AutoSize = True
        Me.cb_services_ma.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cb_services_ma.ForeColor = System.Drawing.Color.White
        Me.cb_services_ma.Location = New System.Drawing.Point(91, 33)
        Me.cb_services_ma.Name = "cb_services_ma"
        Me.cb_services_ma.Size = New System.Drawing.Size(45, 17)
        Me.cb_services_ma.TabIndex = 13
        Me.cb_services_ma.Text = "MA"
        Me.cb_services_ma.UseVisualStyleBackColor = True
        '
        'cb_services_ms
        '
        Me.cb_services_ms.AutoSize = True
        Me.cb_services_ms.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cb_services_ms.ForeColor = System.Drawing.Color.White
        Me.cb_services_ms.Location = New System.Drawing.Point(91, 15)
        Me.cb_services_ms.Name = "cb_services_ms"
        Me.cb_services_ms.Size = New System.Drawing.Size(43, 17)
        Me.cb_services_ms.TabIndex = 12
        Me.cb_services_ms.Text = "MS"
        Me.cb_services_ms.UseVisualStyleBackColor = True
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.cb_services_others)
        Me.GroupBox1.Controls.Add(Me.cb_services_roc)
        Me.GroupBox1.Controls.Add(Me.cb_services_ma)
        Me.GroupBox1.Controls.Add(Me.cb_services_ms)
        Me.GroupBox1.Controls.Add(Me.cb_services_co)
        Me.GroupBox1.Controls.Add(Me.cb_services_cv)
        Me.GroupBox1.Font = New System.Drawing.Font("Segoe UI", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox1.ForeColor = System.Drawing.Color.DarkGray
        Me.GroupBox1.Location = New System.Drawing.Point(363, 99)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(242, 53)
        Me.GroupBox1.TabIndex = 175
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Service Type"
        '
        'cb_services_co
        '
        Me.cb_services_co.AutoSize = True
        Me.cb_services_co.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cb_services_co.ForeColor = System.Drawing.Color.White
        Me.cb_services_co.Location = New System.Drawing.Point(13, 15)
        Me.cb_services_co.Name = "cb_services_co"
        Me.cb_services_co.Size = New System.Drawing.Size(41, 17)
        Me.cb_services_co.TabIndex = 10
        Me.cb_services_co.Text = "CO"
        Me.cb_services_co.UseVisualStyleBackColor = True
        '
        'cb_services_cv
        '
        Me.cb_services_cv.AutoSize = True
        Me.cb_services_cv.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cb_services_cv.ForeColor = System.Drawing.Color.White
        Me.cb_services_cv.Location = New System.Drawing.Point(13, 33)
        Me.cb_services_cv.Name = "cb_services_cv"
        Me.cb_services_cv.Size = New System.Drawing.Size(40, 17)
        Me.cb_services_cv.TabIndex = 11
        Me.cb_services_cv.Text = "CV"
        Me.cb_services_cv.UseVisualStyleBackColor = True
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.BackColor = System.Drawing.Color.Transparent
        Me.Label8.Font = New System.Drawing.Font("Segoe UI", 9.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.ForeColor = System.Drawing.Color.DarkGray
        Me.Label8.Location = New System.Drawing.Point(1091, 98)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(96, 15)
        Me.Label8.TabIndex = 174
        Me.Label8.Text = "Expiration Date"
        Me.Label8.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Font = New System.Drawing.Font("Segoe UI", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label13.ForeColor = System.Drawing.Color.DarkGray
        Me.Label13.Location = New System.Drawing.Point(22, 108)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(96, 13)
        Me.Label13.TabIndex = 172
        Me.Label13.Text = "Transaction Type"
        '
        'lbl_others
        '
        Me.lbl_others.AutoSize = True
        Me.lbl_others.Font = New System.Drawing.Font("Segoe UI", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_others.ForeColor = System.Drawing.Color.DarkGray
        Me.lbl_others.Location = New System.Drawing.Point(613, 108)
        Me.lbl_others.Name = "lbl_others"
        Me.lbl_others.Size = New System.Drawing.Size(41, 13)
        Me.lbl_others.TabIndex = 171
        Me.lbl_others.Text = "Others"
        Me.lbl_others.Visible = False
        '
        'dtp_date_created
        '
        Me.dtp_date_created.CalendarFont = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtp_date_created.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtp_date_created.Location = New System.Drawing.Point(1091, 58)
        Me.dtp_date_created.Name = "dtp_date_created"
        Me.dtp_date_created.Size = New System.Drawing.Size(231, 22)
        Me.dtp_date_created.TabIndex = 5
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Segoe UI", 9.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ForeColor = System.Drawing.Color.DarkGray
        Me.Label4.Location = New System.Drawing.Point(1091, 40)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(34, 15)
        Me.Label4.TabIndex = 169
        Me.Label4.Text = "Date"
        '
        'tb_others
        '
        Me.tb_others.BackColor = System.Drawing.Color.FromArgb(CType(CType(28, Byte), Integer), CType(CType(40, Byte), Integer), CType(CType(52, Byte), Integer))
        Me.tb_others.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.tb_others.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tb_others.ForeColor = System.Drawing.Color.White
        Me.tb_others.Location = New System.Drawing.Point(613, 126)
        Me.tb_others.MaxLength = 30
        Me.tb_others.Name = "tb_others"
        Me.tb_others.Size = New System.Drawing.Size(169, 22)
        Me.tb_others.TabIndex = 168
        Me.tb_others.TabStop = False
        Me.tb_others.Visible = False
        '
        'cmb_type
        '
        Me.cmb_type.BackColor = System.Drawing.Color.FromArgb(CType(CType(28, Byte), Integer), CType(CType(40, Byte), Integer), CType(CType(52, Byte), Integer))
        Me.cmb_type.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmb_type.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.cmb_type.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmb_type.ForeColor = System.Drawing.Color.White
        Me.cmb_type.FormattingEnabled = True
        Me.cmb_type.Items.AddRange(New Object() {"NEW", "RENEW", "OTHERS"})
        Me.cmb_type.Location = New System.Drawing.Point(22, 125)
        Me.cmb_type.Name = "cmb_type"
        Me.cmb_type.Size = New System.Drawing.Size(141, 25)
        Me.cmb_type.TabIndex = 6
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Segoe UI", 9.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.DarkGray
        Me.Label1.Location = New System.Drawing.Point(23, 38)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(40, 15)
        Me.Label1.TabIndex = 166
        Me.Label1.Text = "Payor"
        '
        'RadTitleBar1
        '
        Me.RadTitleBar1.Dock = System.Windows.Forms.DockStyle.Top
        Me.RadTitleBar1.Location = New System.Drawing.Point(0, 0)
        Me.RadTitleBar1.Name = "RadTitleBar1"
        Me.RadTitleBar1.Size = New System.Drawing.Size(1350, 23)
        Me.RadTitleBar1.TabIndex = 164
        Me.RadTitleBar1.TabStop = False
        Me.RadTitleBar1.Text = "NTC Region 10"
        CType(Me.RadTitleBar1.GetChildAt(0), Telerik.WinControls.UI.RadTitleBarElement).Text = "NTC Region 10"
        CType(Me.RadTitleBar1.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(3), Telerik.WinControls.UI.RadImageButtonElement).Visibility = Telerik.WinControls.ElementVisibility.Collapsed
        '
        'dtp_p1_to
        '
        Me.dtp_p1_to.CalendarFont = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtp_p1_to.Enabled = False
        Me.dtp_p1_to.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtp_p1_to.Location = New System.Drawing.Point(42, 93)
        Me.dtp_p1_to.Name = "dtp_p1_to"
        Me.dtp_p1_to.Size = New System.Drawing.Size(200, 22)
        Me.dtp_p1_to.TabIndex = 19
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Font = New System.Drawing.Font("Segoe UI", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label16.ForeColor = System.Drawing.Color.DarkGray
        Me.Label16.Location = New System.Drawing.Point(9, 74)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(33, 13)
        Me.Label16.TabIndex = 26
        Me.Label16.Text = "From"
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.Font = New System.Drawing.Font("Segoe UI", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label19.ForeColor = System.Drawing.Color.DarkGray
        Me.Label19.Location = New System.Drawing.Point(9, 97)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(19, 13)
        Me.Label19.TabIndex = 27
        Me.Label19.Text = "To"
        '
        'Panel3
        '
        Me.Panel3.BackColor = System.Drawing.Color.FromArgb(CType(CType(36, Byte), Integer), CType(CType(48, Byte), Integer), CType(CType(59, Byte), Integer))
        Me.Panel3.Controls.Add(Me.tb_particular_two)
        Me.Panel3.Controls.Add(Me.Label15)
        Me.Panel3.Controls.Add(Me.dtp_p2_from)
        Me.Panel3.Controls.Add(Me.dtp_p2_to)
        Me.Panel3.Controls.Add(Me.Label21)
        Me.Panel3.Controls.Add(Me.Label22)
        Me.Panel3.Controls.Add(Me.cb_particular_two)
        Me.Panel3.Location = New System.Drawing.Point(676, 9)
        Me.Panel3.Margin = New System.Windows.Forms.Padding(3, 3, 0, 3)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(259, 128)
        Me.Panel3.TabIndex = 44
        '
        'tb_particular_two
        '
        Me.tb_particular_two.Enabled = False
        Me.tb_particular_two.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tb_particular_two.Location = New System.Drawing.Point(19, 26)
        Me.tb_particular_two.Margin = New System.Windows.Forms.Padding(5)
        Me.tb_particular_two.MaxLength = 60
        Me.tb_particular_two.Name = "tb_particular_two"
        Me.tb_particular_two.Size = New System.Drawing.Size(223, 23)
        Me.tb_particular_two.TabIndex = 23
        Me.tb_particular_two.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Font = New System.Drawing.Font("Segoe UI", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label15.ForeColor = System.Drawing.Color.DarkGray
        Me.Label15.Location = New System.Drawing.Point(39, 55)
        Me.Label15.Margin = New System.Windows.Forms.Padding(5)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(85, 13)
        Me.Label15.TabIndex = 22
        Me.Label15.Text = "Period Covered"
        '
        'dtp_p2_from
        '
        Me.dtp_p2_from.CalendarFont = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtp_p2_from.Enabled = False
        Me.dtp_p2_from.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtp_p2_from.Location = New System.Drawing.Point(42, 70)
        Me.dtp_p2_from.Name = "dtp_p2_from"
        Me.dtp_p2_from.Size = New System.Drawing.Size(200, 22)
        Me.dtp_p2_from.TabIndex = 21
        '
        'dtp_p2_to
        '
        Me.dtp_p2_to.CalendarFont = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtp_p2_to.Enabled = False
        Me.dtp_p2_to.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtp_p2_to.Location = New System.Drawing.Point(42, 93)
        Me.dtp_p2_to.Name = "dtp_p2_to"
        Me.dtp_p2_to.Size = New System.Drawing.Size(200, 22)
        Me.dtp_p2_to.TabIndex = 22
        '
        'Label21
        '
        Me.Label21.AutoSize = True
        Me.Label21.Font = New System.Drawing.Font("Segoe UI", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label21.ForeColor = System.Drawing.Color.DarkGray
        Me.Label21.Location = New System.Drawing.Point(9, 74)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(33, 13)
        Me.Label21.TabIndex = 26
        Me.Label21.Text = "From"
        '
        'Label22
        '
        Me.Label22.AutoSize = True
        Me.Label22.Font = New System.Drawing.Font("Segoe UI", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label22.ForeColor = System.Drawing.Color.DarkGray
        Me.Label22.Location = New System.Drawing.Point(9, 97)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(19, 13)
        Me.Label22.TabIndex = 27
        Me.Label22.Text = "To"
        '
        'cb_particular_two
        '
        Me.cb_particular_two.AutoSize = True
        Me.cb_particular_two.Font = New System.Drawing.Font("Segoe UI", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cb_particular_two.ForeColor = System.Drawing.Color.DarkGray
        Me.cb_particular_two.Location = New System.Drawing.Point(23, 10)
        Me.cb_particular_two.Name = "cb_particular_two"
        Me.cb_particular_two.Size = New System.Drawing.Size(77, 17)
        Me.cb_particular_two.TabIndex = 20
        Me.cb_particular_two.Text = "Particular"
        Me.cb_particular_two.UseVisualStyleBackColor = True
        '
        'tb_particular_three
        '
        Me.tb_particular_three.Enabled = False
        Me.tb_particular_three.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tb_particular_three.Location = New System.Drawing.Point(19, 26)
        Me.tb_particular_three.Margin = New System.Windows.Forms.Padding(5)
        Me.tb_particular_three.MaxLength = 60
        Me.tb_particular_three.Name = "tb_particular_three"
        Me.tb_particular_three.Size = New System.Drawing.Size(223, 23)
        Me.tb_particular_three.TabIndex = 24
        Me.tb_particular_three.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label23
        '
        Me.Label23.AutoSize = True
        Me.Label23.Font = New System.Drawing.Font("Segoe UI", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label23.ForeColor = System.Drawing.Color.DarkGray
        Me.Label23.Location = New System.Drawing.Point(40, 55)
        Me.Label23.Margin = New System.Windows.Forms.Padding(5)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(85, 13)
        Me.Label23.TabIndex = 22
        Me.Label23.Text = "Period Covered"
        '
        'dtp_p3_from
        '
        Me.dtp_p3_from.CalendarFont = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtp_p3_from.Enabled = False
        Me.dtp_p3_from.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtp_p3_from.Location = New System.Drawing.Point(42, 70)
        Me.dtp_p3_from.Name = "dtp_p3_from"
        Me.dtp_p3_from.Size = New System.Drawing.Size(200, 22)
        Me.dtp_p3_from.TabIndex = 24
        '
        'dtp_p3_to
        '
        Me.dtp_p3_to.CalendarFont = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtp_p3_to.Enabled = False
        Me.dtp_p3_to.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtp_p3_to.Location = New System.Drawing.Point(42, 93)
        Me.dtp_p3_to.Name = "dtp_p3_to"
        Me.dtp_p3_to.Size = New System.Drawing.Size(200, 22)
        Me.dtp_p3_to.TabIndex = 25
        '
        'Label27
        '
        Me.Label27.AutoSize = True
        Me.Label27.Font = New System.Drawing.Font("Segoe UI", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label27.ForeColor = System.Drawing.Color.DarkGray
        Me.Label27.Location = New System.Drawing.Point(9, 74)
        Me.Label27.Name = "Label27"
        Me.Label27.Size = New System.Drawing.Size(33, 13)
        Me.Label27.TabIndex = 26
        Me.Label27.Text = "From"
        '
        'Label28
        '
        Me.Label28.AutoSize = True
        Me.Label28.Font = New System.Drawing.Font("Segoe UI", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label28.ForeColor = System.Drawing.Color.DarkGray
        Me.Label28.Location = New System.Drawing.Point(9, 97)
        Me.Label28.Name = "Label28"
        Me.Label28.Size = New System.Drawing.Size(19, 13)
        Me.Label28.TabIndex = 27
        Me.Label28.Text = "To"
        '
        'cb_particular_three
        '
        Me.cb_particular_three.AutoSize = True
        Me.cb_particular_three.Enabled = False
        Me.cb_particular_three.Font = New System.Drawing.Font("Segoe UI", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cb_particular_three.ForeColor = System.Drawing.Color.DarkGray
        Me.cb_particular_three.Location = New System.Drawing.Point(24, 11)
        Me.cb_particular_three.Name = "cb_particular_three"
        Me.cb_particular_three.Size = New System.Drawing.Size(77, 17)
        Me.cb_particular_three.TabIndex = 23
        Me.cb_particular_three.Text = "Particular"
        Me.cb_particular_three.UseVisualStyleBackColor = True
        '
        'Panel2
        '
        Me.Panel2.BackColor = System.Drawing.Color.FromArgb(CType(CType(36, Byte), Integer), CType(CType(48, Byte), Integer), CType(CType(59, Byte), Integer))
        Me.Panel2.Controls.Add(Me.tb_particular_three)
        Me.Panel2.Controls.Add(Me.Label23)
        Me.Panel2.Controls.Add(Me.dtp_p3_from)
        Me.Panel2.Controls.Add(Me.dtp_p3_to)
        Me.Panel2.Controls.Add(Me.Label27)
        Me.Panel2.Controls.Add(Me.Label28)
        Me.Panel2.Controls.Add(Me.cb_particular_three)
        Me.Panel2.Location = New System.Drawing.Point(955, 9)
        Me.Panel2.Margin = New System.Windows.Forms.Padding(3, 3, 0, 3)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(259, 128)
        Me.Panel2.TabIndex = 43
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.FromArgb(CType(CType(36, Byte), Integer), CType(CType(48, Byte), Integer), CType(CType(59, Byte), Integer))
        Me.Panel1.Controls.Add(Me.tb_particular_one)
        Me.Panel1.Controls.Add(Me.Label17)
        Me.Panel1.Controls.Add(Me.Label18)
        Me.Panel1.Controls.Add(Me.dtp_p1_from)
        Me.Panel1.Controls.Add(Me.dtp_p1_to)
        Me.Panel1.Controls.Add(Me.Label16)
        Me.Panel1.Controls.Add(Me.Label19)
        Me.Panel1.Location = New System.Drawing.Point(393, 9)
        Me.Panel1.Margin = New System.Windows.Forms.Padding(3, 3, 0, 3)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(259, 128)
        Me.Panel1.TabIndex = 42
        '
        'panelParticular1
        '
        Me.panelParticular1.BackColor = System.Drawing.Color.FromArgb(CType(CType(44, Byte), Integer), CType(CType(62, Byte), Integer), CType(CType(80, Byte), Integer))
        Me.panelParticular1.Controls.Add(Me.Panel3)
        Me.panelParticular1.Controls.Add(Me.Panel2)
        Me.panelParticular1.Controls.Add(Me.Panel1)
        Me.panelParticular1.Controls.Add(Me.panel_particular)
        Me.panelParticular1.Controls.Add(Me.p1SubpanelMenu)
        Me.panelParticular1.Controls.Add(Me.pnl_license)
        Me.panelParticular1.Controls.Add(Me.pnl_others)
        Me.panelParticular1.Controls.Add(Me.pnl_roc)
        Me.panelParticular1.Controls.Add(Me.pnl_permits)
        Me.panelParticular1.Location = New System.Drawing.Point(0, 168)
        Me.panelParticular1.Name = "panelParticular1"
        Me.panelParticular1.Size = New System.Drawing.Size(1350, 454)
        Me.panelParticular1.TabIndex = 178
        '
        'tb_serial_starting_digit
        '
        Me.tb_serial_starting_digit.BackColor = System.Drawing.Color.FromArgb(CType(CType(114, Byte), Integer), CType(CType(126, Byte), Integer), CType(CType(138, Byte), Integer))
        Me.tb_serial_starting_digit.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.tb_serial_starting_digit.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tb_serial_starting_digit.ForeColor = System.Drawing.Color.LightGray
        Me.tb_serial_starting_digit.Location = New System.Drawing.Point(14, 58)
        Me.tb_serial_starting_digit.MaxLength = 4
        Me.tb_serial_starting_digit.Name = "tb_serial_starting_digit"
        Me.tb_serial_starting_digit.ReadOnly = True
        Me.tb_serial_starting_digit.Size = New System.Drawing.Size(47, 22)
        Me.tb_serial_starting_digit.TabIndex = 1
        Me.tb_serial_starting_digit.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.tb_serial_starting_digit.Visible = False
        '
        'tb_serial_year
        '
        Me.tb_serial_year.BackColor = System.Drawing.Color.FromArgb(CType(CType(28, Byte), Integer), CType(CType(40, Byte), Integer), CType(CType(52, Byte), Integer))
        Me.tb_serial_year.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.tb_serial_year.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tb_serial_year.ForeColor = System.Drawing.Color.LightGray
        Me.tb_serial_year.Location = New System.Drawing.Point(70, 58)
        Me.tb_serial_year.MaxLength = 4
        Me.tb_serial_year.Name = "tb_serial_year"
        Me.tb_serial_year.Size = New System.Drawing.Size(47, 22)
        Me.tb_serial_year.TabIndex = 2
        Me.tb_serial_year.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.tb_serial_year.Visible = False
        '
        'tb_serial_month
        '
        Me.tb_serial_month.BackColor = System.Drawing.Color.FromArgb(CType(CType(28, Byte), Integer), CType(CType(40, Byte), Integer), CType(CType(52, Byte), Integer))
        Me.tb_serial_month.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.tb_serial_month.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tb_serial_month.ForeColor = System.Drawing.Color.LightGray
        Me.tb_serial_month.Location = New System.Drawing.Point(126, 58)
        Me.tb_serial_month.MaxLength = 2
        Me.tb_serial_month.Name = "tb_serial_month"
        Me.tb_serial_month.Size = New System.Drawing.Size(47, 22)
        Me.tb_serial_month.TabIndex = 3
        Me.tb_serial_month.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.tb_serial_month.Visible = False
        '
        'tb_serial_series
        '
        Me.tb_serial_series.BackColor = System.Drawing.Color.FromArgb(CType(CType(28, Byte), Integer), CType(CType(40, Byte), Integer), CType(CType(52, Byte), Integer))
        Me.tb_serial_series.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.tb_serial_series.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tb_serial_series.ForeColor = System.Drawing.Color.LightGray
        Me.tb_serial_series.Location = New System.Drawing.Point(182, 58)
        Me.tb_serial_series.MaxLength = 4
        Me.tb_serial_series.Name = "tb_serial_series"
        Me.tb_serial_series.Size = New System.Drawing.Size(47, 22)
        Me.tb_serial_series.TabIndex = 4
        Me.tb_serial_series.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.tb_serial_series.Visible = False
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.BackColor = System.Drawing.Color.Transparent
        Me.Label3.Font = New System.Drawing.Font("Segoe UI", 9.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ForeColor = System.Drawing.Color.DarkGray
        Me.Label3.Location = New System.Drawing.Point(307, 654)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(69, 15)
        Me.Label3.TabIndex = 187
        Me.Label3.Text = "Preparator"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'cmb_preparator
        '
        Me.cmb_preparator.BackColor = System.Drawing.Color.FromArgb(CType(CType(28, Byte), Integer), CType(CType(40, Byte), Integer), CType(CType(52, Byte), Integer))
        Me.cmb_preparator.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmb_preparator.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.cmb_preparator.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmb_preparator.ForeColor = System.Drawing.Color.White
        Me.cmb_preparator.FormattingEnabled = True
        Me.cmb_preparator.Location = New System.Drawing.Point(382, 652)
        Me.cmb_preparator.Name = "cmb_preparator"
        Me.cmb_preparator.Size = New System.Drawing.Size(241, 21)
        Me.cmb_preparator.TabIndex = 26
        '
        'cmb_approver
        '
        Me.cmb_approver.BackColor = System.Drawing.Color.FromArgb(CType(CType(28, Byte), Integer), CType(CType(40, Byte), Integer), CType(CType(52, Byte), Integer))
        Me.cmb_approver.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmb_approver.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.cmb_approver.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmb_approver.ForeColor = System.Drawing.Color.White
        Me.cmb_approver.FormattingEnabled = True
        Me.cmb_approver.Location = New System.Drawing.Point(715, 652)
        Me.cmb_approver.Name = "cmb_approver"
        Me.cmb_approver.Size = New System.Drawing.Size(241, 21)
        Me.cmb_approver.TabIndex = 26
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.BackColor = System.Drawing.Color.Transparent
        Me.Label7.Font = New System.Drawing.Font("Segoe UI", 9.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.ForeColor = System.Drawing.Color.DarkGray
        Me.Label7.Location = New System.Drawing.Point(651, 654)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(58, 15)
        Me.Label7.TabIndex = 189
        Me.Label7.Text = "Approver"
        Me.Label7.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'cmb_preparator_id
        '
        Me.cmb_preparator_id.FormattingEnabled = True
        Me.cmb_preparator_id.Location = New System.Drawing.Point(433, 628)
        Me.cmb_preparator_id.Name = "cmb_preparator_id"
        Me.cmb_preparator_id.Size = New System.Drawing.Size(52, 21)
        Me.cmb_preparator_id.TabIndex = 191
        Me.cmb_preparator_id.TabStop = False
        Me.cmb_preparator_id.Visible = False
        '
        'cmb_approver_id
        '
        Me.cmb_approver_id.FormattingEnabled = True
        Me.cmb_approver_id.Location = New System.Drawing.Point(758, 625)
        Me.cmb_approver_id.Name = "cmb_approver_id"
        Me.cmb_approver_id.Size = New System.Drawing.Size(52, 21)
        Me.cmb_approver_id.TabIndex = 192
        Me.cmb_approver_id.TabStop = False
        Me.cmb_approver_id.Visible = False
        '
        'rbAuto
        '
        Me.rbAuto.AutoSize = True
        Me.rbAuto.Checked = True
        Me.rbAuto.Font = New System.Drawing.Font("Segoe UI", 9.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rbAuto.ForeColor = System.Drawing.Color.DarkGray
        Me.rbAuto.Location = New System.Drawing.Point(14, 8)
        Me.rbAuto.Name = "rbAuto"
        Me.rbAuto.Size = New System.Drawing.Size(155, 19)
        Me.rbAuto.TabIndex = 194
        Me.rbAuto.TabStop = True
        Me.rbAuto.Text = "Auto Generate SOA No."
        Me.rbAuto.UseVisualStyleBackColor = True
        '
        'rbCustom
        '
        Me.rbCustom.AutoSize = True
        Me.rbCustom.Font = New System.Drawing.Font("Segoe UI", 9.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rbCustom.ForeColor = System.Drawing.Color.DarkGray
        Me.rbCustom.Location = New System.Drawing.Point(14, 36)
        Me.rbCustom.Name = "rbCustom"
        Me.rbCustom.Size = New System.Drawing.Size(117, 19)
        Me.rbCustom.TabIndex = 195
        Me.rbCustom.Text = "Custom SOA No."
        Me.rbCustom.UseVisualStyleBackColor = True
        '
        'Panel6
        '
        Me.Panel6.Controls.Add(Me.rbAuto)
        Me.Panel6.Controls.Add(Me.rbCustom)
        Me.Panel6.Controls.Add(Me.tb_serial_starting_digit)
        Me.Panel6.Controls.Add(Me.tb_serial_year)
        Me.Panel6.Controls.Add(Me.tb_serial_month)
        Me.Panel6.Controls.Add(Me.tb_serial_series)
        Me.Panel6.Location = New System.Drawing.Point(827, 51)
        Me.Panel6.Name = "Panel6"
        Me.Panel6.Size = New System.Drawing.Size(240, 87)
        Me.Panel6.TabIndex = 196
        '
        'frm_licenser_manual
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(44, Byte), Integer), CType(CType(62, Byte), Integer), CType(CType(80, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(1350, 690)
        Me.Controls.Add(Me.Panel6)
        Me.Controls.Add(Me.cmb_approver_id)
        Me.Controls.Add(Me.cmb_preparator_id)
        Me.Controls.Add(Me.cmb_approver)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.cmb_preparator)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.dtp_expiry_date)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.Panel5)
        Me.Controls.Add(Me.Panel4)
        Me.Controls.Add(Me.btn_addpayor)
        Me.Controls.Add(Me.pnl_mod_others)
        Me.Controls.Add(Me.txtpayor)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.Label13)
        Me.Controls.Add(Me.lbl_others)
        Me.Controls.Add(Me.dtp_date_created)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.tb_others)
        Me.Controls.Add(Me.cmb_type)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.RadTitleBar1)
        Me.Controls.Add(Me.panelParticular1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.MaximizeBox = False
        Me.MaximumSize = New System.Drawing.Size(1350, 690)
        Me.MinimizeBox = False
        Me.MinimumSize = New System.Drawing.Size(1350, 690)
        Me.Name = "frm_licenser_manual"
        Me.ShowIcon = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "NTC Region 10"
        Me.pnl_roc.ResumeLayout(False)
        CType(Me.dgv_roc, System.ComponentModel.ISupportInitialize).EndInit()
        Me.cms_dgv_roc.ResumeLayout(False)
        Me.cms_dgv_others.ResumeLayout(False)
        CType(Me.dgv_others, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnl_others.ResumeLayout(False)
        CType(Me.dgv_license, System.ComponentModel.ISupportInitialize).EndInit()
        Me.cms_dgv_license.ResumeLayout(False)
        Me.pnl_license.ResumeLayout(False)
        Me.p1SubpanelMenu.ResumeLayout(False)
        Me.Panel5.ResumeLayout(False)
        Me.Panel4.ResumeLayout(False)
        Me.Panel4.PerformLayout()
        CType(Me.btn_addpayor, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnl_mod_others.ResumeLayout(False)
        Me.pnl_mod_others.PerformLayout()
        Me.cms_dgv_permits.ResumeLayout(False)
        CType(Me.dgv_permits, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnl_permits.ResumeLayout(False)
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.RadTitleBar1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel3.ResumeLayout(False)
        Me.Panel3.PerformLayout()
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.panelParticular1.ResumeLayout(False)
        Me.Panel6.ResumeLayout(False)
        Me.Panel6.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents Label17 As Label
    Friend WithEvents pnl_roc As Panel
    Friend WithEvents cmb_payment_number_roc As ComboBox
    Friend WithEvents btn_roc_add As Button
    Friend WithEvents cmb_roc_items As ComboBox
    Friend WithEvents dgv_roc As DataGridView
    Friend WithEvents cms_dgv_roc As ContextMenuStrip
    Friend WithEvents tsmi_cms_roc_remove_entry As ToolStripMenuItem
    Friend WithEvents tsmi_cms_others_remove_entry As ToolStripMenuItem
    Friend WithEvents cms_dgv_others As ContextMenuStrip
    Friend WithEvents cmb_payment_number_others As ComboBox
    Friend WithEvents btn_others_add As Button
    Friend WithEvents cmb_others_items As ComboBox
    Friend WithEvents dgv_others As DataGridView
    Friend WithEvents pnl_others As Panel
    Friend WithEvents cmb_payment_number_license As ComboBox
    Friend WithEvents btn_license_add As Button
    Friend WithEvents cmb_license_items As ComboBox
    Friend WithEvents dgv_license As DataGridView
    Friend WithEvents cms_dgv_license As ContextMenuStrip
    Friend WithEvents tsmi_cms_license_remove_entry As ToolStripMenuItem
    Friend WithEvents pnl_license As Panel
    Friend WithEvents btn_tab_license As Button
    Friend WithEvents btn_tab_others As Button
    Friend WithEvents btn_tab_roc As Button
    Friend WithEvents btn_tab_permits As Button
    Friend WithEvents panel_particular As Panel
    Friend WithEvents p1SubpanelMenu As Panel
    Friend WithEvents tb_particular_one As TextBox
    Friend WithEvents Label18 As Label
    Friend WithEvents dtp_expiry_date As DateTimePicker
    Friend WithEvents Panel5 As Panel
    Friend WithEvents btn_save As Button
    Friend WithEvents Panel4 As Panel
    Friend WithEvents tb_total As TextBox
    Friend WithEvents Label24 As Label
    Friend WithEvents Label5 As Label
    Friend WithEvents btn_addpayor As Bunifu.Framework.UI.BunifuImageButton
    Friend WithEvents pnl_mod_others As Panel
    Friend WithEvents Label2 As Label
    Friend WithEvents btn_cancel_others As Button
    Friend WithEvents btn_confirm_others As Button
    Friend WithEvents tb_others_value As TextBox
    Friend WithEvents cms_dgv_permits As ContextMenuStrip
    Friend WithEvents tsmi_cms_permits_remove_entry As ToolStripMenuItem
    Friend WithEvents dgv_permits As DataGridView
    Friend WithEvents cmb_payment_number_permits As ComboBox
    Friend WithEvents btn_permits_add As Button
    Friend WithEvents cmb_permits_items As ComboBox
    Friend WithEvents pnl_permits As Panel
    Friend WithEvents dtp_p1_from As DateTimePicker
    Friend WithEvents txtpayor As TextBox
    Friend WithEvents cb_process_others As CheckBox
    Friend WithEvents GroupBox2 As GroupBox
    Friend WithEvents cb_process_mod As CheckBox
    Friend WithEvents cb_process_dup As CheckBox
    Friend WithEvents cb_services_others As CheckBox
    Friend WithEvents cb_services_roc As CheckBox
    Friend WithEvents cb_services_ma As CheckBox
    Friend WithEvents cb_services_ms As CheckBox
    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents cb_services_co As CheckBox
    Friend WithEvents cb_services_cv As CheckBox
    Friend WithEvents Label8 As Label
    Friend WithEvents Label13 As Label
    Friend WithEvents lbl_others As Label
    Friend WithEvents dtp_date_created As DateTimePicker
    Friend WithEvents Label4 As Label
    Friend WithEvents tb_others As TextBox
    Friend WithEvents cmb_type As ComboBox
    Friend WithEvents Label1 As Label
    Friend WithEvents RadTitleBar1 As Telerik.WinControls.UI.RadTitleBar
    Friend WithEvents dtp_p1_to As DateTimePicker
    Friend WithEvents Label16 As Label
    Friend WithEvents Label19 As Label
    Friend WithEvents Panel3 As Panel
    Friend WithEvents tb_particular_two As TextBox
    Friend WithEvents Label15 As Label
    Friend WithEvents dtp_p2_from As DateTimePicker
    Friend WithEvents dtp_p2_to As DateTimePicker
    Friend WithEvents Label21 As Label
    Friend WithEvents Label22 As Label
    Friend WithEvents cb_particular_two As CheckBox
    Friend WithEvents tb_particular_three As TextBox
    Friend WithEvents Label23 As Label
    Friend WithEvents dtp_p3_from As DateTimePicker
    Friend WithEvents dtp_p3_to As DateTimePicker
    Friend WithEvents Label27 As Label
    Friend WithEvents Label28 As Label
    Friend WithEvents cb_particular_three As CheckBox
    Friend WithEvents Panel2 As Panel
    Friend WithEvents Panel1 As Panel
    Friend WithEvents panelParticular1 As Panel
    Friend WithEvents tb_serial_starting_digit As TextBox
    Friend WithEvents tb_serial_year As TextBox
    Friend WithEvents tb_serial_month As TextBox
    Friend WithEvents tb_serial_series As TextBox
    Friend WithEvents Label3 As Label
    Friend WithEvents cmb_preparator As ComboBox
    Friend WithEvents cmb_approver As ComboBox
    Friend WithEvents Label7 As Label
    Friend WithEvents cmb_preparator_id As ComboBox
    Friend WithEvents cmb_approver_id As ComboBox
    Friend WithEvents rbAuto As RadioButton
    Friend WithEvents rbCustom As RadioButton
    Friend WithEvents Panel6 As Panel
    Friend WithEvents btn_cancel As Button
End Class
