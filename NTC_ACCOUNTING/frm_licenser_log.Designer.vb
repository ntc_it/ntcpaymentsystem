﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frm_licenser_log
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle7 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle8 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.RadTitleBar1 = New Telerik.WinControls.UI.RadTitleBar()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.dgv_soa_data = New System.Windows.Forms.DataGridView()
        Me.tb_transact_type = New System.Windows.Forms.TextBox()
        Me.tb_process_type = New System.Windows.Forms.TextBox()
        Me.tb_service_type = New System.Windows.Forms.TextBox()
        Me.tb_date_created = New System.Windows.Forms.TextBox()
        Me.tb_serial_num = New System.Windows.Forms.TextBox()
        Me.tb_expiration = New System.Windows.Forms.TextBox()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.tb_payor = New System.Windows.Forms.TextBox()
        Me.tb_total = New System.Windows.Forms.TextBox()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.btn_print_assessment = New System.Windows.Forms.Button()
        Me.btn_print_endorse = New System.Windows.Forms.Button()
        Me.btn_edit = New System.Windows.Forms.Button()
        Me.btn_add_approve = New System.Windows.Forms.Button()
        Me.tb_prepared_by = New System.Windows.Forms.TextBox()
        Me.tb_approved = New System.Windows.Forms.TextBox()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.tb_particular_one_pc_to = New System.Windows.Forms.TextBox()
        Me.tb_particular_one_pc_from = New System.Windows.Forms.TextBox()
        Me.tb_particular_one = New System.Windows.Forms.TextBox()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.tb_particular_three_pc_to = New System.Windows.Forms.TextBox()
        Me.tb_particular_three_pc_from = New System.Windows.Forms.TextBox()
        Me.tb_particular_three = New System.Windows.Forms.TextBox()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.Label21 = New System.Windows.Forms.Label()
        Me.Label22 = New System.Windows.Forms.Label()
        Me.Label23 = New System.Windows.Forms.Label()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.tb_particular_two_pc_to = New System.Windows.Forms.TextBox()
        Me.tb_particular_two = New System.Windows.Forms.TextBox()
        Me.tb_particular_two_pc_from = New System.Windows.Forms.TextBox()
        Me.Label24 = New System.Windows.Forms.Label()
        Me.Label25 = New System.Windows.Forms.Label()
        Me.Label26 = New System.Windows.Forms.Label()
        Me.Label27 = New System.Windows.Forms.Label()
        Me.Panel12 = New System.Windows.Forms.Panel()
        Me.btn_attachment = New System.Windows.Forms.Button()
        Me.btn_endorse = New System.Windows.Forms.Button()
        Me.btn_print_as_menu = New System.Windows.Forms.Button()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Panel4 = New System.Windows.Forms.Panel()
        Me.pb_approve_status = New System.Windows.Forms.PictureBox()
        Me.pnl_external = New System.Windows.Forms.Panel()
        Me.btn_receipt = New System.Windows.Forms.Button()
        Me.btn_op = New System.Windows.Forms.Button()
        Me.tb_message = New System.Windows.Forms.TextBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.tb_date_modified = New System.Windows.Forms.TextBox()
        Me.btn_reasses = New System.Windows.Forms.Button()
        Me.pnl_print = New System.Windows.Forms.Panel()
        Me.btn_print_complete = New System.Windows.Forms.Button()
        Me.pnl_refresh = New System.Windows.Forms.Panel()
        Me.btn_refresh = New System.Windows.Forms.Button()
        CType(Me.RadTitleBar1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgv_soa_data, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        Me.Panel2.SuspendLayout()
        Me.Panel3.SuspendLayout()
        Me.Panel12.SuspendLayout()
        Me.Panel4.SuspendLayout()
        CType(Me.pb_approve_status, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnl_external.SuspendLayout()
        Me.pnl_print.SuspendLayout()
        Me.pnl_refresh.SuspendLayout()
        Me.SuspendLayout()
        '
        'RadTitleBar1
        '
        Me.RadTitleBar1.Dock = System.Windows.Forms.DockStyle.Top
        Me.RadTitleBar1.Location = New System.Drawing.Point(0, 0)
        Me.RadTitleBar1.Name = "RadTitleBar1"
        Me.RadTitleBar1.Size = New System.Drawing.Size(1350, 23)
        Me.RadTitleBar1.TabIndex = 0
        Me.RadTitleBar1.TabStop = False
        Me.RadTitleBar1.Text = "NTC Region 10"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.ForeColor = System.Drawing.Color.DarkGray
        Me.Label8.Location = New System.Drawing.Point(505, 653)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(59, 17)
        Me.Label8.TabIndex = 31
        Me.Label8.Text = "Approver"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.ForeColor = System.Drawing.Color.DarkGray
        Me.Label7.Location = New System.Drawing.Point(496, 612)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(68, 17)
        Me.Label7.TabIndex = 30
        Me.Label7.Text = "Preparator"
        '
        'dgv_soa_data
        '
        Me.dgv_soa_data.AllowUserToAddRows = False
        Me.dgv_soa_data.AllowUserToDeleteRows = False
        Me.dgv_soa_data.AllowUserToResizeColumns = False
        Me.dgv_soa_data.AllowUserToResizeRows = False
        Me.dgv_soa_data.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.dgv_soa_data.BackgroundColor = System.Drawing.Color.FromArgb(CType(CType(28, Byte), Integer), CType(CType(40, Byte), Integer), CType(CType(52, Byte), Integer))
        Me.dgv_soa_data.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.dgv_soa_data.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.[Single]
        DataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle7.BackColor = System.Drawing.Color.DarkGray
        DataGridViewCellStyle7.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle7.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle7.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle7.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgv_soa_data.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle7
        Me.dgv_soa_data.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        DataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle8.BackColor = System.Drawing.Color.FromArgb(CType(CType(28, Byte), Integer), CType(CType(40, Byte), Integer), CType(CType(52, Byte), Integer))
        DataGridViewCellStyle8.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle8.ForeColor = System.Drawing.Color.White
        DataGridViewCellStyle8.SelectionBackColor = System.Drawing.Color.FromArgb(CType(CType(181, Byte), Integer), CType(CType(186, Byte), Integer), CType(CType(188, Byte), Integer))
        DataGridViewCellStyle8.SelectionForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgv_soa_data.DefaultCellStyle = DataGridViewCellStyle8
        Me.dgv_soa_data.GridColor = System.Drawing.Color.FromArgb(CType(CType(28, Byte), Integer), CType(CType(40, Byte), Integer), CType(CType(52, Byte), Integer))
        Me.dgv_soa_data.Location = New System.Drawing.Point(12, 280)
        Me.dgv_soa_data.Margin = New System.Windows.Forms.Padding(0)
        Me.dgv_soa_data.MultiSelect = False
        Me.dgv_soa_data.Name = "dgv_soa_data"
        Me.dgv_soa_data.ReadOnly = True
        Me.dgv_soa_data.RowHeadersVisible = False
        Me.dgv_soa_data.RowTemplate.DefaultCellStyle.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dgv_soa_data.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgv_soa_data.Size = New System.Drawing.Size(1329, 309)
        Me.dgv_soa_data.TabIndex = 67
        '
        'tb_transact_type
        '
        Me.tb_transact_type.BackColor = System.Drawing.Color.FromArgb(CType(CType(44, Byte), Integer), CType(CType(62, Byte), Integer), CType(CType(80, Byte), Integer))
        Me.tb_transact_type.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.tb_transact_type.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tb_transact_type.ForeColor = System.Drawing.Color.White
        Me.tb_transact_type.Location = New System.Drawing.Point(127, 70)
        Me.tb_transact_type.Name = "tb_transact_type"
        Me.tb_transact_type.ReadOnly = True
        Me.tb_transact_type.Size = New System.Drawing.Size(264, 18)
        Me.tb_transact_type.TabIndex = 68
        Me.tb_transact_type.TabStop = False
        Me.tb_transact_type.Text = "xxxxx"
        '
        'tb_process_type
        '
        Me.tb_process_type.BackColor = System.Drawing.Color.FromArgb(CType(CType(44, Byte), Integer), CType(CType(62, Byte), Integer), CType(CType(80, Byte), Integer))
        Me.tb_process_type.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.tb_process_type.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tb_process_type.ForeColor = System.Drawing.Color.White
        Me.tb_process_type.Location = New System.Drawing.Point(127, 96)
        Me.tb_process_type.Name = "tb_process_type"
        Me.tb_process_type.ReadOnly = True
        Me.tb_process_type.Size = New System.Drawing.Size(306, 18)
        Me.tb_process_type.TabIndex = 69
        Me.tb_process_type.TabStop = False
        Me.tb_process_type.Text = "xx, xx"
        '
        'tb_service_type
        '
        Me.tb_service_type.BackColor = System.Drawing.Color.FromArgb(CType(CType(44, Byte), Integer), CType(CType(62, Byte), Integer), CType(CType(80, Byte), Integer))
        Me.tb_service_type.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.tb_service_type.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tb_service_type.ForeColor = System.Drawing.Color.White
        Me.tb_service_type.Location = New System.Drawing.Point(127, 124)
        Me.tb_service_type.Name = "tb_service_type"
        Me.tb_service_type.ReadOnly = True
        Me.tb_service_type.Size = New System.Drawing.Size(341, 18)
        Me.tb_service_type.TabIndex = 70
        Me.tb_service_type.TabStop = False
        Me.tb_service_type.Text = "xx, xx"
        '
        'tb_date_created
        '
        Me.tb_date_created.BackColor = System.Drawing.Color.FromArgb(CType(CType(44, Byte), Integer), CType(CType(62, Byte), Integer), CType(CType(80, Byte), Integer))
        Me.tb_date_created.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.tb_date_created.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tb_date_created.ForeColor = System.Drawing.Color.White
        Me.tb_date_created.Location = New System.Drawing.Point(913, 70)
        Me.tb_date_created.Name = "tb_date_created"
        Me.tb_date_created.ReadOnly = True
        Me.tb_date_created.Size = New System.Drawing.Size(402, 18)
        Me.tb_date_created.TabIndex = 71
        Me.tb_date_created.TabStop = False
        Me.tb_date_created.Text = "yyyy-MM-dd"
        '
        'tb_serial_num
        '
        Me.tb_serial_num.BackColor = System.Drawing.Color.FromArgb(CType(CType(44, Byte), Integer), CType(CType(62, Byte), Integer), CType(CType(80, Byte), Integer))
        Me.tb_serial_num.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.tb_serial_num.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tb_serial_num.ForeColor = System.Drawing.Color.White
        Me.tb_serial_num.Location = New System.Drawing.Point(913, 42)
        Me.tb_serial_num.Name = "tb_serial_num"
        Me.tb_serial_num.ReadOnly = True
        Me.tb_serial_num.Size = New System.Drawing.Size(144, 18)
        Me.tb_serial_num.TabIndex = 72
        Me.tb_serial_num.TabStop = False
        Me.tb_serial_num.Text = "yyyy-MM-xxxx"
        '
        'tb_expiration
        '
        Me.tb_expiration.BackColor = System.Drawing.Color.FromArgb(CType(CType(44, Byte), Integer), CType(CType(62, Byte), Integer), CType(CType(80, Byte), Integer))
        Me.tb_expiration.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.tb_expiration.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tb_expiration.ForeColor = System.Drawing.Color.White
        Me.tb_expiration.Location = New System.Drawing.Point(913, 124)
        Me.tb_expiration.Name = "tb_expiration"
        Me.tb_expiration.ReadOnly = True
        Me.tb_expiration.Size = New System.Drawing.Size(257, 18)
        Me.tb_expiration.TabIndex = 73
        Me.tb_expiration.TabStop = False
        Me.tb_expiration.Text = "yyyy-MM-dd"
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label15.ForeColor = System.Drawing.Color.DarkGray
        Me.Label15.Location = New System.Drawing.Point(30, 71)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(73, 17)
        Me.Label15.TabIndex = 79
        Me.Label15.Text = "Transaction"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ForeColor = System.Drawing.Color.DarkGray
        Me.Label3.Location = New System.Drawing.Point(54, 97)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(49, 17)
        Me.Label3.TabIndex = 80
        Me.Label3.Text = "Process"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ForeColor = System.Drawing.Color.DarkGray
        Me.Label4.Location = New System.Drawing.Point(57, 125)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(46, 17)
        Me.Label4.TabIndex = 81
        Me.Label4.Text = "Service"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.ForeColor = System.Drawing.Color.DarkGray
        Me.Label5.Location = New System.Drawing.Point(816, 125)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(76, 17)
        Me.Label5.TabIndex = 82
        Me.Label5.Text = "Date Expires"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.ForeColor = System.Drawing.Color.DarkGray
        Me.Label6.Location = New System.Drawing.Point(755, 71)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(137, 17)
        Me.Label6.TabIndex = 83
        Me.Label6.Text = "Date and Time Created"
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label14.ForeColor = System.Drawing.Color.DarkGray
        Me.Label14.Location = New System.Drawing.Point(836, 43)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(56, 17)
        Me.Label14.TabIndex = 84
        Me.Label14.Text = "SOA No."
        '
        'tb_payor
        '
        Me.tb_payor.BackColor = System.Drawing.Color.FromArgb(CType(CType(114, Byte), Integer), CType(CType(126, Byte), Integer), CType(CType(138, Byte), Integer))
        Me.tb_payor.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.tb_payor.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tb_payor.ForeColor = System.Drawing.Color.LightGray
        Me.tb_payor.Location = New System.Drawing.Point(127, 40)
        Me.tb_payor.MaxLength = 120
        Me.tb_payor.Name = "tb_payor"
        Me.tb_payor.ReadOnly = True
        Me.tb_payor.Size = New System.Drawing.Size(599, 22)
        Me.tb_payor.TabIndex = 89
        Me.tb_payor.TabStop = False
        Me.tb_payor.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'tb_total
        '
        Me.tb_total.BackColor = System.Drawing.Color.FromArgb(CType(CType(28, Byte), Integer), CType(CType(40, Byte), Integer), CType(CType(52, Byte), Integer))
        Me.tb_total.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.tb_total.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tb_total.ForeColor = System.Drawing.Color.White
        Me.tb_total.Location = New System.Drawing.Point(94, 6)
        Me.tb_total.Name = "tb_total"
        Me.tb_total.ReadOnly = True
        Me.tb_total.Size = New System.Drawing.Size(192, 22)
        Me.tb_total.TabIndex = 91
        Me.tb_total.Text = "0"
        Me.tb_total.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Font = New System.Drawing.Font("Segoe UI", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label16.ForeColor = System.Drawing.Color.White
        Me.Label16.Location = New System.Drawing.Point(6, 11)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(33, 13)
        Me.Label16.TabIndex = 92
        Me.Label16.Text = "Total"
        '
        'btn_print_assessment
        '
        Me.btn_print_assessment.BackColor = System.Drawing.Color.FromArgb(CType(CType(218, Byte), Integer), CType(CType(144, Byte), Integer), CType(CType(27, Byte), Integer))
        Me.btn_print_assessment.FlatAppearance.BorderSize = 0
        Me.btn_print_assessment.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_print_assessment.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_print_assessment.ForeColor = System.Drawing.Color.White
        Me.btn_print_assessment.Location = New System.Drawing.Point(6, 6)
        Me.btn_print_assessment.Name = "btn_print_assessment"
        Me.btn_print_assessment.Size = New System.Drawing.Size(110, 35)
        Me.btn_print_assessment.TabIndex = 96
        Me.btn_print_assessment.Text = "Assessment"
        Me.btn_print_assessment.UseVisualStyleBackColor = False
        '
        'btn_print_endorse
        '
        Me.btn_print_endorse.BackColor = System.Drawing.Color.FromArgb(CType(CType(218, Byte), Integer), CType(CType(144, Byte), Integer), CType(CType(27, Byte), Integer))
        Me.btn_print_endorse.FlatAppearance.BorderSize = 0
        Me.btn_print_endorse.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_print_endorse.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_print_endorse.ForeColor = System.Drawing.Color.White
        Me.btn_print_endorse.Location = New System.Drawing.Point(6, 45)
        Me.btn_print_endorse.Name = "btn_print_endorse"
        Me.btn_print_endorse.Size = New System.Drawing.Size(110, 35)
        Me.btn_print_endorse.TabIndex = 97
        Me.btn_print_endorse.Text = "Endorsement"
        Me.btn_print_endorse.UseVisualStyleBackColor = False
        '
        'btn_edit
        '
        Me.btn_edit.BackColor = System.Drawing.Color.FromArgb(CType(CType(218, Byte), Integer), CType(CType(144, Byte), Integer), CType(CType(27, Byte), Integer))
        Me.btn_edit.FlatAppearance.BorderSize = 0
        Me.btn_edit.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_edit.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_edit.ForeColor = System.Drawing.Color.White
        Me.btn_edit.Location = New System.Drawing.Point(181, 7)
        Me.btn_edit.Name = "btn_edit"
        Me.btn_edit.Size = New System.Drawing.Size(80, 35)
        Me.btn_edit.TabIndex = 98
        Me.btn_edit.Text = "Edit"
        Me.btn_edit.UseVisualStyleBackColor = False
        '
        'btn_add_approve
        '
        Me.btn_add_approve.BackColor = System.Drawing.Color.FromArgb(CType(CType(218, Byte), Integer), CType(CType(144, Byte), Integer), CType(CType(27, Byte), Integer))
        Me.btn_add_approve.FlatAppearance.BorderSize = 0
        Me.btn_add_approve.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_add_approve.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_add_approve.ForeColor = System.Drawing.Color.White
        Me.btn_add_approve.Location = New System.Drawing.Point(841, 649)
        Me.btn_add_approve.Name = "btn_add_approve"
        Me.btn_add_approve.Size = New System.Drawing.Size(75, 25)
        Me.btn_add_approve.TabIndex = 99
        Me.btn_add_approve.Text = "Submit to"
        Me.btn_add_approve.UseVisualStyleBackColor = False
        '
        'tb_prepared_by
        '
        Me.tb_prepared_by.BackColor = System.Drawing.Color.FromArgb(CType(CType(44, Byte), Integer), CType(CType(62, Byte), Integer), CType(CType(80, Byte), Integer))
        Me.tb_prepared_by.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.tb_prepared_by.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tb_prepared_by.ForeColor = System.Drawing.Color.White
        Me.tb_prepared_by.Location = New System.Drawing.Point(598, 611)
        Me.tb_prepared_by.Name = "tb_prepared_by"
        Me.tb_prepared_by.ReadOnly = True
        Me.tb_prepared_by.Size = New System.Drawing.Size(188, 18)
        Me.tb_prepared_by.TabIndex = 100
        Me.tb_prepared_by.Text = "************  *. ********"
        '
        'tb_approved
        '
        Me.tb_approved.BackColor = System.Drawing.Color.FromArgb(CType(CType(44, Byte), Integer), CType(CType(62, Byte), Integer), CType(CType(80, Byte), Integer))
        Me.tb_approved.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.tb_approved.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tb_approved.ForeColor = System.Drawing.Color.White
        Me.tb_approved.Location = New System.Drawing.Point(598, 652)
        Me.tb_approved.Name = "tb_approved"
        Me.tb_approved.ReadOnly = True
        Me.tb_approved.Size = New System.Drawing.Size(188, 18)
        Me.tb_approved.TabIndex = 102
        Me.tb_approved.Text = "************  *. ********"
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.FromArgb(CType(CType(36, Byte), Integer), CType(CType(48, Byte), Integer), CType(CType(59, Byte), Integer))
        Me.Panel1.Controls.Add(Me.tb_particular_one_pc_to)
        Me.Panel1.Controls.Add(Me.tb_particular_one_pc_from)
        Me.Panel1.Controls.Add(Me.tb_particular_one)
        Me.Panel1.Controls.Add(Me.Label17)
        Me.Panel1.Controls.Add(Me.Label18)
        Me.Panel1.Controls.Add(Me.Label13)
        Me.Panel1.Controls.Add(Me.Label19)
        Me.Panel1.Location = New System.Drawing.Point(397, 158)
        Me.Panel1.Margin = New System.Windows.Forms.Padding(0)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(259, 122)
        Me.Panel1.TabIndex = 107
        '
        'tb_particular_one_pc_to
        '
        Me.tb_particular_one_pc_to.BackColor = System.Drawing.Color.FromArgb(CType(CType(36, Byte), Integer), CType(CType(48, Byte), Integer), CType(CType(59, Byte), Integer))
        Me.tb_particular_one_pc_to.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.tb_particular_one_pc_to.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tb_particular_one_pc_to.ForeColor = System.Drawing.Color.White
        Me.tb_particular_one_pc_to.Location = New System.Drawing.Point(49, 97)
        Me.tb_particular_one_pc_to.Name = "tb_particular_one_pc_to"
        Me.tb_particular_one_pc_to.Size = New System.Drawing.Size(200, 18)
        Me.tb_particular_one_pc_to.TabIndex = 29
        Me.tb_particular_one_pc_to.Text = "Date"
        Me.tb_particular_one_pc_to.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'tb_particular_one_pc_from
        '
        Me.tb_particular_one_pc_from.BackColor = System.Drawing.Color.FromArgb(CType(CType(36, Byte), Integer), CType(CType(48, Byte), Integer), CType(CType(59, Byte), Integer))
        Me.tb_particular_one_pc_from.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.tb_particular_one_pc_from.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tb_particular_one_pc_from.ForeColor = System.Drawing.Color.White
        Me.tb_particular_one_pc_from.Location = New System.Drawing.Point(49, 75)
        Me.tb_particular_one_pc_from.Name = "tb_particular_one_pc_from"
        Me.tb_particular_one_pc_from.Size = New System.Drawing.Size(200, 18)
        Me.tb_particular_one_pc_from.TabIndex = 28
        Me.tb_particular_one_pc_from.Text = "Date"
        Me.tb_particular_one_pc_from.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'tb_particular_one
        '
        Me.tb_particular_one.BackColor = System.Drawing.Color.FromArgb(CType(CType(36, Byte), Integer), CType(CType(48, Byte), Integer), CType(CType(59, Byte), Integer))
        Me.tb_particular_one.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.tb_particular_one.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tb_particular_one.ForeColor = System.Drawing.Color.White
        Me.tb_particular_one.Location = New System.Drawing.Point(19, 26)
        Me.tb_particular_one.Margin = New System.Windows.Forms.Padding(5)
        Me.tb_particular_one.Name = "tb_particular_one"
        Me.tb_particular_one.Size = New System.Drawing.Size(223, 18)
        Me.tb_particular_one.TabIndex = 23
        Me.tb_particular_one.Text = "Particular"
        Me.tb_particular_one.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Font = New System.Drawing.Font("Segoe UI", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label17.ForeColor = System.Drawing.Color.DarkGray
        Me.Label17.Location = New System.Drawing.Point(40, 55)
        Me.Label17.Margin = New System.Windows.Forms.Padding(5)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(85, 13)
        Me.Label17.TabIndex = 22
        Me.Label17.Text = "Period Covered"
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Font = New System.Drawing.Font("Segoe UI", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label18.ForeColor = System.Drawing.Color.DarkGray
        Me.Label18.Location = New System.Drawing.Point(39, 11)
        Me.Label18.Margin = New System.Windows.Forms.Padding(5)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(58, 13)
        Me.Label18.TabIndex = 21
        Me.Label18.Text = "Particular"
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Font = New System.Drawing.Font("Segoe UI", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label13.ForeColor = System.Drawing.Color.DarkGray
        Me.Label13.Location = New System.Drawing.Point(9, 78)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(33, 13)
        Me.Label13.TabIndex = 26
        Me.Label13.Text = "From"
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.Font = New System.Drawing.Font("Segoe UI", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label19.ForeColor = System.Drawing.Color.DarkGray
        Me.Label19.Location = New System.Drawing.Point(9, 100)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(19, 13)
        Me.Label19.TabIndex = 27
        Me.Label19.Text = "To"
        '
        'Panel2
        '
        Me.Panel2.BackColor = System.Drawing.Color.FromArgb(CType(CType(36, Byte), Integer), CType(CType(48, Byte), Integer), CType(CType(59, Byte), Integer))
        Me.Panel2.Controls.Add(Me.tb_particular_three_pc_to)
        Me.Panel2.Controls.Add(Me.tb_particular_three_pc_from)
        Me.Panel2.Controls.Add(Me.tb_particular_three)
        Me.Panel2.Controls.Add(Me.Label20)
        Me.Panel2.Controls.Add(Me.Label21)
        Me.Panel2.Controls.Add(Me.Label22)
        Me.Panel2.Controls.Add(Me.Label23)
        Me.Panel2.Location = New System.Drawing.Point(992, 158)
        Me.Panel2.Margin = New System.Windows.Forms.Padding(0)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(259, 122)
        Me.Panel2.TabIndex = 108
        '
        'tb_particular_three_pc_to
        '
        Me.tb_particular_three_pc_to.BackColor = System.Drawing.Color.FromArgb(CType(CType(36, Byte), Integer), CType(CType(48, Byte), Integer), CType(CType(59, Byte), Integer))
        Me.tb_particular_three_pc_to.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.tb_particular_three_pc_to.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tb_particular_three_pc_to.ForeColor = System.Drawing.Color.White
        Me.tb_particular_three_pc_to.Location = New System.Drawing.Point(48, 97)
        Me.tb_particular_three_pc_to.Name = "tb_particular_three_pc_to"
        Me.tb_particular_three_pc_to.Size = New System.Drawing.Size(200, 18)
        Me.tb_particular_three_pc_to.TabIndex = 31
        Me.tb_particular_three_pc_to.Text = "Date"
        Me.tb_particular_three_pc_to.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'tb_particular_three_pc_from
        '
        Me.tb_particular_three_pc_from.BackColor = System.Drawing.Color.FromArgb(CType(CType(36, Byte), Integer), CType(CType(48, Byte), Integer), CType(CType(59, Byte), Integer))
        Me.tb_particular_three_pc_from.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.tb_particular_three_pc_from.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tb_particular_three_pc_from.ForeColor = System.Drawing.Color.White
        Me.tb_particular_three_pc_from.Location = New System.Drawing.Point(49, 75)
        Me.tb_particular_three_pc_from.Name = "tb_particular_three_pc_from"
        Me.tb_particular_three_pc_from.Size = New System.Drawing.Size(200, 18)
        Me.tb_particular_three_pc_from.TabIndex = 30
        Me.tb_particular_three_pc_from.Text = "Date"
        Me.tb_particular_three_pc_from.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'tb_particular_three
        '
        Me.tb_particular_three.BackColor = System.Drawing.Color.FromArgb(CType(CType(36, Byte), Integer), CType(CType(48, Byte), Integer), CType(CType(59, Byte), Integer))
        Me.tb_particular_three.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.tb_particular_three.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tb_particular_three.ForeColor = System.Drawing.Color.White
        Me.tb_particular_three.Location = New System.Drawing.Point(19, 26)
        Me.tb_particular_three.Margin = New System.Windows.Forms.Padding(5)
        Me.tb_particular_three.Name = "tb_particular_three"
        Me.tb_particular_three.Size = New System.Drawing.Size(223, 18)
        Me.tb_particular_three.TabIndex = 23
        Me.tb_particular_three.Text = "Particular"
        Me.tb_particular_three.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.Font = New System.Drawing.Font("Segoe UI", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label20.ForeColor = System.Drawing.Color.DarkGray
        Me.Label20.Location = New System.Drawing.Point(40, 55)
        Me.Label20.Margin = New System.Windows.Forms.Padding(5)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(85, 13)
        Me.Label20.TabIndex = 22
        Me.Label20.Text = "Period Covered"
        '
        'Label21
        '
        Me.Label21.AutoSize = True
        Me.Label21.Font = New System.Drawing.Font("Segoe UI", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label21.ForeColor = System.Drawing.Color.DarkGray
        Me.Label21.Location = New System.Drawing.Point(39, 11)
        Me.Label21.Margin = New System.Windows.Forms.Padding(5)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(58, 13)
        Me.Label21.TabIndex = 21
        Me.Label21.Text = "Particular"
        '
        'Label22
        '
        Me.Label22.AutoSize = True
        Me.Label22.Font = New System.Drawing.Font("Segoe UI", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label22.ForeColor = System.Drawing.Color.DarkGray
        Me.Label22.Location = New System.Drawing.Point(9, 78)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(33, 13)
        Me.Label22.TabIndex = 26
        Me.Label22.Text = "From"
        '
        'Label23
        '
        Me.Label23.AutoSize = True
        Me.Label23.Font = New System.Drawing.Font("Segoe UI", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label23.ForeColor = System.Drawing.Color.DarkGray
        Me.Label23.Location = New System.Drawing.Point(9, 100)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(19, 13)
        Me.Label23.TabIndex = 27
        Me.Label23.Text = "To"
        '
        'Panel3
        '
        Me.Panel3.BackColor = System.Drawing.Color.FromArgb(CType(CType(36, Byte), Integer), CType(CType(48, Byte), Integer), CType(CType(59, Byte), Integer))
        Me.Panel3.Controls.Add(Me.tb_particular_two_pc_to)
        Me.Panel3.Controls.Add(Me.tb_particular_two)
        Me.Panel3.Controls.Add(Me.tb_particular_two_pc_from)
        Me.Panel3.Controls.Add(Me.Label24)
        Me.Panel3.Controls.Add(Me.Label25)
        Me.Panel3.Controls.Add(Me.Label26)
        Me.Panel3.Controls.Add(Me.Label27)
        Me.Panel3.Location = New System.Drawing.Point(695, 158)
        Me.Panel3.Margin = New System.Windows.Forms.Padding(0)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(259, 122)
        Me.Panel3.TabIndex = 108
        '
        'tb_particular_two_pc_to
        '
        Me.tb_particular_two_pc_to.BackColor = System.Drawing.Color.FromArgb(CType(CType(36, Byte), Integer), CType(CType(48, Byte), Integer), CType(CType(59, Byte), Integer))
        Me.tb_particular_two_pc_to.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.tb_particular_two_pc_to.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tb_particular_two_pc_to.ForeColor = System.Drawing.Color.White
        Me.tb_particular_two_pc_to.Location = New System.Drawing.Point(48, 97)
        Me.tb_particular_two_pc_to.Name = "tb_particular_two_pc_to"
        Me.tb_particular_two_pc_to.Size = New System.Drawing.Size(200, 18)
        Me.tb_particular_two_pc_to.TabIndex = 31
        Me.tb_particular_two_pc_to.Text = "Date"
        Me.tb_particular_two_pc_to.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'tb_particular_two
        '
        Me.tb_particular_two.BackColor = System.Drawing.Color.FromArgb(CType(CType(36, Byte), Integer), CType(CType(48, Byte), Integer), CType(CType(59, Byte), Integer))
        Me.tb_particular_two.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.tb_particular_two.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tb_particular_two.ForeColor = System.Drawing.Color.White
        Me.tb_particular_two.Location = New System.Drawing.Point(19, 26)
        Me.tb_particular_two.Margin = New System.Windows.Forms.Padding(5)
        Me.tb_particular_two.Name = "tb_particular_two"
        Me.tb_particular_two.Size = New System.Drawing.Size(223, 18)
        Me.tb_particular_two.TabIndex = 23
        Me.tb_particular_two.Text = "Particular"
        Me.tb_particular_two.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'tb_particular_two_pc_from
        '
        Me.tb_particular_two_pc_from.BackColor = System.Drawing.Color.FromArgb(CType(CType(36, Byte), Integer), CType(CType(48, Byte), Integer), CType(CType(59, Byte), Integer))
        Me.tb_particular_two_pc_from.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.tb_particular_two_pc_from.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tb_particular_two_pc_from.ForeColor = System.Drawing.Color.White
        Me.tb_particular_two_pc_from.Location = New System.Drawing.Point(49, 75)
        Me.tb_particular_two_pc_from.Name = "tb_particular_two_pc_from"
        Me.tb_particular_two_pc_from.Size = New System.Drawing.Size(200, 18)
        Me.tb_particular_two_pc_from.TabIndex = 30
        Me.tb_particular_two_pc_from.Text = "Date"
        Me.tb_particular_two_pc_from.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label24
        '
        Me.Label24.AutoSize = True
        Me.Label24.Font = New System.Drawing.Font("Segoe UI", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label24.ForeColor = System.Drawing.Color.DarkGray
        Me.Label24.Location = New System.Drawing.Point(40, 55)
        Me.Label24.Margin = New System.Windows.Forms.Padding(5)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(85, 13)
        Me.Label24.TabIndex = 22
        Me.Label24.Text = "Period Covered"
        '
        'Label25
        '
        Me.Label25.AutoSize = True
        Me.Label25.Font = New System.Drawing.Font("Segoe UI", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label25.ForeColor = System.Drawing.Color.DarkGray
        Me.Label25.Location = New System.Drawing.Point(39, 11)
        Me.Label25.Margin = New System.Windows.Forms.Padding(5)
        Me.Label25.Name = "Label25"
        Me.Label25.Size = New System.Drawing.Size(58, 13)
        Me.Label25.TabIndex = 21
        Me.Label25.Text = "Particular"
        '
        'Label26
        '
        Me.Label26.AutoSize = True
        Me.Label26.Font = New System.Drawing.Font("Segoe UI", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label26.ForeColor = System.Drawing.Color.DarkGray
        Me.Label26.Location = New System.Drawing.Point(9, 78)
        Me.Label26.Name = "Label26"
        Me.Label26.Size = New System.Drawing.Size(33, 13)
        Me.Label26.TabIndex = 26
        Me.Label26.Text = "From"
        '
        'Label27
        '
        Me.Label27.AutoSize = True
        Me.Label27.Font = New System.Drawing.Font("Segoe UI", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label27.ForeColor = System.Drawing.Color.DarkGray
        Me.Label27.Location = New System.Drawing.Point(9, 100)
        Me.Label27.Name = "Label27"
        Me.Label27.Size = New System.Drawing.Size(19, 13)
        Me.Label27.TabIndex = 27
        Me.Label27.Text = "To"
        '
        'Panel12
        '
        Me.Panel12.BackColor = System.Drawing.Color.FromArgb(CType(CType(28, Byte), Integer), CType(CType(40, Byte), Integer), CType(CType(52, Byte), Integer))
        Me.Panel12.Controls.Add(Me.btn_attachment)
        Me.Panel12.Location = New System.Drawing.Point(12, 613)
        Me.Panel12.Name = "Panel12"
        Me.Panel12.Size = New System.Drawing.Size(132, 52)
        Me.Panel12.TabIndex = 109
        '
        'btn_attachment
        '
        Me.btn_attachment.BackColor = System.Drawing.Color.FromArgb(CType(CType(218, Byte), Integer), CType(CType(144, Byte), Integer), CType(CType(27, Byte), Integer))
        Me.btn_attachment.FlatAppearance.BorderSize = 0
        Me.btn_attachment.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_attachment.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_attachment.ForeColor = System.Drawing.Color.White
        Me.btn_attachment.Location = New System.Drawing.Point(10, 8)
        Me.btn_attachment.Name = "btn_attachment"
        Me.btn_attachment.Size = New System.Drawing.Size(110, 35)
        Me.btn_attachment.TabIndex = 104
        Me.btn_attachment.Text = "Attachment"
        Me.btn_attachment.UseVisualStyleBackColor = False
        '
        'btn_endorse
        '
        Me.btn_endorse.BackColor = System.Drawing.Color.FromArgb(CType(CType(218, Byte), Integer), CType(CType(144, Byte), Integer), CType(CType(27, Byte), Integer))
        Me.btn_endorse.FlatAppearance.BorderSize = 0
        Me.btn_endorse.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_endorse.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_endorse.ForeColor = System.Drawing.Color.White
        Me.btn_endorse.Location = New System.Drawing.Point(267, 7)
        Me.btn_endorse.Name = "btn_endorse"
        Me.btn_endorse.Size = New System.Drawing.Size(80, 35)
        Me.btn_endorse.TabIndex = 106
        Me.btn_endorse.Text = "Endorse"
        Me.btn_endorse.UseVisualStyleBackColor = False
        '
        'btn_print_as_menu
        '
        Me.btn_print_as_menu.BackColor = System.Drawing.Color.FromArgb(CType(CType(218, Byte), Integer), CType(CType(144, Byte), Integer), CType(CType(27, Byte), Integer))
        Me.btn_print_as_menu.FlatAppearance.BorderSize = 0
        Me.btn_print_as_menu.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_print_as_menu.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_print_as_menu.ForeColor = System.Drawing.Color.White
        Me.btn_print_as_menu.Location = New System.Drawing.Point(95, 7)
        Me.btn_print_as_menu.Name = "btn_print_as_menu"
        Me.btn_print_as_menu.Size = New System.Drawing.Size(80, 35)
        Me.btn_print_as_menu.TabIndex = 105
        Me.btn_print_as_menu.Text = "Print as"
        Me.btn_print_as_menu.UseVisualStyleBackColor = False
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.DarkGray
        Me.Label2.Location = New System.Drawing.Point(63, 43)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(40, 17)
        Me.Label2.TabIndex = 57
        Me.Label2.Text = "Payor"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.White
        Me.Label1.Location = New System.Drawing.Point(54, 9)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(34, 17)
        Me.Label1.TabIndex = 110
        Me.Label1.Text = "PHP"
        '
        'Panel4
        '
        Me.Panel4.BackColor = System.Drawing.Color.FromArgb(CType(CType(28, Byte), Integer), CType(CType(40, Byte), Integer), CType(CType(52, Byte), Integer))
        Me.Panel4.Controls.Add(Me.tb_total)
        Me.Panel4.Controls.Add(Me.Label1)
        Me.Panel4.Controls.Add(Me.Label16)
        Me.Panel4.Location = New System.Drawing.Point(1049, 589)
        Me.Panel4.Margin = New System.Windows.Forms.Padding(0)
        Me.Panel4.Name = "Panel4"
        Me.Panel4.Size = New System.Drawing.Size(292, 35)
        Me.Panel4.TabIndex = 111
        '
        'pb_approve_status
        '
        Me.pb_approve_status.Image = Global.NTC_ACCOUNTING.My.Resources.Resources.Unfriend_Male_36px
        Me.pb_approve_status.Location = New System.Drawing.Point(799, 643)
        Me.pb_approve_status.Name = "pb_approve_status"
        Me.pb_approve_status.Size = New System.Drawing.Size(36, 36)
        Me.pb_approve_status.TabIndex = 104
        Me.pb_approve_status.TabStop = False
        '
        'pnl_external
        '
        Me.pnl_external.BackColor = System.Drawing.Color.FromArgb(CType(CType(28, Byte), Integer), CType(CType(40, Byte), Integer), CType(CType(52, Byte), Integer))
        Me.pnl_external.Controls.Add(Me.btn_receipt)
        Me.pnl_external.Controls.Add(Me.btn_op)
        Me.pnl_external.Location = New System.Drawing.Point(161, 613)
        Me.pnl_external.Name = "pnl_external"
        Me.pnl_external.Size = New System.Drawing.Size(252, 52)
        Me.pnl_external.TabIndex = 112
        Me.pnl_external.Visible = False
        '
        'btn_receipt
        '
        Me.btn_receipt.BackColor = System.Drawing.Color.FromArgb(CType(CType(218, Byte), Integer), CType(CType(144, Byte), Integer), CType(CType(27, Byte), Integer))
        Me.btn_receipt.FlatAppearance.BorderSize = 0
        Me.btn_receipt.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_receipt.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_receipt.ForeColor = System.Drawing.Color.White
        Me.btn_receipt.Location = New System.Drawing.Point(131, 8)
        Me.btn_receipt.Name = "btn_receipt"
        Me.btn_receipt.Size = New System.Drawing.Size(110, 35)
        Me.btn_receipt.TabIndex = 98
        Me.btn_receipt.Text = "View receipt"
        Me.btn_receipt.UseVisualStyleBackColor = False
        Me.btn_receipt.Visible = False
        '
        'btn_op
        '
        Me.btn_op.BackColor = System.Drawing.Color.FromArgb(CType(CType(218, Byte), Integer), CType(CType(144, Byte), Integer), CType(CType(27, Byte), Integer))
        Me.btn_op.FlatAppearance.BorderSize = 0
        Me.btn_op.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_op.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_op.ForeColor = System.Drawing.Color.White
        Me.btn_op.Location = New System.Drawing.Point(11, 8)
        Me.btn_op.Name = "btn_op"
        Me.btn_op.Size = New System.Drawing.Size(110, 35)
        Me.btn_op.TabIndex = 97
        Me.btn_op.Text = "View OP"
        Me.btn_op.UseVisualStyleBackColor = False
        Me.btn_op.Visible = False
        '
        'tb_message
        '
        Me.tb_message.BackColor = System.Drawing.Color.FromArgb(CType(CType(44, Byte), Integer), CType(CType(62, Byte), Integer), CType(CType(80, Byte), Integer))
        Me.tb_message.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.tb_message.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tb_message.ForeColor = System.Drawing.Color.Red
        Me.tb_message.Location = New System.Drawing.Point(12, 190)
        Me.tb_message.MaxLength = 120
        Me.tb_message.Name = "tb_message"
        Me.tb_message.ReadOnly = True
        Me.tb_message.Size = New System.Drawing.Size(270, 22)
        Me.tb_message.TabIndex = 113
        Me.tb_message.TabStop = False
        Me.tb_message.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.tb_message.Visible = False
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.ForeColor = System.Drawing.Color.DarkGray
        Me.Label9.Location = New System.Drawing.Point(750, 97)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(142, 17)
        Me.Label9.TabIndex = 115
        Me.Label9.Text = "Date and Time Modified"
        '
        'tb_date_modified
        '
        Me.tb_date_modified.BackColor = System.Drawing.Color.FromArgb(CType(CType(44, Byte), Integer), CType(CType(62, Byte), Integer), CType(CType(80, Byte), Integer))
        Me.tb_date_modified.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.tb_date_modified.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tb_date_modified.ForeColor = System.Drawing.Color.White
        Me.tb_date_modified.Location = New System.Drawing.Point(913, 96)
        Me.tb_date_modified.Name = "tb_date_modified"
        Me.tb_date_modified.ReadOnly = True
        Me.tb_date_modified.Size = New System.Drawing.Size(402, 18)
        Me.tb_date_modified.TabIndex = 114
        Me.tb_date_modified.TabStop = False
        Me.tb_date_modified.Text = "yyyy-MM-dd"
        '
        'btn_reasses
        '
        Me.btn_reasses.BackColor = System.Drawing.Color.FromArgb(CType(CType(218, Byte), Integer), CType(CType(144, Byte), Integer), CType(CType(27, Byte), Integer))
        Me.btn_reasses.FlatAppearance.BorderSize = 0
        Me.btn_reasses.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_reasses.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_reasses.ForeColor = System.Drawing.Color.White
        Me.btn_reasses.Location = New System.Drawing.Point(293, 189)
        Me.btn_reasses.Name = "btn_reasses"
        Me.btn_reasses.Size = New System.Drawing.Size(75, 25)
        Me.btn_reasses.TabIndex = 116
        Me.btn_reasses.Text = "Re-Assess"
        Me.btn_reasses.UseVisualStyleBackColor = False
        Me.btn_reasses.Visible = False
        '
        'pnl_print
        '
        Me.pnl_print.Controls.Add(Me.btn_print_complete)
        Me.pnl_print.Controls.Add(Me.btn_print_assessment)
        Me.pnl_print.Controls.Add(Me.btn_print_endorse)
        Me.pnl_print.Location = New System.Drawing.Point(90, 271)
        Me.pnl_print.Name = "pnl_print"
        Me.pnl_print.Size = New System.Drawing.Size(122, 125)
        Me.pnl_print.TabIndex = 117
        Me.pnl_print.Visible = False
        '
        'btn_print_complete
        '
        Me.btn_print_complete.BackColor = System.Drawing.Color.FromArgb(CType(CType(218, Byte), Integer), CType(CType(144, Byte), Integer), CType(CType(27, Byte), Integer))
        Me.btn_print_complete.Enabled = False
        Me.btn_print_complete.FlatAppearance.BorderSize = 0
        Me.btn_print_complete.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_print_complete.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_print_complete.ForeColor = System.Drawing.Color.White
        Me.btn_print_complete.Location = New System.Drawing.Point(6, 84)
        Me.btn_print_complete.Name = "btn_print_complete"
        Me.btn_print_complete.Size = New System.Drawing.Size(110, 35)
        Me.btn_print_complete.TabIndex = 98
        Me.btn_print_complete.Text = "Endorsement w/ OR Stamp"
        Me.btn_print_complete.UseVisualStyleBackColor = False
        '
        'pnl_refresh
        '
        Me.pnl_refresh.BackColor = System.Drawing.Color.FromArgb(CType(CType(28, Byte), Integer), CType(CType(40, Byte), Integer), CType(CType(52, Byte), Integer))
        Me.pnl_refresh.Controls.Add(Me.btn_refresh)
        Me.pnl_refresh.Controls.Add(Me.btn_endorse)
        Me.pnl_refresh.Controls.Add(Me.btn_edit)
        Me.pnl_refresh.Controls.Add(Me.btn_print_as_menu)
        Me.pnl_refresh.Location = New System.Drawing.Point(12, 223)
        Me.pnl_refresh.Name = "pnl_refresh"
        Me.pnl_refresh.Size = New System.Drawing.Size(356, 48)
        Me.pnl_refresh.TabIndex = 119
        Me.pnl_refresh.Visible = False
        '
        'btn_refresh
        '
        Me.btn_refresh.BackColor = System.Drawing.Color.FromArgb(CType(CType(218, Byte), Integer), CType(CType(144, Byte), Integer), CType(CType(27, Byte), Integer))
        Me.btn_refresh.FlatAppearance.BorderSize = 0
        Me.btn_refresh.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_refresh.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_refresh.ForeColor = System.Drawing.Color.White
        Me.btn_refresh.Location = New System.Drawing.Point(9, 7)
        Me.btn_refresh.Name = "btn_refresh"
        Me.btn_refresh.Size = New System.Drawing.Size(80, 35)
        Me.btn_refresh.TabIndex = 120
        Me.btn_refresh.Text = "Refresh"
        Me.btn_refresh.UseVisualStyleBackColor = False
        '
        'frm_licenser_log
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(44, Byte), Integer), CType(CType(62, Byte), Integer), CType(CType(80, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(1350, 713)
        Me.Controls.Add(Me.pnl_refresh)
        Me.Controls.Add(Me.pnl_print)
        Me.Controls.Add(Me.btn_reasses)
        Me.Controls.Add(Me.Label9)
        Me.Controls.Add(Me.tb_date_modified)
        Me.Controls.Add(Me.dgv_soa_data)
        Me.Controls.Add(Me.tb_message)
        Me.Controls.Add(Me.pnl_external)
        Me.Controls.Add(Me.Panel4)
        Me.Controls.Add(Me.Panel12)
        Me.Controls.Add(Me.Panel3)
        Me.Controls.Add(Me.Panel2)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.pb_approve_status)
        Me.Controls.Add(Me.tb_approved)
        Me.Controls.Add(Me.tb_prepared_by)
        Me.Controls.Add(Me.btn_add_approve)
        Me.Controls.Add(Me.tb_payor)
        Me.Controls.Add(Me.Label14)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label15)
        Me.Controls.Add(Me.tb_expiration)
        Me.Controls.Add(Me.tb_serial_num)
        Me.Controls.Add(Me.tb_date_created)
        Me.Controls.Add(Me.tb_service_type)
        Me.Controls.Add(Me.tb_process_type)
        Me.Controls.Add(Me.tb_transact_type)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.RadTitleBar1)
        Me.ForeColor = System.Drawing.Color.White
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.MaximizeBox = False
        Me.MaximumSize = New System.Drawing.Size(1350, 713)
        Me.MinimizeBox = False
        Me.MinimumSize = New System.Drawing.Size(1350, 713)
        Me.Name = "frm_licenser_log"
        Me.ShowIcon = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "NTC Region 10"
        CType(Me.RadTitleBar1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgv_soa_data, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        Me.Panel3.ResumeLayout(False)
        Me.Panel3.PerformLayout()
        Me.Panel12.ResumeLayout(False)
        Me.Panel4.ResumeLayout(False)
        Me.Panel4.PerformLayout()
        CType(Me.pb_approve_status, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnl_external.ResumeLayout(False)
        Me.pnl_print.ResumeLayout(False)
        Me.pnl_refresh.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents RadTitleBar1 As Telerik.WinControls.UI.RadTitleBar
    Friend WithEvents Label8 As Label
    Friend WithEvents Label7 As Label
    Friend WithEvents dgv_soa_data As DataGridView
    Friend WithEvents tb_transact_type As TextBox
    Friend WithEvents tb_process_type As TextBox
    Friend WithEvents tb_service_type As TextBox
    Friend WithEvents tb_date_created As TextBox
    Friend WithEvents tb_serial_num As TextBox
    Friend WithEvents tb_expiration As TextBox
    Friend WithEvents Label15 As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents Label4 As Label
    Friend WithEvents Label5 As Label
    Friend WithEvents Label6 As Label
    Friend WithEvents Label14 As Label
    Friend WithEvents tb_payor As TextBox
    Friend WithEvents tb_total As TextBox
    Friend WithEvents Label16 As Label
    Friend WithEvents btn_print_assessment As Button
    Friend WithEvents btn_print_endorse As Button
    Friend WithEvents btn_edit As Button
    Friend WithEvents btn_add_approve As Button
    Friend WithEvents tb_prepared_by As TextBox
    Friend WithEvents tb_approved As TextBox
    Friend WithEvents pb_approve_status As PictureBox
    Friend WithEvents Panel1 As Panel
    Friend WithEvents tb_particular_one As TextBox
    Friend WithEvents Label17 As Label
    Friend WithEvents Label18 As Label
    Friend WithEvents Label13 As Label
    Friend WithEvents Label19 As Label
    Friend WithEvents Panel2 As Panel
    Friend WithEvents tb_particular_three As TextBox
    Friend WithEvents Label20 As Label
    Friend WithEvents Label21 As Label
    Friend WithEvents Label22 As Label
    Friend WithEvents Label23 As Label
    Friend WithEvents Panel3 As Panel
    Friend WithEvents tb_particular_two As TextBox
    Friend WithEvents Label24 As Label
    Friend WithEvents Label25 As Label
    Friend WithEvents Label26 As Label
    Friend WithEvents Label27 As Label
    Friend WithEvents tb_particular_one_pc_to As TextBox
    Friend WithEvents tb_particular_one_pc_from As TextBox
    Friend WithEvents tb_particular_three_pc_to As TextBox
    Friend WithEvents tb_particular_three_pc_from As TextBox
    Friend WithEvents tb_particular_two_pc_to As TextBox
    Friend WithEvents tb_particular_two_pc_from As TextBox
    Friend WithEvents Panel12 As Panel
    Friend WithEvents Label2 As Label
    Friend WithEvents Label1 As Label
    Friend WithEvents btn_attachment As Button
    Friend WithEvents Panel4 As Panel
    Friend WithEvents pnl_external As Panel
    Friend WithEvents btn_receipt As Button
    Friend WithEvents btn_op As Button
    Friend WithEvents tb_message As TextBox
    Friend WithEvents Label9 As Label
    Friend WithEvents tb_date_modified As TextBox
    Friend WithEvents btn_reasses As Button
    Friend WithEvents btn_endorse As Button
    Friend WithEvents btn_print_as_menu As Button
    Friend WithEvents pnl_print As Panel
    Friend WithEvents btn_print_complete As Button
    Friend WithEvents pnl_refresh As Panel
    Friend WithEvents btn_refresh As Button
End Class
