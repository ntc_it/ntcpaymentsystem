﻿Imports MySql.Data.MySqlClient
Imports System.Net

Public Class frm_review_soa
    Dim query, del_soa_id, del_process_id, del_service_id, del_soa_data_id, del_part_id(3),
        op_serial, op_date_created, starting_digit_op, serial, last_counter, attachment_id,
        processID, serviceID, particularID, soaDataID, process_type, service_type,
        mopCode(30), mopDesc(30), mopCat(30), mopID(30), op_long_date As String
    Dim cmd As MySqlCommand
    Dim reader As MySqlDataReader
    Dim soa_details_id, starting_series_op, op_override_number As Integer
    Dim process_type_counter As Integer = 0
    Dim service_type_counter As Integer = 0
    Dim particular_one_counter As Integer = 0
    Dim particular_two_counter As Integer = 0
    Dim particular_three_counter As Integer = 0
    Dim particular_id() As String = {"0", "0", "0"}
    Dim compute_sub_total, total As Double
    Dim is_first_transact As Boolean = False
    Dim is_op_existed As Boolean = False
    Dim op_override_activate_value = "0"
    Dim op_override_isActivated = False
    Dim mopInitials() As String = {"P/PUR", "AF/FF", "P/POS", "CONST", "RSL", "INS",
        "SUF", "FINES/PEN/SUR", "SUR(RSL)", "SUR(SUF)", "Permit Fees", "INS", "AF/FF",
        "FINES/PEN/SUR", "ARSL", "ROC", "AF/FF", "SEM", "FINES/PEN/SUR", "SUR(ARSL)",
        "SUR(ROC)", "REG", "SRF", "VER/AUTH", "EXAM FEE", "CERT.", "MOD", "MISC",
        "DST", "OTHERS"}
    Public transact_ID, notified_ID, message_type As Integer
    Public user_id, n_ID, purpose_one, purpose_two, authority_level As String

    Private Sub btn_attachment_Click(sender As Object, e As EventArgs) Handles btn_attachment.Click
        ' Open attachment form.
        frm_attachment.attachment_id = attachment_id
        frm_attachment.user_id = user_id
        frm_attachment.btn_attach.Visible = False
        frm_attachment.attachment_menu.Items.Item(1).Enabled = False
        frm_attachment.ShowDialog()
    End Sub

    Private Sub btn_action_two_Click(sender As Object, e As EventArgs) Handles _
        btn_action_two.Click
        ' Button for decline.
        Try
            Dim current_datetime = Date.Now.ToString("yyyy-MM-dd HH:mm:ss")

            If message_type = 1 Then
                ' For approver module.
                Dim title = "SOA declined by approver"
                Dim message = "SOA Serial No. " & tb_serial_num.Text & " has been " &
                    "disapproved. Click the button below To view."


                ' Update SOA approver ID to none. Zero is default number for unassigned.
                class_connection.con.Open()
                query = "update tbl_soa_transact Set user_id_approve = '0' where soa_id = '" &
                    transact_ID.ToString & "'"
                cmd = New MySqlCommand(query, class_connection.con)
                cmd.ExecuteNonQuery()
                class_connection.con.Close()

                ' Set notification to action taken.
                class_connection.con.Open()
                query = "update tbl_notifications Set action_taken = '1' where n_ID = '" + n_ID + "'"
                cmd = New MySqlCommand(query, class_connection.con)
                cmd.ExecuteNonQuery()
                class_connection.con.Close()

                ' Create notification to SOA preparator.
                class_connection.con.Open()
                query = "insert into tbl_notifications(message_title, message_content, transaction_id, " &
                    "user_id_notified,user_id_notifier,user_opened,date_created,action_taken,message_type) " &
                    "Values('" & title & "','" & message & "','" & transact_ID.ToString & "','" &
                    notified_ID.ToString & "','" & user_id & "','0','" &
                    current_datetime & "','0','3')"
                cmd = New MySqlCommand(query, class_connection.con)
                cmd.ExecuteNonQuery()
                class_connection.con.Close()

                MsgBox("You selected DISAPPROVE. This SOA Review will now close.", MsgBoxStyle.Information,
                    "NTC Region 10")

                ' Reload notification form.
                dashboard.uc_notifications.load_notifications()
                dashboard.uc_notifications.retrieve_number_of_unresolved()

                Close()
            ElseIf message_type = 7 Then
                ' For accountant module.
                Dim title = "SOA declined by accountant"
                Dim message = "Order of payment creation of SOA Serial No. " & tb_serial_num.Text &
                    " has been declined. Click the button below to view."

                ' Set notification to action taken.
                class_connection.con.Open()
                query = "update tbl_notifications Set action_taken = '1' where n_ID = '" & n_ID & "'"
                cmd = New MySqlCommand(query, class_connection.con)
                cmd.ExecuteNonQuery()
                class_connection.con.Close()

                ' Set SOA approver ID, is endorsed and user approve to none. Zero is the default number 
                ' for unassigned.
                class_connection.con.Open()
                query = "update tbl_soa_transact Set is_endorse = '0', user_id_approve = '0', " &
                    "user_approve = '0' where soa_id = '" & transact_ID.ToString & "'"
                cmd = New MySqlCommand(query, class_connection.con)
                cmd.ExecuteNonQuery()
                class_connection.con.Close()

                ' Create notification to SOA preparator.
                class_connection.con.Open()
                query = "insert into tbl_notifications(message_title, message_content, transaction_id, " &
                    "user_id_notified, user_id_notifier, user_opened, date_created, action_taken, " &
                    "message_type) Values('" & title & "', '" & message & "', '" &
                    transact_ID.ToString & "','" & notified_ID.ToString & "','" & user_id & "', '0', '" &
                    current_datetime & "', '0', '10')"
                cmd = New MySqlCommand(query, class_connection.con)
                cmd.ExecuteNonQuery()
                class_connection.con.Close()

                ' Delete order of payment entry.
                class_connection.con.Open()
                query = "delete from tbl_soa_endorse where soa_id = '" & transact_ID.ToString & "'"
                cmd = New MySqlCommand(query, class_connection.con)
                cmd.ExecuteNonQuery()
                class_connection.con.Close()

                MsgBox("You selected DECLINE. Preparator has been notified!", MsgBoxStyle.Information,
                    "NTC Region 10")

                ' Reload notification form.
                dashboard.uc_notifications.load_notifications()
                dashboard.uc_notifications.retrieve_number_of_unresolved()

                Close()
            End If
        Catch ex As MySqlException
            class_connection.con.Close()
            MsgBox("Couldn't connect to server. Contact the IT Administrator for assistance.",
                MsgBoxStyle.Exclamation, "NTC Region 10")
        Catch ex As Exception
            class_connection.con.Close()
            MsgBox("Something went wrong. Couldn't process the data.", MsgBoxStyle.Exclamation,
                "NTC Region 10")
        End Try
    End Sub

    Sub load_MOP_to_arrays()
        Try
            ' Load all MOP to an array for comparing values and assigning initials.
            Dim mopCounter As Integer = 0

            class_connection.con.Open()
            query = "select code, description, cat, mop_id from tbl_mode_of_payment"
            cmd = New MySqlCommand(query, class_connection.con)
            reader = cmd.ExecuteReader()

            While reader.Read()
                mopCode(mopCounter) = reader.GetString("code")
                mopDesc(mopCounter) = WebUtility.HtmlDecode(reader.GetString("description"))
                mopCat(mopCounter) = reader.GetString("cat")
                mopID(mopCounter) = reader.GetString("mop_id")
                mopCounter += 1
            End While
            class_connection.con.Close()
        Catch ex As MySqlException
            class_connection.con.Close()
            MsgBox("Couldn't connect to server. Contact the IT Administrator for assistance.",
                MsgBoxStyle.Exclamation, "NTC Region 10")
        Catch ex As Exception
            class_connection.con.Close()
            MsgBox("Something went wrong. Couldn't process the data.", MsgBoxStyle.Exclamation,
                "NTC Region 10")
        End Try
    End Sub

    Private Sub Form_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Icon = My.Resources.ntc_material_logo
        load_MOP_to_arrays()

        load_SOA_details()

        ' Retrieve approver name.
        class_connection.con.Open()
        query = "select full_name from tbl_soa_transact join tbl_user on " &
            "tbl_user.user_id = tbl_soa_transact.user_id_approve where soa_id = '" &
            transact_ID.ToString & "'"
        cmd = New MySqlCommand(query, class_connection.con)
        reader = cmd.ExecuteReader()

        If reader.Read Then
            tb_approved.Text = WebUtility.HtmlDecode(reader.GetString("full_name"))
        Else
            tb_approved.Text = "************  *. ********"
        End If
        class_connection.con.Close()
    End Sub

    Private Sub btn_approve_Click(sender As Object, e As EventArgs) Handles btn_action_one.Click
        ' Button for approve.
        Dim current_datetime = Date.Now.ToString("yyyy-MM-dd HH:mm:ss")

        If message_type = 1 Then
            Try
                ' For approver module.
                Dim title = "SOA approved"
                Dim message = "SOA Serial No. " & tb_serial_num.Text & " has been approved. " &
                    "Click the button below to view."

                ' Set SOA approval status to approved.
                class_connection.con.Open()
                query = "update tbl_soa_transact Set user_approve = '1' where soa_id = '" &
                    transact_ID.ToString & "'"
                cmd = New MySqlCommand(query, class_connection.con)
                cmd.ExecuteNonQuery()
                class_connection.con.Close()

                ' Set notification to action taken.
                class_connection.con.Open()
                query = "update tbl_notifications Set action_taken = '1' where n_ID = '" &
                    n_ID & "'"
                cmd = New MySqlCommand(query, class_connection.con)
                cmd.ExecuteNonQuery()
                class_connection.con.Close()

                ' Create notification to SOA preparator.
                class_connection.con.Open()
                query = "insert into tbl_notifications(message_title, message_content, transaction_id, " &
                    "user_id_notified, user_id_notifier, user_opened, date_created, action_taken, " &
                    "message_type) Values('" & title & "', '" & message & "', '" & transact_ID.ToString &
                    "', '" & notified_ID.ToString & "', '" & user_id & "', '0', '" & current_datetime &
                    "', '0', '2')"
                cmd = New MySqlCommand(query, class_connection.con)
                cmd.ExecuteNonQuery()
                class_connection.con.Close()

                MsgBox("You selected APPROVE. This SOA review will now close.", MsgBoxStyle.Information,
                    "NTC Region 10")

                ' Reload notification form.
                dashboard.uc_notifications.load_notifications()
                dashboard.uc_notifications.retrieve_number_of_unresolved()

                Close()
            Catch ex As MySqlException
                class_connection.con.Close()
                MsgBox("Couldn't connect to server. Contact the IT Administrator for assistance.",
                    MsgBoxStyle.Exclamation, "NTC Region 10")
            Catch ex As Exception
                class_connection.con.Close()
                MsgBox("Something went wrong. Couldn't process the data.", MsgBoxStyle.Exclamation,
                    "NTC Region 10")
            End Try

        ElseIf message_type = 7 Then
            ' For accountant module.
            Dim minor_total As Double = 0.00
            Dim dst_value As Double = 0.00

            If Not is_OP_exist() Then
                createOP()
            End If

            convertDescriptionsToInitials()

            ' Computation of taxes and fees.
            For i = 0 To dgv_soa_data.Rows.Count - 1 Step +1
                If dgv_soa_data.Rows(i).Cells(16).FormattedValue <> "29" Then
                    minor_total += Double.Parse(dgv_soa_data.Rows(i).Cells(15).FormattedValue)
                Else
                    dst_value = dgv_soa_data.Rows(i).Cells(15).FormattedValue
                End If
            Next

            Try
                ' Retrieve OP serial and date creation.
                class_connection.con.Open()
                query = "select op_serial_num, op_date_created from tbl_soa_endorse where soa_id = '" &
                    transact_ID.ToString & "'"
                cmd = New MySqlCommand(query, class_connection.con)
                reader = cmd.ExecuteReader()

                If reader.Read Then
                    op_serial = reader.GetString("op_serial_num")
                    op_date_created = reader.GetDateTime("op_date_created").ToString("MM-dd-yyyy")
                    op_long_date = reader.GetDateTime("op_date_created").ToLongDateString
                End If
                class_connection.con.Close()

                ' Open accountant form.
                frm_accountant.longDate = op_long_date
                frm_accountant.tb_date_created.Text = op_date_created
                frm_accountant.tb_serial_num.Text = op_serial
                frm_accountant.user_id = user_id
                frm_accountant.tb_purpose_one.Text = purpose_one
                frm_accountant.tb_purpose_two.Text = purpose_two
                frm_accountant.transact_ID = transact_ID
                frm_accountant.authority_level = authority_level
                frm_accountant.n_ID = n_ID       'notification ID. Do not remove.
                frm_accountant.tb_payment.Text = FormatNumber(CDbl(tb_total.Text), 2)
                frm_accountant.tb_bank_payment.Text = FormatNumber(CDbl(minor_total), 2)
                frm_accountant.tb_bank_payment_two.Text = FormatNumber(CDbl(dst_value), 2)
                frm_accountant.tb_bank_total.Text = FormatNumber(CDbl(tb_total.Text), 2)
                frm_accountant.Show()

                Close()
            Catch ex As MySqlException
                class_connection.con.Close()
                MsgBox("Couldn't connect to server. Contact the IT Administrator for assistance.",
                    MsgBoxStyle.Exclamation, "NTC Region 10")
            Catch ex As Exception
                class_connection.con.Close()
                MsgBox("Something went wrong. Couldn't process the data.", MsgBoxStyle.Exclamation,
                    "NTC Region 10")
            End Try
        End If
    End Sub

    Sub convertDescriptionsToInitials()
        Try
            If dgv_soa_data.Rows.Count <= 5 Then
                ' If SOA data entries has 5 or less.
                Dim limit As Integer = dgv_soa_data.Rows.Count

                For i = 0 To limit - 1
                    Dim paymentCustomValue As String = ""

                    ' Retrieve custom value from function.
                    paymentCustomValue =
                        retrieve_custom_value(dgv_soa_data.Rows(i).Cells(16).FormattedValue.ToString)

                    For mopCounter = 0 To 29
                        If dgv_soa_data.Rows(i).Cells(0).FormattedValue = mopCode(mopCounter) And
                            dgv_soa_data.Rows(i).Cells(16).FormattedValue = mopID(mopCounter) And
                            dgv_soa_data.Rows(i).Cells(2).FormattedValue = mopCat(mopCounter) Then
                            If paymentCustomValue <> "" Then
                                ' If it does have custom value.
                                purpose_one += mopInitials(mopCounter) + ": " + paymentCustomValue
                            Else
                                ' If it doesn't have custom value.
                                purpose_one += mopInitials(mopCounter)
                            End If
                        End If
                    Next

                    If i <> dgv_soa_data.Rows.Count - 1 Then
                        ' Add comma every after initials.
                        purpose_one += ", "
                    End If
                Next
            Else
                ' If SOA data entries has more than 5.

                ' Serves as maximum number of initials to create for part 1 of 
                ' list of SOA data entries.
                Dim limit As Integer = 5

                ' Part 1 of 2 of list of SOA data entries.
                For i = 0 To limit - 1
                    Dim paymentCustomValue As String = ""

                    ' Retrieve custom value from function.
                    paymentCustomValue =
                        retrieve_custom_value(dgv_soa_data.Rows(i).Cells(16).FormattedValue.ToString)

                    For mopCounter = 0 To 29
                        If dgv_soa_data.Rows(i).Cells(0).FormattedValue = mopCode(mopCounter) And
                            dgv_soa_data.Rows(i).Cells(16).FormattedValue = mopID(mopCounter) And
                            dgv_soa_data.Rows(i).Cells(2).FormattedValue = mopCat(mopCounter) Then
                            If paymentCustomValue <> "" Then
                                ' If it does have custom value.
                                purpose_one += mopInitials(mopCounter) + ": " + paymentCustomValue
                            Else
                                ' If it doesn't have custom value.
                                purpose_one += mopInitials(mopCounter)
                            End If
                        End If
                    Next

                    If i <> dgv_soa_data.Rows.Count - 1 Then
                        ' Add comma every after initials.
                        purpose_one += ", "
                    End If
                Next

                ' Part 2 of 2 of list of SOA data entries.
                For i = 5 To dgv_soa_data.Rows.Count - 1
                    Dim paymentCustomValue As String = ""

                    ' Retrieve custom value from function.
                    paymentCustomValue =
                        retrieve_custom_value(dgv_soa_data.Rows(i).Cells(16).FormattedValue.ToString)

                    For mopCounter = 0 To 29
                        If dgv_soa_data.Rows(i).Cells(0).FormattedValue = mopCode(mopCounter) And
                            dgv_soa_data.Rows(i).Cells(16).FormattedValue = mopID(mopCounter) And
                            dgv_soa_data.Rows(i).Cells(2).FormattedValue = mopCat(mopCounter) Then
                            If paymentCustomValue <> "" Then
                                ' If it does have custom value.
                                purpose_two += mopInitials(mopCounter) + ": " + paymentCustomValue
                            Else
                                ' If it doesn't have custom value.
                                purpose_two += mopInitials(mopCounter)
                            End If
                        End If
                    Next

                    If i <> dgv_soa_data.Rows.Count - 1 Then
                        ' Add comma every after initials.
                        purpose_two += ", "
                    End If
                Next
            End If
        Catch ex As Exception
            MsgBox("Something went wrong. Couldn't process the data.", MsgBoxStyle.Exclamation,
                "NTC Region 10")
        End Try
    End Sub

    Function retrieve_custom_value(row_value)
        ' Retrieve custom values from SOA's MOP.
        Try
            Dim custom_value = ""

            class_connection.con.Open()
            query = "select others_custom_value from tbl_soa_data where soa_data_id = '" & soaDataID &
                "' and mop_id = '" & row_value & "'"
            cmd = New MySqlCommand(query, class_connection.con)
            reader = cmd.ExecuteReader()

            If reader.Read Then
                custom_value = WebUtility.HtmlDecode(reader.GetString("others_custom_value"))
            End If
            class_connection.con.Close()

            Return custom_value
        Catch ex As MySqlException
            class_connection.con.Close()
            MsgBox("Couldn't connect to server. Contact the IT Administrator for assistance.",
                MsgBoxStyle.Exclamation, "NTC Region 10")
            Return Nothing
        Catch ex As Exception
            class_connection.con.Close()
            MsgBox("Something went wrong. Couldn't process the data.", MsgBoxStyle.Exclamation,
            "NTC Region 10")
            Return Nothing
        End Try
    End Function

    Function is_OP_exist()
        ' Check if OP exist.
        Dim is_op_existed = False

        class_connection.con.Open()
        query = "select endorse_id from tbl_soa_endorse where soa_id = '" & transact_ID.ToString &
            "'"
        cmd = New MySqlCommand(query, class_connection.con)
        reader = cmd.ExecuteReader()

        If reader.Read Then
            is_op_existed = True
        End If

        class_connection.con.Close()

        Return is_op_existed
    End Function

    Sub createOP()
        Dim current_year = Date.Now.ToString("yyyy")
        Dim current_month = Date.Now.ToString("MM")
        Dim current_datetime = Date.Now.ToString("yyyy-MM-dd HH:mm:ss")

        ' Retrieve Order of Payment starting digit.
        class_connection.con.Open()
        query = "select setting_value from tbl_settings where setting = 'starting_digit_op'"
        cmd = New MySqlCommand(query, class_connection.con)
        reader = cmd.ExecuteReader()

        If reader.Read Then
            starting_digit_op = reader.GetString("setting_value")
        End If
        class_connection.con.Close()

        ' Retrieve setting override OP serial.
        class_connection.con.Open()
        query = "select setting_value, activate from tbl_settings where setting = " &
            "'override_op_series'"
        cmd = New MySqlCommand(query, class_connection.con)
        reader = cmd.ExecuteReader()

        If reader.Read Then
            op_override_number = reader.GetInt32("setting_value")

            If reader.GetString("activate") = "1" Then
                op_override_activate_value = "1"
            End If
        End If
        class_connection.con.Close()

        ' Retrieve OP starting series. For first live production only.
        class_connection.con.Open()
        query = "select setting_value from tbl_settings where setting = 'starting_series_op'"
        cmd = New MySqlCommand(query, class_connection.con)
        reader = cmd.ExecuteReader()

        If reader.Read Then
            starting_series_op = reader.GetInt32("setting_value")
        End If
        class_connection.con.Close()

        ' Retrieve last OP counter for OP creation.
        class_connection.con.Open()
        query = "select endorse_id, e_counter, op_date_created, op_month, op_year from " &
            "tbl_soa_endorse order by op_year desc, op_month desc, e_counter desc limit 1"
        cmd = New MySqlCommand(query, class_connection.con)
        reader = cmd.ExecuteReader()

        If reader.Read Then
            ' If there is any entry.
            If current_year = reader.GetDateTime("op_date_created").ToString("yyyy") Then
                ' Execute statement if current year is the same with the latest entry's year
                ' Sample output is 63-2019-10-0254.
                serial = starting_digit_op & "-" & current_year & "-" & current_month & "-" &
                    (reader.GetInt32("e_counter") + 1).ToString("D4")
                last_counter = reader.GetString("e_counter")

                ' Use custom serial if OP override is activated.
                If op_override_activate_value = "1" Then
                    serial = starting_digit_op & "-" & current_year & "-" & current_month & "-" &
                        op_override_number.ToString("D4")
                    last_counter = op_override_number - 1
                    op_override_isActivated = True
                End If
            Else
                ' Execute statement if current year is not the same with the latest entry's year
                ' Sample output is 63-2019-10-0001.
                serial = starting_digit_op & "-" & current_year & "-" & current_month & "-0001"
                last_counter = 0

                ' Use custom serial if OP override is activated.
                If op_override_activate_value = "1" Then
                    serial = starting_digit_op & "-" & current_year & "-" & current_month & "-" &
                        op_override_number.ToString("D4")
                    last_counter = op_override_number - 1
                    op_override_isActivated = True
                End If
            End If
        Else
            ' If there is no entry.
            ' If this is the first live production.
            serial = starting_digit_op & "-" & current_year & "-" & current_month & "-" &
                starting_series_op.ToString("D4")
            is_first_transact = True
        End If
        class_connection.con.Close()

        If is_first_transact Then
            ' For first live production only.
            ' Create Order of Payment entry.
            class_connection.con.Open()
            query = "insert into tbl_soa_endorse(soa_id, endorse_officer_id, is_updated, e_counter, " &
                "op_month, op_year, op_serial_num, op_date_created) values('" & transact_ID.ToString &
                "', '" & user_id & "', '0', '" & starting_series_op.ToString & "', '" & current_month &
                "', '" & current_year & "', '" + serial + "', '" & current_datetime & "')"
            cmd = New MySqlCommand(query, class_connection.con)
            cmd.ExecuteNonQuery()
            class_connection.con.Close()
        Else
            ' Create Order of Payment entry.
            Dim newCounter As Integer = Integer.Parse(last_counter) + 1
            class_connection.con.Open()
            query = "insert into tbl_soa_endorse(soa_id, endorse_officer_id, is_updated, e_counter, " &
                "op_month, op_year, op_serial_num, op_date_created) values('" & transact_ID.ToString &
                "', '" & user_id & "', '0', '" & newCounter.ToString & "', '" & current_month &
                "', '" & current_year & "','" & serial & "','" & current_datetime & "')"
            cmd = New MySqlCommand(query, class_connection.con)
            cmd.ExecuteNonQuery()
            class_connection.con.Close()
        End If

        ' Deactivate override after usage.
        If op_override_isActivated Then
            deactivate_op_override()
        End If
    End Sub

    Sub deactivate_op_override()
        ' Update override soa to inactive.
        class_connection.con.Open()
        query = "update tbl_settings set activate = '0' where setting = 'override_op_series'"
        cmd = New MySqlCommand(query, class_connection.con)
        cmd.ExecuteNonQuery()
        class_connection.con.Close()

        op_override_isActivated = False
        op_override_activate_value = "0"
    End Sub

    Sub load_SOA_details()
        Try
            ' Retrieve SOA details and data entries.
            class_connection.con.Open()
            query = "select * from tbl_soa_transact join tbl_soa_details on tbl_soa_details.soa_details_id " &
                "= tbl_soa_transact.soa_details_id join tbl_user on tbl_user.user_id = " &
                "tbl_soa_transact.user_id where tbl_soa_transact.soa_id = '" & transact_ID.ToString & "'"
            cmd = New MySqlCommand(query, class_connection.con)
            reader = cmd.ExecuteReader()

            If reader.Read Then
                tb_transact_type.Text = reader.GetString("application_type")
                tb_date_created.Text = reader.GetDateTime("date_created").ToLongDateString & " " &
                    reader.GetDateTime("date_created").ToString("hh:mm:ss tt")
                tb_serial_num.Text = reader.GetString("serial_id")
                tb_expiration.Text = reader.GetDateTime("date_expires").ToLongDateString
                tb_date_modified.Text = reader.GetDateTime("date_modified").ToLongDateString & " " &
                    reader.GetDateTime("date_modified").ToString("hh:mm:ss tt")
                tb_prepared_by.Text = WebUtility.HtmlDecode(reader.GetString("full_name"))
                soa_details_id = reader.GetInt32("soa_details_id")
                attachment_id = reader.GetString("attachment_id")
                processID = reader.GetString("process_id")
                serviceID = reader.GetString("service_id")
                particularID = reader.GetString("particular_id")
                soaDataID = reader.GetString("soa_data_id")
            End If
            class_connection.con.Close()

            ' Retrieve payor's name. 
            class_connection.con.Open()
            query = "select name from tbl_soa_transact join tbl_soa_details on tbl_soa_details.soa_details_id" &
                " = tbl_soa_transact.soa_details_id join tbl_payor on tbl_payor.payor_id = " &
                "tbl_soa_details.payor_id where tbl_soa_transact.soa_id = '" & transact_ID.ToString & "'"
            cmd = New MySqlCommand(query, class_connection.con)
            reader = cmd.ExecuteReader()

            If reader.Read Then
                tb_payor.Text = WebUtility.HtmlDecode(reader.GetString("name"))
            End If
            class_connection.con.Close()

            ' Retrieve process type.
            class_connection.con.Open()
            query = "select process_name from tbl_process_soa where process_type_id = '" & processID & "'"
            cmd = New MySqlCommand(query, class_connection.con)
            reader = cmd.ExecuteReader()

            While reader.Read
                If process_type_counter > 0 Then
                    ' Add comma every after process.
                    process_type += ", "
                End If

                ' Concat process to process type and increment process counter by 1.
                process_type += reader.GetString("process_name")
                process_type_counter += 1
            End While
            class_connection.con.Close()

            ' Display process type after retrieving.
            tb_process_type.Text = process_type

            ' Retrieve service type.
            class_connection.con.Open()
            query = "select service_name, custom_value from tbl_service_soa where service_type_id = '" &
                serviceID & "'"
            cmd = New MySqlCommand(query, class_connection.con)
            reader = cmd.ExecuteReader()

            While reader.Read
                If service_type_counter > 0 Then
                    ' Add comma every after service.
                    service_type += ", "
                End If

                If reader.GetString("service_name") <> "OTHERS" Then
                    ' Service type is not "OTHERS".
                    service_type += reader.GetString("service_name")
                Else
                    ' Service type is "OTHERS" then also retrieve custom value.
                    service_type += reader.GetString("service_name") + ": " &
                        WebUtility.HtmlDecode(reader.GetString("custom_value"))
                End If

                ' Increment service counter by 1.
                service_type_counter += 1
            End While
            class_connection.con.Close()

            ' Display service type after retrieving.
            tb_service_type.Text = service_type

            retrieve_number_of_attachments()

            initialize_table()

            ' Set all particulars' details to N/A.
            tb_particular_one.Text = "N/A"
            tb_particular_one_pc_from.Text = "N/A"
            tb_particular_one_pc_to.Text = "N/A"
            tb_particular_two.Text = "N/A"
            tb_particular_two_pc_from.Text = "N/A"
            tb_particular_two_pc_to.Text = "N/A"
            tb_particular_three.Text = "N/A"
            tb_particular_three_pc_from.Text = "N/A"
            tb_particular_three_pc_to.Text = "N/A"

            ' Retrieve particular details.
            class_connection.con.Open()
            query = "select part_name, part_pos, part_covered_from, part_covered_to from tbl_particular " &
                "where part_id = '" & particularID & "'"
            cmd = New MySqlCommand(query, class_connection.con)
            reader = cmd.ExecuteReader()

            While reader.Read()
                If reader.GetString("part_pos") = "1" Then
                    tb_particular_one.Text = WebUtility.HtmlDecode(reader.GetString("part_name"))
                    tb_particular_one_pc_from.Text = reader.GetMySqlDateTime("part_covered_from")
                    tb_particular_one_pc_to.Text = reader.GetMySqlDateTime("part_covered_to")
                End If

                If reader.GetString("part_pos") = "2" Then
                    tb_particular_two.Text = WebUtility.HtmlDecode(reader.GetString("part_name"))
                    tb_particular_two_pc_from.Text = reader.GetMySqlDateTime("part_covered_from")
                    tb_particular_two_pc_to.Text = reader.GetMySqlDateTime("part_covered_to")
                End If

                If reader.GetString("part_pos") = "3" Then
                    tb_particular_three.Text = WebUtility.HtmlDecode(reader.GetString("part_name"))
                    tb_particular_three_pc_from.Text = reader.GetMySqlDateTime("part_covered_from")
                    tb_particular_three_pc_to.Text = reader.GetMySqlDateTime("part_covered_to")
                End If

            End While
            class_connection.con.Close()

            ' Display SOA data entries.
            class_connection.con.Open()
            query = "select tbl_soa_data.mop_id, code as 'Code', description as 'Description', cat " &
                "'Category',  others_custom_value, p1_no_of_years as 'No. of Years 1', p1_percent as " &
                "'Percent 1', p1_no_of_units as 'No. of Units 1', p1_fees as 'Fees 1', p2_no_of_years as " &
                "'No. of Years 2', p2_percent as 'Percent 2', p2_no_of_units as 'No. of Units 2', " &
                "p2_fees as 'Fees 2', p3_no_of_years as 'No. of Years 3', p3_percent as 'Percent 3', " &
                "p3_no_of_units as 'No. of Units 3', p3_fees as 'Fees 3' from tbl_soa_transact join " &
                "tbl_soa_details on tbl_soa_details.soa_details_id = tbl_soa_transact.soa_details_id " &
                "join tbl_soa_data on tbl_soa_data.soa_data_id = tbl_soa_details.soa_data_id join " &
                "tbl_mode_of_payment on tbl_mode_of_payment.mop_id = tbl_soa_data.mop_id  where " &
                "tbl_soa_transact.soa_id = '" & transact_ID.ToString & "' order by tbl_mode_of_payment.mop_id"
            cmd = New MySqlCommand(query, class_connection.con)
            reader = cmd.ExecuteReader()

            While reader.Read
                Dim value As Double = reader.GetDouble("Percent 1")
                Dim value2 As Double = reader.GetDouble("Percent 2")
                Dim value3 As Double = reader.GetDouble("Percent 3")
                Dim num, num2, num3 As Double
                Dim percentage, percentage2, percentage3 As Double
                Dim row() As String
                Dim description = WebUtility.HtmlDecode(reader.GetString("Description"))
                Dim custom_value = WebUtility.HtmlDecode(reader.GetString("others_custom_value"))
                Dim entry_code = reader.GetString("Code")
                Dim category = reader.GetString("Category")

                ' Convert percentage to double.
                If Double.TryParse(value, num) And Double.TryParse(value2, num2) And
                    Double.TryParse(value3, num3) Then
                    percentage = num / 100
                    percentage2 = num2 / 100
                    percentage3 = num3 / 100
                End If

                ' Compute sub-total of each SOA data entries.
                compute_sub_total = (reader.GetDouble("No. of Years 1") * percentage *
                    reader.GetDouble("No. of Units 1") * reader.GetDouble("Fees 1")) +
                    (reader.GetDouble("No. of Years 2") * percentage2 * reader.GetDouble("No. of Units 2") *
                    reader.GetDouble("Fees 2")) + (reader.GetDouble("No. of Years 3") * percentage3 *
                    reader.GetDouble("No. of Units 3") * reader.GetDouble("Fees 3"))

                If reader.GetString("others_custom_value") = "" Then
                    ' If it doesn't have custom value.
                    row = {entry_code, description, category, reader.GetString("No. of Years 1"),
                        reader.GetString("Percent 1"), reader.GetString("No. of Units 1"),
                        FormatNumber(CDbl(reader.GetString("Fees 1")), 2), reader.GetString("No. of Years 2"),
                        reader.GetString("Percent 2"), reader.GetString("No. of Units 2"),
                        FormatNumber(CDbl(reader.GetString("Fees 2")), 2), reader.GetString("No. of Years 3"),
                        reader.GetString("Percent 3"), reader.GetString("No. of Units 3"),
                        FormatNumber(CDbl(reader.GetString("Fees 3")), 2), FormatNumber(CDbl(compute_sub_total), 2),
                        reader.GetString("mop_id")}
                Else
                    ' If it does have custom value.
                    row = {entry_code, (description & ": " & custom_value), category,
                        reader.GetString("No. of Years 1"), reader.GetString("Percent 1"),
                        reader.GetString("No. of Units 1"), FormatNumber(CDbl(reader.GetString("Fees 1")), 2),
                        reader.GetString("No. of Years 2"), reader.GetString("Percent 2"),
                        reader.GetString("No. of Units 2"), FormatNumber(CDbl(reader.GetString("Fees 2")), 2),
                        reader.GetString("No. of Years 3"), reader.GetString("Percent 3"),
                        reader.GetString("No. of Units 3"), FormatNumber(CDbl(reader.GetString("Fees 3")), 2),
                        FormatNumber(CDbl(compute_sub_total), 2), reader.GetString("mop_id")}
                End If

                ' Add row to datagridview.
                dgv_soa_data.Rows.Add(row)
            End While
            class_connection.con.Close()

            ' Set all columns unsortable.
            For Each column As DataGridViewColumn In dgv_soa_data.Columns
                column.SortMode = DataGridViewColumnSortMode.NotSortable
            Next

            ' Set total to zero before computations.
            total = 0

            ' Compute all SOA data entries' sub-total to make total.
            For i = 0 To dgv_soa_data.Rows.Count - 1 Step +1
                Try
                    total += dgv_soa_data.Rows(i).Cells("Sub-total").Value
                Catch ex As Exception
                    total += 0
                End Try
            Next

            ' Display total.
            tb_total.Text = FormatNumber(CDbl(total), 2)
        Catch ex As MySqlException
            class_connection.con.Close()
            MsgBox("Couldn't connect to server. Contact the IT Administrator for assistance.",
                MsgBoxStyle.Exclamation, "NTC Region 10")
            Close()
        Catch ex As Exception
            class_connection.con.Close()
            MsgBox("Something went wrong. Couldn't process the data.", MsgBoxStyle.Exclamation,
            "NTC Region 10")
            Close()
        End Try
    End Sub

    Sub retrieve_number_of_attachments()
        ' Retrieve number of attachments available.
        class_connection.con.Open()
        query = "select count(*) as 'number_attach' from tbl_attachment where attachment_id = '" &
            attachment_id & "'"
        cmd = New MySqlCommand(query, class_connection.con)
        reader = cmd.ExecuteReader()

        If reader.Read Then
            If reader.GetInt32("number_attach") = 1 Then
                btn_attachment.Text = reader.GetInt32("number_attach").ToString + " Attachment"
            ElseIf reader.GetInt32("number_attach") > 1 Then
                btn_attachment.Text = reader.GetInt32("number_attach").ToString + " Attachments"
            Else
                btn_attachment.Text = "Attachment"
            End If
        End If
        class_connection.con.Close()
    End Sub

    Sub initialize_table()
        dgv_soa_data.ColumnCount = 17
        dgv_soa_data.Columns(0).Name = "Code"
        dgv_soa_data.Columns(0).ReadOnly = True
        dgv_soa_data.Columns(0).Width = 70
        dgv_soa_data.Columns(1).Name = "Description"
        dgv_soa_data.Columns(1).ReadOnly = True
        dgv_soa_data.Columns(1).Width = 220
        dgv_soa_data.Columns(2).Name = "Category"
        dgv_soa_data.Columns(2).ReadOnly = True
        dgv_soa_data.Columns(3).Width = 80
        dgv_soa_data.Columns(3).Name = "No. of Years"
        dgv_soa_data.Columns(3).DefaultCellStyle.BackColor = Color.FromArgb(107, 117, 122)
        dgv_soa_data.Columns(3).ReadOnly = True
        dgv_soa_data.Columns(3).Width = 80
        dgv_soa_data.Columns(4).Name = "Percent"
        dgv_soa_data.Columns(4).DefaultCellStyle.BackColor = Color.FromArgb(107, 117, 122)
        dgv_soa_data.Columns(4).ReadOnly = True
        dgv_soa_data.Columns(4).Width = 60
        dgv_soa_data.Columns(5).Name = "No. of Units"
        dgv_soa_data.Columns(5).DefaultCellStyle.BackColor = Color.FromArgb(107, 117, 122)
        dgv_soa_data.Columns(5).ReadOnly = True
        dgv_soa_data.Columns(5).Width = 80
        dgv_soa_data.Columns(6).Name = "Fees"
        dgv_soa_data.Columns(6).DefaultCellStyle.BackColor = Color.FromArgb(107, 117, 122)
        dgv_soa_data.Columns(6).ReadOnly = True
        dgv_soa_data.Columns(6).Width = 80
        dgv_soa_data.Columns(7).Name = "No. of Years"
        dgv_soa_data.Columns(7).ReadOnly = True
        dgv_soa_data.Columns(7).Width = 80
        dgv_soa_data.Columns(8).Name = "Percent"
        dgv_soa_data.Columns(8).ReadOnly = True
        dgv_soa_data.Columns(8).Width = 60
        dgv_soa_data.Columns(9).Name = "No. of Units"
        dgv_soa_data.Columns(9).ReadOnly = True
        dgv_soa_data.Columns(9).Width = 80
        dgv_soa_data.Columns(10).Name = "Fees"
        dgv_soa_data.Columns(10).ReadOnly = True
        dgv_soa_data.Columns(10).Width = 80
        dgv_soa_data.Columns(11).Name = "No. of Years"
        dgv_soa_data.Columns(11).DefaultCellStyle.BackColor = Color.FromArgb(107, 117, 122)
        dgv_soa_data.Columns(11).ReadOnly = True
        dgv_soa_data.Columns(11).Width = 80
        dgv_soa_data.Columns(12).Name = "Percent"
        dgv_soa_data.Columns(12).DefaultCellStyle.BackColor = Color.FromArgb(107, 117, 122)
        dgv_soa_data.Columns(12).ReadOnly = True
        dgv_soa_data.Columns(12).Width = 60
        dgv_soa_data.Columns(13).Name = "No. of Units"
        dgv_soa_data.Columns(13).DefaultCellStyle.BackColor = Color.FromArgb(107, 117, 122)
        dgv_soa_data.Columns(13).ReadOnly = True
        dgv_soa_data.Columns(13).Width = 80
        dgv_soa_data.Columns(14).Name = "Fees"
        dgv_soa_data.Columns(14).DefaultCellStyle.BackColor = Color.FromArgb(107, 117, 122)
        dgv_soa_data.Columns(14).ReadOnly = True
        dgv_soa_data.Columns(14).Width = 80
        dgv_soa_data.Columns(15).Name = "Sub-total"
        dgv_soa_data.Columns(15).Width = 80
        dgv_soa_data.Columns(15).ReadOnly = True
        dgv_soa_data.Columns(16).Name = "Payment ID"
        dgv_soa_data.Columns(16).Width = 80
        dgv_soa_data.Columns(16).Visible = False
    End Sub
End Class