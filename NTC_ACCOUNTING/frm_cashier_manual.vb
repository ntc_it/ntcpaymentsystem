﻿Imports MySql.Data.MySqlClient
Imports System.Net
Imports Microsoft.Office.Interop
Imports System.IO
Public Class frm_cashier_manual
    Dim row As String(), query As String
    Dim cmd As MySqlCommand
    Dim reader As MySqlDataReader
    Dim compute_sub_total, total As Double
    Dim cashier_name, mop_cash, mop_check, endorse_id, enable_cashier_signature, user_id_cashier,
        signature_cashier, preparator_id, serial_id As String
    Dim isORSerialExist As Integer = 1
    Dim export_success As Boolean = False
    Public user_id, n_ID, authority_level As String
    Public transact_ID, orderOfPaymentID, result As Integer

    Private Sub btn_cancel_Click(sender As Object, e As EventArgs) Handles btn_cancel.Click
        frm_licenser_manual.closeManualSOA()
        frm_accountant_manual.closeManualOP()
        Close()
    End Sub

    Private Sub cmb_cashier_SelectionChangeCommitted(sender As Object, e As EventArgs) Handles _
        cmb_cashier.SelectionChangeCommitted
        cmb_cashier_id.SelectedIndex = cmb_cashier.SelectedIndex
    End Sub

    Private Sub btn_back_Click(sender As Object, e As EventArgs) Handles btn_back.Click
        frm_accountant_manual.Show()
        Close()
    End Sub

    Private Sub cbCheck_CheckedChanged(sender As Object, e As EventArgs) Handles cbCheck.CheckedChanged
        If cbCheck.Checked = True Then
            pnlCheck.Visible = True
            pnlCheck2.Visible = True
            cbCheck.BackColor = Color.FromArgb(114, 126, 138)
        Else
            pnlCheck.Visible = False
            pnlCheck2.Visible = False
            cbCheck.Checked = False
            cbCheck.BackColor = Color.FromArgb(44, 62, 80)
            tb_check_order.Text = ""
        End If
    End Sub

    Private Sub btn_print_Click_1(sender As Object, e As EventArgs) Handles btn_print.Click
        serial_duplicate_checker()

        If txtOR.Text <> "" And isORSerialExist <> 1 Then
            If Not cbCash.Checked Or (Not cbCheck.Checked And tb_check_order.Text = "") Then
                MsgBox("Select mode of payment.", MsgBoxStyle.Exclamation, "NTC Region 10")
            Else
                If cmb_cashier.Text <> "" Then
                    save_receipt()
                Else
                    MsgBox("You did not select a cashier.", MsgBoxStyle.Exclamation, "NTC Region 10")
                End If
            End If
        Else
            MsgBox("You did not set an OR number or the OR number entered already exist!",
               MsgBoxStyle.Exclamation, "NTC Region 10")
        End If
    End Sub

    Sub save_receipt()
        Try
            frm_licenser_manual.saveSOA()
            frm_accountant_manual.save_OP()

            class_connection.con.Open()
            If cbCash.Checked And cbCheck.Checked Then
                query = "insert into tbl_soa_cashier(soa_id, or_num, date_transact, or_month, or_year, " &
                    "mode_of_payment, check_number, check_date, user_id_cashier,op_id) Values('" +
                   transact_ID.ToString + "', '" + WebUtility.HtmlEncode(txtOR.Text) + "', '" +
                   txtdate.Value.ToString("yyyy-MM-dd HH:mm:ss") + "', '" + txtdate.Value.ToString("MM") +
                   "', '" + txtdate.Value.ToString("yyyy") + "', '2', '" +
                   WebUtility.HtmlEncode(tb_check_order.Text) + "', '" + txtdate.Value.ToString("yyyy-MM-dd") +
                   "', '" + cmb_cashier_id.Text + "','" + orderOfPaymentID.ToString + "')"
            ElseIf cbCash.Checked And Not cbCheck.Checked Then
                query = "insert into tbl_soa_cashier(soa_id, or_num, date_transact, or_month, or_year, " &
                    "mode_of_payment, check_number, check_date, user_id_cashier,op_id) Values('" +
                    transact_ID.ToString + "', '" + WebUtility.HtmlEncode(txtOR.Text) + "', '" +
                    txtdate.Value.ToString("yyyy-MM-dd HH:mm:ss") + "', '" + txtdate.Value.ToString("MM") +
                    "', '" + txtdate.Value.ToString("yyyy") + "','0', '', '" +
                    txtdate.Value.ToString("yyyy-MM-dd") + "', '" + cmb_cashier_id.Text + "','" +
                    orderOfPaymentID.ToString + "')"
            ElseIf Not cbCash.Checked And cbCheck.Checked Then
                query = "insert into tbl_soa_cashier(soa_id, or_num, date_transact, or_month, or_year, " &
                    "mode_of_payment, check_number, check_date, user_id_cashier,op_id) Values('" +
                    transact_ID.ToString + "', '" + WebUtility.HtmlEncode(txtOR.Text) + "', '" +
                    txtdate.Value.ToString("yyyy-MM-dd HH:mm:ss") + "', '" + txtdate.Value.ToString("MM") +
                    "', '" + txtdate.Value.ToString("yyyy") + "','1', '" +
                    WebUtility.HtmlEncode(tb_check_order.Text) + "', '" +
                    dtp_check_date.Value.ToString("yyyy-MM-dd") + "', '" + cmb_cashier_id.Text +
                    "', '" + orderOfPaymentID.ToString + "')"
            End If

            cmd = New MySqlCommand(query, class_connection.con)
            cmd.ExecuteNonQuery()
            class_connection.con.Close()

            MsgBox("Manual Input of SOA, OP and OR Completed!", MsgBoxStyle.Information, "NTC Region 10")
            Close()
        Catch ex As Exception
            class_connection.con.Close()
            MsgBox("Couldn't connect to server. Contact the IT Administrator for assistance.",
                   MsgBoxStyle.Exclamation, "NTC Region 10")
        End Try
    End Sub

    Private Sub frm_cashier_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Try
            load_cashier()

            For Each column As DataGridViewColumn In dgv_cashier.Columns
                column.SortMode = DataGridViewColumnSortMode.NotSortable
            Next

            For i = 0 To dgv_cashier.Rows.Count - 1 Step +1
                Try
                    total += dgv_cashier.Rows(i).Cells("Sub-Total").Value
                Catch ex As Exception
                    total += 0
                End Try
            Next
            tb_total.Text = FormatNumber(CDbl(total), 2)

            txtAmountInWords.Text = class_number_to_word.ConvertNumberToENG(tb_total.Text)

            Icon = My.Resources.ntc_material_logo
        Catch ex As Exception
            MsgBox("Couldn't connect to server. Contact the IT Administrator for assistance.",
                   MsgBoxStyle.Exclamation, "NTC Region 10")
            Close()
        End Try
    End Sub

    Sub initialize_table()
        dgv_cashier.ColumnCount = 3
        dgv_cashier.Columns(0).Name = "Code"
        dgv_cashier.Columns(0).ReadOnly = True
        dgv_cashier.Columns(0).Width = 70
        dgv_cashier.Columns(1).Name = "Description"
        dgv_cashier.Columns(1).ReadOnly = True
        dgv_cashier.Columns(1).Width = 300
        dgv_cashier.Columns(2).Name = "Sub-Total"
        dgv_cashier.Columns(2).ReadOnly = True
        dgv_cashier.Columns(2).Width = 100

        dgv_cashier.Rows.Clear()
    End Sub

    Sub load_cashier()
        cmb_cashier.Items.Clear()
        cmb_cashier_id.Items.Clear()

        class_connection.con.Open()
        query = "select user_id, full_name from tbl_user where authority_level = '3'"
        cmd = New MySqlCommand(query, class_connection.con)
        reader = cmd.ExecuteReader()

        While reader.Read
            cmb_cashier.Items.Add(reader.GetString("full_name"))
            cmb_cashier_id.Items.Add(reader.GetString("user_id"))
        End While
        class_connection.con.Close()
    End Sub

    Sub serial_duplicate_checker()
        Try
            class_connection.con.Open()
            query = "select count(or_num) as 'isSerialExist' from tbl_soa_cashier where or_num = '" +
                WebUtility.HtmlEncode(txtOR.Text) + "'"
            cmd = New MySqlCommand(query, class_connection.con)
            reader = cmd.ExecuteReader()

            If reader.Read Then
                isORSerialExist = reader.GetInt32("isSerialExist")
            End If
            class_connection.con.Close()
        Catch ex As Exception
            class_connection.con.Close()
            MsgBox("Couldn't connect to server. Contact the IT Administrator for assistance.",
                   MsgBoxStyle.Exclamation, "NTC Region 10")
        End Try
    End Sub
End Class